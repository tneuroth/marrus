
# Marrus  
  
A visualization system for multivariate volume data.  
  
## Authors and acknowledgment  
  
Marrus was written by Tyson Neuroth. Its design was informed through dicussions with Qi Wu and Kwan-Liu Ma at UC Davis, Aditya Konduri at India Institute of Science, MK Lee at   University of Alabama, and Martin Reith and Jaqueline Chen at Sandia National Laboratory.  
  
## License  
  
The Marrus visualization system is the vis subfolder uses a modification of the MIT license which stipulates that rights for commercial use must be explicitly granted by the copyright holders. See vis/LICENSE.txt for details.  
  
The source code in workflow, tessellation, processing, io, and common, subdirectories use the MIT license, see LICENSE.txt  in each respective folder for details.     
  
## Citations  
  
When citing Marrus, please use the following paper:   

T. Neuroth, M. Rieth, K. Aditya, M. Lee, J. H. Chen and K. -L. Ma,[Level Set Restricted Voronoi Tessellation for Large scale Spatial Statistical Analysis](https://ieeexplore.ieee.org/document/9904439), in IEEE Transactions on Visualization and Computer Graphics, vol. 29, no. 1, pp. 548-558, Jan. 2023, doi: 10.1109/TVCG.2022.3209473.  
