
#include "ExprtkExpressions.hpp"
#include "Expressions.hpp"

#include <set>
#include <string>
#include <map>
#include <cstdint>

namespace Marrus {

Expressions::Expressions()
{
	m_impl.reset( new Marrus::ExprtkExpressions );
}

std::vector< std::string > Expressions::extractTokens( const std::string & expression )
{
	return m_impl->extractTokens( expression );
}

bool Expressions::validate( 
	const std::string & expression,
	const std::map< std::string, long double  > constants,
	const std::map< std::string, std::string  > derivedConstants,
	const std::set< std::string > variables,
	const std::map< std::string, std::string  > derivedVariables )
{
	return m_impl->validate( 
		expression, 
		constants, 
		derivedConstants,
		variables,
		derivedVariables );
}

Expressions::~Expressions() = default;

template < typename INPUT_TYPE > 
bool Expressions::validate( 
	const std::string & expression,
	const std::map< std::string, long double  > constants,
	const std::map< std::string, std::string  > derivedConstants,
	const std::map< std::string, INPUT_TYPE * > variables,
	const std::map< std::string, std::string  > derivedVariables )
{
	return m_impl->validate( 
		expression, 
		constants, 
		derivedConstants,
		variables,
		derivedVariables );
}
    
template < typename INPUT_TYPE, typename RESULT_TYPE > 
bool Expressions::attemptProcess ( 
	const std::string & expression,
	const size_t N_ELEMENTS,
	const std::map< std::string, long double  > constants,
	const std::map< std::string, std::string  > derivedConstants,	
	const std::map< std::string, INPUT_TYPE * > variables,
	const std::map< std::string, std::string  > derivedVariables,
	RESULT_TYPE * output )
{
	return m_impl->attemptProcess(
    	expression,
    	N_ELEMENTS,
    	constants,
    	derivedConstants,
    	variables,
    	derivedVariables,
    	output );
}

	template < typename INPUT_TYPE, typename RESULT_TYPE > 
	bool Expressions::attemptProcess( 
		const std::string & expression,
		const size_t N_ELEMENTS,
		const std::map< std::string, long double  >       constants,
		const std::map< std::string, std::string  >       derivedConstants,
		const std::map< std::string, INPUT_TYPE * >       variablesNormalized,
		const std::map< std::string, TN::Vec2< double > > variableExtents,
		const std::map< std::string, std::string  >       derivedVariables,		
		RESULT_TYPE * output )
	{
		return m_impl->attemptProcess(
	    	expression,
	    	N_ELEMENTS,
	    	constants,
	    	derivedConstants,
	    	variablesNormalized,
	    	variableExtents,
	    	derivedVariables,
	    	output );
	}

/*===============================================================*/

template 
bool Expressions::validate< float > ( 
	const std::string & expression,
	const std::map< std::string, long double   > constants,
	const std::map< std::string, std::string   > derivedConstants,
	const std::map< std::string, float * > variables,
	const std::map< std::string, std::string   > derivedVariables );

/*===============================================================*/

template 
bool Expressions::attemptProcess< double, float > ( 
	const std::string & expression,
	const size_t N_ELEMENTS,
	const std::map< std::string, long double  > constants,
	const std::map< std::string, std::string  > derivedConstants,
	const std::map< std::string, double * > variables,
	const std::map< std::string, std::string  > derivedVariables,		
	float * output );

template 
bool Expressions::attemptProcess< float, double > ( 
	const std::string & expression,
	const size_t N_ELEMENTS,
	const std::map< std::string, long double  > constants,
	const std::map< std::string, std::string  > derivedConstants,
	const std::map< std::string, float * > variables,
	const std::map< std::string, std::string  > derivedVariables,		
	double * output );

template 
bool Expressions::attemptProcess< double, double > ( 
	const std::string & expression,
	const size_t N_ELEMENTS,
	const std::map< std::string, long double  > constants,
	const std::map< std::string, std::string  > derivedConstants,
	const std::map< std::string, double * > variables,
	const std::map< std::string, std::string  > derivedVariables,		
	double * output );

template 
bool Expressions::attemptProcess< float, float > ( 
	const std::string & expression,
	const size_t N_ELEMENTS,
	const std::map< std::string, long double  > constants,
	const std::map< std::string, std::string  > derivedConstants,
	const std::map< std::string, float * > variables,
	const std::map< std::string, std::string  > derivedVariables,		
	float * output );

template 
bool Expressions::attemptProcess< float, uint8_t > ( 
	const std::string & expression,
	const size_t N_ELEMENTS,
	const std::map< std::string, long double  > constants,
	const std::map< std::string, std::string  > derivedConstants,
	const std::map< std::string, float * > variables,
	const std::map< std::string, std::string  > derivedVariables,		
	uint8_t * output );


template 
bool Expressions::attemptProcess< double, uint8_t > ( 
	const std::string & expression,
	const size_t N_ELEMENTS,
	const std::map< std::string, long double  > constants,
	const std::map< std::string, std::string  > derivedConstants,
	const std::map< std::string, double * > variables,
	const std::map< std::string, std::string  > derivedVariables,		
	uint8_t * output );


// ===============================================================


template 
bool Expressions::attemptProcess< double, float > ( 
	const std::string & expression,
	const size_t N_ELEMENTS,
	const std::map< std::string, long double  > constants,
	const std::map< std::string, std::string  > derivedConstants,
	const std::map< std::string, double * > variables,
	const std::map< std::string, TN::Vec2< double > > variableExtents,
	const std::map< std::string, std::string  > derivedVariables,		
	float * output );

template 
bool Expressions::attemptProcess< float, double > ( 
	const std::string & expression,
	const size_t N_ELEMENTS,
	const std::map< std::string, long double  > constants,
	const std::map< std::string, std::string  > derivedConstants,
	const std::map< std::string, float * > variables,
	const std::map< std::string, TN::Vec2< double > > variableExtents,
	const std::map< std::string, std::string  > derivedVariables,		
	double * output );

template 
bool Expressions::attemptProcess< double, double > ( 
	const std::string & expression,
	const size_t N_ELEMENTS,
	const std::map< std::string, long double  > constants,
	const std::map< std::string, std::string  > derivedConstants,
	const std::map< std::string, double * > variables,
	const std::map< std::string, TN::Vec2< double > > variableExtents,
	const std::map< std::string, std::string  > derivedVariables,		
	double * output );

template 
bool Expressions::attemptProcess< float, float > ( 
	const std::string & expression,
	const size_t N_ELEMENTS,
	const std::map< std::string, long double  > constants,
	const std::map< std::string, std::string  > derivedConstants,
	const std::map< std::string, float * > variables,
	const std::map< std::string, TN::Vec2< double > > variableExtents,
	const std::map< std::string, std::string  > derivedVariables,		
	float * output );

template 
bool Expressions::attemptProcess< float, uint8_t > ( 
	const std::string & expression,
	const size_t N_ELEMENTS,
	const std::map< std::string, long double  > constants,
	const std::map< std::string, std::string  > derivedConstants,
	const std::map< std::string, float * > variables,
	const std::map< std::string, TN::Vec2< double > > variableExtents,
	const std::map< std::string, std::string  > derivedVariables,		
	uint8_t * output );


template 
bool Expressions::attemptProcess< double, uint8_t > ( 
	const std::string & expression,
	const size_t N_ELEMENTS,
	const std::map< std::string, long double  > constants,
	const std::map< std::string, std::string  > derivedConstants,
	const std::map< std::string, double * > variables,
	const std::map< std::string, TN::Vec2< double > > variableExtents,
	const std::map< std::string, std::string  > derivedVariables,		
	uint8_t * output );


} // end namespace Marrus