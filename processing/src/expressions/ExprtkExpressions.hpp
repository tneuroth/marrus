#ifndef MARRUS_EXPRTK_EXPRESSIONS_PROCESSING_HPP
#define MARRUS_EXPRTK_EXPRESSIONS_PROCESSING_HPP

#include "algorithms/Standard/MyAlgorithms.hpp"
#include "utils/ParseUtils.hpp"
#include "ExprtkConsts.hpp"

#include "exprtk/exprtk.hpp"

#include <map>
#include <set>
#include <string>

namespace Marrus 
{	

class ExprtkExpressions 
{
    std::vector< std::string > getExpressionIdentifiers( const std::string & expression_str ) const
    {
        typedef exprtk::lexer::generator generator_t;

        generator_t generator;
        generator.process( expression_str );

        std::vector< std::string > tokens;

        for (std::size_t i = 0; i < generator.size(); ++i)
        {
            if(  ( ExprtkOperators.find( generator[ i ].value ) == ExprtkOperators.end() )
              && ( ExprtkKeyWords.find( generator[ i ].value ) ==  ExprtkKeyWords.end() )
              && ( ExprtkIllegalFirst.find( generator[ i ].value[ 0 ] ) == ExprtkIllegalFirst.end() ) )
            tokens.push_back( generator[ i ].value );
        }

        return tokens;
    }

    double evaluateConstExpression(
        const std::string & expression_str,
        const std::map< std::string, long double > & constants,
        const std::map< std::string, std::string > & derivedConstants ) const
    {
        typedef exprtk::symbol_table< long double > symbol_table_t;
        typedef exprtk::expression<   long double > expression_t;
        typedef exprtk::parser<       long double > parser_t;
        typedef exprtk::lexer::generator generator_t;

        generator_t generator;
        generator.process( expression_str );

        symbol_table_t symbol_table;
        symbol_table.add_constants();

        std::vector< std::string > identifiers = getExpressionIdentifiers( expression_str );

        for( auto & id : identifiers )
        {
            if( constants.find( id ) != constants.end() )
            {
                symbol_table.add_constant( id, constants.at( id ) );
            }
            else if ( derivedConstants.find( id ) != derivedConstants.end() )
            {
                symbol_table.add_constant( 
                    id, 
                    evaluateConstExpression( 
                        derivedConstants.at( id ), 
                        constants, 
                        derivedConstants ) );
            }
        }

        expression_t expression;
        expression.register_symbol_table( symbol_table );

        parser_t parser;
        parser.compile( expression_str, expression );

        return expression.value();
    }

	std::string replaceConstants(
        const std::string & expression_str,
        const std::map< std::string, long double > & constants,
        const std::map< std::string, std::string > & derivedConstants ) const
    {
        typedef exprtk::lexer::generator generator_t;

        generator_t generator;
        generator.process( expression_str );

        std::string result;

        for (std::size_t i = 0; i < generator.size(); ++i)
        {
            if( derivedConstants.find( generator[i].value ) != derivedConstants.end() )
            {
                result += "(" + TN::to_string_with_precision( 
                    evaluateConstExpression( 
                        derivedConstants.find( generator[ i ].value )->second, 
                        constants, 
                        derivedConstants ), 
                    20 ) + ")";
            }
            else if( constants.find( generator[i].value ) != constants.end() )
            {
                result += "(" + TN::to_string_with_precision( 
                    constants.find( generator[i].value )->second, 20 ) + ")";
            }
            else if( TN::validateNumber( generator[ i ].value ) )
            {
                result += "(" + generator[ i ].value + ")";
            }
            else
            {
                result += generator[ i ].value;
            }
        }
        
        return result;
    }

    bool validateConstExpression(
        const std::string & expression_str,
        const std::map< std::string, long double > & constants,
        const std::map< std::string, std::string > & derivedConstants ) const
    {
        typedef exprtk::symbol_table<double> symbol_table_t;
        typedef exprtk::expression<double> expression_t;
        typedef exprtk::parser<double> parser_t;
        typedef exprtk::lexer::generator generator_t;

        generator_t generator;
        generator.process( expression_str );

        symbol_table_t symbol_table;
        symbol_table.add_constants();

        std::vector< std::string > identifiers = getExpressionIdentifiers( expression_str );

        for( auto & id : identifiers )
        {
            bool identifierValid = false;

            if( constants.find( id ) != constants.end() )
            {
                symbol_table.add_constant( id, constants.at( id ) );
                identifierValid = true;
            }
            else if ( derivedConstants.find( id ) != derivedConstants.end() )
            {
                symbol_table.add_constant( 
                    id, 
                    evaluateConstExpression( 
                        derivedConstants.at( id ), 
                        constants, 
                        derivedConstants ) );

                identifierValid = true;
            }

            if( identifierValid == false )
            {
                return false;
            }
        }

        expression_t expression;
        expression.register_symbol_table(symbol_table);

        parser_t parser;
        if( ! parser.compile( expression_str, expression ) )
        {
            return false;
        }

        return true;
    }

    bool validateExpression(
        const std::string & expression_str,
        const std::map< std::string, long double  > & constants,
        const std::map< std::string, std::string  > & derivedConstants,
        const std::set< std::string > & variables,
        const std::map< std::string, std::string  > & derivedVariables )
    {
        if( expression_str == "" )
        {
            return false;
        }

        typedef exprtk::symbol_table < double > symbol_table_t;
        typedef exprtk::expression<    double > expression_t;
        typedef exprtk::parser<        double > parser_t;
        typedef exprtk::lexer::generator generator_t;

        generator_t generator;
        generator.process( expression_str );

        std::vector< std::string > variablesIds;
        std::vector< double > variableValues;
        std::vector< std::string > constantIds;

        std::vector< std::string > expressions;
        std::vector< double > expressionValues;

        for (std::size_t i = 0; i < generator.size(); ++i)
        {
            if( derivedConstants.find( generator[i].value ) != derivedConstants.end() )
            {
                constantIds.push_back( generator[i].value );
            }
            if( constants.find( generator[i].value ) != constants.end() )
            {
                constantIds.push_back( generator[i].value );
            }
            if( variables.find( generator[i].value ) != variables.end() )
            {
                variablesIds.push_back( generator[i].value );
                variableValues.push_back( 1.0 );
            }
            if( derivedVariables.find( generator[i].value ) != derivedVariables.end() )
            {
                expressions.push_back( generator[i].value );
                expressionValues.push_back( 1.0 );
            }
        }

        symbol_table_t symbol_table;
        symbol_table.add_constants();

        for( size_t i = 0, end = variablesIds.size(); i < end; ++i )
        {
            symbol_table.add_variable( variablesIds[ i ], variableValues[ i ] );
        }

        for( size_t i = 0, end = expressions.size(); i < end; ++i )
        {
            symbol_table.add_variable( expressions[ i ], expressionValues[ i ] );
        }

        for( const auto & str : constantIds )
        {
            symbol_table.add_constant( str, 1.0 );
        }

        expression_t expression;
        expression.register_symbol_table( symbol_table );

        parser_t parser;

        if (! parser.compile( expression_str, expression) )
        {
           return false;
        }

        return true;
    }


	template< typename INPUT_TYPE >
	bool validateExpression(
		const std::string & expression_str,
		const std::map< std::string, long double  > & constants,
		const std::map< std::string, std::string  > & derivedConstants,
		const std::map< std::string, INPUT_TYPE * > & variables,
		const std::map< std::string, std::string  > & derivedVariables )
    {
        if( expression_str == "" )
        {
            return false;
        }

        typedef exprtk::symbol_table <INPUT_TYPE > symbol_table_t;
        typedef exprtk::expression<   INPUT_TYPE > expression_t;
        typedef exprtk::parser<       INPUT_TYPE > parser_t;
        typedef exprtk::lexer::generator generator_t;

        generator_t generator;
        generator.process( expression_str );

        std::vector< std::string > variablesIds;
        std::vector< INPUT_TYPE > variableValues;
        std::vector< std::string > constantIds;

        std::vector< std::string > expressions;
        std::vector< INPUT_TYPE > expressionValues;

        for (std::size_t i = 0; i < generator.size(); ++i)
        {
            if( derivedConstants.find( generator[i].value ) != derivedConstants.end() )
            {
                constantIds.push_back( generator[i].value );
            }
            if( constants.find( generator[i].value ) != constants.end() )
            {
                constantIds.push_back( generator[i].value );
            }
            if( variables.find( generator[i].value ) != variables.end() )
            {
                variablesIds.push_back( generator[i].value );
                variableValues.push_back( 1.0 );
            }
            if( derivedVariables.find( generator[i].value ) != derivedVariables.end() )
            {
                expressions.push_back( generator[i].value );
                expressionValues.push_back( 1.0 );
            }
        }

        symbol_table_t symbol_table;
        symbol_table.add_constants();

        for( size_t i = 0, end = variablesIds.size(); i < end; ++i )
        {
            symbol_table.add_variable( variablesIds[ i ], variableValues[ i ] );
        }

        for( size_t i = 0, end = expressions.size(); i < end; ++i )
        {
            symbol_table.add_variable( expressions[ i ], expressionValues[ i ] );
        }

        for( const auto & str : constantIds )
        {
            symbol_table.add_constant( str, 1.0 );
        }

        expression_t expression;
        expression.register_symbol_table( symbol_table );

        parser_t parser;

        if (! parser.compile( expression_str, expression) )
        {
           return false;
        }

        return true;
    }

	template< typename INPUT_TYPE >
    std::string expandExpression(
            const std::string & expression,
            const std::map< std::string, long double  > & constants,
            const std::map< std::string, std::string  > & derivedConstants,
            const std::map< std::string, INPUT_TYPE * > & variables,
            const std::map< std::string, std::string  > & derivedVariables )
    {
        std::string result;

        typedef exprtk::lexer::generator generator_t;

        generator_t generator;
        generator.process( expression );

        for (std::size_t i = 0; i < generator.size(); ++i)
        {
            if( derivedConstants.find( generator[i].value ) != derivedConstants.end() )
            {
                result += "(" + TN::to_string_with_precision( 
                	evaluateConstExpression( 
                		derivedConstants.find( 
                			generator[ i ].value )->second, 
                			constants, derivedConstants ), 
                			20 ) + ")";
            }
            else if( constants.find( generator[i].value ) != constants.end() )
            {
                result += "(" + TN::to_string_with_precision( constants.find( generator[i].value )->second, 20 ) + ")";
            }
            else if( variables.find( generator[i].value ) != variables.end() )
            {
                result += "(" + generator[i].value + ")";
            }
            else if( derivedVariables.find( generator[i].value ) != derivedVariables.end() )
            {
                const std::string dvExpr = derivedVariables.find( generator[i].value )->second;
                result += "(" + expandExpression( dvExpr, constants, derivedConstants, variables, derivedVariables ) + ")";
            }
            else if( TN::validateNumber( generator[ i ].value ) )
            {
                result += "(" + generator[ i ].value + ")";
            }
            else
            {
                result += generator[ i ].value;
            }
        }

        return result;
    }

    template< typename INPUT_TYPE, typename RESULT_TYPE >
    bool evaluateExpression(
        const std::string & expression_str,
        const size_t N_ELEMENTS,
        const std::map< std::string, INPUT_TYPE * >       variablesNormalized,
        const std::map< std::string, TN::Vec2< double > > variableExtents,

        RESULT_TYPE * result )
    {
        typedef double PR_TYPE;

        typedef exprtk::symbol_table< PR_TYPE > symbol_table_t;
        typedef exprtk::expression<   PR_TYPE >   expression_t;
        typedef exprtk::parser<       PR_TYPE >       parser_t;
        typedef exprtk::lexer::generator           generator_t;

        generator_t generator;
        generator.process( expression_str );

        symbol_table_t symbol_table;

        std::unordered_map< std::string, PR_TYPE > variableValueHolders;

        for( auto & v : variablesNormalized )
        {
            variableValueHolders.insert( { v.first, 0.0 } );
        }

        for( auto & v : variableValueHolders )
        {
            symbol_table.add_variable( v.first, v.second );
        }

        expression_t expression;
        expression.register_symbol_table( symbol_table );

        parser_t parser;

        if ( ! parser.compile( expression_str, expression ) )
        {
           return false;
        }

        for( unsigned idx = 0; idx < N_ELEMENTS; ++idx )
        {
            for( auto & v : variablesNormalized )
            {
                const TN::Vec2< double > rng = variableExtents.at( v.first );
                const INPUT_TYPE trueVal = v.second[ idx ] * ( rng.b() - rng.a() ) + rng.a();
                variableValueHolders.find( v.first )->second
                    = static_cast< INPUT_TYPE >( trueVal );
            }

            result[ idx ] = static_cast< RESULT_TYPE >( expression.value() );
        }

        return true;
    }

	template< typename INPUT_TYPE, typename RESULT_TYPE >
    bool evaluateExpression(
        const std::string & expression_str,
        const size_t N_ELEMENTS,
        const std::map< std::string, INPUT_TYPE * > & variables,
        RESULT_TYPE * result )
    {
        typedef double PR_TYPE;

        typedef exprtk::symbol_table< PR_TYPE > symbol_table_t;
        typedef exprtk::expression<   PR_TYPE >   expression_t;
        typedef exprtk::parser<       PR_TYPE >       parser_t;
        typedef exprtk::lexer::generator           generator_t;

        generator_t generator;
        generator.process( expression_str );

        symbol_table_t symbol_table;

        std::unordered_map< std::string, PR_TYPE > variableValueHolders;

        for( auto & v : variables )
        {
            variableValueHolders.insert( { v.first, 0.0 } );
        }

        for( auto & v : variableValueHolders )
        {
            symbol_table.add_variable( v.first, v.second );
        }

        expression_t expression;
        expression.register_symbol_table( symbol_table );

        parser_t parser;

        if ( ! parser.compile( expression_str, expression ) )
        {
           return false;
        }

        for( unsigned idx = 0; idx < N_ELEMENTS; ++idx )
        {
            for( auto & v : variables )
            {
                variableValueHolders.find( v.first )->second
                    = static_cast< INPUT_TYPE >( v.second[ idx ] );
            }

            result[ idx ] = static_cast< RESULT_TYPE >( expression.value() );
        }

        return true;
    }

   	template< typename INPUT_TYPE, typename RESULT_TYPE >
    bool evaluateExpression(
		const std::string & expression,
		const size_t N_ELEMENTS,
		const std::map< std::string, long double  > constants,
		const std::map< std::string, std::string  > derivedConstants,
		const std::map< std::string, INPUT_TYPE * > variables,
		const std::map< std::string, std::string  > derivedVariables,
		RESULT_TYPE * output )
    {
    	const std::string expanded = expandExpression( 
    		expression,
			constants,
			derivedConstants,
			variables,
			derivedVariables );
    
    	return evaluateExpression( expanded, N_ELEMENTS, variables, output );
    }

    template< typename INPUT_TYPE, typename RESULT_TYPE >
    bool evaluateExpression(
        const std::string & expression,
        const size_t N_ELEMENTS,
        const std::map< std::string, long double  > constants,
        const std::map< std::string, std::string  > derivedConstants,
        const std::map< std::string, INPUT_TYPE * >       variablesNormalized,
        const std::map< std::string, TN::Vec2< double > > variableExtents,
        const std::map< std::string, std::string  > derivedVariables,
        RESULT_TYPE * output )
    {
        const std::string expanded = expandExpression( 
            expression,
            constants,
            derivedConstants,
            variablesNormalized,
            derivedVariables );
    
        return evaluateExpression( 
            expanded, 
            N_ELEMENTS, 
            variablesNormalized, 
            variableExtents, 
            output );
    }

public:

    std::vector< std::string > extractTokens( const std::string & expression_str ) 
    {
        typedef exprtk::lexer::generator generator_t;
        generator_t generator;
        generator.process( expression_str );
        std::vector< std::string > result;
        for (std::size_t i = 0; i < generator.size(); ++i)
        {
            result.push_back( generator[ i ].value );
        }
        return result;
    }

    bool validate( 
        const std::string & expression,
        const std::map< std::string, long double  > constants,
        const std::map< std::string, std::string  > derivedConstants,
        const std::set< std::string > variables,
        const std::map< std::string, std::string  > derivedVariables )
    {
        return validateExpression(
            expression, 
            constants, 
            derivedConstants,
            variables,
            derivedVariables );
    }

	template < typename INPUT_TYPE > 
	bool validate( 
		const std::string & expression,
		const std::map< std::string, long double  > constants,
		const std::map< std::string, std::string  > derivedConstants,
		const std::map< std::string, INPUT_TYPE * > variables,
		const std::map< std::string, std::string  > derivedVariables )
	{
		return validateExpression(
			expression, 
			constants, 
			derivedConstants,
			variables,
			derivedVariables );
	}

	template < typename INPUT_TYPE, typename RESULT_TYPE > 
	bool attemptProcess(
		const std::string & expression,
		const size_t N_ELEMENTS,
		const std::map< std::string, long double       > constants,
		const std::map< std::string, std::string  > derivedConstants,
		const std::map< std::string, INPUT_TYPE * > variables,
		const std::map< std::string, std::string  > derivedVariables,
		RESULT_TYPE * output )
	{
		bool valid = validateExpression(
        	expression,
        	constants,
        	derivedConstants,
        	variables,
        	derivedVariables );

		if( ! valid )
		{
            std::cout << "Error invalid expression " << std::endl;
			return false;
		}

    	const std::string expandedExpression = expandExpression(
			expression,
			constants,
			derivedConstants,
			variables,
			derivedVariables );

            std::cout << "Expanded expression: " << expandedExpression << std::endl;

        std::cout << "Imp eval" << std::endl;

    	return evaluateExpression( 
			expandedExpression,
			N_ELEMENTS,
			constants,
			derivedConstants,
			variables,
			derivedVariables,		
			output );
	}

    template < typename INPUT_TYPE, typename RESULT_TYPE > 
    bool attemptProcess( 
        const std::string & expression,
        const size_t N_ELEMENTS,
        const std::map< std::string, long double  >       constants,
        const std::map< std::string, std::string  >       derivedConstants,
        const std::map< std::string, INPUT_TYPE * >       variablesNormalized,
        const std::map< std::string, TN::Vec2< double > > variableExtents,
        const std::map< std::string, std::string  >       derivedVariables,     
        RESULT_TYPE * output )
    {
        bool valid = validateExpression(
            expression,
            constants,
            derivedConstants,
            variablesNormalized,
            derivedVariables );

        if( ! valid )
        {
            std::cout << "Error invalid expression " << std::endl;
            return false;
        }

        const std::string expandedExpression = expandExpression(
            expression,
            constants,
            derivedConstants,
            variablesNormalized,
            derivedVariables );

            std::cout << "Expanded expression: " << expandedExpression << std::endl;

        std::cout << "Imp eval" << std::endl;

        return evaluateExpression( 
            expandedExpression,
            N_ELEMENTS,
            constants,
            derivedConstants,
            variablesNormalized,
            variableExtents,
            derivedVariables,       
            output );
    }

	~ExprtkExpressions() = default;
};

} // end namespace Marrus

#endif