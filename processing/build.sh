#!/bin/bash

#################################################################################################
# gets the path that this script is in

customrealpath() {
    [[ $1 = /* ]] && echo "$1" || echo "$PWD/${1#./}"
}
SCRIPT=`customrealpath $0`
THIS_PATH=`dirname $SCRIPT`
BASE_PATH=$THIS_PATH

g++ -std=c++17 -O3 -fopenmp -frounding-math -w -fPIC \
    -I$BASE_PATH/src/expressions/ \
    -I$BASE_PATH/../thirdParty/ \
    -I$BASE_PATH/../common/ \
    -o $BASE_PATH/lib/libmexprs.so --shared \
    $BASE_PATH/src/expressions/Expressions.cpp

cp $BASE_PATH/src/expressions/Expressions.hpp $BASE_PATH/include/expressions/
cp $BASE_PATH/src/expressions/ExprtkConsts.hpp $BASE_PATH/include/expressions/
