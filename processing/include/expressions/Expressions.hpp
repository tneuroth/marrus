#ifndef MARRUS_EXPRESSIONS_PROCESSING_HPP
#define MARRUS_EXPRESSIONS_PROCESSING_HPP

#include "ExprtkConsts.hpp"

#include <memory>
#include <map>
#include <string>

namespace Marrus {

class ExprtkExpressions;

class Expressions
{
    std::unique_ptr< Marrus::ExprtkExpressions > m_impl;

public:

    Expressions();

    std::vector< std::string > extractTokens( const std::string & expression );

	bool validate( 
		const std::string & expression,
		const std::map< std::string, long double  > constants,
		const std::map< std::string, std::string  > derivedConstants,
		const std::set< std::string > variables,
		const std::map< std::string, std::string  > derivedVariables );

	template < typename INPUT_TYPE > 
	bool validate( 
		const std::string & expression,
		const std::map< std::string, long double  > constants,
		const std::map< std::string, std::string  > derivedConstants,
		const std::map< std::string, INPUT_TYPE * > variables,
		const std::map< std::string, std::string  > derivedVariables );

	template < typename INPUT_TYPE, typename RESULT_TYPE > 
	bool attemptProcess( 
		const std::string & expression,
		const size_t N_ELEMENTS,
		const std::map< std::string, long double  > constants,
		const std::map< std::string, std::string  > derivedConstants,
		const std::map< std::string, INPUT_TYPE * > variables,
		const std::map< std::string, std::string  > derivedVariables,		
		RESULT_TYPE * output );

    template < typename INPUT_TYPE, typename RESULT_TYPE > 
    bool attemptProcess( 
        const std::string & expression,
        const size_t N_ELEMENTS,
        const std::map< std::string, long double  >       constants,
        const std::map< std::string, std::string  >       derivedConstants,
        const std::map< std::string, INPUT_TYPE * >       variablesNormalized,
        const std::map< std::string, TN::Vec2< double > > variableExtents,
        const std::map< std::string, std::string  >       derivedVariables,     
        RESULT_TYPE * output );

	~Expressions();
};

}

#endif
