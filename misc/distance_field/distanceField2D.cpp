#include <string>
#include <vector>
#include <cstdint>
#include <iostream>

#include "TriangleMeshDistance.h"
#include "utils/io.hpp"
#include "utils/IndexUtils.hpp"
#include "geometry/SurfaceGenerator.cpp"

std::string fileName = "../../test_data/test_data_3D/temp_slice.200.200.8.bin";

std::int64_t X = 200;
std::int64_t Y = 200;
std::int64_t Z = 8;

// temp = 15 * 120 
float isvalue = 14.1;

int main()
{
	std::vector< float > temp( X*Y*Z );
	
	loadData( fileName, temp, 0, X*Y*Z );

	std::vector< TN::Vec3< float > > verts;
	std::vector< TN::Vec3< float > > normals;

	computeIsosurface(
	    isvalue,
	    temp,
	    { X, Y, Z },
	    verts,
	    normals,
	    false );

	std::vector< std::array< double, 3 > > vertices(  verts.size() );
	std::vector< std::array<    int, 3 > > triangles( verts.size() / 3 );

	for( int i = 0; i < verts.size(); ++i )
	{
		vertices[ i ] = { verts[ i ].x(), verts[ i ].y(), verts[ i ].z() };
	}

	for( int i = 0; i < verts.size() / 3; ++i )
	{
		triangles[ i ] = { i*3, i*3+1, i*3+2 };
	}

	tmd::TriangleMeshDistance mesh_distance( vertices, triangles );
	std::vector< float > distanceField( X*Y );

	// #pragma omp parallel for
	for( int64_t i = 0; i < X*Y; ++i )
	{
		auto idx3 = TN::index3dCM( i, X, Y, Z );
		float sign = temp[ i ] < isvalue ? 1.f : -1.f;

		tmd::Result result = mesh_distance.signed_distance( { (double) idx3.x(), (double) idx3.y(), (double) idx3.z() } );
		distanceField[ i ] = sign * std::abs( result.distance );
	}

	writeData( "../../test_data/test_data_3D/distance_slice.200.200.bin", distanceField.data(), distanceField.size() );
}
