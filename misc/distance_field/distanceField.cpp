#include <string>
#include <vector>
#include <cstdint>
#include <iostream>

#include "TriangleMeshDistance.h"
#include "utils/io.hpp"
#include "utils/IndexUtils.hpp"
#include "geometry/SurfaceGenerator.cpp"

std::string directory = "/home/ubuntu/dev/marrus/channel/1024x192x512/data/";
std::string fname = "turb_ke.bin";

const std::int64_t X = 512;
const std::int64_t Y = 512;
const std::int64_t Z = 512;

// temp = 15 * 120
float isvalue = 0.08; // 1800

float distance( 
	const TN::Vec3< float > & p,
	const std::vector< TN::Vec3< float > > & verts )
{
	float md = 9999999999;
	for( const auto & v : verts )
	{
		float dx = p.x() - v.x();
		float dy = p.y() - v.y();
		float dz = p.z() - v.z();
		float distq = dx*dx + dy*dy + dz*dz;
		md = std::min( distq, md );
	}
	return md;
}

int main()
{
	std::vector< float > temp( X*Y*Z );
	
	loadData( directory + fname, temp, 0, X*Y*Z );

	std::vector< TN::Vec3< float > > verts;
	std::vector< TN::Vec3< float > > normals;

	computeIsosurface(
	    isvalue,
	    temp,
	    { X, Y, Z },
	    verts,
	    normals,
	    false );

	std::vector< std::array< double, 3 > > vertices(  verts.size() );
	std::vector< std::array<    int, 3 > > triangles( verts.size() / 3 );

	#pragma omp parallel for
	for( int i = 0; i < verts.size(); ++i )
	{
		vertices[ i ] = { verts[ i ].x(), verts[ i ].y(), verts[ i ].z() };
	}

	#pragma omp parallel for
	for( int i = 0; i < verts.size() / 3; ++i )
	{
		triangles[ i ] = { i*3, i*3+1, i*3+2 };
	}

	tmd::TriangleMeshDistance mesh_distance( vertices, triangles );
	std::vector< float > distanceField( X*Y*Z );

	#pragma omp parallel for
	for( int64_t i = 0; i < X*Y*Z; ++i )
	{
		auto idx3 = TN::index3dCM( i, X, Y, Z );
		float sign = temp[ i ] < isvalue ? -1.f : 1.f;

		tmd::Result result = mesh_distance.signed_distance( { (double) idx3.x(), (double) idx3.y(), (double) idx3.z() } );
		distanceField[ i ] = sign * std::abs( result.distance );
	}

	writeData( directory + "turb_ke_dist_0.08.bin", distanceField.data(), distanceField.size() );
}
