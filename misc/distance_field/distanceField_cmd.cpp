#include <string>
#include <vector>
#include <cstdint>
#include <iostream>
#include <stdexcept>
#include "TriangleMeshDistance.h"
#include "utils/io.hpp"
#include "utils/IndexUtils.hpp"
#include "geometry/SurfaceGenerator.cpp"

void printUsage(const char* programName) {
    std::cerr << "Usage: " << programName 
              << " <filepath> <isovalue> <X> <Y> <Z> [output_filepath]" << std::endl;
    std::cerr << "  filepath: Path to input binary file" << std::endl;
    std::cerr << "  isovalue: Threshold value for isosurface extraction" << std::endl;
    std::cerr << "  X, Y, Z: Dimensions of the 3D data" << std::endl;
    std::cerr << "  output_filepath: Path to save distance field" << std::endl;
}

int main(int argc, char* argv[])
{
    // Check for minimum required arguments
    if (argc < 7) {
        printUsage(argv[0]);
        return 1;
    }

    try {

        // Parse command-line arguments
        std::string filepath = argv[1];
        float isvalue = std::stof(argv[2]);
        std::int64_t X = std::stoll(argv[3]);
        std::int64_t Y = std::stoll(argv[4]);
        std::int64_t Z = std::stoll(argv[5]);
        std::string outputPath = argv[6];

        // Allocate and load input data
        std::vector< float > temp( X*Y*Z );
        loadData( filepath, temp, 0, X*Y*Z );

        // Compute isosurface
        std::vector< TN::Vec3< float > > verts;
        std::vector< TN::Vec3< float > > normals;

        computeIsosurface(
            isvalue,
            temp,
            { X, Y, Z },
            verts,
            normals,
            false );

        // Prepare vertices and triangles
        std::vector< std::array< double, 3 > > vertices(  verts.size() );
        std::vector< std::array<    int, 3 > > triangles( verts.size() / 3 );

        #pragma omp parallel for
        for( int i = 0; i < verts.size(); ++i )
        {
            vertices[ i ] = { verts[ i ].x(), verts[ i ].y(), verts[ i ].z() };
        }

        #pragma omp parallel for
        for( int i = 0; i < verts.size() / 3; ++i )
        {
            triangles[ i ] = { i*3, i*3+1, i*3+2 };
        }

        // Compute distance field
        tmd::TriangleMeshDistance mesh_distance( vertices, triangles );
        std::vector< float > distanceField( X*Y*Z );

        #pragma omp parallel for
        for( int64_t i = 0; i < X*Y*Z; ++i )
        {
            auto idx3 = TN::index3dCM( i, X, Y, Z );
            float sign = temp[ i ] < isvalue ? -1.f : 1.f;
            tmd::Result result = mesh_distance.signed_distance( { (double) idx3.x(), (double) idx3.y(), (double) idx3.z() } );
            distanceField[ i ] = sign * std::abs( result.distance );
        }

        // Write output
        writeData( outputPath, distanceField.data(), distanceField.size() );

        std::cout << "Distance field computation completed. Output saved to: " << outputPath << std::endl;

        return 0;
    }
    catch (const std::exception& e) {
        std::cerr << "Error: " << e.what() << std::endl;
        printUsage(argv[0]);
        return 1;
    }
}