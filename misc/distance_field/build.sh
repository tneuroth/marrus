
#!/bin/bash

#################################################################################################
# gets the path that this script is in

customrealpath() {
    [[ $1 = /* ]] && echo "$1" || echo "$PWD/${1#./}"
}

SCRIPT=`customrealpath $0`
THIS_PATH=`dirname $SCRIPT`
BASE_PATH=$THIS_PATH

echo $BASE_PATH

g++ -std=c++17 -O3 -fopenmp -frounding-math -w \
    -o $BASE_PATH/distance_field_cmd \
    $BASE_PATH/../../common/algorithms/IsoContour/CubeMarcher.cpp \
    $BASE_PATH/distanceField_cmd.cpp \
    -I$BASE_PATH/../../common/ \
    -I$BASE_PATH/../../thirdParty/ \
    -I$BASE_PATH/



