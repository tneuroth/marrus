
import numpy as np

for variable in [

    "dudx_squared", 
    "dudy_squared", 
    "dudz_squared", 
    "dvdx_squared", 
    "dvdy_squared", 
    "dvdz_squared", 
    "dwdx_squared", 
    "dwdy_squared", 
    "dwdz_squared",

    "dudx_squared_0.1.2", 
    "dudy_squared_0.1.2", 
    "dudz_squared_0.1.2", 
    "dvdx_squared_0.1.2", 
    "dvdy_squared_0.1.2", 
    "dvdz_squared_0.1.2", 
    "dwdx_squared_0.1.2", 
    "dwdy_squared_0.1.2", 
    "dwdz_squared_0.1.2",

    "dudx_squared_3", 
    "dudy_squared_3", 
    "dudz_squared_3", 
    "dvdx_squared_3", 
    "dvdy_squared_3", 
    "dvdz_squared_3", 
    "dwdx_squared_3", 
    "dwdy_squared_3", 
    "dwdz_squared_3",

    "dudx_squared_4", 
    "dudy_squared_4", 
    "dudz_squared_4", 
    "dvdx_squared_4", 
    "dvdy_squared_4", 
    "dvdz_squared_4", 
    "dwdx_squared_4", 
    "dwdy_squared_4", 
    "dwdz_squared_4",

    "dudx_squared_5", 
    "dudy_squared_5", 
    "dudz_squared_5", 
    "dvdx_squared_5", 
    "dvdy_squared_5", 
    "dvdz_squared_5", 
    "dwdx_squared_5", 
    "dwdy_squared_5", 
    "dwdz_squared_5",

    "dudx_squared_6.7", 
    "dudy_squared_6.7", 
    "dudz_squared_6.7", 
    "dvdx_squared_6.7", 
    "dvdy_squared_6.7", 
    "dvdz_squared_6.7", 
    "dwdx_squared_6.7", 
    "dwdy_squared_6.7", 
    "dwdz_squared_6.7",

    "dissipation",

    "dissipation_0.1.2",
    "dissipation_3",
    "dissipation_4",
    "dissipation_5",
    "dissipation_6.7",

    "dissipation_dist_300.0",
    "dissipation_dist_110.0",
    "dissipation_dist_150.0",
    "dissipation_dist_75.0",
    "dissipation_dist_50.0",
    "dissipation_dist_10.0" ] :

    data = np.fromfile( "512x192x512/data/" + variable + ".bin", dtype='f4' ).reshape( ( 512, 192, 512 ) )
    data = data[ :, 0:96, 40:472 ]
    print( np.shape( data ) )
    data.tofile( "half_cropped/data/" + variable + ".bin" )