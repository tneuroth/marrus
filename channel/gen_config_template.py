
import sys
import json
import copy

n = len(sys.argv)

if n != 2:
	print( "requires command line argument: <tessellation directory>" )
	exit()

tessellation_directory = sys.argv[ 1 ]

data_path = "../data/"

variables = {}

configuration = {
	"bands"      : {},
	"components" : {},
	"cells"      : {}
}

group_summary = {
	"histograms"               : [],
	"moments"                  : [],
	"co-moments"               : [],
	"spectral_energy"          : []
}

####################################################

$1$

$2$

$3$

###############################################################

configuration[ "bands"      ] = copy.deepcopy( group_summary )
configuration[ "components" ] = copy.deepcopy( group_summary )
configuration[ "cells"      ] = copy.deepcopy( group_summary )

with open( tessellation_directory + "/summary/configuration.json", 'w') as f:
    json.dump( configuration, f, ensure_ascii=True, indent=4, sort_keys=False )

with open( tessellation_directory + "/summary/variables.json", 'w') as f:
    json.dump( variables, f, ensure_ascii=True, indent=4, sort_keys=False )

