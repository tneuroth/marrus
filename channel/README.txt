
The pipeline to process the channel data is mostly automated folling these steps:

(1) The channel data should be in its original hdf5 format, 
one folder containing the original 512x192x1024 data,
and another containing the 512^3 truncated datas

(2) Build the new distance_field program

It is built in build_all.sh. But you can also just do:

../misc/distance_field/build.sh

(3) Use prepare_data.py to convert the data to raw binary format,
derive the additional variables, and compute the distance function.

This script requires only to edit lines 10, 11, and 12, to specify correct directories.
It may take a while to run, because of computing the distance function.

python prepare_data.py

(4) Use the scripts to automate the scale decompositions. On my PC I 
can only process a max of 512^3, so I made a version of the 512^3 truncated
data which just uses a tukey window instead of mirror extension. If you 
can process 1024^3, you can use the mirrored version. I also setup one 
to process half the 512x192x1024 domain for testing on my computer.

First: edit line 11 in each script to specify where fdct3d_path is. 

python generate_full.py
python generate_half.py
python generate_truncated_mirrored.py 

For generate_full.py, x is repeated, y is mirrored, then the edge is smoothed
using a Tukey window, then it is zero padded 1024, and z is left alone. 

For generate_full.py it is the same except a Tukey window in z as well since it was cut in half and is no longer periodic.

For generate_truncated_mirrored.py, just symetric padding is used. Use this if you can processing 1024^3 data. 

For generate_truncated_windowed.py, a Tukey window is used to smooth the edges along each 
dimension. Use this instead of generate_truncated_mirrored.py if you can only process 512^3 data.

Running these scripts should auto-generate the summarization files, and project files.

(5) Compute the tessellations.

See compute_tessellations.sh. 

You can run this script to compute all 3 tessellations.

(6) Hopefully afterwards everything should be done, and you can open the projects
in vis/projects/channel/. The generated project files look ok, but not tested.



