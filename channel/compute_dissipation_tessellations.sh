#!/bin/bash

echo -e "\nComputing Tessellation 1\n"
cd half_cropped/tessellation/
./run.sh

echo -e "\nComputing Tessellation 2.\n"
cd ../tessellation_2/
./run.sh

echo -e "\nComputing Tessellation 3\n"
cd ../tessellation_3/
./run.sh

echo -e "\nComputing Tessellation 4\n"
cd ../tessellation_4/
./run.sh

echo -e "\nComputing Tessellation 5\n"
cd ../tessellation_5/
./run.sh
