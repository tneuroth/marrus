import matplotlib.pyplot as plt
from scipy.signal.windows import tukey
import numpy as np
import subprocess
import os
from my_helper_functions import *

##################################################################################
# specify parameters

fdct3d_path = "/home/ubuntu/dev/curveletTF-main/CurveLab-2.1.3/fdct3d/src/custom"

directory = "512x512x512/data/"

variables = [ 
    "PS_11",
    "PS_22",
    "PS_33", 
	"turb_ke"
]

variable_latex = [
	"\\\\Pi_{11}", 
	"\\\\Pi_{22}",
	"\\\\Pi_{33}", 
	"k_t"
]

lfield            = "velocity_magnitude_dist_1.22" 
project_directory = "../vis/projects/channel/"
project_name      = "radial_512x512x512"
tess_directory = "512x512x512/tessellation/"

input_dims = ( 512, 512, 512 )
D = ( 1024, 1024, 1024 )
N = 1024

###########################################################################################
# compute additional parameters

glyph_vars = [ var for var in variables ]
glyph_var_latex = [  "$\\\\hat{E}_{" + gvl + "}(\\\\mathbf{j})$" for gvl in variable_latex ]
variable_latex = [ "$" + vl + "$" for vl in variable_latex ]

f = open( 'options_template', 'r')
ops_template = f.read()
f.close()

cwd = os.getcwd()

# number of scales
nscales = int( np.floor( np.log2( N // 2 ) ) )

scale_list = [
	[ nscales - k for k in range( 1, 4         ) ],
	[ nscales - k for k in range( 4, 6         ) ],
	[ nscales - k for k in range( 6, nscales+1 ) ]
]

scale_strs = sorted( [ ".".join( sorted( [ str( scale ) for scale in scales ] ) ) for scales in scale_list ] )

#############################################################################################
# auto-generate summarization and project configurations

generate_summary_config( variables, tess_directory, scale_strs, N )

generate_vis_config( 
	variables,
	variable_latex,
	scale_strs,
	glyph_vars,
	glyph_var_latex,
	input_dims, 
	tess_directory, 
	directory,
	lfield,
	project_directory,
	project_name )

#############################################################################################
# do the scale decomposition

def mirror_pad( variable ) :

	data = np.fromfile( directory + variable + ".bin", dtype='f4' ).reshape( 512, 512, 512 )

	data = np.pad(
	    data,
	    ( ( 0, 512 ), ( 0, 512 ), ( 0, 512 ) ),
	    mode='symmetric' )

	return data.astype( 'f4' )

for variable in variables :	

	print( "processing " + variable )
	data = mirror_pad( variable )
	
	#print( np.shape( data ) )
	# slice_data = data[ 512, :, : ]
	# plt.figure(figsize=(8, 8))
	# plt.imshow( slice_data, cmap='coolwarm' )
	# plt.show()

	# continue

	# write the padded data to file so fdct3d can access it ######
	data.tofile( "in.bin" )

	# for each way we will filer the data based on scales we want to keep
	for scales in scale_list :

		print( "Keeping scales: " + str( scales ) )

		# set the dimensions parameters
		ops = ops_template.replace( "$1$", str( N ) )

		# set the input file parameter
		ops = ops.replace( "$2$", cwd + "/in.bin" )

		# set the output directory parameter
		ops = ops.replace( "$3$", cwd )

		# set the scales parameter
		ops = ops.replace( "$4$", ",".join( [ str( scale ) for scale in scales ] ) )

		# write the options file so fdct3d can read it
		f = open( 'options_cstm', 'w')
		f.write( ops )
		f.close()

		# run the fdct3s program
		command = [ fdct3d_path ]
		print( subprocess.check_output( command ) )

		# load the output of fdct3d and remove the padding
		result = np.fromfile( "out.bin", dtype='f4' ).reshape( D )

		# extract origional domain from padded result
		result = result[ 512:1024, 512:1024, 512:1024 ]

		# write the result to file
		result.tofile(
			directory + variable  + "_"
			+ ".".join( sorted( [ str( scale ) for scale in scales ] ) )
			+ ".bin" );

	# remove the temporary data
	subprocess.check_output( [ "rm", "in.bin"       ] )
	subprocess.check_output( [ "rm", "out.bin"      ] )
	subprocess.check_output( [ "rm", "options_cstm" ] )


