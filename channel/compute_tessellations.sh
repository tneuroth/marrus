#!/bin/bash

echo -e "\nComputing Tessellation small data full domain.\n"
cd 1024x192x512/tessellation/
./run.sh

echo -e "\nComputing Tessellation for half domain of small data.\n"
cd ../../512x192x512/tessellation/
./run.sh

echo -e "\nComputing Tessellation for truncated 512^3 data\n"
cd ../../512x512x512/tessellation/
./run.sh
