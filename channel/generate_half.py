import matplotlib.pyplot as plt
import numpy as np
from scipy.signal.windows import tukey
import subprocess
import os
from my_helper_functions import *

###########################################################################################
# specify parameters

fdct3d_path = "/home/ubuntu/dev/curveletTF-main/CurveLab-2.1.3/fdct3d/src/custom"

directory = "512x192x512/data/"

variables = [ 
    "dudx_squared", 
    "dudy_squared", 
    "dudz_squared", 
    "dvdx_squared", 
    "dvdy_squared", 
    "dvdz_squared", 
    "dwdx_squared", 
    "dwdy_squared", 
    "dwdz_squared",
    "dissipation" ]

variable_latex = [
    "\\\\left(\\\\frac{\\\\partial u}{\\\\partial x}\\\\right)^2", 
    "\\\\left(\\\\frac{\\\\partial u}{\\\\partial y}\\\\right)^2", 
    "\\\\left(\\\\frac{\\\\partial u}{\\\\partial z}\\\\right)^2", 
    "\\\\left(\\\\frac{\\\\partial v}{\\\\partial x}\\\\right)^2", 
    "\\\\left(\\\\frac{\\\\partial v}{\\\\partial y}\\\\right)^2", 
    "\\\\left(\\\\frac{\\\\partial v}{\\\\partial z}\\\\right)^2", 
    "\\\\left(\\\\frac{\\\\partial w}{\\\\partial x}\\\\right)^2", 
    "\\\\left(\\\\frac{\\\\partial w}{\\\\partial y}\\\\right)^2", 
    "\\\\left(\\\\frac{\\\\partial w}{\\\\partial z}\\\\right)^2",
    "\\\\epsilon" ]

lfield            = "dissipation" 
project_directory = "../vis/projects/channel/"
project_name      = "half_cropped.json"
tess_directory    = "half_cropped/tessellation_4/"

output_dims = ( 512, 192, 512 )
dims_padded = ( 512, 512, 512 )
N = dims_padded[ 0 ]

###########################################################################################
# compute additional parameters

glyph_vars = [ var for var in variables ]
glyph_var_latex = [  "$\\\\hat{E}_{" + gvl + "}(\\\\mathbf{j})$" for gvl in variable_latex ]
variable_latex = [ "$" + vl + "$" for vl in variable_latex ]

f = open( 'options_template', 'r')
ops_template = f.read()
f.close()

cwd = os.getcwd()

# number of scales
nscales = int( np.floor( np.log2( N // 2 ) ) )

scale_list = [ [0,1,2],[3],[4],[5],[6,7] ]
scale_strs = sorted( [ ".".join( sorted( [ str( scale ) for scale in scales ] ) ) for scales in scale_list ] )

####################################################################################################
# auto-generate summarization and project configurations

generate_summary_config( variables, tess_directory, scale_strs, N )

generate_vis_config( 
	variables,
	variable_latex,
	scale_strs,
	glyph_vars,
	glyph_var_latex,
	output_dims, 
	tess_directory, 
	directory,
	lfield,
	project_directory,
	project_name )

exit()

###############################################################################################
# do the scale decomposition

def pad_channel( variable ) :

	data = np.fromfile( directory + variable + ".bin", dtype='f4' ).reshape( 512, 192, 512 )

	window_y = tukey( 512, 0.2 ).reshape( 1, 512, 1 )
	window_z = tukey( 512, 0.2 ).reshape( 1, 1, 512 )

	ypad = ( 512 - 192 ) // 2

	data = np.pad(
	    data,
	    ( ( 0, 0 ), ( ypad, ypad ), ( 0, 0 ) ),
	    mode='reflect')

	data = data * window_y
	data = data * window_z

	return data.astype( 'f4' )

for variable in variables :	

	print( "processing " + variable )
	data = pad_channel( variable )

	# slice_data = data[ 256, :, : ]
	# plt.figure(figsize=(8, 8))
	# plt.imshow( slice_data, cmap='coolwarm' )
	# plt.show()

	data.tofile( "in.bin" )

	# for each way we will filer the data based on scales we want to keep
	for scales in scale_list :

		if os.path.exists( directory + variable  + "_" + ".".join( sorted( [ str( scale ) for scale in scales ] ) ) + ".bin"):
			continue

		print( "Keeping scales: " + str( scales ) )

		# set the dimensions parameters
		ops = ops_template.replace( "$1$", str( N ) )

		# set the input file parameter
		ops = ops.replace( "$2$", cwd + "/in.bin" )

		# set the output directory parameter
		ops = ops.replace( "$3$", cwd )

		# set the scales parameter
		ops = ops.replace( "$4$", ",".join( [ str( scale ) for scale in scales ] ) )

		# write the options file so fdct3d can read it
		f = open( 'options_cstm', 'w')
		f.write( ops )
		f.close()

		# run the fdct3s program
		command = [ fdct3d_path ]
		print( subprocess.check_output( command ) )

		# load the output of fdct3d and remove the padding
		result = np.fromfile( "out.bin", dtype='f4' ).reshape( dims_padded )

		# slice_data = result[ 256, :, : ]
		# plt.figure(figsize=(8, 8))
		# plt.imshow( slice_data, cmap='coolwarm' )
		# plt.show()

		# extract origional domain from padded result
		result = result[ :, 256-192//2:256+192//2, : ]

		print( np.shape( result ) )

		# write the result to file
		result.tofile(
			directory + variable  + "_"
			+ ".".join( sorted( [ str( scale ) for scale in scales ] ) )
			+ ".bin" );

	# remove the temporary data

	if os.path.exists( "in.bin" ):
		subprocess.check_output( [ "rm", "in.bin"       ] )

	if os.path.exists( "out.bin" ):
		subprocess.check_output( [ "rm", "out.bin"      ] )

	if os.path.exists( "options_cstm" ):
		subprocess.check_output( [ "rm", "options_cstm" ] )	


