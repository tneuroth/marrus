def generate_summary_config( variables, tess_directory, scales, n ) :

    f = open( "gen_config_template.py" )
    sum_template = f.read()
    f.close()

    var_str = ""
    mu_str  = ""
    se_str  = ""

    for var in variables :

        var_str += \
            "variables[ \"" + var + "\" ] = {\n\t" + \
            "   \"file\"   : data_path + \"/" + var + ".bin\",\n\t" + \
            "   \"offset\" : 0,\n\t" + \
            "   \"type\"   : \"float\"\n" + \
            "}\n"

        mu_str += "group_summary[ \"moments\" ].append( \"" + var + "\" )\n"

        # se_str += "group_summary[ \"spectral_energy\" ].append( {\n\t" + \
        #     "\"variable\"   : \"" + var + "\",\n\t" + \
        #     "\"scale_decomposition\" : [ \""  + scales[ 0 ] + "\", \"" + scales[ 1 ] + "\", \"" + scales[ 2 ] + "\" ],\n\t" + \
        #     "\"n\" : " + str(n) + "\n" + \
        # "} )\n"

        se_str += "group_summary[ \"spectral_energy\" ].append( {\n\t" + \
            "\"variable\"   : \"" + var + "\",\n\t" + \
            "\"scale_decomposition\" : " + str(scales) + ",\n\t" + \
            "\"n\" : " + str(n) + "\n" + \
            "} )\n"

        for scale in scales :

            mu_str += "group_summary[ \"moments\" ].append( \"" + var + "_" + scale + "\" )\n"

            var_str += \
                "variables[ \"" + var + "_" + scale + "\" ] = {\n\t" + \
                "   \"file\"   : data_path + \"/" + var + "_" + scale + ".bin\",\n\t" + \
                "   \"offset\" : 0,\n\t" + \
                "   \"type\"   : \"float\"\n" + \
                "}\n"

    sum_template = sum_template.replace( "$1$", var_str )
    sum_template = sum_template.replace( "$2$", mu_str )
    sum_template = sum_template.replace( "$3$", se_str )
    
    f = open( tess_directory + "/summary/gen_config.py", 'w')
    f.write(  sum_template )
    f.close()

def generate_vis_config( 
    variables, 
    variable_latex,
    scales, 
    glyph_vars,
    glyph_var_latex,
    dims, 
    tess_directory,
    data_path,
    lfield,
    project_directory,
    project_name ) :

    f = open("vis_config_template.json")
    template = f.read()
    f.close()

    # Dims
    dims_str = "[" + str(dims[0]) + "," + str(dims[1]) + "," + str(dims[2]) + "]"

    # Define variables
    var_str = "[\n"

    for (var, latex) in zip(variables, variable_latex):
        var_str += \
            "\t\t{\n\t\t" + \
            "    \"name\"   : \"" + var + "\",\n\t\t" + \
            "    \"filePath\"   : \"../channel/" + data_path + var + ".bin\",\n\t\t" + \
            "    \"latex\"  : \"" + latex + "\"\n\t\t" + \
            "},\n"

        lx = latex.replace( '$', '' )

        for scale in scales :
            var_str += \
                "\t\t{\n\t\t" + \
                "    \"name\"   : \"" + var + "_" + scale + "\",\n\t\t" + \
                "    \"filePath\"   : \"../channel/" + data_path + var + "_" + scale + ".bin\",\n\t\t" + \
                "    \"latex\"  : \"$" + lx + "." + scale + "$" + "\"\n\t\t" + \
                "},\n"

    var_str += \
        "\t\t{\n\t\t" + \
        "    \"name\"   : \"" + lfield + "\",\n\t\t" + \
        "    \"filePath\"   : \"../channel/" + data_path + lfield + ".bin\",\n\t\t" + \
        "    \"latex\"  : \"$b_f$\"\n\t\t" + \
        "},\n"

    var_str = var_str.rstrip(",\n") + "\n\t]"

    # Glyph axes
    axes_str = "[\n"

    for (var, latex) in zip(glyph_vars, glyph_var_latex):

        components_str = "[\n"

        for scale in scales:
            components_str += \
                "\t\t\t\t\t\t{\n\t\t\t\t\t\t" + \
                "    \"name\"  : \"" + var + "_" + scale + "\",\n\t\t\t\t\t\t" + \
                "    \"latex\" : \"$\\\\mathbf{j}=(" + scale.replace(".", ",") + ")$\",\n\t\t\t\t\t\t" + \
                "    \"file\"  : \"" + "spectral_energy." + var + "_" + scale + ".bin\"\n\t\t\t\t\t\t" + \
                "},\n"

        components_str = components_str.rstrip(",\n") + "\n\t\t\t\t\t]"

        axes_str += \
            "\t\t\t\t{\n\t\t\t\t" + \
            "    \"name\"       : \"" + var + "\",\n\t\t\t\t" + \
            "    \"latex\"      : \"" + latex + "\",\n\t\t\t\t" + \
            "    \"encoding\"   : \"perceptual/viridis\",\n\t\t\t\t" + \
            "    \"components\" : " + components_str + "\n\t\t\t\t" + \
            "},\n"

    axes_str = axes_str.rstrip(",\n") + "\n\t\t\t]"

    isosurfaces = "[]"

    # isosurfaces = "[\n" + \
    #     "\t{\n" + \
    #         "\t\t\"name\"      : \"PS_22_iso\"," + \
    #         "\t\t\"field\"     : \"PS_22\"," + \
    #         "\t\t\"isovalues\" : [ -0.015, -0.0015, 0.0015, 0.015 ]" + \
    #     "\t}," + \
    #     "\t{\n" + \
    #         "\t\t\"name\"      : \"PS_22_0.1.2.iso\"," + \
    #         "\t\t\"field\"     : \"PS_22_0.1.2\"," + \
    #         "\t\t\"isovalues\" : [  -0.00075, -0.0005, -0.00025, 0.00025, 0.00075, 0.0005 ]" + \
    #     "\t}," + \
    #     "\t{\n" + \
    #         "\t\t\"name\"      : \"PS_22_3.4.iso\"," + \
    #         "\t\t\"field\"     : \"PS_22_3.4\"," + \
    #         "\t\t\"isovalues\" : [  -0.01, -0.0035, -0.001, 0.001, 0.0035, 0.01 ]" + \
    #     "\t}," + \
    #     "\t{\n" + \
    #         "\t\t\"name\"      : \"PS_22_5.6.7.iso\"," + \
    #         "\t\t\"field\"     : \"PS_22_5.6.7\"," + \
    #         "\t\t\"isovalues\" : [ -0.02, -0.01, -0.005, 0.005, 0.01, 0.02 ]" + \
    #     "\t}\n" + \
    # "]"

    template = template.replace("$1$", dims_str )
    template = template.replace("$2$", axes_str )
    template = template.replace("$3$", var_str)
    template = template.replace("$4$", "../channel/" + tess_directory)
    template = template.replace("$5$", lfield)
    template = template.replace("$6$", isosurfaces)

    f = open( project_directory + "/" + project_name + ".json", 'w')
    f.write(template)
    f.close()