import h5py
import subprocess
import os
import numpy as np

#############################################################

# specify where your raw hdf5 data is 

df_path = "/home/ubuntu/dev/marrus/misc/distance_field/distance_field_cmd"

hdf5_truncated_data_directory = "/media/ubuntu/VisData/channel/truncated/"
hdf5_small_data_directory     = "/media/ubuntu/VisData/channel/small/"

############################################################################

distance_functions = [ 
    (   "dissipation", 110.0 ), 
    # ( "dissipation", 150.0 ),
    # ( "dissipation",  75.0 ), 
    # ( "dissipation",  50.0 ),
    # ( "dissipation",  10.0 ) 
] 

#############################################################

# will derive: PS_dudx, etc., for velocity gradient

small_full_output_directory = "1024x192x512/data/"
small_half_output_directory = "512x192x512/data/"
truncated_output_directory  = "512x512x512/data/"

# variables = [ 
#     "P", 
#     "u", "v", "w", 
#     "dudx", "dudy", "dudz", 
#     "dvdx", "dvdy", "dvdz", 
#     "dwdx", "dwdy", "dwdz" ]

gradients = [ 
    "dudx", "dudy", "dudz", 
    "dvdx", "dvdy", "dvdz", 
    "dwdx", "dwdy", "dwdz" ]

# load all data and convert to raw binary files ######

# for ( input_dir, output_dir ) in zip( 
# 	(   hdf5_small_data_directory, hdf5_truncated_data_directory,   hdf5_small_data_directory ), 
# 	( small_full_output_directory,    truncated_output_directory, small_half_output_directory ) ) :
    
for ( input_dir, output_dir ) in zip( 
    [   hdf5_small_data_directory ], 
    [ small_half_output_directory ] ) :

    dims = (  512, 512, 512 ) 

    if output_dir == small_full_output_directory :
        dims = ( 1024, 192, 512 ) # 1024 is first because distance function expects column order 
        
    elif output_dir == small_half_output_directory :
        dims = ( 512, 192, 512 )

    print()
    print( "Processing data in " + input_dir ) 
    print( "Writing output to "  + output_dir )
    print()
    print( "Converting hdf5 to raw binary files" )

    for var in gradients :
        
        file = h5py.File( input_dir + "/" + var + "_real_step_000000000_time_0.000E+00.h5", 'r' )

        data = np.array( file[ var ] ).astype( 'f4' )
        
        if output_dir == small_half_output_directory :
            data = data.reshape( ( 512, 192, 1024 ) )
            data = data[ :, :, 0:512 ]

        data.tofile( output_dir + var + ".bin" )

    # dissipation

    gradSum = np.zeros( dims[0]*dims[1]*dims[2] )

    for v_grad in gradients : 

        vg   = np.fromfile( output_dir + v_grad + ".bin", dtype='f4' )
        term = np.square( vg )
        
        print( v_grad + "^2 average : " + str( np.mean( term ) ) )

        gradSum = gradSum + term
        
        term.tofile( output_dir + v_grad + "_squared.bin" )

    # exit()

    gradSum = gradSum.astype( 'f4' )
    gradSum.tofile( output_dir + "dissipation.bin"  )    

    # compute distance functions ###################################

    print( "computing distance fields" )

    for df in distance_functions :

        print( "df = " + df[ 0 ] + " at isovalue " + str( df[ 1 ] ) )

        command = [ df_path, \
            output_dir + "/" + df[ 0 ] + ".bin",
            str( df[ 1 ] ),
            str( dims[ 0 ] ), str( dims[ 1 ] ), str( dims[ 2 ] ),
            output_dir + "/" + df[ 0 ] + "_dist_" + str( df[ 1 ] ) + ".bin" ]

        print( subprocess.check_output( command ) )

