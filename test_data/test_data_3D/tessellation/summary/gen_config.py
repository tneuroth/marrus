
import sys
import json
import copy

n = len(sys.argv)

if n != 2:
	print( "requires command line argument: <tessellation directory>" )
	exit()

tessellation_directory = sys.argv[ 1 ]

#################################################################

# specify the variables we are working with and how to load them

data_path = "../"

variables = {}

variables[ "Temp" ] = {
	"file"   : data_path + "/ts.5.6.data.bin",
	"offset" : 0,
	"type"   : "float"
}

variables[ "OH" ] = {
	"file"   : data_path + "/ts.5.6.data.bin",
	"offset" : 1,
	"type"   : "float"
}

variables[ "Delta_OH_m1" ] = {
	"file"   : data_path + "/ts.5.6.derived.bin",
	"offset" : 0,
	"type"   : "float"
}

variables[ "Delta_OH" ] = {
	"file"   : data_path + "/ts.5.6.derived.bin",
	"offset" : 1,
	"type"   : "float"
}

variables[ "OH_grad_mag" ] = {
	"file"   : data_path + "/ts.5.6.derived.bin",
	"offset" : 2,
	"type"   : "float"
}

variables[ "Delta_Temp_m1" ] = {
	"file"   : data_path + "/ts.5.6.derived.bin",
	"offset" : 3,
	"type"   : "float"
}

variables[ "Delta_Temp" ] = {
	"file"   : data_path + "/ts.5.6.derived.bin",
	"offset" : 4,
	"type"   : "float"
}

variables[ "Temp_grad_mag" ] = {
	"file"   : data_path + "/ts.5.6.derived.bin",
	"offset" : 5,
	"type"   : "float"
}

############################################################

# Specify our summarization

configuration = {
	"bands"      : {},
	"components" : {},
	"cells"      : {}
}

group_summary = {
	"histograms" : [],
	"moments"    : [],
	"co-moments" : []	
}

######################################################################
# statistical moments

for v in variables :
	group_summary[ "moments" ].append( v )

#######################################################################
# histograms 

group_summary[ "histograms" ].append(	{
	"variables" : [ "Temp", "OH" ],
	"dims"      : [ 17, 17 ],
	"edges"     : "bands"
} )

group_summary[ "histograms" ].append(	{
	"variables" : [ "OH" ],
	"dims"      : [ 17 ],
	"edges"     : "bands"
} )

#######################################################################

# co-moments

group_summary[ "co-moments" ].append( [ "Temp", "OH" ] )

#######################################################################

configuration[ "bands"      ] = copy.deepcopy( group_summary )
configuration[ "components" ] = copy.deepcopy( group_summary )
configuration[ "cells"      ] = copy.deepcopy( group_summary )

#######################################################################

# compute summary

with open( tessellation_directory + "/summary/configuration.json", 'w') as f:
    json.dump( configuration, f, ensure_ascii=True, indent=4, sort_keys=False )

with open( tessellation_directory + "/summary/variables.json", 'w') as f:
    json.dump( variables, f, ensure_ascii=True, indent=4, sort_keys=False )

