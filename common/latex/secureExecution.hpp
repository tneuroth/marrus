#ifndef TN_SEUCRE_EXECUTE
#define TN_SEUCRE_EXECUTE

#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <stdio.h>
#include <iostream>

namespace TN 
{

inline int execute( 
  const char * const argv[] )
{
    pid_t pid = fork();

    if ( pid == 0 )
    {
        execve( argv[ 0 ], (char **) argv, NULL );
        std::cerr << "could not execute external program: " << argv[ 0 ] << std::endl;
        exit ( -1 );
    }
    else
    {
      int status;
      pid_t tpid;
      
      do {
         tpid = wait(&status);
      }  while(tpid != pid);

      return status;
    }
}
}
#endif