#ifndef TN_LATEX_TABLE_GENERATOR_HPP
#define TN_LATEX_TABLE_GENERATOR_HPP

#include <string>
#include <vector>
#include <sstream>

namespace TN {

inline std::string generateLatexTable(
	const std::string & title,
	const std::vector< std::vector< std::string > > & table, 
	bool includesRowHeader,
	bool includesColHeader,
    int hLineSkip )
{
   if (table.empty() || table[0].empty()) {
        return "";
    }

    std::stringstream ss;
    const size_t rows = table.size();
    const size_t cols = table[0].size();

    ss << "\\hspace{8pt}\n";

    // Start tabular with array package's extended column types

    ss << "\\begin{tabular}{";
    for (size_t i = 0; i < cols; ++i) {
        ss << "@{}l@{\\hspace{2em}}"; // Left align all columns with consistent spacing
    }
    ss << "@{}}"; // Remove final padding

    // Add title with proper spacing
    if (!title.empty()) {
        ss << "\n\\multicolumn{" << cols << "}{@{}l}{" << title << "} \\\\\n"
           << "\\noalign{\\vspace{0.7em}}\n";
    }

    // Output headers and content
    for (size_t i = 0; i < rows; ++i) {
        for (size_t j = 0; j < cols; ++j) {

            if ((i == 0 && includesColHeader) || (j == 0 && includesRowHeader)) 
            {
                ss << table[i][j];
            } 
            else 
            {
                ss << table[i][j];
            }
            
            if (j < cols - 1) 
            {
                ss << " & ";
            }
        }
        ss << " \\\\\n";

        if(  i % hLineSkip == 0 && i+1 != rows )
        {
            ss << "\\noalign{\\vspace{0.7em}}\n";
            ss << "\\hline\n";
            ss << "\\noalign{\\vspace{0.7em}}\n";
        }
        
        // Add extra space after header row
        if (i == 0 && includesColHeader) {
            ss << "\\hline\n";
            ss << "\\noalign{\\vspace{0.7em}}\n";
        }
    }

    ss << "\\end{tabular}\n";
    ss << "\\hspace{8pt}\n";

    return ss.str();
}

}

#endif