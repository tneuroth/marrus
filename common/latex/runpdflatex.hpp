
#ifndef TN_RUN_PDF_LATEX_HPP
#define TN_RUN_PDF_LATEX_HPP

#include "secureExecution.hpp"

#include <iostream>
#include <fstream>
#include <string>

namespace TN {

inline void generateEquationPNG( 
  const std::string & tmplt,
  const std::string & equation,
  const std::string & outputDirectory,
  const std::string & name,
  const std::string & latexDir   = "/usr/bin/",
  const std::string & cairoDir   = "/usr/bin/",
  const std::string & convertDir = "/usr/bin/",
  bool generateRotatedVersion    = true,
  bool onlyIfNotExists           = false )
{
    std::ifstream texFile( tmplt );

    std::string tmpText( 
    	(std::istreambuf_iterator< char >( texFile ) ),
		(std::istreambuf_iterator< char >() ) );
    
    std::string sp[ 2 ];

    int pos = 0;
    int s   = 0;
    for( int i = 0; i < tmpText.size(); ++i )
    {
        if( tmpText[ i ] == '#' )
        {
            s = 1;
        }
        else
        {
          sp[ s ].push_back( tmpText[ i ] );
        }
    }

    texFile.close();

    std::string tex = sp[ 0 ] + equation + sp[ 1 ];

    std::string fileName = outputDirectory + "/" + name + ".tex";

    bool alreadyExists = false;

    // check if it already exists as tex file
    if( std::ifstream( fileName ).good() )
    {
        // And if the png exists too
        if( std::ifstream( outputDirectory + "/" + name + ".png" ).good() )
        {
            // Check if the existing tex file is the same as the new one

            std::ifstream existingTexFile( fileName );

            std::string existingTexFileString( 
                (std::istreambuf_iterator< char >( existingTexFile ) ),
                (std::istreambuf_iterator< char >() ) );

            if( existingTexFileString == tex )
            {
                alreadyExists = true;
            }

            existingTexFile.close();
        }
    }

    if( alreadyExists && onlyIfNotExists )
    {
        std::cout << "latex for " << name << " already exists, skipping" << std::endl;
        return;
    }


    std::ofstream outTextFile( fileName );
    outTextFile << tex;
    outTextFile.close();

    std::string pdfLatexCom = latexDir + "/pdflatex";
    std::string auxCommand = "-output-directory=" + outputDirectory;
    std::string outCommand = auxCommand;

    const char *const odflArgs[6] = { 
    	pdfLatexCom.c_str(), 
    	auxCommand.c_str(),
    	outCommand.c_str(),
    	"-halt-on-error",
    	fileName.c_str(), 
    	NULL };

    int status = TN::execute( odflArgs );

    // std::string pdfCairoCom = cairoDir + "/convert";   
    // std::string pdfFileName = outputDirectory + "/" + name + ".pdf";
    // std::string pngOutput = outputDirectory + "/" + name + ".png";

    // const char * const pdftoppmArgs[9] = 
    // {
    // 	pdfCairoCom.c_str(),     	
    // 	pdfFileName.c_str(),  
    //     pngOutput.c_str(),
    // 	NULL 
    // };

    // status = TN::execute( 
    //  pdftoppmArgs );    

    std::string pdfCairoCom = cairoDir + "/pdftocairo";   
    std::string pdfFileName = outputDirectory + "/" + name + ".pdf";
    std::string pngOutput = outputDirectory + "/" + name;

    const char * const pdftoppmArgs[9] = 
    {
        pdfCairoCom.c_str(),        
        pdfFileName.c_str(),  
        "-png",
        "-transp",
        "-f",
        "1",
        "-singlefile",
        pngOutput.c_str(),
        NULL 
    };

    status = TN::execute( 
    	pdftoppmArgs );

    std::string pdfConvCom = convertDir + "/convert"; 
    std::string pngInputV = outputDirectory + "/" + name + ".png";
    std::string pngOutputV = outputDirectory + "/" + name + "_v.png";

    const char * const rotateArgs[6] = 
    {
    	pdfConvCom.c_str(),     	
    	pngInputV.c_str(),  
    	"-rotate",
    	"-90",
    	pngOutputV.c_str(),
    	NULL 
    };

    status = TN::execute( 
    	rotateArgs );
}
}

#endif