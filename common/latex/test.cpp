
#include "runpdflatex.hpp"
#include "LatexTableGenerator.hpp"

#include <string>
#include <vector>

int main()
{
	const std::string dir = "./latex";
	TN::generateEquationPNG(
        "./templates/cropped_equation_template.tex",
        "$hello_2$",
        "./",
        "test"
	);
}