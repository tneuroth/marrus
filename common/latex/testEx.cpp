#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <stdio.h>
#include <iostream>

int main()
{
    	char *args[] = { "/usr/bin/pdflatex", "t.tex", 0 };	/* each element represents a command line argument */
	char *env[] = { 0 };	/* leave the environment list null */

	execve( "/usr/bin/pdflatex", args, env);
	perror("execve");	/* if we get here, execve failed */
	exit(1);
}
