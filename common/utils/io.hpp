#ifndef TN_UTILS_IO_HPP
#define TN_UTILS_IO_HPP

#include <fstream>
#include <string>
#include <vector>
#include <string>
#include <iostream>

// namespace TN {

// namespace io {

template <typename T >
inline std::string toStringWFMT( T val, const std::string & fmt = "%.4E" )
{
    char strBuffer[ 128 ]; 
    std::sprintf( strBuffer, fmt.c_str(), (double) val );
    return std::string( strBuffer );
}

template< typename T >
void writeData( 
    const std::string & filePath,
    const T * data,
    int64_t N )
{
    std::ofstream outFile( filePath, std::ios::out | std::ios::binary );
    if( ! outFile.is_open() ) {
        std::cerr << "file could not be opened for writing" << filePath << std::endl;
        exit( 1 );
    }
    outFile.write( ( char* ) data, N * sizeof( T )  );
    outFile.close();
}

template< typename T >
void writeDataWithSizeHeader( 
    const std::string & filePath,
    const T * data,
    int64_t N )
{
    std::ofstream outFile( filePath, std::ios::out | std::ios::binary );
    if( ! outFile.is_open() ) {
        std::cerr << "file could not be opened for writing" << filePath << std::endl;
        exit( 1 );
    }
    outFile.write( ( char* ) &N, sizeof( uint64_t )  );
    outFile.write( ( char* ) data, N * sizeof( T )  );
    outFile.close();
}

template< typename T >
void loadData( 
    const std::string & filePath,
    std::vector< T > & data, 
    size_t offset, 
    size_t numElements )
{
    std::ifstream inFile( filePath, std::ios::in | std::ios::binary );
    if( ! inFile.is_open() ) {
        std::cout << "could not be opened for reading " << filePath << std::endl;
        exit( 1 );
    }
    data.resize( numElements );
    inFile.seekg( offset * sizeof( T ) );
    inFile.read( (char*) data.data(), numElements * sizeof( T ) ); 
    inFile.close();
}

// }

// }

#endif