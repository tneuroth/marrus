#ifndef TN_COLOR_UTILS_HPP
#define TN_COLOR_UTILS_HPP

#include <string>
#include <vector>
#include <map>
#include <iostream>
#include <limits>

#include "geometry/Vec.hpp"
#include "utils/TF.hpp"
#include "utils/MatplotLibColorMaps.hpp"
#include "algorithms/Standard/MyAlgorithms.hpp"

namespace TN {

inline bool haveColorMap( const std::string & cm )
{
	return MATPLOT_LIB_COLORMAPS.count( cm ) > 0;
}

inline std::string colorMapPathRelative( const std::string & cm )
{
	if( ! MATPLOT_LIB_COLORMAPS.count( cm ) > 0 )
	{
		std::cerr << "Error: ColorUtils.hpp, colorMapPathRelative, " << cm << "not found" << std::endl;
		exit( 1 );
	}
	return MATPLOT_LIB_COLORMAPS.at( cm );
}

template< typename T >
inline void mapColors( 
	const std::string & cm_path,  
	const std::vector< T > & values,
	std::vector< TN::Vec4 > & colors )
{
	std::vector< TN::Vec3< float > > cm;
	TN::CM::loadTF( cm_path, cm );
	auto value_range = TN::Sequential::getRange( values );

	for( int i = 0; i < colors.size(); ++i )
	{
		double r = ( values[ i ] - value_range.a() ) /  ( value_range.b() - value_range.a() );
		int index = r * ( cm.size() - 1 );
		colors[ i ] = { 
			cm[ index ].r(),
			cm[ index ].g(),
			cm[ index ].b(),
		    1.0 };
	}
}

template< typename T >
inline void mapColors( 
	const std::string & cm_path,  
	const std::vector< T > & values,
	std::vector< TN::Vec3< float > > & colors )
{
	std::vector< TN::Vec3< float > > cm;
	TN::CM::loadTF( cm_path, cm );
	auto value_range = TN::Sequential::getRange( values );

	for( int i = 0; i < colors.size(); ++i )
	{
		double r = ( values[ i ] - value_range.a() ) /  ( value_range.b() - value_range.a() );
		int index = r * ( cm.size() - 1 );
		colors[ i ] = cm[ i ];
	}
}	

}

#endif