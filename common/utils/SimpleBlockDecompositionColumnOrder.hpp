#ifndef TN_SIMPLE_BLOCK_DECOMPOSITION_COLUMN_ORDER_HPP
#define TN_SIMPLE_BLOCK_DECOMPOSITION_COLUMN_ORDER_HPP

#include "geometry/Vec.hpp"
#include "utils/IndexUtils.hpp"

#include <cmath>

namespace TN
{

struct BlockDecomposition3D
{
	TN::Vec3< int > bDims;
	TN::Vec3< int > dataDims;

	bool operator==( 
		const BlockDecomposition3D & other ) const
	{
		return ( bDims    == other.bDims    )
		    && ( dataDims == other.dataDims );
	}

	void init( 
		const TN::Vec3< int > & dd,
		const TN::Vec3< int > & bd ) 
	{
		dataDims  = dd;
		bDims     = bd;
	}

	BlockDecomposition3D() {}
	BlockDecomposition3D(
		const TN::Vec3< int > & dd,
		const TN::Vec3< int > & bd ) 
	{
		init( dd, bd );
	}

	TN::Vec3< int > bIdx3D( int rank ) const { 
		auto o = TN::index3dCM( 
			rank, 
			bDims.x(),
			bDims.y(),
			bDims.z() );
		return  { (int) o.x(), (int) o.y(), (int) o.z() }; 
	}

	TN::Vec3< int > localDataOffsets( int rank ) const { 
		TN::Vec3< int > blkIdx = bIdx3D( rank );
		const int X_SZ = (int) std::ceil( dataDims.x() / (double) bDims.x() );
		const int Y_SZ = (int) std::ceil( dataDims.y() / (double) bDims.y() );
		const int Z_SZ = (int) std::ceil( dataDims.z() / (double) bDims.z() );
		return  {
			blkIdx.x() * X_SZ,
			blkIdx.y() * Y_SZ,
			blkIdx.z() * Z_SZ,
		}; 
	}

	TN::Vec3< int > localBlockSize( int rank ) const 
	{ 
		TN::Vec3< int > blkIdx = bIdx3D( rank );
		const int X_SZ = (int) std::ceil( dataDims.x() / (double) bDims.x() );
		const int Y_SZ = (int) std::ceil( dataDims.y() / (double) bDims.y() );
		const int Z_SZ = (int) std::ceil( dataDims.z() / (double) bDims.z() );

		const int X0 = blkIdx.x() * X_SZ;
		const int Y0 = blkIdx.y() * Y_SZ;
		const int Z0 = blkIdx.z() * Z_SZ;

		const int XE = std::min( dataDims.x(), ( blkIdx.x() + 1 ) * X_SZ );
		const int YE = std::min( dataDims.y(), ( blkIdx.y() + 1 ) * Y_SZ );
		const int ZE = std::min( dataDims.z(), ( blkIdx.z() + 1 ) * Z_SZ );

		return  {
			XE - X0,
			YE - Y0,
			ZE - Z0	
		}; 
	}

	int64_t gridSize1D( int rank ) const 
	{
		return ( int64_t ) dataDims.x() * dataDims.y() * dataDims.z();
	}
};

}

#endif