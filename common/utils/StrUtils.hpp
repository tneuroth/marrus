#ifndef TN_STR_UTILS_HPP
#define TN_STR_UTILS_HPP

#include <string>
#include <cstdio>

namespace TN
{

template <typename T >
inline std::string toStringWFMT( T val, const std::string & fmt = "%.2E" )
{
	char strBuffer[ 128 ]; 
	std::sprintf( strBuffer, fmt.c_str(), (double) val );
	return std::string( strBuffer );
}

}

#endif