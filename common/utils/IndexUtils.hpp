#ifndef TN_INDEX_UTILS_HPP
#define TN_INDEX_UTILS_HPP

#include "geometry/Vec.hpp"
#include <array>

namespace TN
{

inline TN::Vec3< int64_t > index3dCM( 
    const int64_t idx,
    const int64_t X,
    const int64_t Y,
    const int64_t Z )
{ 
    int64_t tmp = idx;

    int64_t x = tmp % X;
    tmp = ( tmp - x ) / X;
    int64_t y = tmp % Y;
    int64_t z  = ( tmp - y ) / Y;

    return 
    TN::Vec3< int64_t >{ x, y, z };
}

inline TN::Vec3< int64_t > index3dCM( 
    const int64_t idx,
    const std::array< int64_t, 3 > & DIMS ) { 
    return index3dCM( idx, DIMS[ 0 ], DIMS[ 1 ], DIMS[ 2 ] );
}

inline int64_t flatIndex3dCM( 
    const int64_t x,
    const int64_t y,
    const int64_t z,
    const int64_t X,
    const int64_t Y,
    const int64_t Z )
{
     return x + y*X + z*Y*X;
}

inline int64_t flatIndex3dCM( 
    const std::array< int64_t, 3 > & idx3,
    const std::array< int64_t, 3 > & DIMS )
{
     return flatIndex3dCM( 
        idx3[ 0 ], idx3[ 1 ], idx3[ 2 ], 
        DIMS[ 0 ], DIMS[ 1 ], DIMS[ 2 ] );
}

inline int64_t flatIndex2dCM( 
    const int64_t x,
    const int64_t y,
    const int64_t X,
    const int64_t Y )
{
     return x + y*X;
}

}

#endif
