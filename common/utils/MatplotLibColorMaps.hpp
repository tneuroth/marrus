
#ifndef TN_MATPLOT_LIB_CMS_HPP
#define TN_MATPLOT_LIB_CMS_HPP

#include <string>
#include <map>

namespace TN {

const std::map< std::string, std::string > MATPLOT_LIB_COLORMAPS = {
	{"en", "en.sh"},
	{"cividis", "perceptual/cividis.txt"},
	{"magma", "perceptual/magma.txt"},
	{"inferno", "perceptual/inferno.txt"},
	{"plasma", "perceptual/plasma.txt"},
	{"viridis", "perceptual/viridis.txt"},
	{"spring", "sequential2/spring.txt"},
	{"copper", "sequential2/copper.txt"},
	{"gist_yarg", "sequential2/gist_yarg.txt"},
	{"gray", "sequential2/gray.txt"},
	{"pink", "sequential2/pink.txt"},
	{"hot", "sequential2/hot.txt"},
	{"gist_gray", "sequential2/gist_gray.txt"},
	{"gist_heat", "sequential2/gist_heat.txt"},
	{"cool", "sequential2/cool.txt"},
	{"afmhot", "sequential2/afmhot.txt"},
	{"bone", "sequential2/bone.txt"},
	{"binary", "sequential2/binary.txt"},
	{"autumn", "sequential2/autumn.txt"},
	{"summer", "sequential2/summer.txt"},
	{"winter", "sequential2/winter.txt"},
	{"Wistia", "sequential2/Wistia.txt"},
	{"Reds", "sequential/Reds.txt"},
	{"PuBuGn", "sequential/PuBuGn.txt"},
	{"GnBu", "sequential/GnBu.txt"},
	{"YlGn", "sequential/YlGn.txt"},
	{"YlOrRd", "sequential/YlOrRd.txt"},
	{"Purples", "sequential/Purples.txt"},
	{"Greys", "sequential/Greys.txt"},
	{"RdPu", "sequential/RdPu.txt"},
	{"YlGnBu", "sequential/YlGnBu.txt"},
	{"Blues", "sequential/Blues.txt"},
	{"BuPu", "sequential/BuPu.txt"},
	{"BuGn", "sequential/BuGn.txt"},
	{"Oranges", "sequential/Oranges.txt"},
	{"PuRd", "sequential/PuRd.txt"},
	{"Greens", "sequential/Greens.txt"},
	{"OrRd", "sequential/OrRd.txt"},
	{"PuBu", "sequential/PuBu.txt"},
	{"YlOrBr", "sequential/YlOrBr.txt"},
	{"RdBu", "diverging/RdBu.txt"},
	{"bwr", "diverging/bwr.txt"},
	{"Spectral", "diverging/Spectral.txt"},
	{"coolwarm", "diverging/coolwarm.txt"},
	{"BrBG", "diverging/BrBG.txt"},
	{"RdGy", "diverging/RdGy.txt"},
	{"PRGn", "diverging/PRGn.txt"},
	{"RdYlBu", "diverging/RdYlBu.txt"},
	{"PuOr", "diverging/PuOr.txt"},
	{"seismic", "diverging/seismic.txt"},
	{"RdYlGn", "diverging/RdYlGn.txt"},
	{"PiYG", "diverging/PiYG.txt"},
	{"enumerate", "enumerate.sh"},
	{"twilight_shifted", "misc/twilight_shifted.txt"},
	{"flag", "misc/flag.txt"},
	{"gist_ncar", "misc/gist_ncar.txt"},
	{"cubehelix", "misc/cubehelix.txt"},
	{"gnuplot2", "misc/gnuplot2.txt"},
	{"rainbow", "misc/rainbow.txt"},
	{"terrain", "misc/terrain.txt"},
	{"nipy_spectral", "misc/nipy_spectral.txt"},
	{"CMRmap", "misc/CMRmap.txt"},
	{"gist_earth", "misc/gist_earth.txt"},
	{"gnuplot", "misc/gnuplot.txt"},
	{"ocean", "misc/ocean.txt"},
	{"jet", "misc/jet.txt"},
	{"brg", "misc/brg.txt"},
	{"gist_stern", "misc/gist_stern.txt"},
	{"prism", "misc/prism.txt"}
};

}

#endif