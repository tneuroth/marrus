    
#ifndef TN_CM_TF_HPP
#define TN_CM_TF_HPP

#include "geometry/Vec.hpp"

#include <iostream>
#include <fstream>
#include <vector>
#include <string>

namespace TN
{

namespace CM 
{

static inline
void loadTF( 
    const std::string & path, 
    std::vector< TN::Vec3< float > > & colors )
{
    std::ifstream inFile( path );
    
    if( ! inFile.is_open() )
    {
        std::cerr << "failed to open" << std::endl;
        exit ( 1 );
    }

    std::string line;  
    colors.clear();
    while( std::getline( inFile, line ) )
    {
        std::string rgb[ 3 ];
        for( int i = 1, cr = 0; i < line.size() && cr < 3; ++i )
        {
            if( line[i] == ',' )
            {
                ++cr;
                continue;
            }
            else if( line[ i ] == ')' )
            {
                break;
            }
            else if( line[ i ] == ' ' )
            {
                continue;
            }
            else
            {
                rgb[ cr ].push_back( line[ i ] );
            }
        }

        colors.push_back( TN::Vec3< float >(
            std::stof( rgb[ 0 ] ),
            std::stof( rgb[ 1 ] ),
            std::stof( rgb[ 2 ] ) ) );
    }
}
} 
}

#endif