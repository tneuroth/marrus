#ifndef TN_PARSE_UTILS_HPP
#define TN_PARSE_UTILS_HPP

#include <string>
#include <stdexcept>

namespace TN {

static inline bool
isFloat(const std::string& nm)
{
    try {
        (void)std::stod(nm);
        return nm.find(".") != std::string::npos;
    }
    catch (const std::logic_error & e) {
        return false;
    }
}

static inline bool
isInteger(const std::string& nm)
{
    try {
        (void)std::stoll(nm);
        return true;
    }
    catch (const std::logic_error & e) {
        return false;
    }
}

static inline bool
validateNumber(const std::string& nm)
{
    // return  is_numeric( nm.begin(), nm.end() );

    return isInteger(nm) || isFloat(nm);
}

static std::string
typeOfNumericLiteral(const std::string& literal)
{
    return isFloat(literal) ? "double" : "int";
}

}

#endif