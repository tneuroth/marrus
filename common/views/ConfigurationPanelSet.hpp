#ifndef TN_CONFIGURATION_PANEL_SET_HPP
#define TN_CONFIGURATION_PANEL_SET_HPP

#include "views/ConfigurationPanel.hpp"

#include <map>
#include <iostream>
#include <string>
#include <cmath>
#include <memory>

namespace TN {

class ConfigurationPanelSet : public Widget
{
	std::map< std::string, ConfigurationPanel * > m_panels;
	std::string m_selected;

public:

	virtual bool handleInput( const InputState & input ) override
	{
		if( m_panels.count( m_selected ) ) 
		{
			m_panels.at( m_selected )->handleInput( input );
		}

		return false;
	}

	virtual void applyLayout() override {
		for( auto & panel : m_panels ) {
			panel.second->setSize( size().x(), size().y() );
			panel.second->applyLayout();
		}
	}

    void addElement( 
    	const std::string  & key,
    	ConfigurationPanel * view )
    {
    	m_panels.insert( { key, view } );
    }

    void set( const std::string & key ) {
    	
    	if( m_panels.count( key ) ) {
    		m_selected = key;
    	}
    }

    ConfigurationPanel * activePanel() {
    	if( hasActivePanel() ) {
    		return m_panels.at( m_selected );
    	}
    	else {
    		return NULL;
    	}
    }

    void deactivate() {
    	m_selected = "";
    }

    bool hasActivePanel() const {
    	return m_panels.count( m_selected );
    }	

	ConfigurationPanelSet() :  Widget(), m_selected( "" ) {}
	virtual ~ConfigurationPanelSet() {}

};

} // end namespace TN

#endif