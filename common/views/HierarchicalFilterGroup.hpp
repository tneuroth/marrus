
#ifndef TN_HIERARCHICAL_FILTER_VIEW_HPP 
#define TN_HIERARCHICAL_FILTER_VIEW_HPP

#include "views/Widget.hpp"
#include "views/PressButtonWidget.hpp"
#include "views/InputState.hpp"
#include "geometry/Vec.hpp"
#include "filter/FilterView.hpp"
#include "filter/FilterOps.hpp"
#include "filter/BooleanExpressionFilterView.hpp"
#include "filter/BandFilterView.hpp"
#include "filter/UndefinedFilterView.hpp"
#include "expressions/Expressions.hpp"
#include "Tessellation.hpp"

#include <vector>
#include <memory>
#include <iostream>
#include <cmath>

namespace TN {

/*
    bands( boolean combination, statistical, t-sne  )

        components( boolean expressions, statistical, t-sne )

            Voronoi( boolean expressions, statistical, t-sne )

                voxels( boolean expressions )

*/

class FilterModuleContainer : public Widget {

    std::string m_resourceDir;

    TN::TexturedPressButton m_activateButton;
    TN::TexturedPressButton m_createModuleButton;
    TN::TexturedPressButton m_expandButton;

    std::list< std::unique_ptr< Marrus::FilterView > > m_modules;
    std::string m_label;
    Marrus::FilterOp::Type m_filterOp;
    std::set< std::string > m_validFilterTypes;
    TN::Vec3< float > m_headerColor;

    uint8_t m_level;

    void init() 
    {
        m_activateButton.setTexFromPNG(
            m_resourceDir + "/textures/active.png",
            m_resourceDir + "/textures/activePressed.png" );

        m_createModuleButton.setTexFromPNG(
            m_resourceDir + "/textures/create.png",
            m_resourceDir + "/textures/createPressed.png" );

        m_expandButton.setTexFromPNG(
            m_resourceDir + "/textures/upArrow.png",
            m_resourceDir + "/textures/downArrow.png" );

        m_activateButton.resizeByHeight(     30.f );
        m_createModuleButton.resizeByHeight( 30.f );
        m_expandButton.resizeByHeight(       30.f );

        m_filterOp = Marrus::FilterOp::Type::Intersect;

        m_validFilterTypes = {
            "Boolean Expression",
            "Band Selector"
        };

        m_headerColor = { 0.8f, 0.83f, 0.95f };
        m_expandButton.setPressed(   true );
        m_activateButton.setPressed( true );
    }

public: 

    void clear()
    {
        m_activateButton.setPressed( true );
        m_expandButton.setPressed(   true );

        m_filterOp = Marrus::FilterOp::Type::Intersect;
        m_modules.clear();
    }

    template<
        class DATA_MANAGER_T, 
        typename BAND_ID_T,   
        typename COMP_ID_T,   
        typename CELL_ID_T,
        typename FLAG_T >
    void apply( 
        Marrus::Expressions & exprProcessor,
        DATA_MANAGER_T * dm,
        const std::map< std::string, long double  > & constants,
        const std::map< std::string, std::string  > & derivedConstants,
        const std::set< std::string >               & variables,
        const std::map< std::string, std::string  > & derivedVariables,
        const std::vector< BAND_ID_T >  & comp_band_ids,
        const std::vector< COMP_ID_T >  & cell_comp_ids,
        const std::vector< CELL_ID_T >  & elem_cell_ids,
        const uint8_t filterLevel,
        std::vector< FLAG_T > & bitFlags,
        int BIT_MASK )
    {
        const int64_t N = bitFlags.size();
        for( auto & m : m_modules )
        {
            if( m->active() )
            {
                m->apply( 
                    exprProcessor, 
                    dm, 
                    constants,
                    derivedConstants,
                    variables,
                    derivedVariables,
                    comp_band_ids.data(),
                    cell_comp_ids.data(),
                    elem_cell_ids.data(),
                    N,
                    filterLevel,
                    bitFlags.data(), 
                    m_filterOp,
                    BIT_MASK );
            }
        }
    }

    void addFilter()
    {
        std::vector< std::string > options;

        if( m_level == Marrus::FilterOp::FILTER_LEVEL_BAND )
        {
            options = { "Band Selector", "Boolean Expression" };
        }

        else 
        {
            options = { "Boolean Expression" };
        }

        m_modules.push_back( 
            std::unique_ptr< Marrus::FilterView >( 
                new Marrus::UndefinedFilterView( m_resourceDir, options ) ) );
    }

    bool valid()
    {
        bool result = true;
        for( auto & m : m_modules )
        {
            result = result && ( m->valid() || ( ! m->active() ) );
        }
        return result;
    }

    bool active()
    {
        return m_activateButton.isPressed();
    }

    void updateInformation(  
        Marrus::Expressions & exprProcessor,
        const std::map< std::string, long double  > & constants,
        const std::map< std::string, std::string  > & derivedConstants,
        const std::set< std::string >               & variables,
        const std::map< std::string, std::string  > & derivedVariables,
        const Marrus::Tessellation & tesselation )
    {
        for( auto & module : m_modules )
        {
            module->updateInformation(
                exprProcessor,
                constants,
                derivedConstants,
                variables,
                derivedVariables,
                tesselation );
        }
    }

    bool handleInput( 
        const InputState & input )
    {
        Vec2< double > p = input.mouseState.position;
        MouseButtonState mouseButtonState = input.mouseState.buttonState;
        bool changed = false;

        if( pointInViewPort( p ) )
        {
            if( m_activateButton.pointInViewPort( p ) ) 
            {
                if ( input.mouseState.event == MouseEvent::LeftPressed ) {
                    m_activateButton.setPressed( ! m_activateButton.isPressed() );
                    changed = true;
                }
            }
            if( m_createModuleButton.pointInViewPort( p )  ) {                
                if ( input.mouseState.event == MouseEvent::LeftPressed ) {
                    addFilter();
                    changed = true;
                }
            }
            if( m_expandButton.pointInViewPort( p ) ) 
            {
                if ( input.mouseState.event == MouseEvent::LeftPressed ) {
                    m_expandButton.setPressed( ! m_expandButton.isPressed() );
                    changed = true;
                }
            }
        }

        if( ! isCollapsed() )
        {
            Marrus::FilterView * toRemove = 0;

            bool remove = false;
            bool replace = false;
            std::string replaceType = "";

            auto it = m_modules.begin();
            for( ; it != m_modules.end(); ++it )
            {
                auto & m = *it;
                changed |= m->handleInput( input );
                if( m->requestRemoval() )
                {
                    remove = true;
                    break;
                }

                if( m->requestDefine() )
                {
                    replaceType = m->defineType();
                    m->clearRequests();
                    if( m_validFilterTypes.count( replaceType ) )
                    {
                        replace = true;
                        break;
                    }
                }
            }
            if( remove )
            {       
                m_modules.erase( it );
            }
            else if ( replace )
            {
                if( replaceType == "Boolean Expression" )
                {
                    it->reset( new Marrus::BooleanExpressionFilterView( m_resourceDir ) ); 
                }
                else if ( replaceType == "Band Selector" )
                {
                    it->reset( new Marrus::BandSelectorFilterView( m_resourceDir ) ); 
                }
            }
        }

        return changed;
    }

    virtual void applyLayout( 
        const float X0, 
        const float YT,
        const float W ) 
    {
        float y = YT - 34 - 8;
        float M_OFFSET = 20;

        if( ! isCollapsed() )
        {
            for( auto & mod : m_modules ) {
                mod->applyLayout( X0 + M_OFFSET, y, W - M_OFFSET - 8 );
                y -= mod->size().y() + 8;
            }
        }

        m_activateButton.setPosition(     X0 + W - 32.f - 8, YT - 32 );
        m_createModuleButton.setPosition( X0 + W - 66.f - 8, YT - 32 );
        m_expandButton.setPosition(     X0 + 2        - 8, YT - 32 );

        this->setSize( W, YT - y );
        this->setPosition( X0, y );
    }

    const std::string & label() const {
        return m_label;
    }

    FilterModuleContainer( const std::string & resourceDir, const std::string & label, uint8_t level ) : 
        Widget(),
        m_resourceDir( resourceDir ),
        m_label( label ),
        m_level( level )
    {
        init();
    }

    virtual ~FilterModuleContainer() {}

    virtual std::vector< TN::TexturedPressButton * > buttons()
    {
        std::vector< TN::TexturedPressButton * > v;
        v.push_back( & m_activateButton     );
        v.push_back( & m_createModuleButton );
        v.push_back( & m_expandButton );
        return v;
    } 

    std::list< std::unique_ptr< Marrus::FilterView > > & modules()
    {
        return m_modules;
    }

    TN::Vec3< float > headerColor() const
    {
        return m_headerColor;
    }

    bool isCollapsed() const 
    {
        return ! m_expandButton.isPressed();
    }

    std::vector< TN::ComboWidget * > visibleCombos() 
    {
        if( isCollapsed() )
        {
            return {};
        }

        std::vector< TN::ComboWidget * > cmbs;
        for( auto & m : m_modules )
        {
            auto mcmbs = m->visibleCombos();
            for( TN::ComboWidget * c : mcmbs )
            {
                cmbs.push_back( c );
            }
        }

        return cmbs;
    }
};

class HierarchicalFilterGroup : public Widget 
{
	std::string m_resourceDir;

    TN::PressButton m_applyButton;
    bool m_requestApply;

    FilterModuleContainer m_bandFilter;
    FilterModuleContainer m_componentFilter;
    FilterModuleContainer m_cellFilter;
    FilterModuleContainer m_voxelFilter;

    int m_status;

	void init() {
        m_applyButton.text( "Apply" );
        m_requestApply = false;
        m_status = 0;
	};

    std::vector< FilterModuleContainer * > levelPtrs()
    {
        return {
            & m_bandFilter,
            & m_componentFilter,
            & m_cellFilter,
            & m_voxelFilter
        };
    }    

public: 

    void clear()
    {
        m_requestApply = false;

        m_bandFilter.clear();
        m_componentFilter.clear();
        m_cellFilter.clear();
        m_voxelFilter.clear();
    }

    int status() const
    {
        return m_status;
    }

    bool valid() 
    {
        auto levels = levelPtrs();
        bool result = true;
        for( auto l : levels )
        {
            result = result && ( l->valid() || ( ! l->active() ) );
        }
        return result;
    }

    template< 
        class DATA_MANAGER_T, 
        typename BAND_ID_T,   
        typename COMP_ID_T,   
        typename CELL_ID_T,
        typename FLAG_T >
    void apply( 
        Marrus::Expressions             & exprProcessor,
        DATA_MANAGER_T                  * dm,
        const TN::SimpleDataInfoSet     & rawDataInfo,
        Marrus::MultiLevelSummarization & summaryDataInfo,
        const std::vector< BAND_ID_T >  & comp_band_ids,
        const std::vector< COMP_ID_T >  & cell_comp_ids,
        const std::vector< CELL_ID_T >  & elem_cell_ids,
        std::vector< FLAG_T >           & bandBitFlags,
        std::vector< FLAG_T >           & componentBitFlags,
        std::vector< FLAG_T >           & cellBitFlags,
        std::vector< FLAG_T >           & voxelBitFlags )
    {
        m_requestApply = false;

        const size_t N_BAND = bandBitFlags.size();
        const size_t N_COMP = componentBitFlags.size();
        const size_t N_CELL = cellBitFlags.size();
        const size_t N_ELEM = voxelBitFlags.size();

        if( N_BAND == 0 )
        {
            std::cout << "N_BAND is 0" << std::endl;
            return;
        }

        namespace MFO = Marrus::FilterOp;

        std::fill( 
            bandBitFlags.begin(), 
            bandBitFlags.end(), 
            MFO::WITHIN_DECOMPOSITION 
          | MFO::WITHIN_BAND_SELECTION );

        bandBitFlags[ 0 ] &= ~ MFO::WITHIN_DECOMPOSITION;
        bandBitFlags[ 0 ] &= ~ MFO::WITHIN_BAND_SELECTION;

        std::cout << "a" << std::endl;

        if( m_bandFilter.active() )
        {
            m_bandFilter.apply( 
                exprProcessor, 
                dm, 
                rawDataInfo.constants,
                rawDataInfo.derivedConstants,
                summaryDataInfo.bandSummary.scalarFeaturesParseInfo(),
                summaryDataInfo.bandSummary.scalarDerivedFeaturesParseInfo(),
                comp_band_ids,
                cell_comp_ids,
                elem_cell_ids,
                Marrus::FilterOp::FILTER_LEVEL_BAND,
                bandBitFlags,
                MFO::WITHIN_BAND_SELECTION );
        }

        std::fill( 
            componentBitFlags.begin(), 
            componentBitFlags.end(), 
            MFO::WITHIN_DECOMPOSITION 
          | MFO::WITHIN_BAND_SELECTION 
          | MFO::WITHIN_COMPONENT_SELECTION );

        std::cout << "b" << std::endl;

        #pragma omp parallel for
        for( size_t i = 0; i < N_BAND; ++i )
        {
            const BAND_ID_T band_id = comp_band_ids[ i ];

            if( ( bandBitFlags[ band_id ] & MFO::WITHIN_DECOMPOSITION ) == 0 )
            {
                componentBitFlags[ i ] &= ~MFO::WITHIN_DECOMPOSITION;
            }

            if( ( bandBitFlags[ band_id ] & MFO::WITHIN_BAND_SELECTION ) == 0 )
            {
                componentBitFlags[ i ] &= ~MFO::WITHIN_BAND_SELECTION ;
            }
        }
        
        if( m_componentFilter.active() )
        {
            m_componentFilter.apply( 
                exprProcessor, 
                dm, 
                rawDataInfo.constants,
                rawDataInfo.derivedConstants,
                summaryDataInfo.componentSummary.scalarFeaturesParseInfo(),
                summaryDataInfo.componentSummary.scalarDerivedFeaturesParseInfo(),
                comp_band_ids,
                cell_comp_ids,
                elem_cell_ids,
                Marrus::FilterOp::FILTER_LEVEL_COMPONENT,
                componentBitFlags,
                MFO::WITHIN_COMPONENT_SELECTION );                 
        }

        std::fill( 
            cellBitFlags.begin(), 
            cellBitFlags.end(), 
            MFO::WITHIN_DECOMPOSITION 
          | MFO::WITHIN_BAND_SELECTION 
          | MFO::WITHIN_COMPONENT_SELECTION 
          | MFO::WITHIN_CELL_SELECTION );

        std::cout << "c" << std::endl;

        #pragma omp parallel for
        for( size_t i = 0; i < N_COMP; ++i )
        {
            const COMP_ID_T comp_id = cell_comp_ids[ i ];
            const BAND_ID_T band_id = comp_band_ids[ comp_id ];

            if( ( bandBitFlags[ band_id ] & MFO::WITHIN_DECOMPOSITION ) == 0 )
            {
                cellBitFlags[ i ] &= ~MFO::WITHIN_DECOMPOSITION;
            }
            if( ( bandBitFlags[ band_id ] & MFO::WITHIN_BAND_SELECTION ) == 0 )
            {
                cellBitFlags[ i ] &= ~MFO::WITHIN_BAND_SELECTION ;
            }
            if( ( componentBitFlags[ comp_id ] & MFO::WITHIN_COMPONENT_SELECTION ) == 0 )
            {
                cellBitFlags[ i ] &= ~MFO::WITHIN_COMPONENT_SELECTION ;
            }
        }

        if( m_cellFilter.active() )
        {
            m_cellFilter.apply( 
                exprProcessor, 
                dm, 
                rawDataInfo.constants,
                rawDataInfo.derivedConstants,
                summaryDataInfo.cellSummary.scalarFeaturesParseInfo(),
                summaryDataInfo.cellSummary.scalarDerivedFeaturesParseInfo(),
                comp_band_ids,
                cell_comp_ids,
                elem_cell_ids,
                Marrus::FilterOp::FILTER_LEVEL_CELL,
                cellBitFlags,
                MFO::WITHIN_CELL_SELECTION ); 
        }

        // need to check whether exists in a now selected cell

        std::fill( 
            voxelBitFlags.begin(), 
            voxelBitFlags.end(), 
            MFO::WITHIN_DECOMPOSITION 
          | MFO::WITHIN_BAND_SELECTION 
          | MFO::WITHIN_COMPONENT_SELECTION 
          | MFO::WITHIN_CELL_SELECTION
          | MFO::WITHIN_VOXEL_SELECTION );

        // #pragma omp parallel for
        for( size_t i = 0; i < N_ELEM; ++i )
        {
            const CELL_ID_T cell_id = elem_cell_ids[ i ];

            if( cell_id < 0 )
            {
                voxelBitFlags[ i ] &= ~MFO::WITHIN_DECOMPOSITION;
                voxelBitFlags[ i ] &= ~MFO::WITHIN_BAND_SELECTION;
                voxelBitFlags[ i ] &= ~MFO::WITHIN_COMPONENT_SELECTION;
                voxelBitFlags[ i ] &= ~MFO::WITHIN_CELL_SELECTION;
                voxelBitFlags[ i ] &= ~MFO::WITHIN_VOXEL_SELECTION;
            }
            else
            {
                const COMP_ID_T comp_id = cell_comp_ids[ cell_id ];
                const BAND_ID_T band_id = comp_band_ids[ comp_id ];

                if( band_id == 0 )
                {
                    voxelBitFlags[ i ] &= ~MFO::WITHIN_DECOMPOSITION;
                    voxelBitFlags[ i ] &= ~MFO::WITHIN_BAND_SELECTION;
                    voxelBitFlags[ i ] &= ~MFO::WITHIN_COMPONENT_SELECTION;
                    voxelBitFlags[ i ] &= ~MFO::WITHIN_CELL_SELECTION;
                    voxelBitFlags[ i ] &= ~MFO::WITHIN_VOXEL_SELECTION;
                }
                else if( ( bandBitFlags[ band_id ] & MFO::WITHIN_BAND_SELECTION ) == 0 )
                {
                    voxelBitFlags[ i ] &= ~MFO::WITHIN_BAND_SELECTION;
                    voxelBitFlags[ i ] &= ~MFO::WITHIN_COMPONENT_SELECTION;
                    voxelBitFlags[ i ] &= ~MFO::WITHIN_CELL_SELECTION;
                    voxelBitFlags[ i ] &= ~MFO::WITHIN_VOXEL_SELECTION;
                }
                else if( ( componentBitFlags[ comp_id ] & MFO::WITHIN_COMPONENT_SELECTION ) == 0 )
                {
                    voxelBitFlags[ i ] &= ~MFO::WITHIN_COMPONENT_SELECTION;
                    voxelBitFlags[ i ] &= ~MFO::WITHIN_CELL_SELECTION;
                    voxelBitFlags[ i ] &= ~MFO::WITHIN_VOXEL_SELECTION;             
                }
                else if( ( cellBitFlags[ comp_id ] & MFO::WITHIN_CELL_SELECTION ) == 0 )
                {
                    voxelBitFlags[ i ] &= ~MFO::WITHIN_CELL_SELECTION;
                    voxelBitFlags[ i ] &= ~MFO::WITHIN_VOXEL_SELECTION;
                }
            }
        }

        if( m_voxelFilter.active() )
        {
            std::cout << "e" << std::endl;

            m_voxelFilter.apply( 
                exprProcessor, 
                dm, 
                rawDataInfo.constants,
                rawDataInfo.derivedConstants,
                rawDataInfo.variables,
                rawDataInfo.derivedVariables,
                comp_band_ids,
                cell_comp_ids,
                elem_cell_ids,
                Marrus::FilterOp::FILTER_LEVEL_ELEMENT,
                voxelBitFlags,
                MFO::WITHIN_VOXEL_SELECTION ); 
        }

        std::cout << "f" << std::endl;

        m_status = 1;
    }

    void updateInformation(
        Marrus::Expressions & exprProcessor,
        TN::SimpleDataInfoSet & rawDataInfo,
        Marrus::MultiLevelSummarization & summaryDataInfo,
        const Marrus::Tessellation & tesselation )
    {
        m_bandFilter.updateInformation(
            exprProcessor,
            rawDataInfo.constants,
            rawDataInfo.derivedConstants,
            summaryDataInfo.bandSummary.scalarFeaturesParseInfo(),
            summaryDataInfo.bandSummary.scalarDerivedFeaturesParseInfo(),
            tesselation );

        m_componentFilter.updateInformation(
            exprProcessor,
            rawDataInfo.constants,
            rawDataInfo.derivedConstants,
            summaryDataInfo.componentSummary.scalarFeaturesParseInfo(),
            summaryDataInfo.componentSummary.scalarDerivedFeaturesParseInfo(),
            tesselation );

        m_cellFilter.updateInformation(
            exprProcessor,
            rawDataInfo.constants,
            rawDataInfo.derivedConstants,
            summaryDataInfo.cellSummary.scalarFeaturesParseInfo(),
            summaryDataInfo.cellSummary.scalarDerivedFeaturesParseInfo(),
            tesselation );

        m_voxelFilter.updateInformation(
            exprProcessor,
            rawDataInfo.constants,
            rawDataInfo.derivedConstants,
            rawDataInfo.variables,
            rawDataInfo.derivedVariables,
            tesselation );
    }

    FilterModuleContainer & bandFilterContainer() 
    {
        return m_bandFilter;
    }

    FilterModuleContainer & componentFilterContainer()
    {
        return m_componentFilter;
    }

    FilterModuleContainer & cellFilterContainer() 
    {
        return m_cellFilter;
    }

    FilterModuleContainer & voxelFilterContainer() 
    {
        return m_voxelFilter;
    }

    virtual void applyLayout() {

        const float H  = size().y();
        const float W  = size().x();
        const float X0 = position().x();
        const float YT = position().y() + size().y();

        m_applyButton.setSize( 100, 30 );
        m_applyButton.setPosition( W - 108, YT - 38 );

        std::vector< FilterModuleContainer * > modulePtrs = {
            &m_bandFilter,
            &m_componentFilter,
            &m_cellFilter,
            &m_voxelFilter
        };

        float y = YT - 10 - 40;
        for( auto & mp : modulePtrs ) {
            mp->applyLayout( X0 + 18, y, W-18 );
            y -= mp->size().y() + 10;
        }
    }

    bool handleInput( 
        const InputState & input )
    {
        TN::Vec2< double > p = input.mouseState.position;
        MouseButtonState mouseButtonState = input.mouseState.buttonState;
        bool changed = false;

        if( m_applyButton.pointInViewPort( p )  ) {                
            if ( input.mouseState.event == MouseEvent::LeftPressed ) {
                m_requestApply = true;
                changed = true;
            }
        }

        // Since a combo may be open and occluding others
        // need to handle the combo events first, and then terminate
        // the event propegation if any combo was open
        // note that this implies the combos events shouldn't be handled internally

        std::vector< TN::ComboWidget * > comb = visibleCombos();
        bool existsPressedCombo = false;
        for( auto & c : comb ) 
        {
            if( c->isPressed() )
            {
                existsPressedCombo = true;
                break;
            }
        }

        if( input.mouseState.event == MouseEvent::LeftPressed ) {
            for( auto & c : comb ) 
            {
                if( c->pointInViewPort( p ) )
                {
                    if( ! existsPressedCombo && ! c->isPressed() )
                    {
                        changed = true;
                        c->setPressed( true );
                        break;
                    }
                    else if( c->isPressed() )
                    {
                        changed = true;
                        int idx = c->pressed( p );
                        c->setPressed( false );
                        break;
                    }
                }
            }
        }
        else
        {
            for( auto & c : comb ) 
            {
                if( c->isPressed() )
                {
                    int id = c->mousedOver( p );
                    if( id == -1 )
                    {
                        c->setPressed( false );
                    }
                    break;
                }
            }
        }

        if( ! existsPressedCombo )
        {
            auto levels = levelPtrs();
            for( auto & l : levels )
            {
                changed |= l->handleInput( input );
            }
        }

        return changed;
    }

	virtual void setSize( float x, float y ) override
	{
		Widget::setSize( x, y );
        applyLayout();
	}

	HierarchicalFilterGroup( const std::string & resourceDir )
		: Widget(),
		  m_resourceDir( resourceDir ),
          m_bandFilter( resourceDir,     "Layer Filters",      Marrus::FilterOp::FILTER_LEVEL_BAND ),
          m_componentFilter( resourceDir, "Component Filters", Marrus::FilterOp::FILTER_LEVEL_COMPONENT ),
          m_cellFilter( resourceDir,      "Cell Filters",      Marrus::FilterOp::FILTER_LEVEL_CELL ),
          m_voxelFilter( resourceDir,     "Element Filter",    Marrus::FilterOp::FILTER_LEVEL_ELEMENT )
	{
		init();
	}

    bool requestApply() const 
    {
        return m_requestApply;
    }

    TN::PressButton * applyButton() {
        return & m_applyButton;
    }

    std::vector< TN::ComboWidget * > visibleCombos()
    {
        std::vector< TN::ComboWidget * > result;
        auto levels = levelPtrs();
        for( auto l : levels )
        {
            auto cmbs = l->visibleCombos();
            for( auto c : cmbs )
            {
                result.push_back( c );
            }
        }
        return result;
    }
    
    virtual ~HierarchicalFilterGroup() {}

};

} // end namespace TN

#endif