#ifndef TN_EXPRESSION_EDIT
#define TN_EXPRESSION_EDIT

#include "views/InputState.hpp"
#include "views/Widget.hpp"
#include "geometry/Vec.hpp"

#include <chrono>
#include <map>
#include <set>
#include <string>

namespace TN
{

class ExpressionEdit : public Widget
{
    bool m_flash;
    double m_flashPhase;

    int m_cursorPos;

    std::string m_text;
    std::string m_label;

    bool m_focused;

    Vec3< float > m_validColor;
    Vec3< float > m_errorColor;
    Vec3< float > m_flashColor;

    bool m_editable;

    void handleKey( const std::string & key )
    {
        if( key == "left" )
        {
            decrementCursorPos();
        }
        else if( key == "right"  )
        {
            incrementCursorPos();
        }
        else if( key == "backspace" )
        {
            if( m_editable ) {
                deleteFromCursor();
            } 
        }
        else if( key == "down" || key == "up" )
        {}
        else 
        {   
            if( m_editable ) {
                for( auto c : key )
                {
                    addFromCursor( c );
                }
            }
        }
    }

public:

    virtual bool handleInput( const InputState & input ) override
    {
        const TN::Vec2< double > & pos = input.mouseState.position;

        if( m_focused )
        {
            const std::set< std::string > & keysPressed = input.keyState.justPressed;
            for( const std::string & key : keysPressed ) {
                handleKey( key );
            }

            const std::map< std::string, std::chrono::time_point< std::chrono::high_resolution_clock > > & keysHeld = input.keyState.held;
            for( const auto & key : keysHeld ) {

                if( keysPressed.count( key.first ) )
                {
                    continue;
                }

                // in milliseconds
                int64_t dt = std::round( input.keyState.timeHeld( key.first ) * 1e-8 ); // tenths of a second 
                if( dt > 5 )
                {
                    handleKey( key.first );
                }
            }
        }

        if( input.mouseState.event == MouseEvent::LeftPressed ) {
            if( pointInViewPort( pos ) )
            {
                setFocused( true );
            }
            else
            {
                setFocused( false );
            }
        }

        return false;
    }

    virtual void resize( float width, float height )
    {
        this->setSize( width, height );
    }

    ExpressionEdit() :
        m_flash( false ),
        m_flashPhase( 0 ),
        m_validColor( 1, .7, .6 ),
        m_errorColor( 1, .7, .6 ),
        m_flashColor( 1, .7, .6 ),
        m_cursorPos( -1 ),
        m_editable( true ),
        m_focused( false )
    {}

    void setFocused( bool c )
    {
        m_focused = c;
    }

    std::string text() const
    {
        return m_text;
    }

    void clear()
    {
        m_text = "";
    }

    void append( char c )
    {
        m_text.push_back( c );
        incrementCursorPos();
    }

    void deleteFromCursor()
    {
        if( m_text.size() > 0 && m_cursorPos >= 0 )
        {
            m_text.erase( m_text.begin() + m_cursorPos );
            decrementCursorPos();
        }
    }

    void addFromCursor( char c )
    {
        if( m_cursorPos >= int( m_text.size() ) - 1 )
        {
            append( c );
        }
        else
        {
            m_text.insert( m_text.begin() + m_cursorPos + 1, c );
            incrementCursorPos();
        }
    }

    void pop()
    {
        if( m_text.size() > 0 )
        {
            m_text.pop_back();
            decrementCursorPos();
        }
    }

    void setText( const std::string & text ) {
         m_text = text;
    } 

    void setEditable( bool c ) {
        m_editable = c;
    }

    bool hasFocus() const { return m_focused; }

    const Vec3< float > & flashColor() const
    {
        return m_flashColor;
    }
    const Vec3< float > & errorColor() const
    {
        return m_errorColor;
    }
    const Vec3< float > & validColor() const
    {
        return m_validColor;
    }

    void incrementCursorPos()
    {
        m_cursorPos = std::min( int( m_text.size() - 1 ), m_cursorPos + 1 );
    }

    int labelX() const
    {
        return position().x() - m_label.size()*8 - 4;
    }

    int labelY() const
    {
        return this->position().y() + 10;
    }

    void label( const std::string & txt )
    {
        m_label = txt;
    }

    const std::string & label() const { return m_label; } 

    void decrementCursorPos()
    {
        m_cursorPos = std::max( int( m_cursorPos - 1 ), -1 );
    }

    int cursorPos() const { return m_cursorPos; }

    bool flash() const { return m_flash; }
    void flash( bool b ) { m_flash = b; }

    double flashPhase() const { return m_flashPhase; }
    void flashPhase( double phase ) { m_flashPhase = phase; }
};

}


#endif // TN_EXPRESSION_EDIT

