#ifndef TN_EXPORT_PANEL_HPP
#define TN_EXPORT_PANEL_HPP

namespace TN {
	
class ExportPanel : public ConfigurationPanel
{

	public: 

	virtual void applyLayout() override {

	}

	virtual bool handleInput( const InputState & input ) override
	{
		bool changed = false;

		return changed;
	}

	virtual void clear() override {

	}

	ExportPanel() : ConfigurationPanel() {}

	virtual ~ExportPanel() {}
};

} // end namespace TN

#endif 