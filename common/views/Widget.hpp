#ifndef WIDGET_HPP
#define WIDGET_HPP

#include "views/Viewport/ViewPort.hpp"
#include "views/InputState.hpp"

namespace TN
{

struct Label
{
    std::string text;
    Vec2< float > position;
    bool bold;
    Label() : bold( false )
    {}
};

class Widget
{
    Widget * m_root;

    Vec2< float > m_maxPosition;
    Vec2< float > m_minSize;

protected :

    ViewPort m_viewPort;

    bool active;
    float xScroll;
    float yScroll;

    double height() {
        return m_viewPort.height();
    }

    double width() {
        return m_viewPort.width();
    }

public :

    Widget() : m_root( 0 ), m_viewPort(), active( true ), xScroll( 0.f ), yScroll( 0.f ) {
        setSize( 10, 10 );
        minSize( 2,   2 );
    }

    virtual bool handleInput( const InputState & input ) 
    {
        return false;
    }

    bool isActive()
    {
        return active;
    }

    virtual void activate( bool a )
    {
        active = a;
    }

    Widget *parent() const
    {
        return m_root;
    }

    void parent( Widget * p )
    {
        m_root = p;
    }

    void scroll( TN::Vec2< float > dxy ) {
        xScroll += dxy.x();
        yScroll += dxy.y();        
    }   

    //! in screen space ( pixels )
    virtual void setSize( float width, float height )
    {
        m_viewPort.setSize( 
            std::max( width,  m_minSize.x() ), 
            std::max( height, m_minSize.y() ) );
    }

    virtual void applyLayout() {}

    //! in screen space ( pixels )
    virtual void setPosition( float x, float y )
    {
        m_viewPort.offsetX( x );
        m_viewPort.offsetY( y );
    }

    Vec2< float > size() const
    {
        return Vec2< float >( m_viewPort.width(), m_viewPort.height() );
    }

    // screen space, pixels
    Vec2< float > position() const
    {
        if( m_root == 0 )
        {
            return Vec2< float >( m_viewPort.offsetX(), m_viewPort.offsetY() );
        }
        return Vec2< float >( m_viewPort.offsetX(), m_viewPort.offsetY() ) + m_root->position();
    }

    const BoxShadowFrame & boxShadow()
    {
        return m_viewPort.boxShadow();
    }

    virtual bool pointInViewPort( const Vec2< float > & p )
    {
        if( active )
        {
            return m_viewPort.pointInViewPort( p );
        }
        else
        {
            return false;
        }
    }

    const ViewPort & viewPort() const
    {
        return m_viewPort;
    }

    void minSize( float x, float y )
    {
        m_minSize = Vec2< float >( x, y );
    }

    void maxPosition( float x, float y )
    {
        m_maxPosition = Vec2< float >( x, y );
    }

    const Vec2< float > & maxPosition() const
    {
        return m_maxPosition;
    }
    const Vec2< float > & minSize() const
    {
        return m_minSize;
    }


    TN::Vec2< float > getScroll() const
    {
        return { xScroll, yScroll };
    }


    void bkgColor( const Vec4 & cl )
    {
        m_viewPort.bkgColor( cl );
    }

    virtual ~Widget() { }
};

}

#endif // WIDGET_HPP

