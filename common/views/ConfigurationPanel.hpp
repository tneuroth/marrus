#ifndef TN_CONFIGURATION_PANEL_HPP
#define TN_CONFIGURATION_PANEL_HPP

#include "views/Widget.hpp"
#include "views/ComboWidget.hpp"
#include "views/PressButtonWidget.hpp"
#include "views/ExpressionEdit.hpp"
#include "views/InputState.hpp"
#include "views/SliderWidget.hpp"
#include "geometry/Vec.hpp"

#include <vector>
#include <iostream>
#include <cmath>

namespace TN {

class ConfigurationPanel : public Widget {

	std::string m_title;

	public : 
	
	void setTitle( const std::string & title ) 
	{
		m_title = title;
	}

	virtual void clear() = 0;

	virtual bool handleInput( const InputState & input ) override = 0;
	virtual void applyLayout() = 0;
	
	virtual std::vector< TN::PressButton    * > buttons()         { return {}; }
	virtual std::vector< TN::ComboWidget    * > combos()          { return {}; }
	virtual std::vector< TN::Label          * > labels()          { return {}; }
	virtual std::vector< TN::SliderWidget   * > sliders()         { return {}; }
	virtual std::vector< TN::ExpressionEdit * > expressionEdits() { return {}; }
	virtual bool isTransparent() { return true; }

	const std::string & title() const {
		return m_title;
	}

	ConfigurationPanel() : Widget(), m_title( "" ) {}
	virtual ~ConfigurationPanel() {}
};

} // end namespace TN

#endif 