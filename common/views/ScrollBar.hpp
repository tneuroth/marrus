#ifndef TN_SCROLL_HPP
#define TN_SCROLL_HPP

#include "OpenGL/Widgets/Widget.hpp"
#include "Types/Vec.hpp"

namespace TN
{

struct ScrollBar : public Widget
{
    enum {
        HORIZONTEL,
        VERTICAL
    };

    double scrollOffset;
    int direction;
    bool handleIsPressed;
    double areaLength;
    double jumpLength;

    Vec2< float > lastP;

    void setAreaLength( double l )
    {
        if( direction == HORIZONTEL )
        {
            areaLength = std::max( width(), l );
        }
        else
        {
            areaLength = std::max( height(), l );
        }
    }

    void setJumpLength( float l )
    {
        jumpLength = l;
    }

    float barLength()
    {
        if( direction == HORIZONTEL )
        {
            return size().x() - height() * 2;
        }
        else
        {
            return size().y() - width() * 2;
        }
    }

    Vec2< float > handlePosition()
    {
        if( direction == HORIZONTEL )
        {
            return Vec2< float >( position().x() + scrollOffset + height(), position().y() );
        }
        else
        {
            return Vec2< float >( position().x(), position().y() + scrollOffset + width() );
        }
    }

    Vec2< float > handleSize()
    {
        float lng = barLength();

        if( direction == HORIZONTEL )
        {
            return Vec2< float >( ( size().x() / areaLength ) * lng, size().y()-1 );
        }
        else
        {
            return Vec2< float >( size().x()-1, ( size().y() / areaLength ) * lng );
        }
    }

    bool pointOnHigherArrow( const Vec2< float > & p )
    {
        if( ! pointInViewPort( p ) )
        {
            return false;
        }

        if( direction == HORIZONTEL )
        {
            if( p.x() > ( position().x() + size().x() - height() ) )
            {
                return true;
            }
        }
        else
        {
            if( p.y() > ( position().y() + size().y() - width() ) )
            {
                return true;
            }
        }
    }

    bool pointOnLowerArrow( const Vec2< float > & p )
    {
        if( ! pointInViewPort( p ) )
        {
            return false;
        }

        if( direction == HORIZONTEL )
        {
            if( p.x() < position().x() + height() )
            {
                return true;
            }
        }
        else
        {
            if( p.y() < position().y() + width() )
            {
                return true;
            }
        }
    }

    bool pointOnScrollHandle( const Vec2< float > & p )
    {
        if( ! pointInViewPort( p ) )
        {
            return false;
        }

        if( direction == HORIZONTEL )
        {
            float handleLength = handleSize().x();
            if( p.x() >= ( position().x() + scrollOffset + height() ) && p.x() <= ( position().x() + height() + scrollOffset + handleLength ) )
            {
                return true;
            }
        }
        else
        {
            float handleLength = handleSize().y();
            if( p.y() >= ( position().y() + width() + scrollOffset ) && p.y() <= ( position().y() + width() + scrollOffset + handleLength ) )
            {
                return true;
            }
        }
    }

    void mousePress( const Vec2< float > & p )
    {
        if( pointOnScrollHandle( p ) )
        {
            qDebug() << "pressed";
            handleIsPressed = true;
            lastP = p;
        }

        bool jump = false;
        int sign = 1;

        if( pointOnLowerArrow( p ) )
        {
            sign = -1;
            jump = true;
        }
        if( pointOnHigherArrow( p ) )
        {
            sign = 1;
            jump = true;
        }

        if( jump )
        {
            if( direction == HORIZONTEL )
            {
                double jump = ( jumpLength * barLength() ) / areaLength;
                float handleLength = handleSize().x();

                scrollOffset += sign * jump;

                if( sign > 0 )
                {
                    int cols = std::floor( scrollOffset / jump );
                    scrollOffset -= scrollOffset - cols * jump;
                }
                else
                {
                    int cols = std::ceil( scrollOffset / jump );
                    scrollOffset -= scrollOffset - cols * jump;
                }


                scrollOffset = std::max( 0.0, scrollOffset );
                scrollOffset = std::min( double( barLength() - handleLength ), scrollOffset );
            }
            else
            {
                float handleLength = handleSize().y();
                scrollOffset += sign * jumpLength;
                scrollOffset = std::max( 0.0, scrollOffset );
                scrollOffset = std::min( double( barLength() - handleLength ), scrollOffset );
            }
        }
    }

    bool update( const Vec2< float > & p )
    {
        if( handleIsPressed )
        {
            Vec2< float > deltaP = p - lastP;
            lastP = p;

            if( direction == HORIZONTEL )
            {

                float handleLength = handleSize().x();
                scrollOffset += deltaP.x();
                scrollOffset = std::max( 0.0, scrollOffset );
                scrollOffset = std::min( double( barLength() - handleLength ), scrollOffset );
            }
            else
            {

                float handleLength = handleSize().y();
                scrollOffset += deltaP.y();
                scrollOffset = std::max( 0.0, scrollOffset );
                scrollOffset = std::min( double( barLength() - handleLength ), scrollOffset );
            }

            return true;
        }
        return false;
    }

    void release()
    {
        handleIsPressed = false;
    }

    ScrollBar( int _direction ) : direction( _direction ), scrollOffset( 0 ), areaLength( 1 ), handleIsPressed( false )
    {}

    float getAreaOffset()
    {
        return ( scrollOffset / barLength() ) * areaLength;
    }
};

}

#endif // TN_SCROLL_HPP

