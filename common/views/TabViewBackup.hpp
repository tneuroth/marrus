#ifndef TN_TOOL_PANEL_HPP
#define TN_TOOL_PANEL_HPP

#include "views/Widget.hpp"
#include "views/PressButtonWidget.hpp"
#include "geometry/Vec.hpp"
#include "views/InputState.hpp"

#include <string>
#include <vector>
#include <iostream>
#include <map>

namespace TN {

struct PanelElement 
{
    TexturedPressButton tabButton;
    Widget * view;

    PanelElement(     	
    	const std::string & btnTex,
    	const std::string & btnTexPressed,
    	Widget * v ) : view( v )
    {
        tabButton.setPressed( false );
        tabButton.setTexFromPNG( btnTex, btnTexPressed );
    }

    bool isOpen() const
    {
    	return tabButton.isPressed();
    }
};

enum class TabViewOrientation
{
    OpenRight,
    OpenLeft,
    OpenUp,
    OpenDown
};

class TabView : public Widget
{
    std::map< std::string, PanelElement > m_panelElements;
    std::vector< std::string > m_elementKeyOrder;
    int m_activePanel;
    TabViewOrientation m_orientation;
    TN::TexturedPressButton m_collapseButton;

    public:

    TabView() : m_activePanel( -1 ), m_orientation( TabViewOrientation::OpenRight )
    {}

    TabView( TabViewOrientation orientation ) : m_activePanel( -1 ), m_orientation( orientation )
    {}

    void setOrientation( TabViewOrientation o ) { m_orientation = o; }

    void applyLayout()
    {
    	const double W = this->size().x();
    	const double H = this->size().y();

    	const double X = this->position().x();
    	const double Y = this->position().y();

    	const double PAD        = 2;
    	const double SPACING    = 0;
    	const double BTN_OFFSET = 100;
        const double VIEW_SZ    = 320;

        const double BTN_SZ = m_orientation == TabViewOrientation::OpenLeft ||
                              m_orientation == TabViewOrientation::OpenRight 
                              ? W - 2*PAD 
                              : H - 2*PAD;

        m_collapseButton.setSize( BTN_SZ, BTN_SZ );

        Vec2< float > collapsePos = 
            m_orientation == TabViewOrientation::OpenRight || m_orientation == TabViewOrientation::OpenLeft ?
            
                Vec2< float >( X + PAD, Y + H - BTN_OFFSET -  BTN_SZ )
          :
                Vec2< float >( X + BTN_OFFSET, Y + PAD );

        m_collapseButton.setPosition(  );

        for( size_t i = 0, end = m_elementKeyOrder.size(); i < end; ++i )
        {
        	auto & p = m_panelElements.at( m_elementKeyOrder[ i ] );
        	p.tabButton.setSize( BTN_SZ, BTN_SZ );
            
            Vec2< float > pos = 
                m_orientation == TabViewOrientation::OpenRight || m_orientation == TabViewOrientation::OpenLeft ?
                
                    Vec2< float >( X + PAD, Y + H - BTN_OFFSET -  i * ( BTN_SZ + SPACING ) - BTN_SZ )
              :
                    Vec2< float >( X + BTN_OFFSET + i * ( BTN_SZ + SPACING ), Y + PAD );

            p.tabButton.setPosition( pos.x(), pos.y() );

            Vec2< float > vSize = 
                m_orientation == TabViewOrientation::OpenRight || m_orientation == TabViewOrientation::OpenLeft ?
                
                    Vec2< float >( VIEW_SZ, H )
              :
                    Vec2< float >( W, VIEW_SZ ); 

            p.view->setSize( vSize.x(), vSize.y() ); 

            Vec2< float > vPos = 
                m_orientation == TabViewOrientation::OpenLeft  ? 
                    Vec2< float >( X - VIEW_SZ, Y )
              : m_orientation == TabViewOrientation::OpenRight ? 
                    Vec2< float >( X , Y )
              : m_orientation == TabViewOrientation::OpenUp    ? 
                    Vec2< float >( X, Y )
              :
                    Vec2< float >( X, Y - VIEW_SZ );                    

            p.view->setPosition( vPos.x(), vPos.y() ); 
        }
    }

    bool handleInput( 
        const InputState & input )
    {
    	Vec2< double > p = input.mouseState.position;

        // if( ! this->pointInViewPort( p ) )
        // {
        // 	return false;
        // }

        bool changed = false;

        if( input.mouseState.event == MouseEvent::LeftPressed )
        {
	    	int newlyPressedTab = -1;
	    	for( size_t i = 0, end = m_elementKeyOrder.size(); i < end; ++i )
	    	{
	            auto & e = m_panelElements.at( m_elementKeyOrder[ i ] );
	            if( e.tabButton.pointInViewPort( p ) )
	            {
	            	e.tabButton.setPressed( ! e.tabButton.isPressed() );
	            	newlyPressedTab = i;
	            	
                    changed = true;

	            	break;
	            }
	    	}

	        if( newlyPressedTab != -1 )
	        {
		    	for( size_t i = 0, end = m_elementKeyOrder.size(); i < end; ++i )
		    	{
		            auto e = m_panelElements.at( m_elementKeyOrder[ i ] );

		            if( i == newlyPressedTab )
		            {
		            	if( i == m_activePanel )
		            	{
	                        e.tabButton.setPressed( false );
	                        m_activePanel = -1;
		            	}
		            	else
		            	{
		            		e.tabButton.setPressed( true );
		            		m_activePanel = newlyPressedTab;
		            	}
		            }
		            else
		            {
	                    e.tabButton.setPressed( false );
		            }
		    	}
	        }
        }
        return changed;
    }

    void addElement( 
    	const std::string & key,
    	const std::string & btnTex,
    	const std::string & btnTexPressed,
    	Widget * view )
    {
    	if( m_panelElements.count( key ) )
    	{
    		std::cerr << "Error: panel element with key: " << key << " already exists" << std::endl;
    		exit( 1 );
    	}

        m_elementKeyOrder.push_back( key );
        m_panelElements.insert( { key, PanelElement( btnTex, btnTexPressed, view ) } );
    }

    virtual void resize( float width, float height )
    {
        this->setSize( width, height );
    }

    Widget * elementView( const std::string & key )
    {
    	if( m_panelElements.count( key ) == 0 )
    	{
    		std::cerr << "error no panel with key: " << key << std::endl;
    		exit( 1 );
    	}
        return m_panelElements.at( key ).view;
    }

    std::vector< PanelElement * > elements()
    {
        std::vector< PanelElement * > e( m_elementKeyOrder.size() );
        for( size_t i = 0; i < m_elementKeyOrder.size(); ++i )
        {
            e[ i ] = & m_panelElements.at( m_elementKeyOrder[ i ] );
        }
        return e;
    }

    bool active( const std::string & key ) const 
    {
    	if( ! m_panelElements.count( key ) )
    	{
            return false; // maybe should throw error
    	}

    	else return m_panelElements.at( key ).isOpen();
    }

    void setActive( const std::string & key )
    {
    	if( m_panelElements.count( key ) == 0 )
    	{
            m_activePanel = -1;
    	}

    	for( size_t i = 0, end = m_elementKeyOrder.size(); i < end; ++i )
    	{
    		auto & e = m_panelElements.at( m_elementKeyOrder[ i ] );
    		if( m_elementKeyOrder[ i ] == key )
    		{
    			m_activePanel = i;
    			e.tabButton.setPressed( true );
    		}
    		else
    		{
    			e.tabButton.setPressed( false );
    		}
    	}
    }

    ~TabView()
    {
    	clean();
    }
};

}

#endif