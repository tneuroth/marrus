
#include "SpatialView3D.hpp"

#include <string>
#include <atomic>

namespace TN {

std::atomic< int > SpatialView3D::nextId( 0 );

SpatialView3D::SpatialView3D( const SpatialViewConfiguration & config ) 
    : TN::Widget(), 
      m_minimized(   false ), 
      m_needsUpdate( true )
{
	m_uniqueId = "spatial_view_" + std::to_string( nextId );
	++SpatialView3D::nextId;

    configure( config );
    initialize(); 
}

SpatialView3D::SpatialView3D() : 
    TN::Widget(), 
    m_minimized(   false ), 
    m_needsUpdate( true ) 
{
	m_uniqueId = "spatial_view_" + std::to_string( nextId );
	++SpatialView3D::nextId;

    initialize(); 
}

}