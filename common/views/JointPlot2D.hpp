#ifndef TN_JOINT_PLOT_2D
#define TN_JOINT_PLOT_2D

#include "geometry/Vec.hpp"
#include "views/Widget.hpp"
#include "views/MarginalView.hpp"
#include "views/InputState.hpp"
#include "views/PressButtonWidget.hpp"
#include "model/Hist2DParams.hpp"
#include "configuration/JointPlotConfiguration.hpp"
#include "algorithms/Filter/BasicFilter.hpp"
#include "filter/FilterOps.hpp"
#include "render/Texture/Texture.hpp"

#include "statistical/kde.hpp"

#include <vector>
#include <iostream>

inline void constructLineIndicators(
    const TN::Vec2< float > & linePos,
    const TN::Vec2< float > & marginalPos,
    const TN::Vec2< float > & marginalSize,
    const TN::Vec2< float > & jointPos,
    const TN::Vec2< float > & jointSize,
    const TN::Vec2< float > & windowSize,    
    std::vector< TN::Vec2< float > > & lines,
    std::vector< TN::Vec2< float > > & triangles,
    const std::string & axis )
{
    const int OFFSET   = 8;
    const float T_SIZE = 8;

    if( axis == "x" )
    {
        lines =
            std::vector< TN::Vec2< float > > (
        {
            {
                linePos.a(),
                ( marginalPos.y() + marginalSize.y() + OFFSET )
            },
            {
                marginalPos.x(),
                ( marginalPos.y() + marginalSize.y() + OFFSET )
            },

            {
                linePos.b(),
                ( marginalPos.y() + marginalSize.y() + OFFSET )
            },
            {
                ( marginalPos.x() + marginalSize.x() ),
                ( marginalPos.y() + marginalSize.y() + OFFSET )
            },

            {
                marginalPos.x(),
                ( marginalPos.y() + marginalSize.y() + OFFSET )
            },
            {
                marginalPos.x(),
                ( jointPos.y() )
            },

            {
                ( marginalPos.x() + marginalSize.x() ),
                ( marginalPos.y() + marginalSize.y() + OFFSET )
            },
            {
                ( marginalPos.x() + marginalSize.x() ) ,
                ( jointPos.y() )
            }
        } );

        triangles =
            std::vector< TN::Vec2< float > > (
        {
            {
                linePos.a(),
                ( marginalPos.y() + marginalSize.y() - 2 )
            },

            {
                linePos.a() - T_SIZE / 2,
                ( marginalPos.y() + marginalSize.y() + T_SIZE - 2 )
            },

            {
                linePos.a() + T_SIZE / 2,
                ( marginalPos.y() + marginalSize.y() + T_SIZE - 2 )
            },

            {
                linePos.b(),
                ( marginalPos.y() + marginalSize.y() - 2 )
            },

            {
                linePos.b() - T_SIZE / 2,
                ( marginalPos.y() + marginalSize.y() + T_SIZE - 2 )
            },

            {
                linePos.b() + T_SIZE / 2,
                ( marginalPos.y() + marginalSize.y() + T_SIZE - 2 )
            },
        } );

    }

    else if( axis == "y" )
    {

        lines =
            std::vector< TN::Vec2< float > > (
        {
            {
                ( marginalPos.x() + marginalSize.x() ),
                linePos.a(),
            },
            {
                ( marginalPos.x() + marginalSize.x() + OFFSET ),
                linePos.a()
            },

            {
                ( marginalPos.x() + marginalSize.x() ),
                linePos.b()
            },
            {
                ( marginalPos.x() + marginalSize.x() + OFFSET ),
                linePos.b(),
            },

            {
                ( marginalPos.x() + marginalSize.x() + OFFSET ),
                linePos.a(),
            },
            {
                ( marginalPos.x() + marginalSize.x() + OFFSET ),
                marginalPos.y()
            },

            {
                ( marginalPos.x() + marginalSize.x() + OFFSET ),
                linePos.b(),
            },
            {
                ( marginalPos.x() + marginalSize.x() + OFFSET ),
                ( marginalPos.y() + marginalSize.y() )
            },

            {
                ( marginalPos.x() + marginalSize.x() + OFFSET ),                
                marginalPos.y()
            },
            {
                ( jointPos.x() ),                
                marginalPos.y()
            },

            {
                ( marginalPos.x() + marginalSize.x() + OFFSET ),
                ( marginalPos.y() + marginalSize.y() )
            },
            {
                ( jointPos.x() ),
                ( marginalPos.y() + marginalSize.y() )
            }
        } );

        triangles =
            std::vector< TN::Vec2< float > > (
        {
            {
                ( marginalPos.x() + marginalSize.x() - 2 ),
                linePos.a()
            },

            {
                ( marginalPos.x() + marginalSize.x() + T_SIZE - 2 ),
                linePos.a() - T_SIZE / 2            
            },

            {
                ( marginalPos.x() + marginalSize.x() + T_SIZE - 2 ),
                linePos.a() + T_SIZE / 2            
            },

            {
                ( marginalPos.x() + marginalSize.x() - 2 ),
                linePos.b()
            },

            {
                ( marginalPos.x() + marginalSize.x() + T_SIZE - 2 ),
                linePos.b() - T_SIZE / 2
            },

            {
                ( marginalPos.x() + marginalSize.x() + T_SIZE - 2 ),
                linePos.b() + T_SIZE / 2
            }
        } );

    }

    for( auto & p : lines )
    {
        p.x( p.x() * 2.f / windowSize.x() - 1 );
        p.y( p.y() * 2.f / windowSize.y() - 1 );
    }

    for( auto & p : triangles )
    {
        p.x( p.x() * 2.f / windowSize.x() - 1 );
        p.y( p.y() * 2.f / windowSize.y() - 1 );
    }
}

namespace TN
{

enum class ProbeType {
    Box,
    Lasso
};

struct MouseState
{
    bool on;
    Vec2< float > pos0;
    bool leftButton;
    bool rightButton;
    bool wheelButton;
    Vec2< float > pos;

    MouseState()   
    {}
};

struct JointPlotStateInfo 
{
    MouseState mouseState;
    Vec2< float > value;
};

struct ConditionalCurves 
{
    std::vector< float > meanCurve;
    std::vector< float > standardDeviationCurve;
    std::vector<   int > counts;
    std::vector< float > binCenters;    
};

class JointPlot2D : public Widget
{
    Widget m_container;

    /////////////////////////////////////

    TN::JointPlotConfiguration m_configuration;
    TN::JointPlotUpdatesNeeded m_updatesNeeded;

    /////////////////////////////////////

    std::string m_workingDirectory;
    std::string m_projectDirectory;
    std::string m_type;
    std::string m_style;
    bool m_undefined;

    ConditionalCurves m_conditionalCurves;

    RangeSelector m_xMarginalView;
    RangeSelector m_yMarginalView;

    Vec2< float > m_xRange;
    Vec2< float > m_yRange;
    
    Vec2< float > m_xSubRange;
    Vec2< float > m_ySubRange;

    Vec2< float > m_xBoxRange;
    Vec2< float > m_yBoxRange;

    Vec2< float > m_xBoxValueRange;
    Vec2< float > m_yBoxValueRange;

    std::vector< TN::Vec2< float > > m_lassoPolygone;
    std::vector< TN::Vec2< float > > m_lassoScreenSpace;

    TN::Vec2< float > m_lassoBBX;
    TN::Vec2< float > m_lassoBBY;    

    ProbeType m_probeType;

    std::string m_xLabel;
    std::string m_yLabel;
    std::string m_wLabel;

    std::string m_title;

    std::string m_xKey;
    std::string m_yKey;
    std::string m_wKey;

    float txBinnedMin;
    float txBinnedMax;

    bool m_leftMouseReleased;
    bool m_rightMouseReleased;
    bool m_mouseClicked;

    JointPlotStateInfo m_stateInfo;

    bool m_hovered;
    TN::Vec2< float > m_hoveredPosition;

    bool m_probeOn;

    std::vector< float > m_pdfContextData;
    std::vector< float > m_pdfData;    
    std::vector< float > m_cdfData;    
    std::vector< float > m_xPDFData;
    std::vector< float > m_yPDFData;  
    std::vector< float > m_wXAverage;
    std::vector< float > m_wYAverage;
    std::vector< float > m_wXYAverage;

    TN::Vec2<      int > m_texDims;

    float m_fullCount;
    float m_contextCount;  
    float m_subCount;    
    float m_rCount;    
    float m_gCount; 

    TN::TextureLayer m_xSubMarginalTexture;
    TN::TextureLayer m_ySubMarginalTexture;
    TN::TextureLayer m_texture;
    TN::TextureLayer m_contextTexture;
    TN::TextureLayer m_cdfTexture;
    TN::TextureLayer m_gmmTexture;
    TN::TextureLayer m_kdeTexture;    

    TN::Texture2D m_scatterPlotTexture;
    TN::Texture2D m_lassoPointsTexture;;

    int m_hoveredBin;

    template < class TP >
    bool mouseOver( const Vec2< TP > & p )
    {
        return p.x() > position().x() && p.x() < position().x() + size().x() 
            && p.y() > position().y() && p.y() < position().y() + size().y();
    }

    Vec2< double > valueAt( const Vec2< double > & p )
    {
        float rX  = p.x();
        float rY  = p.y();

        return 
        { 
            m_xSubRange.a() + ( m_xSubRange.b() - m_xSubRange.a() ) * rX,
            m_ySubRange.a() + ( m_ySubRange.b() - m_ySubRange.a() ) * rY
        };
    }

    std::vector< TN::PressButton > m_modeButtons;
    int m_currentMode;

    TN::TexturedPressButton m_deleteButton;
    TN::TexturedPressButton m_optimizeButton;
    TN::TexturedPressButton m_infoButton;
    TN::TexturedPressButton m_exportButton;

    TN::TexturedPressButton m_highLightButton;
    TN::TexturedPressButton m_conditionalButton;
    TN::TexturedPressButton m_subMarginalButton;

    TN::TexturedPressButton m_xLabelButton;
    TN::TexturedPressButton m_yLabelButton;

    bool m_modeChanged;
    bool m_layersChanged;

    bool m_deleteMe;
    bool m_exportMe;

    double m_meanX;
    double m_varX;
    double m_skewX;
    double m_kurtX;

    double m_meanY;
    double m_varY;
    double m_skewY;
    double m_kurtY;

    double m_covariance;
    double m_coskewness;
    double m_cokurtosis;

    double m_meanXH;
    double m_varXH;
    double m_skewXH;
    double m_kurtXH;

    double m_meanYH;
    double m_varYH;
    double m_skewYH;
    double m_kurtYH;

    double m_covarianceH;
    double m_coskewnessH;
    double m_cokurtosisH;

    double m_meanW;

    int m_contextBitMask;
    int m_focusBitMask;    

bool updateButtonState( const InputState & inputState )
    {
        bool changed = false;

        for( size_t i = 0; i < m_modeButtons.size(); ++i )
        {
            auto & button = m_modeButtons[ i ];

            if( button.pointInViewPort( inputState.mouseState.eventPosition ) )
            {
                if( inputState.mouseState.event == MouseEvent::LeftPressed )
                {
                    if( ! button.isPressed() )
                    {
                        changed = true;
                    }
                    button.setPressed( true );
                    m_currentMode = i;
                    m_modeChanged = true;

                    if(      i == 0 ) { type( "scatter" ); }
                    else if( i == 1 ) { type( "hist2d"  ); }
                    else if( i == 2 ) { type( "cdf2d"   ); }
                    else if( i == 3 ) { type( "kde"     ); }
                    // else if( i == 4 ) { type( "gmm"     ); }

                    for( int j = 0; j < m_modeButtons.size(); ++j )
                    {
                        if( i != j )
                        {
                            m_modeButtons[ j ].setPressed( false );
                        }
                    }
                    break;
                }
            }
        }

        if( m_deleteButton.pointInViewPort( inputState.mouseState.eventPosition ) 
         && inputState.mouseState.event == MouseEvent::LeftPressed )
        {
            m_deleteMe = true;
        }

        if( m_infoButton.pointInViewPort( inputState.mouseState.eventPosition ) 
         && inputState.mouseState.event == MouseEvent::LeftPressed )
        {
            m_infoButton.setPressed( ! m_infoButton.isPressed() );
        }

        if( m_exportButton.pointInViewPort( inputState.mouseState.eventPosition ) 
         && inputState.mouseState.event == MouseEvent::LeftPressed )
        {
            m_exportMe = true;
        }

        if( m_highLightButton.pointInViewPort( inputState.mouseState.eventPosition ) 
         && inputState.mouseState.event == MouseEvent::LeftPressed )
        {
            m_highLightButton.setPressed( ! m_highLightButton.isPressed() );
        }

        if( m_conditionalButton.pointInViewPort( inputState.mouseState.eventPosition ) 
         && inputState.mouseState.event == MouseEvent::LeftPressed )
        {
            if( ! m_conditionalButton.isPressed() )
            {
                changed = true;
            }

            m_conditionalButton.setPressed( ! m_conditionalButton.isPressed() );
        }

        if( m_subMarginalButton.pointInViewPort( inputState.mouseState.eventPosition ) 
         && inputState.mouseState.event == MouseEvent::LeftPressed )
        {
            m_subMarginalButton.setPressed( ! m_subMarginalButton.isPressed() );
            this->setSize( this->size().x(), this->size().y() );   
            this->setPosition( this->position().x(), this->position().y()  );         
        }

        return changed;
    }

public:

    virtual ~JointPlot2D() 
    {}

    double covariance() { return m_covariance; }
    double coskewness() { return m_coskewness; }
    double cokurtosis() { return m_cokurtosis; }

    double meanX()     { return m_meanX; }
    double varianceX() { return m_varX;  }
    double skewnessX() { return m_skewX; }
    double kurtosisX() { return m_kurtX;  }            

    double meanY()     { return m_meanY; }
    double varianceY() { return m_varY;  }
    double skewnessY() { return m_skewY; }
    double kurtosisY() { return m_kurtY;  }  

    double meanXH()     { return m_meanXH; }
    double varianceXH() { return m_varXH;  }
    double skewnessXH() { return m_skewXH; }
    double kurtosisXH() { return m_kurtXH;  }            

    double meanYH()     { return m_meanYH; }
    double varianceYH() { return m_varYH;  }
    double skewnessYH() { return m_skewYH; }
    double kurtosisYH() { return m_kurtYH;  }         

    double covarianceH() { return m_covarianceH; }
    double coskewnessH() { return m_coskewnessH; }
    double cokurtosisH() { return m_cokurtosisH; }        

    double meanXE()     { return std::abs( m_meanXH - m_meanX ); }
    double varianceXE() { return std::abs(  m_varXH -  m_varX ); }
    double skewnessXE() { return std::abs( m_skewXH - m_skewX ); }
    double kurtosisXE() { return std::abs( m_kurtXH - m_kurtX ); }            

    double meanYE()     { return std::abs( m_meanYH - m_meanY ); }
    double varianceYE() { return std::abs(  m_varYH -  m_varY ); }
    double skewnessYE() { return std::abs( m_skewYH - m_skewY ); }
    double kurtosisYE() { return std::abs( m_kurtYH - m_kurtY ); }         

    double covarianceE() { return std::abs( m_covarianceH - m_covariance ); }
    double coskewnessE() { return std::abs( m_coskewnessH - m_coskewness ); }
    double cokurtosisE() { return std::abs( m_cokurtosisH - m_cokurtosis ); }        

    TN::Vec2< int > binDims()
    {
        if( m_texDims.a() > 0 && m_texDims.b() > 0 )
        {
            return m_texDims;
        }
        else
        {
            return { 10, 10 };
        }
    }

    bool showSubMarginals() const
    {
        return m_subMarginalButton.isPressed();
    }

    const ConditionalCurves & conditionalCurves()
    {
        return  m_conditionalCurves;
    }

    bool leftMouseReleaseEvent() { 
        auto tmp = m_leftMouseReleased; 
        m_leftMouseReleased = false; 
        return tmp; 
    }

    bool rightMouseReleaseEvent() { 
        auto tmp = m_rightMouseReleased; 
        m_rightMouseReleased = false; 
        return tmp; 
    }

    bool mouseClickedEvent() 
    { 
        auto tmp = m_mouseClicked; 
        m_mouseClicked = false; 
        return tmp; 
    }

    void showSubMarginals( bool b )
    {
        m_subMarginalButton.setPressed( b );
    }

    void showConditionalCurves( bool b )
    {
        m_conditionalButton.setPressed( b );
    }

    bool showMoments() const
    {
        return m_infoButton.isPressed();
    }

    bool showConditionalCurves() const
    {
        return m_conditionalButton.isPressed();
    }

    bool showSelectedPoints() const
    {
        return m_highLightButton.isPressed();
    }

    void setMode( int m )
    {
        if( m >= m_modeButtons.size() )
        {
            std::cerr << "Error: invalid mode button on JointPlot2D::setMode" << std::endl;
            exit( 1 );
        }
        m_currentMode = m;
        m_modeButtons[ m ].setPressed( true );
        for( size_t i = 0; i < m_modeButtons.size(); ++i )
        {
            if( i != m )
            {
                m_modeButtons[ i ].setPressed( false );
            }
        }
        if(      m == 0 ) { type( "scatter" ); }
        else if( m == 1 ) { type( "hist2d"  ); }
        else if( m == 2 ) { type( "cdf2d"   ); }
        else if( m == 3 ) { type( "kde"     ); }
        else if( m == 4 ) { type( "gmm"     ); }
    }

    TN::Vec2< float > hoveredPosition() const
    {
        return m_hoveredPosition;
    }

    int mode() const
    {
        return m_currentMode;
    }

    TN::JointPlotConfiguration configuration() const {
        return m_configuration;
    }

    void initialize(
        const TN::JointPlotConfiguration & c )
    {
        setXY( c.x, c.y );
        setW( c.w == "None" ? "" : c.w );

        xMarginal().setTF( m_workingDirectory + "/../TF/sequential/Reds.txt" );
        yMarginal().setTF( m_workingDirectory + "/../TF/sequential/Reds.txt" );

        // TODO: set limits based on configuration
        setLims( { -1.1, 1.1 }, { -1.1, 1.1 } );

        m_undefined = false;
        m_configuration = c;

        m_scatterPlotTexture.create();
        m_lassoPointsTexture.create();
    }

    JointPlot2D( const std::string & workingDir, const std::string & projDir ) : 
        m_probeOn( false ),
        m_xMarginalView( "x" ),
        m_yMarginalView( "y" ),
        m_texDims( 0, 0 ),
        m_workingDirectory( workingDir ),
        m_projectDirectory( projDir ),        
        m_wKey( "" ),
        m_probeType( ProbeType::Lasso ),
        m_undefined( true ),
        m_contextBitMask( Marrus::FilterOp::WITHIN_COMPONENT_SELECTION ),
        m_focusBitMask(       Marrus::FilterOp::WITHIN_VOXEL_SELECTION )
    {
        m_deleteMe = false;
        m_exportMe = false;
        
        m_type = "scatter";

        m_xSubRange = m_xMarginalView.selectedRange();
        m_ySubRange = m_yMarginalView.selectedRange();

        m_leftMouseReleased = false;
        m_rightMouseReleased = false;
        m_mouseClicked  = false;
        m_hovered = false;

        m_modeButtons.push_back( TN::PressButton() );
        m_modeButtons.push_back( TN::PressButton() );
        m_modeButtons.push_back( TN::PressButton() );
        m_modeButtons.push_back( TN::PressButton() );                
        // m_modeButtons.push_back( TN::PressButton() );  

        m_modeButtons[ 0 ].color( { 0.99, 0.99, 0.99 } );
        m_modeButtons[ 1 ].color( { 0.99, 0.99, 0.99 } );
        m_modeButtons[ 2 ].color( { 0.99, 0.99, 0.99 } );
        m_modeButtons[ 3 ].color( { 0.99, 0.99, 0.99 } );

        m_modeButtons[ 0 ].text( "pts."  );
        m_modeButtons[ 1 ].text( "hst"   );
        m_modeButtons[ 2 ].text( "cdf"   );
        m_modeButtons[ 3 ].text( "kde" );
        // m_modeButtons[ 4 ].text( "gmm" );

        m_modeButtons[ 0 ].setPressed( true );
        m_currentMode = 0;

        m_deleteButton.setPressed( false );
        m_deleteButton.color( { 0.99, 0.99, 0.99 } );
        m_deleteButton.setTexFromPNG( m_workingDirectory + "/textures/x.png", m_workingDirectory + "/textures/x.png" );

        m_infoButton.setPressed( false );
        m_infoButton.color( { 0.99, 0.99, 0.99 } );
        m_infoButton.setTexFromPNG( m_workingDirectory + "/textures/info.png", m_workingDirectory + "/textures/info_pressed.png" );

        m_exportButton.setPressed( false );
        m_exportButton.color( { 0.99, 0.99, 0.99 } );
        m_exportButton.setTexFromPNG( m_workingDirectory + "/textures/export.png", m_workingDirectory + "/textures/export.png" );

        m_optimizeButton.setPressed( false );
        m_optimizeButton.color( { 0.99, 0.99, 0.99 } );
        m_optimizeButton.setTexFromPNG( m_workingDirectory + "/textures/wrench.png", m_workingDirectory + "/textures/wrench.png" );

        m_highLightButton.setPressed( true );
        m_highLightButton.color( { 0.99, 0.99, 0.99 } );
        m_highLightButton.setTexFromPNG( m_workingDirectory + "/textures/points.png", m_workingDirectory + "/textures/points.png" );

        m_conditionalButton.setPressed( false );
        m_conditionalButton.color( { 0.99, 0.99, 0.99 } );
        m_conditionalButton.setTexFromPNG( m_workingDirectory + "/textures/curves.png", m_workingDirectory + "/textures/curves.png" );

        m_subMarginalButton.setPressed( false );
        m_subMarginalButton.color( { 0.99, 0.99, 0.99 } );
        m_subMarginalButton.setTexFromPNG( m_workingDirectory + "/textures/marginals.png", m_workingDirectory + "/textures/marginals.png" );

        m_xLabelButton.setTexFromPNG( m_projectDirectory + "/latex_symbols/xaxis.png", m_projectDirectory   + "/latex_symbols/xaxis.png" );
        m_yLabelButton.setTexFromPNG( m_projectDirectory + "/latex_symbols/yaxis_v.png", m_projectDirectory + "/latex_symbols/yaxis_v.png" );

        m_fullCount = 0;
        m_subCount  = 0;    

        m_covariance = 0;
        m_coskewness = 0;
        m_cokurtosis = 0;

        m_hoveredBin = -1;
    }

    virtual void setSize( float x, float y ) override 
    { 
        float SMOFFSET = m_subMarginalButton.isPressed() ? 10 : 0;
        float DEFAULTMW = 50.f;
       
        Widget::setSize( x, y );

        m_xMarginalView.setSize( x, DEFAULTMW - SMOFFSET );
        m_yMarginalView.setSize( DEFAULTMW - SMOFFSET, y );

        for( auto & b : m_modeButtons )
        {
            b.setSize( this->size().x() / (float) m_modeButtons.size(), 22 );
        }

        m_highLightButton.setSize(   22, 22 );
        m_conditionalButton.setSize( 22, 22 );
        m_subMarginalButton.setSize( 22, 22 );

        m_deleteButton.setSize(   22, 22 );
        m_exportButton.setSize(   22, 22 );
        m_optimizeButton.setSize( 22, 22 );
        m_infoButton.setSize(     22, 22 );

        m_xLabelButton.resizeByHeight( 22 );
        m_yLabelButton.resizeByWidth(  22 );

        m_updatesNeeded.scatter = true;
        m_updatesNeeded.lassoPoints = true;
    }

    virtual void setContainerPosition( float x, float y  )
    {
        m_container.setPosition( x, y );
    }

    virtual void setContainerSize( float x, float y ) 
    {
        m_container.setSize( x, y );
    }

    virtual void setPosition(  float x, float y ) override 
    { 
        Widget::setPosition(     x, y ); 

        float SMOFFSET = m_subMarginalButton.isPressed() ? 20 : 0;

        m_xMarginalView.setPosition( x, y + size().y() + SMOFFSET );
        m_yMarginalView.setPosition( x + size().x() + SMOFFSET, y );

        for( size_t i = 0; i < m_modeButtons.size(); ++i )
        {
            auto & b = m_modeButtons[ i ];
            b.setPosition( 
                this->position().x() + i * b.size().x(),
                this->position().y() - b.size().y() - 52 );
        }

        m_highLightButton.setPosition(
            this->position().x() + this->size().x() + 2,
            this->position().y() - m_highLightButton.size().y() - 54 );

        m_conditionalButton.setPosition(
            m_highLightButton.position().x() + m_highLightButton.size().x() + 2,
            m_highLightButton.position().y() );

        m_subMarginalButton.setPosition(
            m_conditionalButton.position().x() + m_conditionalButton.size().x() + 2,
            m_conditionalButton.position().y() );

        ///////////////////////////////////////////////////////////////////////////

        m_deleteButton.setPosition( 
            m_yMarginalView.position().x() + m_yMarginalView.size().x() - 22,
            m_xMarginalView.position().y() + m_xMarginalView.size().y() - 22 );

        m_optimizeButton.setPosition( 
            m_deleteButton.position().x() - m_deleteButton.size().x(),
            m_deleteButton.position().y() - m_deleteButton.size().y() );

        m_exportButton.setPosition( 
            m_deleteButton.position().x(),
            m_deleteButton.position().y() - m_deleteButton.size().y() );

        m_infoButton.setPosition( 
            m_deleteButton.position().x() - m_deleteButton.size().x(),
            m_deleteButton.position().y() );

        m_xLabelButton.setPosition( 
            this->position().x() + this->size().x() / 2 - m_xLabelButton.size().x() / 2,
            this->position().y() - 44 );

        m_yLabelButton.setPosition( 
            this->position().x() - 44,
            this->position().y() + this->size().y() / 2 - m_yLabelButton.size().y() / 2 );
    } 

    bool deleteMe() const { return m_deleteMe; }
    bool exportMe() const { return m_exportMe; }

    void exportMe( bool c ) { m_exportMe = c; }

    std::vector< TN::PressButton * > buttons()
    {
        std::vector< TN::PressButton * > v;
        int i = 0;

        if( m_wKey == "" )
        {
            for( auto & b : m_modeButtons )
            {
                v.push_back( & b );
                ++i;
                if( i >= 5 )
                {
                    break;
                }
            }
        }
        else
        {
            v.push_back( & m_modeButtons[ 0 ] );
            v.push_back( & m_modeButtons[ 1 ] );   
            
            m_modeButtons[ 1 ].text( "cm" );
        }

        // v.push_back( & m_deleteButton );
        // v.push_back( & m_optimizeButton );
        // v.push_back( & m_exportButton );
        // v.push_back( & m_infoButton );

        v.push_back( & m_highLightButton );
        v.push_back( & m_conditionalButton );
        v.push_back( & m_subMarginalButton );
        v.push_back( & m_xLabelButton      );
        v.push_back( & m_yLabelButton      );

        return v;
    } 

    void setTitle( const std::string & title ) 
    {
        m_title = title;
    }

    void setXY( 
        const std::string & x, 
        const std::string & y )
    {
        m_xKey = x;
        m_yKey = y;
    
        m_xLabelButton.setTexFromPNG( m_projectDirectory + "/latex_symbols/" + m_xKey+ ".png", m_projectDirectory + "/latex_symbols/" + m_xKey+ ".png" );
        m_yLabelButton.setTexFromPNG( m_projectDirectory + "/latex_symbols/" + m_yKey+ "_v.png", m_projectDirectory + "/latex_symbols/" + m_yKey+ "_v.png" );
    
        m_xLabelButton.resizeByHeight( 18 );
        m_yLabelButton.resizeByHeight( 18 );

        m_undefined = false;

        m_xLabel = x;
        m_yLabel = y;
    }

    void setW( const std::string & w )
    {
        m_wKey = w;
        m_wLabel = w;
    }

    void type( const std::string & t )
    {
        m_type = t;
    }

    std::string type() const
    {
        return m_type;
    }

    std::string xKey()   const { return m_xKey;  }
    std::string yKey()   const { return m_yKey;  }
    std::string wKey()   const { return m_wKey;  }

    std::string xLabel() const { return m_xLabel;  }
    std::string yLabel() const { return m_yLabel;  }
    std::string wLabel() const { return m_wLabel;  }

    bool undefined() const { return m_undefined; }

    std::string title()  const { return m_title; }

    void setLims( const TN::Vec2< float  > & xLims, const TN::Vec2< float > & yLims ) 
    { 
        float padX = ( xLims.b() - xLims.a() ) / 10.f;
        m_xRange = xLims;
        m_xRange.a( m_xRange.a() - padX );
        m_xRange.b( m_xRange.b() + padX );

        float padY = ( yLims.b() - yLims.a() ) / 10.f;
        m_yRange = yLims;
        m_yRange.a( m_yRange.a() - padY );
        m_yRange.b( m_yRange.b() + padY );

        m_xMarginalView.setRange( m_xRange );  
        m_yMarginalView.setRange( m_yRange );  

        m_xSubRange = m_xMarginalView.selectedRange();
        m_ySubRange = m_yMarginalView.selectedRange();
    } 

    RangeSelector & xMarginal() { return m_xMarginalView; }
    RangeSelector & yMarginal() { return m_yMarginalView; }

    Vec2< float >    xRange() { return m_xRange;    }
    Vec2< float >    yRange() { return m_yRange;    }
    Vec2< float > xSubRange() { return m_xSubRange; }
    Vec2< float > ySubRange() { return m_ySubRange; }
    Vec2< float > xBoxRange() { return m_xBoxRange; }
    Vec2< float > yBoxRange() { return m_yBoxRange; }

    void xRange(    const Vec2< float > & r ) {    m_xRange = r; }
    void yRange(    const Vec2< float > & r ) {    m_yRange = r; }
    void xSubRange( const Vec2< float > & r ) { m_xSubRange = r; }
    void ySubRange( const Vec2< float > & r ) { m_ySubRange = r; }
    void xBoxRange( const Vec2< float > & r ) { m_xBoxRange = r; }
    void yBoxRange( const Vec2< float > & r ) { m_yBoxRange = r; }

    void leftMouseDrag( 
        const Vec2< float > & origin, 
        const Vec2< float > & last,
        const Vec2< float > & current )
    {
        if( m_probeType == ProbeType::Box || true )
        {
            m_xBoxRange = { std::min( origin.x(), current.x() ), std::max( origin.x(), current.x() ) };
            m_yBoxRange = { std::min( origin.y(), current.y() ), std::max( origin.y(), current.y() ) };
        
            m_xBoxRange = m_xBoxRange - TN::Vec2< float >( position().x(), position().x() );
            m_yBoxRange = m_yBoxRange - TN::Vec2< float >( position().y(), position().y() );

            m_xBoxRange = m_xBoxRange / size().x();
            m_yBoxRange = m_yBoxRange / size().y();

            auto upperRight = valueAt( { m_xBoxRange.b(), m_yBoxRange.b() } );
            auto lowerLeft  = valueAt( { m_xBoxRange.a(), m_yBoxRange.a() } );

            m_xBoxValueRange = { lowerLeft.x(), upperRight.x() };
            m_yBoxValueRange = { lowerLeft.y(), upperRight.y() };
        }        
        
        if( m_probeType == ProbeType::Lasso )
        {
            bool appendPoint = false;

            if( m_lassoPolygone.size() == 0 )
            {
                appendPoint = true;
            }
            else
            {
                auto lastLassoPoint = m_lassoScreenSpace.back();
                if( lastLassoPoint.distance( current ) > 4 ) // 3 pixel min distance 
                {
                    appendPoint = true;
                }
            }

            if( appendPoint )
            {
                TN::Vec2< float > c = current;
                c.x( ( c.x() - position().x() ) / (float) size().x() );
                c.y( ( c.y() - position().y() ) / (float) size().y() );

                float vaX = xSubRange().a();
                float vaY = ySubRange().a();
                float vbX = xSubRange().b();
                float vbY = ySubRange().b();

                TN::Vec2< float > vc( { ( 1.0 - c.x() ) * vaX + c.x() * vbX, 
                                        ( 1.0 - c.y() ) * vaY + c.y() * vbY } );

                m_lassoBBX.a( std::min( (float)vc.x(), m_lassoBBX.a() ) );
                m_lassoBBX.b( std::max( (float)vc.x(), m_lassoBBX.b() ) );         
                m_lassoBBY.a( std::min( (float)vc.y(), m_lassoBBY.a() ) );
                m_lassoBBY.b( std::max( (float)vc.y(), m_lassoBBY.b() ) );   
                m_lassoPolygone.push_back( vc );
                m_lassoScreenSpace.push_back( current );
            }
        }
    }

    void rightMouseDrag( 
        const Vec2< double > & origin, 
        const Vec2< double > & last,
        const Vec2< double > & current )
    {

    }    
    
    void mouseOn()
    {

    }

    void mouseOff()
    {

    }

    void mouseWheel( int delta )
    {

    }

    std::vector< TN::Vec2< float > > boxLines( bool extended ) const
    {
        if( extended )
        {
            return {
                { m_xBoxRange.a(), 0 }, { m_xBoxRange.a(), 1 }, 
                { 0, m_yBoxRange.b() }, { 1, m_yBoxRange.b() }, 
                { m_xBoxRange.b(), 0 }, { m_xBoxRange.b(), 1 }, 
                { 0, m_yBoxRange.a() }, { 1, m_yBoxRange.a() }
            };
        }
        else
        {
            return {
                { m_xBoxRange.a(), m_yBoxRange.a() }, { m_xBoxRange.a(), m_yBoxRange.b() }, 
                { m_xBoxRange.a(), m_yBoxRange.b() }, { m_xBoxRange.b(), m_yBoxRange.b() }, 
                { m_xBoxRange.b(), m_yBoxRange.b() }, { m_xBoxRange.b(), m_yBoxRange.a() }, 
                { m_xBoxRange.b(), m_yBoxRange.a() }, { m_xBoxRange.a(), m_yBoxRange.a() }
            };   
        }
    }

    int binIndex( TN::Vec2< double > & p )
    {
        float rx = ( p.x() - position().x() ) / ( size().x() );
        float ry = ( p.y() - position().y() ) / ( size().y() );
    
        int xBin = std::floor( rx * m_texDims.a() );
        int yBin = std::floor( ry * m_texDims.b() );

        if( xBin > 0 && yBin > 0 && xBin < m_texDims.a() && yBin < m_texDims.b() )
        {
            return yBin * m_texDims.a() + xBin;
        }
        else
        {
            return -1;
        }
    }

    bool layersChanged() const 
    {
        return m_layersChanged;
    }

    bool modeChanged() const  
    {
        return m_modeChanged;
    }

    void processChanges() 
    {
        m_layersChanged = false;
        m_modeChanged   = false;
    }

    bool handleInput( 
        const InputState & input )
    {
        m_layersChanged = updateButtonState( input );

        Vec2< double > p = input.mouseState.position;
        MouseButtonState mouseButtonState = input.mouseState.buttonState;
 
        bool changed = false;
        m_leftMouseReleased = false;
        m_rightMouseReleased = false;
        m_hovered = false;

        m_hoveredBin = -1;

        if( mouseOver( p ) )
        {
            m_hovered = true;
            m_hoveredBin = binIndex( p );

            m_hoveredPosition = valueAt( {
                ( p.x() - position().x() ) / ( size().x() ),
                ( p.y() - position().y() ) / ( size().y() ) } );

            if( input.mouseState.event == MouseEvent::LeftReleased ) // || input.mouseState.event == MouseEvent::RightReleased )
            {
                m_leftMouseReleased = true;
            }

            if( input.mouseState.event == MouseEvent::RightReleased ) // || input.mouseState.event == MouseEvent::RightReleased )
            {
                m_rightMouseReleased = true;
            }

            if( ! m_stateInfo.mouseState.on )
            {
                mouseOn();
            }

            if( input.mouseState.event == MouseEvent::LeftPressed || input.mouseState.event == MouseEvent::RightPressed  )
            {
                m_stateInfo.mouseState.pos0 = input.mouseState.eventPosition;
                m_probeOn = false;
                if( input.mouseState.event == MouseEvent::LeftPressed )
                {
                    m_lassoPolygone.clear();
                    m_lassoScreenSpace.clear();
                    m_lassoBBX = { std::numeric_limits<float>::max(), -std::numeric_limits<float>::max() };
                    m_lassoBBY = m_lassoBBX;
                    m_mouseClicked = true;
                }
            }

            if( m_stateInfo.mouseState.rightButton && mouseButtonState == MouseButtonState::RightPressed )
            {
                rightMouseDrag( 
                    m_stateInfo.mouseState.pos0,
                    m_stateInfo.mouseState.pos, 
                    p );

                m_xMarginalView.mouseDrag( p );
                m_yMarginalView.mouseDrag( p );

                changed = true;

                m_updatesNeeded.kde = true;
                m_updatesNeeded.histogram = true;
                m_updatesNeeded.scatter = true;
                m_updatesNeeded.lassoPoints = true;
            }

            if( m_stateInfo.mouseState.leftButton && mouseButtonState == MouseButtonState::LeftPressed )
            {
                leftMouseDrag(
                    m_stateInfo.mouseState.pos0,
                    m_stateInfo.mouseState.pos, 
                    p ); 

                m_probeOn = true;
                changed = true;      
            }

            m_stateInfo.mouseState.leftButton  = mouseButtonState == MouseButtonState::LeftPressed;
            m_stateInfo.mouseState.rightButton = mouseButtonState == MouseButtonState::RightPressed;
            m_stateInfo.mouseState.pos = p; 
            m_stateInfo.mouseState.on = true;
        }
        else
        {
            if( m_stateInfo.mouseState.on )
            {
                mouseOff();
            }
            m_stateInfo.mouseState.on = false;
        }

        if( m_xMarginalView.pointInViewPort( p ) )
        {
            m_xMarginalView.mouseWheel( - input.mouseState.scrollDelta() / 150.f  );

            if( std::abs( input.mouseState.scrollDelta() ) > 0 )
            {
                m_updatesNeeded.kde = true;
                m_updatesNeeded.histogram = true;
                m_updatesNeeded.scatter   = true; 
                m_updatesNeeded.lassoPoints = true;
            }

            if(  mouseButtonState == MouseButtonState::LeftPressed )
            {
                m_xMarginalView.mouseDrag( p );

                m_updatesNeeded.kde = true;
                m_updatesNeeded.histogram = true;
                m_updatesNeeded.scatter   = true;
                m_updatesNeeded.lassoPoints = true;
            }
        }

        else if( m_yMarginalView.pointInViewPort( p ) )
        {          
            if( std::abs( input.mouseState.scrollDelta() ) > 0 )
            {
                m_updatesNeeded.kde = true;
                m_updatesNeeded.histogram = true;
                m_updatesNeeded.scatter   = true; 
                m_updatesNeeded.lassoPoints = true;
            }

            m_yMarginalView.mouseWheel( -input.mouseState.scrollDelta() / 150.f );

            if(  mouseButtonState == MouseButtonState::LeftPressed )
            {
                m_yMarginalView.mouseDrag( p );

                m_updatesNeeded.kde = true;
                m_updatesNeeded.histogram = true;
                m_updatesNeeded.scatter   = true;
                m_updatesNeeded.lassoPoints = true;
            }
        } 
        else if( mouseOver( p ) )
        {
            m_xMarginalView.mouseWheel( -input.mouseState.scrollDelta() / 50.f );
            m_yMarginalView.mouseWheel( -input.mouseState.scrollDelta() / 50.f );

            if( std::abs( input.mouseState.scrollDelta() ) > 0  )
            {
                m_updatesNeeded.kde = true;
                m_updatesNeeded.histogram = true;
                m_updatesNeeded.scatter   = true;
                m_updatesNeeded.lassoPoints = true;
            }
        }

        if( ! ( m_xSubRange == m_xMarginalView.selectedRange() ) 
        ||  ! ( m_ySubRange == m_yMarginalView.selectedRange() ) )
        {
            changed = true;

            m_xSubRange = m_xMarginalView.selectedRange();
            m_ySubRange = m_yMarginalView.selectedRange();   

            m_xBoxRange.a( ( m_xBoxValueRange.a() - m_xSubRange.a() ) / ( m_xSubRange.b() - m_xSubRange.a() ) );
            m_xBoxRange.b( ( m_xBoxValueRange.b() - m_xSubRange.a() ) / ( m_xSubRange.b() - m_xSubRange.a() ) );

            m_yBoxRange.a( ( m_yBoxValueRange.a() - m_ySubRange.a() ) / ( m_ySubRange.b() - m_ySubRange.a() ) );
            m_yBoxRange.b( ( m_yBoxValueRange.b() - m_ySubRange.a() ) / ( m_ySubRange.b() - m_ySubRange.a() ) );
        }

        return changed;
    }

    bool hovered() const { return m_hovered; }

    double hoveredBinValue() const
    {
        if( m_hoveredBin >= 0 )
        {
            if( type() == "hist2d")
            {
                if( m_hoveredBin < m_pdfData.size() )
                {
                    return m_pdfData[ m_hoveredBin ];
                }
                else
                {
                    return -1;
                }
            }
            else if( type() == "cdf2d" )
            {
                if( m_hoveredBin < m_cdfData.size() )
                {
                    return m_cdfData[ m_hoveredBin ];
                }
                else
                {
                    return -1;
                }
            }
        }
        else
        {
            return -1;
        }
    }

    std::vector< TN::Vec2< float > > hoveredBinBox() const
    {
        if( m_hoveredBin >= 0 && m_texDims.a() > 0 && m_texDims.b() > 0 )
        {
            int xd = m_texDims.a();
            int yd = m_texDims.b();      
            
            int xb =  m_hoveredBin % xd;
            int yb = ( m_hoveredBin - xb ) / xd;

            float dx = size().x() / (double) xd;
            float dy = size().y() / (double) yd;

            TN::Vec2< float > ll = { position().x() + xb * dx, position().y() + yb * dy };
            TN::Vec2< float > ur = { position().x() + ( xb + 1 ) * dx, position().y() + ( yb + 1 ) * dy };

            return
            {
                ll,                 { ur.x(), ll.y() },
                { ur.x(), ll.y() }, ur,
                ur,                 { ll.x(), ur.y() },
                { ll.x(), ur.y() }, ll
            };
        }
        else
        {
            return { { 0, 0 }, { 0, 0 } };
        }
    }

    bool probeActive() const { return m_probeOn; }

void estimatenMomentsFromHistogram()
{
   // x

    const int XD = m_xPDFData.size();

    m_meanXH = 0;
    m_varXH  = 0;
    m_skewXH  = 0;
    m_kurtXH  = 0;

    double m2x = 0.0;
    double m3x = 0.0;
    double m4x = 0.0;

    double m2y = 0.0;
    double m3y = 0.0;
    double m4y = 0.0;  

    float dx = ( m_xSubRange.b() - m_xSubRange.a() ) / XD;

    for( int i = 0; i < XD; ++i )
    {
        double binCenter = m_xSubRange.a() + dx * ( i + 1 / 2.0 );
        m_meanXH += m_xPDFData[ i ] * binCenter;
    }
    
    for( int i = 0; i < XD; ++i )
    {
        double binCenter = m_xSubRange.a() + dx * ( i + 1 / 2.0 );
        double diffX = binCenter - m_meanXH;

        m2x += std::pow( diffX, 2.f ) * m_xPDFData[ i ];
        m3x += std::pow( diffX, 3.f ) * m_xPDFData[ i ];
        m4x += std::pow( diffX, 4.f ) * m_xPDFData[ i ];
    }    

    m_varXH = m2x;
    double sdevXH = std::sqrt( m_varXH );
    m_skewXH = m3x / std::pow( sdevXH, 3.0 ); 
    m_kurtXH = m4x / std::pow( m2x,   2.0 ) - 3.0; 

    // y 

    const int YD = m_yPDFData.size();

    m_meanYH = 0;
    m_varYH  = 0;
    m_skewYH  = 0;
    m_kurtYH  = 0;

    float dy = ( m_ySubRange.b() - m_ySubRange.a() ) / YD;

    for( int i = 0; i < YD; ++i )
    {
        double binCenter = m_ySubRange.a() + dy * ( i + 1 / 2.0 );
        m_meanYH += m_yPDFData[ i ] * binCenter;
    }

    for( int i = 0; i < YD; ++i )
    {
        double binCenter = m_ySubRange.a() + dy * ( i + 1 / 2.0 );
        double diffY = binCenter - m_meanYH;

        m2y += std::pow( diffY, 2.f ) * m_yPDFData[ i ];
        m3y += std::pow( diffY, 3.f ) * m_yPDFData[ i ];
        m4y += std::pow( diffY, 4.f ) * m_yPDFData[ i ];  
    }    

    m_varYH = m2y;
    double sdevYH = std::sqrt( m_varYH );
    m_skewYH = m3y / std::pow( sdevYH, 3.0 ); 
    m_kurtYH = m4y / std::pow( m2y,   2.0 ) - 3.0; 

    // x X y

    m_covarianceH = 0.0;
    m_coskewnessH = 0.0;
    m_cokurtosisH = 0.0;

    for( int i = 0; i < XD; ++i )
    {
        for( int j = 0; j < YD; ++ j )
        {
            double cX = m_xSubRange.a() + dx * ( i + 1 / 2.0 );
            double cY = m_ySubRange.a() + dy * ( j + 1 / 2.0 );        

            double diffX = cX - m_meanXH;
            double diffY = cY - m_meanYH;        

            double p = m_pdfData[ j * XD + i ]; 

            m_covarianceH += diffX * diffY * p;
            m_coskewnessH += diffX * diffX * diffY * p;
            m_cokurtosisH += diffX * diffX * diffY * diffY * p;
        }
    }

    m_coskewnessH /=  m_varXH * std::sqrt( m_varYH );
    m_cokurtosisH /=  m_varXH * m_varYH;
}

//////////////////////////////////////////////////////////////////////////////

void updateConfiguration( const TN::JointPlotConfiguration & c )
{
    /* Update is considered needed if previously needed to be updated,
       or  if configuration change necessitates it */

    std::cout << "updating configuration" << std::endl;

    m_updatesNeeded.update( 
        m_configuration.update( c ) );

    setXY( c.x, c.y );
    setW(  c.w == "None" ? "" : c.w );
}

void updateYMarginal(
    const std::vector< float > & y,
    const std::vector< int >   & flags )
{
    if( m_updatesNeeded.y_marginals )
    {
        std::cout << "updating y marginal" << std::endl;
        yMarginal().update( y, flags, TN::VOXEL_IN_DECOMPOSITION );
    }
    m_updatesNeeded.y_marginals = false;
}

void updateXMarginal(
    const std::vector< float > & x, 
    const std::vector< int > & flags )
{
    if( m_updatesNeeded.x_marginals )
    {
        std::cout << "updating x marginal" << std::endl;
        xMarginal().update( x, flags, TN::VOXEL_IN_DECOMPOSITION );
    }

    m_updatesNeeded.x_marginals = false;
}

void updateGMM(
    const std::vector< float > & x, 
    const std::vector< float > & y, 
    const std::vector< int >   & flags )
{

    m_updatesNeeded.gmm = false;
}

void updateKde(
    const std::vector< float > & x, 
    const std::vector< float > & y, 
    const std::vector< int >   & flags )
{
    if( ! m_updatesNeeded.kde )
    {
        return;
    }

    const double x0 = xSubRange().a();
    const double y0 = ySubRange().a();

    const double xwidth = xSubRange().b() - xSubRange().a();
    const double ywidth = ySubRange().b() - ySubRange().a();

    const int kdeWidth     = 512;
    const int kdeHeight    = 512;

    const double bandwidth = 4.0;

    std::vector< float > image( kdeWidth*kdeHeight );
    Marrus::computeKDE(  
        kdeWidth, 
        kdeHeight, 
        bandwidth, 
        x,
        y,
        x0,
        y0,
        xwidth,
        ywidth,
        image );

    auto texRange = TN::Sequential::getRange( image );
    m_kdeTexture.load( image, kdeWidth, kdeHeight  );

    m_updatesNeeded.kde = false;
}

void setUpdateAll()
{
    m_updatesNeeded.setAll( true );
}

///////////////////////////////////////////////////////////////////////////////

void updateHist2D( 
        const std::vector< float > & x, 
        const std::vector< float > & y,
        const std::vector< int > & flags,
        const Hist2DParams & params = Hist2DParams() )
    {
        // bin the data and then make a texture from it

        if( ! m_updatesNeeded.histogram )
        {
            return;
        }

        std::cout << "updating histogram" << std::endl;

        const int CONTEXT_MASK = m_contextBitMask;
        const int FOCUS_MASK   = m_focusBitMask;        

        m_fullCount = x.size();
        m_gCount    = 0.f;
        m_rCount    = 0.f;
        m_subCount  = 0.f;
        m_contextCount = 0.f;

        if( params.edgeMode == Hist2DEdgeMode::Uniform )
        {
            m_texDims = { params.nBinsX, params.nBinsY };

            if( m_pdfData.size() != params.nBinsX * params.nBinsY )
            {
                m_pdfData.resize( m_texDims.x() * m_texDims.y() );
            }

            std::fill( m_pdfData.begin(), m_pdfData.end(), 0.f );
            m_pdfContextData = m_pdfData;
            
            TN::Vec2<float > rx;
            TN::Vec2<float > ry;          

            if( params.rangeMode == Hist2DRangeMode::Dynamic ) // use the user interface
            {
                // use the user's range selection
                rx = xSubRange();
                ry = ySubRange();
            }
            else // fixed resolution
            {
                // use the resolution given in the params file
                // set the marginals ranges to match
                // careful to account for normalization
                //...
            }

            const float xMN = rx.a();
            const float xMX = rx.b();
            const float yMN = ry.a();
            const float yMX = ry.b();

            const float XW = xMX - xMN;
            const float YW = yMX - yMN;

            const int NBX = params.nBinsX;
            const int NBY = params.nBinsY;

            m_xPDFData.resize( NBX );
            std::fill( m_xPDFData.begin(), m_xPDFData.end(), 0.f );

            m_yPDFData.resize( NBY );
            std::fill( m_yPDFData.begin(), m_yPDFData.end(), 0.f );

            m_meanX = 0;
            m_meanY = 0;

            const size_t NP = x.size();
            for( size_t i = 0; i < NP; ++i )
            {
                // not in the spatial region
                if( ! ( flags[ i ] & CONTEXT_MASK )  )
                {
                    continue;
                }

                m_rCount += 1.f;

                // is one of the points in the region which is highlighted
                if( flags[ i ] & FOCUS_MASK )
                {
                    m_gCount += 1.f;
                }

                int xBin = std::floor( ( ( x[ i ] - xMN ) / XW ) * NBX );
                int yBin = std::floor( ( ( y[ i ] - yMN ) / YW ) * NBY );

                // is a point in the region which falls within the histogram edge limits
                if( xBin >= 0 && xBin < NBX 
                 && yBin >= 0 && yBin < NBY )
                {
                    const int flatIDX = yBin * NBX + xBin; 
                 
                    m_pdfContextData[ flatIDX ] = 1.f;
                    m_contextCount += 1.f;

                    if( flags[ i ] & FOCUS_MASK )
                    {
                        m_pdfData[ flatIDX ] += 1.f;
                        m_subCount += 1.f;
                        m_xPDFData[ xBin ] += 1.f;
                        m_yPDFData[ yBin ] += 1.f;
                        m_meanX += x[ i ];
                        m_meanY += y[ i ];
                    }
                }
            }
            
            if( m_subCount > 1 )
            {
                m_meanX /= ( m_subCount );
                m_meanY /= ( m_subCount );
            }      

            m_covariance = 0;

            float m2x = 0.0;
            float m3x = 0.0;
            float m4x = 0.0;      

            float m2y = 0.0;
            float m3y = 0.0;
            float m4y = 0.0;

            m_coskewness = 0;      
            m_cokurtosis = 0;      

            if( m_subCount > 1 )
            {
                for( size_t i = 0; i < NP; ++i )
                {
                    if( ! ( flags[ i ] & FOCUS_MASK )  )
                    {
                        continue;
                    }

                    int xBin = std::floor( ( ( x[ i ] - xMN ) / XW ) * NBX );
                    int yBin = std::floor( ( ( y[ i ] - yMN ) / YW ) * NBY );

                    if( xBin >= 0 && xBin < NBX 
                     && yBin >= 0 && yBin < NBY )
                    {
                        double diffX = x[ i ] - m_meanX;
                        double diffY = y[ i ] - m_meanY;

                        m_covariance += diffX * diffY;
                        m_coskewness += diffX * diffX * diffY;
                        m_cokurtosis += diffX * diffX * diffY * diffY;

                        m2x += std::pow( diffX, 2.f );
                        m3x += std::pow( diffX, 3.f );
                        m4x += std::pow( diffX, 4.f );

                        m2y += std::pow( diffY, 2.f );
                        m3y += std::pow( diffY, 3.f );
                        m4y += std::pow( diffY, 4.f );                        
                    }
                }

                m2x /= m_subCount;
                m3x /= m_subCount;
                m4x /= m_subCount;

                m2y /= m_subCount;
                m3y /= m_subCount;
                m4y /= m_subCount;

                m_varX = m2x;
                double sdevX = std::sqrt( m_varX );
                m_skewX = m3x / std::pow( sdevX, 3.0 ); 
                m_kurtX = m4x / std::pow( m2x,   2.0 ) - 3.0; 

                m_varY = m2y;
                double sdevY = std::sqrt( m_varY );
                m_skewY = m3y / std::pow( sdevY, 3.0 ); 
                m_kurtY = m4y / std::pow( m2y,   2.0 ) - 3.0; 

                m_covariance /= m_subCount;
                m_coskewness /= m_subCount * m_varX * sdevY;
                m_cokurtosis /= m_subCount * m_varX * m_varY;

                txBinnedMin = std::numeric_limits<float>::max();
                txBinnedMax = -txBinnedMin;

                for( size_t i = 0; i < NBX * NBY; ++i )
                {
                    m_pdfData[ i ] /= m_subCount;
                    txBinnedMin = std::min( txBinnedMin, m_pdfData[ i ] );
                    txBinnedMax = std::max( txBinnedMax, m_pdfData[ i ] );     

                }     
                #pragma omp parallel for 
                for( size_t i = 0; i < NBX; ++i )
                {
                    m_xPDFData[ i ] /= m_subCount;
                }
                #pragma omp parallel for 
                for( size_t i = 0; i < NBY; ++i )
                {
                    m_yPDFData[ i ] /= m_subCount;
                }     
            }

            m_cdfData.resize( NBX * NBY );
            std::fill( m_cdfData.begin(), m_cdfData.end(), 0.f );

            for( int xBin = 0; xBin < NBX; ++xBin )
            {
                for( int yBin = 0; yBin < NBY; ++yBin )
                {
                    const int flatIDX = yBin * NBX + xBin; 

                    for( int xP = xBin; xP >= 0; --xP )
                    {
                        for( int yP = yBin; yP >= 0; --yP )
                        {
                            const int pfId = yP * NBX + xP; 
                            m_cdfData[ flatIDX ] += m_pdfData[ pfId ]; 
                        }
                    }
                }
            }

            m_texture.load(          m_pdfData,        NBX, NBY );
            m_contextTexture.load(   m_pdfContextData, NBX, NBY );
            m_cdfTexture.load( m_cdfData,           NBX, NBY );
            m_xSubMarginalTexture.load( m_xPDFData, NBX,   1 );
            m_ySubMarginalTexture.load( m_yPDFData, 1,   NBY );

            estimatenMomentsFromHistogram();
        }
        else
        {

        }

        m_updatesNeeded.histogram = false; 
    }

    void updateCDF1D(
        const std::vector< float > & x )
    {

    }

    void updateCDF2D(
        const std::vector< float > & x,
        const std::vector< float > & y )
    {
    
    }

    void updateConditional2D(
        const std::vector< float > & x,
        const std::vector< float > & y,
        const std::vector< int >   & flags )
    {
        if( ! m_updatesNeeded.curves )
        {
            return;
        }

        std::cout << "updating curves" << std::endl;

        const int FOCUS_MASK = m_focusBitMask;
        const int N_BINS = 128;

        m_conditionalCurves;

        auto rx = xSubRange();
        auto ry = ySubRange();

        std::vector< float > sums(   N_BINS, 0.f );
        std::vector< int > counts(   N_BINS, 0.f );
        std::vector< float > means ( N_BINS, 0.f );
        std::vector< float > sdevs ( N_BINS, 0.f );

        size_t NP = x.size(); 

        for( size_t i = 0; i < NP; ++i )
        {
            if( ! ( flags[ i ] & FOCUS_MASK )  )
            {
                continue;
            }

            int bIdx = std::floor( ( x[ i ] - rx.a() ) / ( rx.b() - rx.a() ) * N_BINS );
            if( bIdx >= 0 && bIdx < N_BINS )
            {
                sums[ bIdx ] += y[ i ];
                counts[ bIdx ] += 1;
            }
        }

        // flag for empty bins?
        for( size_t i = 0; i < N_BINS; ++i )
        {
           means[ i ] = counts[ i ] > 0 ? sums[ i ] / counts[ i ] : 0.f;
        }

        for( size_t i = 0; i < NP; ++i )
        {
            if( ! ( flags[ i ] & FOCUS_MASK )  )
            {
                continue;
            }

            int bIdx = std::floor( ( x[ i ] - rx.a() ) / ( rx.b() - rx.a() ) * N_BINS );
            if( bIdx >= 0 && bIdx < N_BINS )
            {
                sdevs[ bIdx ] += std::pow( means[ bIdx ] - y[ i ], 2.f );
            }
        }

        for( size_t i = 0; i < N_BINS; ++i )
        {
            sdevs[ i ] = counts[ i ] > 0 ? std::sqrt( sdevs[ i ] / counts[ i ] ) : 0.f;
        }

        m_conditionalCurves.meanCurve = means;
        m_conditionalCurves.standardDeviationCurve = sdevs;
        m_conditionalCurves.counts = counts;

        float binWidth = ( rx.b() - rx.a() ) / N_BINS;
        m_conditionalCurves.binCenters.resize( N_BINS );

        for( int i = 0; i < N_BINS; ++i )
        {
            m_conditionalCurves.binCenters[ i ] = rx.a() + i * binWidth + binWidth / 2.f;
        }

        m_updatesNeeded.curves = false;
    }

    void updateConditional2D(
        const std::vector< float > & x,
        const std::vector< float > & y )
    {
        if( ! m_updatesNeeded.curves )
        {
            return;
        }

        std::cout << "updating curves" << std::endl;

        int N_BINS = 128;
        m_conditionalCurves;

        auto rx = xSubRange();
        auto ry = ySubRange();

        std::vector< float > sums(   N_BINS, 0.f );
        std::vector< int > counts(   N_BINS, 0.f );
        std::vector< float > means ( N_BINS, 0.f );
        std::vector< float > sdevs ( N_BINS, 0.f );

        size_t NP = x.size(); 

        for( size_t i = 0; i < NP; ++i )
        {
            int bIdx = std::floor( ( x[ i ] - rx.a() ) / ( rx.b() - rx.a() ) * N_BINS );
            if( bIdx >= 0 && bIdx < N_BINS )
            {
                sums[ bIdx ] += y[ i ];
                counts[ bIdx ] += 1;
            }
        }

        // flag for empty bins?
        for( size_t i = 0; i < N_BINS; ++i )
        {
           means[ i ] = counts[ i ] > 0 ? sums[ i ] / counts[ i ] : 0.f;
        }

        for( size_t i = 0; i < NP; ++i )
        {
            int bIdx = std::floor( ( x[ i ] - rx.a() ) / ( rx.b() - rx.a() ) * N_BINS );
            if( bIdx >= 0 && bIdx < N_BINS )
            {
                sdevs[ bIdx ] += std::pow( means[ bIdx ] - y[ i ], 2.f );
            }
        }

        for( size_t i = 0; i < N_BINS; ++i )
        {
            sdevs[ i ] = counts[ i ] > 0 ? std::sqrt( sdevs[ i ] ) / counts[ i ] : 0.f;
        }

        m_conditionalCurves.meanCurve = means;
        m_conditionalCurves.standardDeviationCurve = sdevs;
        m_conditionalCurves.counts = counts;

        m_updatesNeeded.curves = false;
    }

    float fullCount() const
    {
        return m_fullCount;
    }

    float subCount () const
    {
        return m_subCount;
    }

    float gCount () const
    {
        return m_gCount;
    }

    float rCount () const
    {
        return m_rCount;
    }

    float getBinnedMin() const
    {
        return txBinnedMin;
    }

    float getBinnedMax() const
    {
        return txBinnedMax;
    }

    TN::Vec2< float > pdfRange() const
    {
        return { txBinnedMin, txBinnedMax };
    }

    TN::Vec2< float > kdeRange() const
    {
        return { m_kdeTexture.minValue, m_kdeTexture.maxValue };
    }

    TN::Vec2< float > cdfRange() const
    {
        return { m_cdfTexture.minValue, m_cdfTexture.maxValue };
    }

    std::vector< TN::Vec2< float > > lasso() const
    {
        return m_lassoPolygone;
    }

    TN::Vec2< float > lassoBBX() const
    {
        return m_lassoBBX;
    }

    TN::Vec2< float > lassoBBY() const
    {
        return m_lassoBBY;
    }

    ProbeType probeType() const 
    {
        return m_probeType;
    }

    void setProbeType( TN::ProbeType t )
    {
        m_probeType = t;
    }

    void updateConditional3D( 
        const std::vector< float > & x, 
        const std::vector< float > & y,
        const std::vector< float > & w,        
        const std::vector< int >   & flags,
        const Hist2DParams & params = Hist2DParams() )
    {
        const int CONTEXT_MASK = m_contextBitMask;
        const int FOCUS_MASK = m_focusBitMask;

        updateHist2D( 
            x, 
            y, 
            flags,       
            params );

        // std::cout << "updated the hist 2d portion" << std::endl;

        if( params.edgeMode == Hist2DEdgeMode::Uniform )
        {
            // std::cout << "setting params" << std::endl;

            m_texDims = { params.nBinsX, params.nBinsY };
            
            TN::Vec2<float > rx;
            TN::Vec2<float > ry;          

            if( params.rangeMode == Hist2DRangeMode::Dynamic ) // use the user interface
            {
                // use the user's range selection
                rx = xSubRange();
                ry = ySubRange();
            }
            else // fixed resolution
            {
                // use the resolution given in the params file
                // set the marginals ranges to match
                // careful to account for normalization
                //...
            }

            const float xMN = rx.a();
            const float xMX = rx.b();
            const float yMN = ry.a();
            const float yMX = ry.b();

            const float XW = xMX - xMN;
            const float YW = yMX - yMN;

            const int NBX = params.nBinsX;
            const int NBY = params.nBinsY;

            m_contextCount = 0.f;

            if( m_wXYAverage.size() != NBX * NBY  )
            {
                m_wXYAverage.resize( NBX * NBY );
            }

            m_pdfContextData.resize( NBX * NBY );
            std::fill( m_pdfContextData.begin(), m_pdfContextData.end(), 0.f );            

            std::fill( m_wXYAverage.begin(), m_wXYAverage.end(), 0.f );
            auto countsXY = m_wXYAverage;

            m_wXAverage.resize( NBX );
            std::fill( m_wXAverage.begin(), m_wXAverage.end(), 0.f );
            auto countsX = m_wXAverage;

            m_wYAverage.resize( NBY );
            std::fill( m_wYAverage.begin(), m_wYAverage.end(), 0.f );
            auto countsY = m_wYAverage;

            m_meanW = 0;

            // std::cout << "binning" << std::endl;

            const size_t NP = x.size();
            for( size_t i = 0; i < NP; ++i )
            {
                // not in the spatial region
                if( ! ( flags[ i ] & CONTEXT_MASK )  )
                {
                    continue;
                }


                int xBin = std::floor( ( ( x[ i ] - xMN ) / XW ) * NBX );
                int yBin = std::floor( ( ( y[ i ] - yMN ) / YW ) * NBY );

                // is a point in the region which falls within the histogram edge limits
                if( xBin >= 0 && xBin < NBX 
                 && yBin >= 0 && yBin < NBY )
                {
                    const int flatIDX = yBin * NBX + xBin; 
                    m_pdfContextData[ flatIDX ] = 1.f;
                    m_contextCount += 1.f;

                    if( flags[ i ] & FOCUS_MASK )
                    {
                        m_wXYAverage[ flatIDX ] += w[ i ];
                        m_wXAverage[ xBin ] += w[ i ];
                        m_wYAverage[ yBin ] += w[ i ];
                        countsX[ xBin ] += 1;
                        countsY[ yBin ] += 1;          
                        countsXY[ flatIDX ] += 1;          
                        m_meanW += w[ i ];
                    }
                }
            }
            
            if( m_subCount > 1 )
            {
                m_meanW /= ( m_subCount );
            }      

            // std::cout << "normalizing" << std::endl;

            txBinnedMin = std::numeric_limits<float>::max();
            txBinnedMax = -txBinnedMin;

            for( int i = 0; i < NBX; ++i )
            {
                if( countsX[ i ] > 0 )
                {
                    m_wXAverage[ i ] /= countsX[ i ];
                }
            }
            for( int i = 0; i < NBY; ++i )
            {
                if( countsY[ i ] > 0 )
                {
                    m_wYAverage[ i ] /= countsY[ i ];
                }
            }
            for( int i = 0; i < NBX*NBY; ++i )
            {
                if( countsXY[ i ] > 0 )
                {
                    m_wXYAverage[ i ] /= countsXY[ i ];
                    txBinnedMin = std::min( txBinnedMin, m_wXYAverage[ i ] );
                    txBinnedMax = std::max( txBinnedMax, m_wXYAverage[ i ] );                    
                }
            }
            
            m_cdfData.resize( NBX * NBY );
            std::fill( m_cdfData.begin(), m_cdfData.end(), 0.f );

            // std::cout << "computing cdf" << std::endl;

            for( int xBin = 0; xBin < NBX; ++xBin )
            {
                for( int yBin = 0; yBin < NBY; ++yBin )
                {
                    const int flatIDX = yBin * NBX + xBin; 

                    for( int xP = xBin; xP >= 0; --xP )
                    {
                        for( int yP = yBin; yP >= 0; --yP )
                        {
                            const int pfId = yP * NBX + xP; 
                            m_cdfData[ flatIDX ] += m_wXYAverage[ pfId ]; 
                        }
                    }
                }
            }

            m_texture.load(             m_wXYAverage, NBX, NBY );
            m_contextTexture.load(      m_pdfContextData, NBX, NBY );
            m_cdfTexture.load(          m_cdfData,    NBX, NBY );
            m_xSubMarginalTexture.load( m_wXAverage,  NBX,   1 );
            m_ySubMarginalTexture.load( m_wYAverage,  1,   NBY );
        }
        else
        {

        }

        //std::cout << "done" << std::endl;
    }

    void updateKDE1D( const std::vector< float > & x )
    {

    }

    void updateKDE2D(
        const std::vector< float > & x,
        const std::vector< float > & y )
    {

    }

    void updateGuassian1D( const std::vector< float > & x )
    {

    }

    void updateGuassian2D(
        const std::vector< float > & x,
        const std::vector< float > & y )
    {

    }

    void updateGuassianMixture1D( const std::vector< float > & x )
    {

    }

    void updateGuassianMixture2D(
        const std::vector< float > & x,
        const std::vector< float > & y )
    {
        
    }

    TN::Vec2< float > containerPosition() const {
        return m_container.position();
    }

    TN::Vec2< float > containerSize() const {
        return m_container.size();
    }

    void setScatterPlotNeedsUpdate( bool c )
    {
        m_updatesNeeded.scatter = c;
    }

    bool scatterPlotNeedsUpdate() const 
    {
        return m_updatesNeeded.scatter; 
    }

    void setLassoPointsNeedsUpdate( bool c )
    {
        m_updatesNeeded.lassoPoints = c;
    }

    bool lassoPointsNeedsUpdate() const 
    {
        return m_updatesNeeded.lassoPoints; 
    }


    Widget & container() { return m_container; }

    TN::TextureLayer & kdeTexture()        { return m_kdeTexture; }
    TN::TextureLayer & cdfTexture()        { return m_cdfTexture; }
    TN::TextureLayer & texture()           { return m_texture; }
    TN::TextureLayer & contextTexture()    { return m_contextTexture; }    
    TN::TextureLayer & xsmTex()            { return m_xSubMarginalTexture; }
    TN::TextureLayer & ysmTex()            { return m_ySubMarginalTexture; }    
    TN::Texture2D & getScatterPlotTexture() { return m_scatterPlotTexture; }
    TN::Texture2D & getLassoPointsTexture() { return m_lassoPointsTexture; }

};

}

#endif