
#ifndef TN_RenderGroup3D 
#define TN_RenderGroup3D

#include "configuration/GlyphConfiguration.hpp"

#include "views/Widget.hpp"
#include "views/InputState.hpp"
#include "geometry/Vec.hpp"
#include "views/SpatialView3D.hpp"
#include "render/RenderModule3D/RenderModule.hpp"

#include <vector>
#include <memory>
#include <iostream>
#include <cmath>

namespace TN {

class RenderGroup3D : public Widget 
{
	std::vector< std::unique_ptr< SpatialView3D > > m_spatialViews;

	std::vector< std::unique_ptr< SpatialView3D > > m_views;
	std::vector< size_t > m_viewOrder;
	std::string m_rendererBaseDir;
	std::string m_resourceDir;

	TN::GlyphConfiguration m_defaultGlyphConfig;

    TN::TexturedPressButton m_sideLayoutButton;
    TN::TexturedPressButton m_gridLayoutButton;
    TN::TexturedPressButton m_singularLayoutButton;
    TN::TexturedPressButton m_createViewButton;    
    TN::TexturedPressButton m_removeViewButton;   

    TN::TexturedPressButton m_linkButton;

    std::vector< TN::TexturedPressButton * > m_layoutButtonGroup;

    std::vector< std::vector< Vec2< float > > > m_innerVerticalLines;
    std::vector< std::vector< Vec2< float > > > m_innerHorizontelLines;
    std::vector< std::vector< Vec2< float > > > m_activeViewBorder;

    std::vector< TN::Vec2< float > > m_viewOrderSwitchGlyph;

    int m_activeView;

	static constexpr int TOOL_BAR_HEIGHT = 30;

	enum class Layout { 
		Grid,
		Side,
		Singular
	};

	RenderGroup3D::Layout m_layout; 

	void init() {

		m_activeView = -1;

        m_sideLayoutButton.setTexFromPNG(
            m_resourceDir + "/textures/sideLayout.png",
            m_resourceDir + "/textures/sideLayoutPressed.png" );

        m_gridLayoutButton.setTexFromPNG(
            m_resourceDir + "/textures/gridLayout.png",
            m_resourceDir + "/textures/gridLayoutPressed.png" );

        m_singularLayoutButton.setTexFromPNG(
            m_resourceDir + "/textures/singularLayout.png",
            m_resourceDir + "/textures/singularLayoutPressed.png" );

        m_linkButton.setTexFromPNG(
            m_resourceDir + "/textures/link.png",
            m_resourceDir + "/textures/linkPressed.png" );

        m_createViewButton.setTexFromPNG(
            m_resourceDir + "/textures/create.png",
            m_resourceDir + "/textures/createPressed.png" );

        m_removeViewButton.setTexFromPNG(
            m_resourceDir + "/textures/remove.png",
            m_resourceDir + "/textures/removePressed.png" );

        std::cout << m_resourceDir + "/textures/singularLayoutPressed.png" << std::endl;

        m_layoutButtonGroup =
        {
        	& m_gridLayoutButton,
            & m_sideLayoutButton,
            & m_singularLayoutButton
        };

        for( size_t i = 0; i < m_layoutButtonGroup.size(); ++i )
        {
            TN::TexturedPressButton * button = m_layoutButtonGroup[ i ];
            button->resizeByHeight( 30.f );
        }

        m_linkButton.resizeByHeight( 30.f );
        m_createViewButton.resizeByHeight( 30.f );
        m_removeViewButton.resizeByHeight( 30.f );

		if( m_layout == RenderGroup3D::Layout::Side ) { 
			m_sideLayoutButton.setPressed( true );
		}        
		else if ( m_layout == RenderGroup3D::Layout::Grid ) {
			m_gridLayoutButton.setPressed( true );
		}
		else if ( m_layout == RenderGroup3D::Layout::Singular ) { 
			m_singularLayoutButton.setPressed( true );
		}
	}

	void removeActiveView() {

		// need at least one view for now
		if( m_views.size() > 1 ) {
			m_activeViewBorder.clear();
			if( m_activeView != -1 ) {
				size_t IDX = m_viewOrder[ m_activeView ];
				m_views.erase( m_views.begin() + IDX );
				m_viewOrder.erase( m_viewOrder.begin() + m_activeView );
				for( auto & idx : m_viewOrder ) {
					if( idx > IDX ) {
						--idx;
					}
				}
			}
		}
		m_activeView = -1;
	}

	void constructViewSwitchGlyph ( Vec2< float > pos, std::string orientation ) 
	{
		m_viewOrderSwitchGlyph.clear();

		const float TotWidth = 60.f;
		const float ArrWidth = 15.f;

		if( orientation == "x" ) {
			m_viewOrderSwitchGlyph = {
				{ pos.x() - TotWidth / 2.f           , pos.y() },
				{ pos.x() - TotWidth / 2.f + ArrWidth, pos.y() + ArrWidth / 2.f },
				{ pos.x() - TotWidth / 2.f + ArrWidth, pos.y() - ArrWidth / 2.f },

				{ pos.x() + TotWidth / 2.f           , pos.y() },
				{ pos.x() + TotWidth / 2.f - ArrWidth, pos.y() + ArrWidth / 2.f },
				{ pos.x() + TotWidth / 2.f - ArrWidth, pos.y() - ArrWidth / 2.f },

				{ pos.x() - TotWidth / 2.f + ArrWidth, pos.y() + ArrWidth / 4.f },
				{ pos.x() + TotWidth / 2.f - ArrWidth, pos.y() + ArrWidth / 4.f },
				{ pos.x() - TotWidth / 2.f + ArrWidth, pos.y() - ArrWidth / 4.f },

				{ pos.x() - TotWidth / 2.f + ArrWidth, pos.y() - ArrWidth / 4.f },
				{ pos.x() + TotWidth / 2.f - ArrWidth, pos.y() + ArrWidth / 4.f },
				{ pos.x() + TotWidth / 2.f - ArrWidth, pos.y() - ArrWidth / 4.f }
			};
		} 
		else if ( orientation == "y" ) {
			m_viewOrderSwitchGlyph = {
				{ pos.x()                 , pos.y() - TotWidth / 2.f            },
				{ pos.x() + ArrWidth / 2.f, pos.y() - TotWidth / 2.f + ArrWidth },
				{ pos.x() - ArrWidth / 2.f, pos.y() - TotWidth / 2.f + ArrWidth },

				{ pos.x()                 , pos.y() + TotWidth / 2.f            }, 
				{ pos.x() + ArrWidth / 2.f, pos.y() + TotWidth / 2.f - ArrWidth },
				{ pos.x() - ArrWidth / 2.f, pos.y() + TotWidth / 2.f - ArrWidth },

				{ pos.x() + ArrWidth / 4.f, pos.y() - TotWidth / 2.f + ArrWidth },
				{ pos.x() + ArrWidth / 4.f, pos.y() + TotWidth / 2.f - ArrWidth },
				{ pos.x() - ArrWidth / 4.f, pos.y() - TotWidth / 2.f + ArrWidth },

				{ pos.x() - ArrWidth / 4.f, pos.y() - TotWidth / 2.f + ArrWidth },
				{ pos.x() + ArrWidth / 4.f, pos.y() + TotWidth / 2.f - ArrWidth },
				{ pos.x() - ArrWidth / 4.f, pos.y() + TotWidth / 2.f - ArrWidth }
			};
		}
	}

public: 

	void setDefaultGlyphConfig( const TN::GlyphConfiguration & g )
	{
		m_defaultGlyphConfig = g;
	}

    bool handleInput( 
        const InputState & input )
    {
        Vec2< double > p = input.mouseState.position;
        MouseButtonState mouseButtonState = input.mouseState.buttonState;
        bool changed = false;

        if( pointInViewPort( p ) )
        {
			m_viewOrderSwitchGlyph.clear();

            if( input.mouseState.event == MouseEvent::LeftReleased )
            {

            }

            if( input.mouseState.event == MouseEvent::LeftPressed || input.mouseState.event == MouseEvent::RightPressed  )
            {

            }

            // mouseWheel( -input.mouseState.scrollDelta() / 50.f );

	        for( auto & b : m_layoutButtonGroup )
	        {
	            if( b->pointInViewPort( p ) && input.mouseState.event == MouseEvent::LeftReleased ) {
	            	
	            	b->setPressed( true );
	            	for( auto & b2 : m_layoutButtonGroup ) {
	            		if( b2 != b ) {
	            			b2->setPressed( false );
	            			changed = true;
	            		}
	            	}
	            	break;
	            }
	        }

	        m_layout = m_sideLayoutButton.isPressed()     ? RenderGroup3D::Layout::Side
	        		 : m_gridLayoutButton.isPressed()     ? RenderGroup3D::Layout::Grid
	        		 : m_singularLayoutButton.isPressed() ? RenderGroup3D::Layout::Singular
	        		 : m_layout;
        
	        if( m_linkButton.pointInViewPort( p ) && input.mouseState.event == MouseEvent::LeftReleased  ) {
	        	m_linkButton.setPressed( ! m_linkButton.isPressed() );
	        }

	        if( m_createViewButton.pointInViewPort( p )  ) {
	        	if ( input.mouseState.event == MouseEvent::LeftPressed ) {
	        		addView();
	        		changed = true;
	        	}
	        }

	        if( m_removeViewButton.pointInViewPort( p )  ) {
	        	if ( input.mouseState.event == MouseEvent::LeftPressed ) {
					if( m_activeView != -1 ) {
						removeActiveView();
						changed = true;
					}
	        	}
	        }

	        std::vector< size_t > viewOrderCopy = m_viewOrder;

	        for( size_t i = 0; i < m_viewOrder.size(); ++i ) {


	        	if( m_views[ m_viewOrder[ i ] ]->pointInViewPort( p ) ) {

	        		auto & v = m_views[ m_viewOrder[ i ] ];

	        		if( input.mouseState.event == MouseEvent::LeftPressed 
	        		 || input.mouseState.event == MouseEvent::RightPressed ) {
	        			m_activeView = (int) i;
	        		}
	        		
	        		// make the active view the primary view
	        		// which is the last view in the view order
	        		const int NV = m_views.size();	
					const float THRESHOLD = 10.f;
					
					float minDist = 99999999.f;
					int minIndex = -1;
					Vec2< float > glyphPos;
					std::string glyphOrientation;

					// ( 1 ) determine of mouse is near an edge of hovered view

					float dx = std::min(  
						std::abs( p.x() - ( v->position().x() + v->size().x() ) ),
						std::abs( p.x()- v->position().x()                      )
					);
					
					float dy = std::min(  
						std::abs( p.y() - ( v->position().y() + v->size().y() ) ),
						std::abs( p.y()- v->position().y()                      )
					);

					float dm = std::min( std::abs( dx ), std::abs( dy ) );

					if( dm < THRESHOLD ) {

						if( std::abs( dx ) < std::abs( dy ) ) {

							glyphOrientation = "x";
							glyphPos.y( p.y() );
							glyphPos.x( 
								p.x() < v->position().x() + v->size().x() / 2.0 
									  ? v->position().x() 
									  : v->position().x() + v->size().x() );
						}
						else {
							glyphOrientation = "y";
							glyphPos.x( p.x() );
							glyphPos.y( 
								p.y() < v->position().y() + v->size().y() / 2.0 
									  ? v->position().y() 
									  : v->position().y() + v->size().y() );
						}

						constructViewSwitchGlyph( glyphPos, glyphOrientation );
						bool validSwitch = false;

						for( size_t j = 0; j < m_viewOrder.size(); ++j ) {
							if( j != i ) {
								for( auto & vertex : m_viewOrderSwitchGlyph ) {
									if( m_views[ m_viewOrder[ j ] ]->pointInViewPort( vertex ) ) {
						        		if( input.mouseState.event == MouseEvent::LeftPressed ) {
					        				const size_t TMP = viewOrderCopy[ j ];
					        				viewOrderCopy[ j ] = viewOrderCopy[ i ];
					        				viewOrderCopy[ i ] = TMP;
					        				m_activeView = NV - 1;
					        				changed = true;
						        		}
										validSwitch = true;
										break;
									} 
								}
							}
						}

						if( ! validSwitch ) {
							m_viewOrderSwitchGlyph.clear();
						}
					}

					break;
	        	}
	        }

	        m_viewOrder = viewOrderCopy;
        	computeActiveViewLines();
        }
        else
        {

        }


        return changed;
    }

    void computeActiveViewLines() {
    	m_activeViewBorder.clear();
		if( m_activeView != -1 ) {

			auto & V = m_views[ m_viewOrder[ m_activeView ] ];

			Vec2< float > A( V->position().x(), V->position().y() );
			Vec2< float > B( A.x() + V->size().x(), A.y() );
			Vec2< float > C( B.x(), A.y() + V->size().y() );
			Vec2< float > D( A.x(), C.y() );

			m_activeViewBorder.push_back ( { A, B } );
			m_activeViewBorder.push_back ( { B, C } );
			m_activeViewBorder.push_back ( { C, D } );
			m_activeViewBorder.push_back ( { D, A } );
		}
    }

	virtual void applyLayout() {

		const float BTN_HT = 30.f;
        float leftOffset = position().x();
        float yOffset = position().y() + size().y() - BTN_HT - 2;
        const int NV = m_views.size();
		const double X0 = position().x();
		const double Y0 = position().y();
		const double GW = size().x() - TOOL_BAR_HEIGHT;
		const double GH = size().y();

		float bdy = yOffset - BTN_HT;
		const float bx = position().x() + size().x() - BTN_HT - 2;
        for( size_t i = 0; i < m_layoutButtonGroup.size(); ++i )
        {
            TN::TexturedPressButton * button = m_layoutButtonGroup[ i ];
            button->setPosition( bx, bdy );
            bdy -= BTN_HT;
        }

        m_createViewButton.setPosition( bx, bdy );

		bdy -= BTN_HT;
        m_removeViewButton.setPosition( bx, bdy );

        bdy -= BTN_HT;
        m_linkButton.setPosition( bx, bdy );

        for( auto & v : m_views ) {
        	v->setMinimized( false );
        }

		m_innerVerticalLines.clear();
		m_innerHorizontelLines.clear();

 		if( NV == 0 ) {

 		}
		else if( NV == 1 ) {
			m_views[ 0 ]->setPosition( X0, Y0 );
			m_views[ 0 ]->setSize( GW, GH );	
		} 
		else {

			if( m_layout == RenderGroup3D::Layout::Grid ) 
			{
				int nRows = 1;
				int nCols = 1;

				while( NV > nRows * nCols ) {
					
					double dy = ( GH - 2 ) / (double) nRows;
					double dx = GW / (double) nCols;

					int a = std::abs( ( ( nRows + 1 ) * nCols ) - NV );
					int b = std::abs( ( ( nCols + 1 ) * nRows ) - NV );

					if( a == b ) {
						if( dy > dx ) {
							++nRows;
						}
						else {
							++nCols;
						}
					}
					else if( a > b ) { 
						++nCols;
					}
					else {
						++nRows;
					}
				}

				double VH = GH / nRows;
				double VW = GW / nCols;

				for( int i = 0; i < nRows; ++i )
				for( int j = 0; j < nCols; ++j ) 
				{
					const int IDX = i * nCols + j;

					if( IDX < NV ) {
					    m_views[ m_viewOrder[ IDX ] ]->setPosition( X0 + j * VW, Y0 + i * VH );
					    m_views[ m_viewOrder[ IDX ] ]->setSize( VW, VH );				
					}
				}

				for( int j = 1; j < nCols; ++j ) {
					for( int i = 0; i < nRows; ++i ) {
						m_innerVerticalLines.push_back( {
							{ X0 + j * VW, Y0 + i * VH },
							{ X0 + j * VW, Y0 + i * VH + RenderGroup3D::TOOL_BAR_HEIGHT }
						} );	
					}
				}

				for( int j = 1; j < nCols; ++j ) {
					m_innerVerticalLines.push_back( {
						{ X0 + j * VW, Y0 + RenderGroup3D::TOOL_BAR_HEIGHT },
						{ X0 + j * VW, Y0 + GH - 1 }
					} );	
				}
			}
			else if( m_layout == RenderGroup3D::Layout::Side )
			{
				const double SIDE_WIDTH = ( GH ) / (float) ( NV - 1.0 ); 
				const double VW = ( GW - SIDE_WIDTH );

				for( int i = 0; i < NV-1; ++i )
				{
				    m_views[ m_viewOrder[ i ] ]->setSize(     SIDE_WIDTH, SIDE_WIDTH );
				    m_views[ m_viewOrder[ i ] ]->setPosition( X0 + VW, Y0 + i * SIDE_WIDTH );
				}

				m_views[ m_viewOrder[ NV-1 ] ]->setSize(     VW, GH );
				m_views[ m_viewOrder[ NV-1 ] ]->setPosition( X0, Y0 );

				m_innerVerticalLines.push_back( {
					{ X0 + VW, Y0 },
					{ X0 + VW, Y0 + RenderGroup3D::TOOL_BAR_HEIGHT }
				} );

				m_innerVerticalLines.push_back( {
					{ X0 + VW, Y0 + RenderGroup3D::TOOL_BAR_HEIGHT },
					{ X0 + VW, Y0 + GH - 1 }
				} );	
			}
			else if( m_layout == RenderGroup3D::Layout::Singular )
			{
				const double SIDE_WIDTH = 80; 
				const double VW = ( GW - SIDE_WIDTH );

				for( int i = 0; i < NV-1; ++i )
				{
				    m_views[ m_viewOrder[ i ] ]->setMinimized( true );
				    m_views[ m_viewOrder[ i ] ]->setSize(     SIDE_WIDTH, SIDE_WIDTH );
				    m_views[ m_viewOrder[ i ] ]->setPosition( X0 + VW, Y0 + GH - ( i + 1 ) * SIDE_WIDTH );

					m_innerHorizontelLines.push_back( {
						{ X0 + VW,              Y0 + GH - ( i + 1 ) * SIDE_WIDTH },
						{ X0 + VW + SIDE_WIDTH, Y0 + GH - ( i + 1 ) * SIDE_WIDTH }
					} );	
				}

				m_views[ m_viewOrder[ NV-1 ] ]->setSize(     VW, GH );
				m_views[ m_viewOrder[ NV-1 ] ]->setPosition( X0, Y0 );

				m_innerVerticalLines.push_back( {
					{ X0 + VW, Y0 },
					{ X0 + VW, Y0 + RenderGroup3D::TOOL_BAR_HEIGHT }
				} );

				m_innerVerticalLines.push_back( {
					{ X0 + VW, Y0 + RenderGroup3D::TOOL_BAR_HEIGHT },
					{ X0 + VW, Y0 + GH - 1 }
				} );	
			}
		}

		computeActiveViewLines();
	}

	virtual void setSize( float x, float y ) override
	{
		Widget::setSize( x, y );
	}

	RenderGroup3D( const std::string & resourceDir, const std::string & rendererDir  ) 
		: Widget(), 
		  m_resourceDir( resourceDir ),
		  m_rendererBaseDir( rendererDir ),
		  m_layout( RenderGroup3D::Layout::Side ) {
		  	init();
		  }

	RenderGroup3D( const std::string & resourceDir, const std::string & rendererDir , int nViews )
		: Widget(),
		  m_resourceDir( resourceDir ),
		  m_rendererBaseDir( rendererDir ),
		  m_layout( RenderGroup3D::Layout::Grid )
	{
		for( int i = 0; i < nViews; ++i ) 
		{
			addView( SpatialViewConfiguration() );
		}

		init();
	}

	void addView( const SpatialViewConfiguration & config ) {

		m_viewOrder.push_back( m_viewOrder.size() );
		m_views.push_back( std::unique_ptr< SpatialView3D >( new SpatialView3D( config ) ) );
		m_views.back()->configuration().glyphConfiguration = m_defaultGlyphConfig;
	}

	void addView() {
		addView( SpatialViewConfiguration() );
	}

	void clear() {
    	m_activeView = -1;
		m_views.clear();
		m_viewOrder.clear();
	}

	void setAllViewsNeedUpdates()
	{
		for( auto & v : m_views )
		{
			v->setNeedsUpdate();
		}
	}

    virtual ~RenderGroup3D() {}

    std::vector< std::unique_ptr< SpatialView3D > > & views() { return m_views; } 
    std::vector< TN::TexturedPressButton * > buttons() { 

    	std::vector< TN::TexturedPressButton * > bts = m_layoutButtonGroup;
    	bts.push_back( &m_linkButton );
    	bts.push_back( &m_createViewButton );
    	bts.push_back( &m_removeViewButton );

    	return bts; 
    }

    bool linked() {
    	return m_linkButton.isPressed();
    }

    std::vector< std::vector< Vec2< float > > > getHorizontelLinesOfSeparation() {
    	 
    	std::vector< std::vector< Vec2< float > > >  lines = { 
    		{
    			Vec2< float >( position().x(),              position().y() ),
    			Vec2< float >( position().x() + size().x(), position().y() )
    		},
    		{ 
    			Vec2< float >( position().x(),              position().y() + size().y() ),
    			Vec2< float >( position().x() + size().x(), position().y() + size().y() )
    		}
    	};

    	for( auto & l : m_innerHorizontelLines ) {
    		lines.push_back( l );
    	}

    	return lines;
    }

    std::vector< std::vector< Vec2< float > > > getVerticalLinesOfSeparation() {
    	 
    	std::vector< std::vector< Vec2< float > > >  lines;
    	for( auto & l : m_innerVerticalLines ) {
    		lines.push_back( l );
    	}
    	return lines;
    }

    std::vector< std::vector< Vec2< float > > > getActiveViewBorder() {
    	return m_activeViewBorder;
    }

    std::vector< Vec2< float > > getViewSwitchGlyph() {
    	return m_viewOrderSwitchGlyph;
    }

    void deselect() {
    	m_activeView = -1;
    	m_activeViewBorder.clear();
    }

    int activePlot() const {
    	return m_activeView;
    }

    void updateInformation( const TN::SurfaceSetCollection & ssc, int nLayers ) 
    {
    	for( auto & v : m_views )
    	{
    		v->updateInformation( ssc, nLayers );
    	}
    }

    TN::SpatialView3D * activeView() 
    {
    	if( m_activeView != -1 )
    	{
    		return m_views[ m_viewOrder[ m_activeView ] ].get();
    	}
    	else
    	{
    		return NULL;
    	}
    }
};

} // end namespace TN

#endif