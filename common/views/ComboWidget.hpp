#ifndef COMBOWIDGET_HPP
#define COMBOWIDGET_HPP

#include "views/Widget.hpp"
#include "geometry/Vec.hpp"
#include "views/PressButtonWidget.hpp"

#include <vector>
#include <unordered_set>
#include <string>
#include <iostream>
#include <algorithm>
#include <iterator>

namespace TN
{

class ComboWidget : public Widget
{
    int m_id;
    Vec3< float > m_color;
    Vec3< float > m_flashColor;

    std::vector< std::string > m_items;
    std::unordered_set< std::string > m_itemSet;
    std::map< std::string, std::unique_ptr< TN::TexturedPressButton > > m_itemTextures;

    int m_selectedItemId;
    int m_hoverItemId;
    bool m_isPressed;
    bool m_flash;
    double m_flashPhase;
    std::string m_text;

public:

    int textX() const
    {
        return position().x() - m_text.size()*8 - 4;
    }

    int textY() const
    {
        return this->position().y() + 10;
    }

    void text( const std::string & txt )
    {
        m_text = txt;
    }

    const std::string & text() const
    {
        return m_text;
    }

    virtual void resize( float width, float height )
    {
        this->setSize( width, height );
    }

    ComboWidget() : Widget(), m_selectedItemId( -1 ), m_hoverItemId( -1 ), m_isPressed( false ), m_flash( false ), m_flashPhase(0), m_flashColor( 1, .7, .6 ), m_text("") {}

    ComboWidget( const Vec3< float > & color, int width, int height, int id ) :
        Widget(),
        m_id( id ), m_color( color ), m_selectedItemId( -1 ),
        m_hoverItemId( -1 ), m_isPressed( false ), m_flash( false ), m_flashPhase( 0 ), m_flashColor( 1, .7, .6 ), m_text("")
    {
        resize( width, height );
    }

    int addItem( const std::string & item )
    {
        if( m_selectedItemId == -1 )
        {
            m_selectedItemId = 0;
        }
        int itemIdx = m_items.size();
        m_items.push_back( item );
        m_itemSet.insert( item );

        return itemIdx;
    }

    int addItem( const std::string & item, const std::string & texturePath )
    {
        if( m_selectedItemId == -1 )
        {
            m_selectedItemId = 0;
        }

        int itemIdx = m_items.size();
        m_items.push_back( item );
        m_itemSet.insert( item );
        m_itemTextures.insert( { item, std::unique_ptr< TN::TexturedPressButton >(  new TN::TexturedPressButton() ) } );
        m_itemTextures.at( item )->setTexFromPNG( texturePath, texturePath );
        m_itemTextures.at( item )->resizeByHeight( 22 );

        return itemIdx;
    }

    void setKeys( const std::vector< std::string > & keys )
    {
        clear();
        for( const auto & key : keys )
        {
            addItem( key );
        }
    }

    void setKeys( const std::vector< std::string > & keys, const std::map< std::string, std::string > & texturePaths )
    {
        auto tmp =selectedItemText();
    
        clear();
        for( size_t i = 0, end = keys.size(); i < end; ++i ) 
        {
            if( texturePaths.count( keys[ i ] ) )
            {
                addItem( keys[ i ], texturePaths.at( keys[ i ] ) );
            }
            else 
            {
                addItem( keys[ i ] );
            }
        }

        selectByText( tmp );
    }

    void addKeys( const std::vector< std::string > & keys, const std::map< std::string, std::string > & texturePaths )
    {
        auto tmp =selectedItemText();

        for( size_t i = 0, end = keys.size(); i < end; ++i ) 
        {
            if( ! m_itemSet.count( keys[ i ] ) ) 
            {
                if( texturePaths.count( keys[ i ] ) )
                {
                    addItem( keys[ i ], texturePaths.at( keys[ i ] ) );
                }
                else 
                {
                    addItem( keys[ i ] );
                }
            }
        }
    
        selectByText( tmp );
    }

    void removeKeys( const std::vector< std::string > & keys )
    {
        auto tmp =selectedItemText();

        //////// items = existing items minus items to remove ///////////////////////

        std::vector< std::string > diff;

        TN::set_difference( m_items.begin(), m_items.end(), keys.begin(), keys.end(),
            std::inserter( diff, diff.begin() ) );

        m_items = diff;

        for ( const auto & k : keys ) 
        {
            if( m_itemSet.count( k ) )
            {
                m_itemSet.erase( k );
            }

            if( m_itemTextures.count( k ) ) 
            {
                m_itemTextures.erase( k );
            }
        }

        /////////////////////////////////////////////////////////////////////////////

        selectByText( tmp );
    }

    void updateKeys( const std::vector< std::string > & keys, const std::map< std::string, std::string > & texturePaths )
    {
        auto tmp =selectedItemText();

        // keys to remove ////////////////////////////////////////////////////////////

        std::vector< std::string > toRemove;
        TN::set_intersection( m_items.begin(), m_items.end(), keys.begin(), keys.end(),
            std::inserter( toRemove, toRemove.begin() ) );

        removeKeys( toRemove );

        // keys to add ////////////////////////////////////////////////////////////////

        std::vector< std::string > toAdd;
        TN::set_difference( keys.begin(), keys.end(), m_items.begin(), m_items.end(),
            std::inserter( toAdd, toAdd.begin() ) );

        addKeys( toAdd, texturePaths );

        ///////////////////////////////////////////////////////////////////////////////

        selectByText( tmp );                
    }

    void selectItem( int id )
    {
        m_selectedItemId = id;
    }

    std::string selectedItemText() const
    {
        if( m_items.size() && m_selectedItemId >= 0 )
        {
            return m_items[ m_selectedItemId ];
        }
        return "";
    }

    void clear()
    {
        m_selectedItemId = -1;
        m_items.clear();
        m_itemSet.clear();
        m_itemTextures.clear();
    }

    const Vec3< float > & color() const
    {
        return m_color;
    }

    bool isPressed() const
    {
        return m_isPressed;
    }

    int numItems() const
    {
        return m_items.size();
    }

    const std::vector< std::string > & items() const
    {
        return m_items;
    }

    const std::map< std::string, std::unique_ptr< TN::TexturedPressButton > > & textureButtons () const{
        return m_itemTextures;
    }

    int selectedItem() const
    {
        return m_selectedItemId;
    }

    int hoveredItem() const
    {
        return m_hoverItemId;
    }

    int id() const
    {
        return m_id;
    }

    void setPressed( bool pressed )
    {
        m_isPressed = pressed;
    }

    int mousedOver( const Vec2< float > & p )
    {
        int id = -1;

        if( p.x() >= position().x() && p.x() <= position().x() + size().x() && p.y() <= position().y() + size().y() )
        {
            for( unsigned i = 1, end = m_items.size()+1; i < end; ++i )
            {
                if( p.y() >= position().y() - i*size().y() )
                {
                    id = i-1;
                    break;
                }
            }
        }
        m_hoverItemId = id;
        return id;
    }

    int pressed( const Vec2< float > & p )
    {
        int id = -1;

        if( p.x() >= position().x() && p.x() <= position().x() + size().x() && p.y() <= position().y() + size().y() )
        {
            for( unsigned i = 1, end = m_items.size()+1; i < end; ++i )
            {
                if( p.y() >= position().y() - i*size().y() )
                {
                    id = i-1;
                    break;
                }
            }
        }

        m_selectedItemId = id;
        return id;
    }

    std::vector< std::pair< TN::Vec2< float >, TN::Vec2< float >  > > outline() const
    {
        return {
            { 
                { position().x(), position().y() + size().y() }, { position().x() + size().x(), position().y() + size().y() }
            },
            { 
                { position().x(), position().y() + size().y() }, { position().x(), position().y() - m_items.size()*size().y() }
            },
            {
                { position().x() + size().x(), position().y() + size().y() }, { position().x() + size().x(), position().y() - m_items.size()*size().y() }
            },
            {
                { position().x() + size().x(), position().y() - m_items.size()*size().y()  }, { position().x(), position().y() - m_items.size()*size().y() }
            }
        };
    }

    virtual bool pointInViewPort( const Vec2< float > & p )
    {
        if( m_isPressed )
        {
            bool is = false;
            if( p.x() >= position().x() && p.x() <= position().x() + size().x() && p.y() <= position().y() + size().y() )
            {
                for( unsigned i = 1, end = m_items.size()+1; i < end; ++i )
                {
                    if( p.y() >= position().y() - i*size().y() )
                    {
                        is = true;
                        break;
                    }
                }
            }
            return is;
        }
        else
        {
            return Widget::pointInViewPort( p );
        }
    }

    void selectByText( const std::string & txt )
    {
        if( numItems() < 2 )
        {
            return;
        }

        bool found = false;
        int idx = 0;
        for( int i = 0; i < numItems(); ++i )
        {
            if( items()[ i ] == txt )
            {
                idx = i;
                found = true;
                break;
            }
        }
        if( found )
        {
            selectItem( idx );
        }
        else
        {
            m_selectedItemId = -1;
        }
    }

    void flashColor( const Vec3< float > & cl )
    {
        m_flashColor = cl;
    }
    const Vec3< float > &  flashColor() const
    {
        return m_flashColor;
    }

    bool flash() const
    {
        return m_flash;
    }
    void flash( bool b )
    {
        m_flash = b;
    }

    double flashPhase() const
    {
        return m_flashPhase;
    }
    void flashPhase( double phase )
    {
        m_flashPhase = phase;
    }
};

}


#endif // COMBOWIDGET_HPP