
#ifndef TN_PLOT_GROUP_HPP 
#define TN_PLOT_GROUP_HPP

#include "views/Widget.hpp"
#include "views/InputState.hpp"
#include "geometry/Vec.hpp"
#include "views/JointPlot2D.hpp"

#include <vector>
#include <memory>
#include <iostream>
#include <cmath>

namespace TN {


class PlotGroup2D : public Widget 
{
	std::vector< std::unique_ptr< TN::JointPlot2D > > m_views;
	std::vector< size_t > m_viewOrder;

	std::string m_workingDir;
	std::string m_projectDirectory;
	TN::Widget m_resizeHandle;
	
	float m_minHeight;
	float m_midHeight;
	bool m_resizeHovered;
	bool m_resizePressed;

    TN::TexturedPressButton m_createViewButton;    
    TN::TexturedPressButton m_removeViewButton;   
    TN::TexturedPressButton m_squareButton;   

    std::vector< TN::TexturedPressButton * > m_layoutButtonGroup;

    std::vector< std::vector< Vec2< float > > > m_innerVerticalLines;
    std::vector< std::vector< Vec2< float > > > m_innerHorizontelLines;
    std::vector< std::vector< Vec2< float > > > m_activeViewBorder;

    std::vector< TN::Vec2< float > > m_viewOrderSwitchGlyph;

    int m_activeView;

	enum class Layout { 
		Auto
	};

	PlotGroup2D::Layout m_layout; 

	void init() {

		m_activeView = -1;

        m_createViewButton.setTexFromPNG(
            m_workingDir + "/textures/create.png",
            m_workingDir + "/textures/createPressed.png" );

        m_removeViewButton.setTexFromPNG(
            m_workingDir + "/textures/remove.png",
            m_workingDir + "/textures/removePressed.png" );

        m_squareButton.setTexFromPNG(
            m_workingDir + "/textures/square.png",
            m_workingDir + "/textures/square.png" );

        m_squareButton.setPressed( true );

        m_createViewButton.resizeByHeight( 30 );
        m_removeViewButton.resizeByHeight( 30 );
        m_squareButton.resizeByHeight(     30 );

        setSize( 500, m_midHeight );

        m_resizeHovered = false;
        m_resizePressed = false;
    }

	void removeActiveView() {

		// need at least one view for now
		if( m_views.size() > 1 ) {
			m_activeViewBorder.clear();
			if( m_activeView != -1 ) {
				size_t IDX = m_viewOrder[ m_activeView ];
				m_views.erase( m_views.begin() + IDX );
				m_viewOrder.erase( m_viewOrder.begin() + m_activeView );
				for( auto & idx : m_viewOrder ) {
					if( idx > IDX ) {
						--idx;
					}
				}
			}
		}

		m_activeView = -1;

	}

	void constructViewSwitchGlyph ( Vec2< float > pos, std::string orientation ) 
	{
		m_viewOrderSwitchGlyph.clear();

		const float TotWidth = 60.f;
		const float ArrWidth = 15.f;

		if( orientation == "x" ) {
			m_viewOrderSwitchGlyph = {
				{ pos.x() - TotWidth / 2.f           , pos.y() },
				{ pos.x() - TotWidth / 2.f + ArrWidth, pos.y() + ArrWidth / 2.f },
				{ pos.x() - TotWidth / 2.f + ArrWidth, pos.y() - ArrWidth / 2.f },

				{ pos.x() + TotWidth / 2.f           , pos.y() },
				{ pos.x() + TotWidth / 2.f - ArrWidth, pos.y() + ArrWidth / 2.f },
				{ pos.x() + TotWidth / 2.f - ArrWidth, pos.y() - ArrWidth / 2.f },

				{ pos.x() - TotWidth / 2.f + ArrWidth, pos.y() + ArrWidth / 4.f },
				{ pos.x() + TotWidth / 2.f - ArrWidth, pos.y() + ArrWidth / 4.f },
				{ pos.x() - TotWidth / 2.f + ArrWidth, pos.y() - ArrWidth / 4.f },

				{ pos.x() - TotWidth / 2.f + ArrWidth, pos.y() - ArrWidth / 4.f },
				{ pos.x() + TotWidth / 2.f - ArrWidth, pos.y() + ArrWidth / 4.f },
				{ pos.x() + TotWidth / 2.f - ArrWidth, pos.y() - ArrWidth / 4.f }
			};
		} 
		else if ( orientation == "y" ) {
			m_viewOrderSwitchGlyph = {
				{ pos.x()                 , pos.y() - TotWidth / 2.f            },
				{ pos.x() + ArrWidth / 2.f, pos.y() - TotWidth / 2.f + ArrWidth },
				{ pos.x() - ArrWidth / 2.f, pos.y() - TotWidth / 2.f + ArrWidth },

				{ pos.x()                 , pos.y() + TotWidth / 2.f            }, 
				{ pos.x() + ArrWidth / 2.f, pos.y() + TotWidth / 2.f - ArrWidth },
				{ pos.x() - ArrWidth / 2.f, pos.y() + TotWidth / 2.f - ArrWidth },

				{ pos.x() + ArrWidth / 4.f, pos.y() - TotWidth / 2.f + ArrWidth },
				{ pos.x() + ArrWidth / 4.f, pos.y() + TotWidth / 2.f - ArrWidth },
				{ pos.x() - ArrWidth / 4.f, pos.y() - TotWidth / 2.f + ArrWidth },

				{ pos.x() - ArrWidth / 4.f, pos.y() - TotWidth / 2.f + ArrWidth },
				{ pos.x() + ArrWidth / 4.f, pos.y() + TotWidth / 2.f - ArrWidth },
				{ pos.x() - ArrWidth / 4.f, pos.y() + TotWidth / 2.f - ArrWidth }
			};
		}
	}

public: 

	static constexpr int TOOL_BAR_HEIGHT = 34;

    bool handleInput( 
        const InputState & input )
    {
        Vec2< double > p = input.mouseState.position;
        MouseButtonState mouseButtonState = input.mouseState.buttonState;
        bool changed = false;

        /////////////////////////////////////////////////////////////////////////////////////////////////////
        // resize handle

        if( m_resizeHandle.pointInViewPort( p ) ) {
            m_resizeHovered = true;
            if( input.mouseState.event == MouseEvent::LeftPressed && m_resizePressed == false ) {
                m_resizePressed = true;
            }
        } 
        else {
            m_resizeHovered = false;
        }

        if( m_resizePressed ) {
            float newHeight = ( p.y() - position().y() ) + 5.f;
            m_midHeight = std::max(newHeight, m_minHeight ); 
            setSize( size().x(), m_midHeight );  
            m_resizeHandle.setPosition( 
                position().x(),
                position().y() + m_midHeight - 10 );
            changed = true;
        }

        if( m_resizePressed && input.mouseState.event == MouseEvent::LeftReleased )
        {
            m_resizePressed = false;
        }

        if( m_squareButton.pointInViewPort( p ) && input.mouseState.event == MouseEvent::LeftPressed ) 
        {
        	m_squareButton.setPressed( ! m_squareButton.isPressed() );
        	changed = true;
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////////

        if( pointInViewPort( p ) )
        {
			m_viewOrderSwitchGlyph.clear();

            if( input.mouseState.event == MouseEvent::LeftReleased )
            {

            }

            if( input.mouseState.event == MouseEvent::LeftPressed || input.mouseState.event == MouseEvent::RightPressed  )
            {

            }

            // mouseWheel( -input.mouseState.scrollDelta() / 50.f );

	        if( m_createViewButton.pointInViewPort( p )  ) {
	        	if ( input.mouseState.event == MouseEvent::LeftPressed ) {
					auto & v = addView();
					changed = true;

	        	}
	        }

	        if( m_removeViewButton.pointInViewPort( p )  ) {
	        	if ( input.mouseState.event == MouseEvent::LeftPressed ) {
					if( m_activeView != -1 ) {
						removeActiveView();
						changed = true;
					}
	        	}
	        }

	        std::vector< size_t > viewOrderCopy = m_viewOrder;

	        for( size_t i = 0; i < m_viewOrder.size(); ++i ) {
	        	if( m_views[ m_viewOrder[ i ] ]->container().pointInViewPort( p ) ) {

	        		bool viewChanged = m_views[ m_viewOrder[ i ] ]->handleInput( input );

	        		auto & vc = m_views[ m_viewOrder[ i ] ]->container();

	        		if( input.mouseState.event == MouseEvent::LeftPressed 
	        		 || input.mouseState.event == MouseEvent::RightPressed ) {
	        			m_activeView = (int) i;
	        		}
	        		
	        		// make the active view the primary view
	        		// which is the last view in the view order
	        		const int NV = m_views.size();	
					const float THRESHOLD = 10.f;
					
					float minDist = 99999999.f;
					int minIndex = -1;
					Vec2< float > glyphPos;
					std::string glyphOrientation;

					// ( 1 ) determine of mouse is near an edge of hovered view

					float dx = std::min(  
						std::abs( p.x() - ( vc.position().x() + vc.size().x() ) ),
						std::abs( p.x()- vc.position().x()                      )
					);
					
					float dy = std::min(  
						std::abs( p.y() - ( vc.position().y() + vc.size().y() ) ),
						std::abs( p.y()- vc.position().y()                      )
					);

					float dm = std::min( std::abs( dx ), std::abs( dy ) );

					if( dm < THRESHOLD ) {

						if( std::abs( dx ) < std::abs( dy ) ) {

							glyphOrientation = "x";
							glyphPos.y( p.y() );
							glyphPos.x( 
								p.x() < vc.position().x() + vc.size().x() / 2.0 
									  ? vc.position().x() 
									  : vc.position().x() + vc.size().x() );
						}
						else {
							glyphOrientation = "y";
							glyphPos.x( p.x() );
							glyphPos.y( 
								p.y() < vc.position().y() + vc.size().y() / 2.0 
									  ? vc.position().y() 
									  : vc.position().y() + vc.size().y() );
						}

						constructViewSwitchGlyph( glyphPos, glyphOrientation );
						bool validSwitch = false;

						for( size_t j = 0; j < m_viewOrder.size(); ++j ) {
							if( j != i ) {
								for( auto & vertex : m_viewOrderSwitchGlyph ) {
									if( m_views[ m_viewOrder[ j ] ]->container().pointInViewPort( vertex ) ) {
						        		if( input.mouseState.event == MouseEvent::LeftPressed ) {
					        				const size_t TMP = viewOrderCopy[ j ];
					        				viewOrderCopy[ j ] = viewOrderCopy[ i ];
					        				viewOrderCopy[ i ] = TMP;
					        				m_activeView = NV - 1;
					        				changed = true;
						        		}
										validSwitch = true;
										break;
									} 
								}
							}
						}

						if( ! validSwitch ) {
							m_viewOrderSwitchGlyph.clear();
						}
					}

					break;
	        	}
	        }

	        m_viewOrder = viewOrderCopy;
        	computeActiveViewLines();
        }
        else
        {

        }


        return changed;
    }

    void computeActiveViewLines() {
    	m_activeViewBorder.clear();
		if( m_activeView != -1 ) {

			auto & V = m_views[ m_viewOrder[ m_activeView ] ];

			Vec2< float > A( V->containerPosition().x(), V->containerPosition().y() );
			Vec2< float > B( A.x() + V->containerSize().x(), A.y() );
			Vec2< float > C( B.x(), A.y() + V->containerSize().y() );
			Vec2< float > D( A.x(), C.y() );

			m_activeViewBorder.push_back ( { A, B } );
			m_activeViewBorder.push_back ( { B, C } );
			m_activeViewBorder.push_back ( { C, D } );
			m_activeViewBorder.push_back ( { D, A } );
		}
    }

	virtual void applyLayout() {

        m_resizeHandle.setSize( size().x(), 10 );
        m_resizeHandle.setPosition( position().x(), position().y() + size().y() - 10 );

        m_createViewButton.setPosition( position().x() + size().x() - 34, position().y() + size().y() - 34 - 10 );
        m_removeViewButton.setPosition( m_createViewButton.position().x(), m_createViewButton.position().y() - 30 );
        m_squareButton.setPosition( m_removeViewButton.position().x(), m_removeViewButton.position().y() - 30 );

        const double LeftMargin   = 57;
        const double RightMargin  = 76;
        const double BottomMargin = 90;
        const double TopMargin    = 40;

        auto & plotViews = views();

        const int NPLOTS = plotViews.size();

        if( plotViews.size() == 0 )
        {
        	return;
        }

        // optimal number of rows?

        float maxSqaure   = 0;
        int   optimalRows = 1;
        const int MAX_ROWS = std::min( NPLOTS, 8 );
        for( int i = 1; i < MAX_ROWS; ++i ) 
        {
        	const int NC = std::ceil( NPLOTS / ( double ) i );

			double pw  =  size().x()          / (double) NC;
			double ph = ( size().y() - 11.f ) / ( double ) i; 
			double ps = std::min( pw, ph );

			if( ps > maxSqaure ) {
				maxSqaure = ps;
				optimalRows = i;
			}
        }

       	const int NROWS  = optimalRows;
        const int NCOLS  =  std::ceil( NPLOTS / ( double ) NROWS );

        bool square = m_squareButton.isPressed();

		double plotWidth  = ( size().x() - 34 ) / ( double ) NCOLS;
		double plotHeight = ( size().y() - 11 ) / ( double ) NROWS;

		if( square ) {
			plotWidth  = std::min( plotWidth, plotHeight );
			plotHeight = plotWidth;
		}

		double plotViewxOffset = position().x();

		double gridWidth  = plotWidth  - (   LeftMargin + RightMargin + 34 );
		double gridHeight = plotHeight - ( BottomMargin + TopMargin   + 34 );

		if( square ) {
			gridWidth = std::min( gridWidth, gridHeight );
			gridHeight = gridWidth;
		}

        int plotIdx = 0;
        for( int i = 0; i < plotViews.size(); ++i, ++plotIdx )
        {
            int currRow = plotIdx / NCOLS;
            int currCol = plotIdx - currRow * NCOLS;

            double px = currCol * plotWidth;
            double py = currRow * plotHeight;

            plotViews[ m_viewOrder[ i ] ]->setContainerSize( plotWidth, plotHeight );
            plotViews[ m_viewOrder[ i ] ]->setContainerPosition( plotViewxOffset + px, py );            

            plotViews[ m_viewOrder[ i ] ]->setSize( gridWidth, gridHeight );
            plotViews[ m_viewOrder[ i ] ]->setPosition( plotViewxOffset + px + LeftMargin, py + BottomMargin );
        }

		computeActiveViewLines();
	}

	virtual void setSize( float x, float y ) override
	{
		Widget::setSize( x, y );
	}

    virtual void resize( float width, float height )
    {
        setSize( width, height );
        applyLayout();
    }

	PlotGroup2D( const std::string & resourceDir, const std::string & projectDirectory  ) 
		: Widget(), 
		  m_workingDir( resourceDir ),
		  m_projectDirectory( projectDirectory ),
		  m_layout( PlotGroup2D::Layout::Auto ),
		  m_minHeight( 400.f ),
		  m_midHeight( 500.f ) 
		  {
		  	init();
		  }

	PlotGroup2D( const std::string & resourceDir, const std::string & projectDirectory , int nViews )
		: Widget(),
		  m_workingDir( resourceDir ),
		  m_projectDirectory( projectDirectory ),
		  m_layout( PlotGroup2D::Layout::Auto ),
		  m_minHeight( 400.f ),
		  m_midHeight( 500.f ) 
	{
		for( int i = 0; i < nViews; ++i ) 
		{
			auto & v = addView();
		}

		init();
	}

	std::unique_ptr< TN::JointPlot2D > & addView() {
		m_viewOrder.push_back( m_viewOrder.size() );
		m_views.push_back( std::unique_ptr< TN::JointPlot2D >( new TN::JointPlot2D( m_workingDir, m_projectDirectory ) ) );
		return m_views.back();
	}


    virtual ~PlotGroup2D() {}

    std::vector< std::unique_ptr< TN::JointPlot2D > > & views() { return m_views; } 

    TN::JointPlot2D * activeView() 
    {
    	if( m_activeView != -1 )
    	{
    		return m_views[ m_viewOrder[ m_activeView ] ].get();
    	}
    	else
    	{
    		return NULL;
    	}
    }

    std::vector< TN::TexturedPressButton * > buttons() { 
    	std::vector< TN::TexturedPressButton * > bts = m_layoutButtonGroup;
    	bts.push_back( &m_createViewButton );
    	bts.push_back( &m_removeViewButton );
    	bts.push_back( & m_squareButton    );
    	return bts; 
    }

    std::vector< std::vector< Vec2< float > > > getHorizontelLinesOfSeparation() const {
    	 
    	std::vector< std::vector< Vec2< float > > >  lines = { 
    		{
    			Vec2< float >( position().x(),              position().y() ),
    			Vec2< float >( position().x() + size().x(), position().y() )
    		},
    		{ 
    			Vec2< float >( position().x(),              position().y() + size().y() ),
    			Vec2< float >( position().x() + size().x(), position().y() + size().y() )
    		}
    	};

    	for( auto & l : m_innerHorizontelLines ) {
    		lines.push_back( l );
    	}

    	return lines;
    }

    std::vector< std::vector< Vec2< float > > > getVerticalLinesOfSeparation() const {
    	 
    	std::vector< std::vector< Vec2< float > > >  lines;
    	for( auto & l : m_innerVerticalLines ) {
    		lines.push_back( l );
    	}
    	return lines;
    }

    void setProjectDirectory( const std::string & d ) { 
    	m_projectDirectory = d; 
    }

    int activePlot() const {
    	return m_activeView;
    }

    void deselect() {
    	m_activeView = -1;
    	m_activeViewBorder.clear();
    }

    std::vector< std::vector< Vec2< float > > > getActiveViewBorder() const {
    	return m_activeViewBorder;
    }

    std::vector< Vec2< float > > getViewSwitchGlyph() const {
    	return m_viewOrderSwitchGlyph;
    }

    bool resizeHovered() const {
        return m_resizeHovered;
    }

    bool resizing() const {
        return m_resizePressed;
    }

    float midHeight() const {
    	return m_midHeight;
    }

    void clear() {
		m_activeView = -1;
		m_views.clear();
		m_viewOrder.clear();
    }

};

} // end namespace TN

#endif