#ifndef TN_SPATIAL_VIEW_3D_HPP
#define TN_SPATIAL_VIEW_3D_HPP

#include "configuration/SpatialViewConfiguration.hpp"
#include "views/InputState.hpp"
#include "render/Camera/TrackBallCamera.hpp"
#include "views/Widget.hpp"
#include "geometry/Vec.hpp"

#include <glm/glm.hpp>
#include <glm/gtx/transform.hpp>

#include <atomic>

namespace TN
{

class SpatialView3D : public TN::Widget
{
    static std::atomic< int > nextId;

    static constexpr int TOOL_BAR_HEIGHT = 30;
    TN::SpatialViewConfiguration m_config;
    TN::TrackBallCamera m_camera;
    bool m_minimized;
    TN::Widget m_renderFrame;
    bool m_needsUpdate;
    std::string m_uniqueId;

public :

    void setMinimized( bool c ) {
        m_minimized = c;
    } 

    bool isMinimized() {
         return m_minimized;
    }

    void initialize() {

    }

    void pan( TN::Vec2< float > delta, MouseButtonState buttonState )
    {
        const float RW = m_renderFrame.size().x();
        if( buttonState == MouseButtonState::LeftPressed )
        {
            m_camera.orbit( glm::vec2( -delta.x() * 0.04 / RW, -delta.y() * 0.04 / RW ) );
        }
        else if( buttonState == MouseButtonState::RightPressed  )
        {
            m_camera.track( glm::vec2( -delta.x() / RW, -delta.y() / RW ) );
        }
    }

    void zoom( int dz )
    {
        m_camera.zoom( -dz / 3.0 );
    }

    virtual bool handleInput( const InputState & input ) override
    {
        bool changed = false;
        if( pointInViewPort( 
            TN::Vec2< float >( 
                input.mouseState.position.x(), 
                input.mouseState.position.y() ) ) )
        {
            if( std::abs( input.mouseState.scrollDelta() ) > 0 )
            {
                zoom( input.mouseState.scrollDelta() );
                changed = true;
            }
            if( ( std::abs( input.mouseState.positionDelta.x() ) > 0 || std::abs( input.mouseState.positionDelta.y() ) > 0 )
             && ( input.mouseState.buttonState == MouseButtonState::LeftPressed 
                  || input.mouseState.buttonState == MouseButtonState::RightPressed ) )
            {
                pan(  input.mouseState.positionDelta*-1.0, input.mouseState.buttonState );
                changed = true;
            }
        }

        if( changed )
        {
            m_needsUpdate = true;
        }

        return changed;
    }

    virtual void setSize( float x, float y ) override {
        
       if( size() != TN::Vec2< float >( x, y ) )
       {
           m_needsUpdate = true;
       }

       Widget::setSize( x, y );
       
       if( m_minimized ) {
           m_renderFrame.setSize( x, y );
       }
       else {
           m_renderFrame.setSize( x, y - TOOL_BAR_HEIGHT );
       }
    }

    virtual void setPosition( float x, float y ) override {
        Widget::setPosition( x, y );
        if( m_minimized ) {
            m_renderFrame.setPosition( x, y );
        }
        else {
            m_renderFrame.setPosition( x, y + TOOL_BAR_HEIGHT );
        }
    }

    const SpatialViewConfiguration & configuration() const { return m_config; }
    SpatialViewConfiguration & configuration() { return m_config; }

    void configure( const SpatialViewConfiguration & config )  
    {
        if( ! m_config.visuallyEquivalent( config ) )
        {
            m_needsUpdate = true;
        }

        m_config = config;
    }

    void updateInformation( 
        const TN::SurfaceSetCollection & surfaceInformation,
        int nLayers )
    {
        m_config.updateInformation( surfaceInformation, nLayers );
    }

    SpatialView3D( const SpatialViewConfiguration & config );
    SpatialView3D();

    const TN::Widget & renderFrame() const { return m_renderFrame; }

    const TN::TrackBallCamera & getCamera() const { return m_camera; }

    void setCamera( const TN::TrackBallCamera & cam ) {

        if( m_camera != cam )
        {
            m_needsUpdate = true;
        }

        m_camera = cam; 
        m_camera.setAspectRatio( float( m_renderFrame.size().x() ) / m_renderFrame.size().y() );
    }

    void setNeedsUpdate()
    {
        m_needsUpdate = true;
    }

    void updated()
    {
        m_needsUpdate = false;
    }

    bool needsUpdate() const
    {
        return m_needsUpdate;
    }

    TN::Vec2< float > renderFrameSize()     const { return m_renderFrame.size(); }
    TN::Vec2< float > renderFramePosition() const { return m_renderFrame.position(); }

    const std::string & uniqueId() const 
    {
        return m_uniqueId;
    }

    virtual void applyLayout() override
    {

    }

    virtual ~SpatialView3D() {}
};

}

#endif // TN_SPATIAL_VIEW_3D_HPP

