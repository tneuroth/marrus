#ifndef VIEW_PLOT_DEFS_HPP
#define VIEW_PLOT_DEFS_HPP

#include "utils/geometry/Vec/hpp"

#include <string>
#include <vector>

// paramaters structures for different plot types, serialization with JSON

    // "type" : "Hist2D",
    
    // "title": "v_magnetic",    

    // "description": "",

    // "xLabel": "vpara",
    // "yLabel": "vpara",

    // "xKey": "vpara",
    // "yKey": "vpara",

    // "edgeMode": "uniform",

    // "edges": [ 
    //  [  ],
    //  [  ] ],

    // "xRange": [  ],
    // "yRange": [  ]

struct HistView2DParams 
{
    //{
    // "edgeMode": "uniform",
    // "nBinsX" : 17,
    // "nBinsY" : 17,
    // "edges": [ 
    // 	[  ],
    // 	[  ] ],
    // "xRange": [  ],
    // "yRange": [  ] }

    std::string edgeMode;
    int nBinsX;
    int nBinsY;
    std::vector< double > xEdges;
    std::vector< double > yEdges;
};

struct HistView1DParams 
{

};

struct Conditional2DParams 
{

};

struct Conditional3DParams 
{

};

struct CDF1DParams 
{

};

struct CDF2DParams 
{

};

#endif