#ifndef TN_MARGINAL_VIEW_HPP
#define TN_MARGINAL_VIEW_HPP

#include "views/Widget.hpp"
#include "geometry/Vec.hpp"
#include "utils/StrUtils.hpp"
#include "algorithms/Filter/BasicFilter.hpp"

#include <unordered_map>
#include <unordered_set>

namespace TN
{

struct RangeSelector : public Widget
{
std::vector< Vec2< float > > m_geometry;
std::vector< Vec3< float > > m_colors;

float m_mean;
float m_variance;
float m_skewness;
float m_kurtosis;

TN::Vec3< float > m_barColorFG;

int m_currentBarColorIndex;

float m_sdev;

void loadTF( const std::string & path, std::vector< TN::Vec3< float > > & colors )
{
    std::ifstream inFile( path );
    std::string line;
    colors.clear();    
    
    while( std::getline( inFile, line ) )
    {
        std::string rgb[ 3 ];
        for( int i = 1, cr = 0; i < line.size() && cr < 3; ++i )
        {
            if( line[i] == ',' )
            {
                ++cr;
                continue;
            }
            else if( line[ i ] == ')' )
            {
                break;
            }
            else if( line[ i ] == ' ' )
            {
                continue;
            }
            else
            {
                rgb[ cr ].push_back( line[ i ] );
            }
        }

        colors.push_back( TN::Vec3< float >(
            std::stof( rgb[ 0 ] ),
            std::stof( rgb[ 1 ] ),
            std::stof( rgb[ 2 ] ) ) );  
    }
}

std::vector< TN::Vec3< float > > m_tf;
std::string m_tfPath;

void setTF( const std::string & path )
{
    if( m_tfPath != path ) 
    {
        loadTF( path, m_tf );
    }
    m_tfPath = path;
}

int nBins;
float m_maxF;
Vec2< float > m_wRange;
Vec2< float > m_wSelectedRange;

std::vector< float > m_binVals;

std::string m_axis;

bool logscale;

void generateGeometry( 
    bool heatmap = false )
{
    for( int bin = 0; bin < nBins; ++bin )
    {
        if( m_axis == "x" )
        {
            const float dx = 2.0 / nBins;

            const float dy = heatmap == true ? 2.0
                           : m_maxF == 0 ? 0
                           : ( m_binVals[ bin ] / m_maxF ) * 2.0 + 0.15;

            m_geometry[ bin * 6 + 0 ] = Vec2< float >( -1 + bin*dx, -1 + dy );
            m_geometry[ bin * 6 + 1 ] = Vec2< float >( -1 + bin*dx, -1      );
            m_geometry[ bin * 6 + 2 ] = Vec2< float >( -1 + (bin+1)*dx, -1  );

            m_geometry[ bin * 6 + 3 ] = Vec2< float >( -1 +     bin*dx, -1 + dy );
            m_geometry[ bin * 6 + 4 ] = Vec2< float >( -1 + (bin+1)*dx, -1 );
            m_geometry[ bin * 6 + 5 ] = Vec2< float >( -1 + (bin+1)*dx, -1 + dy );
        }
        else
        {
            const float dx = heatmap == true ? 2.0
                           : m_maxF == 0 ? 0
                           : ( m_binVals[ bin ] / m_maxF ) * 2.0 + 0.15;
                           
            const float dy = 2.0 / nBins;

            m_geometry[ bin * 6 + 0 ] = Vec2< float >( -1 + dx, -1 +    bin * dy );
            m_geometry[ bin * 6 + 1 ] = Vec2< float >( -1     , -1 +    bin * dy );
            m_geometry[ bin * 6 + 2 ] = Vec2< float >( -1     , -1 + (bin+1)* dy );

            m_geometry[ bin * 6 + 3 ] = Vec2< float >( -1 + dx, -1 +    bin * dy );
            m_geometry[ bin * 6 + 4 ] = Vec2< float >( -1     , -1 + (bin+1)* dy );
            m_geometry[ bin * 6 + 5 ] = Vec2< float >( -1 + dx, -1 + (bin+1)* dy );
        }

        Vec3< float > cl;
        if( m_maxF > 0 )
        {
            if( ! heatmap )
            {
                cl = m_barColorFG;
            }
            else
            {
                int index = std::min( size_t( ( m_binVals[ bin ] / m_maxF )  * m_tf.size() ), m_tf.size() - 1 );
                Vec4 cl4 = m_tf[ index ];
                cl = Vec3< float >( cl4.r, cl4.g, cl4.b );
            }

            if( m_binVals[ bin ] == 0 )
            {
                cl = Vec3< float >( 1.0, 1.0, 0.8 );
            }
        }
        else
        {
            cl = Vec3< float >( 1.0, 1.0, 0.8 );
        }

        m_colors[ bin * 6 + 0 ] = cl;
        m_colors[ bin * 6 + 1 ] = cl;
        m_colors[ bin * 6 + 2 ] = cl;

        m_colors[ bin * 6 + 3 ] = cl;
        m_colors[ bin * 6 + 4 ] = cl;
        m_colors[ bin * 6 + 5 ] = cl;
    }
}

RangeSelector() : Widget(), m_axis( "x" )
{
    nBins = 256; 
    m_binVals = std::vector< float >( nBins, 0 );
    m_geometry = std::vector< Vec2< float > >( nBins*6, Vec2< float >( 0, 0 ) );
    m_colors = std::vector< Vec3< float > >(   nBins*6, Vec3< float >( 0, 0, 0 ) );
    m_wRange = { 0.0, 1.0 };
    m_wSelectedRange = m_wRange;
    m_barColorFG = TN::Vec3< float >(  {    0.4, 0.4, 0.4 }  )  * 0.8;
    m_currentBarColorIndex = 4;
}

RangeSelector( const std::string & axis ) : Widget(), m_axis( axis )
{
    nBins = 256;     
    m_binVals = std::vector< float >( nBins, 0 );
    m_geometry = std::vector< Vec2< float > >( nBins*6, Vec2< float >( 0, 0 ) );
    m_colors = std::vector< Vec3< float > >(   nBins*6, Vec3< float >( 0, 0, 0 ) );
    m_wRange = { 0.0, 1.0 };
    m_barColorFG = TN::Vec3< float >(  {   0.4, 0.4, 0.4 }  )  * 0.8;
    m_currentBarColorIndex = 4;
    m_wSelectedRange = m_wRange;
}

void cycleBarColors()
{
    float brightness = 0.8;
    const std::vector< TN::Vec3< float > > FGC =
    {
        TN::Vec3< float >( { 0.7001, 0.7, 0.8 } ) * brightness,
        TN::Vec3< float >( { 0.7001, 0.3, 0.2 } ) * brightness,
        TN::Vec3< float >( {    0.4, 0.7, 0.2 } ) * brightness,     
        TN::Vec3< float >( {    0.4, 0.7, 0.9 } ) * brightness,
        TN::Vec3< float >( {    0.4, 0.4, 0.4 } ) * brightness          
    };
    
    ++m_currentBarColorIndex; 
    m_currentBarColorIndex %= FGC.size();
    m_barColorFG = FGC[ m_currentBarColorIndex ];
}

void setBarColor( const TN::Vec3< float > & c )
{
    m_barColorFG = c;
}

virtual void resize( float width, float height )
{
    this->setSize( width, height );
}

void remakeGeometry()
{
    generateGeometry();
}

float shift( float amount )
{
    float allowed = amount;
    if( ( m_wSelectedRange.a() + amount ) < m_wRange.a() )
    {
        allowed = m_wSelectedRange.a() - m_wRange.a();
    }
    else if( ( m_wSelectedRange.b() + amount ) > m_wRange.b() )
    {
        allowed = m_wRange.b() - m_wSelectedRange.b();
    }
    m_wSelectedRange = m_wSelectedRange + Vec2< float >( allowed, allowed );

    return allowed;
}

void mouseDrag( const Vec2< float > & pos )
{
    if( m_axis == "x" )
    {
        float selectedRangeWidth = m_wSelectedRange.b() - m_wSelectedRange.a();
        float fullRangeWidth = m_wRange.b() - m_wRange.a();

        float rangeWidthScreen = ( selectedRangeWidth / fullRangeWidth ) * size().x();

        float newLeftScreen  = pos.x() - rangeWidthScreen / 2.0;
        float newRightScreen = pos.x() + rangeWidthScreen / 2.0;

        if( newLeftScreen < position().x() )
        {
            newLeftScreen = position().x();
            newRightScreen = position().x() + rangeWidthScreen;
        }
        else if( newRightScreen > position().x() + size().x() )
        {
            newRightScreen = position().x() + size().x();
            newLeftScreen = newRightScreen - rangeWidthScreen;
        }

        m_wSelectedRange.a( m_wRange.a() + ( ( newLeftScreen  - position().x() ) / ( float ) size().x() ) * fullRangeWidth  );
        m_wSelectedRange.b( m_wRange.a() + ( ( newRightScreen - position().x() ) / ( float ) size().x() ) * fullRangeWidth  );
    }
    else if( m_axis == "y" )
    {
        float selectedRangeWidth = m_wSelectedRange.b() - m_wSelectedRange.a();
        float fullRangeWidth = m_wRange.b() - m_wRange.a();

        float rangeWidthScreen = ( selectedRangeWidth / fullRangeWidth ) * size().y();

        float newLeftScreen  = pos.y() - rangeWidthScreen / 2.0;
        float newRightScreen = pos.y() + rangeWidthScreen / 2.0;

        if( newLeftScreen < position().y() )
        {
            newLeftScreen = position().y();
            newRightScreen = position().y() + rangeWidthScreen;
        }
        else if( newRightScreen > position().y() + size().y() )
        {
            newRightScreen = position().y() + size().y();
            newLeftScreen = newRightScreen - rangeWidthScreen;
        }

        m_wSelectedRange.a( m_wRange.a() + ( ( newLeftScreen - position().y() ) / ( float ) size().y() ) * fullRangeWidth  );
        m_wSelectedRange.b( m_wRange.a() + ( ( newRightScreen - position().y() ) / ( float ) size().y() ) * fullRangeWidth  );
    }
}

void mouseWheel( float degree )
{
    float fullRangeWidth = m_wRange.b() - m_wRange.a();
    float selectedRangeWidth = m_wSelectedRange.b() - m_wSelectedRange.a();
    float selectedRangeCenter = m_wSelectedRange.a() + selectedRangeWidth / 2.0;

    float newRangeWidth = selectedRangeWidth + ( degree ) * fullRangeWidth;

    if( newRangeWidth <= 0 )
    {
        newRangeWidth = fullRangeWidth / 150.0;
    }

    m_wSelectedRange.a( selectedRangeCenter - newRangeWidth / 2.0 );
    m_wSelectedRange.b( selectedRangeCenter + newRangeWidth / 2.0 );

    m_wSelectedRange.a( std::max( m_wRange.a(), m_wSelectedRange.a() ) );
    m_wSelectedRange.b( std::min( m_wRange.b(), m_wSelectedRange.b() ) );
}

void resetSelectedRange()
{
    float rangePad = ( m_wRange.b() - m_wRange.a() ) / 8.f;
    m_wSelectedRange = Vec2< float >( m_wRange.a() + rangePad, m_wRange.b() - rangePad );
}

Vec2< float > getRangeSelectionPositions() const
{
    float fullRangeWidth = m_wRange.b() - m_wRange.a();

    if( m_axis == "x" )
    {
        return
        {
            position().x() + ( ( m_wSelectedRange.a() - m_wRange.a() ) / fullRangeWidth ) * size().x(),
            position().x() + ( ( m_wSelectedRange.b() - m_wRange.a() ) / fullRangeWidth ) * size().x(),
        };
    }
    else
    {
        return
        {
            position().y() + ( ( m_wSelectedRange.a() - m_wRange.a() ) / fullRangeWidth ) * size().y(),
            position().y() + ( ( m_wSelectedRange.b() - m_wRange.a() ) / fullRangeWidth ) * size().y(),
        };        
    }
}

template <typename T >
void setRange( const Vec2< T > & r )
{
    m_wRange.a( r.a() );
    m_wRange.b( r.b() );
    m_wSelectedRange = m_wRange;
}

TN::Vec2< float > selectedRange() const
{
    return m_wSelectedRange;
}

template <typename T >
void setSelectedRange( const Vec2< T > & r ) 
{
    m_wSelectedRange = r;
}

void update( const std::vector< float > & values )
{
    float mn = m_wRange.a();
    float mx = m_wRange.b();

    float dv = mx - mn;

    if( dv <= 0 )
    {
        return;
    }

    for( auto & v : m_binVals )
    {
        v = 0;
    }

    float sum = 0;
    float count = 0;
    m_maxF = 0.f;
    
    const size_t N = values.size();

    for( size_t i = 0; i < N; ++i )
    {
        float p = ( values[ i ] - mn ) / dv;
        int binIdx = std::floor( p * nBins );

        if( binIdx >= nBins || binIdx  < 0 )
        {
            continue;
        }

        count += 1.f;
        sum   += values[ i ];

        m_binVals[ binIdx ] += 1.0;
    }
    if( count > 0 )
    {
        m_mean = sum / count;
    }

    for( auto & binVal : m_binVals )
    {
        m_maxF = std::max( m_maxF, binVal );
    }

    float m2 = 0.0;
    float m3 = 0.0;
    float m4 = 0.0;

    for( size_t i = 0; i < N; ++i )
    {
        float p = ( values[ i ] - mn ) / dv;
        int binIdx = std::floor( p * nBins );

        if( binIdx >= nBins || binIdx  < 0 )
        {
            continue;
        }

        const float diff = values[ i ] - m_mean;

        m2 += std::pow( diff, 2.f );
        m3 += std::pow( diff, 3.f );
        m4 += std::pow( diff, 4.f );
    }

    m2 /= count;
    m3 /= count;
    m4 /= count;

    m_variance = m2;
    m_sdev = std::sqrt( m_variance );
    m_skewness = m3 / std::pow( m_sdev, 3.0 ); 
    m_kurtosis = m4 / std::pow( m2,     2.0 ) - 3.0; 

    generateGeometry();
}


void update( 
    const std::vector< float > & values, 
    const std::vector< int > & bitflags, 
    const int BITMASK )
{
    float mn = m_wRange.a();
    float mx = m_wRange.b();

    float dv = mx - mn;

    if( dv <= 0 ) { return; }
    for( auto & v : m_binVals ) { v = 0; }

    float sum = 0;
    float count = 0;
    m_maxF = 0.f;
    
    const size_t N = values.size();

    for( size_t i = 0; i < N; ++i )
    {
        if( ! ( bitflags[ i ] & BITMASK )  )
        {
            continue;
        }

        const float p = ( values[ i ] - mn ) / dv;
        const int binIdx = std::floor( p * nBins );

        if( binIdx >= nBins || binIdx  < 0 )
        {
            continue;
        }

        count += 1.f;
        sum   += values[ i ];

        m_binVals[ binIdx ] += 1.0;
    }

    for( auto & binVal : m_binVals )
    {
        m_maxF = std::max( m_maxF, binVal );
    }

    if( count > 0 )
    {
        m_mean = sum / count;
    }

    float m2 = 0.0;
    float m3 = 0.0;
    float m4 = 0.0;

    for( size_t i = 0; i < N; ++i )
    {
        if( ! ( bitflags[ i ] & BITMASK )  )
        {
            continue;
        }

        const float p = ( values[ i ] - mn ) / dv;
        const int binIdx = std::floor( p * nBins );

        if( binIdx >= nBins || binIdx  < 0 )
        {
            continue;
        }

        const float diff = values[ i ] - m_mean;

        m2 += std::pow( diff, 2.f );
        m3 += std::pow( diff, 3.f );
        m4 += std::pow( diff, 4.f );
    }

    m2 /= count;
    m3 /= count;
    m4 /= count;

    m_variance = m2;
    m_sdev = std::sqrt( m_variance );
    m_skewness = m3 / std::pow( m_sdev, 3.0 ); 
    m_kurtosis = m4 / std::pow( m2,     2.0 ) - 3.0; 

    generateGeometry();
}

const std::vector< Vec2< float > > & geometry() const
{
    return m_geometry;
}
const std::vector< Vec3< float > > & colors() const
{
    return m_colors;
}

float mean()     const { return m_mean; }
float sdev()     const { return m_sdev; }
float var()      const { return m_variance; }
float skewness() const { return m_skewness; }
float kurtosis() const { return m_kurtosis; }

std::string statsStr() const
{
    return  "m1 = " + TN::toStringWFMT( m_mean      ) + ", " 
            "m2 = " + TN::toStringWFMT( m_variance  ) + ", "
           "sm3 = " + TN::toStringWFMT( m_variance  ) + ", "
           "sm4 = " + TN::toStringWFMT( m_variance  );           
}

};

}

#endif // MARGINALPLOT_HPP
