#ifndef TN_DATA_DEFINITION_PANEL_HPP
#define TN_DATA_DEFINITION_PANEL_HPP

#include "views/ConfigurationPanel.hpp"
#include "views/ComboWidget.hpp"
#include "views/PressButtonWidget.hpp"
#include "views/SliderWidget.hpp"
#include "geometry/Vec.hpp"
#include "algorithms/Standard/MyAlgorithms.hpp"

#include <cmath>
#include <memory>
#include <map>
#include <string>
#include <vector>

namespace TN {
	
class DataDefinitionPanel : public ConfigurationPanel
{

	std::unique_ptr< TN::TexturedPressButton > m_conditionalStatsTable;
	float m_scroll;

	public: 

	virtual void applyLayout() override {
		m_conditionalStatsTable->resizeByWidth( size().x() );

		m_conditionalStatsTable->setPosition( 
			0, 
			position().y() + size().y() - m_conditionalStatsTable->size().y() - 10 + m_scroll );
	}

	virtual bool handleInput( const InputState & input ) override
	{
		bool changed = false;
    	Vec2< double > pos = input.mouseState.position;

		if( pointInViewPort( pos ) )
		{
			if( std::abs( input.mouseState.scrollDelta() ) > 0 )
	        {
	        	m_scroll -= input.mouseState.scrollDelta() * 20.f;
	        	m_scroll = std::min( ( m_conditionalStatsTable->size().y() + 100.f ) - size().y(), m_scroll );
	        	m_scroll = std::max( m_scroll, 0.f );
	        }

	        changed = true;
		}

		if( changed )
		{
			applyLayout();
		}

		return changed;
	}
	
	virtual void clear() override {}
	
	void setTable( const std::string & tablePngPath )
	{
		m_conditionalStatsTable->setTexFromPNG(
			tablePngPath,
			tablePngPath
		);

		applyLayout();
	}

	virtual std::vector< TN::PressButton * > buttons() override { 
		return { 
			m_conditionalStatsTable.get() 
		};
	}

	virtual bool isTransparent() override { return false; }

	DataDefinitionPanel() : 
		ConfigurationPanel(), 
		m_conditionalStatsTable( std::unique_ptr<TN::TexturedPressButton>( new TN::TexturedPressButton() ) ),
		m_scroll( 0.f )
	{}

	virtual ~DataDefinitionPanel() {}
};

} // end namespace TN

#endif 