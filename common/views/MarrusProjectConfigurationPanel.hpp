#ifndef TN_MARRUS_PROJECT_CONFIGURATION_PANEL_HPP
#define TN_MARRUS_PROJECT_CONFIGURATION_PANEL_HPP

#include "views/ConfigurationPanel.hpp"
#include "views/ComboWidget.hpp"
#include "views/ExpressionEdit.hpp"
#include "views/PressButtonWidget.hpp"
#include "input/FileSelector.hpp"

namespace TN {

class MarrusProjectConfigurationPanel : public ConfigurationPanel
{
	TN::Label m_projectFileLabel;

	TN::ExpressionEdit m_projectFileEdit;
	TN::PressButton    m_createProjectButton;
	TN::PressButton    m_loadProjectButton;
	TN::PressButton    m_applyButton;
	TN::FileSelector * m_fileSelector;
	bool m_requestLoadProject;

	std::string m_workingDir;

	void openFile() 
	{
		if( m_fileSelector != nullptr )
		{
			std::string path = m_fileSelector->getFile( m_workingDir + "/projects/example/config2.json", "json" );
			m_projectFileEdit.setText( path );
		}
	}

	void init( const std::string & wd )
	{
		setTitle( "Project" );
		m_projectFileLabel.text = "Project File";
		m_projectFileEdit.label( "Path" );
		m_applyButton.text( "Apply Changes"  );
		m_createProjectButton.text( "New Project" );
		m_loadProjectButton.text( "Open Project"   );
		m_workingDir = wd;
		m_requestLoadProject = false;

		std::cout <<  "set working directory: " << m_workingDir << std::endl;
	}

	bool configurationValid() {
		return true;
	}

	public: 

	virtual void clear() override 
	{
		
	}

	virtual void applyLayout() override {

		m_projectFileLabel.position = { position().x() + 20, position().y() + size().y() - 34*2 - 20 };

		m_projectFileEdit.setSize( size().x() - 70, 30 );
		m_createProjectButton.setSize( 100, 30 );
		m_loadProjectButton.setSize(   100, 30 );
		m_applyButton.setSize(         100, 30 );

		m_createProjectButton.setPosition( position().x() + 10, position().y() + size().y() - 34 * 3 - 30);
		m_loadProjectButton.setPosition(  
			m_createProjectButton.position().x() + m_createProjectButton.size().x() + 10, 
			m_createProjectButton.position().y() );

		m_projectFileEdit.setPosition( position().x() + 50, position().y() + size().y() - 34 * 4 - 30 );

		m_applyButton.setPosition( 
			position().x() + size().x() - m_applyButton.size().x() - 20, 
			position().y() + size().y() - 34 * 5 - 50 );
	}

	virtual bool handleInput( const InputState & input ) override
	{
    	Vec2< double > pos = input.mouseState.position;

    	// apply 
		if(  m_applyButton.pointInViewPort( pos ) ) {
			if ( input.mouseState.event == MouseEvent::LeftPressed )
			{
				m_requestLoadProject = true;
			}
		}

		// create project
		if(  m_createProjectButton.pointInViewPort( pos ) ) {
			if( m_createProjectButton.isPressed() && input.mouseState.event == MouseEvent::LeftReleased )
			{
				// do something

				m_createProjectButton.setPressed( false );
			}
			else if ( ! m_createProjectButton.isPressed() && input.mouseState.event == MouseEvent::LeftPressed )
			{
				m_createProjectButton.setPressed( true );
			}
		}
		else if( input.mouseState.event == MouseEvent::LeftReleased )
		{
			m_createProjectButton.setPressed( false );
		}		

		// open project
		if(  m_loadProjectButton.pointInViewPort( pos ) ) {
			if ( input.mouseState.event == MouseEvent::LeftPressed )
			{
				openFile();
			}
		}

		bool pe = m_projectFileEdit.handleInput( input );

		return false;
	}

	MarrusProjectConfigurationPanel( const std::string & wd, TN::FileSelector * fs ) : ConfigurationPanel(), m_fileSelector( fs )
	{
		init( wd );
	}	

	MarrusProjectConfigurationPanel( const std::string & wd ) : ConfigurationPanel(), m_fileSelector( nullptr )
	{
		init( wd );
	}	

	MarrusProjectConfigurationPanel() : ConfigurationPanel(), m_fileSelector( nullptr )
	{
		init( "~" );
	}	

	void setFileSelector( FileSelector * fs ) 
	{
		m_fileSelector = fs;
	}

	bool requestLoadProject() const 
	{
		return m_requestLoadProject;
	}

	void resolveRequests() {
		m_requestLoadProject = false;
	}

	std::string getProjectFile() const {
		return m_projectFileEdit.text();
	}

	virtual std::vector< TN::Label       * > labels()  override { 
		return {
			& m_projectFileLabel
		};
	}
	
	virtual std::vector< TN::PressButton * > buttons() override { 
		return {
			& m_createProjectButton,
			& m_loadProjectButton,
			& m_applyButton
		}; 
	}

	virtual std::vector< TN::ExpressionEdit * > expressionEdits() override { 
		return {
			& m_projectFileEdit
		}; 
	}
	
	virtual std::vector< TN::ComboWidget * > combos()  override { 
		return { 

		}; 
	}

	virtual ~MarrusProjectConfigurationPanel() {}
};

} // end namespace TN

#endif 