#ifndef TN_JOINT_PLOT_CONFIGURATION_PANEL_HPP
#define TN_JOINT_PLOT_CONFIGURATION_PANEL_HPP

#include "views/ConfigurationPanel.hpp"
#include "views/ComboWidget.hpp"
#include "views/ExpressionEdit.hpp"

namespace TN {

class JointPlotConfigurationPanel : public ConfigurationPanel
{
	TN::Label m_varSetLabel;

	TN::ComboWidget m_xAxisCombo;
	TN::ComboWidget m_yAxisCombo;
	TN::ComboWidget m_wCombo;

	TN::Label m_binsLabel;

	TN::ComboWidget m_xBinsCombo;
	TN::ComboWidget m_yBinsCombo;

	TN::PressButton m_applyButton;

	std::map< std::string, std::pair< std::string, TN::Vec2< double > > > m_variableInformation;
	std::string m_projectDirectory;

	bool m_waitingToProcessChange;

	public: 

	virtual void applyLayout() override {

		m_varSetLabel.position = { position().x() + 20, position().y() + size().y() - 34*2 - 20 };

		m_xAxisCombo.setSize( size().x() - 110, 30 );
		m_yAxisCombo.setSize( size().x() - 110, 30 );
		m_wCombo.setSize(     size().x() - 110, 30 );

		m_xAxisCombo.setPosition( position().x() + 90, position().y() + size().y() - 34 * 3 - 30 );
		m_yAxisCombo.setPosition( position().x() + 90, position().y() + size().y() - 34 * 4 - 30 );
		m_wCombo.setPosition(     position().x() + 90, position().y() + size().y() - 34 * 5 - 30 );

		// m_binsLabel.position = { position().x() + 40, position().y() + size().y() - 34*6 - 30 };

		// m_xBinsCombo.setPosition( position().x() + 90, position().y() + size().y() - 34 * 7 - 40 );
		// m_yBinsCombo.setPosition( position().x() + 90, position().y() + size().y() - 34 * 8 - 40 );

		// m_xBinsCombo.setSize( size().x() - 110, 30 );
		// m_yBinsCombo.setSize( size().x() - 110, 30 );

		m_applyButton.setPosition( 
			position().x() + size().x() - m_applyButton.size().x() - 20, 
			position().y() + size().y() - 34 * 6 - 50 );
	}

	bool configurationValid() 
	{
		// ...

		return true;
	}

	virtual void clear() override {

		TN::Label m_varSetLabel;

		TN::ComboWidget m_xAxisCombo;
		TN::ComboWidget m_yAxisCombo;
		TN::ComboWidget m_wCombo;

		TN::Label m_binsLabel;

		TN::ComboWidget m_xBinsCombo;
		TN::ComboWidget m_yBinsCombo;

		TN::PressButton m_applyButton;

		std::map< std::string, std::pair< std::string, TN::Vec2< double > > > m_variableInformation;
		std::string m_projectDirectory;

		bool m_waitingToProcessChange;

		m_yAxisCombo.clear();
		m_yAxisCombo.setKeys( {
			"None"
		} );
		m_wCombo.setKeys( {
			"None"
		} );
	}

	virtual bool handleInput( const InputState & input ) override
	{
    	Vec2< double > pos = input.mouseState.position;

	    auto comb = combos();
	    bool existsPressedCombo = false;

		for( auto & c : comb ) 
		{
			if( c->isPressed() )
			{
			    existsPressedCombo = true;
			    break;
			}
		}

	    if( input.mouseState.event == MouseEvent::LeftPressed ) {
		    for( auto & c : comb ) 
		    {
				if( c->pointInViewPort( pos ) )
				{
				    if( ! existsPressedCombo && ! c->isPressed() )
				    {
				        c->setPressed( true );
				        break;
				    }
				    else if( c->isPressed() )
				    {
				        int idx = c->pressed( pos );
				        c->setPressed( false );
				        break;
				    }
				}
		    }
		}
		else
		{
			for( auto & c : comb ) 
			{
			    if( c->isPressed() )
			    {
			        int id = c->mousedOver( pos );
			        if( id == -1 )
			        {
			            c->setPressed( false );
			        }
			        break;
			    }
			}
		}

		if(  m_applyButton.pointInViewPort( pos ) ) {
			if ( input.mouseState.event == MouseEvent::LeftPressed )
			{
				m_waitingToProcessChange = true;
			}
		}

		return false;
	}

	JointPlotConfigurationPanel() : ConfigurationPanel(), m_waitingToProcessChange( false )
	{
		m_varSetLabel.text = "Variables";
		m_varSetLabel.bold = true;

		m_xAxisCombo.text( "X-Axis" );
		m_yAxisCombo.text( "Y-Axis" );
		
		m_xAxisCombo.setKeys( {
			"None"
		} );
		m_yAxisCombo.setKeys( {
			"None"
		} );
		
		m_wCombo.text( "w    " );
		
		m_wCombo.setKeys( {
			"None"
		} );

		m_binsLabel.text = "Binning";
		m_varSetLabel.bold = true;

		// m_xBinsCombo.text( "X-Bins" );
		// m_xBinsCombo.setKeys( {
		// 	"Uniform Constant",
		// 	"Uniform Auto"
		// } );

		// m_yBinsCombo.text( "Y-Bins" );
		// m_yBinsCombo.setKeys( {
		// 	"Uniform Constant",
		// 	"Uniform Auto"
		// } );

		setTitle( "Joint Plot Configuration" );

		m_applyButton.text( "Apply Changes" );
		m_applyButton.setSize( 120, 30.f );
	}	

	JointPlotConfiguration processChanges( const JointPlotConfiguration & current )
	{
		m_waitingToProcessChange = false;
		JointPlotConfiguration jpc = current;
		jpc.x = m_xAxisCombo.selectedItemText();
		jpc.y = m_yAxisCombo.selectedItemText();
		jpc.w = m_wCombo.selectedItemText();
		return jpc;
	}

	bool waitingToProcess() {
		return m_waitingToProcessChange;
	}

	void configure( 
		const std::string & xkey, 
		const std::string & ykey, 
		const std::string & wkey = "" )
	{
		m_xAxisCombo.selectByText( xkey );
		m_yAxisCombo.selectByText( ykey );
		m_wCombo.selectByText( wkey == "" ? "None" :  wkey );
	}

	void updateVariableInformation( 
		const std::map< std::string, std::pair< std::string, TN::Vec2< double > > > & varInfo, 
		const std::string & projDir )
	{
		m_projectDirectory = projDir;
		
		if( m_variableInformation == varInfo )
		{
			return;
		}

		m_variableInformation = varInfo;

		std::vector< std::string > vars = { "None" };
		std::map< std::string, std::string > texPaths;
		for( auto & v : varInfo ) 
		{
			vars.push_back( v.first );
			texPaths.insert( { 
				v.first,  
				projDir + "/latex_symbols/" + v.first + ".png"
			} );
		}

		m_xAxisCombo.updateKeys( vars, texPaths );
		m_yAxisCombo.updateKeys( vars, texPaths );
		m_wCombo.updateKeys(     vars, texPaths );
	}

	virtual std::vector< TN::Label       * > labels()  override { 
		return {
			& m_varSetLabel //,
			// & m_binsLabel
		}; 
	}
	
	virtual std::vector< TN::PressButton * > buttons() override { 
		return {
			& m_applyButton
		}; 
	}

	virtual std::vector< TN::ExpressionEdit * > expressionEdits() override { 
		return {

		}; 
	}
	
	
	virtual std::vector< TN::ComboWidget * > combos()  override { 
		return { 
			& m_xAxisCombo,
			& m_yAxisCombo// ,
			//& m_wCombo //,
			// & m_xBinsCombo,
			// & m_yBinsCombo
		}; 
	}

	virtual ~JointPlotConfigurationPanel() {}
};

} // end namespace TN

#endif 