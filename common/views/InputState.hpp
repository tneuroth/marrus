#ifndef TN_INPUT_HPP
#define TN_INPUT_HPP

#include "geometry/Vec.hpp"

#include <string>
#include <chrono>
#include <map>
#include <set>
#include <chrono>

struct KeyState
{
    typedef std::chrono::time_point<std::chrono::high_resolution_clock> TimePoint;

    std::map< std::string, TimePoint > held;
    std::set< std::string > justPressed;
    std::set< std::string > justReleased;

    void press( const std::string & k ) 
    {
        justPressed.insert( { k } );
        held.insert( { k, std::chrono::system_clock::now() } );
    }

    void release( const std::string & k ) 
    {
        std::map< std::string, TimePoint >::iterator iter = held.find( k ) ;
        if( iter != held.end() )
        {
            held.erase( iter );
        }
    }

    void clear() 
    {
        justPressed.clear();
        justReleased.clear();
    }

    double timeHeld( const std::string & key ) const
    {
        if( held.count( key ) )
        {
            return ( std::chrono::high_resolution_clock::now() - held.at( key ) ).count();
        }
        else 
        {
            return 0.0;
        }
    }

    KeyState()
    {
        clear();
    }
};

enum class MouseButtonState
{
	NonePressed   = 0,
    LeftPressed   = 1,
    RightPressed  = 2,
    MiddlePressed = 3
};

enum class MouseEvent
{
    None           = 0,
    LeftReleased   = 1,
    LeftPressed    = 2,
    RightReleased  = 3,
    RightPressed   = 4,
    MiddleReleased = 5,
    MiddlePressed  = 6
};

struct Probe
{
    bool active;
    bool toggle( bool a ) { return active = ! active; }
    void setActive( bool a ) { active = a; }
};

struct RadialProbe : public Probe 
{
    TN::Vec2< float > center;
    float radius;
};

struct CartesianProbe : public Probe 
{
    TN::Vec2< float > offset;
    TN::Vec2< float > size;
};

struct PolygonProbe : public Probe 
{
    std::vector< TN::Vec2< float > > polygon;
};

struct UniformCartesianGridProbe : public Probe 
{
    int rows;
    int cols;
};


struct MouseState
{
    MouseButtonState buttonState;

    TN::Vec2< double > position;
    TN::Vec2< double > previousPosition;    
    TN::Vec2< double > positionDelta;

    double scrollLast;
    double scroll;

    MouseEvent event;
    TN::Vec2< double > eventPosition;


    double scrollDelta() const { return scroll - scrollLast; }

    void updateScroll( double _scroll )
    {
    	scroll += _scroll;
    }

    void updatePosition( const TN::Vec2< double > & _pos )
    {
        previousPosition = position;
        position = _pos;
        positionDelta = _pos - previousPosition;
    }

    MouseState() : 
    	buttonState( MouseButtonState::NonePressed ), 
    	position( 0.f, 0.f ), 
    	previousPosition( 0.f, 0.f ),
    	scrollLast( 0.f ),
    	scroll( 0.f ),
    	positionDelta( 0.f, 0.f ),
        event( MouseEvent::None )
    {}
};

struct InputState 
{
    MouseState mouseState;
    KeyState keyState;

    void next()
    {
    	keyState.clear();
        mouseState.scrollLast = mouseState.scroll;
        mouseState.event = MouseEvent::None;
    }
};

#endif