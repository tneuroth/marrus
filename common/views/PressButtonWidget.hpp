
#ifndef UIWIDGET
#define UIWIDGET

#include "views/Widget.hpp"
#include "geometry/Vec.hpp"
#include "image/ImageLoader.hpp"
#include "render/Texture/Texture.hpp"

#include <vector>
#include <cstdint>
#include <iostream>

namespace TN
{

class PressButton : public Widget
{

public:

    virtual bool textured() { return false; }

    static const int LINK_BUTTON_WIDTH = 13;

    enum class Style
    {
        Default,
        Linker
    };

    enum class Mode
    {
        Default,
        Toggle
    };

protected:

    int m_id;
    Vec3< float > m_color;
    bool m_isPressed;
    bool m_flash;
    double m_flashPhase;
    Vec3< float > m_flashColor;
    std::string m_text;
    Style m_style;
    Mode m_mode;
    bool m_emboss;
    bool m_clearColor;

public:

    virtual ~PressButton()
    {}

    Mode mode() const
    {
        return m_mode;
    }

    void mode( Mode m )
    {
        m_mode = m;
    }

    bool emboss() const
    {
        return m_emboss;
    }

    Style style() const
    {
        return m_style;
    }

    void style( Style s )
    {
        m_style = s;
    }

    virtual void resize( float width, float height )
    {
        this->setSize( width, height );
    }

    PressButton( bool emboss = true ) : 
        m_isPressed( false ), 
        m_flash( false ), 
        m_flashPhase(0), 
        m_text( "" ),
        m_emboss(emboss),
        m_color( { 1.0, 1.0, 1.0 } ),
        m_clearColor( false )
    {}

    PressButton( const Vec3< float > & color, int width, int height, int id, bool emboss = true ) :
        m_id( id ),
        m_color( color ),
        m_isPressed( false ),
        m_flash( false ),
        m_flashPhase(0),
        m_text(""),
        m_style( Style::Default ),
        m_emboss( emboss ),
        m_clearColor( true )
    {
        resize( width, height );
    }

    int textX() const
    {
        return position().x() + ( size().x() - m_text.size()*7 )/2;
    }

    int textY() const
    {
        return this->position().y() + 7;
    }

    void text( const std::string & txt )
    {
        m_text = txt;
    }

    const std::string & text() const
    {
        return m_text;
    }

    const Vec3< float > & color() const
    {
        return m_color;
    }

    void setClearColor( bool c ) 
    {
        m_clearColor = c;
    }

    const bool clearColor() const 
    {
        return m_clearColor;
    }

    void color( const Vec3< float > & c ) 
    {
        m_color = c;
    }

    bool isPressed() const
    {
        return m_isPressed;
    }

    int id() const
    {
        return m_id;
    }

    void toggle()
    {
        m_isPressed = ! m_isPressed;
    }

    void setPressed( bool pressed )
    {
        m_isPressed = pressed;
    }

    bool flash() const
    {
        return m_flash;
    }
    void flash( bool b )
    {
        m_flash = b;
    }

    void flashColor( const Vec3< float > & cl )
    {
        m_flashColor = cl;
    }

    Vec3< float > flashColor() const
    {
        return m_flashColor;
    }

    double flashPhase() const
    {
        return m_flashPhase;
    }
    void flashPhase( double phase )
    {
        m_flashPhase = phase;
    }
};

class TexturedPressButton : public PressButton
{
    TextureLayerRGBA m_texPressed;
    TextureLayerRGBA m_tex;

public :

    virtual bool textured() override { return true; }

    void bind( bool pressed )
    {
        if( pressed )
        {
            m_texPressed.tex.bind();
        }
        else
        {
            m_tex.tex.bind();
        }
    }
    
    TexturedPressButton()
        : PressButton( false )
    {}

    TexturedPressButton( const TN::Vec3< float > & color, int width, int height, int id )
        : PressButton( color, width, height, id, false )
    {}

    void setTexFromData( 
        const std::vector<std::array< std::uint8_t, 3 > > & d, 
        int width,
        int height )
    {
        std::vector< float > data( width*height*4 );
        for( int i = 0; i < width*height; ++i )
        {
            data[ i*4 + 0 ] = d[ i ][ 0 ] / 255.f;
            data[ i*4 + 1 ] = d[ i ][ 1 ] / 255.f; 
            data[ i*4 + 2 ] = d[ i ][ 2 ] / 255.f;
            data[ i*4 + 3 ] = 1.f;
        }

        m_tex.load(        data, width, height, true );
        m_texPressed.load( data, width, height, true );
    }

    void setTexFromPNG( 
        const std::string & pngPath, 
        const std::string & pngPathPressed )
    {
        std::vector< float > data;
        TN::Vec2< int > dims;

        // std::cout << "loading png " << pngPath << std::endl;

        TN::loadRGBA( 
            pngPath,
            data,
            dims );

        // std::cout << "loading png into texture " << dims.a() << " " << dims.b() << " " << data.size() << std::endl;

        m_tex.load( data, dims.a(), dims.b(), true );

        // std::cout << "loading png " << pngPath << std::endl;

        TN::loadRGBA( 
            pngPathPressed,
            data,
            dims );

        // std::cout << "loading png into texture " << dims.a() << " " << dims.b() << " " << data.size() << std::endl;

        m_texPressed.load( data, dims.a(), dims.b(), true );
    }

    void resizeByHeight( double h )
    {
        double aspec = m_tex.width() / ( double ) m_tex.height();
        setSize( aspec*h, h );
    }

    void resizeByWidth( double w )
    {
        double aspect = m_tex.height() / ( double ) m_tex.width();
        setSize( w, aspect * w );
    }


    virtual ~TexturedPressButton()
    {}
};

}

#endif // UIWIDGET

