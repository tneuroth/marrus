#ifndef TN_TOOL_PANEL_HPP
#define TN_TOOL_PANEL_HPP

#include "views/Widget.hpp"
#include "views/PressButtonWidget.hpp"
#include "geometry/Vec.hpp"
#include "views/InputState.hpp"

#include <string>
#include <vector>
#include <iostream>
#include <map>

namespace TN {

struct PanelElement 
{
    PressButton tabButton;
    Widget * view;

    PanelElement(       
        const std::string & text,
        Widget * v ) : view( v )
    {
        tabButton.text( text );
        tabButton.setPressed( false );
    }

    bool isOpen() const
    {
        return tabButton.isPressed();
    }
};

enum class TabViewOrientation
{
    OpenRight,
    OpenLeft,
    OpenUp,
    OpenDown
};

class TabView : public Widget
{
    Widget m_resizeHandle;
    std::map< std::string, PanelElement > m_panelElements;
    std::vector< std::string > m_elementKeyOrdered;
    int m_activePanel;
    TabViewOrientation m_orientation;
    TN::TexturedPressButton m_collapseButton;

    bool m_resizePressed;
    bool m_resizeHovered;

    float m_minWidth;
    float m_width;

    void init( const std::string & resourceDir ) {

        m_collapseButton.setTexFromPNG(
            resourceDir + "/textures/leftArrow.png",
            resourceDir + "/textures/rightArrow.png" );
    
        m_collapseButton.resizeByHeight( 30.f );

        m_minWidth = 900;
        m_width    = 1000;

        m_resizePressed = false;
        m_resizeHovered = false;

        setSize( m_width, 900 );
    }

    public:

    bool isOpen() {
        return ! m_collapseButton.isPressed();
    }

    TabView( const std::string & resourceDir ) : m_activePanel( -1 ), m_orientation( TabViewOrientation::OpenRight )
    {
        init( resourceDir );
    }

    TabView( const std::string & resourceDir, TabViewOrientation orientation ) : m_activePanel( -1 ), m_orientation( orientation )
    {
        init( resourceDir );
    }

    void setOrientation( TabViewOrientation o ) { m_orientation = o; }

    void applyLayout()
    {
    	const double W = size().x();
    	const double H = size().y();

    	const double X = position().x();
    	const double Y = position().y();

        const double BTN_Width  = 90;
        const double BTN_Height = 30;

        const double PAD    = 2;

        m_collapseButton.setSize( BTN_Height, BTN_Height );
        m_collapseButton.setPosition(
                m_orientation == TabViewOrientation::OpenRight
                    ?  X + PAD  
                    : ( X + W ) - ( PAD + BTN_Width ),
                Y + H - ( PAD + BTN_Height )
        );

        for( size_t i = 0; i < m_elementKeyOrdered.size(); ++i ) {

            auto & key     = m_elementKeyOrdered[ i ];
            auto & element = m_panelElements.at( key );
            
            element.tabButton.setSize( BTN_Width, BTN_Height );
            element.tabButton.setPosition( 
                m_orientation == TabViewOrientation::OpenRight
                    ?  X        + ( i ) * ( BTN_Width ) + ( BTN_Height + PAD ) 
                    : ( X + W ) - ( i ) * ( BTN_Width ) - ( BTN_Height + PAD ),
                Y + H - ( PAD + BTN_Height )
            );

            if( i == m_activePanel ) 
            {
                element.view->setPosition( position().x(), position().y() );
                element.view->setSize( size().x() - m_resizeHandle.size().x(), size().y() - BTN_Height - 4 );
                element.view->applyLayout();
            }
        }

        m_resizeHandle.setSize( 10, H - ( BTN_Height + PAD*2) );
        m_resizeHandle.setPosition( W - 10, X );
    }

    bool handleInput( 
        const InputState & input ) override
    {
    	Vec2< double > p = input.mouseState.position;

        bool changed = false;

        /////////////////////////////////////////////////////////////////////////////////////////////////////
        // resize handle

        if( m_resizeHandle.pointInViewPort( p ) ) {
            m_resizeHovered = true;
            if( input.mouseState.event == MouseEvent::LeftPressed && m_resizePressed == false ) {
                m_resizePressed = true;
            }
        } 
        else {
            m_resizeHovered = false;
        }

        if( m_resizePressed ) {
            float newWidth = ( p.x() - position().x() ) + 5.f;
            newWidth = std::max( newWidth, m_minWidth ); 
            setSize( newWidth, size().y() );  
            m_resizeHandle.setPosition( 
                position().x() + newWidth - 10, 
                m_resizeHandle.position().y() );
            changed = true;
        }

        if( m_resizePressed && input.mouseState.event == MouseEvent::LeftReleased )
        {
            m_resizePressed = false;
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////////


        if( m_collapseButton.pointInViewPort( p ) &&  input.mouseState.event == MouseEvent::LeftPressed )
        {
            m_collapseButton.setPressed( ! m_collapseButton.isPressed() );
            return true;
        }

        if( input.mouseState.event == MouseEvent::LeftPressed )
        {
	    	int newlyPressedTab = -1;
	    	for( size_t i = 0, end = m_elementKeyOrdered.size(); i < end; ++i )
	    	{
	            auto & e = m_panelElements.at( m_elementKeyOrdered[ i ] );
	            if( e.tabButton.pointInViewPort( p ) )
	            {
	            	e.tabButton.setPressed( true );
	            	newlyPressedTab = i;
	            	break;
	            }
	    	}

	        if( newlyPressedTab != -1 )
	        {
		    	for( size_t i = 0, end = m_elementKeyOrdered.size(); i < end; ++i )
		    	{
		            if( i != newlyPressedTab )
		            {
                        auto & e = m_panelElements.at( m_elementKeyOrdered[ i ] );
	                    e.tabButton.setPressed( false );
		            }
		    	}

                changed = true;
                m_activePanel = newlyPressedTab;
            }
        }

        if( m_activePanel != -1 ) 
        {
            changed |= m_panelElements.at( m_elementKeyOrdered[ m_activePanel ] ).view->handleInput( input );
        }

        return changed;
    }

    void addElement( 
    	const std::string & key,
    	const std::string & text,
    	Widget * view )
    {
    	if( m_panelElements.count( key ) )
    	{
    		std::cerr << "Error: panel element with key: " << key << " already exists" << std::endl;
    		exit( 1 );
    	}

        m_elementKeyOrdered.push_back( key );
        m_panelElements.insert( { key, PanelElement( text, view ) } );
    }

    virtual void resize( float width, float height )
    {
        setSize( width, height );
        applyLayout();
    }

    Widget * elementView( const std::string & key )
    {
    	if( m_panelElements.count( key ) == 0 )
    	{
    		std::cerr << "error no panel with key: " << key << std::endl;
    		exit( 1 );
    	}
        return m_panelElements.at( key ).view;
    }

    std::vector< PanelElement * > elements()
    {
        std::vector< PanelElement * > e( m_elementKeyOrdered.size() );
        for( size_t i = 0; i < m_elementKeyOrdered.size(); ++i )
        {
            e[ i ] = & m_panelElements.at( m_elementKeyOrdered[ i ] );
        }
        return e;
    }

    bool active( const std::string & key ) const 
    {
    	if( ! m_panelElements.count( key ) )
    	{
            return false; // maybe should throw error
    	}

    	else return m_panelElements.at( key ).isOpen();
    }

    void setActive( const std::string & key )
    {
    	if( m_panelElements.count( key ) == 0 )
    	{
            m_activePanel = -1;
    	}

    	for( size_t i = 0, end = m_elementKeyOrdered.size(); i < end; ++i )
    	{
    		auto & e = m_panelElements.at( m_elementKeyOrdered[ i ] );
    		if( m_elementKeyOrdered[ i ] == key )
    		{
    			m_activePanel = i;
    			e.tabButton.setPressed( true );
    		}
    		else
    		{
    			e.tabButton.setPressed( false );
    		}
    	}
    }

    bool resizeHovered() const {
        return m_resizeHovered;
    }

    bool resizing() const {
        return m_resizePressed;
    }

    std::vector< PressButton * > buttons() {
        std::vector< PressButton * > bts = { & m_collapseButton };
        if( isOpen() ) {
            for( auto & e : m_panelElements ) {
                bts.push_back( & e.second.tabButton );
            }
        }
        return bts;
    }

    ~TabView()
    {}
};

}

#endif