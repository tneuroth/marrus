
#ifndef SLIDERWIDGET_HPP
#define SLIDERWIDGET_HPP

#include "views/Widget.hpp"
#include "geometry/Vec.hpp"

namespace TN
{

class SliderWidget : public Widget
{
    int m_id;
    Vec3< float > m_color;
    float m_sliderPosition;
    float m_defaultPosition;
    bool m_isPressed;
    std::string m_text;
    bool m_invalidates;

public:

    const int SLIDER_HANDLE_WIDTH = 16;

    virtual void resize( float width, float height )
    {
        this->setSize( width, height );
    }

    SliderWidget() : m_sliderPosition( .5 ), m_defaultPosition( 0.5 ), m_isPressed( false ), m_text("") {}

    SliderWidget(
        const Vec3< float > & color,
        int width,
        int height,
        int id ) :
        m_id( id ),
        m_color( color ),
        m_sliderPosition(.5),
        m_defaultPosition( 0.5 ),
        m_isPressed( false ),
        m_text(""),
        m_invalidates( false )
    {
        resize( width, height );
    }

    void setDefault()
    {
        m_sliderPosition = m_defaultPosition;
    }

    void setDefault( float defaultPos )
    {
        m_defaultPosition = defaultPos;
        setDefault();
    }

    float defaultSliderPos() const
    {
        return m_defaultPosition;
    }

    const Vec3< float > & color() const
    {
        return m_color;
    }

    bool invalidates() const
    {
        return m_invalidates;
    }

    void invalidates( bool c )
    {
        m_invalidates = c;
    }

    int textX() const
    {
        return position().x() - m_text.size()*7 - 4;
    }

    int textY() const
    {
        return this->position().y() + 5;
    }

    void text( const std::string & txt )
    {
        m_text = txt;
    }

    const std::string & text() const
    {
        return m_text;
    }

    float sliderPosition()
    {
        return m_sliderPosition;
    }

    bool isPressed() const
    {
        return m_isPressed;
    }

    void setSliderPos( double pos )
    {
        m_sliderPosition = pos;
    }

    void setPressed( bool pressed )
    {
        m_isPressed = pressed;
    }

    bool overSliderHandle( const Vec2< float > & pos )
    {
        if( Widget::pointInViewPort( pos ) )
        {
            float rp1 = ( pos.x() - position().x() - SLIDER_HANDLE_WIDTH / 2.0 ) / double( size().x() );
            float rp2 = ( pos.x() - position().x() + SLIDER_HANDLE_WIDTH / 2.0 ) / double( size().x() );
            float r   = ( pos.x() - position().x() ) / double( size().x() );
            if( r >= rp1 && r < rp2 )
            {
                return true;
            }
        }
        return false;
    }

    void translate( const Vec2< float > & pos )
    {
        if ( pos.x() < position().x() || pos.x() > position().x() + size().x() )
        {
            return;
        }
        else
        {
            m_sliderPosition = ( pos.x() - position().x() ) / double( size().x() );
        }
    }

    double value() const
    {
        return m_sliderPosition;
    }
};

inline void translateLinkedSliders( SliderWidget & slider1, SliderWidget & slider2, Vec2< float > pos )
{
    float sliderPos1 = slider1.sliderPosition();
    float sliderPos2 = slider2.sliderPosition();

    slider1.translate( pos );
    float sliderPos1Result = slider1.sliderPosition();

    // if moved left
    if( sliderPos1Result <= sliderPos1 )
    {
        // if slider 1 is left most
        if( sliderPos1 <= sliderPos2 )
        {
            // just need to set slider 2 to be the same origional distance from slider 1
            slider2.setSliderPos( sliderPos1Result + ( sliderPos2 - sliderPos1 ) );
        }

        // slider 2 is left most
        else
        {
            // if slider 1, after moved, still leaves room for slider 2 to be positioned the same origional distance from slider 1
            if( sliderPos1Result >= ( sliderPos1 - sliderPos2 ) )
            {
                // then we can keep slider 1's origional position and just move slider 2
                slider2.setSliderPos( sliderPos1Result - ( sliderPos1 - sliderPos2 ) );
            }

            // otherwise we need to set slider 2 to 0 and slider 1 to correct relative position from it
            else
            {
                slider2.setSliderPos( 0 );
                slider1.setSliderPos( sliderPos1 - sliderPos2 );
            }
        }
    }

    // moved right
    else
    {
        // if slider 1 is right most
        if( sliderPos1 >= sliderPos2 )
        {
            // just need to set slider 2 to be the same origional distance from slider 1
            slider2.setSliderPos( sliderPos1Result - ( sliderPos1 - sliderPos2 ) );
        }

        // slider 2 is right most
        else
        {
            // if slider 1, after moved, still leaves room for slider 2 to be positioned the same origional distance from slider 1
            if( sliderPos1Result + ( sliderPos2 - sliderPos1 ) <= 1.0 )
            {
                // then we can keep slider 1's origional position and just move slider 2
                slider2.setSliderPos( sliderPos1Result + ( sliderPos2 - sliderPos1 ) );
            }

            // otherwise we need to set slider 2 to 1 and slider 1 to correct relative position from it
            else
            {
                slider2.setSliderPos( 1 );
                slider1.setSliderPos( 1 - ( sliderPos2 - sliderPos1 ) );
            }
        }
    }
}

}

#endif // SLIDERWIDGET_HPP

