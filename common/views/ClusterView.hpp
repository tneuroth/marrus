#ifndef TN_CLUSTER_VIEW_HPP
#define TN_CLUSTER_VIEW_HPP

#include "geometry/Vec.hpp"
#include "views/Widget.hpp"
#include "views/InputState.hpp"
#include "views/PressButtonWidget.hpp"
#include "render/Texture/Texture.hpp"

#include <vector>
#include <iostream>

namespace TN
{

struct ProjectionMouseState
{
    bool on;
    Vec2< float > pos0;
    bool leftButton;
    bool rightButton;
    bool wheelButton;
    Vec2< float > pos;

    ProjectionMouseState()   
    {}
};

struct ProjectionStateInfo 
{
    ProjectionMouseState mouseState;
    Vec2< float > value;
};

class ProjectionPlot : public Widget
{
    std::string m_style;
    std::string m_title;

    bool m_leftMouseReleased;
    bool m_rightMouseReleased;
    bool m_mouseClicked;
    ProjectionStateInfo m_stateInfo;

    float m_scale;
    TN::Vec2< float > m_displacement;

    TN::Vec2< float > m_defaultXRange;
    TN::Vec2< float > m_defaultYRange;
    
    TN::Vec2< float > m_boxLowerLeft;
    TN::Vec2< float > m_boxUpperRight;
    bool m_probeOn;
    std::vector< TN::Vec2< float > > m_lassoPolygone;
    std::vector< TN::Vec2< float > > m_lassoScreenSpace;
    TN::Vec2< float > m_lassoBBX;
    TN::Vec2< float > m_lassoBBY;    
    ProbeType m_probeType;


    template < class TP >
    bool mouseOver( const Vec2< TP > & p )
    {
        return pointInViewPort( p );
    }
 

public:

    TN::Vec2< float > xRange() const
    {
        float xW = ( m_defaultXRange.b() - m_defaultXRange.a() );

        TN::Vec2< float > r = ( m_defaultXRange - xW / 2.f ) * m_scale
                            + ( m_defaultXRange.a() + xW/2.f );

        return r + m_displacement.x();
    }

    TN::Vec2< float > yRange() const
    {
        float yW = ( m_defaultYRange.b() - m_defaultYRange.a() );

        TN::Vec2< float > r = ( m_defaultYRange - yW / 2.f ) * m_scale
                            + ( m_defaultYRange.a() + yW/2.f );

        return r + m_displacement.y();
    }

    TN::Vec2< float > xBoxRange() const
    {
        return { m_boxLowerLeft.x(), m_boxUpperRight.x() };
    }

    TN::Vec2< float > yBoxRange() const
    {
        return { m_boxLowerLeft.y(), m_boxUpperRight.y() };
    }

    std::vector< TN::Vec2< float > > lasso() const
    {
        return m_lassoPolygone;
    }

    TN::Vec2< float > lassoBBX() const
    {
        return m_lassoBBX;
    }

    TN::Vec2< float > lassoBBY() const
    {
        return m_lassoBBY;
    }

    TN::Vec2< double > valueAt( const Vec2< double > & p )
    {
        auto xR = xRange();
        auto yR = yRange();

        float xw = xR.b() - xR.a();
        float yw = yR.b() - yR.a();        

        float dx = ( p.x() - position().x() ) / (float) size().x();
        float dy = ( p.y() - position().y() ) / (float) size().y();

        return  {
            xR.a() + dx * xw,
            yR.a() + dy * yw };
    }

    std::vector< TN::Vec2< float > > getBoxLines() const
    {
        return {
            m_boxLowerLeft,
            { m_boxLowerLeft.x(), m_boxUpperRight.y() },
            m_boxUpperRight,
            { m_boxUpperRight.x(), m_boxLowerLeft.y() },
            m_boxLowerLeft
        }; 
    }

    virtual ~ProjectionPlot() 
    {}

    void clean()
    {}

    bool leftMouseReleaseEvent() { 
        auto tmp = m_leftMouseReleased; 
        m_leftMouseReleased = false; 
        return tmp; 
    }

    bool rightMouseReleaseEvent() { 
        auto tmp = m_rightMouseReleased; 
        m_rightMouseReleased = false; 
        return tmp; 
    }


    bool mouseClickedEvent() 
    { 
        auto tmp = m_mouseClicked; 
        m_mouseClicked = false; 
        return tmp; 
    }

    ProjectionPlot() : 
        m_probeOn( false ),
        m_probeType( ProbeType::Lasso )
    {
        m_defaultXRange = { 0.f, 1.f };
        m_defaultYRange = { 0.f, 1.f };

        m_scale          = 1.0;
        m_displacement = { 0.0, 0.0 };

        m_leftMouseReleased = false;
        m_rightMouseReleased = false;
        m_mouseClicked  = false;               
    }

    virtual void setSize( float x, float y ) override 
    {  
        Widget::setSize( x, y );
    }

    virtual void setPosition(  float x, float y ) override 
    { 
        Widget::setPosition( x, y ); 
    } 

    void setTitle( 
        const std::string & title )
    {
        m_title = title;
    }

    std::string title()  const { return m_title; }

    void leftMouseDrag( 
        const Vec2< float > & origin, 
        const Vec2< float > & last,
        const Vec2< float > & current )
    {

        if( m_probeType == ProbeType::Box )
        {
            if( current.x() > origin.x() )
            {
                m_boxLowerLeft.x( origin.x() );
                m_boxUpperRight.x( current.x() );
            }
            else 
            {
                m_boxLowerLeft.x( current.x() );
                m_boxUpperRight.x( origin.x() );
            }

            if( current.y() > origin.y() )
            {
                m_boxLowerLeft.y( origin.y() );
                m_boxUpperRight.y( current.y() );
            }
            else
            {
                m_boxLowerLeft.y( current.y() );
                m_boxUpperRight.y( origin.y() );
            }

            m_boxLowerLeft  = valueAt( m_boxLowerLeft );
            m_boxUpperRight = valueAt( m_boxUpperRight );
        }
        else if( m_probeType == ProbeType::Lasso )
        {
            bool appendPoint = false;

            if( m_lassoPolygone.size() == 0 )
            {
                appendPoint = true;
            }
            else
            {
                auto lastLassoPoint = m_lassoScreenSpace.back();
                if( lastLassoPoint.distance( current ) > 4 ) // 3 pixel min distance 
                {
                    appendPoint = true;
                }
            }

            if( appendPoint )
            {
                auto val = valueAt( current );
                m_lassoBBX.a( std::min( (float)val.x(), m_lassoBBX.a() ) );
                m_lassoBBX.b( std::max( (float)val.x(), m_lassoBBX.b() ) );         
                m_lassoBBY.a( std::min( (float)val.y(), m_lassoBBY.a() ) );
                m_lassoBBY.b( std::max( (float)val.y(), m_lassoBBY.b() ) );   
                m_lassoPolygone.push_back( val );
                m_lassoScreenSpace.push_back( current );
            }
        }
    }

    void rightMouseDrag( 
        const Vec2< double > & origin, 
        const Vec2< double > & last,
        const Vec2< double > & current )
    {
        auto dsp = ( last - current ) / (double) size().x();
        m_displacement.x( m_displacement.x() + dsp.x() );
        m_displacement.y( m_displacement.y() + dsp.y() );        
    }    
    
    void mouseOn()
    {

    }

    void mouseOff()
    {

    }

    ProbeType probeType() const 
    {
        return m_probeType;
    }

    void mouseWheel( float delta )
    {
        m_scale += delta;
    }

    bool probeActive() const { return m_probeOn; }

    void setProbeType( TN::ProbeType t )
    {
        m_probeType = t;
    }

    bool handleInput( 
        const InputState & input )
    {
        Vec2< double > p = input.mouseState.position;
        MouseButtonState mouseButtonState = input.mouseState.buttonState;
 
        bool changed = false;
        m_leftMouseReleased = false;
        m_rightMouseReleased = false;

        if( mouseOver( p ) )
        {
            if( input.mouseState.event == MouseEvent::LeftReleased )
            {
                m_leftMouseReleased = true;
            }
            if( ! m_stateInfo.mouseState.on )
            {
                mouseOn();
            }

            if( input.mouseState.event == MouseEvent::LeftPressed || input.mouseState.event == MouseEvent::RightPressed  )
            {
                m_stateInfo.mouseState.pos0 = input.mouseState.eventPosition;
                m_probeOn = false;
                if( input.mouseState.event == MouseEvent::LeftPressed )
                {
                    m_lassoPolygone.clear();
                    m_lassoScreenSpace.clear();
                    m_lassoBBX = { std::numeric_limits<float>::max(), -std::numeric_limits<float>::max() };
                    m_lassoBBY = m_lassoBBX;
                    m_mouseClicked = true;
                }
            }

            if( m_stateInfo.mouseState.rightButton && mouseButtonState == MouseButtonState::RightPressed )
            {
                rightMouseDrag( 
                    m_stateInfo.mouseState.pos0,
                    m_stateInfo.mouseState.pos, 
                    p );

                changed = true;
            }

            if( m_stateInfo.mouseState.leftButton && mouseButtonState == MouseButtonState::LeftPressed )
            {
                leftMouseDrag(
                    m_stateInfo.mouseState.pos0,
                    m_stateInfo.mouseState.pos, 
                    p ); 

                m_probeOn = true;
                changed = true;      
            }

            mouseWheel( -input.mouseState.scrollDelta() / 50.f );

            m_stateInfo.mouseState.leftButton  = mouseButtonState == MouseButtonState::LeftPressed;
            m_stateInfo.mouseState.rightButton = mouseButtonState == MouseButtonState::RightPressed;
            m_stateInfo.mouseState.pos = p; 
            m_stateInfo.mouseState.on = true;
        }
        else
        {
            if( m_stateInfo.mouseState.on )
            {
                mouseOff();
            }
            m_stateInfo.mouseState.on = false;
        }
        return changed;
    }

};

class ClusterView : public Widget
{

public:

    ProjectionPlot compProjection;
    ProjectionPlot cellProjection;
    ProjectionPlot cellProjection2;

    void applyLayout( const double ML )
    {
        double ml = ML;
        double mr = 10;
        double mt = 64;
        double sp = 20;

        double width = this->width() - ml - mr;
    
        compProjection.setSize( width, width );
        cellProjection.setSize( width, width );
        cellProjection2.setSize( width, width );

        compProjection.setPosition( 
            this->position().x() + ml, 
            this->position().y() + this->size().y() - width - mt );

        cellProjection.setPosition( 
            compProjection.position().x(), 
            compProjection.position().y() - width - sp );

        cellProjection2.setPosition( 
            compProjection.position().x(), 
            compProjection.position().y() - width*2 - sp*2 );
    }
};

}

#endif