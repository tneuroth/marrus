#ifndef TN_VIEW_3D_CONFIGURATION_PANEL_HPP
#define TN_VIEW_3D_CONFIGURATION_PANEL_HPP

#include "views/ConfigurationPanel.hpp"
#include "views/ComboWidget.hpp"
#include "views/PressButtonWidget.hpp"
#include "views/SliderWidget.hpp"
#include "geometry/Vec.hpp"
#include "algorithms/Standard/MyAlgorithms.hpp"
#include "configuration/GlyphConfiguration.hpp"

#include <cmath>
#include <memory>
#include <map>
#include <string>
#include <vector>

namespace TN {

class View3DConfigurationPanel : public ConfigurationPanel
{	
	std::map< std::string, std::unique_ptr< TN::TexturedPressButton > > m_surfaceSetVisibilityButtons;
	std::map< std::string ,TN::Label > m_surfaceSetVisibilityLabels;
	std::map< std::string, std::map< std::string, std::unique_ptr< TN::TexturedPressButton > > > m_surfaceVisibilityButtons;
	std::map< std::string, std::map< std::string, std::unique_ptr< TN::PressButton > > > m_surfaceColorButtons;
	std::map< std::string, std::map< std::string, TN::Label > > m_surfaceVisibilityLabels;

	TN::Label m_surfaceSetLabel;
    bool m_waitingToProcessChange;
	TN::PressButton m_applyButton;

	int m_numLayers;
	float m_scroll;

	//////////////////////////////////////////////////////////////////////////////////////////////////////

	// glyphs

	TN::Label m_glyphSetLabel;

	std::unique_ptr< TN::PressButton > m_loadGlyphButton;
	std::unique_ptr< TN::PressButton > m_loadGlyphTextureButton;

	std::unique_ptr< TN::TexturedPressButton > m_glyphSetVisibilityButton;
	std::unique_ptr< TN::TexturedPressButton > m_previewGlyphButton;

	std::vector< std::unique_ptr< TN::TexturedPressButton > > m_glyphVisibilityButtons;
	std::vector< TN::Label > m_glyphLayerLabels;

	std::unique_ptr< TN::TexturedPressButton > m_rotationNoneButton;
	std::unique_ptr< TN::TexturedPressButton > m_rotationViewButton;
	std::unique_ptr< TN::TexturedPressButton > m_rotationNormButton;

	TN::Label m_rotationLabel;
	TN::Label m_rotationNoneLabel;
	TN::Label m_rotationViewLabel;
	TN::Label m_rotationNormLabel;

	std::unique_ptr< TN::TexturedPressButton > m_colorModeTextureButton;
	std::unique_ptr< TN::TexturedPressButton > m_colorModeScalarButton;
	std::unique_ptr< TN::TexturedPressButton > m_colorModeSolidButton;

	TN::Label m_colorModeLabel;
	TN::Label m_colorModeTextureLabel;
	TN::Label m_colorModeScalarLabel;
	TN::Label m_colorModeSolidLabel;

	TN::Label m_colorModeConstLabel;
	std::unique_ptr< TN::TexturedPressButton > m_colorModeConstButton;

	TN::Label m_colorModeCompLabel;
	std::unique_ptr< TN::TexturedPressButton > m_colorModeCompButton;

	std::set<std::string> m_hiddenSurfaceSets;

	std::vector< std::unique_ptr< TN::PressButton > > m_glyphAxisColorButtons;

	std::vector< std::unique_ptr< TN::TexturedPressButton > > m_glyphAxisLabels;
	std::vector< std::unique_ptr< TN::TexturedPressButton > > m_glyphCompLabels;

	std::vector< std::string > m_glyphAxisLatexPaths;
	std::vector< std::string > m_glyphCompLatexPaths;

	TN::GlyphConfiguration m_currentGlyph;

	TN::Label m_glyphPreviewLabel;

	TN::SliderWidget m_glyphScaleSlider;
	float m_glyphScaleFactor;

	// Axis + Component as grid? ////////////////////////////////////////////////

	TN::Label m_axisSelectionLabel;
	TN::Label m_componentSelectionLabel;

	std::vector< std::unique_ptr< TN::TexturedPressButton > > m_axisSelectors;
	std::vector< std::unique_ptr< TN::TexturedPressButton > > m_componentSelectors;

	std::vector< std::unique_ptr< TN::TexturedPressButton > > m_glyphAxisSelectorLabels;
	std::vector< std::unique_ptr< TN::TexturedPressButton > > m_glyphCompSelectorLabels;

	/////////////////////////////////////////////////////////////////////////////

	TN::FileSelector * m_fileSelector;

	bool m_requestLoadGlyph;
	bool m_requestLoadGlyphTexture;
	bool m_requestUpdateGlyphPreview;

	std::string m_glyphPath;
	std::string m_glyphTexPath;

	std::string m_workingDir;

	//////////////////////////////////

	TN::Label m_showScatterPlotSelectionLabel;
	std::unique_ptr< TN::TexturedPressButton > m_showScatterPlotSelectionButton;

	TN::Label m_showAxesLabel;
	std::unique_ptr< TN::TexturedPressButton > m_showAxesButton;

	TN::Label m_lightBackgroundLabel;
	std::unique_ptr< TN::TexturedPressButton > m_lightBackgroundButton;


	//////////////////////////////////////////////////////////////////////////////////////////////////////

	TN::Label m_mappingOptionsLabel;

	TN::Label m_perBandLabel;
	std::unique_ptr< TN::TexturedPressButton > m_perBandButton;

	TN::Label m_perBinLabel;
	std::unique_ptr< TN::TexturedPressButton > m_perBinButton;

	TN::Label m_localNormLabel;
	std::unique_ptr< TN::TexturedPressButton > m_localNormButton;

	TN::Label m_normalizeFromZeroLabel;
	std::unique_ptr< TN::TexturedPressButton > m_normalizeFromZeroButton;

	TN::Label m_allAxisNormalizationLabel;
	std::unique_ptr< TN::TexturedPressButton > m_allAxisNormalizationButton;

	//////////////////////////////////////////////////////////////////////////////////////////////////////

	TN::Label m_scalingModeLabel;

	TN::Label m_linScaleLabel;
	std::unique_ptr< TN::TexturedPressButton > m_linScaleButton;

	TN::Label m_sqrtScaleLabel;
	std::unique_ptr< TN::TexturedPressButton > m_sqrtScaleButton;

	TN::Label m_logScaleLabel;
	std::unique_ptr< TN::TexturedPressButton > m_logScaleButton;

	//////////////////////////////////////////////////////////////////////////////////////////////////////

	TN::Label m_showBordersLabel;
	std::unique_ptr< TN::TexturedPressButton > m_showBordersButton;

	///////////////////////////////////////////////////////////////////////////////////////////////////////


	TN::ComboWidget m_glyphStyleCombo;


	////////////////////////////////////////////////////////

	inline std::string formatValue(double value) {
    std::stringstream ss;
    
    // For very small numbers or very large numbers, use scientific notation
    if (std::abs(value) < 0.01 || std::abs(value) >= 10000.0) {
        ss << std::scientific << std::setprecision(2) << value;
        // Convert e-00N to e-N and e+00N to eN for cleaner look
        std::string str = ss.str();
        size_t e_pos = str.find('e');
        if (e_pos != std::string::npos) {
            // Remove leading plus sign after 'e' if present
            if (str[e_pos + 1] == '+') {
                str.erase(e_pos + 1, 1);
            }
            // Remove leading zeros in exponent
            size_t exp_start = e_pos + 1;
            if (str[exp_start] == '-') exp_start++;
            while (exp_start < str.length() - 1 && str[exp_start] == '0') {
                str.erase(exp_start, 1);
            }
            return str;
        }
        return ss.str();
    } 
    // For numbers close to integers, don't show unnecessary decimals
    else if (std::abs(value - std::round(value)) < 1e-10) {
        ss << std::fixed << std::setprecision(0) << value;
        return ss.str();
    }
    // For regular numbers, use fixed notation with appropriate precision
    else {
        // Determine number of decimal places needed (max 4)
        int precision = 2;
        double testVal = std::abs(value);
        if (testVal < 1.0) precision = 3;
        if (testVal < 0.1) precision = 4;
        
        ss << std::fixed << std::setprecision(precision) << value;
        return ss.str();
    }
}

	void openObJFile() 
	{
		if( m_fileSelector != nullptr )
		{
			m_glyphPath = m_fileSelector->getFile( m_workingDir + "/models/domino_t.obj", "obj" );
		}
	}

	void openPNGFile() 
	{
		if( m_fileSelector != nullptr )
		{
			m_glyphTexPath = m_fileSelector->getFile( m_workingDir + "/models/domino_texture.png", "png" );
		}
	}

	public:

	void setFileSelector( FileSelector * fs, const std::string & wd ) 
	{
		m_fileSelector = fs;
		m_workingDir = wd;
	}

	virtual void clear() override {

		m_surfaceSetVisibilityButtons.clear();
		m_surfaceSetVisibilityLabels.clear();
		m_surfaceVisibilityButtons.clear();
		m_surfaceColorButtons.clear();
		m_surfaceVisibilityLabels.clear();
		m_waitingToProcessChange = false;
		m_glyphVisibilityButtons.clear();
		m_glyphLayerLabels.clear();
		m_axisSelectors.clear();
		m_componentSelectors.clear();
		m_glyphAxisSelectorLabels.clear();
		m_glyphCompLabels.clear();
		m_glyphAxisLabels.clear();
		m_glyphVisibilityButtons.clear();
		m_glyphLayerLabels.clear();

		m_numLayers = 0;
	}

	virtual void applyLayout() override {

		float y = position().y() + size().y() - 90;

		m_surfaceSetLabel.position = { position().x() + 20, y + m_scroll };

		y -= 40;

		for( auto & m : m_surfaceVisibilityButtons )
		{	
			auto setKey = m.first;

			if( m_hiddenSurfaceSets.count( setKey ) )
			{
				continue;
			}

			auto & b = m_surfaceSetVisibilityButtons.at( setKey );
			auto & l = m_surfaceSetVisibilityLabels.at(  setKey );

			b->setPosition( position().x() + 20, y + m_scroll );
			l.position = { position().x()  + 64,  y + 8 + m_scroll };

			y -= 34;

			for( auto & e : m.second )
			{
				auto surfKey = e.first;

				auto & vB = m_surfaceVisibilityButtons.at( setKey ).at( surfKey );
				auto & vL = m_surfaceVisibilityLabels.at(  setKey ).at( surfKey );
				auto & cB = m_surfaceColorButtons.at(      setKey ).at( surfKey );
			
				vB->setPosition( position().x() + 50, y  + m_scroll );
				cB->setPosition( position().x() + 90,  y  + m_scroll );
				vL.position = {  position().x() + 140,  y + 8 + m_scroll };

				y -= 34;
			}
		}

		m_glyphSetLabel.position = { position().x() + 20, y + 8 + m_scroll };
		y-=34;
		m_glyphSetVisibilityButton->setPosition( position().x() + 20, y + m_scroll );

		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		m_previewGlyphButton->setPosition( 
			position().x() + size().x() - m_previewGlyphButton->size().x() - 10, 
			y - m_previewGlyphButton->size().y() + 10 + m_scroll );

		m_glyphPreviewLabel.position = {  
			m_previewGlyphButton->position().x() + 10,  
			m_previewGlyphButton->position().y() + m_previewGlyphButton->size().y() - 23  };

		if( m_currentGlyph.glyph_type == "radial" )
		{
			m_glyphPreviewLabel.position = {  
				m_previewGlyphButton->position().x() + 10,  
				m_previewGlyphButton->position().y() + m_previewGlyphButton->size().y() - 23  };

			const size_t nAxes = m_currentGlyph.axes.size();
			const size_t NAX   = m_currentGlyph.active_axes.size();

			if( nAxes == m_glyphAxisLabels.size() )
			{
				for( size_t i = 0; i < NAX; ++ i )
				{
					float dt = ( 2 * M_PI ) / NAX;

    				float rx = -std::cos( i * ( dt ) + dt / 2 - M_PI / 2.0 );
    				float ry =  std::sin( i * ( dt ) + dt / 2 - M_PI / 2.0 );

    				float cx = m_previewGlyphButton->position().x() + m_previewGlyphButton->size().x() / 2.0;
    				float cy = m_previewGlyphButton->position().y() + m_previewGlyphButton->size().y() / 1.73;

    				float ux = m_previewGlyphButton->size().x() / 2.75; 
    				float uy = m_previewGlyphButton->size().y() / 3.55;

    				m_glyphAxisLabels[ m_currentGlyph.active_axes[ i ] ]->setPosition( cx + rx*ux - 44, cy + ry*uy - 22  ); 
				}
			}
			else
			{
				std::cerr << "mismatch in number of labels and axis" << std::endl;
			}
		}
		else if ( m_currentGlyph.glyph_type == "star" )
		{
			const size_t nAxes = m_currentGlyph.axes.size();
			const size_t NAX   = m_currentGlyph.active_axes.size();

			if( nAxes == m_glyphAxisLabels.size() )
			{
				for( size_t i = 0; i < NAX; ++ i )
				{
    				float rx = -std::cos( i * ( ( 2 * M_PI ) / NAX ) - M_PI / 2.0 );
    				float ry =  std::sin( i * ( ( 2 * M_PI ) / NAX ) - M_PI / 2.0 );

    				float cx = m_previewGlyphButton->position().x() + m_previewGlyphButton->size().x() / 2.0;
    				float cy = m_previewGlyphButton->position().y() + m_previewGlyphButton->size().y() / 1.73;
    				float ux = m_previewGlyphButton->size().x() / 2.75; 
    				float uy = m_previewGlyphButton->size().y() / 3.55;

    				m_glyphAxisLabels[ m_currentGlyph.active_axes[ i ] ]->setPosition( cx + rx*ux - 44, cy + ry*uy - 22  ); 
				}
			}
			else
			{
				std::cerr << "mismatch in number of labels and axis" << std::endl;
			}
		}
		else if ( m_currentGlyph.glyph_type == "directional" )
		{
			const size_t nAxes = m_currentGlyph.vectors.size();
			if( nAxes == m_glyphAxisLabels.size() )
			{
				float dx = 0.9 * ( m_previewGlyphButton->size().x() ) / ( nAxes );

				for( size_t i = 0; i < nAxes; ++ i )
				{
    				float x = m_previewGlyphButton->position().x() + 28 + i * dx;
    				float y = m_previewGlyphButton->position().y() + 60;

    				m_glyphAxisColorButtons[ i ]->setPosition( 
    					x + ( m_glyphAxisLabels[ i ]->size().x() ) / 2 - m_glyphAxisColorButtons[ i ]->size().x() / 2, 
    					y - m_glyphAxisLabels[ i ]->size().y() - 10 + m_scroll );

    				m_glyphAxisLabels[ i ]->setPosition( x, y + m_scroll );
				}
			}
			else
			{
				std::cerr << "mismatch in number of labels and axis" << std::endl;
			}
		}


		m_glyphScaleSlider.setSize( 
			size().x() - 80, 
			30 );
		
		if( m_axisSelectors.size() > 0 )
		{
			m_glyphScaleSlider.setPosition( 
				position().x() + 60, 
				std::min( 
					m_previewGlyphButton->position().y() - 46, 
					m_axisSelectors.back()->position().y() - 46 )  );
		}
		else
		{
			m_glyphScaleSlider.setPosition( 
				position().x() + 60, m_previewGlyphButton->position().y() - 46  );
		}

		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		float xoffset = position().x() + size().x() - m_previewGlyphButton->size().x() - 20 - m_loadGlyphButton->size().x() - 10;

		int row_id = m_surfaceVisibilityButtons.size() > 0 ? 0 : -1;

		if( m_currentGlyph.glyph_type == "experimental" )
		{
			m_loadGlyphButton->setPosition(        xoffset, y - 34 * (row_id++) + m_scroll );
			m_loadGlyphTextureButton->setPosition( xoffset, y - 34 * (row_id++) + m_scroll );
		}

		xoffset = position().x() + size().x() - m_previewGlyphButton->size().x() - 20 - m_colorModeTextureButton->size().x() - 10;
		
		// Color Mode

		if(  m_currentGlyph.glyph_type == "experimental"  )
		{
			m_colorModeTextureButton->setPosition( xoffset, y - 34 * (row_id++) + m_scroll );
			m_colorModeScalarButton->setPosition(  xoffset, y - 34 * (row_id++) + m_scroll );
			m_colorModeSolidButton->setPosition(   xoffset, y - 34 * (row_id++) + m_scroll );
		}

		float lxoffset = xoffset - m_colorModeTextureButton->size().x() - 130;
		m_colorModeLabel.position       =  { lxoffset, y - 34 * row_id + m_scroll };

		if( m_currentGlyph.glyph_type == "experimental" )
		{
			m_colorModeTextureLabel.position = { lxoffset, m_colorModeTextureButton->position().y() + 8 };
			m_colorModeScalarLabel.position  = { lxoffset, m_colorModeScalarButton->position().y() + 8  };
			m_colorModeSolidLabel.position   = { lxoffset, m_colorModeSolidButton->position().y() + 8   };
		}

		// orientation mode

		if( m_currentGlyph.glyph_type == "radial" || m_currentGlyph.glyph_type == "star" )
		{
			m_glyphStyleCombo.setPosition( lxoffset + 40, y - 34 * (row_id++) + m_scroll );
			m_glyphStyleCombo.setSize( 150, 30 );
		}

		m_rotationLabel.position = { lxoffset, y - 34 * (row_id++) + m_scroll };

		if( m_currentGlyph.glyph_type == "experimental" )
		{
			m_rotationNoneButton->setPosition( xoffset, y - 34 * (row_id++) + m_scroll );
		}		

		m_rotationViewButton->setPosition(     xoffset, y - 34 * (row_id++) + m_scroll );
		m_rotationNormButton->setPosition(     xoffset, y - 34 * (row_id++) + m_scroll );

		m_rotationNoneLabel.position = { lxoffset + 10, m_rotationNoneButton->position().y() + 8  };
		m_rotationViewLabel.position = { lxoffset + 10, m_rotationViewButton->position().y() + 8  };
		m_rotationNormLabel.position = { lxoffset + 10, m_rotationNormButton->position().y() + 8  };

		m_mappingOptionsLabel.position = { lxoffset, m_rotationNormButton->position().y() - 22  };

		m_normalizeFromZeroButton->setPosition(  xoffset,    m_mappingOptionsLabel.position.y() - 34   );
		m_localNormButton->setPosition(          xoffset,    m_mappingOptionsLabel.position.y() - 2*34 );
		m_perBandButton->setPosition(            xoffset,    m_mappingOptionsLabel.position.y() - 3*34 );
		m_perBinButton->setPosition(             xoffset,    m_mappingOptionsLabel.position.y() - 4*34 );
		m_allAxisNormalizationButton->setPosition(  xoffset, m_mappingOptionsLabel.position.y() - 5*34 );

		m_normalizeFromZeroLabel.position = { lxoffset + 10, m_normalizeFromZeroButton->position().y() + 8 };
		m_localNormLabel.position = { lxoffset + 10, m_localNormButton->position().y() + 8  };
		m_perBandLabel.position   = { lxoffset + 10, m_perBandButton->position().y()   + 8  };
		m_perBinLabel.position    = { lxoffset + 10, m_perBinButton->position().y()    + 8  };
		m_allAxisNormalizationLabel.position    = { lxoffset + 10, m_allAxisNormalizationButton->position().y()    + 8  };

		m_scalingModeLabel.position = { lxoffset, m_allAxisNormalizationButton->position().y() - 22 };

		float spacing = 34;

		m_linScaleButton->setPosition(  lxoffset + spacing - 10,  m_allAxisNormalizationButton->position().y() - 34 - 30  );
		m_sqrtScaleButton->setPosition( lxoffset + spacing - 2 + (m_linScaleButton->size().x()+spacing),   m_allAxisNormalizationButton->position().y() - 34 - 30  );
		m_logScaleButton->setPosition(  lxoffset + spacing - 2 + (m_linScaleButton->size().x()+spacing)*2, m_allAxisNormalizationButton->position().y() - 34 - 30  );

		m_showBordersButton->setPosition(  lxoffset + spacing - 2 + (m_linScaleButton->size().x()+spacing)*2, m_logScaleButton->position().y() - 40  );

		m_linScaleLabel.position     = {  m_linScaleButton->position().x() - 27, m_linScaleButton->position().y() + 8 };
		m_sqrtScaleLabel.position    = {  m_sqrtScaleButton->position().x() -36, m_linScaleButton->position().y() + 8 };
		m_logScaleLabel.position     = {  m_logScaleButton->position().x() - 27, m_linScaleButton->position().y() + 8 };
		m_showBordersLabel.position  = {   lxoffset , m_showBordersButton->position().y()    + 8  };

		y -= 34 + 40;

		for( int i = 0; i < m_glyphVisibilityButtons.size(); ++i )
		{
			m_glyphVisibilityButtons[ i ]->setPosition( position().x() + 50, y + 40 + m_scroll );
			m_glyphLayerLabels[ i ].position = { position().x()  + 90,  y + 8 + 40 + m_scroll  };
            y -= 34;
		}

		////////////////////////////////////////////////////////////////////////////

		y -= 10;

		y = std::min( y, m_showBordersLabel.position.y() - 34 );

		m_axisSelectionLabel.position      = { position().x() +  50, y + m_scroll };
		m_componentSelectionLabel.position = { position().x() + 220, y + m_scroll };		

		y -= 44;

		if( m_currentGlyph.glyph_type == "radial" || m_currentGlyph.glyph_type == "star" || m_currentGlyph.glyph_type == "directional" )
		{
			for( int i = 0; i < m_axisSelectors.size(); ++i )
			{
				m_axisSelectors[ i ]->setPosition( position().x() + 50, y - 40 * i  + m_scroll );

			    m_glyphAxisSelectorLabels[ i ]->setPosition( 
			    	m_axisSelectors[ i ]->position().x() + m_axisSelectors[ i ]->size().x() + 20, 
			    	y - 40 * i  + m_scroll  );
			}

			for( int i = 0; i < m_componentSelectors.size(); ++i )
			{
				m_componentSelectors[ i ]->setPosition( 
					position().x() + 220, 
					y - 40 * i + m_scroll );

			    m_glyphCompSelectorLabels[ i ]->setPosition( 
			    	m_componentSelectors[ i ]->position().x() + m_componentSelectors[ i ]->size().x() + 20, 
			    	y - 40 * i  + m_scroll );
			}
		}

		/////////////////////////////////////////////////////////////////////////////

		m_showScatterPlotSelectionButton->setPosition( 
			position().x() + 190, 
			m_glyphScaleSlider.position().y() - 88  
		);

		m_showAxesButton->setPosition( 
			position().x() + 190, 
			m_showScatterPlotSelectionButton->position().y() - 34 
		);

		m_showScatterPlotSelectionLabel.position = { 
			position().x() + 30.f, 
			m_showScatterPlotSelectionButton->position().y() + 8 };

		m_showAxesLabel.position = { 
			position().x() + 30.f, 
			m_showAxesButton->position().y() + 8 };

		m_lightBackgroundButton->setPosition( 
			position().x() + 190, 
			m_showAxesButton->position().y() - 34  
		);

		m_lightBackgroundLabel.position = { 
			position().x() + 30.f, 
			m_lightBackgroundButton->position().y() + 8  };

		m_applyButton.setPosition( 
			position().x() + size().x() - m_applyButton.size().x() - 20, 
			m_lightBackgroundButton->position().y() - 84 );
	}

	virtual bool handleInput( const InputState & input ) override
	{
    	Vec2< double > pos = input.mouseState.position;

    	bool changed = false;

		if( pointInViewPort( pos ) )
		{
			if( std::abs( input.mouseState.scrollDelta() ) > 0 )
	        { 
	        	float prev = m_scroll;

	        	m_scroll -= input.mouseState.scrollDelta() * 20.f;
	        	
	        	m_scroll = std::min( 
	        		( ( position().y() + size().y() ) - ( m_applyButton.position().y() - 200.f ) ) - size().y(), 
	        		m_scroll );
	        	
	        	m_scroll = std::max( m_scroll, 0.f );

	        	if( std::abs( prev - m_scroll ) > 0.1 )
	        	{
					changed = true;
	        	}
	        }
		}

	    auto comb = combos();
	    bool existsPressedCombo = false;

		for( auto & c : comb ) 
		{
			if( c->isPressed() )
			{
			    existsPressedCombo = true;
			    break;
			}
		}

	    if( input.mouseState.event == MouseEvent::LeftPressed ) {
		    for( auto & c : comb ) 
		    {
				if( c->pointInViewPort( pos ) )
				{
				    if( ! existsPressedCombo && ! c->isPressed() )
				    {
				        c->setPressed( true );
				        break;
				    }
				    else if( c->isPressed() )
				    {
				        int idx = c->pressed( pos );

				        if( c->text() == "Style" && c->selectedItemText() == "star" )
				        {
				        	for( auto & cs : m_componentSelectors )
				        	{
				        		cs->setPressed( false );
				        	}

				        	if( m_componentSelectors.size() > 0 )
				        	{
				        		m_componentSelectors[ 0 ]->setPressed( true );
				        	}
				        }

				        c->setPressed( false );
				        break;
				    }
				}
		    }
		}
		else
		{
			for( auto & c : comb ) 
			{
			    if( c->isPressed() )
			    {
			        int id = c->mousedOver( pos );
			        if( id == -1 )
			        {
			            c->setPressed( false );
			        }
			        break;
			    }
			}
		}

		for( auto & m : m_surfaceVisibilityButtons )
		{	
			auto setKey = m.first;
			
			if( m_hiddenSurfaceSets.count( setKey ) )
			{
				continue;
			}

			auto & b = m_surfaceSetVisibilityButtons.at( setKey );

			if( b->pointInViewPort( pos ) && input.mouseState.event == MouseEvent::LeftPressed ) {
				b->setPressed( ! b->isPressed() );
			}

			for( auto & e : m.second )
			{
				auto surfKey = e.first;

				auto & vB = m_surfaceVisibilityButtons.at( setKey ).at( surfKey );
				auto & cB = m_surfaceColorButtons.at(      setKey ).at( surfKey );
			
				if( vB->pointInViewPort( pos ) && input.mouseState.event == MouseEvent::LeftPressed ) {
					vB->setPressed( ! vB->isPressed() );
				}

				if( cB->pointInViewPort( pos ) && input.mouseState.event == MouseEvent::LeftPressed ) {
					// TODO: color picker
				}
			}
		}

		if( m_numLayers > 0 )
		{
			if( m_glyphSetVisibilityButton->pointInViewPort( pos ) && input.mouseState.event == MouseEvent::LeftPressed ) {
				m_glyphSetVisibilityButton->setPressed( ! m_glyphSetVisibilityButton->isPressed() );
			}

			for( auto & b : m_glyphVisibilityButtons )
			{
				if( b->pointInViewPort( pos ) && input.mouseState.event == MouseEvent::LeftPressed ) {
					b->setPressed( ! b->isPressed() );
				}
			}

			if( m_loadGlyphButton->pointInViewPort( pos ) && input.mouseState.event == MouseEvent::LeftPressed  )
			{
				openObJFile();
				m_requestLoadGlyph = true;
			}

			if( m_loadGlyphTextureButton->pointInViewPort( pos ) && input.mouseState.event == MouseEvent::LeftPressed  )
			{
				openPNGFile();
				m_requestLoadGlyphTexture = true;
			}

			std::vector< TN::TexturedPressButton * > roBtns = 

				m_currentGlyph.glyph_type == "experimental" ?
			std::vector< TN::TexturedPressButton * >( {   
				m_rotationNormButton.get(),
				m_rotationNoneButton.get(),
				m_rotationViewButton.get()
			} ) : 
			std::vector< TN::TexturedPressButton * >( {
				m_rotationNormButton.get(),
				m_rotationViewButton.get()
			} );

			// Orientation Mode radios

			int selected = -1;
			for( int i = 0; i < roBtns.size(); ++i )
			{
				if( roBtns[ i ]->pointInViewPort( pos ) && input.mouseState.event == MouseEvent::LeftPressed  )
				{
					if( ! roBtns[ i ]->isPressed() )
					{
						selected = i;
						roBtns[ i ]->setPressed( true );
					}
				}
			}
			if( selected >= 0 )
			{
				m_requestUpdateGlyphPreview = true;
				for( int i = 0; i < roBtns.size(); ++i )
				{
					if( i != selected )
					{
						roBtns[ i ]->setPressed( false );
					}
				}
			}

			//////////////////////////////////////////////////////////

			std::vector< TN::TexturedPressButton * > scaleBtns = {   
				m_linScaleButton.get(),
				m_sqrtScaleButton.get(),
				m_logScaleButton.get()
			};

			// Orientation Mode radios

			selected = -1;
			for( int i = 0; i < scaleBtns.size(); ++i )
			{
				if( scaleBtns[ i ]->pointInViewPort( pos ) && input.mouseState.event == MouseEvent::LeftPressed  )
				{
					if( ! scaleBtns[ i ]->isPressed() )
					{
						selected = i;
						scaleBtns[ i ]->setPressed( true );
					}
				}
			}
			if( selected >= 0 )
			{
				m_requestUpdateGlyphPreview = true;
				for( int i = 0; i < scaleBtns.size(); ++i )
				{
					if( i != selected )
					{
						scaleBtns[ i ]->setPressed( false );
					}
				}
			}

			//////////////////////////////////////////////////////////

			// Color Mode radios

			std::vector< TN::TexturedPressButton * > cmBtns = 

				m_currentGlyph.glyph_type == "experimental" ?
				
				std::vector< TN::TexturedPressButton * >( {   
					m_colorModeTextureButton.get(),
					m_colorModeScalarButton.get(),
					m_colorModeSolidButton.get()
			} ) : 

			std::vector< TN::TexturedPressButton * >( {  
				
			} );	

		    selected = -1;
			for( int i = 0; i < cmBtns.size(); ++i )
			{
				if( cmBtns[ i ]->pointInViewPort( pos ) && input.mouseState.event == MouseEvent::LeftPressed  )
				{
					if( ! cmBtns[ i ]->isPressed() )
					{
						selected = i;
						cmBtns[ i ]->setPressed( true );
					}
				}
			}
			if( selected >= 0 )
			{
				m_requestUpdateGlyphPreview = true;
				for( int i = 0; i < cmBtns.size(); ++i )
				{
					if( i != selected )
					{
						cmBtns[ i ]->setPressed( false );
					}
				}
			}
		}

		if( m_applyButton.pointInViewPort( pos ) ) {
			if ( input.mouseState.event == MouseEvent::LeftPressed )
			{
				m_waitingToProcessChange = true;
			}
		}

		// Slider 

		if( m_glyphScaleSlider.overSliderHandle( pos ) && input.mouseState.event == MouseEvent::LeftPressed )
		{
			m_glyphScaleSlider.setPressed( true );
		}
		if( m_glyphScaleSlider.isPressed() )
	    {
	        m_glyphScaleSlider.translate( pos );
	    }
		if( input.mouseState.event == MouseEvent::LeftReleased )
		{
			m_glyphScaleSlider.setPressed( false );
		}

		if( m_showScatterPlotSelectionButton->pointInViewPort( pos ) && input.mouseState.event == MouseEvent::LeftPressed  )
		{
			m_showScatterPlotSelectionButton->setPressed( ! m_showScatterPlotSelectionButton->isPressed() );
		}

		if( m_showAxesButton->pointInViewPort( pos ) && input.mouseState.event == MouseEvent::LeftPressed  )
		{
			m_showAxesButton->setPressed( ! m_showAxesButton->isPressed() );
		}

		if( m_localNormButton->pointInViewPort( pos ) && input.mouseState.event == MouseEvent::LeftPressed  )
		{
			m_localNormButton->setPressed( ! m_localNormButton->isPressed() );

			if( m_localNormButton->isPressed() && m_currentGlyph.glyph_type == "star" ) 
			{
				m_perBinLabel.text  = "No Effect";
				m_perBandLabel.bold = false;
			}
			else
			{
				m_perBinLabel.text  = "(Per|All) Scale Bins";
				m_perBinLabel.bold  = true;
			}
		}


		if( m_allAxisNormalizationButton->pointInViewPort( pos ) && input.mouseState.event == MouseEvent::LeftPressed  )
		{
			m_allAxisNormalizationButton->setPressed( ! m_allAxisNormalizationButton->isPressed() );
		}

		if( m_normalizeFromZeroButton->pointInViewPort( pos ) && input.mouseState.event == MouseEvent::LeftPressed  )
		{
			m_normalizeFromZeroButton->setPressed( ! m_normalizeFromZeroButton->isPressed() );
		}

		if( m_perBandButton->pointInViewPort( pos ) && input.mouseState.event == MouseEvent::LeftPressed  )
		{
			m_perBandButton->setPressed( ! m_perBandButton->isPressed() );
		}

		if( m_showBordersButton->pointInViewPort( pos ) && input.mouseState.event == MouseEvent::LeftPressed  )
		{
			m_showBordersButton->setPressed( ! m_showBordersButton->isPressed() );
		}

		if( m_perBinButton->pointInViewPort( pos ) && input.mouseState.event == MouseEvent::LeftPressed  )
		{
			m_perBinButton->setPressed( ! m_perBinButton->isPressed() );
		}

		if( m_lightBackgroundButton->pointInViewPort( pos ) && input.mouseState.event == MouseEvent::LeftPressed  )
		{
			m_lightBackgroundButton->setPressed( ! m_lightBackgroundButton->isPressed() );
		}

		if( m_currentGlyph.glyph_type == "star" || m_currentGlyph.glyph_type == "radial" || m_currentGlyph.glyph_type == "directional" )
		{
			for( auto & b : m_axisSelectors )
			{
				if( b->pointInViewPort( pos ) && input.mouseState.event == MouseEvent::LeftPressed )
				{
					b->setPressed( ! b->isPressed() );
				}
			}

			if( m_currentGlyph.glyph_type == "radial" )
			{
				for( auto & b : m_componentSelectors )
				{
					if( b->pointInViewPort( pos ) && input.mouseState.event == MouseEvent::LeftPressed )
					{
						b->setPressed( ! b->isPressed() );
					}
				}
			}
			else if( "star" )
			{	
				int selected = -1;
				for( int i = 0; i < m_componentSelectors.size(); ++i )
				{
					auto & b = m_componentSelectors[ i ];
					if( b->pointInViewPort( pos ) && input.mouseState.event == MouseEvent::LeftPressed )
					{
						selected = i;
					}
				}

				if( selected >= 0 )
				{
					for( int i = 0; i < m_componentSelectors.size(); ++i )
					{
						if( i == selected )
						{
							m_componentSelectors[ i ]->setPressed( true );
						}
						else
						{
							m_componentSelectors[ i ]->setPressed( false );
						}
					}
				}
			}
		}

		if( changed )
		{
			applyLayout();
		}

		return changed;
	}

	void updateInformation( 
		const TN::SurfaceSetCollection & ssc,
		const std::string & baseDir,
		const std::string & projDir,
		const std::vector< std::array< float, 2 > > & layers,
		const TN::GlyphConfiguration & glyphConfig,
		const std::vector< TN::GlyphConfiguration > & glyphConfigurations )
	{	
		////////////////////////////////////////////////////////////////////////////////

		// Glyph stuff

		std::vector< std::string > glyphAxisLatexPaths = glyphConfig.getAxesLatexPaths( projDir + "/latex_symbols/" );
		std::vector< std::string > glyphCompLatexPaths = glyphConfig.getCompLatexPaths( projDir + "/latex_symbols/" );

		if( glyphAxisLatexPaths != m_glyphAxisLatexPaths )
		{
			// need to recreate the labels

			////////////////////////////////////////////////////////////////////////////////////////

			m_glyphAxisLatexPaths = glyphAxisLatexPaths;

			while( m_glyphAxisLabels.size() < glyphAxisLatexPaths.size() )
			{
				m_glyphAxisLabels.push_back( std::unique_ptr< TN::TexturedPressButton >(
					new TN::TexturedPressButton() ) );

				m_glyphAxisColorButtons.push_back( std::unique_ptr< TN::PressButton >(
					new TN::PressButton() ) );

				m_glyphAxisSelectorLabels.push_back( std::unique_ptr< TN::TexturedPressButton >(
					new TN::TexturedPressButton() ) );
			}
			while( m_glyphAxisLabels.size() > glyphAxisLatexPaths.size() )
			{
				m_glyphAxisLabels.pop_back();
				m_glyphAxisColorButtons.pop_back();
				m_glyphAxisSelectorLabels.pop_back();
			}

			for( size_t i = 0; i < m_glyphAxisLatexPaths.size(); ++i )
			{
				m_glyphAxisLabels[ i ]->setTexFromPNG(  
					m_glyphAxisLatexPaths[ i ], 
					m_glyphAxisLatexPaths[ i ] 
				);

				m_glyphAxisSelectorLabels[ i ]->setTexFromPNG(  
					m_glyphAxisLatexPaths[ i ], 
					m_glyphAxisLatexPaths[ i ] 
				);

				if( glyphConfig.glyph_type == "directional" )
				{
					m_glyphAxisColorButtons[ i ]->color( glyphConfig.vectorColors[ i ] );
					m_glyphAxisColorButtons[ i ]->setClearColor( true );
				}

				m_glyphAxisLabels[ i ]->resizeByHeight( 30 );
				m_glyphAxisColorButtons[ i ]->resize( 30, 30 );	
				m_glyphAxisSelectorLabels[ i ]->resizeByHeight( 30 );
			}

			m_glyphAxisLatexPaths = m_glyphAxisLatexPaths;
		}

		if( glyphCompLatexPaths != m_glyphCompLatexPaths )
		{

			m_glyphCompLatexPaths = glyphCompLatexPaths;
			while( m_glyphCompLabels.size() < glyphCompLatexPaths.size() )
			{
				m_glyphCompLabels.push_back( std::unique_ptr< TN::TexturedPressButton >(
					new TN::TexturedPressButton() ) );

				m_glyphCompSelectorLabels.push_back( std::unique_ptr< TN::TexturedPressButton >(
					new TN::TexturedPressButton() ) );
			}
			while( m_glyphCompLabels.size() > glyphCompLatexPaths.size() )
			{
				m_glyphCompLabels.pop_back();
				m_glyphCompSelectorLabels.pop_back();
			}

			for( size_t i = 0; i < m_glyphCompLatexPaths.size(); ++i )
			{
				m_glyphCompLabels[ i ]->setTexFromPNG(  
					m_glyphCompLatexPaths[ i ], 
					m_glyphCompLatexPaths[ i ] 
				);

				m_glyphCompLabels[ i ]->resizeByHeight( 30 );

				m_glyphCompSelectorLabels[ i ]->setTexFromPNG(  
					m_glyphCompLatexPaths[ i ], 
					m_glyphCompLatexPaths[ i ] 
				);

				m_glyphCompSelectorLabels[ i ]->resizeByHeight( 30 );
			}

			m_glyphCompLatexPaths = glyphCompLatexPaths;
		}

		//////////////////////////////////////////////////////////////////////////////////////////////

		m_currentGlyph = glyphConfig;

		////////////////////////////////////////////////////////////////////////////////////////////////

		int nLayers = layers.size();

		const std::string Radio        = baseDir + "/textures/radio.png";
		const std::string RadioPressed = baseDir + "/textures/radioPressed.png";

		const auto & surfaceSets = ssc.surfaceSets();

		// Detect new surface sets /////////////////////////////////////////

		std::set< std::string > newKeys;
		for( const auto & surfaceSet : surfaceSets )
		{
			if( ! m_surfaceSetVisibilityButtons.count( surfaceSet.first ) )
			{
				newKeys.insert( surfaceSet.first );
				std::cout << "adding surface set key " << surfaceSet.first  << std::endl;
			}
		}

		///// Add Buttons for Each new Surface set and surace within the set //////////

		for( auto & key : newKeys )
		{
			const auto & surfaceSet = surfaceSets.at( key );

			m_surfaceSetVisibilityButtons.insert( { key, std::unique_ptr<TN::TexturedPressButton>( new TN::TexturedPressButton() ) } );

			m_surfaceSetVisibilityButtons.at( key )->setTexFromPNG( Radio, RadioPressed );
	        m_surfaceSetVisibilityButtons.at( key )->resizeByHeight( 30 );

			m_surfaceSetVisibilityLabels.insert( { key, TN::Label() } );
			m_surfaceSetVisibilityLabels.at( key ).text = key;

			// the surfaces within the set

			m_surfaceVisibilityButtons.insert( { key, std::map< std::string, std::unique_ptr< TN::TexturedPressButton > >() } );
			m_surfaceVisibilityLabels.insert(  { key, { } } );
			m_surfaceColorButtons.insert(      { key, std::map< std::string, std::unique_ptr< TN::PressButton > >() } );

			auto & btns = m_surfaceVisibilityButtons.at( key );
			auto & lbls = m_surfaceVisibilityLabels.at(  key );
			auto & cbs  = m_surfaceColorButtons.at(      key );

			for( const auto & s : surfaceSet->surfaces )
			{
				const auto & surface = s.second;

				std::cout << "adding surface " << s.first << std::endl;

				btns.insert( { s.first, std::unique_ptr<TN::TexturedPressButton>( new TN::TexturedPressButton() ) } );
				btns.at( s.first )->setTexFromPNG( Radio, RadioPressed );
		        btns.at( s.first )->resizeByHeight( 30 );

				cbs.insert( { s.first, std::unique_ptr<TN::PressButton>( new TN::PressButton() ) } );
				cbs.at( s.first )->color( { surface.color.r, surface.color.g, surface.color.b } );
				cbs.at( s.first )->resize( 30, 30 );
				cbs.at( s.first )->setClearColor( true );

				lbls.insert( { s.first, TN::Label() } );
				lbls.at( s.first ).text =  s.first;
			}
		}

		// ////// Remove surface sets which no longer exist //////////////////////////////////

		std::set< std::string > oldKeys;

		for( const auto & sb : m_surfaceSetVisibilityButtons )
		{
			if( ! surfaceSets.count( sb.first ) )
			{
				std::cout << "removing surface set key " << sb.first;
				oldKeys.insert( sb.first );
			}
		}

		for( auto & key : oldKeys )
		{
			m_surfaceSetVisibilityButtons.erase( key );
			m_surfaceSetVisibilityLabels.erase( key );
		}

		// ///////////////////////////////////////////////////////////////////////////

		// // for each surace set, make sure the surfaces within it match

		for( const auto & ss : surfaceSets )
		{
			auto & btns = m_surfaceVisibilityButtons.at( ss.first );
			auto & lbls = m_surfaceVisibilityLabels.at(  ss.first );
			auto & cbs  = m_surfaceColorButtons.at(      ss.first );

			const auto & surfaceSet = ss.second;

			// // add new surfaces /////////////////////////////////
			for( const auto & s : surfaceSet->surfaces )
			{
				const auto & surface = s.second;
				if( ! btns.count( surface.name ) )
				{
					btns.insert( { surface.name, std::unique_ptr<TN::TexturedPressButton>( new TN::TexturedPressButton() ) } );
					btns.at( surface.name )->setTexFromPNG( Radio, RadioPressed );
			        btns.at( surface.name )->resizeByHeight( 30 );

					cbs.insert( { s.first, std::unique_ptr<TN::PressButton>( new TN::PressButton() ) } );
					cbs.at( s.first )->color( { surface.color.r, surface.color.g, surface.color.b } );
					cbs.at( s.first )->resize( 30, 30 );
					cbs.at( s.first )->setClearColor( true );

					lbls.insert( { s.first, TN::Label() } );
					lbls.at( s.first ).text = s.first;
				}
			}	

			// remove old surfaces //////////////////////////////

			std::set< std::string > oldSurfaceKeys;

			for( const auto & btn : btns )
			{
				if( ! surfaceSet->surfaces.count( btn.first ) ) {
					oldSurfaceKeys.insert( btn.first );
				}
			}

			for( auto & key : oldSurfaceKeys )
			{
				btns.erase( key );
				lbls.erase( key );
				cbs.erase(  key );
			}

			/////////////////////////////////////////////////////
		}

		// glyphs

		bool layersChanged = m_numLayers != nLayers;
		
		while( m_glyphVisibilityButtons.size() > nLayers )
		{
			m_glyphVisibilityButtons.pop_back();
			m_glyphLayerLabels.pop_back();
		}
		
		while( m_glyphVisibilityButtons.size() < nLayers )
		{
			m_glyphVisibilityButtons.push_back( std::unique_ptr<TN::TexturedPressButton>( new TN::TexturedPressButton() ) );
			m_glyphVisibilityButtons.back()->setTexFromPNG( Radio, RadioPressed );
		    m_glyphVisibilityButtons.back()->resizeByHeight( 30 );
			m_glyphLayerLabels.push_back( TN::Label() );
		}

		for( int i = 0; i < nLayers; ++i )
		{
			m_glyphLayerLabels[ i ].text = "Band: [" 
				+ formatValue( layers[ i ][ 0 ] )
				+ ","
				+ formatValue( layers[ i ][ 1 ] ) 
				+ ")";
		}

		// TODO: no need to reset them 
		if( m_numLayers != nLayers )
		{
			m_showScatterPlotSelectionButton->setTexFromPNG( Radio, RadioPressed );
			m_showScatterPlotSelectionButton->resizeByHeight( 30 );
			m_showScatterPlotSelectionButton->setPressed( false );

			m_showAxesButton->setTexFromPNG( Radio, RadioPressed );
			m_showAxesButton->resizeByHeight( 30 );
			m_showAxesButton->setPressed( false );	

			m_lightBackgroundButton->setTexFromPNG( Radio, RadioPressed );
			m_lightBackgroundButton->resizeByHeight( 30 );
			m_lightBackgroundButton->setPressed( false );	

			m_localNormButton->setTexFromPNG( Radio, RadioPressed );
			m_localNormButton->resizeByHeight( 30 );
			m_localNormButton->setPressed( false );	

			m_normalizeFromZeroButton->setTexFromPNG( Radio, RadioPressed );
			m_normalizeFromZeroButton->resizeByHeight( 30 );
			m_normalizeFromZeroButton->setPressed( false );	

			m_allAxisNormalizationButton->setTexFromPNG( Radio, RadioPressed );
			m_allAxisNormalizationButton->resizeByHeight( 30 );
			m_allAxisNormalizationButton->setPressed( false );	

			m_perBinButton->setTexFromPNG( Radio, RadioPressed );
			m_perBinButton->resizeByHeight( 30 );
			m_perBinButton->setPressed( false );	

			m_perBandButton->setTexFromPNG( Radio, RadioPressed );
			m_perBandButton->resizeByHeight( 30 );
			m_perBandButton->setPressed( false );	

			m_linScaleButton->setTexFromPNG( Radio, RadioPressed );
			m_linScaleButton->resizeByHeight( 30 );
			m_linScaleButton->setPressed( true );	

			m_sqrtScaleButton->setTexFromPNG( Radio, RadioPressed );
			m_sqrtScaleButton->resizeByHeight( 30 );
			m_sqrtScaleButton->setPressed( false );	

			m_logScaleButton->setTexFromPNG( Radio, RadioPressed );
			m_logScaleButton->resizeByHeight( 30 );
			m_logScaleButton->setPressed( false );	

			m_showBordersButton->setTexFromPNG( Radio, RadioPressed );
			m_showBordersButton->resizeByHeight( 30 );
			m_showBordersButton->setPressed( false );	

			m_glyphSetVisibilityButton->setTexFromPNG( Radio, RadioPressed );
			m_glyphSetVisibilityButton->resizeByHeight( 30 );
			m_glyphSetVisibilityButton->setPressed( true );

			m_colorModeTextureButton->setTexFromPNG( Radio, RadioPressed );
			m_colorModeScalarButton->setTexFromPNG( Radio, RadioPressed );
			m_colorModeSolidButton->setTexFromPNG( Radio, RadioPressed );

			m_colorModeTextureButton->resizeByHeight( 30 );
			m_colorModeScalarButton->resizeByHeight( 30 );
			m_colorModeSolidButton->resizeByHeight( 30 );

			m_colorModeTextureButton->setPressed( true );
			m_colorModeScalarButton->setPressed( false );
			m_colorModeSolidButton->setPressed( false  );

			m_rotationNoneButton->setTexFromPNG( Radio, RadioPressed );
			m_rotationViewButton->setTexFromPNG( Radio, RadioPressed );
			m_rotationNormButton->setTexFromPNG( Radio, RadioPressed );

			m_rotationNoneButton->resizeByHeight( 30 );
			m_rotationViewButton->resizeByHeight( 30 );
			m_rotationNormButton->resizeByHeight( 30 );

			m_rotationNoneButton->setPressed( false );
			m_rotationViewButton->setPressed( false );
			m_rotationNormButton->setPressed( true  );

			m_previewGlyphButton->setTexFromPNG( 
				baseDir + "/textures/glyph140-red.png", 
				baseDir + "/textures/glyph140-red.png" 
			);
			m_previewGlyphButton->resizeByHeight( 400 );

			m_requestUpdateGlyphPreview = true;
		
			///////////////////////////////////////////////////////

			// Update axis and component selectors

			if( m_currentGlyph.glyph_type != "experimental" )
			{
				int nVars = m_currentGlyph.glyph_type == "radial" || m_currentGlyph.glyph_type == "star" ? m_currentGlyph.axes.size() : m_currentGlyph.vectors.size();
				int nComp = m_currentGlyph.glyph_type == "radial" || m_currentGlyph.glyph_type == "star" ? m_currentGlyph.componentsPerAxis() : 0;

				while( nVars < m_axisSelectors.size() ) {           m_axisSelectors.pop_back(); }
				while( nComp < m_componentSelectors.size() ) { m_componentSelectors.pop_back(); }

				while( nVars > m_axisSelectors.size() ) 
				{ 
					m_axisSelectors.push_back( std::unique_ptr<TN::TexturedPressButton>(  new TN::TexturedPressButton() ) ); 
					m_axisSelectors.back()->setTexFromPNG( Radio, RadioPressed );
					m_axisSelectors.back()->resizeByHeight( 30 );
				}

				while( nComp > m_componentSelectors.size() ) 
				{ 
					m_componentSelectors.push_back( std::unique_ptr<TN::TexturedPressButton>(  new TN::TexturedPressButton() ) ); 
					m_componentSelectors.back()->setTexFromPNG( Radio, RadioPressed );
					m_componentSelectors.back()->resizeByHeight( 30 );
				}

				for( int i = 0; i < nVars; ++i ) {      m_axisSelectors[ i ]->setPressed( false ); }
				for( int i = 0; i < nComp; ++i ) { m_componentSelectors[ i ]->setPressed( false ); }

				if( m_currentGlyph.glyph_type == "directional" )
				{
					for( auto & s : m_currentGlyph.active_axes ) { m_axisSelectors[ s ]->setPressed( true ); }
				}
				else if ( m_currentGlyph.glyph_type == "star" || m_currentGlyph.glyph_type == "radial" )
				{
					for( auto & s : m_currentGlyph.active_axes )       {      m_axisSelectors[ s ]->setPressed( true ); }
					for( auto & s : m_currentGlyph.active_components ) { m_componentSelectors[ s ]->setPressed( true ); }
				}
			}
			else
			{
				// ?
			}

			/////////////////////////////////////////////////////////
		}

		m_numLayers = nLayers;

		applyLayout();
	}

	void setGlyphPreviewData( const std::vector< std::array< std::uint8_t, 3 > > & data, int width, int height )
	{
		m_previewGlyphButton->setTexFromData( data, width, height );
	}

	bool requestLoadGlyph() const 
	{
		return m_requestLoadGlyph;
	}

	bool requestLoadGlyphTexture() const 
	{
		return m_requestLoadGlyphTexture;
	}

	bool requestUpdateGlyphPreview() const 
	{
		return m_requestUpdateGlyphPreview;
	}

	const TN::Vec2< float > glyphPreviewSize() const
	{
		return m_previewGlyphButton->size();
	}

	const std::string & glyphPath() const
	{
		return m_glyphPath;
	}

	const std::string & glyphTexPath() const
	{
		return m_glyphTexPath;
	}

	void resolveRequests() 
	{
		m_requestLoadGlyphTexture   = false;
		m_requestLoadGlyph          = false;
		m_requestUpdateGlyphPreview = false;
	}

	bool waitingToProcess() {
		return m_waitingToProcessChange;
	}

	void configure( 
		const SpatialViewConfiguration & config,
		const std::string & projectDirectory )
	{			
		// enable or disable according to configuration

		const std::map< std::string,  bool > & set_vis = config.surface_set_visibility;
		const std::map< std::string, std::map< std::string, bool > > & surface_vis = config.surface_visibility;

		for( const auto & s : set_vis )
		{
			if( ! m_surfaceSetVisibilityButtons.count( s.first ) )
			{
				std::cerr << "Error: surface set not synchronized between config and interface" << std::endl;
				exit( 1 );
			}
			m_surfaceSetVisibilityButtons.at( s.first )->setPressed( s.second );
		}

		for( const auto & s : surface_vis )
		{
			if( ! m_surfaceVisibilityButtons.count( s.first ) )
			{
				std::cerr << "Error: surface set not synchronized between config and interface" << std::endl;
				exit( 1 );
			}

			auto & sb = m_surfaceVisibilityButtons.at( s.first );
			for( const auto & sf : s.second )
			{
				if( ! sb.count( sf.first ) )
				{
					std::cerr << "Error: surface set not synchronized between config and interface" << std::endl;
					exit( 1 );
				}
				sb.at( sf.first )->setPressed( sf.second );
			}
		}

		const auto & glyphSetVisibility   = config.glyphSetVisibility;
		const auto & glyphLayerVisibility = config.glyphLayerVisibility;

		if( m_numLayers > 0 )
		{
			m_glyphSetVisibilityButton->setPressed( glyphSetVisibility );

			if( glyphLayerVisibility.size() != m_glyphVisibilityButtons.size() )
			{
				std::cerr << "Error: View3DConfigurationPanel.hpp, configure, glyph layer number mismatch" << std::endl;
			}
			else
			{
				for( int i = 0; i < glyphLayerVisibility.size(); ++i )
				{
					m_glyphVisibilityButtons[ i ]->setPressed( glyphLayerVisibility[ i ] );
				}
			}

			// orientation

			int orientation = config.orientation;
			if( orientation == TN::GlyphOrentation::Viewer ) {
				m_rotationNormButton->setPressed( false );
				m_rotationViewButton->setPressed(  true );
				m_rotationNoneButton->setPressed( false );
			}
			else if ( orientation == TN::GlyphOrentation::Normal ) {
				m_rotationNormButton->setPressed(  true );
				m_rotationViewButton->setPressed( false );
				m_rotationNoneButton->setPressed( false );
			}
			else {
				m_rotationNormButton->setPressed( false );
				m_rotationViewButton->setPressed( false );
				m_rotationNoneButton->setPressed(  true );
			}

			// color mode

			int colorMode = config.colorMode;
			if( colorMode == TN::GlyphColorMode::Texture ) {
				m_colorModeTextureButton->setPressed(  true );
				m_colorModeScalarButton->setPressed(  false );
				m_colorModeSolidButton->setPressed(   false );
			}
			else if ( colorMode == TN::GlyphColorMode::Scalar ) {
				m_colorModeTextureButton->setPressed( false );
				m_colorModeScalarButton->setPressed(   true );
				m_colorModeSolidButton->setPressed(   false );
			}
			else {
				m_colorModeTextureButton->setPressed( false );
				m_colorModeScalarButton->setPressed(  false );
				m_colorModeSolidButton->setPressed(    true );
			}

			m_linScaleButton->setPressed(  config.scaleMode == 0 );
			m_sqrtScaleButton->setPressed( config.scaleMode == 1 );
			m_logScaleButton->setPressed(  config.scaleMode == 2 );

			m_showBordersButton->setPressed( config.showGlyphBorders );
		}

		float sliderPos = 1.f;
		float gScale = config.glyphScale;

		if( gScale > 1 )
		{
			sliderPos = 0.5 + ( ( gScale - 1.0 ) / ( m_glyphScaleFactor - 1.0 ) ) * 0.5;
		}
		else if ( gScale < 1 )
		{					   
			sliderPos = ( ( gScale - 1 / m_glyphScaleFactor ) / ( 1 - 1 / m_glyphScaleFactor ) ) * 0.5;
		}
		else
		{
			sliderPos = 0.5;
		}

		m_glyphScaleSlider.setSliderPos( sliderPos );
		m_showScatterPlotSelectionButton->setPressed( config.show_voxel_selection );
		m_showAxesButton->setPressed( config.show_axis );
		m_lightBackgroundButton->setPressed( config.light_background );

		m_allAxisNormalizationButton->setPressed( config.allAxisNormalization );
		m_normalizeFromZeroButton->setPressed( config.normalizeGlyphsFromZero );
		m_localNormButton->setPressed( config.perGlyphNormalization );
		m_perBinButton->setPressed( config.perScaleBinNormalization );
		m_perBandButton->setPressed( config.perBandNormalization );

		m_currentGlyph = config.glyphConfiguration;
	
		if( m_currentGlyph.glyph_type != "experimental" ) 
		{
			for( int i = 0; i < m_axisSelectors.size(); ++i )
			{
				m_axisSelectors[ i ]->setPressed( false );
			}
			for( auto i : m_currentGlyph.active_axes )
			{
				m_axisSelectors[ i ]->setPressed( true );
			}

			for( int i = 0; i < m_componentSelectors.size(); ++i )
			{
				m_componentSelectors[ i ]->setPressed( false );
			}

			for( auto i : m_currentGlyph.active_components )
			{
				m_componentSelectors[ i ]->setPressed( true );
			}
		}
	}

	View3DConfigurationPanel() : 
		ConfigurationPanel(), 
		m_previewGlyphButton( std::unique_ptr<TN::TexturedPressButton>( new TN::TexturedPressButton() ) ),
		m_glyphSetVisibilityButton( std::unique_ptr<TN::TexturedPressButton>( new TN::TexturedPressButton() ) ),
		m_colorModeTextureButton( std::unique_ptr<TN::TexturedPressButton>( new TN::TexturedPressButton() ) ),
		m_colorModeScalarButton( std::unique_ptr<TN::TexturedPressButton>( new TN::TexturedPressButton() ) ),
		m_colorModeSolidButton( std::unique_ptr<TN::TexturedPressButton>( new TN::TexturedPressButton() ) ),
		m_loadGlyphTextureButton( std::unique_ptr<TN::PressButton>( new TN::PressButton() ) ),
		m_loadGlyphButton( std::unique_ptr<TN::PressButton>( new TN::PressButton() ) ),
		m_rotationNoneButton( std::unique_ptr<TN::TexturedPressButton>( new TN::TexturedPressButton() ) ),
		m_rotationViewButton( std::unique_ptr<TN::TexturedPressButton>( new TN::TexturedPressButton() ) ),
		m_rotationNormButton( std::unique_ptr<TN::TexturedPressButton>( new TN::TexturedPressButton() ) ),
		m_showScatterPlotSelectionButton( std::unique_ptr<TN::TexturedPressButton>( new TN::TexturedPressButton() ) ),
		m_showAxesButton( std::unique_ptr<TN::TexturedPressButton>( new TN::TexturedPressButton() ) ),
		m_lightBackgroundButton( std::unique_ptr<TN::TexturedPressButton>( new TN::TexturedPressButton() ) ),
		m_perBinButton( std::unique_ptr<TN::TexturedPressButton>( new TN::TexturedPressButton() ) ),
		m_perBandButton( std::unique_ptr<TN::TexturedPressButton>( new TN::TexturedPressButton() ) ),
		m_localNormButton( std::unique_ptr<TN::TexturedPressButton>( new TN::TexturedPressButton() ) ),
		m_normalizeFromZeroButton( std::unique_ptr<TN::TexturedPressButton>( new TN::TexturedPressButton() ) ),
		m_allAxisNormalizationButton( std::unique_ptr<TN::TexturedPressButton>( new TN::TexturedPressButton() ) ),
		m_linScaleButton( std::unique_ptr<TN::TexturedPressButton>( new TN::TexturedPressButton() ) ),
		m_sqrtScaleButton( std::unique_ptr<TN::TexturedPressButton>( new TN::TexturedPressButton() ) ),
		m_logScaleButton( std::unique_ptr<TN::TexturedPressButton>( new TN::TexturedPressButton() ) ),
		m_showBordersButton( std::unique_ptr<TN::TexturedPressButton>( new TN::TexturedPressButton() ) )
	{

		m_scroll = 0.f;

		m_glyphStyleCombo.setKeys( { "radial", "star" } );
		m_glyphStyleCombo.text( "Style" );

		m_showScatterPlotSelectionLabel.text = "Show Plot Selection";
		m_showScatterPlotSelectionLabel.bold = true;

		m_showAxesLabel.text = "Show Axes";
		m_showAxesLabel.bold = true;

		m_lightBackgroundLabel.text = "Light Background";
		m_lightBackgroundLabel.bold = true;

		m_mappingOptionsLabel.text = "Visual Mapping";
		m_mappingOptionsLabel.bold = true;

		m_linScaleLabel.text  = "lin";
		m_sqrtScaleLabel.text = "sqrt";
		m_logScaleLabel.text  = "log";

		m_scalingModeLabel.text = "Scaling";
		m_scalingModeLabel.bold = true;

		m_showBordersLabel.text  = "Show Annotations";
		m_showBordersLabel.bold  = true;

		m_normalizeFromZeroLabel.text = "Zero Anchored";
		m_localNormLabel.text 		  = "(local|global)";
		m_perBandLabel.text   		  = "(sel|all) Bands";
		m_perBinLabel.text    		  = "(sel|all) Scale Bins";

        m_allAxisNormalizationLabel.text = "all axes";
		m_allAxisNormalizationLabel.bold = true;

		m_normalizeFromZeroLabel.bold = true;
		m_localNormLabel.bold = true;
		m_perBandLabel.bold   = true;
		m_perBinLabel.bold    = true;

		m_glyphScaleSlider.text( "Scale" );
		m_glyphScaleFactor = 4.0;
		m_glyphScaleSlider.setSliderPos( 0.7 );

		m_glyphPreviewLabel.text = "Band Average";
		m_glyphPreviewLabel.bold = true;
		

		m_axisSelectionLabel.text = "Fields";
		m_axisSelectionLabel.bold = true;

		m_componentSelectionLabel.text = "Scale Bins";
		m_componentSelectionLabel.bold = true;

		m_hiddenSurfaceSets = { "Bands" };

		m_requestLoadGlyph = false;
		m_requestLoadGlyphTexture = false;
		m_requestUpdateGlyphPreview = false;

		m_glyphPath    = "";
		m_glyphTexPath = "";

		setTitle( "" );
		m_applyButton.text( "Apply Changes" );
		m_applyButton.setSize( 120, 30.f );

		m_loadGlyphButton->text( "Load Glyph OBJ" );
		m_loadGlyphButton->setSize( 180, 30.f );

		m_loadGlyphTextureButton->text( "Load Glyph Tex PNG" );
		m_loadGlyphTextureButton->setSize( 180, 30.f );

		m_surfaceSetLabel.text = "Surface Sets";
		m_glyphSetLabel.text = "glyphs";

		m_rotationLabel.text = "Rotation";
		m_rotationLabel.bold = true;
		m_rotationNoneLabel.text = "None";
		m_rotationViewLabel.text = "Viewer";
		m_rotationNormLabel.text = "Normal";

		m_colorModeLabel.text = "Coloring";
		m_colorModeLabel.bold = true;
		m_colorModeTextureLabel.text = "Texture";
		m_colorModeScalarLabel.text = "Scalar";
		m_colorModeSolidLabel.text = "Solid";

		m_glyphSetLabel.bold     = true;
		m_surfaceSetLabel.bold   = true;
		m_waitingToProcessChange = false;

		m_colorModeTextureButton->setPressed( true );
		m_colorModeScalarButton->setPressed( false );
		m_colorModeSolidButton->setPressed( false );

		m_rotationNormButton->setPressed(  true );
		m_rotationViewButton->setPressed( false );
		m_rotationNoneButton->setPressed( false );
	}	

	SpatialViewConfiguration processChanges( const SpatialViewConfiguration & current )
	{
		m_waitingToProcessChange = false;
		SpatialViewConfiguration jpc = current;

		for( const auto & s : m_surfaceSetVisibilityButtons )
		{
			bool pressed = s.second->isPressed();

			if( ! jpc.surface_set_visibility.count( s.first ) )
			{
				jpc.surface_set_visibility.insert( { s.first, false } );
			}
			else
			{
				jpc.surface_set_visibility.at( s.first ) = pressed;
			}
		}

		for( const auto & s : m_surfaceVisibilityButtons )
		{
			if( ! jpc.surface_visibility.count( s.first ) )
			{
				jpc.surface_visibility.insert( { s.first, {} } );
			}

			for( const auto & sf : s.second )
			{
				bool pressed = sf.second->isPressed();

				if( ! jpc.surface_visibility.at( s.first ).count( sf.first ) )
				{
					jpc.surface_visibility.at( s.first ).insert( { sf.first, false } );
				}
				else
				{
					jpc.surface_visibility.at( s.first ).at( sf.first ) = pressed;  
				}
			}
		}

		if( m_numLayers > 0 )
		{
			jpc.glyphSetVisibility = m_glyphSetVisibilityButton->isPressed();
			jpc.glyphLayerVisibility.clear();

			for( int i = 0; i < m_numLayers; ++i )
			{
				jpc.glyphLayerVisibility.push_back( m_glyphVisibilityButtons[ i ]->isPressed() );
			}
			
			// orientation

			jpc.orientation =
				m_rotationNormButton->isPressed() ? TN::GlyphOrentation::Normal :
				m_rotationViewButton->isPressed() ? TN::GlyphOrentation::Viewer :
													TN::GlyphOrentation::None;
			// color mode

			jpc.colorMode =
				m_colorModeTextureButton->isPressed() ? TN::GlyphColorMode::Texture :
				m_colorModeScalarButton->isPressed()  ? TN::GlyphColorMode::Scalar  :
													    TN::GlyphColorMode::Solid;
		}

		jpc.scaleMode =
			m_linScaleButton->isPressed() ? 0 :
			m_sqrtScaleButton->isPressed() ? 1 : 2;

		float sliderPosition = m_glyphScaleSlider.sliderPosition();

		if( sliderPosition < 0.5 )
		{
			jpc.glyphScale = 1 / m_glyphScaleFactor + 2*sliderPosition*( 1 - 1 / m_glyphScaleFactor );
		}
		else if( sliderPosition > 0.5 )
		{
			jpc.glyphScale = 1.f + ( sliderPosition - 0.5 ) * 2.0 * ( m_glyphScaleFactor - 1 );
		}
		else
		{
			jpc.glyphScale = 1.f;		
		}

		jpc.show_voxel_selection = m_showScatterPlotSelectionButton->isPressed();
		jpc.show_axis            = m_showAxesButton->isPressed();
		jpc.light_background     = m_lightBackgroundButton->isPressed();

		jpc.normalizeGlyphsFromZero  = m_normalizeFromZeroButton->isPressed();
		jpc.perGlyphNormalization    = m_localNormButton->isPressed();
		jpc.perBandNormalization     = m_perBandButton->isPressed();
		jpc.perScaleBinNormalization = m_perBinButton->isPressed();
		jpc.allAxisNormalization     = m_allAxisNormalizationButton->isPressed();

		jpc.showGlyphBorders = m_showBordersButton->isPressed();

		if( m_currentGlyph.glyph_type != "experimental" ) 
		{
			m_currentGlyph.active_axes.clear();

			for( int i = 0; i < m_axisSelectors.size(); ++i )
			{
				if( m_axisSelectors[ i ]->isPressed() )
				{
					m_currentGlyph.active_axes.push_back( i );
				}
			}

			m_currentGlyph.active_components.clear();
			for( int i = 0; i < m_componentSelectors.size(); ++i )
			{
				if( m_componentSelectors[ i ]->isPressed() )
				{
					m_currentGlyph.active_components.push_back( i );
				}
			}
		}

		if( m_currentGlyph.glyph_type == "star" || m_currentGlyph.glyph_type == "radial" )
		{
			m_currentGlyph.glyph_type = m_glyphStyleCombo.selectedItemText();
		}

		jpc.glyphConfiguration = m_currentGlyph;

		return jpc;
	}

	virtual std::vector< TN::Label       * > labels()  override {

		std::vector< TN::Label * > result;

		for( auto & e : m_surfaceSetVisibilityLabels )
		{
			if( m_hiddenSurfaceSets.count( e.first ) )
			{
				continue;
			}
			result.push_back( & e.second );
		}

		for( auto & m : m_surfaceVisibilityLabels )
		{	
			if( m_hiddenSurfaceSets.count( m.first ) )
			{
				continue;
			}

			for( auto & e : m.second )
			{
				result.push_back( & e.second );
			}
		}

		if( m_surfaceSetVisibilityLabels.size() > 0 )
		{
			result.push_back( & m_surfaceSetLabel );
		}

		if( m_numLayers > 0 )
		{
			result.push_back( & m_glyphSetLabel   );
			for( auto & l : m_glyphLayerLabels ) 
			{
				result.push_back( & l );
			}

			if( m_currentGlyph.glyph_type == "experimental" )
			{
				result.push_back( & m_rotationNoneLabel );
				result.push_back( & m_colorModeLabel );
				result.push_back( & m_colorModeTextureLabel );
				result.push_back( & m_colorModeScalarLabel );
				result.push_back( & m_colorModeSolidLabel );
			}
			else
			{
				result.push_back( & m_axisSelectionLabel  );
				result.push_back( & m_componentSelectionLabel  );
			}

			result.push_back( & m_rotationLabel     );
			result.push_back( & m_glyphPreviewLabel );
			result.push_back( & m_rotationViewLabel );
			result.push_back( & m_rotationNormLabel );
		}

		result.push_back( & m_showScatterPlotSelectionLabel );
		result.push_back( & m_showAxesLabel );
		result.push_back( & m_lightBackgroundLabel );

		result.push_back( & m_mappingOptionsLabel );
		result.push_back( & m_normalizeFromZeroLabel );
		result.push_back( & m_allAxisNormalizationLabel );
		result.push_back( & m_localNormLabel );
		result.push_back( & m_perBinLabel );
		result.push_back( & m_perBandLabel );

		result.push_back( & m_scalingModeLabel );

		result.push_back( & m_linScaleLabel );
		result.push_back( & m_sqrtScaleLabel );
		result.push_back( & m_logScaleLabel );

		result.push_back( & m_showBordersLabel );

		return result;
	}
	
	virtual std::vector< TN::SliderWidget * > sliders() override { 
		return {
			& m_glyphScaleSlider
		};
	}

	virtual std::vector< TN::PressButton * > buttons() override { 

		std::vector< TN::PressButton * > result;

		for( auto & e : m_surfaceSetVisibilityButtons )
		{
			if( m_hiddenSurfaceSets.count( e.first ) )
			{
				continue;
			}

			result.push_back( e.second.get() );
		}

		for( auto & m : m_surfaceVisibilityButtons )
		{	
			if( m_hiddenSurfaceSets.count( m.first ) )
			{
				continue;
			}

			for( auto & e : m.second )
			{
				result.push_back( e.second.get() );
			}
		}

		for( auto & m : m_surfaceColorButtons )
		{	
			if( m_hiddenSurfaceSets.count( m.first ) )
			{
				continue;
			}

			for( auto & e : m.second )
			{
				result.push_back( e.second.get() );
			}
		}

		if( m_numLayers > 0 )
		{
			result.push_back( m_previewGlyphButton.get() );
			result.push_back( m_glyphSetVisibilityButton.get() );
			for( auto & e : m_glyphVisibilityButtons )
			{
				result.push_back( e.get() );
			}

			if( m_currentGlyph.glyph_type == "experimental" )
			{
				result.push_back( m_loadGlyphButton.get() );
				result.push_back( m_loadGlyphTextureButton.get() );
				result.push_back( m_rotationNoneButton.get() );
				result.push_back( m_colorModeTextureButton.get() );
				result.push_back( m_colorModeScalarButton.get() );
				result.push_back( m_colorModeSolidButton.get() );
			}

			result.push_back( m_previewGlyphButton.get() );
			result.push_back( m_rotationViewButton.get() );
			result.push_back( m_rotationNormButton.get() );

			for( auto & btn : m_axisSelectors )
			{
				result.push_back( btn.get() );
			}

			for( auto & btn : m_componentSelectors )
			{
				result.push_back( btn.get() );
			}
		}

		if( m_currentGlyph.glyph_type == "radial" 
		 || m_currentGlyph.glyph_type == "star" )
		{
			for( auto i : m_currentGlyph.active_axes )
			{
				result.push_back( m_glyphAxisLabels[ i ].get() );
			}
		}
		else if( m_currentGlyph.glyph_type == "directional"  ) 
		{
			for( int i = 0; i < m_axisSelectors.size(); ++i )
			{
				result.push_back( m_glyphAxisLabels[ i ].get() );
			}
		}

		for( auto & b : m_glyphAxisSelectorLabels )
		{
			result.push_back( b.get() );
		}

		for( auto & b : m_glyphCompSelectorLabels )
		{
			result.push_back( b.get() );
		}

		if( m_currentGlyph.glyph_type == "directional" )
		{
			for( auto & b : m_glyphAxisColorButtons )
			{
				result.push_back( b.get() );
			}
		}

		result.push_back( m_showAxesButton.get() );
		result.push_back( m_showScatterPlotSelectionButton.get() );
		result.push_back( & m_applyButton );
		result.push_back( m_lightBackgroundButton.get() );

		result.push_back( m_perBandButton.get() );
		result.push_back( m_perBinButton.get() );
		result.push_back( m_localNormButton.get() );
		result.push_back( m_normalizeFromZeroButton.get() );
		result.push_back( m_allAxisNormalizationButton.get() );

		result.push_back( m_linScaleButton.get() );
		result.push_back( m_sqrtScaleButton.get() );
		result.push_back( m_logScaleButton.get() );
		result.push_back( m_showBordersButton.get() );

		return result;
	}
	
	virtual std::vector< TN::ComboWidget * > combos()  override { 
		return { 
			& m_glyphStyleCombo
		}; 
	}

	virtual ~View3DConfigurationPanel() {}
};

} // end namespace TN

#endif 