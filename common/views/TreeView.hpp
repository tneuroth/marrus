#ifndef TN_TREE_VIEW_HPP
#define TN_TREE_VIEW_HPP

#include "views/Widget.hpp"
#include "views/PressButtonWidget.hpp"
#include "geometry/Vec.hpp"
#include "views/InputState.hpp"
#include "stats/Stats.hpp"
#include "utils/TF.hpp"
#include "algorithms/Filter/BasicFilter.hpp"

// #include <graphviz/gvc.h>

#include <string>
#include <vector>
#include <iostream>
#include <map>

namespace TN {

struct NodeEncodingProperties
{
    float relativeMeanX;
    float relativeMeanY;
    float relativeCovariance;
    float relativeCoSkewness;
    float relativeCoKurtosis;

    TN::Vec3< float > m1Color;
    TN::Vec3< float > m2Color;
    TN::Vec3< float > m3Color;
    TN::Vec3< float > m4Color;
    TN::Vec3< float > m5Color;

    bool sampleTooSmall;
};

struct TreeNode
{
    int32_t componentId;
    int32_t layerId;
    PressButton button;
    bool isLeaf;
    double levelSpan;

    NodeEncodingProperties encodings;
};

struct TreeNodeCluster
{
    bool singular;
    NodeEncodingProperties encodings;
};

class TreeView : public Widget
{
    std::string m_metric1label;
    std::string m_metric2label;

    std::vector< int32_t > m_hierarchyRelableTable;

    std::vector< TN::Vec3< float > > m_cm1;
    std::vector< TN::Vec3< float > > m_cm2;
    std::vector< TN::Vec3< float > > m_cm3;
    std::vector< TN::Vec3< float > > m_cm4;
    std::vector< TN::Vec3< float > > m_cm5;

    std::vector< TreeNodeCluster > m_nodeClusters;
	std::map< int32_t, std::pair< TN::Vec2< float >, TN::Vec2< float > > > m_clusterBoundaryies; 
    std::vector< int32_t > m_supernodes;
    std::vector< float > m_cmpCounts;

    // Agraph_t * gv_graph;
    // std::vector< Agnode_t* > gv_nodes;
    // std::vector< Agedge_t* > gv_edges;
    // GVC_t * gv_context;

    std::vector< TN::Vec2< float > > m_layers;

	int32_t m_nLevels;
    std::map< int32_t, std::vector< int32_t > > m_hierarchy; // node to its subtress
    std::map< int32_t, int32_t > m_subtreeToParentNode; // subtree parent   
    std::vector< int32_t > m_componentLayerTable;
    std::map< int32_t, std::vector< int32_t > > m_layerToNodes;
    std::map< int32_t, TreeNode > m_nodes;

    TN::TexturedPressButton m_levelFieldSymbol;
  
    static constexpr float LW = 14.f;

    static constexpr float lm = 20.f;
    static constexpr float rm = 30.f;
    static constexpr float tm = 70.f;
    static constexpr float bm = 10.f;

public :

    inline TN::Vec3< float > cm( 
        const float val,
        const TN::Vec2< float > & vRange,
        const std::vector< TN::Vec3< float > > & CM )
    {
        int index = ( val - vRange.a() ) / ( vRange.b() - vRange.a() ) * CM.size();
        return CM[ std::max( std::min( index, (int) CM.size() - 1 ), 0 ) ];
    }

    void setCMs( 
        const std::string & path1,
        const std::string & path2,
        const std::string & path3,
        const std::string & path4,
        const std::string & path5 )
    {
        TN::CM::loadTF( path1, m_cm1 );
        TN::CM::loadTF( path2, m_cm2 );        
        TN::CM::loadTF( path3, m_cm3 );   
        TN::CM::loadTF( path4, m_cm4 );  
        TN::CM::loadTF( path5, m_cm5 );  
    }

    void setMomements(  
        const std::vector< TN::STATS::Moments2D > & cmp_moments, 
        const std::vector< float > & cmp_counts,
        const std::vector< TN::STATS::Moments2D > & cls_moments, 
        const std::vector< float > & cls_counts,
        const TN::Vec2<float >     & meanXRange,
        const TN::Vec2<float >     & meanYRange,
        const TN::Vec2<float >     & covarianceRange,
        const TN::Vec2<float >     & skewnessRange,
        const TN::Vec2<float >     & kurtosisRange,

        const float minSample )
    {
        m_cmpCounts = cmp_counts;
        for( auto & node : m_nodes )
        {
            auto & enc = node.second.encodings;
            enc.sampleTooSmall = cmp_counts[ node.first ] < minSample;

            if( enc.sampleTooSmall )
            {
                enc.m1Color = enc.m2Color = enc.m3Color = enc.m4Color = enc.m5Color = { 0.3, 0.3, 0.3 };
            }
            else
            {
                auto & mm = cmp_moments[ node.first ];

                enc.m1Color =
                    cm( 
                        mm.x.mean,
                        meanXRange,
                        m_cm1 );    

                enc.m2Color =
                    cm( 
                        mm.y.mean,
                        meanYRange,
                        m_cm2 );    

                enc.m3Color =
                    cm( 
                        mm.coVariance,
                        covarianceRange,
                        m_cm3 );    

                enc.m4Color =
                    cm( 
                        mm.coSkewness,
                        skewnessRange,
                        m_cm4 );    

                enc.m5Color =
                    cm( 
                        mm.coKurtosis,
                        kurtosisRange,
                        m_cm5 );                     
            }        
        }

        for( int i = 0; i < m_nodeClusters.size(); ++i )
        {
            /// TODO : double check the indices for clusters 

            auto & enc =  m_nodeClusters[ i ].encodings;
            enc.sampleTooSmall = cls_counts[ i ] < minSample;  

            if( enc.sampleTooSmall )
            {
                enc.m1Color = enc.m2Color = enc.m3Color = enc.m4Color = enc.m5Color = { 0.3, 0.3, 0.3 };
            }
            else
            {
                auto & mm = cls_moments[ i ];
                enc.m1Color =
                    cm( 
                        mm.x.mean,
                        meanXRange,
                        m_cm1 );    

                enc.m2Color =
                    cm( 
                        mm.y.mean,
                        meanYRange,
                        m_cm2 );    

                enc.m3Color =
                    cm( 
                        mm.coVariance,
                        covarianceRange,
                        m_cm3 );    

                enc.m4Color =
                    cm( 
                        mm.coSkewness,
                        skewnessRange,
                        m_cm4 );    

                enc.m5Color =
                    cm( 
                        mm.coKurtosis,
                        kurtosisRange,
                        m_cm5 );                     
            }        
        }
    }

    virtual void setPosition(  float x, float y ) override 
    { 
        Widget::setPosition( x, y ); 
    } 

    virtual void setSize(  float x, float y ) override 
    { 
        Widget::setSize( x, y ); 
    } 

    bool isRootedFrom( int32_t node, int32_t root )
    {
        while( m_subtreeToParentNode.count( node ) )
    	{
    		int32_t parent = m_subtreeToParentNode.at( node );
    		if( node == 0 || parent == root )
    		{
    			return true;
    		}
    		node = parent;
    	}
    	return false;
    }

    bool isLeaf( int32_t node )
    {
    	if( ! m_hierarchy.count( node ) )
    	{
    		return true;
    	}
    	else 
    	{
    		return m_hierarchy.at( node ).size() == 0;
    	}
    }

    int32_t maxSubTreeWidth( int32_t root )
    {
    	int maxCount = 0;
    	int layer    = m_componentLayerTable[ root ];

        for( int i = m_nLevels - 1; i > 0; --i )
        {
        	int count = 0;
        	for( auto & node : m_layerToNodes.at( i ) )
        	{
				if( isRootedFrom( node, root ) )
				{
					++count;
				}
        	}
        	maxCount = std::max( maxCount, count );
        }
        return maxCount;
    }

void applySubLayoutFrom(
        int32_t node, 
        float x, 
        float y, 
        float w, 
        float dl, 
        float lw )
    {
        if( ! m_nodes.count( node ) )
        {
            std::cout << "m_nodes missing element: " << node << std::endl;
            exit(  1 );
        }

        TreeNode & treeNode = m_nodes.at( node );
        treeNode.button.setSize( lw, lw );
        treeNode.button.setPosition( x, y );
        treeNode.button.text( std::to_string( node ) );

        if( ! m_hierarchy.count( node ) )
        {
            std::cout << "m_hierarchy missing element: " << node << std::endl;
            exit( 1 );
        }

        const auto & subTrees = m_hierarchy.at( node );
        //std::cout << "node: " << node << " with " << subTrees.size() << " subtrees" << std::endl;

        std::vector< float > subTreeMaxWidths(   subTrees.size() );
        std::vector< float > subTreeProportions( subTrees.size() );

        int total = 0;
        for( int i = 0; i < subTrees.size(); ++i )
        {
            subTreeMaxWidths[ i ] = ( float ) maxSubTreeWidth( subTrees[ i ] );
            total += subTreeMaxWidths[ i ];
        }

        if( total > 0 )
        {
            for( int i = 0; i < subTrees.size(); ++i )
            {
                //std::cout << "node : " << subTrees[ i ] << " has " << "max subtree width = " << subTreeMaxWidths[ i ] << std::endl;
                subTreeProportions[ i ] = subTreeMaxWidths[ i ] / (double) total;
            }   
        }

        float dy = 0;
        float lasty = 0;

        std::vector< int64_t > subTreeOrder( subTrees.size() );
        std::vector< int64_t > subTreeSizes( subTrees.size() );

        if( subTrees.size() == 0 ) { return; }

        for( size_t i = 0; i < subTrees.size(); ++i ) {
            if( m_hierarchy.count( subTrees[ i ] ) ) {
                subTreeSizes[ i ] = m_hierarchy.at( subTrees[ i ] ).size(); 
            }
            else {
                subTreeSizes[ i ] = 0;   
            }
            subTreeOrder[ i ] = i;
        }

        if( m_cmpCounts.size() == m_nodes.size() ) {

        }
        else {
            std::cout << "wrong size " << m_cmpCounts.size() << " vs " << m_nodes.size() << std::endl; 
            exit( 1 );
        }

        std::sort(
            subTreeOrder.begin(),
            subTreeOrder.end(),
            [ & ]( const size_t & a, const size_t & b )
        {
            return ( subTreeSizes[ a ] > subTreeSizes[ b ]  ) ? true 
                 : ( subTreeSizes[ a ] < subTreeSizes[ b ]  ) ? false 
                 : m_cmpCounts[ subTreeSizes[ a ] ] > m_cmpCounts[ subTreeSizes[ b ] ];
        } );

        for( size_t idx = 0; idx < subTrees.size(); ++idx ) 
        {       
            int64_t i = subTreeOrder[ idx ];
            applySubLayoutFrom( 
                subTrees[ i ],
                x + dl + lw,
                y + dy,
                w - ( dl + lw ),
                dl,
                lw );
            
            lasty = m_nodes.at( subTrees[ i ] ).button.position().y();
            dy -= std::max( subTreeSizes[ i ], 1L ) * lw * 1.2;
        }
    }

    // void applySubLayoutFrom(
    //     int32_t node, 
    // 	float x, 
    //     float y, 
    //     float w, 
    //     float h, 
    //     float dl, 
    //     float lw )
    // {
    //     if( ! m_nodes.count( node ) )
    //     {
    //     	std::cout << "m_nodes missing element: " << node << std::endl;
    //     	exit(  1 );
    //     }

    // 	TreeNode & treeNode = m_nodes.at( node );
    // 	treeNode.button.setSize( lw, lw );
    // 	treeNode.button.setPosition( x, y + h / 2.f - lw / 2.f );
    // 	treeNode.button.text( std::to_string( node ) );

    //     if( ! m_hierarchy.count( node ) )
    //     {
    //     	std::cout << "m_hierarchy missing element: " << node << std::endl;
    //     	exit( 1 );
    //     }

    //     const auto & subTrees = m_hierarchy.at( node );
    //     //std::cout << "node: " << node << " with " << subTrees.size() << " subtrees" << std::endl;

    //     std::vector< float > subTreeMaxWidths(   subTrees.size() );
    //     std::vector< float > subTreeProportions( subTrees.size() );

    //     int total = 0;
    // 	for( int i = 0; i < subTrees.size(); ++i )
    // 	{
    // 		subTreeMaxWidths[ i ] = ( float ) maxSubTreeWidth( subTrees[ i ] );
    // 		total += subTreeMaxWidths[ i ];
    // 	}

    //     if( total > 0 )
    //     {
	   //  	for( int i = 0; i < subTrees.size(); ++i )
	   //  	{
    // 		    //std::cout << "node : " << subTrees[ i ] << " has " << "max subtree width = " << subTreeMaxWidths[ i ] << std::endl;
	   //  		subTreeProportions[ i ] = subTreeMaxWidths[ i ] / (double) total;
	   //  	}	
    //     }

    //     float available = h - subTrees.size() * lw;

    //     float dy = 0;
    //     float lasty = 0;

    //     std::vector< int64_t > subTreeOrder( subTrees.size() );
    //     std::vector< int64_t > subTreeSizes( subTrees.size() );

    //     if( subTrees.size() == 0 ) { return; }

    //     for( size_t i = 0; i < subTrees.size(); ++i ) {
    //         if( m_hierarchy.count( subTrees[ i ] ) ) {
    //             subTreeSizes[ i ] = m_hierarchy.at( subTrees[ i ] ).size(); 
    //         }
    //         else {
    //             subTreeSizes[ i ] = 0;   
    //         }
    //         subTreeOrder[ i ] = i;

    //         std::cout << subTreeSizes[ i ] << " ";
    //     }

    //     std::cout << std::endl;

    //     std::sort(
    //         subTreeOrder.begin(),
    //         subTreeOrder.end(),
    //         [ & ]( const size_t & a, const size_t & b )
    //     {
    //         return ( subTreeSizes[ a ] > subTreeSizes[ b ] );
    //     } );

    // 	for( size_t idx = 0; idx < subTrees.size(); ++idx ) 
    //     {    	
    //         int64_t i = subTreeOrder[ idx ];
    //         float myPortion = available * subTreeProportions[ i ]; 

    //         applySubLayoutFrom( 
    //         	subTrees[ i ],
    //         	x + dl + lw,
    //         	y + dy,
    //         	w - ( dl + lw ),
    //         	myPortion,
    //         	dl,
    //         	lw );
            
    //         lasty = m_nodes.at( subTrees[ i ] ).button.position().y();
    //         dy += myPortion + lw;
    // 	}
    // }



    static inline TN::Vec2< double > parsePointStr( const std::string & s )
    {
    	TN::Vec2< double > result;
        
        int comp = 0;
        std::string components[ 2 ];

        for( int i = 0; i < s.size(); ++i )
        {
        	if( s[ i ] == ',' )
        	{
        		comp = 1;
        	}
        	else
        	{
				components[ comp ].push_back( s[ i ] );
			}
        }

    	return { std::stod( components[ 0 ] ), std::stod( components[ 1 ] ) };
    }

    // void applyClusteredLayout()
    // {

    // }
 
    void applyLayoutGV()
    {

    	// std::string size = std::to_string( this->size().x() ) 
    	//  				 + ","
    	//  				 + std::to_string( this->size().y() ) + "!";
     //    std::cout << "setting graph size=" << size << std::endl;

    	// int r = agsafeset( gv_graph, "size", &size[ 0 ], "" );
     //   	const char * gsztr = agget( gv_graph, "size" );
     //   	if( gsztr == 0 ) { std::cout << "size is NULL" << std::endl; }
     //   	else { std::cout << "size is " << gsztr << std::endl; }

     //   	gvLayout( gv_context, gv_graph, "dot" );

     //    std::cout << "rendering layout " << std::endl;      

     //    gvRender( gv_context, gv_graph, "dot", 0 );
    
	    // gvFreeLayout( gv_context, gv_graph );

     //    std::cout << "getting node positions " << std::endl;     

     //    double maxX = 0, minX = 999999999;
     //    double maxY = 0, minY = 999999999;

     //    for( int i = 0; i < gv_nodes.size(); ++i )
     //    {
     //    	const char * pstr = agget( gv_nodes[ i ], "pos" );

     //        if( pstr == 0 )
     //        {
     //        	std::cout << "no position found for node " << i << std::endl;
     //        }
     //        else
     //        { 
     //        	std::cout << "parsing position " << pstr << std::endl;
     //        }

     //    	TN::Vec2< double > pos = parsePointStr( pstr );

     //        maxX = std::max( maxX, pos.x() );
     //        maxY = std::max( maxY, pos.y() );
     //        minX = std::min( minX, pos.x() );
     //        minY = std::min( minY, pos.y() );  	
     //    }

     //    double xOffset = -minX;
     //    double yOffset = -minY;

     //    double xScale  = ( this->size().x() - 60 ) / ( maxX - minX );
     //    double yScale  = ( this->size().y() - 60 ) / ( maxY - minY );

     //    for( int i = 0; i < gv_nodes.size(); ++i )
     //    {
     //    	const char * pstr = agget( gv_nodes[ i ], "pos" );

     //        if( pstr == 0 )
     //        {
     //        	std::cout << "no position found for node " << i << std::endl;
     //        }
     //        else
     //        { 
     //        	std::cout << "parsing position " << pstr << std::endl;
     //        }

     //    	TN::Vec2< double > pos = parsePointStr( pstr );

     //        std::cout << "setting the position" << std::endl;

    	//     TreeNode & treeNode = m_nodes.at( i );

     //    	treeNode.button.setSize( 12, 12 );
     //    	treeNode.button.setPosition( 
     //    		this->position().x() + 20 + ( pos.x() - minX ) * xScale, 
     //    		this->position().y() + 20 + ( pos.y() - minY ) * yScale );
    	// 	treeNode.button.text( std::to_string( i ) );
     //    }


     //    std::cout << "done getting node positions" << std::endl;      
    }

    void applyLayout() 
    { 
        // applyLayoutGV();
        // return;

   //  	std::cout << "computing the tree layout" << std::endl;

        float dl = ( this->size().x() - lm - rm - m_nLevels * LW * 2 ) / ( m_nLevels - 1 );

        	// ( ( this->size().x()  
        	// 	- ( lm + rm ) )  
        	// 	- ( m_nLevels - 1 ) * dl ) 
        	// / m_nLevels;

        float diagramHeight = this->size().y();
        int32_t root = 0;

        if( m_hierarchy.count( root ) )
        {
			applySubLayoutFrom( 
				root, 
				this->position().x() + lm,
				this->position().y() + this->size().y() - tm,
				this->size().x() - ( lm + rm ),
				dl,
				LW ); 
        }

        for( auto & h : m_hierarchy )
        {
            auto & node = m_nodes.at( h.first );
            node.isLeaf = isLeaf( h.first );

        	if( h.second.size() > 0 )
        	{
        		auto & boundary = m_clusterBoundaryies.at( h.first );

        	    float xMin = 99999999;
        	    float xMax = 0;

        	    float yMin = 99999999;
        	    float yMax = 0;

        	    for( auto & n : h.second )
        	    {
        	    	auto & node = m_nodes.at( n );

        	    	xMin = std::min( xMin, node.button.position().x() );
        	    	xMax = std::max( xMax, node.button.position().x() );

        	    	yMin = std::min( yMin, node.button.position().y() + LW / 2.f );
        	    	yMax = std::max( yMax, node.button.position().y() + LW / 2.f );        	    	        	    	        	    	
        	    }

        	    boundary.first  = { xMin, yMin };
        	    boundary.second = { xMax-xMin, yMax-yMin };        	    
        	}
        }

        m_levelFieldSymbol.resizeByHeight( 18 );
        m_levelFieldSymbol.setPosition( 
        	this->position().x() + this->size().x() / 2.0 - m_levelFieldSymbol.size().x() / 2.0,
        	this->position().y() + this->size().y() - m_levelFieldSymbol.size().y() - 12 );        
    } 

    TN::TexturedPressButton * levelSymbol()
    {
    	return & m_levelFieldSymbol;
    }

    void updateNodeSelection( const std::vector< int32_t > & flags )
    {
        if( flags.size() != m_nodes.size() )
        {
            std::cerr << "nodes and flags different size " << flags.size() << " " << m_nodes.size() << std::endl;
            exit( 1 );
        }
        for( int i = 0; i < flags.size(); ++i )
        {
            if( m_nodes.count( i ) == 0 )
            {
                std::cerr << " node " << i << " is missing" << std::endl;
                exit( 1 );
            }

            if( flags[ i ] & TN::SUBSELECTION_ELEMENT )
            {
                m_nodes.at( i ).button.setPressed( true );
            }

            else
            {
                m_nodes.at( i ).button.setPressed( false );
            }
        }
    }

    const std::map< int32_t, TreeNode > & nodeMap() const 
    {
        return m_nodes;
    }

    std::vector< TreeNode * > nodes() 
    {
    	std::vector< TreeNode * > n;
    	for( auto & node : m_nodes )
    	{
    		n.push_back( & node.second );
    	}
        return n;
    }

    std::vector< PressButton * > buttons() 
    {
    	std::vector< PressButton * > b;
    	for( auto & node : m_nodes )
    	{
    		b.push_back( & node.second.button );
    	}
        return b;
    }

    TreeView() 
    	: Widget()
    {
	    // gv_context = gvContext();
     //    char * argv[ 1 ] = { "dot" };
    	// gvParseArgs( gv_context, 1, argv );

    	// gv_graph = agopen( "g", Agstrictdirected, 0 );

     //    int rv = agsafeset( gv_graph, "rankdir", "LR", "" );
     //    const char * rdir = agget( gv_graph, "rankdir" );
     //   	if( rdir == 0 )
     //   	{
     //   		std::cout << "rankdir was NULL" << std::endl;
     //   	}
     //   	else
     //   	{
     //    	std::cout << "rankdir was " << rdir << std::endl;      		
     //   	}
    }

    virtual ~TreeView() 
    {
     //    agclose( gv_graph );
    	// gvFreeContext( gv_context );
    }

    std::vector< TreeNodeCluster > getNodeClusters()
    {
        return m_nodeClusters;
    }

    std::vector< int32_t > getClusterRelableTable()
    {
        return m_hierarchyRelableTable;
    }

    std::map< int32_t, std::pair< TN::Vec2< float >, TN::Vec2< float > > > getClusterBoundaries()
    {
		return m_clusterBoundaryies;
    }

	std::vector< TN::Vec2< float > > sCurve( 
	    TN::Vec2< float > & p1,
	    TN::Vec2< float > & p2,            
	    int nsegments )
	{
	    if( p1.y() == p2.y() )
	    {
	        return { p1, p2 };
	    }

	    double x0 = -6.0;
	    double xe =  6.0;

	    double xnorm = xe - x0;
	    double dx = ( xe - x0 ) / nsegments;
	    double dpy = p2.y() - p1.y();

	    std::vector< TN::Vec2< float > > curve;
	    for( int i = 0; i <= nsegments; ++i )
	    {
	        double xi = x0 + dx * i;
	        double yi = 1.0 / ( 1.0 + std::exp( -xi ) );

	        double xn = ( xi - x0 ) / xnorm;
	        double px = xn * p2.x() + ( 1.0 - xn ) * p1.x();

	       curve.push_back( { 
	           px,
	           p1.y() + yi * dpy
	       } );
	    }

	    return curve;
	}

    std::vector< std::vector< TN::Vec2< float > > > getEdges()
    {
    	std::vector< std::vector< TN::Vec2< float > > > result;
    	for( auto & h : m_hierarchy )
    	{
    		auto & node1 = m_nodes.at( h.first );

    		TN::Vec2< float > p1;

	    	p1 = {
	    		node1.button.position().x() + node1.button.size().x(),
	    		node1.button.position().y() + LW / 2.0 
	    	};  

            /* Clusters */

    		if( h.second.size() > 0 )
    		{
    		    auto & psz = m_clusterBoundaryies.at( h.first );
    		    TN::Vec2< float > p2 = {
    		    	psz.first.x(),
    		    	psz.first.y() + psz.second.y() // - LW * 0.5 
    		    };

    		 //    result.push_back( std::vector< TN::Vec2< float > >() );
       //          result.back().push_back( p1 );
    			// result.back().push_back( p2 );

    			result.push_back( sCurve( p1, p2, 18 ) );
    		}

            /* All edges */ 
    		// for( auto & a : h.second )
    		// {
    		// 	auto & node2 = m_nodes.at( a );
    		// 	TN::Vec2< float > p2 = {
    		// 		node2.button.position().x(),
    		// 		node2.button.position().y() + node2.button.size().y() / 2.f 
    		// 	};  

    		// 	result.push_back( std::vector< TN::Vec2< float > >() );
      //           result.back().push_back( p1 );
    		// 	result.back().push_back( p2 );
    		// }
    	}


    	return result;
    }

    std::vector< int8_t > componentActivations()
    {
    	std::vector< int8_t > activations( m_nodes.size() );
    	for( auto & node : m_nodes )
    	{
    		activations[ node.first ] = node.second.button.isPressed();
    	}
    	return activations;
    }

    void setLField( const std::string & projectDirectory, const std::string & lfield )
    {
       	m_levelFieldSymbol.setTexFromPNG( 
       		projectDirectory + "/latex_symbols/" + lfield + ".png", 
       		projectDirectory + "/latex_symbols/" + lfield + ".png" );

        m_levelFieldSymbol.resizeByHeight( 18 );
    }

    std::vector< TN::Vec2< float > > layerGrid()
    {
    	std::vector< TN::Vec2< float > > lines;

    	// xsz - lm - rm - lw
        float dx = ( this->size().x() - lm - rm - m_nLevels * LW ) / ( m_nLevels - 1 );
        float  x = lm + dx / 2.f; // lm + dx / 2.0

    	for( int i = 0; i < m_nLevels; ++i )
    	{
    		lines.push_back( {
    			this->position().x() + x,
    			this->position().y() // -diagramHeight
    		} );

    		lines.push_back( {
    			this->position().x() + x,
    			this->position().y() + this->size().y() - tm
    		} );

    		x += dx;
    	}
    	return lines;
    }

    std::vector< TN::Vec2< float > > layers()
    {
    	return m_layers;
    }

    void set( 
    	const std::map< int32_t, std::vector< int32_t > > & hierarchy,
    	const std::vector< int32_t > & componentLayerTable,
        const std::vector< int32_t > & supernodes,
        const std::vector< int32_t > & relableTable,
    	const std::vector< TN::Vec2< float > > & layers,
    	const std::string & lfield,
    	const std::string & workingDirectory )
    {
        m_nodeClusters.resize( supernodes.size() );
        m_supernodes = supernodes;
        m_hierarchyRelableTable = relableTable;

        for( int i = 0; i < m_nodeClusters.size(); ++i )
        {
            m_nodeClusters[ i ].singular = hierarchy.at( supernodes[ i ] ).size() <= 1 ? true : false;
        
           // std::cout << "cluster " << i << " is from supernode " << supernodes[ i ] << " which is singular? " << m_nodeClusters[ i ].singular << std::endl;
        }

        //std::cout << "setting tree\ncomponentIds\n";

        // for( auto nh : hierarchy )
        // {
        // 	std::cout << nh.first << "\n";
        // }

        // for( int i = 0; i < componentLayerTable.size(); ++i )
        // {
        // 	std::cout << i << " " << componentLayerTable[ i ] << std::endl;
        // }

        m_nLevels = layers.size() + 1;
        m_hierarchy = hierarchy;
        m_layers = layers;

        m_componentLayerTable = componentLayerTable;

        for( int i = 0; i < m_componentLayerTable.size(); ++i )
        {
        	m_nodes.insert( { i, TreeNode() } );
        }

        for( int i = 0; i < m_nLevels; ++i )
        {
        	m_layerToNodes.insert( { i, std::vector<int32_t>() } );
        }

        for( auto & node : m_nodes )
        {
        	int32_t layer = m_componentLayerTable[ node.first ];
        	// std::cout << "mapping layer " << layer << " to node " << node.first << std::endl; 

            if( m_layerToNodes.count( layer ) == 0 )
            {
            	std::cout << "layer " << layer << " not mapped" << std::endl;
            	exit( 1 ); 
            }

        	m_layerToNodes.at( layer ).push_back( node.first );
        }

        for( const auto & nodeSubtrees : m_hierarchy )
        {
        	for( const auto & subtree : nodeSubtrees.second )
        	{
        		m_subtreeToParentNode.insert( {  subtree, nodeSubtrees.first } );
        	}
        }

        //std::cout << "doing gv stuff 2 " << std::endl;

    	// for( auto & nd : m_hierarchy )
    	// {
    	// 	std::string id = std::to_string( nd.first );
    	// 	Agnode_t * n = agnode( gv_graph, &id[ 0 ], 1 );
     //        if( id == "0" )
     //        {
     //        	agsafeset( n, "root", "true", "" );
     //        } 
     //        gv_nodes.push_back( n );
    	// }

        //std::cout << "doing gv stuff 3 " << std::endl;

    	for( auto & nd : m_hierarchy )
    	{
    		if( nd.second.size() > 0 )
    		{
    			m_clusterBoundaryies.insert( { 
    				nd.first, std::pair< TN::Vec2< float >, 
    				TN::Vec2< float > >() } );
    		}
            // for( int j = 0; j < nd.second.size(); ++j )
            // {
            // 	Agedge_t * e = agedge( gv_graph, gv_nodes[ nd.first ],  gv_nodes[ j ], "e", 1 );
            // 	gv_edges.push_back( e );
            // }
    	}
    }

    void engageSubtree( int32_t root, bool onOff )
    {
    	auto & node = m_nodes.at( root );
    	node.button.setPressed( onOff );
    	auto & sh = m_hierarchy.at( root );
    	for( int32_t subtree : sh ) {
    		engageSubtree( subtree, onOff );
    	}
    }

   	bool handleInput( 
   	const InputState & input )
   	{
        bool changed = false;
   		Vec2< double > p = input.mouseState.position;
        if( ! this->pointInViewPort( p ) ) {
        	return false;
        }

        float dy =  - input.mouseState.scrollDelta();
        float dx = 0;

        this->scroll( { dx, dy * 100 } );
        auto psc = p - TN::Vec2< double >( { this->getScroll().x(), this->getScroll().y() } );

        for( auto & e : m_nodes ) {
        	auto & node = e.second;
        	if( node.button.pointInViewPort( psc ) ) {
        		if( input.mouseState.event == MouseEvent::LeftPressed ) {
        			engageSubtree( e.first, ! node.button.isPressed() );
                    changed = true;
        		}
        	}
        }

        return changed;
   	}
};

}

#endif