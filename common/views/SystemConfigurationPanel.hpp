#ifndef TN_SYSTEM_CONFIGURATION_PANEL_HPP
#define TN_SYSTEM_CONFIGURATION_PANEL_HPP

namespace TN {
	
class SystemConfigurationPanel : public ConfigurationPanel
{

	public: 

	virtual void applyLayout() override {

	}

	virtual bool handleInput( const InputState & input ) override
	{
		bool changed = false;

		return changed;
	}

	virtual void clear() override {}

	SystemConfigurationPanel() : ConfigurationPanel() {}

	virtual ~SystemConfigurationPanel() {}
};

} // end namespace TN

#endif 