#ifndef VIEWPORT
#define VIEWPORT

#include "geometry/Vec.hpp"
#include "views/Viewport/BoxShadowFrame.hpp"

namespace TN
{

class ViewPort
{

    Vec4  m_bkgColor;

    BoxShadowFrame m_boxShadow;

    std::vector< Vec2< float > > m_geometry;
    std::vector< Vec4 > m_colors;

    float m_height;
    float m_width;

    float m_offsetX;
    float m_offsetY;

public :

    /********************************************
     *
     *  Constructors
     *
     * ******************************************/

    ViewPort() : m_bkgColor( 1.f, 1.f, 1.f, 1.f ),
        m_offsetX( 0.0 ), m_offsetY( 0.0 )
    {}

    /********************************************
     *
     *  Setters
     *
     * ******************************************/

    //! in screen space ( pixels )
    double  height() const
    {
        return m_height;
    }

    //! in screen space ( pixels )
    double   width() const
    {
        return m_width;
    }

    //! in screen space ( pixels )
    double offsetX() const
    {
        return m_offsetX;
    }

    //! in screen space ( pixels )
    double offsetY() const
    {
        return m_offsetY;
    }

    const Vec4 & bkgColor() const
    {
        return m_bkgColor;
    }

    /********************************************
     *
     *  Getters
     *
     * ******************************************/

    //! in screen space ( pixels )
    void setSize( float w, float h  )
    {
        m_height = h;
        m_width = w;
        m_boxShadow.setSize( w, h );
    }

    void setPosition( float x, float y )
    {
        m_offsetX = x;
        m_offsetY = y;
    }

    void height(float h  )
    {
        m_height = h;
        m_boxShadow.setSize( m_width, h );
    }

    void width(float w  )
    {
        m_width = w;
        m_boxShadow.setSize( w, m_height );
    }

    //! in screen space ( pixels )
    void offsetX( float x )
    {
        m_offsetX  = x;
    }

    //! in screen space ( pixels )
    void offsetY( float y )
    {
        m_offsetY  = y;
    }

    void bkgColor( Vec4 cl )
    {
        m_bkgColor = cl;
    }

    //! in screen space ( pixels )
    const std::vector< Vec2< float > > & geometry() const
    {
        return m_geometry;
    }
    const std::vector< Vec4 > &   colors() const
    {
        return   m_colors;
    }

    /********************************************
     *
     *  Helpers
     *
     * ******************************************/

    bool pointInViewPort( Vec2< float > p )
    {
        return (
                   p.x() > m_offsetX &&
                   p.x() < m_offsetX + m_width &&
                   p.y() > m_offsetY &&
                   p.y() < m_offsetY + m_height
               );
    }

    const BoxShadowFrame & boxShadow() const
    {
        return m_boxShadow;
    }

    /********************************************
     *
     *  Destructor
     *
     * ******************************************/

    ~ViewPort() {}
};

}

#endif // VIEWPORT

