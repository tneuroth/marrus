#ifndef BOXSHADOWFRAME_HPP
#define BOXSHADOWFRAME_HPP

#include "geometry/Vec.hpp"

namespace TN
{

class BoxShadowFrame
{

    TN::Vec4  m_bkgColor;

    std::vector< Vec2< float > > m_verts;
    std::vector< TN::Vec4 > m_colors;

    float m_height;
    float m_width;

    float m_thickness1;
    float m_thickness2;

public :

    // for now I am implementing for only one thickness. I will add 2 later if I think it's worth it, but then
    // I will need to make the necessary adjustments to the geometry generation.
    BoxShadowFrame() : m_bkgColor( 0.6f, 0.6f, 0.6f, 0.9f ),
        m_verts( 16*3 ), m_colors( 16*3 ), m_thickness1( 3.f ), m_thickness2( 3.f )
    {}

    void convertToNDC()
    {
        if( m_width > 0.f && m_height > 0.f )
        {

            float widthConversion =  2.0 / ( double ) m_width;
            float heightConversion = 2.0 / ( double ) m_height;

            const int SZ = m_verts.size();
            for( int i = 0; i < SZ; ++i )
            {
                m_verts[ i ].x( m_verts[ i ].x() * widthConversion - 1.f );
                m_verts[ i ].y( m_verts[ i ].y() * heightConversion - 1.f );
            }
        }
    }

    void generateGeometry()
    {
        TN::Vec4 transp( 0.6f, 0.6f, 0.6f, 0.f );
        TN::Vec4 lightSideColor( 0.97f, 0.97f, .97f, 0.95f );

        float hOffset = m_height - m_thickness1 - m_thickness2;
        float wOffset = m_width  - m_thickness1 - m_thickness2;

        //t1
        m_verts[  0 ] = Vec2< float >( 0.f , 0.f  );
        m_verts[  1 ] = Vec2< float >( 0.f , m_thickness1 );
        m_verts[  2 ] = Vec2< float >( m_thickness1, m_thickness1 );

        m_colors[  0 ] = m_bkgColor;
        m_colors[  1 ] = m_bkgColor;
        m_colors[  2 ] = transp;

        //t2
        m_verts[  3 ] = Vec2< float >( 0.f , 0.f  );
        m_verts[  4 ] = Vec2< float >( m_thickness1, m_thickness1 );
        m_verts[  5 ] = Vec2< float >( m_thickness1, 0.f  );

        m_colors[  3 ] = m_bkgColor;
        m_colors[  4 ] = transp;
        m_colors[  5 ] = transp;

        //t3
        m_verts[  6 ] = Vec2< float >( 0.f , m_thickness1 );
        m_verts[  7 ] = Vec2< float >( m_thickness1, m_thickness1 );
        m_verts[  8 ] = Vec2< float >( 0.f , m_thickness1 + hOffset );

        m_colors[  6 ] = m_bkgColor;
        m_colors[  7 ] = transp;
        m_colors[  8 ] = m_bkgColor;

        //t4
        m_verts[  9 ] = Vec2< float >( m_thickness1, m_thickness1 );
        m_verts[ 10 ] = Vec2< float >( 0.f , m_thickness1 + hOffset );
        m_verts[ 11 ] = Vec2< float >( m_thickness1, m_thickness1 + hOffset );

        m_colors[  9 ] = transp;
        m_colors[ 10 ] = m_bkgColor;
        m_colors[ 11 ] = transp;

        //t5
        m_verts[ 12 ] = Vec2< float >( 0.f, m_thickness1 + hOffset );
        m_verts[ 13 ] = Vec2< float >( 0.f, m_thickness1 + m_thickness2 + hOffset );
        m_verts[ 14 ] = Vec2< float >( m_thickness1, m_thickness1 + hOffset );

        m_colors[  12 ] = m_bkgColor;
        m_colors[  13 ] = m_bkgColor;
        m_colors[  14 ] = transp;

        //t6
        m_verts[ 15 ] = Vec2< float >( 0.f , m_thickness1 + m_thickness2 + hOffset );
        m_verts[ 16 ] = Vec2< float >( m_thickness1, m_thickness1 + m_thickness2 + hOffset);
        m_verts[ 17 ] = Vec2< float >( m_thickness1, m_thickness1 + hOffset );

        m_colors[  15 ] = lightSideColor;//m_bkgColor;
        m_colors[  16 ] = transp;
        m_colors[  17 ] = transp;

        //t7
        m_verts[ 18 ] = Vec2< float >( /*m_thickness1*/0.f, 0.f  );
        m_verts[ 19 ] = Vec2< float >( /*m_thickness1*/0.f, m_thickness1 );
        m_verts[ 20 ] = Vec2< float >( m_thickness1+wOffset+m_thickness2, 0.f  );

        m_colors[  18 ] = lightSideColor;//m_bkgColor;
        m_colors[  19 ] = transp;
        m_colors[  20 ] = lightSideColor;//m_bkgColor;

        //t8
        m_verts[ 21 ] = Vec2< float >( 0.f , m_thickness1 );
        m_verts[ 22 ] = Vec2< float >( m_thickness1+wOffset+m_thickness2, 0.f   );
        m_verts[ 23 ] = Vec2< float >( m_thickness1+wOffset+m_thickness2, m_thickness1 );

        m_colors[  21 ] = transp;
        m_colors[  22 ] = lightSideColor;//m_bkgColor;
        m_colors[  23 ] = transp;

        //t9
        m_verts[ 24 ] = m_verts[18] + Vec2< float >( 0.f , hOffset + m_thickness1 );
        m_verts[ 25 ] = m_verts[19] + Vec2< float >( 0.f , hOffset + m_thickness1 );
        m_verts[ 26 ] = m_verts[20] + Vec2< float >( 0.f , hOffset + m_thickness1 );

        m_colors[  24 ] = transp;
        m_colors[  25 ] = m_bkgColor;
        m_colors[  26 ] = transp;

        //t10
        m_verts[ 27 ] = m_verts[21] + Vec2< float >( 0.f , hOffset + m_thickness1 );
        m_verts[ 28 ] = m_verts[22] + Vec2< float >( 0.f , hOffset + m_thickness1 );
        m_verts[ 29 ] = m_verts[23] + Vec2< float >( 0.f , hOffset + m_thickness1 );

        m_colors[  27 ] = m_bkgColor;
        m_colors[  28 ] = transp;
        m_colors[  29 ] = m_bkgColor;

        //t11
        m_verts[ 30 ] = m_verts[0] + Vec2< float >( wOffset + m_thickness1, 0.f  );
        m_verts[ 31 ] = m_verts[1] + Vec2< float >( wOffset + m_thickness1, 0.f  );
        m_verts[ 32 ] = m_verts[2] + Vec2< float >( wOffset + m_thickness1, 0.f  );

        m_colors[  30 ] = transp;
        m_colors[  31 ] = transp;
        m_colors[  32 ] = lightSideColor;//m_bkgColor;

        //t12
        m_verts[ 33 ] = m_verts[3] + Vec2< float >( wOffset + m_thickness1, 0.f  );
        m_verts[ 34 ] = m_verts[4] + Vec2< float >( wOffset + m_thickness1, 0.f  );
        m_verts[ 35 ] = m_verts[5] + Vec2< float >( wOffset + m_thickness1, 0.f  );

        m_colors[  33 ] = transp;
        m_colors[  34 ] = lightSideColor;//m_bkgColor;
        m_colors[  35 ] = lightSideColor;//m_bkgColor;

        //t13
        m_verts[ 36 ] = m_verts[6] + Vec2< float >( wOffset + m_thickness1, 0.f  );
        m_verts[ 37 ] = m_verts[7] + Vec2< float >( wOffset + m_thickness1, 0.f  );
        m_verts[ 38 ] = m_verts[8] + Vec2< float >( wOffset + m_thickness1, 0.f  );

        m_colors[  36 ] = transp;
        m_colors[  37 ] = lightSideColor;//m_bkgColor;
        m_colors[  38 ] = transp;

        //t14
        m_verts[ 39 ] = m_verts[9] + Vec2< float >( wOffset + m_thickness1,  0.f  );
        m_verts[ 40 ] = m_verts[10] + Vec2< float >( wOffset + m_thickness1, 0.f  );
        m_verts[ 41 ] = m_verts[11] + Vec2< float >( wOffset + m_thickness1, 0.f  );

        m_colors[  39 ] = lightSideColor;//m_bkgColor;
        m_colors[  40 ] = transp;
        m_colors[  41 ] = lightSideColor;//_bkgColor;

        //t15
        m_verts[ 42 ] = m_verts[12] + Vec2< float >( wOffset + m_thickness1, 0.f  );
        m_verts[ 43 ] = m_verts[13] + Vec2< float >( wOffset + m_thickness1, 0.f  );
        m_verts[ 44 ] = m_verts[14] + Vec2< float >( wOffset + m_thickness1, 0.f  );

        m_colors[  42 ] = transp;
        m_colors[  43 ] = transp;
        m_colors[  44 ] = lightSideColor;//m_bkgColor;

        //t16
        m_verts[ 45 ] = m_verts[15] + Vec2< float >( wOffset + m_thickness1, 0.f  );
        m_verts[ 46 ] = m_verts[16] + Vec2< float >( wOffset + m_thickness1, 0.f  );
        m_verts[ 47 ] = m_verts[17] + Vec2< float >( wOffset + m_thickness1, 0.f  );

        m_colors[  45 ] = transp;
        m_colors[  46 ] = lightSideColor;//m_bkgColor;
        m_colors[  47 ] = lightSideColor;//m_bkgColor;

        convertToNDC();
    }

    const std::vector< Vec2< float > > & verts( )  const
    {
        return m_verts;
    }
    const std::vector< TN::Vec4 > & colors( ) const
    {
        return m_colors;
    }

    void height( float h )
    {
        m_height = h;
    }
    void width( float w )
    {
        m_width = w;
    }

    float height()
    {
        return m_height;
    }
    float width()
    {
        return m_width;
    }
    const TN::Vec4 & bkgColor()
    {
        return m_bkgColor;
    }

    void setSize( float w, float h )
    {
        m_height = h;
        m_width = w;
        generateGeometry();
    }

    void bkgColor( const TN::Vec4 & cl )
    {
        m_bkgColor = cl;
    }
};

}

#endif // BOXSHADOWFRAME_HPP

