
#include "RenderModule.hpp"

#include "render/GLErrorChecking.hpp"
#include "image/ImageLoader.hpp"

#include <random>
#include <iostream>
#include <string>
#include <fstream>
#include <stdexcept>
#include <cstdint>

#include <glm/gtc/matrix_inverse.hpp>

namespace TN{

void RenderModule::setGlyphTextureData( 
    const std::vector< float > & data,
    const int per_instance_size,
    const int num_instances )
{
    std::int64_t previousSize = m_perInstanceGlyphBufferSize * m_nGlyhphInstances;
    std::int64_t newSize      = per_instance_size * num_instances;

    m_perInstanceGlyphBufferSize = per_instance_size;
    m_nGlyhphInstances           = num_instances;

    if( newSize != previousSize )
    {
        GL_CHECK_CALL( glDeleteBuffers( 1, & m_glyphTexBuffer ) );
        GL_CHECK_CALL( glGenBuffers(    1, & m_glyphTexBuffer ) );
    }

    GL_CHECK_CALL( glBindBuffer( GL_TEXTURE_BUFFER, m_glyphTexBuffer ) );
    GL_CHECK_CALL( glBufferData( GL_TEXTURE_BUFFER, sizeof(float) * newSize, data.data(), GL_STATIC_DRAW) );
    GL_CHECK_CALL( glBindTexture( GL_TEXTURE_BUFFER, m_glyphBufferTex ) );
    GL_CHECK_CALL( glTexBuffer(  GL_TEXTURE_BUFFER, GL_R32F, m_glyphTexBuffer ) );    
}

void RenderModule::renderGlyphPreview( 
    const TN::GlyphConfiguration           & config,
    const TN::Vec2< float >                & size,
    const std::vector< float >             & data,
    const std::vector< TN::Vec2< float > > & norms,
    const TN::Vec2< float >                & vNorms,
    bool perGlyphNormalization,
    bool perSelScaleBinNormalization,
    bool normalizeGlyphsFromZero,
    bool allAxisNormalization,
    bool showBorder,
    int scaleMode )
{
    TN::TrackBallCamera camera;
    camera.setUpAndEye(  glm::vec3( 0.f, 1.f, 0.f ), glm::vec3( 0.f, 0.f, 1.f )  );

    setSize( size.x()*3, size.y()*3 );
    setPosition( 0, 0 );
    setCamera( camera );
    clear( { 1.f,1.f,1.f } );

    std::pair< TN::Vec3< float >, TN::Vec3< float > > boundingBox = { 
        { -1.75f, -1.75f, -1.75f },
        {  1.75f,  1.75f,  1.75f }
    };

    TN::Vec3< float > center = { 0.0f,  1.0f,  -12.5f };

    //////////////////////////////////////////////////////////////////

    // axis

    if( config.glyph_type == "directional" )
    {
        // std::vector< TN::Vec3< float > > xAxis =
        // {
        //     center, center + TN::Vec3< float >( 2.f, 0.f, 0.f )
        // };

        // std::vector< TN::Vec3< float > > yAxis =
        // {
        //     center, center + TN::Vec3< float >( 0.f, 2.f, 0.f )
        // };

        // std::vector< TN::Vec3< float > > zAxis =
        // {
        //     center, center + TN::Vec3< float >( 0.f, 0.f, 2.f )
        // };

        // GL_CHECK_CALL( glLineWidth( 40 ) );

        // renderPrimitives(
        //     xAxis,
        //     { 238.f / 255.f, 136.f / 255.f, 40.f / 255.f },
        //     GL_LINES );

        // renderPrimitives(
        //     yAxis,
        //     { 40.f / 255.f, 187.f / 255.f, 150.f / 255.f },
        //     GL_LINES );

        // renderPrimitives(
        //     zAxis,
        //     { 40.f / 255.f, 150.f / 255.f, 221.f / 255.f },
        //     GL_LINES );

        // GL_CHECK_CALL( glLineWidth( 2 ) );
    }

    ///////////////////////////////////////////////////////////////////

    setGlyphTextureData( 
        data,
        data.size(),
        1 );

    renderRadialGlyphs(
        config,
        config.glyph_type != "directional" ? 1.f : 0.6,
        norms,
        vNorms,
        { center.x(), center.y(), center.z() }, // position
        { 0.5f,  0.0f,  -0.5f  },               // forward
        { 0.7f, -0.5,    0.5f  },               // right
        { 0.0f, -0.4f,  -0.7f  },               // up       
        { 0.f                  },               // scalar value
        { 0.9f, 0.7f, 0.6f     },               // color
        0,
        boundingBox,
        TN::GlyphOrentation::Normal,
        scaleMode,
        perGlyphNormalization,
        perSelScaleBinNormalization,
        normalizeGlyphsFromZero,
        allAxisNormalization,
        showBorder,
        true );

    auto tmp = m_material;
    auto occ = m_occlusionRadius;

    renderSSAO( "glyph_preview", { 1.f, 1.f, 1.f } );

    m_material        = tmp;
    m_occlusionRadius = occ;

    GL_CHECK_CALL( glBindFramebuffer( GL_FRAMEBUFFER, 0 ) );
    GL_CHECK_CALL( glDisable(             GL_DEPTH_TEST ) );
    GL_CHECK_CALL( glClear(         GL_DEPTH_BUFFER_BIT ) );
}

void RenderModule::renderGlyphPreview(
	int colorMode,
	const TN::Vec2< float > & size )
{
    TN::TrackBallCamera camera;
    setSize( size.x(), size.y() );
    setPosition( 0, 0 );
    setCamera( camera );
    clear();

    std::pair< TN::Vec3< float >, TN::Vec3< float > > boundingBox = {
        { -1.75f, -1.75f, -1.75f },
        {  1.75f,  1.75f,  1.75f }
    };

    renderGlyphs3DTextured(
        { 0.0f,  0.0f,  0.0f },    // position
        { 0.7f,  0.0f, -0.7f },    // forward
        { 0.7f, -0.5,   0.5f },    // right
        { 0.0f, -0.7f, -0.7f },    // up
        { 0.f },                   // scalar value
        { 0.9f, 0.7f, 0.6f },   // color
        colorMode,
        boundingBox,
        TN::GlyphOrentation::Normal );

    auto tmp = m_material;
    auto occ = m_occlusionRadius;

    renderSSAO( "glyph_preview", { 1.f, 1.f, 1.f } );

    m_material = tmp;
    m_occlusionRadius = occ;

    GL_CHECK_CALL( glBindFramebuffer( GL_FRAMEBUFFER, 0 ) );
    GL_CHECK_CALL( glDisable( GL_DEPTH_TEST ) );
    GL_CHECK_CALL( glClear( GL_DEPTH_BUFFER_BIT ) );
}

void RenderModule::loadGlyphTexture( const std::string & glyphTexture )
{
    std::vector< float > data;
    TN::Vec2< int > dims;

    TN::loadRGBA( 
       glyphTexture,
       data,
       dims );

    m_glyphTextureTemplate.load( data, dims.a(), dims.b(), true );
}

void RenderModule::loadGlyph( const std::string & glyph )
{
    // TODO: handle errors in opening file

    std::vector< TN::Vec3< float > > glyphVerts;
    std::vector< TN::Vec3< float > > glyphNorms;
    std::vector< TN::Vec2< float > > uv;

    m_glyphIsTextured = objIstextured( glyph );

    if( ! m_glyphIsTextured )
    {
        std::cout << "glyph is not textured: " << glyph << std::endl;
        loadObj( glyphVerts, glyphNorms, m_glyphObjOffsets, glyph );    
    }
    else
    {
        loadObj( glyphVerts, glyphNorms, uv, m_glyphObjOffsets, glyph );
    }

    std::cout << "loaded glyph " << glyphVerts.size() << std::endl;

    GL_CHECK_CALL( glBindBuffer( GL_ARRAY_BUFFER, m_glyphPsBuffer ) );
    GL_CHECK_CALL( glBufferData( GL_ARRAY_BUFFER, glyphVerts.size() * 3 * sizeof( float ), glyphVerts.data(), GL_STATIC_DRAW ) );

    GL_CHECK_CALL( glBindBuffer( GL_ARRAY_BUFFER, m_glyphNmBuffer ) );
    GL_CHECK_CALL( glBufferData( GL_ARRAY_BUFFER, glyphNorms.size() * 3 * sizeof( float ), glyphNorms.data(), GL_STATIC_DRAW ) );

    GL_CHECK_CALL( glBindBuffer( GL_ARRAY_BUFFER, m_glyphTcBuffer ) );
    GL_CHECK_CALL( glBufferData( GL_ARRAY_BUFFER, uv.size() * 2 * sizeof( float ), uv.data(), GL_STATIC_DRAW ) );

    NSPHERE_VERTS = glyphVerts.size() * 3;  
}

void RenderModule::renderRadialGlyphs(
    const TN::GlyphConfiguration & glyph,
    const float glyphScale,
    const std::vector< TN::Vec2< float > > & axes_lims,
    const TN::Vec2< float > & vector_lims,
    const std::vector< float > & positions,
    const std::vector< float > & u,
    const std::vector< float > & v,
    const std::vector< float > & n,
    const std::vector< float > & c,
    const TN::Vec3< float >    & color,
    int colorMode,
    const std::pair< TN::Vec3< float >, TN::Vec3< float > > & bbox,
    int orientationMode,
    int scaleMode,
    bool perGlyphNormalization,
    bool perSelBinNormalization,
    bool normalizeGlyphsFromZero,
    bool allAxisNormalization,
    bool showBorder,
    bool isPreview )
{
    //////////////// buffers //////////////////////////////////

    auto program = glyph.glyph_type == "radial" ? m_radialGlyphProgram 
                 : glyph.glyph_type == "star"   ? m_starGlyphProgram
                 : m_directionalGlyphProgram;

    GL_CHECK_CALL( glBindVertexArray( m_glyphVAO ) ); 

    // positions

    GL_CHECK_CALL( glBindBuffer( GL_ARRAY_BUFFER, m_glyphOffsetBuffer ) );
    GL_CHECK_CALL( glBufferData( GL_ARRAY_BUFFER, positions.size() * sizeof( float ), positions.data(), GL_DYNAMIC_DRAW ) );

    // u

    GL_CHECK_CALL( glBindBuffer( GL_ARRAY_BUFFER, m_glyphDirection1Buffer ) );
    GL_CHECK_CALL( glBufferData( GL_ARRAY_BUFFER, u.size() * sizeof( float ), u.data(), GL_DYNAMIC_DRAW ) );

    // v

    GL_CHECK_CALL( glBindBuffer( GL_ARRAY_BUFFER, m_glyphDirection2Buffer ) );
    GL_CHECK_CALL( glBufferData( GL_ARRAY_BUFFER, v.size() * sizeof( float ), v.data(), GL_DYNAMIC_DRAW ) );

    // v

    GL_CHECK_CALL( glBindBuffer( GL_ARRAY_BUFFER, m_glyphNormalBuffer ) );
    GL_CHECK_CALL( glBufferData( GL_ARRAY_BUFFER, n.size() * sizeof( float ), n.data(), GL_DYNAMIC_DRAW ) );

    // c 

    if( colorMode == TN::GlyphColorMode::Scalar )
    {
        GL_CHECK_CALL( glBindBuffer( GL_ARRAY_BUFFER, m_glyphScalarBuffer ) );
        GL_CHECK_CALL( glBufferData( GL_ARRAY_BUFFER, c.size() * sizeof( float ), c.data(), GL_DYNAMIC_DRAW ) );
    }

    ///////////////////////////////////////////////////////////////////////////////////////

    // Transformations

    glm::mat4 proj = m_camera.matProj();
    glm::mat4 M( 1.f );

    double xW = ( bbox.second.x() - bbox.first.x() );
    double yW = ( bbox.second.y() - bbox.first.y() );    
    double zW = ( bbox.second.z() - bbox.first.z() );

    float maxWidth = std::max( std::max( xW, yW ), zW );

    TN::Vec3< float > gs = { 1.0 / maxWidth, 1.0 / maxWidth, 1.0 / maxWidth  };

    TN::Vec3< float > gt = {
        -( bbox.first.x() + xW / 2.0 ),
        -( bbox.first.y() + yW / 2.0 ),    
        -( bbox.first.z() + zW / 2.0 ) };

    glm::mat4 gS( 1.f );
    glm::mat4 gT( 1.f );

    gS = glm::scale( gS, { 1.0 / maxWidth, 1.0 / maxWidth, 1.0 / maxWidth  } );

    gT = glm::translate( gT, {
        -( bbox.first.x() + xW / 2.0 ),
        -( bbox.first.y() + yW / 2.0 ),    
        -( bbox.first.z() + zW / 2.0 ) } );

    M = glm::scale( M, { 1.0 / maxWidth, 1.0 / maxWidth, 1.0 / maxWidth  } );

    M = glm::translate( M, {
        -( bbox.first.x() + xW / 2.0 ),
        -( bbox.first.y() + yW / 2.0 ),    
        -( bbox.first.z() + zW / 2.0 ) } );

    auto V = m_camera.matView( 1.f );

    glm::mat4 MV  = V    * M;
    glm::mat4 MVP = proj * MV;
    glm::mat3 NM  = glm::inverseTranspose( glm::mat3( MV ) );

    ///////////////////////////////////////////////////////////////////////////

    // Settings

    GL_CHECK_CALL( glDisable (GL_BLEND) );
    GL_CHECK_CALL( glDisable( GL_MULTISAMPLE ) );
    GL_CHECK_CALL( glEnable( GL_DEPTH_TEST ) );

    GL_CHECK_CALL( glBindFramebuffer(GL_FRAMEBUFFER, gBuffer) );
    GL_CHECK_CALL( glUseProgram( program ) );

    //////////////////////////////////////////////////////////////////////////////

    // Uniforms for Glyph Data Parameters
    // At least for now it is assumed each axis has the same number of components.

    GLuint loc;

    /////////////////////

    loc = GL_CHECK_CALL_ASSIGN( glGetUniformLocation( program, "gs" ) );
    GL_CHECK_CALL( glUniform3fv( loc, 1, (float*) & gs ) );

    loc = GL_CHECK_CALL_ASSIGN( glGetUniformLocation( program, "gt" ) );
    GL_CHECK_CALL( glUniform3fv( loc, 1, (float*) & gt ) );

    loc = GL_CHECK_CALL_ASSIGN( glGetUniformLocation( program, "gS" ) );
    GL_CHECK_CALL( glUniformMatrix4fv( loc, 1, GL_FALSE, &gS[ 0 ][ 0 ] ) );

    loc = GL_CHECK_CALL_ASSIGN( glGetUniformLocation( program, "gT" ) );
    GL_CHECK_CALL( glUniformMatrix4fv( loc, 1, GL_FALSE, &gT[ 0 ][ 0 ] ) );

    //////////////////////////

    int nAxes = glyph.glyph_type == "directional" ? glyph.vectors.size() : axes_lims.size();
    int nComp = glyph.glyph_type == "directional" ? 3 : glyph.axes[ 0 ].components.size();

    loc = GL_CHECK_CALL_ASSIGN( glGetUniformLocation( program, "nAxes" ) );
    GL_CHECK_CALL( glUniform1i( loc, nAxes ) );

    loc = GL_CHECK_CALL_ASSIGN( glGetUniformLocation( program, "nComp" ) );
    GL_CHECK_CALL( glUniform1i( loc, nComp ) );

    loc = GL_CHECK_CALL_ASSIGN( glGetUniformLocation( program, "scaleMode" ) );
    GL_CHECK_CALL( glUniform1i( loc, scaleMode ) );

    if( glyph.glyph_type == "star" )
    {
        int activeComponent = glyph.active_components.size() > 0 ? glyph.active_components[ 0 ] : 0;
        
        loc = GL_CHECK_CALL_ASSIGN( glGetUniformLocation( program, "activeComponent" ) );
        GL_CHECK_CALL( glUniform1i( loc, activeComponent ) );

        loc = GL_CHECK_CALL_ASSIGN( glGetUniformLocation( program, "componentColors" ) );
        GL_CHECK_CALL( glUniform3fv( loc, glyph.component_colors.size(), (float*) glyph.component_colors.data() ) );

        loc = GL_CHECK_CALL_ASSIGN( glGetUniformLocation( program, "useComponentColors" ) );
        GL_CHECK_CALL( glUniform1i( loc, true ) );
    }

    if( glyph.glyph_type == "star" || glyph.glyph_type == "radial" )
    {
        loc = GL_CHECK_CALL_ASSIGN( glGetUniformLocation( program, "axes_indices" ) );
        GL_CHECK_CALL( glUniform1iv( loc, glyph.active_axes.size(), (int*) glyph.active_axes.data() ) );

        loc = GL_CHECK_CALL_ASSIGN( glGetUniformLocation( program, "n_active_axes" ) );
        GL_CHECK_CALL( glUniform1i( loc, glyph.active_axes.size() ) );

        loc = GL_CHECK_CALL_ASSIGN( glGetUniformLocation( program, "perGlyphNormalization" ) );
        GL_CHECK_CALL( glUniform1i( loc, (int) perGlyphNormalization ) );

        loc = GL_CHECK_CALL_ASSIGN( glGetUniformLocation( program, "perSelBinNormalization" ) );
        GL_CHECK_CALL( glUniform1i( loc, (int) perSelBinNormalization ) );   

        loc = GL_CHECK_CALL_ASSIGN( glGetUniformLocation( program, "normalizeFromZero" ) );
        GL_CHECK_CALL( glUniform1i( loc, normalizeGlyphsFromZero ) );   

        loc = GL_CHECK_CALL_ASSIGN( glGetUniformLocation( program, "allAxisNormalization" ) );
        GL_CHECK_CALL( glUniform1i( loc, allAxisNormalization ) );   

        loc = GL_CHECK_CALL_ASSIGN( glGetUniformLocation( program, "showBorders" ) );
        GL_CHECK_CALL( glUniform1i( loc, showBorder ) );   
    }

    if( glyph.glyph_type == "radial" )
    {
        loc = GL_CHECK_CALL_ASSIGN( glGetUniformLocation( program, "comp_indices" ) );
        GL_CHECK_CALL( glUniform1iv( loc, glyph.active_components.size(), (int*) glyph.active_components.data() ) );

        loc = GL_CHECK_CALL_ASSIGN( glGetUniformLocation( program, "n_active_comp" ) );
        GL_CHECK_CALL( glUniform1i( loc, glyph.active_components.size() ) );
    }

    loc = GL_CHECK_CALL_ASSIGN( glGetUniformLocation( program, "axes_lims" ) );
    GL_CHECK_CALL( glUniform2fv( loc, axes_lims.size(), (float*) axes_lims.data() ) );

    loc = GL_CHECK_CALL_ASSIGN( glGetUniformLocation( program, "vector_lims" ) );
    GL_CHECK_CALL( glUniform2fv( loc, 1, (float*) & vector_lims ) );

    GLuint instanceDivisor = glyph.glyph_type == "directional" ? nAxes : 1; 

    if( glyph.glyph_type == "directional" )
    {
        loc = GL_CHECK_CALL_ASSIGN( glGetUniformLocation( program, "invert" ) );
        GL_CHECK_CALL( glUniform1i( loc, ! isPreview ) );

        loc = GL_CHECK_CALL_ASSIGN( glGetUniformLocation( program, "nInstances" ) );
        GL_CHECK_CALL( glUniform1i( loc, instanceDivisor ) );

        if( m_glyphObjOffsets.size() >= 4 )
        {
            loc = GL_CHECK_CALL_ASSIGN( glGetUniformLocation( program, "coneOffset" ) );
            GL_CHECK_CALL( glUniform1i( loc, m_glyphObjOffsets[0] ) );

            loc = GL_CHECK_CALL_ASSIGN( glGetUniformLocation( program, "cylinderOffset" ) );
            GL_CHECK_CALL( glUniform1i( loc, m_glyphObjOffsets[1] ) );

            loc = GL_CHECK_CALL_ASSIGN( glGetUniformLocation( program, "coneBaseOffset" ) );
            GL_CHECK_CALL( glUniform1i( loc, m_glyphObjOffsets[2] ) );
        }

        loc = GL_CHECK_CALL_ASSIGN( glGetUniformLocation( program, "vectorColors" ) );
        GL_CHECK_CALL( glUniform3fv( loc, glyph.vectorColors.size(), (float*) glyph.vectorColors.data() ) );

        // create an array with <0|1> for each vector indicating whether it should be shown or not
        std::vector< int > activations( glyph.vectorColors.size(), 0 );
        for( int i = 0; i < glyph.active_axes.size(); ++i )
        {
            activations[ glyph.active_axes[ i ] ] = 1;
        }

        loc = GL_CHECK_CALL_ASSIGN( glGetUniformLocation( program, "activeVectors" ) );
        GL_CHECK_CALL( glUniform1iv( loc, activations.size(), (int*) activations.data() ) );
    }

    //////////////////////////////////////////////////////////////////////////////

    // Uniforms

    loc = GL_CHECK_CALL_ASSIGN( glGetUniformLocation( program, "orientationMode" ) );
    GL_CHECK_CALL( glUniform1i( loc, orientationMode ) );

    loc = GL_CHECK_CALL_ASSIGN( glGetUniformLocation( program, "scale" ) );
    GL_CHECK_CALL( glUniform1f( loc, glyphScale ) );

    GL_CHECK_CALL( glActiveTexture( GL_TEXTURE0 ) );
    m_tfTexture.bind();
    loc = GL_CHECK_CALL_ASSIGN( glGetUniformLocation( program, "tf" ) );
    GL_CHECK_CALL( glUniform1i( loc, 0 ) );

    GL_CHECK_CALL( glActiveTexture( GL_TEXTURE1 ) );
    m_glyphTextureTemplate.bind();
    loc = GL_CHECK_CALL_ASSIGN( glGetUniformLocation( program, "textureImage" ) );
    GL_CHECK_CALL( glUniform1i( loc, 1 ) );

    GL_CHECK_CALL( glActiveTexture( GL_TEXTURE2 ) );
    GL_CHECK_CALL( glBindTexture( GL_TEXTURE_BUFFER, m_glyphBufferTex ) );
    loc = GL_CHECK_CALL_ASSIGN( glGetUniformLocation( program, "bufferTex" ) );
    GL_CHECK_CALL( glUniform1i( loc, 2 ) );

    GL_CHECK_CALL( glActiveTexture( GL_TEXTURE3 ) );
    m_glyphTFTexture.bind();    
    loc = GL_CHECK_CALL_ASSIGN( glGetUniformLocation( program, "axes_tfs" ) );
    GL_CHECK_CALL( glUniform1i( loc, 3 ) );

    loc = GL_CHECK_CALL_ASSIGN( glGetUniformLocation( program, "nBins" ) );
    GL_CHECK_CALL( glUniform1i( loc, m_perInstanceGlyphBufferSize ) );

    TN::Vec2< float > range( -1.f, 1.f ); 
    loc = GL_CHECK_CALL_ASSIGN( glGetUniformLocation( program, "scalarMappingRange" ) );
    GL_CHECK_CALL( glUniform2fv( loc, 1, (float*) & range ) );

    loc = GL_CHECK_CALL_ASSIGN( glGetUniformLocation( program, "model" ) );
    GL_CHECK_CALL( glUniformMatrix4fv( loc, 1, GL_FALSE, &M[ 0 ][ 0 ] ) );

    loc = GL_CHECK_CALL_ASSIGN( glGetUniformLocation( program, "view" ) );
    GL_CHECK_CALL( glUniformMatrix4fv( loc, 1, GL_FALSE, &V[ 0 ][ 0 ] ) );

    loc = GL_CHECK_CALL_ASSIGN( glGetUniformLocation( program, "proj" ) );
    GL_CHECK_CALL( glUniformMatrix4fv( loc, 1, GL_FALSE, &proj[ 0 ][ 0 ] ) );

    loc = GL_CHECK_CALL_ASSIGN( glGetUniformLocation( program, "modelView" ) );
    GL_CHECK_CALL( glUniformMatrix4fv( loc, 1, GL_FALSE, &MV[ 0 ][ 0 ] ) );

    loc = GL_CHECK_CALL_ASSIGN( glGetUniformLocation( program, "normalMatrix" ) );
    GL_CHECK_CALL( glUniformMatrix3fv( loc, 1, GL_FALSE, &NM[ 0 ][ 0 ] ) );

    loc = GL_CHECK_CALL_ASSIGN( glGetUniformLocation( program, "mvp" ) );
    GL_CHECK_CALL( glUniformMatrix4fv( loc, 1, GL_FALSE, &MVP[ 0 ][ 0 ] ) );

    // attributes ///////////////////////////////////////////////////////

    // Base Instance Model

    GL_CHECK_CALL( glBindVertexArray( m_glyphVAO ) ); 

    GL_CHECK_CALL( glEnableVertexAttribArray( 0 ) );
    GL_CHECK_CALL( glBindBuffer( GL_ARRAY_BUFFER, m_glyphPsBuffer ) );
    GL_CHECK_CALL( glVertexAttribPointer(
        0,          // attribute
        3,          // size
        GL_FLOAT,   // type
        GL_FALSE,   // normalized?
        0,          // stride
        (void*)0    // array buffer offset
    ) );
    GL_CHECK_CALL( glVertexAttribDivisor( 0, 0 ) );

    GL_CHECK_CALL( glEnableVertexAttribArray( 1 ) );
    GL_CHECK_CALL( glBindBuffer( GL_ARRAY_BUFFER, m_glyphNmBuffer ) );
    GL_CHECK_CALL( glVertexAttribPointer(
        1,          // attribute
        3,          // size
        GL_FLOAT,   // type
        GL_FALSE,   // normalized?
        0,          // stride
        (void*)0    // array buffer offset
    ) );
    GL_CHECK_CALL( glVertexAttribDivisor( 1, 0 ) );

    if( colorMode == TN::GlyphColorMode::Texture )
    {
        GL_CHECK_CALL( glEnableVertexAttribArray( 2 ) );
        GL_CHECK_CALL( glBindBuffer( GL_ARRAY_BUFFER, m_glyphTcBuffer ) );
        GL_CHECK_CALL( glVertexAttribPointer(
            2,          // attribute
            2,          // size
            GL_FLOAT,   // type
            GL_FALSE,   // normalized?
            0,          // stride
            (void*)0    // array buffer offset
        ) );
        GL_CHECK_CALL( glVertexAttribDivisor( 2, 0 ) );
    }

    ///////////////////////////////////////////////////////////////////////

    // Per-Instance Attributes

    GL_CHECK_CALL( glEnableVertexAttribArray( 3 ) );
    GL_CHECK_CALL( glBindBuffer( GL_ARRAY_BUFFER, m_glyphOffsetBuffer ) );
    GL_CHECK_CALL( glVertexAttribPointer(
        3,          // attribute
        3,          // size
        GL_FLOAT,   // type
        GL_FALSE,   // normalized?
        0,          // stride
        (void*)0    // array buffer offset
    ) );
    GL_CHECK_CALL( glVertexAttribDivisor( 3, instanceDivisor ) );

    GL_CHECK_CALL( glEnableVertexAttribArray( 4 ) );
    GL_CHECK_CALL( glBindBuffer( GL_ARRAY_BUFFER, m_glyphDirection1Buffer ) );
    GL_CHECK_CALL( glVertexAttribPointer(
        4,          // attribute
        3,          // size
        GL_FLOAT,   // type
        GL_FALSE,   // normalized?
        0,          // stride
        (void*)0    // array buffer offset
    ) );
    GL_CHECK_CALL( glVertexAttribDivisor( 4, instanceDivisor ) );

    GL_CHECK_CALL( glEnableVertexAttribArray( 5 ) );
    GL_CHECK_CALL( glBindBuffer( GL_ARRAY_BUFFER, m_glyphDirection2Buffer ) );
    GL_CHECK_CALL( glVertexAttribPointer(
        5,          // attribute
        3,          // size
        GL_FLOAT,   // type
        GL_FALSE,   // normalized?
        0,          // stride
        (void*)0    // array buffer offset
    ) );
    GL_CHECK_CALL( glVertexAttribDivisor( 5, instanceDivisor ) );

    GL_CHECK_CALL( glEnableVertexAttribArray( 6 ) );
    GL_CHECK_CALL( glBindBuffer( GL_ARRAY_BUFFER, m_glyphNormalBuffer ) );
    GL_CHECK_CALL( glVertexAttribPointer(
        6,          // attribute
        3,          // size
        GL_FLOAT,   // type
        GL_FALSE,   // normalized?
        0,          // stride
        (void*)0    // array buffer offset
    ) );
    GL_CHECK_CALL( glVertexAttribDivisor( 6, instanceDivisor ) );

    if( colorMode == TN::GlyphColorMode::Scalar )
    {
        GL_CHECK_CALL( glEnableVertexAttribArray( 7 ) );
        GL_CHECK_CALL( glBindBuffer( GL_ARRAY_BUFFER, m_glyphScalarBuffer ) );
        GL_CHECK_CALL( glVertexAttribPointer(
            7,          // attribute
            1,          // size
            GL_FLOAT,   // type
            GL_FALSE,   // normalized?
            0,          // stride
            (void*)0    // array buffer offset
        ) );
        GL_CHECK_CALL( glVertexAttribDivisor( 7, instanceDivisor ) );
    }

    // render //////////////////////////////////////////////////////////////////////

    GL_CHECK_CALL( glDrawArraysInstanced( 
        GL_TRIANGLES, 
        0, 
        NSPHERE_VERTS, 
        instanceDivisor * positions.size() / 3 ) );    
}

void RenderModule::loadGlyph( const std::string & glyph, const std::string & glyphTexture )
{
    loadGlyph( glyph );
    loadGlyphTexture( glyphTexture );
}

void RenderModule::renderGlyphs3DTexBuffer(
    const std::vector< float > & positions,
    const std::vector< float > & u,
    const std::vector< float > & v,
    const std::vector< float > & n,
    const std::vector< float > & c,
    const TN::Vec3< float >    & color,
    int colorMode,
    const std::pair< TN::Vec3< float >, TN::Vec3< float > > & bbox,
    int orientationMode )
{
    //////////////// buffers //////////////////////////////////

    auto program = m_glyph3DProgramBuff;

    GL_CHECK_CALL( glBindVertexArray( m_glyphVAO ) ); 

    // positions

    GL_CHECK_CALL( glBindBuffer( GL_ARRAY_BUFFER, m_glyphOffsetBuffer ) );
    GL_CHECK_CALL( glBufferData( GL_ARRAY_BUFFER, positions.size() * sizeof( float ), positions.data(), GL_DYNAMIC_DRAW ) );

    // u

    GL_CHECK_CALL( glBindBuffer( GL_ARRAY_BUFFER, m_glyphDirection1Buffer ) );
    GL_CHECK_CALL( glBufferData( GL_ARRAY_BUFFER, u.size() * sizeof( float ), u.data(), GL_DYNAMIC_DRAW ) );

    // v

    GL_CHECK_CALL( glBindBuffer( GL_ARRAY_BUFFER, m_glyphDirection2Buffer ) );
    GL_CHECK_CALL( glBufferData( GL_ARRAY_BUFFER, v.size() * sizeof( float ), v.data(), GL_DYNAMIC_DRAW ) );

    // v

    GL_CHECK_CALL( glBindBuffer( GL_ARRAY_BUFFER, m_glyphNormalBuffer ) );
    GL_CHECK_CALL( glBufferData( GL_ARRAY_BUFFER, n.size() * sizeof( float ), n.data(), GL_DYNAMIC_DRAW ) );

    // c 

    if( colorMode == TN::GlyphColorMode::Scalar )
    {
        GL_CHECK_CALL( glBindBuffer( GL_ARRAY_BUFFER, m_glyphScalarBuffer ) );
        GL_CHECK_CALL( glBufferData( GL_ARRAY_BUFFER, c.size() * sizeof( float ), c.data(), GL_DYNAMIC_DRAW ) );
    }

    ///////////////////////////////////////////////////////////////////////////////////////

    // Transformations

    glm::mat4 proj = m_camera.matProj();
    glm::mat4 M( 1.f );

    double xW = ( bbox.second.x() - bbox.first.x() );
    double yW = ( bbox.second.y() - bbox.first.y() );    
    double zW = ( bbox.second.z() - bbox.first.z() );

    float maxWidth = std::max( std::max( xW, yW ), zW );

    M = glm::scale( M, { 1.0 / maxWidth, 1.0 / maxWidth, 1.0 / maxWidth  } );

    M = glm::translate( M, {
        -( bbox.first.x() + xW / 2.0 ),
        -( bbox.first.y() + yW / 2.0 ),    
        -( bbox.first.z() + zW / 2.0 ) } );

    auto V = m_camera.matView( 1.f );

    glm::mat4 MV = V * M;
    glm::mat4 MVP = proj * MV;
    glm::mat3 NM = glm::inverseTranspose( glm::mat3( MV ) );

    ///////////////////////////////////////////////////////////////////////////

    // Settings

    GL_CHECK_CALL( glDisable (GL_BLEND) );
    GL_CHECK_CALL( glDisable( GL_MULTISAMPLE ) );
    GL_CHECK_CALL( glEnable( GL_DEPTH_TEST ) );

    GL_CHECK_CALL( glBindFramebuffer(GL_FRAMEBUFFER, gBuffer) );
    GL_CHECK_CALL( glUseProgram( program ) );

    ////////////////////////////////////////////////////////////////////////////

    // Uniforms

    GLuint loc;
    loc = GL_CHECK_CALL_ASSIGN( glGetUniformLocation( program, "colorMode" ) );
    GL_CHECK_CALL( glUniform1i( loc, colorMode ) );

    loc = GL_CHECK_CALL_ASSIGN( glGetUniformLocation( program, "orientationMode" ) );
    GL_CHECK_CALL( glUniform1i( loc, orientationMode ) );

    loc = GL_CHECK_CALL_ASSIGN( glGetUniformLocation( program, "scale" ) );
    GL_CHECK_CALL( glUniform1f( loc, 1.7f ) );

    GL_CHECK_CALL( glActiveTexture( GL_TEXTURE0 ) );
    m_tfTexture.bind();
    loc = GL_CHECK_CALL_ASSIGN( glGetUniformLocation( program, "tf" ) );
    GL_CHECK_CALL( glUniform1i( loc, 0 ) );

    GL_CHECK_CALL( glActiveTexture( GL_TEXTURE1 ) );
    m_glyphTextureTemplate.bind();
    loc = GL_CHECK_CALL_ASSIGN( glGetUniformLocation( program, "textureImage" ) );
    GL_CHECK_CALL( glUniform1i( loc, 1 ) );

    GL_CHECK_CALL( glActiveTexture( GL_TEXTURE2 ) );
    GL_CHECK_CALL( glBindTexture( GL_TEXTURE_BUFFER, m_glyphBufferTex ) );
    loc = GL_CHECK_CALL_ASSIGN( glGetUniformLocation( program, "bufferTex" ) );
    GL_CHECK_CALL( glUniform1i( loc, 2 ) );

    loc = GL_CHECK_CALL_ASSIGN( glGetUniformLocation( program, "nBins" ) );
    GL_CHECK_CALL( glUniform1i( loc, m_perInstanceGlyphBufferSize ) );

    TN::Vec2< float > range( -1.f, 1.f ); 
    loc = GL_CHECK_CALL_ASSIGN( glGetUniformLocation( program, "scalarMappingRange" ) );
    GL_CHECK_CALL( glUniform2fv( loc, 1, (float*) & range ) );

    loc = GL_CHECK_CALL_ASSIGN( glGetUniformLocation( program, "model" ) );
    GL_CHECK_CALL( glUniformMatrix4fv( loc, 1, GL_FALSE, &M[ 0 ][ 0 ] ) );

    loc = GL_CHECK_CALL_ASSIGN( glGetUniformLocation( program, "view" ) );
    GL_CHECK_CALL( glUniformMatrix4fv( loc, 1, GL_FALSE, &V[ 0 ][ 0 ] ) );

    loc = GL_CHECK_CALL_ASSIGN( glGetUniformLocation( program, "modelView" ) );
    GL_CHECK_CALL( glUniformMatrix4fv( loc, 1, GL_FALSE, &MV[ 0 ][ 0 ] ) );

    loc = GL_CHECK_CALL_ASSIGN( glGetUniformLocation( program, "normalMatrix" ) );
    GL_CHECK_CALL( glUniformMatrix3fv( loc, 1, GL_FALSE, &NM[ 0 ][ 0 ] ) );

    loc = GL_CHECK_CALL_ASSIGN( glGetUniformLocation( program, "mvp" ) );
    GL_CHECK_CALL( glUniformMatrix4fv( loc, 1, GL_FALSE, &MVP[ 0 ][ 0 ] ) );  

    loc = GL_CHECK_CALL_ASSIGN( glGetUniformLocation( program, "solidColor" ) );
    GL_CHECK_CALL( glUniform3fv( loc, 1, (float*) & color ) );

    // attributes ///////////////////////////////////////////////////////

    // Base Instance Model

    GL_CHECK_CALL( glBindVertexArray( m_glyphVAO ) ); 

    GL_CHECK_CALL( glEnableVertexAttribArray( 0 ) );
    GL_CHECK_CALL( glBindBuffer( GL_ARRAY_BUFFER, m_glyphPsBuffer ) );
    GL_CHECK_CALL( glVertexAttribPointer(
        0,          // attribute
        3,          // size
        GL_FLOAT,   // type
        GL_FALSE,   // normalized?
        0,          // stride
        (void*)0    // array buffer offset
    ) );
    GL_CHECK_CALL( glVertexAttribDivisor( 0, 0 ) );

    GL_CHECK_CALL( glEnableVertexAttribArray( 1 ) );
    GL_CHECK_CALL( glBindBuffer( GL_ARRAY_BUFFER, m_glyphNmBuffer ) );
    GL_CHECK_CALL( glVertexAttribPointer(
        1,          // attribute
        3,          // size
        GL_FLOAT,   // type
        GL_FALSE,   // normalized?
        0,          // stride
        (void*)0    // array buffer offset
    ) );
    GL_CHECK_CALL( glVertexAttribDivisor( 1, 0 ) );

    if( colorMode == TN::GlyphColorMode::Texture )
    {
        GL_CHECK_CALL( glEnableVertexAttribArray( 2 ) );
        GL_CHECK_CALL( glBindBuffer( GL_ARRAY_BUFFER, m_glyphTcBuffer ) );
        GL_CHECK_CALL( glVertexAttribPointer(
            2,          // attribute
            2,          // size
            GL_FLOAT,   // type
            GL_FALSE,   // normalized?
            0,          // stride
            (void*)0    // array buffer offset
        ) );
        GL_CHECK_CALL( glVertexAttribDivisor( 2, 0 ) );
    }

    ///////////////////////////////////////////////////////////////////////

    // Per-Instance Attributes

    GL_CHECK_CALL( glEnableVertexAttribArray( 3 ) );
    GL_CHECK_CALL( glBindBuffer( GL_ARRAY_BUFFER, m_glyphOffsetBuffer ) );
    GL_CHECK_CALL( glVertexAttribPointer(
        3,          // attribute
        3,          // size
        GL_FLOAT,   // type
        GL_FALSE,   // normalized?
        0,          // stride
        (void*)0    // array buffer offset
    ) );
    GL_CHECK_CALL( glVertexAttribDivisor( 3, 1 ) );


    GL_CHECK_CALL( glEnableVertexAttribArray( 4 ) );
    GL_CHECK_CALL( glBindBuffer( GL_ARRAY_BUFFER, m_glyphDirection1Buffer ) );
    GL_CHECK_CALL( glVertexAttribPointer(
        4,          // attribute
        3,          // size
        GL_FLOAT,   // type
        GL_FALSE,   // normalized?
        0,          // stride
        (void*)0    // array buffer offset
    ) );
    GL_CHECK_CALL( glVertexAttribDivisor( 4, 1 ) );

    GL_CHECK_CALL( glEnableVertexAttribArray( 5 ) );
    GL_CHECK_CALL( glBindBuffer( GL_ARRAY_BUFFER, m_glyphDirection2Buffer ) );
    GL_CHECK_CALL( glVertexAttribPointer(
        5,          // attribute
        3,          // size
        GL_FLOAT,   // type
        GL_FALSE,   // normalized?
        0,          // stride
        (void*)0    // array buffer offset
    ) );
    GL_CHECK_CALL( glVertexAttribDivisor( 5, 1 ) );

    GL_CHECK_CALL( glEnableVertexAttribArray( 6 ) );
    GL_CHECK_CALL( glBindBuffer( GL_ARRAY_BUFFER, m_glyphNormalBuffer ) );
    GL_CHECK_CALL( glVertexAttribPointer(
        6,          // attribute
        3,          // size
        GL_FLOAT,   // type
        GL_FALSE,   // normalized?
        0,          // stride
        (void*)0    // array buffer offset
    ) );
    GL_CHECK_CALL( glVertexAttribDivisor( 6, 1 ) );

    if( colorMode == TN::GlyphColorMode::Scalar )
    {
        GL_CHECK_CALL( glEnableVertexAttribArray( 7 ) );
        GL_CHECK_CALL( glBindBuffer( GL_ARRAY_BUFFER, m_glyphScalarBuffer ) );
        GL_CHECK_CALL( glVertexAttribPointer(
            7,          // attribute
            1,          // size
            GL_FLOAT,   // type
            GL_FALSE,   // normalized?
            0,          // stride
            (void*)0    // array buffer offset
        ) );
        GL_CHECK_CALL( glVertexAttribDivisor( 7, 1 ) );
    }

    GL_CHECK_CALL( glDrawArraysInstanced( GL_TRIANGLES, 0, NSPHERE_VERTS, positions.size() / 3 ) );
}

void RenderModule::renderGlyphs3DTextured(
    const std::vector< float > & positions,
    const std::vector< float > & u,
    const std::vector< float > & v,
    const std::vector< float > & n,
    const std::vector< float > & c,
    const TN::Vec3< float >    & color,
    int colorMode,
    const std::pair< TN::Vec3< float >, TN::Vec3< float > > & bbox,
    int orientationMode )
{
    //////////////// buffers //////////////////////////////////

    auto program = m_glyph3DProgramBuff;

    GL_CHECK_CALL( glBindVertexArray( m_glyphVAO ) ); 

    // positions

    GL_CHECK_CALL( glBindBuffer( GL_ARRAY_BUFFER, m_glyphOffsetBuffer ) );
    GL_CHECK_CALL( glBufferData( GL_ARRAY_BUFFER, positions.size() * sizeof( float ), positions.data(), GL_DYNAMIC_DRAW ) );

    // u

    GL_CHECK_CALL( glBindBuffer( GL_ARRAY_BUFFER, m_glyphDirection1Buffer ) );
    GL_CHECK_CALL( glBufferData( GL_ARRAY_BUFFER, u.size() * sizeof( float ), u.data(), GL_DYNAMIC_DRAW ) );

    // v

    GL_CHECK_CALL( glBindBuffer( GL_ARRAY_BUFFER, m_glyphDirection2Buffer ) );
    GL_CHECK_CALL( glBufferData( GL_ARRAY_BUFFER, v.size() * sizeof( float ), v.data(), GL_DYNAMIC_DRAW ) );

    // v

    GL_CHECK_CALL( glBindBuffer( GL_ARRAY_BUFFER, m_glyphNormalBuffer ) );
    GL_CHECK_CALL( glBufferData( GL_ARRAY_BUFFER, n.size() * sizeof( float ), n.data(), GL_DYNAMIC_DRAW ) );

    // c 

    if( colorMode == TN::GlyphColorMode::Scalar )
    {
	    GL_CHECK_CALL( glBindBuffer( GL_ARRAY_BUFFER, m_glyphScalarBuffer ) );
	    GL_CHECK_CALL( glBufferData( GL_ARRAY_BUFFER, c.size() * sizeof( float ), c.data(), GL_DYNAMIC_DRAW ) );
    }

    ///////////////////////////////////////////////////////////////////////////////////////

    // Transformations

    glm::mat4 proj = m_camera.matProj();
    glm::mat4 M( 1.f );

    double xW = ( bbox.second.x() - bbox.first.x() );
    double yW = ( bbox.second.y() - bbox.first.y() );    
    double zW = ( bbox.second.z() - bbox.first.z() );

    float maxWidth = std::max( std::max( xW, yW ), zW );

    M = glm::scale( M, { 1.0 / maxWidth, 1.0 / maxWidth, 1.0 / maxWidth  } );

    M = glm::translate( M, {
        -( bbox.first.x() + xW / 2.0 ),
        -( bbox.first.y() + yW / 2.0 ),    
        -( bbox.first.z() + zW / 2.0 ) } );

    auto V = m_camera.matView( 1.f );

    glm::mat4 MV = V * M;
    glm::mat4 MVP = proj * MV;
    glm::mat3 NM = glm::inverseTranspose( glm::mat3( MV ) );

    ///////////////////////////////////////////////////////////////////////////

    // Settings

    GL_CHECK_CALL( glDisable (GL_BLEND) );
    GL_CHECK_CALL( glDisable( GL_MULTISAMPLE ) );
    GL_CHECK_CALL( glEnable( GL_DEPTH_TEST ) );

    GL_CHECK_CALL( glBindFramebuffer(GL_FRAMEBUFFER, gBuffer) );
    GL_CHECK_CALL( glUseProgram( program ) );

    ////////////////////////////////////////////////////////////////////////////

    // Uniforms

    GLuint loc;
    loc = GL_CHECK_CALL_ASSIGN( glGetUniformLocation( program, "colorMode" ) );
    GL_CHECK_CALL( glUniform1i( loc, colorMode ) );

    loc = GL_CHECK_CALL_ASSIGN( glGetUniformLocation( program, "orientationMode" ) );
    GL_CHECK_CALL( glUniform1i( loc, orientationMode ) );

    loc = GL_CHECK_CALL_ASSIGN( glGetUniformLocation( program, "scale" ) );
    GL_CHECK_CALL( glUniform1f( loc, 1.7f ) );

    GL_CHECK_CALL( glActiveTexture( GL_TEXTURE0 ) );
    m_tfTexture.bind();
    loc = GL_CHECK_CALL_ASSIGN( glGetUniformLocation( program, "tf" ) );
    GL_CHECK_CALL( glUniform1i( loc, 0 ) );

    GL_CHECK_CALL( glActiveTexture( GL_TEXTURE1 ) );
    m_glyphTextureTemplate.bind();
    loc = GL_CHECK_CALL_ASSIGN( glGetUniformLocation( program, "textureImage" ) );
    GL_CHECK_CALL( glUniform1i( loc, 1 ) );

    TN::Vec2< float > range( -1.f, 1.f ); 
    loc = GL_CHECK_CALL_ASSIGN( glGetUniformLocation( program, "scalarMappingRange" ) );
    GL_CHECK_CALL( glUniform2fv( loc, 1, (float*) & range ) );

    loc = GL_CHECK_CALL_ASSIGN( glGetUniformLocation( program, "model" ) );
    GL_CHECK_CALL( glUniformMatrix4fv( loc, 1, GL_FALSE, &M[ 0 ][ 0 ] ) );

    loc = GL_CHECK_CALL_ASSIGN( glGetUniformLocation( program, "view" ) );
    GL_CHECK_CALL( glUniformMatrix4fv( loc, 1, GL_FALSE, &V[ 0 ][ 0 ] ) );

    loc = GL_CHECK_CALL_ASSIGN( glGetUniformLocation( program, "modelView" ) );
    GL_CHECK_CALL( glUniformMatrix4fv( loc, 1, GL_FALSE, &MV[ 0 ][ 0 ] ) );

    loc = GL_CHECK_CALL_ASSIGN( glGetUniformLocation( program, "normalMatrix" ) );
    GL_CHECK_CALL( glUniformMatrix3fv( loc, 1, GL_FALSE, &NM[ 0 ][ 0 ] ) );

    loc = GL_CHECK_CALL_ASSIGN( glGetUniformLocation( program, "mvp" ) );
    GL_CHECK_CALL( glUniformMatrix4fv( loc, 1, GL_FALSE, &MVP[ 0 ][ 0 ] ) );  

    loc = GL_CHECK_CALL_ASSIGN( glGetUniformLocation( program, "solidColor" ) );
    GL_CHECK_CALL( glUniform3fv( loc, 1, (float*) & color ) );

    // attributes ///////////////////////////////////////////////////////

    // Base Instance Model

    GL_CHECK_CALL( glBindVertexArray( m_glyphVAO ) ); 

    GL_CHECK_CALL( glEnableVertexAttribArray( 0 ) );
    GL_CHECK_CALL( glBindBuffer( GL_ARRAY_BUFFER, m_glyphPsBuffer ) );
    GL_CHECK_CALL( glVertexAttribPointer(
        0,          // attribute
        3,          // size
        GL_FLOAT,   // type
        GL_FALSE,   // normalized?
        0,          // stride
        (void*)0    // array buffer offset
    ) );
    GL_CHECK_CALL( glVertexAttribDivisor( 0, 0 ) );

    GL_CHECK_CALL( glEnableVertexAttribArray( 1 ) );
    GL_CHECK_CALL( glBindBuffer( GL_ARRAY_BUFFER, m_glyphNmBuffer ) );
    GL_CHECK_CALL( glVertexAttribPointer(
        1,          // attribute
        3,          // size
        GL_FLOAT,   // type
        GL_FALSE,   // normalized?
        0,          // stride
        (void*)0    // array buffer offset
    ) );
    GL_CHECK_CALL( glVertexAttribDivisor( 1, 0 ) );

    if( colorMode == TN::GlyphColorMode::Texture )
    {
	    GL_CHECK_CALL( glEnableVertexAttribArray( 2 ) );
	    GL_CHECK_CALL( glBindBuffer( GL_ARRAY_BUFFER, m_glyphTcBuffer ) );
	    GL_CHECK_CALL( glVertexAttribPointer(
	        2,          // attribute
	        2,          // size
	        GL_FLOAT,   // type
	        GL_FALSE,   // normalized?
	        0,          // stride
	        (void*)0    // array buffer offset
	    ) );
        GL_CHECK_CALL( glVertexAttribDivisor( 2, 0 ) );
    }

    ///////////////////////////////////////////////////////////////////////

    // Per-Instance Attributes

    GL_CHECK_CALL( glEnableVertexAttribArray( 3 ) );
    GL_CHECK_CALL( glBindBuffer( GL_ARRAY_BUFFER, m_glyphOffsetBuffer ) );
    GL_CHECK_CALL( glVertexAttribPointer(
        3,          // attribute
        3,          // size
        GL_FLOAT,   // type
        GL_FALSE,   // normalized?
        0,          // stride
        (void*)0    // array buffer offset
    ) );
    GL_CHECK_CALL( glVertexAttribDivisor( 3, 1 ) );


    GL_CHECK_CALL( glEnableVertexAttribArray( 4 ) );
    GL_CHECK_CALL( glBindBuffer( GL_ARRAY_BUFFER, m_glyphDirection1Buffer ) );
    GL_CHECK_CALL( glVertexAttribPointer(
        4,          // attribute
        3,          // size
        GL_FLOAT,   // type
        GL_FALSE,   // normalized?
        0,          // stride
        (void*)0    // array buffer offset
    ) );
    GL_CHECK_CALL( glVertexAttribDivisor( 4, 1 ) );

    GL_CHECK_CALL( glEnableVertexAttribArray( 5 ) );
    GL_CHECK_CALL( glBindBuffer( GL_ARRAY_BUFFER, m_glyphDirection2Buffer ) );
    GL_CHECK_CALL( glVertexAttribPointer(
        5,          // attribute
        3,          // size
        GL_FLOAT,   // type
        GL_FALSE,   // normalized?
        0,          // stride
        (void*)0    // array buffer offset
    ) );
    GL_CHECK_CALL( glVertexAttribDivisor( 5, 1 ) );

    GL_CHECK_CALL( glEnableVertexAttribArray( 6 ) );
    GL_CHECK_CALL( glBindBuffer( GL_ARRAY_BUFFER, m_glyphNormalBuffer ) );
    GL_CHECK_CALL( glVertexAttribPointer(
        6,          // attribute
        3,          // size
        GL_FLOAT,   // type
        GL_FALSE,   // normalized?
        0,          // stride
        (void*)0    // array buffer offset
    ) );
    GL_CHECK_CALL( glVertexAttribDivisor( 6, 1 ) );

    if( colorMode == TN::GlyphColorMode::Scalar )
    {
	    GL_CHECK_CALL( glEnableVertexAttribArray( 7 ) );
	    GL_CHECK_CALL( glBindBuffer( GL_ARRAY_BUFFER, m_glyphScalarBuffer ) );
	    GL_CHECK_CALL( glVertexAttribPointer(
	        7,          // attribute
	        1,          // size
	        GL_FLOAT,   // type
	        GL_FALSE,   // normalized?
	        0,          // stride
	        (void*)0    // array buffer offset
	    ) );
	    GL_CHECK_CALL( glVertexAttribDivisor( 7, 1 ) );
	}

    // render //////////////////////////////////////////////////////////////////////

    GL_CHECK_CALL( glDrawArraysInstanced( GL_TRIANGLES, 0, NSPHERE_VERTS, positions.size() / 3 ) );
}

}