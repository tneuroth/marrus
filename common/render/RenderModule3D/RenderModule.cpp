
#include "RenderModule.hpp"
#include "image/ImageLoader.hpp"

#include "render/GLErrorChecking.hpp"

#include <random>
#include <iostream>
#include <string>
#include <fstream>
#include <stdexcept>

#include <glm/gtc/matrix_inverse.hpp>

namespace TN {

RenderModule::RenderModule( 
    const TN::Vec2< int > & pos, 
    const TN::Vec2< int > & size, 
    const std::string & _baseDir ) 
{
    initializedGL = false;

    m_activeViewId = "default";

    m_numViewTextures = 0;
    m_baseDirectory = _baseDir;

    m_glyphPath    = m_baseDirectory + "/models/domino_t.obj";
    m_glyphTexPath = m_baseDirectory + "/models/domino_texture.png";
    
    Widget::setSize(   size.x(), size.y() );
    setPosition( pos.x(), pos.y() );

    frameBuffersReady = false;

    m_tubeRadius = 1.f;

    for( int i = 0; i < MAX_VIEW_TEXTURES; ++i ) 
    {
        m_viewTextures[ i ] = 0;
    }

    m_occlusionNoiseScale = 4.f; 
    m_quadVAO = 0;
    m_quadVBO = 0;

    m_vao = 0;
    m_vbo = 0;
    m_nbo = 0;
    m_ibo = 0;

    m_glyphVAO = 0;

    m_glyphPsBuffer = 0;
    m_glyphNmBuffer = 0;
    m_glyphTcBuffer = 0;

    m_glyphOffsetBuffer = 0;
    m_glyphScalarBuffer = 0;
    m_glyphFiltersBuffer = 0;

    m_glyphNormalBuffer = 0;
    m_glyphDirection1Buffer = 0;
    m_glyphDirection2Buffer = 0;

    m_frameBufferToViewTexture = 0;

    /////////////////////////////////

    m_glyphTexBuffer = 0;
    m_perInstanceGlyphBufferSize = 0;
    m_glyphBufferTex   = 0;
    m_nGlyhphInstances = 0;

    ///////////////////////////////

    gBuffer = 0;
    ssaoFBO = 0;
    ssaoBlurFBO = 0;

    gPosition = 0;
    gNormal = 0;
    gAlbedo = 0;

    rboDepth = 0;

    ssaoColorBuffer = 0;
    ssaoColorBufferBlur = 0;
    noiseTexture = 0;

    initialize();
}

void RenderModule::createFrameBuffers( int w, int h )
{
    GL_CHECK_CALL( glGenFramebuffers(1, &gBuffer) );
    GL_CHECK_CALL( glGenFramebuffers(1, & m_frameBufferToViewTexture ) );

    GL_CHECK_CALL( glGenTextures(1, &gPosition) );
    GL_CHECK_CALL( glGenTextures(1, &gNormal) );
    GL_CHECK_CALL( glGenTextures(1, &gAlbedo) );

    GL_CHECK_CALL( glGenFramebuffers(1, &ssaoFBO) );
    GL_CHECK_CALL( glGenFramebuffers(1, &ssaoBlurFBO) );

    GL_CHECK_CALL( glGenTextures(1, &ssaoColorBuffer) );
    GL_CHECK_CALL( glGenTextures(1, &ssaoColorBufferBlur) );
    GL_CHECK_CALL( glGenTextures(1, &noiseTexture) );

    GL_CHECK_CALL( glBindFramebuffer( GL_FRAMEBUFFER, gBuffer ) );

    GL_CHECK_CALL( glBindTexture(GL_TEXTURE_2D, gPosition) );
    GL_CHECK_CALL( glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB16F, w, h, 0, GL_RGB, GL_FLOAT, NULL) );
    GL_CHECK_CALL( glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST) );
    GL_CHECK_CALL( glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST) );
    GL_CHECK_CALL( glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE) );
    GL_CHECK_CALL( glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE) );
    GL_CHECK_CALL( glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, gPosition, 0) );

    // normal color buffer
    GL_CHECK_CALL( glBindTexture(GL_TEXTURE_2D, gNormal) );
    GL_CHECK_CALL( glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB16F, w, h, 0, GL_RGB, GL_FLOAT, NULL) );
    GL_CHECK_CALL( glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST) );
    GL_CHECK_CALL( glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST) );
    GL_CHECK_CALL( glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, GL_TEXTURE_2D, gNormal, 0) );

    // color + specular color buffer
    GL_CHECK_CALL( glBindTexture(GL_TEXTURE_2D, gAlbedo) );
    GL_CHECK_CALL( glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, w, h, 0, GL_RGB, GL_UNSIGNED_BYTE, NULL) );
    GL_CHECK_CALL( glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST) );
    GL_CHECK_CALL( glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST) );
    GL_CHECK_CALL( glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT2, GL_TEXTURE_2D, gAlbedo, 0) );

    // tell OpenGL which color attachments we'll use (of this framebuffer) for rendering
    unsigned int attachments[3] = { GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1, GL_COLOR_ATTACHMENT2 };
    GL_CHECK_CALL( glDrawBuffers(3, attachments) );

    // create and attach depth buffer (renderbuffer)
    GL_CHECK_CALL( glGenRenderbuffers(1, &rboDepth) );
    GL_CHECK_CALL( glBindRenderbuffer(GL_RENDERBUFFER, rboDepth) );
    GL_CHECK_CALL( glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, w, h ) );
    GL_CHECK_CALL( glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, rboDepth) );

    checkError( "depth buffer" );

    if ( GL_CHECK_CALL_ASSIGN( glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE ) )
    {
        std::cout << "Framebuffer not complete!" << std::endl;
    }

    //////////

    GL_CHECK_CALL( glBindFramebuffer(GL_FRAMEBUFFER, ssaoFBO) );

    // SSAO color buffer
    GL_CHECK_CALL( glBindTexture(GL_TEXTURE_2D, ssaoColorBuffer) );
    GL_CHECK_CALL( glTexImage2D(GL_TEXTURE_2D, 0, GL_RED, w, h, 0, GL_RGB, GL_FLOAT, NULL) );
    GL_CHECK_CALL( glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST) );
    GL_CHECK_CALL( glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST) );
    GL_CHECK_CALL( glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, ssaoColorBuffer, 0) );

    if ( GL_CHECK_CALL_ASSIGN( glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE ) )
    {
        std::cout << "SSAO Framebuffer not complete!" << std::endl;
    }

    

    // and blur stage
    GL_CHECK_CALL( glBindFramebuffer(GL_FRAMEBUFFER, ssaoBlurFBO) );
    GL_CHECK_CALL( glBindTexture(GL_TEXTURE_2D, ssaoColorBufferBlur) );
    GL_CHECK_CALL( glTexImage2D(GL_TEXTURE_2D, 0, GL_RED, w, h, 0, GL_RGB, GL_FLOAT, NULL) );
    GL_CHECK_CALL( glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST) );
    GL_CHECK_CALL( glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST) );
    GL_CHECK_CALL( glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, ssaoColorBufferBlur, 0) );

    if ( GL_CHECK_CALL_ASSIGN( glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE ) )
    {
        std::cout << "SSAO Blur Framebuffer not complete!" << std::endl;
    }
    

    GL_CHECK_CALL( glBindFramebuffer(GL_FRAMEBUFFER, 0) );

    // generate noise texture
    // ----------------------

    std::uniform_real_distribution<GLfloat> randomFloats(0.0, 1.0); // generates random floats between 0.0 and 1.0
    std::default_random_engine generator;
    ssaoKernel.clear();

    for (unsigned int i = 0; i < aoKernelSize; ++i)
    {
        TN::Vec3<float> sample( randomFloats( generator ) * 2.0 - 1.0, randomFloats(generator) * 2.0 - 1.0, randomFloats(generator));
        sample.normalize();
        sample = sample * randomFloats( generator );
        float scale = float(i) / aoKernelSize;

        //scale samples s.t. they're more aligned to center of kernel
        scale = .1 + ( scale * scale ) * ( 1.0 - .1f ); //lerp( 0.1f, 1.0f, scale * scale );
        sample = sample * scale;
        ssaoKernel.push_back( glm::vec3( sample.x(), sample.y(), sample.z() ) );
    }

    for (unsigned int i = 0; i < 16; i++)
    {
        TN::Vec3< float > noise(randomFloats(generator) * 2.0 - 1.0, randomFloats(generator) * 2.0 - 1.0, 0.0f); // rotate around z-axis (in tangent space)
        ssaoNoise.push_back(noise);
    }

    GL_CHECK_CALL( glBindTexture(GL_TEXTURE_2D, noiseTexture ) );
    GL_CHECK_CALL( glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB32F, 4, 4, 0, GL_RGB, GL_FLOAT, &ssaoNoise[0]) );
    GL_CHECK_CALL( glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST) );
    GL_CHECK_CALL( glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST) );
    GL_CHECK_CALL( glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT) );
    GL_CHECK_CALL( glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT) );

    GL_CHECK_CALL( glBindFramebuffer( GL_FRAMEBUFFER, 0 ) );

    GL_CHECK_CALL( glGenTextures( 1, & m_glyphBufferTex ) );

    frameBuffersReady = true;    

    
}

void RenderModule::deleteFrameBuffers()
{
    GL_CHECK_CALL( glDeleteFramebuffers( 1, &gBuffer ) );
    GL_CHECK_CALL( glDeleteFramebuffers( 1, &ssaoFBO ) );
    GL_CHECK_CALL( glDeleteFramebuffers( 1, &ssaoBlurFBO ) );

    GL_CHECK_CALL( glDeleteTextures( 1, &gPosition ) );
    GL_CHECK_CALL( glDeleteTextures( 1, &gNormal ) );
    GL_CHECK_CALL( glDeleteTextures( 1, &gAlbedo ) );

    GL_CHECK_CALL( glDeleteRenderbuffers( 1, &rboDepth ) );

    GL_CHECK_CALL( glDeleteTextures(1, &ssaoColorBuffer ) );
    GL_CHECK_CALL( glDeleteTextures(1, &ssaoColorBufferBlur ) );
    GL_CHECK_CALL( glDeleteTextures(1, &noiseTexture ) );

    m_frameBufferToViewTexture = 0;

    gBuffer = 0;
    ssaoFBO = 0;
    ssaoBlurFBO = 0;

    gPosition = 0;
    gNormal = 0;
    gAlbedo = 0;

    rboDepth = 0;

    ssaoColorBuffer = 0;
    ssaoColorBufferBlur = 0;
    noiseTexture = 0;

    frameBuffersReady = false;

    
}

RenderModule::~RenderModule() 
{
    GL_CHECK_CALL( glDeleteVertexArrays( 1, & m_vao ) );
    GL_CHECK_CALL( glDeleteBuffers(      1, & m_vbo ) );
    GL_CHECK_CALL( glDeleteBuffers(      1, & m_nbo ) );
    GL_CHECK_CALL( glDeleteBuffers(      1, & m_cbo ) );        
    GL_CHECK_CALL( glDeleteBuffers(      1, & m_ibo ) );

    GL_CHECK_CALL( glDeleteBuffers(  1, & m_glyphTexBuffer ) );
    GL_CHECK_CALL( glDeleteTextures( 1, & m_glyphBufferTex ) );

    GL_CHECK_CALL( glDeleteBuffers(      1, & m_glyphNmBuffer ) );        
    GL_CHECK_CALL( glDeleteBuffers(      1, & m_glyphPsBuffer ) );
    GL_CHECK_CALL( glDeleteBuffers(      1, & m_glyphTcBuffer ) );

    GL_CHECK_CALL( glDeleteBuffers(      1, & m_glyphOffsetBuffer ) );

    GL_CHECK_CALL( glDeleteBuffers(      1, & m_glyphNormalBuffer ) );
    GL_CHECK_CALL( glDeleteBuffers(      1, & m_glyphDirection1Buffer ) );
    GL_CHECK_CALL( glDeleteBuffers(      1, & m_glyphDirection2Buffer ) );

    // TODO: check these and reorganize more clearly
    GL_CHECK_CALL( glDeleteBuffers(      1, & m_glyphScalarBuffer ) );
    GL_CHECK_CALL( glDeleteBuffers(      1, & m_glyphFiltersBuffer ) );

    GL_CHECK_CALL( glDeleteVertexArrays( 1, & m_quadVAO ) );
    GL_CHECK_CALL( glDeleteBuffers(      1, & m_quadVBO ) );

    deleteFrameBuffers();

    // view texturres and view framebuffer

    for( int i = 0; i < m_numViewTextures; ++i )
    {
        GL_CHECK_CALL( glDeleteTextures( 1, &m_viewTextures[ i ] ) );
    }

    m_numViewTextures = 0;
    GL_CHECK_CALL( glDeleteFramebuffers( 1, & m_frameBufferToViewTexture ) );

    GL_CHECK_CALL( glDeleteProgram( m_aoShaderProgram         ) );
    GL_CHECK_CALL( glDeleteProgram( m_aoBlurProgram           ) );
    GL_CHECK_CALL( glDeleteProgram( m_aoGeometryProgram       ) );
    GL_CHECK_CALL( glDeleteProgram( m_aoGeometryPrimProg      ) );
    GL_CHECK_CALL( glDeleteProgram( m_aoGeometryProgramBF     ) );
    GL_CHECK_CALL( glDeleteProgram( m_aoLightingProgram       ) );        
    GL_CHECK_CALL( glDeleteProgram( m_glyph3DProgram          ) );   
    GL_CHECK_CALL( glDeleteProgram( m_glyph3DProgramG         ) );   
    GL_CHECK_CALL( glDeleteProgram( m_glyph3DProgramBuff      ) );   
    GL_CHECK_CALL( glDeleteProgram( m_radialGlyphProgram      ) );   
    GL_CHECK_CALL( glDeleteProgram( m_directionalGlyphProgram ) );   
    GL_CHECK_CALL( glDeleteProgram( m_starGlyphProgram        ) );   
    GL_CHECK_CALL( glDeleteProgram( m_texProgram              ) );
}


void RenderModule::checkError( const std::string & prefix)
{
    GLenum glErr = glGetError();

    while(glErr != GL_NO_ERROR) {
        std::string error;
        switch (glErr)
        {
        case GL_NO_ERROR:               error="NO_ERROR";               break;
        case GL_INVALID_OPERATION:      error="INVALID_OPERATION";      break;
        case GL_INVALID_ENUM:           error="INVALID_ENUM";           break;
        case GL_INVALID_VALUE:          error="INVALID_VALUE";          break;
        case GL_OUT_OF_MEMORY:          error="OUT_OF_MEMORY";          break;
        case GL_INVALID_FRAMEBUFFER_OPERATION:  error="INVALID_FRAMEBUFFER_OPERATION";  break;
        }

        if(error != "NO_ERROR") {
            std::cerr << prefix << ":" << error << std::endl;
        }
        glErr = glGetError();
    }
}

void RenderModule::resetCamera()
{
    m_camera.reset( { -0.5, -0.5, -0.5 }, { 0.5, 0.5, 0.5 } );
    //m_camera.reset(  { -64.1251f, -72.0172f, -64.1251f }, { 66.7518f, 87.3524f, 66.7518f  }  );
}

void RenderModule::compileShaders()
{
    loadShaders( 
        m_baseDirectory + "/shaders/",
        "mesh.vert", 
        "SSAO/ssao_geometry1.fs", 
        "",
         m_aoGeometryProgram );

    loadShaders( 
        m_baseDirectory + "/shaders/",
        "primitives3D.vert", 
        "SSAO/ssao_geom_prim.fs", 
        "",
         m_aoGeometryPrimProg );

    loadShaders( 
        m_baseDirectory + "/shaders/",
        "meshBF.vert", 
        "SSAO/ssao_geometryBF.fs", 
        "",
         m_aoGeometryProgramBF );    

    loadShaders( 
        m_baseDirectory + "/shaders/",
        "SSAO/ssao.vs", 
        "SSAO/ssao_lighting.fs",
        "",
         m_aoLightingProgram );

    loadShaders( 
        m_baseDirectory + "/shaders/",
        "SSAO/ssao.vs", 
        "SSAO/ssao.fs",
        "",
        m_aoShaderProgram );

    loadShaders( 
        m_baseDirectory + "/shaders/",
        "SSAO/ssao.vs", 
        "SSAO/ssao_blur.fs",
        "",
        m_aoBlurProgram );

    loadShaders( 
        m_baseDirectory + "/shaders/",
        "Glyph3D/glyph3D.vert", 
        "Glyph3D/glyph3D.fs",
        "",
        m_glyph3DProgram );

    loadShaders( 
        m_baseDirectory + "/shaders/",
        "Glyph3D/glyph3Dg.vert", 
        "Glyph3D/glyph3Dg.fs",
        "Glyph3D/glyph3D.geom",
        m_glyph3DProgramG );

    loadShaders( 
        m_baseDirectory + "/shaders/",
        "Glyph3D/glyph3DTexBuff.vert", 
        "Glyph3D/glyph3DTexBuff.fs",
        "",
        m_glyph3DProgramBuff );

    loadShaders( 
        m_baseDirectory + "/shaders/",
        "Glyph3D/radialGlyph.vert", 
        "Glyph3D/radialGlyph.fs",
        "",
        m_radialGlyphProgram );

    loadShaders( 
        m_baseDirectory + "/shaders/",
        "Glyph3D/directionalGlyph.vert", 
        "Glyph3D/directionalGlyph.fs",
        "",
        m_directionalGlyphProgram );

    loadShaders( 
        m_baseDirectory + "/shaders/",
        "Glyph3D/radialGlyph.vert", 
        "Glyph3D/starGlyph.fs",
        "",
        m_starGlyphProgram );

    loadShaders( 
        m_baseDirectory + "/shaders/",
        "simpleTex.vert", 
        "textureRGB.frag",
        "",
        m_texProgram );
}

bool RenderModule::objIstextured( const std::string & path )
{
    std::ifstream inFile( path );
    std::string line;
    while( std::getline( inFile, line ) )
    {
        std::stringstream ss( line );
        std::string c;
        ss >> c;

        if( c == "vt" )
        {
            return true;
        }
    }
    return false;
}

void RenderModule::loadObj( 
        std::vector< TN::Vec3< float > > & vertsOut,
        std::vector< TN::Vec3< float > > & normsOut,
        std::vector< TN::Vec2< float > > & uvOut,      
        std::vector< int > & objOffsetsOut,
        const std::string & path )
{
    vertsOut.clear();
    normsOut.clear();
    uvOut.clear();
    objOffsetsOut.clear();

    std::ifstream inFile( path );
    std::string line;

    std::vector< TN::Vec3< float > > verts;
    std::vector< TN::Vec3< float > > norms;
    std::vector< TN::Vec2< float > > uv;

    std::vector< std::array< int, 9 > > faces;

    while( std::getline( inFile, line ) )
    {
        std::stringstream ss( line );
        std::string c;
        ss >> c;

        if( c == "o" )
        {
            objOffsetsOut.push_back( faces.size()*3 );
        }

        if( c == "v" )
        {
            float x, y, z;
            ss >> x >> y >> z;
            verts.push_back( { x, y, z } );
        }
        else if( c == "vn" )
        {
            float x, y, z;
            ss >> x >> y >> z;
            norms.push_back( { x, y, z } );
        }
        else if( c == "f" )
        {
            std::array< int, 9 > f;
            char c;

            ss >> f[0] >> c >> f[1] >> c >> f[2]
               >> f[3] >> c >> f[4] >> c >> f[5]
               >> f[6] >> c >> f[7] >> c >> f[8];

            faces.push_back( f );
        }
        else if( c == "vt"  )
        {
            float x, y;
            ss >> x >> y;
            uv.push_back( { x, y } );
        }
    }

    for( int i = 0; i < faces.size(); ++i )
    {
        auto & f = faces[ i ];

        objOffsetsOut.push_back( f[ 0 ] - 1 );

        vertsOut.push_back( verts[ f[ 0 ] - 1 ] );
        uvOut.push_back(    uv[    f[ 1 ] - 1 ] );
        normsOut.push_back( norms[ f[ 2 ] - 1 ] );

        vertsOut.push_back( verts[ f[ 3 ] - 1 ] );
        uvOut.push_back(    uv[    f[ 4 ] - 1 ] );
        normsOut.push_back( norms[ f[ 5 ] - 1 ] );

        vertsOut.push_back( verts[ f[ 6 ] - 1 ] );
        uvOut.push_back(    uv[    f[ 7 ] - 1 ] );
        normsOut.push_back( norms[ f[ 8 ] - 1 ] );
    }
}


void RenderModule::loadObj( 
        std::vector< TN::Vec3< float > > & vertsOut,
        std::vector< TN::Vec3< float > > & normsOut,
        std::vector< int > & objOffsetsOut,
        const std::string & path )
{
    vertsOut.clear();
    normsOut.clear();
    objOffsetsOut.clear();

    std::ifstream inFile( path );
    std::string line;

    std::vector< TN::Vec3< float > > verts;
    std::vector< TN::Vec3< float > > norms;

    std::vector< std::array< int, 6 > > faces;

    while( std::getline( inFile, line ) )
    {
        std::stringstream ss( line );
        std::string c;
        ss >> c;

        if( c == "o" )
        {
            objOffsetsOut.push_back( faces.size()*3 );
        }
        else if( c == "v" )
        {
            float x, y, z;
            ss >> x >> y >> z;
            verts.push_back( { x, y, z } );
        }
        else if( c == "vn" )
        {
            float x, y, z;
            ss >> x >> y >> z;
            norms.push_back( { x, y, z } );
        }
        else if( c == "f" )
        {
            std::array< int, 6 > f;
            char c;

            ss >> f[0] >> c >> c >> f[1]
               >> f[2] >> c >> c >> f[3]
               >> f[4] >> c >> c >> f[5];
            faces.push_back( f );
        }
    }

    for( int i = 0; i < faces.size(); ++i )
    {
        auto & f = faces[ i ];

        vertsOut.push_back( verts[ f[ 0 ] - 1 ] );
        normsOut.push_back( norms[ f[ 1 ] - 1 ] );

        vertsOut.push_back( verts[ f[ 2 ] - 1 ] );
        normsOut.push_back( norms[ f[ 3 ] - 1 ] );

        vertsOut.push_back( verts[ f[ 4 ] - 1 ] );
        normsOut.push_back( norms[ f[ 5 ] - 1 ] );
    }
}

void RenderModule::initialize()
{
    resetCamera();

    GL_CHECK_CALL( glDisable (GL_BLEND) );
    GL_CHECK_CALL( glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA) );

    GL_CHECK_CALL( glDisable(GL_CULL_FACE) );
    GL_CHECK_CALL( glEnable(GL_DEPTH_TEST) );

    // GL_CHECK_CALL( glHint( GL_LINE_SMOOTH_HINT, GL_NICEST ) );
    // GL_CHECK_CALL( glHint( GL_POLYGON_SMOOTH_HINT, GL_NICEST ) );
    // GL_CHECK_CALL( glEnable( GL_MULTISAMPLE ) );

    compileShaders();
    checkError( "load shaders" );

    m_tfTexture.create();
    m_glyphTFTexture.create();
    m_tfTextureForLayers.create();

    loadTF( m_baseDirectory + "/TF/diverging/Spectral.txt", m_tfTexture );
    loadTF( m_baseDirectory + "/TF/diverging/Spectral.txt", m_tfTextureForLayers );     

    GL_CHECK_CALL( glGenVertexArrays( 1, & m_vao ) );
    GL_CHECK_CALL( glBindVertexArray( m_vao ) );
    GL_CHECK_CALL( glGenBuffers(      1, & m_vbo ) );
    GL_CHECK_CALL( glGenBuffers(      1, & m_nbo ) );
    GL_CHECK_CALL( glGenBuffers(      1, & m_cbo ) );          
    GL_CHECK_CALL( glGenBuffers(      1, & m_ibo ) ); 

    GL_CHECK_CALL( glGenVertexArrays( 1, & m_glyphVAO ) );    
    GL_CHECK_CALL( glBindVertexArray( m_glyphVAO ) );    

    GL_CHECK_CALL( glGenBuffers(      1, & m_glyphPsBuffer ) );        
    GL_CHECK_CALL( glGenBuffers(      1, & m_glyphNmBuffer ) );
    GL_CHECK_CALL( glGenBuffers(      1, & m_glyphTcBuffer ) );

    GL_CHECK_CALL( glGenBuffers(      1, & m_glyphOffsetBuffer ) );

    GL_CHECK_CALL( glGenBuffers(      1, & m_glyphNormalBuffer ) );
    GL_CHECK_CALL( glGenBuffers(      1, & m_glyphDirection1Buffer ) );
    GL_CHECK_CALL( glGenBuffers(      1, & m_glyphDirection2Buffer ) );

    GL_CHECK_CALL( glGenBuffers(      1, & m_glyphScalarBuffer ) );
    GL_CHECK_CALL( glGenBuffers(      1, & m_glyphFiltersBuffer ) );

    m_glyphTextureTemplate.create();

    loadGlyph( m_glyphPath, m_glyphTexPath );

    m_material.Ka = glm::vec3( 1, 1, 1 );
    m_material.Kd = glm::vec3( 1, 1, 1 );
    m_material.Ks = glm::vec3( 1, 1, 1 );

    m_material.Ka_scale = 0.7;
    m_material.Kd_scale = 1.0;
    m_material.Ks_scale = 1.0;

    m_lightSource.Ia = glm::vec3( 0.5f, 0.5f, 0.5f );
    m_lightSource.Id = glm::vec3( 0.5f, 0.5f, 0.5f );
    m_lightSource.Is = glm::vec3( 0.5f, 0.5f, 0.5f );

    m_lightSource.Ia_scale = 1.0;
    m_lightSource.Id_scale = 1.0;
    m_lightSource.Is_scale = 1.0;

    m_lightSource.direction = glm::vec3(.1f, 0.5f, 0.3f );
    m_lightSource.type = 0;
    m_lightSource.position = glm::vec3( 0, 10, 100 );

    initRenderMode( 1, false );

    initializedGL = true;
}

void RenderModule::loadShaders(
    const std::string & path, 
    const std::string & vert, 
    const std::string & frag, 
    const std::string & geo, 
    GLuint & shader )
{
    shader = glCreateProgram();

    // vertex shader ///////////

    std::ifstream tv( path + vert );
    std::string vertexShaderStr( ( std::istreambuf_iterator<char>( tv ) ),
                     std::istreambuf_iterator<char>( ) );
    const char * vertexShaderSrc   = vertexShaderStr.c_str();

    int vertexShader = glCreateShader( GL_VERTEX_SHADER );
    GL_CHECK_CALL( glShaderSource( vertexShader, 1, & vertexShaderSrc, NULL ) );
    GL_CHECK_CALL( glCompileShader(vertexShader) );

    // check for shader compile errors
    int success;
    char infoLog[512];
    GL_CHECK_CALL( glGetShaderiv( vertexShader, GL_COMPILE_STATUS, &success ) );
    if (!success)
    {
        GL_CHECK_CALL( glGetShaderInfoLog( vertexShader, 512, NULL, infoLog ) );
        std::cout << "ERROR::SHADER::VERTEX::COMPILATION_FAILED\n" << infoLog << std::endl;
    }

    GL_CHECK_CALL( glAttachShader( shader, vertexShader ) );

    // fragment shader //////////////

    std::ifstream tf( path + frag );
    std::string fragmentShaderStr( ( std::istreambuf_iterator<char>( tf ) ),
                     std::istreambuf_iterator<char>( ) );
    const char * fragmentShaderSrc = fragmentShaderStr.c_str();

    int fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
    GL_CHECK_CALL( glShaderSource(fragmentShader, 1, & fragmentShaderSrc, NULL) );
    GL_CHECK_CALL( glCompileShader(fragmentShader) );

    // check for shader compile errors
    GL_CHECK_CALL( glGetShaderiv(fragmentShader, GL_COMPILE_STATUS, &success) );
    if (!success)
    {
        GL_CHECK_CALL( glGetShaderInfoLog(fragmentShader, 512, NULL, infoLog) );
        std::cout << "ERROR::SHADER::FRAGMENT::COMPILATION_FAILED\n" << infoLog << std::endl;
    }

    GL_CHECK_CALL( glAttachShader( shader, fragmentShader ) );

    // geometry shader ///////////////////////////

    int geoShader;
    if( geo != "" )
    {   
        std::ifstream tg( path + geo );
        std::string geoShaderStr( ( std::istreambuf_iterator<char>( tg ) ),
                         std::istreambuf_iterator<char>( ) );
        const char * geoShaderSrc = geoShaderStr.c_str();

        geoShader = glCreateShader(GL_GEOMETRY_SHADER);
        GL_CHECK_CALL( glShaderSource( geoShader, 1, & geoShaderSrc, NULL) );
        GL_CHECK_CALL( glCompileShader( geoShader ) );

        // check for shader compile errors
        GL_CHECK_CALL( glGetShaderiv( geoShader, GL_COMPILE_STATUS, &success) );
        if (!success)
        {
            GL_CHECK_CALL( glGetShaderInfoLog(geoShader, 512, NULL, infoLog) );
            std::cout << "ERROR::SHADER::GEOMETRY::COMPILATION_FAILED\n" << infoLog << std::endl;
        }

        GL_CHECK_CALL( glAttachShader( shader, geoShader ) );    
    }

    //////////////////////////////////////////////////////

    GL_CHECK_CALL( glLinkProgram( shader) );

    GL_CHECK_CALL( glGetProgramiv( shader, GL_LINK_STATUS, &success) );
    if (!success) 
    {
        GL_CHECK_CALL( glGetProgramInfoLog( shader, 512, NULL, infoLog) );
        std::cout << "ERROR::SHADER::PROGRAM::LINKING_FAILED\n" << infoLog << std::endl;
    }

    GL_CHECK_CALL( glDeleteShader( vertexShader   ) );
    GL_CHECK_CALL( glDeleteShader( fragmentShader ) );

    if( geo != "" )
    {
        GL_CHECK_CALL( glDeleteShader( geoShader ) );
    }
}

void RenderModule::renderQuad()
{
    if ( GL_CHECK_CALL_ASSIGN( glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE ) )
    {
        std::cout << "Framebuffer not complete rendering quad!" << std::endl;
    }

    if ( m_quadVAO == 0 )
    {
        float quadVertices[] = {
            // positions        // texture Coords
            -1.0f,  1.0f, 0.0f, 0.0f, 1.0f,
            -1.0f, -1.0f, 0.0f, 0.0f, 0.0f,
             1.0f,  1.0f, 0.0f, 1.0f, 1.0f,
             1.0f, -1.0f, 0.0f, 1.0f, 0.0f,
        };
        // setup plane VAO
        GL_CHECK_CALL( glGenVertexArrays(1, & m_quadVAO) );
        GL_CHECK_CALL( glGenBuffers(1, & m_quadVBO) );
        GL_CHECK_CALL( glBindVertexArray( m_quadVAO) );
        GL_CHECK_CALL( glBindBuffer(GL_ARRAY_BUFFER, m_quadVBO) );
        GL_CHECK_CALL( glBufferData(GL_ARRAY_BUFFER, sizeof(quadVertices), &quadVertices, GL_STATIC_DRAW) );
        GL_CHECK_CALL( glEnableVertexAttribArray(0) );
        GL_CHECK_CALL( glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)0) );
        GL_CHECK_CALL( glEnableVertexAttribArray(1) );
        GL_CHECK_CALL(glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)(3 * sizeof(float))) );
    }
    

    GL_CHECK_CALL( glBindVertexArray(m_quadVAO) );

    GL_CHECK_CALL( glDrawArrays(GL_TRIANGLE_STRIP, 0, 4) );

    GL_CHECK_CALL( glBindVertexArray( 0 ) );
}

void RenderModule::renderGeometry(
    const std::vector< TN::Vec3< float > > & verts,
    const std::vector< TN::Vec3< float > > & norms,
    const std::vector< int > & bitFlags,
    const TN::Vec3< float > & color1,
    const TN::Vec3< float > & color2,
    const std::pair< TN::Vec3< float >, TN::Vec3< float > > & bbox )
{
    //////////////// buffers //////////////////////////////////

    GL_CHECK_CALL( glBindFramebuffer(GL_FRAMEBUFFER, gBuffer) );
    GL_CHECK_CALL( glViewport( 0, 0, width() * m_multiSample, height() * m_multiSample ) );

    GL_CHECK_CALL( glBindVertexArray( m_vao ) ); 

    GL_CHECK_CALL( glBindBuffer( GL_ARRAY_BUFFER, m_vbo ) );
    GL_CHECK_CALL( glBufferData( GL_ARRAY_BUFFER, verts.size() * 3 * sizeof( float ), verts.data(), GL_DYNAMIC_DRAW ) );

    GL_CHECK_CALL( glBindBuffer( GL_ARRAY_BUFFER, m_nbo ) );
    GL_CHECK_CALL( glBufferData( GL_ARRAY_BUFFER, norms.size() * 3 * sizeof( float ), norms.data(), GL_DYNAMIC_DRAW ) );

    GL_CHECK_CALL( glBindBuffer( GL_ARRAY_BUFFER, m_ibo ) );
    GL_CHECK_CALL( glBufferData( GL_ARRAY_BUFFER, bitFlags.size() * sizeof( int ), bitFlags.data(), GL_DYNAMIC_DRAW ) );

    GLuint loc; 

    glm::mat4 proj = m_camera.matProj();
    glm::mat4 M( 1.f );

    double xW = ( bbox.second.x() - bbox.first.x() );
    double yW = ( bbox.second.y() - bbox.first.y() );    
    double zW = ( bbox.second.z() - bbox.first.z() );

    float maxWidth = std::max( std::max( xW, yW ), zW );
    M = glm::scale( M, { 1.0 / maxWidth, 1.0 / maxWidth, 1.0 / maxWidth  } );

    M = glm::translate( M, {
        -( bbox.first.x() + xW / 2.0 ),
        -( bbox.first.y() + yW / 2.0 ),    
        -( bbox.first.z() + zW / 2.0 ) } );

    glm::mat4 MV = m_camera.matView( 1.f ) * M;
    glm::mat4 MVP = proj * MV;
    glm::mat3 NM = glm::inverseTranspose( glm::mat3( MV ) );

    GL_CHECK_CALL( glDisable (GL_BLEND) );
    GL_CHECK_CALL( glDisable( GL_MULTISAMPLE ) );

    GL_CHECK_CALL( glBindFramebuffer(GL_FRAMEBUFFER, gBuffer) );
    GL_CHECK_CALL( glUseProgram( m_aoGeometryProgramBF ) );

    // normals

    loc = GL_CHECK_CALL_ASSIGN( glGetUniformLocation( m_aoGeometryProgramBF, "modelView" ) );
    GL_CHECK_CALL( glUniformMatrix4fv( loc, 1, GL_FALSE, &MV[ 0 ][ 0 ] ) );

    loc = GL_CHECK_CALL_ASSIGN( glGetUniformLocation( m_aoGeometryProgramBF, "normalMatrix" ) );
    GL_CHECK_CALL( glUniformMatrix3fv( loc, 1, GL_FALSE, &NM[ 0 ][ 0 ] ) );

    loc = GL_CHECK_CALL_ASSIGN( glGetUniformLocation( m_aoGeometryProgramBF, "mvp" ) );
    GL_CHECK_CALL( glUniformMatrix4fv( loc, 1, GL_FALSE, &MVP[ 0 ][ 0 ] ) );  

    loc = GL_CHECK_CALL_ASSIGN( glGetUniformLocation( m_aoGeometryProgramBF, "cl1" ) );
    GL_CHECK_CALL( glUniform3fv( loc, 1, (float*) & color1 ) );

    loc = GL_CHECK_CALL_ASSIGN( glGetUniformLocation( m_aoGeometryProgramBF, "cl2" ) );
    GL_CHECK_CALL( glUniform3fv( loc, 1, (float*) & color2 ) );

    // attributes

    GL_CHECK_CALL( glBindVertexArray( m_vao ) ); 

    GL_CHECK_CALL( glBindBuffer( GL_ARRAY_BUFFER, m_vbo ) );
    GL_CHECK_CALL( glEnableVertexAttribArray( 0 ) );
    GL_CHECK_CALL( glVertexAttribPointer(
        0,          // attribute
        3,          // size
        GL_FLOAT,   // type
        GL_FALSE,   // normalized?
        0,          // stride
        (void*)0    // array buffer offset
    ) );

    GL_CHECK_CALL( glBindBuffer( GL_ARRAY_BUFFER, m_nbo ) );
    GL_CHECK_CALL( glEnableVertexAttribArray( 1 ) );
    GL_CHECK_CALL( glVertexAttribPointer(
        1,          // attribute
        3,          // size
        GL_FLOAT,   // type
        GL_FALSE,   // normalized?
        0,          // stride
        (void*)0    // array buffer offset
    ) );

    GL_CHECK_CALL( glBindBuffer( GL_ARRAY_BUFFER, m_ibo ) );
    GL_CHECK_CALL( glEnableVertexAttribArray( 2 ) );
    GL_CHECK_CALL( glVertexAttribIPointer(
        2,          // attribute
        1,          // size
        GL_INT,   // type
        0,          // stride
        (void*)0    // array buffer offset
    ) );

    // render 

    GL_CHECK_CALL( glDrawArrays( GL_TRIANGLES, 0, verts.size() ) );
}

void RenderModule::renderGeometry(
    const std::vector< TN::Vec3< float > > & verts,
    const std::vector< TN::Vec3< float > > & norms,    
    const std::vector< float > & v,
    const TN::Vec2<    float > & vR,
    const std::pair< TN::Vec3< float >, TN::Vec3< float > > & bbox )
{
    //////////////// buffers //////////////////////////////////

    GL_CHECK_CALL( glBindVertexArray( m_vao ) ); 

    GL_CHECK_CALL( glBindBuffer( GL_ARRAY_BUFFER, m_vbo ) );
    GL_CHECK_CALL( glBufferData( GL_ARRAY_BUFFER, verts.size() * 3 * sizeof( float ), verts.data(), GL_DYNAMIC_DRAW ) );

    GL_CHECK_CALL( glBindBuffer( GL_ARRAY_BUFFER, m_nbo ) );
    GL_CHECK_CALL( glBufferData( GL_ARRAY_BUFFER, norms.size() * 3 * sizeof( float ), norms.data(), GL_DYNAMIC_DRAW ) );

    GL_CHECK_CALL( glBindBuffer( GL_ARRAY_BUFFER, m_cbo ) );
    GL_CHECK_CALL( glBufferData( GL_ARRAY_BUFFER, v.size() * sizeof( float ), v.data(), GL_DYNAMIC_DRAW ) );

    GLuint loc;

    glm::mat4 proj = m_camera.matProj();
    glm::mat4 M( 1.f );

    double xW = ( bbox.second.x() - bbox.first.x() );
    double yW = ( bbox.second.y() - bbox.first.y() );    
    double zW = ( bbox.second.z() - bbox.first.z() );

    float maxWidth = std::max( std::max( xW, yW ), zW );

    M = glm::scale( M, { 1.0 / maxWidth, 1.0 / maxWidth, 1.0 / maxWidth  } );

    M = glm::translate( M, {
        -( bbox.first.x() + xW / 2.0 ),
        -( bbox.first.y() + yW / 2.0 ),    
        -( bbox.first.z() + zW / 2.0 ) } );

    glm::mat4 MV = m_camera.matView( 1.f ) * M;
    glm::mat4 MVP = proj * MV;
    glm::mat3 NM = glm::inverseTranspose( glm::mat3( MV ) );

    GL_CHECK_CALL( glDisable (GL_BLEND) );
    GL_CHECK_CALL( glDisable( GL_MULTISAMPLE ) );

    GL_CHECK_CALL( glBindFramebuffer(GL_FRAMEBUFFER, gBuffer) );
    GL_CHECK_CALL( glUseProgram( m_aoGeometryProgram ) );

    // uniforms

    GL_CHECK_CALL( glActiveTexture( GL_TEXTURE0 ) );
    m_tfTexture.bind();
    GLuint texID = GL_CHECK_CALL_ASSIGN( glGetUniformLocation( m_aoGeometryProgram, "tf" ) );
    GL_CHECK_CALL( glUniform1i( texID, 0 ) );

    loc = GL_CHECK_CALL_ASSIGN( glGetUniformLocation( m_aoGeometryProgram, "modelView" ) );
    GL_CHECK_CALL( glUniformMatrix4fv( loc, 1, GL_FALSE, &MV[ 0 ][ 0 ] ) );

    loc = GL_CHECK_CALL_ASSIGN( glGetUniformLocation( m_aoGeometryProgram, "normalMatrix" ) );
    GL_CHECK_CALL( glUniformMatrix3fv( loc, 1, GL_FALSE, &NM[ 0 ][ 0 ] ) );

    loc = GL_CHECK_CALL_ASSIGN( glGetUniformLocation( m_aoGeometryProgram, "mvp" ) );
    GL_CHECK_CALL( glUniformMatrix4fv( loc, 1, GL_FALSE, &MVP[ 0 ][ 0 ] ) );  
    
    // loc = GL_CHECK_CALL_ASSIGN( glGetUniformLocation( m_aoGeometryProgram, "cl" ) );
    // GL_CHECK_CALL( glUniform3fv( loc, 1, (float*) & color ) );

    loc = GL_CHECK_CALL_ASSIGN( glGetUniformLocation( m_aoGeometryProgram, "crange" ) );
    GL_CHECK_CALL( glUniform2fv( loc, 1, (float*) & vR ) );

    loc = GL_CHECK_CALL_ASSIGN( glGetUniformLocation( m_aoGeometryProgram, "solidColor" ) );
    GL_CHECK_CALL( glUniform1i( loc, false ) );

    // attributes

    GL_CHECK_CALL( glBindVertexArray( m_vao ) ); 

    GL_CHECK_CALL( glBindBuffer( GL_ARRAY_BUFFER, m_vbo ) );
    GL_CHECK_CALL( glEnableVertexAttribArray( 0 ) );
    GL_CHECK_CALL( glVertexAttribPointer(
        0,          // attribute
        3,          // size
        GL_FLOAT,   // type
        GL_FALSE,   // normalized?
        0,          // stride
        (void*)0    // array buffer offset
    ) );

    GL_CHECK_CALL( glBindBuffer( GL_ARRAY_BUFFER, m_nbo ) );
    GL_CHECK_CALL( glEnableVertexAttribArray( 1 ) );
    GL_CHECK_CALL( glVertexAttribPointer(
        1,          // attribute
        3,          // size
        GL_FLOAT,   // type
        GL_FALSE,   // normalized?
        0,          // stride
        (void*)0    // array buffer offset
    ) );

    GL_CHECK_CALL( glBindBuffer( GL_ARRAY_BUFFER, m_cbo ) );
    GL_CHECK_CALL( glEnableVertexAttribArray( 2 ) );
    GL_CHECK_CALL( glVertexAttribPointer(
        2,          // attribute
        1,          // size
        GL_FLOAT,   // type
        GL_FALSE,   // normalized?
        0,          // stride
        (void*)0    // array buffer offset
    ) );

    // render 

    GL_CHECK_CALL( glDrawArrays( GL_TRIANGLES, 0, verts.size() ) );
}

void RenderModule::renderGeometry(
    const std::vector< TN::Vec3< float > > & verts,
    const std::vector< TN::Vec3< float > > & norms,
    const TN::Vec3< float > & color,
    const std::pair< TN::Vec3< float >, TN::Vec3< float > > & bbox )
{
    //////////////// buffers //////////////////////////////////

    GL_CHECK_CALL( glBindVertexArray( m_vao ) ); 

    GL_CHECK_CALL( glBindBuffer( GL_ARRAY_BUFFER, m_vbo ) );
    GL_CHECK_CALL( glBufferData( GL_ARRAY_BUFFER, verts.size() * 3 * sizeof( float ), verts.data(), GL_DYNAMIC_DRAW ) );

    GL_CHECK_CALL( glBindBuffer( GL_ARRAY_BUFFER, m_nbo ) );
    GL_CHECK_CALL( glBufferData( GL_ARRAY_BUFFER, norms.size() * 3 * sizeof( float ), norms.data(), GL_DYNAMIC_DRAW ) );

    ///////////////

    GLuint loc; 

    glm::mat4 proj = m_camera.matProj();
    glm::mat4 M( 1.f );

    double xW = ( bbox.second.x() - bbox.first.x() );
    double yW = ( bbox.second.y() - bbox.first.y() );    
    double zW = ( bbox.second.z() - bbox.first.z() );

    float maxWidth = std::max( std::max( xW, yW ), zW );

    M = glm::scale( M, { 1.0 / maxWidth, 1.0 / maxWidth, 1.0 / maxWidth  } );

    M = glm::translate( M, {
        -( bbox.first.x() + xW / 2.0 ),
        -( bbox.first.y() + yW / 2.0 ),    
        -( bbox.first.z() + zW / 2.0 ) } );

    glm::mat4 MV = m_camera.matView( 1.f ) * M;
    glm::mat4 MVP = proj * MV;
    glm::mat3 NM = glm::inverseTranspose( glm::mat3( MV ) );

    GL_CHECK_CALL( glDisable (GL_BLEND) );
    GL_CHECK_CALL( glDisable( GL_MULTISAMPLE ) );

    GL_CHECK_CALL( glBindFramebuffer(GL_FRAMEBUFFER, gBuffer) );
    GL_CHECK_CALL( glUseProgram( m_aoGeometryProgram ) );

    // normals

    loc = GL_CHECK_CALL_ASSIGN( glGetUniformLocation( m_aoGeometryProgram, "modelView" ) );
    GL_CHECK_CALL( glUniformMatrix4fv( loc, 1, GL_FALSE, &MV[ 0 ][ 0 ] ) );

    loc = GL_CHECK_CALL_ASSIGN( glGetUniformLocation( m_aoGeometryProgram, "normalMatrix" ) );
    GL_CHECK_CALL( glUniformMatrix3fv( loc, 1, GL_FALSE, &NM[ 0 ][ 0 ] ) );

    loc = GL_CHECK_CALL_ASSIGN( glGetUniformLocation( m_aoGeometryProgram, "mvp" ) );
    GL_CHECK_CALL( glUniformMatrix4fv( loc, 1, GL_FALSE, &MVP[ 0 ][ 0 ] ) );  

    loc = GL_CHECK_CALL_ASSIGN( glGetUniformLocation( m_aoGeometryProgram, "cl" ) );
    GL_CHECK_CALL( glUniform3fv( loc, 1, (float*) & color ) );

    loc = GL_CHECK_CALL_ASSIGN( glGetUniformLocation( m_aoGeometryProgram, "solidColor" ) );
    GL_CHECK_CALL( glUniform1i( loc, true ) );

    // attributes

    GL_CHECK_CALL( glBindVertexArray( m_vao ) ); 

    GL_CHECK_CALL( glBindBuffer( GL_ARRAY_BUFFER, m_vbo ) );
    GL_CHECK_CALL( glEnableVertexAttribArray( 0 ) );
    GL_CHECK_CALL( glVertexAttribPointer(
        0,          // attribute
        3,          // size
        GL_FLOAT,   // type
        GL_FALSE,   // normalized?
        0,          // stride
        (void*)0    // array buffer offset
    ) );

    GL_CHECK_CALL( glBindBuffer( GL_ARRAY_BUFFER, m_nbo ) );
    GL_CHECK_CALL( glEnableVertexAttribArray( 1 ) );
    GL_CHECK_CALL( glVertexAttribPointer(
        1,          // attribute
        3,          // size
        GL_FLOAT,   // type
        GL_FALSE,   // normalized?
        0,          // stride
        (void*)0    // array buffer offset
    ) );

    GL_CHECK_CALL( glDrawArrays( GL_TRIANGLES, 0, verts.size() ) );
}

void RenderModule::renderPrimitives(
    const std::vector< TN::Vec3< float > > & verts,
    const TN::Vec3< float > & color,
    unsigned int mode )
{
    //////////////// buffers //////////////////////////////////

    GL_CHECK_CALL( glBindVertexArray( m_vao ) ); 

    GL_CHECK_CALL( glBindBuffer( GL_ARRAY_BUFFER, m_vbo ) );
    GL_CHECK_CALL( glBufferData( GL_ARRAY_BUFFER, verts.size() * 3 * sizeof( float ), verts.data(), GL_DYNAMIC_DRAW ) );

    ///////////////////////////////////////////////////////////

    GLuint loc; 

    glm::mat4 proj = m_camera.matProj();
    glm::mat4 M( 1.f );
    glm::mat4 MV = m_camera.matView( 1.f ) * M;
    glm::mat4 MVP = proj * MV;
    glm::mat3 NM = glm::inverseTranspose( glm::mat3( MV ) );

    GL_CHECK_CALL( glDisable (GL_BLEND) );
    GL_CHECK_CALL( glDisable( GL_MULTISAMPLE ) );

    GL_CHECK_CALL( glBindFramebuffer(GL_FRAMEBUFFER, gBuffer) );
    GL_CHECK_CALL( glUseProgram( m_aoGeometryPrimProg ) );

    // normals

    loc = GL_CHECK_CALL_ASSIGN( glGetUniformLocation( m_aoGeometryPrimProg, "modelView" ) );
    GL_CHECK_CALL( glUniformMatrix4fv( loc, 1, GL_FALSE, &MV[ 0 ][ 0 ] ) );

    loc = GL_CHECK_CALL_ASSIGN( glGetUniformLocation( m_aoGeometryPrimProg, "normalMatrix" ) );
    GL_CHECK_CALL( glUniformMatrix3fv( loc, 1, GL_FALSE, &NM[ 0 ][ 0 ] ) );

    loc = GL_CHECK_CALL_ASSIGN( glGetUniformLocation( m_aoGeometryPrimProg, "mvp" ) );
    GL_CHECK_CALL( glUniformMatrix4fv( loc, 1, GL_FALSE, &MVP[ 0 ][ 0 ] ) );  

    loc = GL_CHECK_CALL_ASSIGN( glGetUniformLocation( m_aoGeometryPrimProg, "cl" ) );
    GL_CHECK_CALL( glUniform3fv( loc, 1, (float*) & color ) );

    // attributes

    GL_CHECK_CALL( glBindVertexArray( m_vao ) ); 
    GL_CHECK_CALL( glBindBuffer( GL_ARRAY_BUFFER, m_vbo ) );
    GL_CHECK_CALL( glEnableVertexAttribArray( 0 ) );
    GL_CHECK_CALL( glVertexAttribPointer(
        0,          // attribute
        3,          // size
        GL_FLOAT,   // type
        GL_FALSE,   // normalized?
        0,          // stride
        (void*)0    // array buffer offset
    ) );

    GL_CHECK_CALL( glDrawArrays( mode, 0, verts.size() ) );
}

void RenderModule::renderSSAO( const std::string viewId, const TN::Vec3< float > bkgColor )
{
    if( ! m_managedViewTextures.count( viewId ) && viewId != "default" )
    {
        createView( viewId );
    }

    GL_CHECK_CALL( glDisable ( GL_BLEND ) );
    GL_CHECK_CALL( glDisable( GL_MULTISAMPLE ) );
    GL_CHECK_CALL( glEnable( GL_DEPTH_TEST ) );

    GLuint loc;

    ////////////////////////////////////////////////////////////////////////////////////////////

    GL_CHECK_CALL( glBindFramebuffer(GL_FRAMEBUFFER, ssaoFBO ) );

    GL_CHECK_CALL( glViewport( 0, 0, width() * m_multiSample, height() * m_multiSample ) );
    GL_CHECK_CALL( glClear(GL_COLOR_BUFFER_BIT) );

    GL_CHECK_CALL( glUseProgram( m_aoShaderProgram ) );

    loc = GL_CHECK_CALL_ASSIGN( glGetUniformLocation( m_aoShaderProgram, "gPosition" ) );
    GL_CHECK_CALL( glUniform1i( loc, 0 ) );

    loc = GL_CHECK_CALL_ASSIGN( glGetUniformLocation( m_aoShaderProgram, "gNormal" ) );
    GL_CHECK_CALL( glUniform1i( loc, 1 ) );

    loc = GL_CHECK_CALL_ASSIGN( glGetUniformLocation( m_aoShaderProgram, "texNoise" ) );
    GL_CHECK_CALL( glUniform1i( loc, 2 ) );

    // Send kernel + rotation
    for (unsigned int i = 0; i < aoKernelSize; ++i)
    {
        loc = glGetUniformLocation( m_aoShaderProgram,  ( "samples[" + std::to_string(i) + "]" ).c_str()  );
        GL_CHECK_CALL( glUniform3fv( loc, 1, (const GLfloat *) & ssaoKernel[i].x ) );
    }

    glm::mat4 proj = m_camera.matProj();

    loc = GL_CHECK_CALL_ASSIGN( glGetUniformLocation( m_aoShaderProgram, "projection" ) );
    GL_CHECK_CALL( glUniformMatrix4fv( loc, 1, GL_FALSE, &proj[ 0 ][ 0 ] ) );

    loc = GL_CHECK_CALL_ASSIGN( glGetUniformLocation( m_aoShaderProgram, "width" ) );
    GL_CHECK_CALL( glUniform1i( loc, width() ) );

    loc = GL_CHECK_CALL_ASSIGN( glGetUniformLocation( m_aoShaderProgram, "height" ) );
    GL_CHECK_CALL( glUniform1i( loc, height() ) );

    loc = GL_CHECK_CALL_ASSIGN( glGetUniformLocation( m_aoShaderProgram, "occlusionRadius" ) );
    GL_CHECK_CALL( glUniform1f( loc, m_occlusionRadius * m_occlusionScale ) );

    loc = GL_CHECK_CALL_ASSIGN( glGetUniformLocation( m_aoShaderProgram, "bias" ) );
    GL_CHECK_CALL( glUniform1f( loc, m_occlusionBias * m_occlusionScale ) );

    loc = GL_CHECK_CALL_ASSIGN( glGetUniformLocation( m_aoShaderProgram,"noise" ) );
    GL_CHECK_CALL( glUniform1f( loc, m_occlusionNoiseScale ) );

    loc = GL_CHECK_CALL_ASSIGN( glGetUniformLocation( m_aoShaderProgram,"kernelSize" ) );
    GL_CHECK_CALL( glUniform1i( loc, aoKernelSize ) );

    GL_CHECK_CALL( glActiveTexture(GL_TEXTURE0) );
    GL_CHECK_CALL( glBindTexture(GL_TEXTURE_2D, gPosition) );

    GL_CHECK_CALL( glActiveTexture(GL_TEXTURE1) );
    GL_CHECK_CALL( glBindTexture(GL_TEXTURE_2D, gNormal) );

    GL_CHECK_CALL( glActiveTexture(GL_TEXTURE2) );
    GL_CHECK_CALL( glBindTexture(GL_TEXTURE_2D, noiseTexture) );

    renderQuad();

    //////////////////////////////////////////////////////////////////////////////////////

    // Generate Blur Buffer

    GL_CHECK_CALL( glBindFramebuffer(GL_FRAMEBUFFER, ssaoBlurFBO) );
    GL_CHECK_CALL( glViewport( 0, 0, width() * m_multiSample, height() * m_multiSample ) );    
    GL_CHECK_CALL( glClear(GL_COLOR_BUFFER_BIT) );

    GL_CHECK_CALL( glUseProgram( m_aoBlurProgram ) );

    loc = GL_CHECK_CALL_ASSIGN( glGetUniformLocation( m_aoBlurProgram, "ssaoInput" ) );
    GL_CHECK_CALL( glUniform1i( loc, 0 ) );

    GL_CHECK_CALL( glActiveTexture(GL_TEXTURE0) );
    GL_CHECK_CALL( glBindTexture(GL_TEXTURE_2D, ssaoColorBuffer) );

    renderQuad();

    ///////////////////////////////////////////////////////////////////////////////////////

    // If rendering to texture, need to prepare the framebuffer 

    unsigned int frameBufferTarget = 0;

    if( viewId != "default" )
    {
        auto index = m_managedViewTextures.at( viewId );

        if( index < 0 || index >= m_numViewTextures )
        {
            throw std::runtime_error( "RenderModule, view texture index out of range." );
        }

        GL_CHECK_CALL( glDeleteFramebuffers( 1, & m_frameBufferToViewTexture ) );

        GL_CHECK_CALL( glGenFramebuffers(    1, & m_frameBufferToViewTexture ) );
        GL_CHECK_CALL( glBindFramebuffer( GL_FRAMEBUFFER, m_frameBufferToViewTexture ) );
        
        GL_CHECK_CALL( glBindTexture( GL_TEXTURE_2D, m_viewTextures[ index ] ) );
        GL_CHECK_CALL( glTexImage2D(  GL_TEXTURE_2D, 0, GL_RGB, width(), height(), 0, GL_RGB, GL_UNSIGNED_BYTE, NULL ) );

        GL_CHECK_CALL( glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR ) );
        GL_CHECK_CALL( glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR ) );

        GL_CHECK_CALL( glBindTexture( GL_TEXTURE_2D, 0 ) );

        GL_CHECK_CALL( glFramebufferTexture2D( GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, m_viewTextures[ index ], 0 ) );  

        if( GL_CHECK_CALL_ASSIGN( glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE ) )
        {
            throw std::runtime_error( "RenderModule, view texture framebuffer incomplete" );
        }

        frameBufferTarget = m_frameBufferToViewTexture;
    }

    ///////////////////////////////////////////////////////////////////////////////////////

    GL_CHECK_CALL( glBindFramebuffer( GL_FRAMEBUFFER, frameBufferTarget ) );

    if( viewId == "default" )
    {
        GL_CHECK_CALL( glViewport( position().x(), position().y(), width(), height() ) );
    }
    else
    {
        GL_CHECK_CALL( glViewport( 0, 0, width(), height() ) );
    }

    // glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

    GL_CHECK_CALL( glUseProgram( m_aoLightingProgram ) );

    GL_CHECK_CALL( glDisable (GL_BLEND) );
    GL_CHECK_CALL( glDisable( GL_MULTISAMPLE ) );

    loc = GL_CHECK_CALL_ASSIGN( glGetUniformLocation( m_aoLightingProgram, "gPosition" ) );
    GL_CHECK_CALL( glUniform1i( loc, 0 ) );

    loc = GL_CHECK_CALL_ASSIGN( glGetUniformLocation( m_aoLightingProgram, "gNormal" ) );
    GL_CHECK_CALL( glUniform1i( loc, 1 ) );

    loc = GL_CHECK_CALL_ASSIGN( glGetUniformLocation( m_aoLightingProgram, "gAlbedo" ) );
    GL_CHECK_CALL( glUniform1i( loc, 2 ) );

    loc = GL_CHECK_CALL_ASSIGN( glGetUniformLocation( m_aoLightingProgram, "ssao" ) );
    GL_CHECK_CALL( glUniform1i( loc, 3 ) );

    // Can I get the lighting direction based on the camera?

    auto eye = m_camera.getEye();
    eye.y += 0.2;

    loc = GL_CHECK_CALL_ASSIGN( glGetUniformLocation( m_aoLightingProgram, "light.direction" ) );
    GL_CHECK_CALL( glUniform3fv( loc, 1, (const GLfloat *) & eye.x ) );

    loc = GL_CHECK_CALL_ASSIGN( glGetUniformLocation( m_aoLightingProgram, "MT.Ka" ) );
    GL_CHECK_CALL( glUniform1f( loc, m_material.Ka_scale ) );

    loc = GL_CHECK_CALL_ASSIGN( glGetUniformLocation( m_aoLightingProgram, "MT.Kd" ) );
    GL_CHECK_CALL( glUniform1f( loc, m_material.Kd_scale ) );

    loc = GL_CHECK_CALL_ASSIGN( glGetUniformLocation( m_aoLightingProgram, "MT.Ks" ) );
    GL_CHECK_CALL( glUniform1f( loc, m_material.Ks_scale ) );

    loc = GL_CHECK_CALL_ASSIGN( glGetUniformLocation( m_aoLightingProgram, "MT.shininess" ) );
    GL_CHECK_CALL( glUniform1f( loc, m_material.shininess ) );

    loc = GL_CHECK_CALL_ASSIGN( glGetUniformLocation( m_aoLightingProgram, "bkgColor" ) );
    GL_CHECK_CALL( glUniform3fv( loc, 1, (float*) & bkgColor ) );

    GL_CHECK_CALL( glActiveTexture(GL_TEXTURE0) );
    GL_CHECK_CALL( glBindTexture(GL_TEXTURE_2D, gPosition) );

    GL_CHECK_CALL( glActiveTexture(GL_TEXTURE1) );
    GL_CHECK_CALL( glBindTexture(GL_TEXTURE_2D, gNormal) );

    GL_CHECK_CALL( glActiveTexture(GL_TEXTURE2) );
    GL_CHECK_CALL( glBindTexture(GL_TEXTURE_2D, gAlbedo) );

    GL_CHECK_CALL( glActiveTexture(GL_TEXTURE3) ); // add extra SSAO texture to lighting pass
    GL_CHECK_CALL( glBindTexture(GL_TEXTURE_2D, ssaoColorBufferBlur ) );

    GL_CHECK_CALL( glEnable (GL_BLEND) );
    GL_CHECK_CALL( glEnable( GL_MULTISAMPLE ) );

    renderQuad();
}

void RenderModule::clear( const TN::Vec3<float> & color  )
{
    if( ! frameBuffersReady )
    {
        createFrameBuffers( width(), height() );
    }

    GL_CHECK_CALL( glDisable (GL_BLEND) );
    GL_CHECK_CALL( glDisable( GL_MULTISAMPLE ) );
    GL_CHECK_CALL( glEnable( GL_DEPTH_TEST ) );

    GL_CHECK_CALL( glBindFramebuffer(GL_FRAMEBUFFER, gBuffer) );
    GL_CHECK_CALL( glViewport( 0, 0, width() * m_multiSample, height() * m_multiSample ) );

    GL_CHECK_CALL( glClearColor( color.x(), color.y(), color.z(), 1.0f ) );    
    GL_CHECK_CALL( glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT) );
}


void RenderModule::clear()
{
    clear( m_backgroundColor );
}

void RenderModule::setBackfaceCulling( bool cond )
{
    if ( cond )
        GL_CHECK_CALL( glEnable(GL_CULL_FACE) );
    else
        GL_CHECK_CALL( glDisable(GL_CULL_FACE) );
}

void RenderModule::setRenderingMode( int type ) {
    m_renderMode = type;
}

void RenderModule::setPolygonMode(int mode)
{
    unsigned int modeMap[] = { GL_FILL, GL_LINE };
    GL_CHECK_CALL( glPolygonMode(GL_FRONT_AND_BACK, modeMap[mode] ) );
}

void RenderModule::setProjectionMode(int mode)
{
    TN::TrackBallCamera::ProjMode modeMap[] = { TN::TrackBallCamera::PM_Perspective, TN::TrackBallCamera::PM_Orthographic };
    m_camera.setProjMode( modeMap[mode] );
}

void RenderModule::setSize( float wd, float ht )
{
    if( wd == width() && ht == height() )
    {
        return;
    }

    int w = wd;
    int h = ht;

    while( w % 4 != 0 )
    {
        --w;
    }
    while( h % 4 != 0 )
    {
        --h;
    }

    TN::Widget::setSize( w, h );

    m_camera.setAspectRatio( float( width() ) / height() );

    deleteFrameBuffers();
    createFrameBuffers( w * m_multiSample, h * m_multiSample );
}

void RenderModule::pan( TN::Vec2< float > delta, MouseButtonState buttonState )
{
    if( buttonState == MouseButtonState::LeftPressed )
    {
        m_camera.orbit( glm::vec2( -delta.x() * 0.04 / width(), -delta.y() * 0.04 / width() ) );
    }
    else if( buttonState == MouseButtonState::RightPressed  )
    {
        m_camera.track( glm::vec2( -delta.x() / width(), -delta.y() / width() ) );
    }
}

void RenderModule::zoom( int dz )
{
    m_camera.zoom( -dz / 3.0 );
}

}
