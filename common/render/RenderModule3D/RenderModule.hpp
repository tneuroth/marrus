#ifndef TN_RENDER_MODULE_HPP
#define TN_RENDER_MODULE_HPP

#include "render/Camera/TrackBallCamera.hpp"
#include "render/Parameters/Material.hpp"
#include "render/Parameters/LightSource.hpp"
#include "geometry/Vec.hpp"
#include "render/Texture/Texture.hpp"
#include "views/Widget.hpp"
#include "views/InputState.hpp"
#include "configuration/SpatialViewConfiguration.hpp"
#include "configuration/GlyphConfiguration.hpp"

#include <glad/glad.h>

#include <glm/glm.hpp>
#include <glm/gtx/transform.hpp>

#include <random>
#include <memory>
#include <fstream>
#include <sstream>
#include <stdexcept>

namespace TN {

class RenderModule : public TN::Widget
{
    std::string m_baseDirectory;

    bool objIstextured( const std::string & path );

    void loadObj( 
        std::vector< TN::Vec3< float > > & verts,
        std::vector< TN::Vec3< float > > & norms,
        std::vector< int > & objOffsetsOut, 
        const std::string & path );

    void loadObj( 
        std::vector< TN::Vec3< float > > & verts,
        std::vector< TN::Vec3< float > > & norms,
        std::vector< TN::Vec2< float > > & uv,     
        std::vector< int > & objOffsetsOut,   
        const std::string & path );

    void loadGlyphTexture( const std::string & glyphTexture="" );
    void loadGlyph( const std::string & glyph, const std::string & glyphTexture );
    void loadGlyph( const std::string & glyph );

public:

    void renderGlyphPreview( int colorMode, const TN::Vec2< float > & size );
    
    void renderGlyphPreview( 
        const TN::GlyphConfiguration     & config,
        const TN::Vec2< float >          & size,
        const std::vector< float >       & data,
        const std::vector< TN::Vec2< float > > & norms,
        const TN::Vec2< float > & vlims,
        bool perGlyphNormalization,
        bool perSelScaleBinNormalization,
        bool normalizeFromZero,
        bool allAxisNormalization,
        bool showBorder,
        int scaleMode );

    RenderModule( 
        const TN::Vec2< int > & pos, 
        const TN::Vec2< int > & size, 
        const std::string & workingDirectory );
    virtual ~RenderModule();

    bool frameBuffersReady;

    void createFrameBuffers( int width, int height );
    void deleteFrameBuffers();

    void initialize();

    void loadShaders( 
        const std::string & path,
        const std::string & vert, 
        const std::string & frag, 
        const std::string & geo,         
        GLuint & shader );

    void clear();
    void clear( const TN::Vec3<float> & color  );

    void renderQuad();

    void renderGeometry(
        const std::vector< TN::Vec3< float > > & verts,
        const std::vector< TN::Vec3< float > > & norms,
        const std::vector< float > & v,   
        const TN::Vec2<    float > & vR,
        const std::pair< TN::Vec3< float >, TN::Vec3< float > > & bbox );

    void renderGeometry(
        const std::vector< TN::Vec3< float > > & verts,
        const std::vector< TN::Vec3< float > > & norms,
        const TN::Vec3<    float > & color,
        const std::pair< TN::Vec3< float >, TN::Vec3< float > > & bbox );

    void renderGeometry(
        const std::vector< TN::Vec3< float > > & verts,
        const std::vector< TN::Vec3< float > > & norms,
        const std::vector< int > & bitFlags,
        const TN::Vec3<    float > & color,
        const TN::Vec3< float > & color2,
        const std::pair< TN::Vec3< float >, TN::Vec3< float > > & bbox );

    void renderPrimitives(
        const std::vector< TN::Vec3< float > > & verts,
        const TN::Vec3< float > & color,
        unsigned int mode );

    void setGlyphTextureData( 
        const std::vector< float > & data,
        const int per_instance_size,
        const int num_instances );

    void renderGlyphs3DTextured(
        const std::vector< float > & positions,
        const std::vector< float > & u,
        const std::vector< float > & v,
        const std::vector< float > & n,
        const std::vector< float > & c,
        const TN::Vec3< float > & color,
        int colorMode,
        const std::pair< TN::Vec3< float >, TN::Vec3< float > > & bbox,
        int orientationMode );

    void renderRadialGlyphs(
        const TN::GlyphConfiguration & glyph,
        const float glyphScale,
        const std::vector< TN::Vec2< float > > & axes_lims,
        const TN::Vec2< float > & vector_lims,
        const std::vector< float >  & positions,
        const std::vector< float >  & u,
        const std::vector< float >  & v,
        const std::vector< float >  & n,
        const std::vector< float >  & c,
        const TN::Vec3< float >     & color,
        int colorMode,
        const std::pair< TN::Vec3< float >, TN::Vec3< float > > & bbox,
        int orientationMode,
        int scaleMode,
        bool perGlyphNormalization,
        bool perSelScalebinNormalization,
        bool normalizeGlyphsFromZero,
        bool allAxisNormalization,
        bool showBorder,
        bool isPreview );

    void renderGlyphs3DTexBuffer(
        const std::vector< float > & positions,
        const std::vector< float > & u,
        const std::vector< float > & v,
        const std::vector< float > & n,
        const std::vector< float > & c,
        const TN::Vec3< float >    & color,
        int colorMode,
        const std::pair< TN::Vec3< float >, TN::Vec3< float > > & bbox,
        int orientationMode );

    // void renderGlyphs3D(
    //     const std::vector< float > & positions,
    //     const std::vector< float > & u,
    //     const std::vector< float > & v,
    //     const std::vector< float > & n,
    //     const std::vector< float > & c,
    //     const TN::Vec3< float > & color,
    //     int colorMode,
    //     const std::pair< TN::Vec3< float >, TN::Vec3< float > > & bbox,
    //     int orientationMode );
    
    void renderSSAO( const std::string viewId = "default", const TN::Vec3< float > bkgColor = { 0.1f, 0.1f, 0.1f } );

    void checkError( const std::string & prefix );

    void resetCamera();
    void setCameraCenter( glm::vec3 center ) 
    {
        m_camera.setCenter( center );
    }

    void setCameraOrientation( const glm::vec3 & up, const glm::vec3 & eye )
    {
        m_camera.setUpAndEye( up, eye );
    }

    void rotateCamera( const std::string & dir )
    {
        m_camera.rotate( dir );
    }

    const TN::TrackBallCamera & getCamera() { return m_camera; }
    void setCamera( const TN::TrackBallCamera & cam ) { 
        m_camera = cam; 
        m_camera.setAspectRatio( float( width() ) / height() );
    }

    void setPolygonMode(int mode);
    void setProjectionMode(int mode);
    void setBackfaceCulling( bool cond );
    void setRenderingMode( int type );

    virtual void setSize( float width, float height ) override;

    int width()  const { return size().x(); }
    int height() const { return size().y(); }

    void compileShaders();

    void pan( TN::Vec2< float > delta, MouseButtonState buttonState );
    void zoom( int dz );
    
    bool handleInput( const InputState & input )
    {
        bool changed = false;
        if( pointInViewPort( 
            TN::Vec2< float >( 
                input.mouseState.position.x(), 
                input.mouseState.position.y() ) ) )
        {
            if( std::abs( input.mouseState.scrollDelta() ) > 0 )
            {
                zoom( input.mouseState.scrollDelta() );
                changed = true;
            }
            if( ( std::abs( input.mouseState.positionDelta.x() ) > 0 || std::abs( input.mouseState.positionDelta.y() ) > 0 )
             && ( input.mouseState.buttonState == MouseButtonState::LeftPressed 
                  || input.mouseState.buttonState == MouseButtonState::RightPressed ) )
            {
                pan(  input.mouseState.positionDelta*-1.0, input.mouseState.buttonState );
                changed = true;
            }
        }
        return changed;
    }

std::vector< TN::Vec3< float > > getColors( const std::string & path )
{
    std::ifstream inFile( path );
    std::string line;
    std::vector< TN::Vec3< float > > colors;    
    
    while( std::getline( inFile, line ) )
    {
        std::string rgb[ 3 ];
        for( int i = 1, cr = 0; i < line.size() && cr < 3; ++i )
        {
            if( line[i] == ',' )
            {
                ++cr;
                continue;
            }
            else if( line[ i ] == ')' )
            {
                break;
            }
            else if( line[ i ] == ' ' )
            {
                continue;
            }
            else
            {
                rgb[ cr ].push_back( line[ i ] );
            }
        }
    
        colors.push_back( TN::Vec3< float >(
            std::stof( rgb[ 0 ] ),
            std::stof( rgb[ 1 ] ),
            std::stof( rgb[ 2 ] ) ) );
    }

    return colors;
}

private:

    void loadTF( const std::string & path, TN::Texture1D3 & tex )
    {
        auto colors = getColors( path );
        tex.load( colors );
    }

    TN::TrackBallCamera m_camera;

    bool initializedGL;

    GLuint m_aoShaderProgram;
    GLuint m_aoBlurProgram;
    GLuint m_aoGeometryProgram;
    GLuint m_aoGeometryPrimProg;
    GLuint m_aoGeometryProgramBF;
    GLuint m_aoLightingProgram;        
    GLuint m_glyph3DProgram;
    GLuint m_glyph3DProgramG;
    GLuint m_glyph3DProgramBuff;
    GLuint m_radialGlyphProgram;
    GLuint m_directionalGlyphProgram;
    GLuint m_starGlyphProgram;

    GLuint m_texProgram;

    std::string m_glyphPath;
    std::string  m_glyphTexPath;
    std::vector< int > m_glyphObjOffsets;

    int NSPHERE_VERTS;

    unsigned int gBuffer;
    unsigned int gPosition, gNormal, gAlbedo;
    unsigned int ssaoFBO, ssaoBlurFBO;
    unsigned int ssaoColorBuffer, ssaoColorBufferBlur;
    unsigned int noiseTexture;
    unsigned int rboDepth;

    const int MAX_KERNEL_SIZE = 512;
    std::vector<glm::vec3> ssaoKernel;
    std::vector<TN::Vec3<float>> ssaoNoise;

    unsigned int aoKernelSize;
    float m_occlusionBias;
    float m_occlusionNoiseScale;
    float m_occlusionRadius;
    float m_occlusionScale;

    int m_multiSample;

//*******************************************************

    GLuint m_frameBufferToViewTexture;

    // maps view id to index into the view textures array, and size
    std::map< std::string, int > m_managedViewTextures;
    static constexpr int MAX_VIEW_TEXTURES = 64;
    int m_numViewTextures;
    std::array< GLuint, MAX_VIEW_TEXTURES > m_viewTextures;
    std::string m_activeViewId;

//*******************************************************

    GLuint m_vao;

    GLuint m_vbo;
    GLuint m_nbo;
    GLuint m_cbo;        
    GLuint m_ibo;

    GLuint m_quadVAO;
    GLuint m_quadVBO;

    TN::Texture1D3 m_tfTexture;
    TN::Texture1D3 m_tfTextureForLayers;

//*******************************************************

    GLuint m_glyphVAO;

    GLuint m_glyphOffsetBuffer;
    GLuint m_glyphNormalBuffer;
    GLuint m_glyphDirection1Buffer;
    GLuint m_glyphDirection2Buffer;

    GLuint m_glyphScalarBuffer;   
    GLuint m_glyphFiltersBuffer;

    GLuint m_glyphPsBuffer;
    GLuint m_glyphNmBuffer;
    GLuint m_glyphTcBuffer;

    TN::Texture1D3 m_glyphTFTexture;

//*******************************************************

    GLuint m_glyphTexBuffer;
    GLuint m_glyphBufferTex;

    TN::Texture2D3 m_glyphTextureTemplate;
    bool m_glyphIsTextured;

    int m_perInstanceGlyphBufferSize;
    int m_nGlyhphInstances;

//*******************************************************

    int m_renderMode;
    TN::Vec3< float > m_backgroundColor;
    float m_tubeRadius;
    TN::Material m_material;
    TN::LightSource m_lightSource;
    TN::Vec2< float > m_mousePrev;

    int m_occlusionRadiusMode;
    int m_occlusionBiasMode;
    int m_occlusionScaleMode;    
    int m_diffuseMode;
    int m_specularMode;
    int m_specularExponentMode;
    int m_ambientMode;
    int m_multiSampleMode;
    int m_kernelSizeMode;    
    int m_backgroundMode;

public:

    void createGlyphTF( std::vector< std::string > & cms )
    {
        const std::string D = m_baseDirectory + "/TF/";

        std::vector< TN::Vec3< float > > all;
        const int NC_EACH = 256;

        for( int i = 0; i < cms.size(); ++i )
        {
            auto colors = getColors( D + cms[ i ] + ".txt" );
            
            const size_t count  = colors.size();  
            const size_t stride = count / NC_EACH;

            for( size_t k = 0; k < colors.size(); k+= stride )
            {
                all.push_back( colors[ k ] );
            }
        }

        m_glyphTFTexture.load( all );
    }

    void createView( const std::string & viewId )
    {
        if( m_numViewTextures >= MAX_VIEW_TEXTURES )
        {
            throw std::runtime_error( "RenderModule, exceded maximum 3D views." );
        }

        m_managedViewTextures.insert( { viewId, m_numViewTextures } );
        ++m_numViewTextures;

        auto & texId = m_viewTextures[ m_numViewTextures - 1 ];

        glGenTextures(   1, & texId );
    }

    void removeView( const std::string & viewId  )
    {
        if( ! m_managedViewTextures.count( viewId ) )
        {
            throw std::runtime_error( "RenderModule, attempted to remove view " + viewId + " which doesn't exist" );
        }

        int index = m_managedViewTextures.at( viewId );
        m_managedViewTextures.erase( viewId );

        // shift all existing textures beyond index backwards
        for( auto & v : m_managedViewTextures )
        {
            if( v.second > index ) {
                --v.second;
            }
        }

        --m_numViewTextures;

        // erase the last one

        glDeleteTextures( 1, & m_viewTextures[ m_numViewTextures ] );
    } 

    void renderViewTexture( const std::string & viewId )
    {
        if( ! m_managedViewTextures.count( viewId ) )
        {
            throw std::runtime_error( "RenderModule, attempted to render view " + viewId + " which doesn't exist" );
        }

        glBindFramebuffer( GL_FRAMEBUFFER, 0 );

        checkError( "before render view texture" );

        glViewport( position().x(), position().y(), width(), height() );

        const int texIndex = m_managedViewTextures.at( viewId );

        glUseProgram( m_texProgram );

        auto loc = glGetUniformLocation( m_texProgram, "tex" );
        glUniform1i( loc, 0 );

        glActiveTexture( GL_TEXTURE0 );
        glBindTexture(   GL_TEXTURE_2D, m_viewTextures[ texIndex ] );

        renderQuad();

        checkError( "after render view texture" );
    }

    void printSetting()
    {
        std::cout << "m_occlusionRadiusMode   = " << m_occlusionRadiusMode << std::endl;
        std::cout << "m_occlusionBiasMode     = " << m_occlusionBiasMode   << std::endl;
        std::cout << "m_occlusionScaleMode    = " << m_occlusionScaleMode  << std::endl;        
        std::cout << "m_diffuseMode           = " << m_diffuseMode         << std::endl;
        std::cout << "m_specularMode          = " << m_specularMode        << std::endl;
        std::cout << "m_specularExponentMode  = " << m_specularMode        << std::endl;        
        std::cout << "m_ambientMode           = " << m_ambientMode         << std::endl;
        std::cout << "m_multiSampleMode       = " << m_multiSampleMode     << std::endl;
        std::cout << "m_kernelSizeMode        = " << m_kernelSizeMode      << std::endl;
        std::cout << "m_backgroundMode        = " << m_backgroundMode      << std::endl;        
    }

    std::string m_TF;
    int m_TFMode;

    void setTFMode( int mode )
    {
        const std::string base = m_baseDirectory + "/TF/";
        const std::vector< std::string > modes = 
        {
             base + "perceptual/viridis.txt",
             base + "perceptual/plasma.txt",
            base + "perceptual/inferno.txt",     
            base + "perceptual/magma.txt",
            base + "perceptual/cividis.txt",

            base + "sequential/Purples.txt",
            base + "sequential/Blues.txt",
            base + "sequential/Greens.txt",
            base + "sequential/Oranges.txt",                        
            base + "sequential/Reds.txt",
            base + "sequential/YlOrBr.txt",
            base + "sequential/YlOrRd.txt",
            base + "sequential/Reds.txt",      
            base + "sequential/PuRd.txt",
            base + "sequential/RdPu.txt",
            base + "sequential/BuPu.txt",
            base + "sequential/GnBu.txt",      
            base + "sequential/PuBu.txt",
            base + "sequential/YlGnBu.txt",
            base + "sequential/PuBuGn.txt",
            base + "sequential/BuGn.txt",      
            base + "sequential/YlGn.txt",

            base + "sequential2/bone.txt",
            base + "sequential2/pink.txt",
            base + "sequential2/spring.txt",      
            base + "sequential2/summer.txt",
            base + "sequential2/autumn.txt",
            base + "sequential2/winter.txt",
            base + "sequential2/cool.txt",      
            base + "sequential2/Wistia.txt",
            base + "sequential2/hot.txt",
            base + "sequential2/afmhot.txt",
            base + "sequential2/gist_heat.txt",      
            base + "sequential2/copper.txt",

            base + "misc/flag.txt",
            base + "misc/prism.txt",      
            base + "misc/ocean.txt",
            base + "misc/gist_earth.txt",
            base + "misc/terrain.txt",
            base + "misc/gist_stern.txt",      
            base + "misc/gnuplot.txt",
            base + "misc/gnuplot2.txt",
            base + "misc/CMRmap.txt",
            base + "misc/cubehelix.txt",      
            base + "misc/brg.txt",
            base + "misc/rainbow.txt",
            base + "misc/jet.txt",
            base + "misc/nipy_spectral.txt",      
            base + "misc/gist_ncar.txt",
            base + "misc/twilight_shifted.txt",


            base + "diverging/PiYG.txt",
            base + "diverging/PRGn.txt",
            base + "diverging/BrBG.txt",
            base + "diverging/PuOr.txt",      
            base + "diverging/RdGy.txt",
            base + "diverging/RdBu.txt",
            base + "diverging/RdYlBu.txt",
            base + "diverging/RdYlGn.txt",      
            base + "diverging/Spectral.txt",
            base + "diverging/coolwarm.txt",
            base + "diverging/bwr.txt",
            base + "diverging/seismic.txt"
        };

        m_TFMode = mode % modes.size();

        std::cout << "tf " << modes[ m_TFMode ] << " set " << std::endl;
 
        m_TF = modes[ m_TFMode ];           
        loadTF( m_TF, m_tfTexture );      
    }

    std::string cycleTF() 
    { 
        setTFMode( m_TFMode + 1 ); 
        return m_TF;
    }

    TN::Texture1D3 & getTF()
    {
        return m_tfTexture;
    }

    /////


    void setOcclusionScaleMode( int mode )
    {
        const std::vector< float > modes =
        {
               1.0,
               1.5,
               2.0,
               2.5,                
               3.0
        };

        m_occlusionScaleMode = mode % modes.size();
        m_occlusionScale = modes[ m_occlusionScaleMode ];     
        std::cout << "occlusion scale =  " << m_occlusionScale << std::endl; 

    }
    
    void cycleOcclusionScale() { setOcclusionScaleMode( m_occlusionScaleMode + 1 ); }

    /////

    void setOcclusionMode( int mode )
    {
        const std::vector< float > modes = 
        {                                            
            0.012,
            0.013,                 
            0.014,
            0.015,
            0.0175,  
            0.02,  
            0.03,  
            0.04,  
            0.06,
            0.07,            
            0.075,
            0.1,
            0.125,
            0.15,
            0.2
        };
        m_occlusionRadiusMode = mode % modes.size();
        m_occlusionRadius = modes[ m_occlusionRadiusMode ];  
        std::cout << "set occulusion radius =  " << m_occlusionRadius << std::endl;                   
    }
    void cycleOcclusionRadius() { setOcclusionMode( m_occlusionRadiusMode + 1 ); }

    /////

    void setOcclusionBiasMode( int mode )
    {
        const std::vector< float > modes = 
        {
            0.002,
            0.0025,            
            0.003,
            0.0035,
            0.004,  
            0.0045,          
            0.005,
            0.0055,
            0.006,
            0.0065,
            0.007,
            0.0075,
            0.008, 
            0.0085,             
            0.009,
            0.0095,
            0.010
        };
        m_occlusionBiasMode = mode % modes.size();
        m_occlusionBias = modes[ m_occlusionBiasMode ];  
        std::cout << "set occlusion bias " << m_occlusionBias << std::endl;                     
    }
    void cycleOcclusionBias() { setOcclusionBiasMode( m_occlusionBiasMode + 1 ); }

    /////

    void setDiffuseMode( int mode )
    {
        const std::vector< float > modes = 
        {  
            0.0,
            0.05,
            0.1,
            0.15,
            0.2,
            0.25,
            0.3,
            0.4,
            0.5,
            0.6,
            0.7,
            0.8,
            0.9,
            1 
        };
        m_diffuseMode = mode % modes.size();
        m_material.Kd_scale = modes[ m_diffuseMode ];  
        std::cout << "set diffuse " << m_material.Kd_scale << std::endl;
    }
    void cycleDiffuse() { setDiffuseMode( m_diffuseMode + 1 ); }

    //////

    void setSpecularMode( int mode )
    {
        const std::vector< float > modes = 
        {  
            0.0,
            0.025,
            0.05,
            0.0525,
            0.06,
            0.1,
            0.2,
            0.3,
            0.4,
            0.5,
            0.6,                                    
            1 
        };
        m_specularMode = mode % modes.size();
        m_material.Ks_scale = modes[ m_specularMode ];   

        std::cout << "set specular " << m_material.Ks_scale << std::endl; 
    }
    void cycleSpecular() { setSpecularMode( m_specularMode + 1 ); }

    /////

    void setSpecularExponentMode( int mode )
    {
        const std::vector< float > modes = 
        {  
            2.0,
            3.0,
            4.0,
            10.0,
            16.0,
            24.0,
            32.0,
            48.0,
            64.0,
            100.0,
            150.9
        };
        m_specularExponentMode = mode % modes.size();
        m_material.shininess = modes[ m_specularExponentMode ];     

        std::cout << "set specular exponent " << m_material.shininess << std::endl; 
    }
    void cycleSpecularExponent() { setSpecularExponentMode( m_specularExponentMode + 1 ); }

    /////

    void setAmbientMode( int mode )
    {
        const std::vector< float > modes = 
        {  
            1.0,
            0.975,
            0.95,
            0.925,
            0.9,
            0.88,
            0.85,
            0.8,
            0.75,
            0.7,
            0.65,
            0.6,
            0.5,
            0.4,
            0.3
        };
        m_ambientMode = mode % modes.size();
        m_material.Ka_scale = modes[ m_ambientMode ];         
        std::cout << "set ambient " << m_material.Ka_scale << std::endl;           
    }
    void cycleAmbient() { setAmbientMode( m_ambientMode + 1 ); }

    /////

    void setKernelSizeMode( int mode, bool remakeFrameBuffer = false )
    {
        const std::vector< int > modes = 
        {  
            8,
            16,
            32,
            64,
            128,
            256,
            512
        };
        m_kernelSizeMode = mode % modes.size();
        aoKernelSize = modes[ m_kernelSizeMode ];
        
        std::cout << "set aoKernelSize " << aoKernelSize << std::endl;

        if( remakeFrameBuffer )
        {
            createFrameBuffers( width() * m_multiSample, height() * m_multiSample );
        }          
    }
    void cycleKernelSize() { setKernelSizeMode( m_kernelSizeMode + 1, true ); }

    //

    void setMultiSampleMode( int mode, bool remakeFrameBuffer = false )
    {
        const std::vector< int > modes = 
        {  
            1,
            2
            //3
       //   4
        };
        m_multiSampleMode = mode % modes.size();
        m_multiSample = modes[ m_multiSampleMode ];

        if( remakeFrameBuffer )
        {
            createFrameBuffers( width() * m_multiSample, height() * m_multiSample );
        }   
    }
    void cycleMultiSample() { setMultiSampleMode( m_multiSampleMode + 1, true ); }

    //

    void setBackgroundMode( int mode )
    {
        const std::vector< TN::Vec3< float > > modes = 
        {
            { 0.1, 0.1, 0.1 },
            // { 0.95, 0.95, 0.95 },
            // { 0.9, 0.9, 0.9 },            
            // { 0.8, 0.8, 0.8 },
            // { 0.7, 0.7, 0.7 },
            // { 0.6, 0.6, 0.6 },
            // { 0.5, 0.5, 0.5 },
            // { 0.4, 0.4, 0.4 },
            // { 0.3, 0.3, 0.3 },
            { 0.0, 0.0, 0.2 }//,
            // { 0.1, 0.1, 0.1 },
            // { 0.0, 0.0, 0.0 }                               
        };
        
        m_backgroundMode = mode % modes.size();
        m_backgroundColor = modes[ m_backgroundMode ];       
    }
    void cycleBackground() { setBackgroundMode( m_backgroundMode + 1 ); }
    bool darkMode() const { return m_backgroundMode > 0; }

    ////////////////////

    void initRenderMode( int setting, bool setTF, bool forceUpdate = false )
    {
        m_renderMode = setting;

        if( setting == 0 )
        {
            setOcclusionMode       ( 13 );
            setOcclusionBiasMode   ( 15 );
            setOcclusionScaleMode  ( 0 );            
            setDiffuseMode         ( 7 );
            setSpecularMode        ( 6 );
            setSpecularExponentMode( 6 );            
            setAmbientMode         ( 6 );
            setMultiSampleMode     ( 1, false ); //m_multiSampleMode != 1 || forceUpdate );
            setKernelSizeMode      ( 4, forceUpdate );    
            setBackgroundMode      ( 0 );       

            if( setTF )
            {
                setTFMode( 42 );       
            }            
        }
        else if( setting == 1 )
        {
            setOcclusionMode       ( 14 );
            setOcclusionBiasMode   ( 4 );
            setOcclusionScaleMode  ( 0 );            
            setDiffuseMode         ( 0 );
            setSpecularMode        ( 7 );
            setSpecularExponentMode( 8 );            
            setAmbientMode         ( 5 );
            setMultiSampleMode     ( 1, false ); //m_multiSampleMode != 1 || forceUpdate );
            setKernelSizeMode      ( 4, forceUpdate );         
            setBackgroundMode      ( 0 );            

            if( setTF )
            {
                setTFMode( 42 );       
            }                   
        }
    }

    void cycleRenderMode()
    {
        initRenderMode( ( m_renderMode + 1 ) % 2, false, true );
    }

    void printCamera()
    {
        m_camera.print();
    }

    void setCameraOrientation( glm::vec3 eye, glm::vec3 center, glm::vec3 up, float zoom )
    {
        m_camera.set( eye, center, up, zoom );
    }

    void setGlyphTexture( const std::string & tex )
    {
        m_glyphTexPath = tex;   
        loadGlyphTexture( tex );
    }

    void setGlyph( const std::string & glyph )
    {
        m_glyphPath    = glyph;        
        loadGlyph( glyph );
    }

    void setGlyph( const std::string & glyph, const std::string & tex )
    {
        m_glyphPath    = glyph;
        m_glyphTexPath = tex;
        loadGlyph( glyph, tex );
    }

    void getGlyphPreviewData( std::vector<std::array< std::uint8_t, 3 > > & data, int & width, int & height )
    {
        const std::string key = "glyph_preview";
        if( ! m_managedViewTextures.count( key ) )
        {
            // std::cerr << "Error: RenderModule::getGlyphPreviewData, not glyph preview was created." << std::endl;

            width  = 256;
            height = 256;
            
            data.resize( width * height );
            for( size_t i = 0; i < width*height; ++i )
            {
                data[ i ] = { 255, 255, 255 };
            }
        }
        else
        {
            auto texIndex = m_managedViewTextures.at( key );
            auto texId = m_viewTextures[ texIndex ];

            glBindTexture( GL_TEXTURE_2D, texId );
            
            GLint w, h, internalFormat;

            glGetTexLevelParameteriv( GL_TEXTURE_2D, 0,  GL_TEXTURE_COMPONENTS, & internalFormat );
            glGetTexLevelParameteriv( GL_TEXTURE_2D, 0,  GL_TEXTURE_WIDTH,  & width );
            glGetTexLevelParameteriv( GL_TEXTURE_2D, 0,  GL_TEXTURE_HEIGHT, & height );

            data.resize( width * height );

            glGetTexImage( 
                GL_TEXTURE_2D,
                0,
                internalFormat,
                GL_UNSIGNED_BYTE,
                data.data() );
        }
    }
};

}

#endif // RENDERWINDOW_H
