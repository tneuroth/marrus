#ifndef TN_GL_ERROR_CHECKING_HPP
#define TN_GL_ERROR_CHECKING_HPP

#include <glm/glm.hpp>
#include <string>
#include <sstream>
#include <stdexcept>
#include <iostream>
#include <vector>

#define GL_CHECK_CALL_ASSIGN(call) \
    ([&]() { \
        decltype(call) result = call; \
        checkCallErrors(#call, __FILE__, __LINE__); \
        return result; \
    }())

#define GL_CHECK_CALL(call)                                                      \
    do {                                                                    \
        call;                                                               \
        checkCallErrors(#call, __FILE__, __LINE__);                             \
    } while (0)

static inline std::string GetGLErrorStr(GLenum err)
{
    switch (err)
    {
        case GL_NO_ERROR:          return "GL_NO_ERROR: No error has been recorded";
        case GL_INVALID_ENUM:      return "GL_INVALID_ENUM: An unacceptable value is specified for an enumerated argument";
        case GL_INVALID_VALUE:     return "GL_INVALID_VALUE: A numeric argument is out of range";
        case GL_INVALID_OPERATION: return "GL_INVALID_OPERATION: The specified operation is not allowed in the current state";
        case GL_STACK_OVERFLOW:    return "GL_STACK_OVERFLOW: An operation would cause a stack overflow";
        case GL_STACK_UNDERFLOW:   return "GL_STACK_UNDERFLOW: An operation would cause a stack underflow";
        case GL_OUT_OF_MEMORY:     return "GL_OUT_OF_MEMORY: There is not enough memory left to execute the command";
        case GL_INVALID_FRAMEBUFFER_OPERATION: 
            return "GL_INVALID_FRAMEBUFFER_OPERATION: The framebuffer object is not complete";
        default:                   return "UNKNOWN_ERROR: An unrecognized error occurred";
    }
}

static inline void checkCallErrors(const char* call, const char* file, int line) {
    
    GLenum error;
    while ( (error = glGetError() ) != GL_NO_ERROR) {

    	auto errorString = GetGLErrorStr( error );

        std::cerr << "OpenGL error (" << errorString << ") after " << call
                  << " at " << file << ":" << line << std::endl;
    }
}

class InputValidator {

public:

	// Validate finite value
	template<typename T>
	static bool isFinite(const T& value) {
	    if constexpr (std::is_floating_point_v<T>) {
	        return std::isfinite(value);
	    }
	    return true;
	}

	// Validate vector of values
	template<typename T>
	static bool validateVector(const std::vector<T>& vec, 
	                           size_t minSize = 1) {
	    if (vec.empty()) {
	        return false;
	    }

	    if (vec.size() < minSize) {
	        return false;
	    }

	    // Validate each element is finite
	    for (const auto& elem : vec) {
	        if (!isFinite(elem)) {
	            return false;
	        }
	    }

	    return true;
	}

	template<typename VecType>
	static bool validateRange(const VecType& range) {
	    // Validate finite values
	    if (! isFinite(range.a()) ||
	        ! isFinite(range.b())) 
	    {
	    	std::cerr << "Error: Invalid primitive type." << std::endl;
	        return false;
	    }

	    // Validate range is valid
	    return range.b() > range.a();
	}

	static bool validatePrimitiveType(unsigned int type) {

		if( type != GL_POINTS 
		 && type != GL_LINES  
		 && type != GL_LINE_STRIP  
		 && type != GL_LINE_LOOP  
		 && type != GL_TRIANGLES  
		 && type != GL_TRIANGLE_STRIP  
		 && type != GL_TRIANGLE_FAN )
		{
			std::cerr << "Error: Invalid primitive type." << std::endl;
		    return false;
		}

	    return true;
	}

	// Get minimum vertices required for a primitive type
	static size_t getMinVerticesForType(unsigned int type) {
	    switch (type) {
	        case GL_POINTS:
	            return 1;
	        case GL_LINES:
	        case GL_LINE_STRIP:
	            return 2;
	        case GL_LINE_LOOP:
	        case GL_TRIANGLES:
	        case GL_TRIANGLE_STRIP:
	        case GL_TRIANGLE_FAN:
	            return 3;
	        default:
	            return std::numeric_limits<size_t>::max(); // Invalid type
	    }
	}

	// Validate color components
	template<typename ColorType>
	static bool validate(const ColorType& color) {
	    return isFinite(color.r) &&
	           isFinite(color.g) &&
	           isFinite(color.b) &&
	           isFinite(color.a);
	}
};

#endif