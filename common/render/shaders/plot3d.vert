#version 330 core

layout(location = 0) in vec3 pos;
layout(location = 1) in vec4 col;

out vec4 color;

uniform mat4 TR;

void main(void)
{
    vec4 pos = TR * vec4(
        pos.x,
        pos.y,
        pos.z,
        1.0
    );

    color = col;
    gl_Position = pos;
}
