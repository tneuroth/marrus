#version 410

uniform mat4 modelView;
uniform mat4 mvp;
uniform mat3 normalMatrix;

layout ( location = 0 ) in vec3 posAttr;
layout ( location = 1 ) in vec3 normalAttr;
layout ( location = 2 ) in vec3 offset;
layout ( location = 3 ) in float c;
layout ( location = 4 ) in float v_filter;

uniform float xScale;
uniform float yScale;
uniform float zScale;

out vec3  normal;
out vec3  position;
out vec3  eye;

out float scalar;
flat out float f;

void main(void)
{
    float scale = 100;
    
    if( v_filter > 0.5 )
    {
        scale = 1.25;
    }

    vec3 p = vec3( 
     	offset.x + posAttr.x * yScale*scale, 
    	offset.y + posAttr.y * xScale*scale, 
    	offset.z + posAttr.z * zScale*scale );

    position = vec4( modelView * vec4( p, 1.0 ) ).xyz;
    normal = normalize( normalMatrix * normalAttr );
    eye = normalize( -vec4( modelView * vec4( p, 1.0 ) ).xyz );
    scalar = c;
    f = v_filter;

    gl_Position = mvp * vec4( p, 1.0 );
}

