#version 410

uniform mat4 modelView;
uniform mat4 mvp;
uniform mat3 normalMatrix;

layout (location = 0) in vec3 posAttr;
out vec3  position;

void main(void)
{
    vec3 p = posAttr.xyz;
    position = vec4( modelView * vec4( p, 1.0 ) ).xyz;
    gl_Position = mvp * vec4( p, 1.0 );
}

