#version 410

#define COLOR_MODE_SOLID   0
#define COLOR_MODE_SCALAR  1
#define COLOR_MODE_TEXTURE 2

#define ORIENTATION_MODE_NONE 0
#define ORIENTATION_MODE_VIEW 1
#define ORIENTATION_MODE_NORM 2

layout ( location = 0 ) in vec3 posAttr;
layout ( location = 1 ) in vec3 normalAttr;
layout ( location = 2 ) in vec2 uv;
layout ( location = 3 ) in vec3 offset;
layout ( location = 4 ) in vec3 u;
layout ( location = 5 ) in vec3 v;
layout ( location = 6 ) in vec3 n;
layout ( location = 7 ) in float scalarValue;

uniform mat4 model;
uniform mat4 view;
uniform mat4 modelView;
uniform mat4 mvp;
uniform mat3 normalMatrix;

uniform int colorMode;
uniform int orientationMode;
uniform float scale;

out V2G
{
    vec3 offset;
    vec3 normal;
    vec2 uv;
    vec3 u;
    vec3 v;
    vec3 n;
    float scalar;
} v2g;
void main(void)
{
    v2g.normal = normalAttr;
    v2g.uv     = uv;
    v2g.offset = offset;
    v2g.u      = u;
    v2g.v      = v;
    v2g.n      = n;
    v2g.scalar = scalarValue;

    gl_Position = vec4( posAttr, 1.0 );
}

