#version 330 core

#define COLOR_MODE_SOLID   0
#define COLOR_MODE_SCALAR  1
#define COLOR_MODE_TEXTURE 2
#define COLOR_MODE_TEX_BUF 3
#define FRAG_IS_NOT_BIN   -1

layout (location = 0) out vec3 gPosition;
layout (location = 1) out vec3 gNormal;
layout (location = 2) out vec3 gAlbedo;

in vec3 normal;
in vec3 position;
in vec2 tex_coord;
in float scalar;
flat in float objScale;
flat in int instanceID;

uniform sampler2D textureImage;
uniform sampler1D tf;

uniform vec2 scalarMappingRange;
uniform int colorMode;

uniform vec3 solidColor;

uniform samplerBuffer bufferTex;
uniform int nBins;

int colorToBinIdx( vec3 color )
{
    return 0;
}

// 0, 0,255
// 64,0,255
// 2,0,255

void main()
{    
    gPosition = position;
    gNormal = normalize( normal );
    
    if( colorMode == COLOR_MODE_SCALAR )
    {
        float rw = ( scalarMappingRange.y - scalarMappingRange.x );
        float v  = clamp( ( scalar - scalarMappingRange.x ) / rw, 0.0, 1.0 );
        gAlbedo.rgb = texture( tf, v ).rgb;
    }
    else if( colorMode == COLOR_MODE_TEXTURE )
    {
        vec3 texColor = texture( textureImage, tex_coord ).rgb;
        gAlbedo.rgb   = texColor;
    }
    else if( true ) //colorMode == COLOR_MODE_TEX_BUF )
    {
        vec3 texColor  = texture( textureImage, tex_coord ).rgb;

        // Translate the template's texture color to a bin index
        int binIdx   = colorToBinIdx( texColor );

        // Fragmnet isn't part of the glyph that isn't color encoding a bin value
        if( binIdx == FRAG_IS_NOT_BIN )
        {
            gAlbedo.rgb = texColor;
        }
        else 
        {
            // Get the bin value from the buffer texture

            int   texIndex = instanceID * nBins + binIdx;
            float binValue = texelFetch( bufferTex, texIndex ).r;

            // color map the bin value
            float rw = ( scalarMappingRange.y - scalarMappingRange.x );
            float v  = clamp( ( binValue - scalarMappingRange.x ) / rw, 0.0, 1.0 );
            gAlbedo.rgb = texture( tf, v ).rgb;
        }
    } 
    else // solid
    {
        gAlbedo.rgb = solidColor;
    }
}
