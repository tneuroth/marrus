#version 410

layout ( location = 0 ) in vec3 posAttr;
layout ( location = 1 ) in vec3 normalAttr;
layout ( location = 2 ) in vec2 uv;
layout ( location = 3 ) in vec3 offset;
layout ( location = 4 ) in vec3 u;
layout ( location = 5 ) in vec3 v;
layout ( location = 6 ) in vec3 n;
layout ( location = 7 ) in float scalarValue;

uniform mat4 model;
uniform mat4 view;
uniform mat4 proj;

uniform mat4 modelView;
uniform mat4 mvp;
uniform mat3 normalMatrix;

uniform mat4 gT;
uniform mat4 gS; 

uniform vec3 gt;
uniform vec3 gs; 

uniform samplerBuffer bufferTex;
uniform int nAxes;
uniform int nComp;

uniform int nInstances;

uniform int coneOffset;
uniform int cylinderOffset;
uniform int coneBaseOffset;

uniform vec2 vector_lims;

uniform int orientationMode;
uniform float scale;

uniform int scaleMode;

uniform bool invert;

out vec3 fragPosition;
out vec3 normal;
out vec3 position;
out vec3 eye;
out vec3 vColor;

flat out float objScale;

out vec2 tex_coord;
out float scalar;
flat out int instanceID;

#define MAX_VECTORS 10
uniform vec3 vectorColors[ MAX_VECTORS ];

#define MAX_VECTORS 10
uniform int activeVectors[ MAX_VECTORS ];

#define FLT_MIN 1.175494351e-38

vec3 projectOntoPlane(vec3 v, vec3 planeNormal) {
    vec3 normalizedNormal = normalize(planeNormal);
    float dotProduct = dot(v, normalizedNormal);
    vec3 projection = v - dotProduct * normalizedNormal;
    return projection;
}

mat4 computeOrientationMatrix(vec3 up, vec3 right) {
    // Ensure 'right' is orthogonal to 'up'
    right = normalize(projectOntoPlane(right, up));
    
    // Compute the forward vector
    vec3 forward = normalize(cross(right, up));

    // Normalize 'up' to ensure it’s a unit vector
    up = normalize(up);

    // Construct the orientation matrix
    mat4 orientationMatrix;
    orientationMatrix[0] = vec4(right, 0.0);
    orientationMatrix[1] = vec4(up, 0.0);       // Changed from -up to up
    orientationMatrix[2] = vec4(forward, 0.0);
    orientationMatrix[3] = vec4(0.0, 0.0, 0.0, 1.0);

    return orientationMatrix;
}


float scaleValue(float value, float minVal, float maxVal, int mode) {

    value = clamp(value, minVal, maxVal);

    float normalized = (value - minVal) / (maxVal - minVal);

    if (mode == 0) 
    { 
        return normalized;
    } 
    else if (mode == 1 ) 
    {
        return  sqrt( normalized );
    } 
    else if (mode == 2) {
        
        float logMin   = log( max( minVal + FLT_MIN, FLT_MIN ) );
        float logMax   = log( max( maxVal + FLT_MIN, FLT_MIN ) );
        float logValue = log( max( value  + FLT_MIN, FLT_MIN ) );

        return (logValue - logMin ) / ( logMax - logMin );
    }

    return normalized;
}


vec3 colorPalette[11] = vec3[11](
    vec3(255.0/255.0, 150.0/255.0, 0.0    ),  // Blue
    vec3(0.0 / 155.0, 158.0 / 255.0, 115.0 / 255.0  ), // Light Green
    vec3(204 / 255.0, 121 / 255.0, 167 /  255.0  ),    // Mauve
    vec3(0.8, 0.47, 0.65  ),  // Pink
    vec3(0.8, 0.4, 0.0),     // Brown-Orange
    vec3(0.94, 0.89, 0.26), // Yellow
    vec3(0.35, 0.35, 0.35), // Gray
    vec3(0.0, 0.0, 0.8),    // Dark Blue
    vec3(0.87, 0.56, 0.0),   // Orange
    vec3(0.0, 0.62, 0.45),  // Green
    vec3(0.3, 0.3, 0.3 ) //black
);


void main(void)
{   
    ////////////////////////////////////////

    int vector_id = ( gl_InstanceID % nAxes );
    bool showVector = activeVectors[ vector_id ] == 1;

    if( ! showVector )
    {
        vColor   = vec3(0,0,0);
        position = vec4( 10000, 10000, 10000, 1 ).xyz;
        normal   = vec3( 1, 0, 0 );
        eye      = vec3( 1, 0, 0 );
        scalar = 1;
        tex_coord = uv;
        instanceID = gl_InstanceID;
        gl_Position =  vec4( 10000, 10000, 10000, 1 );

        return;
    }

    vec3  vector = vec3( 
        texelFetch( bufferTex, ( gl_InstanceID / 3 ) * nAxes*nComp + nComp*vector_id + 0 ).r,
        texelFetch( bufferTex, ( gl_InstanceID / 3 ) * nAxes*nComp + nComp*vector_id + 1 ).r,
        texelFetch( bufferTex, ( gl_InstanceID / 3 ) * nAxes*nComp + nComp*vector_id + 2 ).r );

    float magnitude  = length(          vector );
    vec3  direction  = normalize(       vector );
    float YS         = scaleValue( magnitude, vector_lims.x, vector_lims.y, scaleMode ) * 5;

    vec3 up = direction;
    bool facingIntoMesh = invert && dot( up, -n ) < 0.0;

    if( facingIntoMesh )
    {
        up = -up;
    }

    vec3 right = vec3( 0.0, 0.0, 1.0 );

    bool inCone     = gl_VertexID < cylinderOffset || gl_VertexID >= coneBaseOffset;
    bool inCylinder = gl_VertexID >= cylinderOffset && gl_VertexID < coneBaseOffset;

    float SCALE = scale * 1.2;

    vec4 pos = vec4( 
        posAttr.x * SCALE, 
        posAttr.y * SCALE, 
        posAttr.z * SCALE, 
        1.0 );

    if( facingIntoMesh )
    {
        if( ! inCone )
        {
            pos = vec4( -pos.xyz, 1 );
        }
    }

    if( ! inCone )
    {
        pos.y = pos.y * YS;
    }
    else
    {
        pos.x = pos.x*2.0;
        pos.z = pos.z*2.0;
        pos.y = pos.y*2.0;
    }

    fragPosition = posAttr;

    mat4 OM = computeOrientationMatrix( direction, right );
    pos = OM * pos; 
    vec3 transformedNormal = normalize( OM * vec4(normalAttr, 0.0)).xyz;
    pos = vec4( vec3( pos.xyz + offset.xyz ), 1.0 );

    if( inCone )
    {
       pos = vec4( pos.xyz + up*SCALE*YS*1.01, 1.0 ); 
    }

    /////////////////////////////////////////////////////////////////////////////

    vec3 delta = vec3( 0, 5.0, 0 );

    vec4 a = mvp * vec4( offset.xyz + delta, 1 );
    vec4 b = mvp * vec4( offset.xyz - delta, 1 );

    a = a / a.w;
    b = b / b.w;

    objScale = abs( a.y - b.y );

    //////////////////////////////////////////////////////////////////////////////

    vColor = colorPalette[ vector_id ];

    position = vec4( modelView * pos ).xyz;
    normal   = normalAttr;
    eye      = normalize( -vec4( modelView * pos ).xyz );

    scalar = scalarValue;
    tex_coord = uv;

    instanceID = gl_InstanceID;
    gl_Position = vec4( proj*view*model * pos );

}

