#version 410

#define COLOR_MODE_SOLID   0
#define COLOR_MODE_SCALAR  1
#define COLOR_MODE_TEXTURE 2

#define ORIENTATION_MODE_NONE 0
#define ORIENTATION_MODE_VIEW 1
#define ORIENTATION_MODE_NORM 2

layout ( location = 0 ) in vec3 posAttr;
layout ( location = 1 ) in vec3 normalAttr;
layout ( location = 2 ) in vec2 uv;
layout ( location = 3 ) in vec3 offset;
layout ( location = 4 ) in vec3 u;
layout ( location = 5 ) in vec3 v;
layout ( location = 6 ) in vec3 n;
layout ( location = 7 ) in float scalarValue;

uniform mat4 model;
uniform mat4 view;
uniform mat4 modelView;
uniform mat4 mvp;
uniform mat3 normalMatrix;

uniform int colorMode;
uniform int orientationMode;
uniform float scale;

out vec3  normal;
out vec3  position;
out vec3  eye;

flat out float objScale;

out vec2 tex_coord;
out float scalar;
flat out int instanceID;

vec3 projectOntoPlane(vec3 v, vec3 planeNormal) {
    vec3 normalizedNormal = normalize(planeNormal);
    float dotProduct = dot(v, normalizedNormal);
    vec3 projection = v - dotProduct * normalizedNormal;
    return projection;
}

mat4 computeOrientationMatrix(vec3 up, vec3 right) {
    // Ensure 'right' is orthogonal to 'up'
    right = normalize(projectOntoPlane(right, up));
    
    // Compute the forward vector
    vec3 forward = normalize(cross(right, up));

    // Normalize 'up' to ensure it’s a unit vector
    up = normalize(up);

    // Construct the orientation matrix
    mat4 orientationMatrix;
    orientationMatrix[0] = vec4(right, 0.0);
    orientationMatrix[1] = vec4(up, 0.0);       // Changed from -up to up
    orientationMatrix[2] = vec4(forward, 0.0);
    orientationMatrix[3] = vec4(0.0, 0.0, 0.0, 1.0);

    return orientationMatrix;
}

void main(void)
{   
    vec3 up    = n;
    vec3 right = u;

    vec4 pos = vec4(
        posAttr.x * scale,
        posAttr.y * scale,
        posAttr.z * scale, 1.0 );
    
    vec3 transformedNormal;

    if( orientationMode == ORIENTATION_MODE_NORM )
    {
        mat4 OM = computeOrientationMatrix( up, right );
        pos = OM * pos; 

        transformedNormal = normalize(OM * vec4(normalAttr, 0.0)).xyz;

    }
    else if( orientationMode == ORIENTATION_MODE_VIEW )
    {
        // tilt the model along the x-axis so the up vector is now on the negative z-axis instead of the y-axis.
        mat4 OM = computeOrientationMatrix( vec3( 0, 0, -1 ), vec3( 0, -1, 0 ) );
        pos = OM * pos; 

        // Extract the rotation part of the view matrix (3x3)
        mat3 viewRotation = mat3( view );

        // Invert the rotation part to make the model face the camera
        mat3 billboardRotation = transpose( viewRotation );

        // Apply the billboard rotation
        pos.xyz = billboardRotation * pos.xyz;

        transformedNormal = normalize(billboardRotation * (OM * vec4(normalAttr, 0.0)).xyz);
    }
    else
    {
         transformedNormal = normalize(normalMatrix * normalAttr);
    }

    pos = vec4( vec3( pos.xyz + offset.xyz ), 1.0 );

    /////////////////////////////////////////////////////////////////////////////

    vec3 delta = vec3( 0, 5.0, 0 );

    vec4 a = mvp * vec4( offset.xyz + delta, 1 );
    vec4 b = mvp * vec4( offset.xyz - delta, 1 );

    a = a / a.w;
    b = b / b.w;

    objScale = abs( a.y - b.y );

    //////////////////////////////////////////////////////////////////////////////

    position = vec4( modelView * pos ).xyz;
    normal   = transformedNormal;
    eye      = normalize( -vec4( modelView * pos ).xyz );

    if( colorMode == COLOR_MODE_SCALAR )
    {
        scalar = scalarValue;
    }
    if( colorMode == COLOR_MODE_TEXTURE )
    {
        tex_coord = uv;
    }

    instanceID = gl_InstanceID;
    gl_Position = vec4( mvp * pos );
}

