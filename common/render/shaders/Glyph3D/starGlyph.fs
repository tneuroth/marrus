#version 330 core

layout (location = 0) out vec3 gPosition;
layout (location = 1) out vec3 gNormal;
layout (location = 2) out vec3 gAlbedo;

// NOTES: might try distance from line to get better borders/outlines

in vec3 fragPosition;

in vec3 normal;
in vec3 position;
in vec2 tex_coord;

in float scalar;
flat in float objScale;
flat in int instanceID;

uniform sampler2D textureImage;
uniform sampler1D tf;

uniform vec2 scalarMappingRange;

uniform samplerBuffer bufferTex;

uniform int scaleMode;
uniform int perGlyphNormalization;
uniform int perSelBinNormalization;
uniform bool normalizeFromZero;

uniform int myBufferSize;
uniform int nBins;

uniform int nAxes;
uniform int nComp;

uniform sampler1D axes_tfs;

#define MAX_AXIS 16
uniform vec2 axes_lims[   MAX_AXIS ];
uniform int axes_indices[ MAX_AXIS ];
uniform int n_active_axes;

#define MAX_COMPS 16
uniform vec3 componentColors[ MAX_COMPS ];
uniform int activeComponent;
uniform bool useComponentColors;
uniform bool showBorders;
uniform bool allAxisNormalization;

#define TWO_PI_INV 0.15915494309
#define TWO_PI 6.28318530718
#define PI 3.14159265359

#define FLT_MIN 1.175494351e-38

// ///////////////////////////////////////////////////

bool pointInWedge( vec2 p, vec2 v1, vec2 v2 ) 
{
    return (v2.x - v1.x) * (p.y - v1.y) - (v2.y - v1.y) * (p.x - v1.x) > 0.0;
}

float distanceToLine( vec2 point, vec2 lineStart, vec2 lineEnd ) 
{
    vec2 lineDir = lineEnd - lineStart;      // Direction of the line
    vec2 pointToLineStart = point - lineStart;
    
    // Compute the perpendicular distance
    float areaTwice = abs(lineDir.x * pointToLineStart.y - lineDir.y * pointToLineStart.x);
    float lineLength = length(lineDir);
    
    return areaTwice / lineLength;           // Distance from point to line
}

int axisId( const float s )
{
    return  min( n_active_axes - 1, int( floor( s * n_active_axes * TWO_PI_INV ) ) );
}

int compId( const float r )
{
    return  min( nComp - 1, int( floor( r * nComp ) ) );
}

float scaleValue(float value, float minVal, float maxVal, int mode) {

    value = clamp( value, minVal, maxVal );

    float c = FLT_MIN;
    float normalized = (value - minVal) / (maxVal - minVal);

    if (mode == 0) 
    { 
        return normalized;
    } 
    else if (mode == 1 ) 
    {
        return  sqrt( normalized + c );
    } 
    else if (mode == 2) {
        
        float logMin   = log( max( minVal + c, c ) );
        float logMax   = log( max( maxVal + c, c ) );
        float logValue = log( max( value  + c, c ) );

        return (logValue - logMin ) / ( logMax - logMin );
    }

    return normalized;
}


///////////////////////////////////////////////////////

vec3 colorPalette[11] = vec3[11](
    vec3(0.3, 0.3, 0.3 ), //black
    vec3(0.99607843, 0.38039216, 0.0), // Yellow
    vec3(0.8, 0.47, 0.65),  // Pink
    vec3(0.57, 0.82, 0.31), // Light Green
    vec3(0.8, 0.4, 0.0),     // Brown-Orange
    vec3(0.8, 0.6, 0.7),    // Mauve
    vec3(0.35, 0.35, 0.35), // Gray
    vec3(0.0, 0.0, 0.8),    // Dark Blue
    vec3(0.0, 0.45, 0.70),  // Blue
    vec3(0.87, 0.56, 0.0),   // Orange
    vec3(0.0, 0.62, 0.45)  // Green
);


void main()
{    
    gPosition = position;
    // gNormal   = vec3( 0.0, 0.0, 0.0 );
    gNormal   = normalize( normal );
    gAlbedo.rgb = vec3( 1,1,1 );

    //////////////////////////////////////////////////////////

    // Visual Parameters

    //////////////////////////////////////////////////////////

    vec3 gridColor = vec3( 0.7,0.7,0.7 );
    vec3 axisColor = vec3( 0.7,0.7,0.7 );
    vec3 edgeColor = vec3( 0.7,0.7,0.7 );
    vec3 markColor = vec3( 0.2,0.2,0.2 );

    float gridLineWidth = 0.02;
    int   nGridLines    = 4;
    float markSize      = 0.175;

    bool colorCoded = true;

    ///////////////////////////////////////////////////////////

    vec3 polygonColor = useComponentColors ? componentColors[ activeComponent ] : vec3( 0.2, 0.2, 0.2 );

    float radius  = length( fragPosition );
    float angle   = atan( fragPosition.x, fragPosition.z ) + PI;
    float d_theta = TWO_PI / n_active_axes;
    float W       = round( angle / d_theta );
    float A_ID    = int(W) % n_active_axes;
    float WA      = W * d_theta;
    float A_INDEX = axes_indices[ int( A_ID ) ];

    ///////////////////////////////////////////////////////////

    bool onEdge     = radius > 1.0 - gridLineWidth;
    bool onOrigin   = radius < 0.08;
    bool onAxis     = abs( WA - angle ) * radius < 0.0175;
    bool onMark = radius > 1.0 - ( markSize - gridLineWidth ) 
               && abs( WA - angle ) * radius < ( markSize - gridLineWidth*2 ) / 2;

    bool onMarkEdge = ! onMark && ( radius > 1.0 - markSize && abs( WA - angle ) * radius < markSize / 2 );

    ///////////////////////////////////////////////////////////

    if( onEdge && showBorders )
    {
        gAlbedo.rgb = edgeColor;
    }

    if( onOrigin && showBorders )
    {
        gAlbedo.rgb = vec3(0.1,0.1,0.1);
        return;
    }

    if( showBorders) {
        float dr = 1.0 / nGridLines;
        for( int i = 0; i < nGridLines; ++i )
        {
            if( radius > i * dr - gridLineWidth 
             && radius < i * dr + gridLineWidth  )
            {
                gAlbedo.rgb = gridColor;
            }
        }
    }

    if ( onAxis && showBorders )
    {
        gAlbedo.rgb = vec3( 0.7,0.7,0.7 );
    }

    int axis_i = axisId( angle );
    int axis_j = ( axis_i + 1 ) % n_active_axes;

    float angle_i = axis_i * d_theta;
    float angle_j = axis_j * d_theta;

    vec2 A = vec2( cos( angle_i ), sin( angle_i ) );
    vec2 B = vec2( cos( angle_j ), sin( angle_j ) );
    vec2 C = radius * vec2( cos(   angle ), sin(   angle ) );

    if( colorCoded && showBorders )
    {
        if( onMark )
        {
            //float TF_OFFSET = ( A_INDEX * 256.0 + 128 ) / ( nAxes * 256.0 );
            gAlbedo.rgb = colorPalette[ int( A_INDEX ) ];//texture( axes_tfs, TF_OFFSET ).rgb;
        }
        else if( onMarkEdge )
        {
            gAlbedo.rgb = markColor;
        }
    }
    else if( A_ID == 0 && showBorders ) 
    {
        if( onMark )
        {
            gAlbedo.rgb = markColor;
        }
    }

    int idx_i = instanceID*nAxes*nComp + nComp*axes_indices[ axis_i ] + activeComponent;
    int idx_j = instanceID*nAxes*nComp + nComp*axes_indices[ axis_j ] + activeComponent;

    float val_i = texelFetch( bufferTex, idx_i ).r;
    float val_j = texelFetch( bufferTex, idx_j ).r;

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    vec2 lims_i = axes_lims[ axes_indices[ axis_i ] ];
    vec2 lims_j = axes_lims[ axes_indices[ axis_j ] ];

    vec2 allAxisLims = lims_i;

    if( allAxisNormalization )
    {

        float maxSum = 0;
        allAxisLims = vec2( 999999.9, 0.0 );

        for( int i = 0; i < n_active_axes; ++i )
        {
            int a_idx = axes_indices[ i ];
            vec2 a_lims = axes_lims[ a_idx ];
            
            allAxisLims.x = min( allAxisLims.x, a_lims.x );
            allAxisLims.y = max( allAxisLims.y, a_lims.y );

            float sum = 0.0;
            for( int c_idx = 0; c_idx < nComp; c_idx++ )
            {
                int IDX = instanceID*nAxes*nComp + nComp*a_idx + c_idx;
                sum += texelFetch( bufferTex, IDX ).r;
            }

            maxSum = max( maxSum, sum );
        }

        if( perGlyphNormalization != 0 && maxSum > 0 )
        {
            val_i /= maxSum;
            val_j /= maxSum; 
        }
    }
    else if( perGlyphNormalization != 0 )
    {
        float sum_i = 0.0;
        float sum_j = 0.0;

        for( int c_idx = 0; c_idx < nComp; c_idx++ )
        {
            int idx_i_c = instanceID*nAxes*nComp + nComp*axes_indices[ axis_i ] + c_idx;
            int idx_j_c = instanceID*nAxes*nComp + nComp*axes_indices[ axis_j ] + c_idx;

            sum_i += texelFetch( bufferTex, idx_i_c ).r;
            sum_j += texelFetch( bufferTex, idx_j_c ).r;
        }

        if( sum_i > 0 )
        {
            val_i /= sum_i;
        }

        if( sum_j > 0 )
        {
            val_j /= sum_j;
        }
    }

    if( normalizeFromZero )
    {
        lims_i.x = 0;
        lims_j.x = 0;
        allAxisLims.x = 0.0;
    }

    if( allAxisNormalization )
    {
        val_i = scaleValue( val_i, lims_i.x, lims_i.y, scaleMode );
        val_j = scaleValue( val_j, lims_j.x, lims_j.y, scaleMode );
    }
    else
    {
        val_i = scaleValue( val_i, lims_i.x, lims_i.y, scaleMode );
        val_j = scaleValue( val_j, lims_j.x, lims_j.y, scaleMode );
    }

    val_i = 0.08 + val_i*0.92;
    val_j = 0.08 + val_j*0.92;

    bool insideWedge     = pointInWedge(   C, val_i*A, val_j*B );
    bool onWedgeOutline  = distanceToLine( C, val_i*A, val_j*B ) < 0.015;

    if( ( onAxis && insideWedge && showBorders ) || onWedgeOutline && showBorders )
    {
        gAlbedo.rgb = ( polygonColor * 3 + vec3( 0, 0, 0 ) ) / 4;
        return; 
    }
    else if( insideWedge )
    {
        gAlbedo.rgb = polygonColor;
        return;
    }
}

