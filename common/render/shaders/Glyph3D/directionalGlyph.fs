#version 330 core

layout (location = 0) out vec3 gPosition;
layout (location = 1) out vec3 gNormal;
layout (location = 2) out vec3 gAlbedo;

in vec3 fragPosition;

in vec3 normal;
in vec3 position;
in vec2 tex_coord;
in vec3 vColor;

in float scalar;
flat in float objScale;
flat in int instanceID;

uniform sampler2D textureImage;
uniform sampler1D tf;

uniform vec2 scalarMappingRange;
uniform int colorMode;

uniform vec3 solidColor;

uniform samplerBuffer bufferTex;

uniform int myBufferSize;
uniform int nBins;

uniform int nAxes;
uniform int nComp;

#define MAX_AXIS 10
uniform sampler1D axes_tfs;
uniform vec2 axes_lims[ MAX_AXIS ];

void main()
{    
    gPosition = position;
    gNormal = normalize( normal );
    
    vec3 texColor = gAlbedo.rgb = texture( textureImage, tex_coord ).rgb;

    if( true ) //texColor.g > 0.9 )
    {
        if( texColor.b > 0.9  )
        {
            gAlbedo.rgb = vec3(1,1,1); 
        }
        else
        {
            gAlbedo.rgb = vColor; 
        }
    }
    else
    {
        gAlbedo.rgb = texture( textureImage, tex_coord ).rgb;
    }
}
