#version 410

#define ORIENTATION_MODE_NONE 0
#define ORIENTATION_MODE_VIEW 1
#define ORIENTATION_MODE_NORM 2

layout ( location = 0 ) in vec3 posAttr;
layout ( location = 1 ) in vec3 normalAttr;
layout ( location = 2 ) in vec2 uv;
layout ( location = 3 ) in vec3 offset;
layout ( location = 4 ) in vec3 u;
layout ( location = 5 ) in vec3 v;
layout ( location = 6 ) in vec3 n;
layout ( location = 7 ) in float scalarValue;

uniform mat4 model;
uniform mat4 view;
uniform mat4 proj;

uniform mat4 modelView;
uniform mat4 mvp;
uniform mat3 normalMatrix;

uniform mat4 gT;
uniform mat4 gS; 

uniform vec3 gt;
uniform vec3 gs; 

uniform int orientationMode;
uniform float scale;

out vec3 fragPosition;
out vec3 normal;
out vec3 position;
out vec3 eye;

flat out float objScale;

out vec2 tex_coord;
out float scalar;
flat out int instanceID;

vec3 projectOntoPlane(vec3 v, vec3 planeNormal) {
    vec3 normalizedNormal = normalize(planeNormal);
    float dotProduct = dot(v, normalizedNormal);
    vec3 projection = v - dotProduct * normalizedNormal;
    return projection;
}

mat4 computeOrientationMatrix(vec3 up, vec3 right) {

    // Ensure 'right' is orthogonal to 'up'
    // right = normalize(projectOntoPlane(right, up));

    vec3 forward = normalize( cross( right, up ) );
    up = normalize( up );

    // Construct the orientation matrix
    mat4 orientationMatrix;
    orientationMatrix[0] = vec4(right,   0.0);
    orientationMatrix[1] = vec4(up,      0.0);
    orientationMatrix[2] = vec4(forward, 0.0);
    orientationMatrix[3] = vec4(0.0, 0.0, 0.0, 1.0);

    return orientationMatrix;
}

void main(void)
{   
    vec4 pos = vec4(
        posAttr.x * scale * 2.8,
        posAttr.y * scale * 2.8,
        posAttr.z * scale * 2.8, 1.0 );
        
    fragPosition = posAttr;

    vec3 transformedNormal = normalAttr;

    if( orientationMode == ORIENTATION_MODE_NORM )
    {
        mat3 VR = transpose( mat3( view ) );

        vec3 r_v = VR * vec3( 1,  0,  0 );
        vec3 f_v = VR * vec3( 0,  0, -1 ); 
        vec3 u_v = VR * vec3( 0,  1,  0 ); 

        vec3 u = normalize( n );

        vec3 r = normalize( cross( u, u_v ) ); 
        vec3 f = normalize( cross( r, u ) );

        mat4 OM;
        OM[0] = vec4( r, 0.0 );
        OM[1] = vec4( u, 0.0 );
        OM[2] = vec4( f, 0.0 );
        OM[3] = vec4( 0.0, 0.0, 0.0, 1.0 );

        pos = OM * pos; 

        transformedNormal = normalize(OM * vec4(normalAttr, 0.0)).xyz;
    }
    else if( orientationMode == ORIENTATION_MODE_VIEW )
    {
        // tilt the model along the x-axis so the up vector is now on the negative z-axis instead of the y-axis.
        mat4 OM = computeOrientationMatrix( vec3( 0, 0, -1 ), vec3( 1, 0, 0 ) );
        pos     = OM * pos;

        // Extract the rotation part of the view matrix (3x3)
        mat3 viewRotation = transpose( mat3( view ) );

        // Apply the billboard rotation
        pos.xyz = viewRotation * pos.xyz;

        transformedNormal = normalize( viewRotation * (OM * vec4(normalAttr, 0.0)).xyz);
    }
    else
    {
         //transformedNormal = normalize(normalMatrix * normalAttr);
    }

    pos = vec4( vec3( pos.xyz + offset.xyz ), 1.0 );

    /////////////////////////////////////////////////////////////////////////////

    vec3 delta = vec3( 0, 5.0, 0 );

    vec4 a = mvp * vec4( offset.xyz + delta, 1 );
    vec4 b = mvp * vec4( offset.xyz - delta, 1 );

    a = a / a.w;
    b = b / b.w;

    objScale = abs( a.y - b.y );

    //////////////////////////////////////////////////////////////////////////////

    position = vec4( modelView * pos ).xyz;
    normal   = transformedNormal;
    eye      = normalize( -vec4( modelView * pos ).xyz );

    scalar = scalarValue;
    tex_coord = uv;

    instanceID = gl_InstanceID;
    gl_Position = vec4( mvp * pos );
}

