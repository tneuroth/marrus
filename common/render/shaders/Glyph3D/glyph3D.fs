#version 330 core

#define COLOR_MODE_SOLID   0
#define COLOR_MODE_SCALAR  1
#define COLOR_MODE_TEXTURE 2

layout (location = 0) out vec3 gPosition;
layout (location = 1) out vec3 gNormal;
layout (location = 2) out vec3 gAlbedo;

in vec3 normal;
in vec3 position;
in vec2 tex_coord;
in float scalar;
flat in float objScale;

uniform sampler2D textureImage;
uniform sampler1D tf;
uniform vec2 scalarMappingRange;
uniform int colorMode;
uniform vec3 solidColor;

flat in int instanceID;

void main()
{    
    gPosition = position;
    gNormal = normalize( normal );
    
    if( colorMode == COLOR_MODE_SCALAR )
    {
        float rw = ( scalarMappingRange.y - scalarMappingRange.x );
        float v  = clamp( ( scalar - scalarMappingRange.x ) / rw, 0.0, 1.0 );
        gAlbedo.rgb = texture( tf, v ).rgb;
    }
    else if( colorMode == COLOR_MODE_TEXTURE )
    {
        vec3 textColor = texture( textureImage, tex_coord ).rgb;
        gAlbedo.rgb = textColor;

        if( true )
        {
            gAlbedo.rgb = textColor;
        }
        else
        {
            gAlbedo.rgb = vec3( 1, 1, 1 );
        }
    }
    else
    {
        gAlbedo.rgb = solidColor;
    }
}
