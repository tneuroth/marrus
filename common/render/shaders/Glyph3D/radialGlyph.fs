#version 330 core

layout (location = 0) out vec3 gPosition;
layout (location = 1) out vec3 gNormal;
layout (location = 2) out vec3 gAlbedo;

in vec3 fragPosition;

in vec3 normal;
in vec3 position;
in vec2 tex_coord;

in float scalar;
flat in float objScale;
flat in int instanceID;

uniform sampler2D textureImage;

uniform sampler1D tf;
uniform vec2 scalarMappingRange;

uniform samplerBuffer bufferTex;

uniform int nBins;
uniform int nAxes;
uniform int nComp;

uniform int perGlyphNormalization;
uniform int perSelBinNormalization;
uniform bool normalizeFromZero;
uniform bool allAxisNormalization;

uniform bool showBorders;

uniform int scaleMode;

uniform sampler1D axes_tfs;

#define MAX_AXIS 16
uniform vec2 axes_lims[   MAX_AXIS ];
uniform int axes_indices[ MAX_AXIS ];
uniform int n_active_axes;

#define MAX_COMP 16
uniform int comp_indices[ MAX_AXIS ];
uniform int n_active_comp;

#define TWO_PI_INV 0.15915494309
#define TWO_PI 6.28318530718
#define PI 3.14159265359

#define FLT_MIN 1.175494351e-38
#define FLT_MAX 3.402823466e+18

int colorToBinIdx( vec3 color )
{
    return 0;
}

// /////////////////////////////////////////////////

float getAngle()
{
    return atan( fragPosition.x, fragPosition.z ) + PI;
}

int axisId( const float s )
{
    return  min( n_active_axes - 1, int( floor( s * n_active_axes * TWO_PI_INV ) ) );
}

int compId( const float r )
{
    return  min( n_active_comp - 1, int( floor( r * n_active_comp ) ) );
}

float scaleValue(float value, float minVal, float maxVal, int mode) {

    value = clamp( value, minVal, maxVal );

    float c = FLT_MIN;
    float normalized = (value - minVal) / (maxVal - minVal);

    if (mode == 0) 
    { 
        return normalized;
    } 
    else if (mode == 1 ) 
    {
        return  sqrt( normalized + c );
    } 
    else if (mode == 2) {
        
        float logMin   = log( max( minVal + c, c ) );
        float logMax   = log( max( maxVal + c, c ) );
        float logValue = log( max( value  + c, c ) );

        return (logValue - logMin ) / ( logMax - logMin );
    }

    return normalized;
}


///////////////////////////////////////////////////////

void main()
{    
    gPosition = position;
    //gNormal   = vec3( 0.0, 0.0, 0.0 );
    gNormal   = normalize( normal );

    float radius = length( fragPosition );
    float angle  = getAngle();

    int comp  = compId( radius );
    int axis  = axisId( angle  );

    int comp_index = comp_indices[ comp ];
    int axis_index = axes_indices[ axis ];

    // Check if it is a border 

    vec3 borderColor = vec3( 0.4, 0.4, 0.4 );

    if( n_active_axes > 1 && false )
    {
        if( ( abs( -angle * radius ) < 0.06 
          ||  abs( ( angle - TWO_PI ) * radius ) < 0.06 )
            && radius > 0.9  )
        {
            gAlbedo.rgb = vec3(1,1,1);
            return;
        }
        else if( ( abs( -angle * radius ) < 0.1 
          ||  abs( ( angle - TWO_PI ) * radius ) < 0.1 )
            && radius > 0.87  )
        {
            gAlbedo.rgb = vec3(.2,.2,.2);
            return;
        }
    }

    if( showBorders )
    {
        // Outer border
        if( radius > 0.94 )
        {
            gAlbedo.rgb = borderColor;
            return;
        }

        // Axis Borders

        if( n_active_axes > 1 ) {
            if( abs( axis_index - (  angle * TWO_PI_INV ) * n_active_axes           ) * radius < 0.02
             || abs( axis_index - ( (angle - TWO_PI) * TWO_PI_INV ) * n_active_axes ) * radius < 0.02  )
            {
                gAlbedo.rgb = borderColor;
                return;
            }
        }

        // Component Borders
        if( abs( comp - radius * nComp ) < 0.06 )
        {
            gAlbedo.rgb = borderColor;
            return;
        }
    }

    // Get the min and max values of the axis over all regions 
    
    vec2 lims = axes_lims[ axis_index ];

    // Get the offset and then value of the component in the axis for this region

    int   data_offset = instanceID*nAxes*nComp + nComp*axis_index + comp_index;
    float data_value  = texelFetch( bufferTex, data_offset ).r;

    if( perGlyphNormalization != 0 && ! allAxisNormalization )
    {
        // vec2 perGlyphNormalization = vec2( FLT_MAX, -FLT_MAX );
        float sum = 0;

        if( perSelBinNormalization != 0 )
        {
            for( int idx = 0; idx < n_active_comp; idx++ )
            {
                int c_idx = comp_indices[ idx ];

                int d_index = instanceID*nAxes*nComp + nComp*axis_index + c_idx;
                float val = texelFetch( bufferTex, d_index ).r;

                sum += val;

                // perGlyphNormalization.x = min( perGlyphNormalization.x, val );
                // perGlyphNormalization.y = max( perGlyphNormalization.y, val );
            }
        }
        else
        {
            for( int c_idx = 0; c_idx < nComp; c_idx++ )
            {
                int d_index = instanceID*nAxes*nComp + nComp*axis_index + c_idx;
                float val = texelFetch( bufferTex, d_index ).r;

                sum += val;

                // perGlyphNormalization.x = min( perGlyphNormalization.x, val );
                // perGlyphNormalization.y = max( perGlyphNormalization.y, val );
            }
        }
        
        // lims = perGlyphNormalization;

        if( sum > 0 )
        {
            data_value /= sum;
        }
    }

    float minV = lims.x;

    if( normalizeFromZero )
    {
        minV = 0.0;
    }

    vec2 allAxisLims = lims;

    float vn = 0.0;

    if( allAxisNormalization )
    {

        float maxSum = 0;
        float minSum = 9999999999.0;

        for( int i = 0; i < n_active_axes; ++i )
        {
            int a_idx = axes_indices[ i ];
            vec2 a_lims = axes_lims[ a_idx ];

            allAxisLims.x = min( allAxisLims.x, a_lims.x );
            allAxisLims.y = max( allAxisLims.y, a_lims.y );

            float sum = 0.0;
            for( int c_idx = 0; c_idx < nComp; c_idx++ )
            {
                int IDX = instanceID*nAxes*nComp + nComp*a_idx + c_idx;
                sum += texelFetch( bufferTex, IDX ).r;
            }

            maxSum = max( maxSum, sum );
            minSum = min( minSum, sum );
        }

        if( normalizeFromZero )
        {
            allAxisLims.x = 0;
        }

        if( perGlyphNormalization != 0 && maxSum > 0 )
        {
            vn = scaleValue( data_value / maxSum, allAxisLims.x, allAxisLims.y, scaleMode );
        }
        else
        {
            vn = scaleValue( data_value, allAxisLims.x, allAxisLims.y, scaleMode );
        }
    }
    else
    {
        // Normalize based on min and max of axis, mapping value to between 0, and 1
        vn = scaleValue( data_value, minV, lims.y, scaleMode );
    }

    float ai = float( axis_index );
    float na = float( nAxes );

    float TF_OFFSET = ai / na + ( 1.0 / ( nAxes * 256.0 ) );
    float tw = 1.0 / na - ( 2.0 / ( nAxes * 256.0 ) );

    float v_TF = TF_OFFSET + vn*tw;
    gAlbedo.rgb = texture( axes_tfs, v_TF ).rgb;
}
