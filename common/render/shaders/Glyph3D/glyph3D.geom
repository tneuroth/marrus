#version 410

#define COLOR_MODE_SOLID   0
#define COLOR_MODE_SCALAR  1
#define COLOR_MODE_TEXTURE 2

#define ORIENTATION_MODE_NONE 0
#define ORIENTATION_MODE_VIEW 1
#define ORIENTATION_MODE_NORM 2

layout (triangles) in;
layout (triangle_strip, max_vertices = 3) out;
  
uniform mat4 model;
uniform mat4 view;
uniform mat4 modelView;
uniform mat4 mvp;
uniform mat3 normalMatrix;

uniform int colorMode;
uniform int orientationMode;
uniform float scale;

in V2G
{
    vec3 offset;
    vec3 normal;
    vec2 uv;
    vec3 u;
    vec3 v;
    vec3 n;
    float scalar;
} v2g[];

out G2F
{
	vec3  position;
	vec3  normal;
	float objScale;
	vec2  tex_coord;
	float scalar;
} g2f;

vec3 projectOntoPlane(vec3 v, vec3 planeNormal)
{
    vec3 normalizedNormal = normalize(planeNormal);
    float dotProduct = dot(v, normalizedNormal);
    vec3 projection = v - dotProduct * normalizedNormal;
    return projection;
}

mat4 computeOrientationMatrix(vec3 up, vec3 right ) {

    right = projectOntoPlane( right, up ); 

    // Compute the forward vector as the cross product of right and up vectors
    vec3 forward = normalize(cross( right, up ) );

    // Construct the orientation matrix
    mat4 orientationMatrix;
    orientationMatrix[0] = vec4(right, 0.0);
    orientationMatrix[1] = vec4(-up, 0.0);
    orientationMatrix[2] = vec4(forward, 0.0);
    orientationMatrix[3] = vec4(0.0, 0.0, 0.0, 1.0);

    return orientationMatrix;
}

void pass()
{
    const float scale = 1.7;

    for( int i = 0; i < 3; ++i )
    {
	    vec3 up    = v2g[i].n;
	    vec3 right = v2g[i].u;

	    ////////////////////////////////////////////////////////////////////

	    vec4 pos = vec4(
	        gl_in[i].gl_Position.x * scale,
	        gl_in[i].gl_Position.y * scale,
	        gl_in[i].gl_Position.z * scale, 1.0 );

	    vec3 transformedNormal;

	    if( orientationMode == ORIENTATION_MODE_NORM )
	    {
	        mat4 OM = computeOrientationMatrix( up, right );
	        pos = OM * pos; 
	        transformedNormal = normalize(OM * vec4( v2g[i].normal, 0.0)).xyz;
	    }
	    else if( orientationMode == ORIENTATION_MODE_VIEW )
	    {
	        // tilt the model along the x-axis so the up vector is now on the negative z-axis instead of the y-axis.
	        mat4 OM = computeOrientationMatrix( vec3( 0, 0, -1 ), vec3( 0, -1, 0 ) );
	        pos = OM * pos; 

	        // Extract the rotation part of the view matrix (3x3)
	        mat3 viewRotation = mat3( view );

	        // Invert the rotation part to make the model face the camera
	        mat3 billboardRotation = transpose( viewRotation );

	        // Apply the billboard rotation
	        pos.xyz = billboardRotation * pos.xyz;

	        transformedNormal = normalize(billboardRotation * (OM * vec4( v2g[i].normal, 0.0)).xyz);
	    }
	    else
	    {
	         transformedNormal = normalize( normalMatrix * v2g[i].normal );
	    }

    	pos = vec4( vec3( pos.xyz + v2g[i].offset.xyz ), 1.0 );

	    vec3 delta = vec3( 0, 5.0, 0 );

	    vec4 a = mvp * vec4( v2g[i].offset.xyz + delta, 1 );
	    vec4 b = mvp * vec4( v2g[i].offset.xyz - delta, 1 );

	    a = a / a.w;
	    b = b / b.w;

	    g2f.objScale = abs( a.y - b.y );

	    g2f.position = vec4( modelView * pos ).xyz;
	    g2f.normal = transformedNormal;

	    if( colorMode == COLOR_MODE_SCALAR )
	    {
	        g2f.scalar = v2g[i].scalar;
	    }
	    if( colorMode == COLOR_MODE_TEXTURE )
	    {
	        g2f.tex_coord = v2g[i].uv;
	    }

	    gl_Position = vec4( mvp * pos );

		EmitVertex();
	}

    EndPrimitive();
}

void main() {    
	pass();
}    