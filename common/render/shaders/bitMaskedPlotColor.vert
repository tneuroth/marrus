#version 330 core

layout(location = 0) in float xOffset;
layout(location = 1) in float yOffset;
layout(location = 2) in vec3 colorIn;
layout(location = 3) in int flag;

flat out int bitFlags;
flat out vec3 color;

uniform mat4 TR;
uniform float xScale;
uniform float yScale;

void main(void)
{
    color = colorIn;
    bitFlags = flag;   

    vec4 pos = TR * vec4(
        xOffset,
        yOffset,
        0,
        1.0
    );

    gl_Position = pos;
}
