#version 330 core
layout (location = 0) out vec3 gPosition;
layout (location = 1) out vec3 gNormal;
layout (location = 2) out vec3 gAlbedo;

in G2F{
    vec3 e_n;
    vec3 e_pos;
    float c;
}g2f;

uniform sampler1D tf;
uniform vec2 crange;

uniform bool useCL;
uniform vec3 cl;

uniform bool logScale;

uniform bool fromBaseLine;
uniform float baseLine;
uniform vec2 fromBaselineRange;

void main()
{    
    // store the fragment position vector in the first gbuffer texture
    gPosition = g2f.e_pos;
    // also store the per-fragment normals into the gbuffer
    gNormal = normalize( g2f.e_n );
    // and the diffuse per-fragment color

//    if( !useCL )
//    {
        if( fromBaseLine )
        {
            float diff = g2f.c - baseLine;
            float r = 0.5 + ( diff / ( fromBaselineRange.y - fromBaselineRange.x ) )*0.8;
            if( r < 0 )
            {
                r = 0;
            }
            if( r > 1 )
            {
                r = 1;
            }
            gAlbedo.rgb = texture( tf, r ).rgb * 0.85;
        }
        else
        {
            float r = ( g2f.c - crange.x ) / ( crange.y - crange.x );
            if( logScale )
            {
                r = log2( r * 9 + 1.0 ) / log2( 10.0 );
            }
            gAlbedo.rgb = texture( tf, r ).rgb;
        }
//    }
//    else
//    {
//        gAlbedo.rgb = cl;
//    }
}
