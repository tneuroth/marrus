// #version 330 core
// out float FragColor;

// in vec2 TexCoords;

// uniform sampler2D gPosition;
// uniform sampler2D gNormal;
// uniform sampler2D texNoise;

// uniform int width;
// uniform int height;

// uniform float occlusionRadius;
// uniform float bias;
// uniform int kernelSize;
// uniform float noise;

// const int MAX_KERNEL_SIZE = 512;

// uniform vec3 samples[MAX_KERNEL_SIZE];

// uniform mat4 projection;



// void main()
// {
//     // tile noise texture over screen based on screen dimensions divided by noise size
//     vec2 noiseScale = vec2( width / noise, height / noise );

//     // get input for SSAO algorithm
//     vec3 fragPos = texture(gPosition, TexCoords).xyz;
//     vec3 normal = normalize(texture(gNormal, TexCoords).rgb);
//     vec3 randomVec = normalize(texture(texNoise, TexCoords * noiseScale).xyz);

//     // create TBN change-of-basis matrix: from tangent-space to view-space
//     vec3 tangent = normalize(randomVec - normal * dot(randomVec, normal));
//     vec3 bitangent = cross(normal, tangent);
//     mat3 TBN = mat3(tangent, bitangent, normal);

//     // iterate over the sample kernel and calculate occlusion factor
//     float occlusion = 0.0;
//     for(int i = 0; i < kernelSize; ++i)
//     {
//         // get sample position
//         vec3 sample = TBN * samples[i]; // from tangent to view-space
//         sample = fragPos + sample * occlusionRadius;
        
//         // project sample position (to sample texture) (to get position on screen/texture)
//         vec4 offset = vec4(sample, 1.0);
//         offset = projection * offset; // from view to clip-space
//         offset.xyz /= offset.w; // perspective divide
//         offset.xyz = offset.xyz * 0.5 + 0.5; // transform to range 0.0 - 1.0
        
//         // get sample depth
//         float sampleDepth = texture(gPosition, offset.xy).z; // get depth value of kernel sample
        
//         // range check & accumulate
//         float rangeCheck = smoothstep(0.0, 1.0, occlusionRadius / abs(fragPos.z - sampleDepth));
//         occlusion += (sampleDepth >= sample.z + bias ? 1.0 : 0.0) * rangeCheck;           
//     }
//     occlusion = 1.0 - ( occlusion / kernelSize );
//     FragColor = occlusion;
// }

#version 330 core
out float FragColor;

in vec2 TexCoords;

uniform sampler2D gPosition;
uniform sampler2D gNormal;
uniform sampler2D texNoise;

uniform int width;
uniform int height;

uniform float occlusionRadius;

uniform float bias;
uniform int kernelSize;
uniform float noise;

const int MAX_KERNEL_SIZE = 512;

uniform vec3 samples[MAX_KERNEL_SIZE];

uniform mat4 projection;

// Function to compute SSAO with a given radius
float computeOcclusion(float radius) {
    // tile noise texture over screen based on screen dimensions divided by noise size
    vec2 noiseScale = vec2( width / noise, height / noise );

    // get input for SSAO algorithm
    vec3 fragPos = texture(gPosition, TexCoords).xyz;
    vec3 normal = normalize(texture(gNormal, TexCoords).rgb);
    vec3 randomVec = normalize(texture(texNoise, TexCoords * noiseScale).xyz);

    // create TBN change-of-basis matrix: from tangent-space to view-space
    vec3 tangent = normalize(randomVec - normal * dot(randomVec, normal));
    vec3 bitangent = cross(normal, tangent);
    mat3 TBN = mat3(tangent, bitangent, normal);

    // iterate over the sample kernel and calculate occlusion factor
    float occlusion = 0.0;
    for(int i = 0; i < kernelSize; ++i)
    {
        // get sample position
        vec3 sample = TBN * samples[i]; // from tangent to view-space
        sample = fragPos + sample * radius;
        
        // project sample position (to sample texture) (to get position on screen/texture)
        vec4 offset = vec4(sample, 1.0);
        offset = projection * offset; // from view to clip-space
        offset.xyz /= offset.w; // perspective divide
        offset.xyz = offset.xyz * 0.5 + 0.5; // transform to range 0.0 - 1.0
        
        // get sample depth
        float sampleDepth = texture(gPosition, offset.xy).z; // get depth value of kernel sample
        
        // range check & accumulate
        float rangeCheck = smoothstep(0.0, 1.0, radius / abs(fragPos.z - sampleDepth));
        occlusion += (sampleDepth >= sample.z + bias ? 1.0 : 0.0) * rangeCheck;           
    }
    return 1.0 - ( occlusion / kernelSize );
}

void main()
{
    // Compute occlusion with two different radii
    float occlusion1 = computeOcclusion( occlusionRadius * 0.2 );
    // float occlusion2 = computeOcclusion( occlusionRadius / 2 );

    // Blend the results, e.g., simple average blending
    FragColor = occlusion1; // * 0.5 + occlusion2 * 0.5;
}