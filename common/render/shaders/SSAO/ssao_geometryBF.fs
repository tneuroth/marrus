#version 330 core
layout (location = 0) out vec3 gPosition;
layout (location = 1) out vec3 gNormal;
layout (location = 2) out vec3 gAlbedo;

in vec3 normal;
in vec3 position;
flat in int bf;
uniform vec3 cl1;
uniform vec3 cl2;
void main()
{    
    gPosition = position;
    gNormal = normalize( normal );
    
    if( ( 1 & bf ) == 1 )
    {
        gAlbedo.rgb = cl2;
    }
    else
    {
        gAlbedo.rgb = cl1;
    }
}
