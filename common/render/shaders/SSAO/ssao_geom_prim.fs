#version 330 core
layout (location = 0) out vec3 gPosition;
layout (location = 1) out vec3 gNormal;
layout (location = 2) out vec3 gAlbedo;

in vec3 position;
uniform vec3 cl;

void main()
{    
    gPosition = position;
    gAlbedo = cl;
    gNormal = vec3( 1, 0, 0 );
}
