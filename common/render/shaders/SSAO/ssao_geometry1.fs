#version 330 core
layout (location = 0) out vec3 gPosition;
layout (location = 1) out vec3 gNormal;
layout (location = 2) out vec3 gAlbedo;

in vec3 normal;
in vec3 position;
in float scalar;
flat in float f;

uniform sampler1D tf;
uniform vec2 crange;

uniform bool logScale;
uniform bool useFilter;
uniform bool solidColor;
uniform vec3 cl;

void main()
{    
    gPosition = position;
    gNormal = normalize( normal );
    
    // if( ! gl_FrontFacing ) 
    // {
    //     gAlbedo.rgb = vec3( 0.3, 0.3, 0.3 );
    // } else 
    if( useFilter && f < 0.5 )
    {
        gAlbedo.rgb = vec3( 0.65, 0.65, 0.65 );
    }
    else if( useFilter && f >= 0.5 && solidColor )
    {
        gAlbedo.rgb = cl;
    }
    else {
        if( ! solidColor )
        {
            float r = clamp( ( scalar - crange.x ) / ( crange.y - crange.x ), 0.0, 1.0 );

            if( logScale )
            {
                r = log2( r * 9 + 1.0 ) / log2( 10.0 );
            }

            gAlbedo.rgb = texture( tf, r ).rgb;
        }
        else 
        {
            gAlbedo.rgb = cl;
        }
    }
}
