#version 330 core
out vec4 FragColor;

in vec2 TexCoords;

uniform sampler2D gPosition;
uniform sampler2D gNormal;
uniform sampler2D gAlbedo;
uniform sampler2D ssao;

uniform vec3 bkgColor;

struct Light {
    vec3 direction;
};

uniform Light light;

struct Material {
    float Ks;
    float Kd;
    float Ka;
    float shininess;
};

uniform Material MT;

void main()
{             
    // retrieve data from gbuffer
    
    vec3 FragPos = texture( gPosition, TexCoords ).rgb;
    vec3 Normal  = texture( gNormal,   TexCoords ).rgb;
    vec3 Diffuse = texture( gAlbedo,   TexCoords ).rgb;
    
    if( Normal == vec3( 0.0, 0.0, 0.0 ) )
    {
        FragColor = vec4( Diffuse.xyz, 1.0 );
        return;
    }
    
    if( FragPos.z > -0.001 )
    {
        FragColor = vec4( bkgColor.xyz, 1.0 );
        return;
    }

    float AmbientOcclusion = texture(ssao, TexCoords).r;
    
    // then calculate lighting as usual
    vec3 ambient = vec3( Diffuse * AmbientOcclusion ) * 1.0;

    // diffuse
    vec3 lightDir  = normalize( vec3( -0.5, 0.5, 0.5 ) );
    vec3 lightDir2 = normalize( light.direction );
    
    vec3 diffuse = vec3( 0, 0, 0 );
    
    if( dot( Normal, lightDir2 ) > 0.0 )
    {
        diffuse = max( dot( Normal, lightDir2 ), 0.0) * Diffuse;
    }

    // specular
    vec3 viewDir    = normalize(-FragPos); // viewpos is (0.0.0)
    vec3 halfwayDir = normalize( lightDir2 + viewDir );
    float spec      = pow(max(dot( Normal, halfwayDir ), 0.0), 50 ); //MT.shininess );
    vec3 specular   = vec3( spec );

    vec3 lighting = ambient * 0.9 + diffuse * 0.2 + specular * 0.1; //MT.Ks;
    FragColor     = vec4( min( lighting, vec3( 1.0 ) ), 1.0 );
}
