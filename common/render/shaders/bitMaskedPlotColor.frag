#version 130

uniform int bitMask;
out vec4 fragColor;

flat in vec3 color;
flat in int bitFlags;

void main(void)
{   
    // this point is to be discarded
    if( ( bitFlags & bitMask ) == 0 )
    {
        discard;
    }

    vec2 coord = gl_PointCoord - vec2( 0.5 );
    float r = length( coord );

    if( r > 0.5 )
    {                 
        discard;
    }
    else
    {
        fragColor = vec4( color.xyz, 1.0 );
    }    
}
