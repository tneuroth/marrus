#version 410

uniform mat4 modelView;
uniform mat4 mvp;
uniform mat3 normalMatrix;

layout (location = 0) in vec3 posAttr;
layout (location = 1) in vec3 normalAttr;
layout (location = 2) in float c;

out vec3  normal;
out vec3  position;
out float scalar;
flat out float f;

//out vec3  eye;


void main(void)
{
    vec3 p = posAttr.xyz;
   // p.x = posAttr.y;
   // p.y = -posAttr.z;
   // p.z = posAttr.x;

    position = vec4( modelView * vec4( p, 1.0 ) ).xyz;
    normal = normalize( normalMatrix * normalAttr );
    scalar = c;

//    eye = normalize( -vec4( modelView * vec4( p, 1.0 ) ).xyz );

    gl_Position = mvp * vec4( p, 1.0 );
}

