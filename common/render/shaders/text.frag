#version 130

in  vec2 uv;
out vec4 color;

uniform sampler2D tex;
uniform vec4 textColor;

void main()
{
    vec4 sampled = vec4( 1.0, 1.0, 1.0, texture( tex, uv ).r );
    color = textColor * sampled;
}
