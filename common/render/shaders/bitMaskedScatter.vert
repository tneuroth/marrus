#version 330 core

layout(location = 0) in float xOffset;
layout(location = 1) in float yOffset;
layout(location = 2) in int flag;

flat out int bitFlags;
uniform mat4 TR;

void main(void)
{
    bitFlags = flag;   

    vec4 pos = TR * vec4(
        xOffset,
        yOffset,
        0,
        1.0
    );

    gl_Position = pos;
}
