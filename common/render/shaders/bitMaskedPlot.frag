#version 130

uniform int bitMask;
uniform vec4 color;
out vec4 fragColor;

flat in int bitFlags;

void main(void)
{   
    if( ( bitFlags & bitMask ) == 0 )
    {
        discard;
    } 

    float R = 0.5;	
	  vec2 coord = gl_PointCoord - vec2( R );
    float r = length( coord );

  	if( r > R )
  	{                 
  	    discard;
  	}
  	else
  	{
         fragColor = color;
    }
}
