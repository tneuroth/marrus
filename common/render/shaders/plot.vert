#version 330 core

layout(location = 0) in vec2 glyphPos;
layout(location = 1) in vec2 offset;
layout(location = 2) in float scalar;
layout(location = 3) in float v_filter;

flat out int instanceId; 
flat out float v;
flat out float f;

uniform mat4 TR;
uniform float xScale;
uniform float yScale;

void main(void)
{
    instanceId = gl_InstanceID;
    v = scalar;
    f = scalar;

    vec4 pos = TR * vec4(
        offset.x + glyphPos.x * 0.0033 * xScale,
        offset.y + glyphPos.y * 0.0033 * yScale,
        0,
        1.0
    );

    gl_Position = pos;
}
