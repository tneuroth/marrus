#version 330 core

layout (location = 0) in vec2 ps;
layout (location = 1) in vec2 tc;

uniform mat4 P;

out vec2 uv;

void main(void)
{
    uv = tc;
    gl_Position = P * vec4( ps.x, ps.y, 0, 1 );
}
