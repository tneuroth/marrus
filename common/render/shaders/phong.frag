#version 330

const int SUN   = 0;
const int POINT = 1;
const int SPOT  = 2;

const int MAX_LIGHTS = 8;

struct Material {

    vec3 Ka;
    vec3 Kd;
    vec3 Ks;

    vec3 Ka_scale;
    vec3 Kd_scale;
    vec3 Ks_scale;

    bool overrideAmbient;

    float alpha;
    float opacity;
};

struct LightSource {

    // 0 = sun, 1 = point, 2 = spot
    int type;

    //all sources
    vec3 direction;

    vec3 Ia;
    vec3 Id;
    vec3 Is;

    //spot and point
    vec3 position;

    //attenuation
    float at1;
    float at2;

    //spot
    float cutOff;
    float exponent;
};

uniform Material M;
uniform LightSource L[ MAX_LIGHTS ];
uniform bool enabled[ MAX_LIGHTS ];

uniform bool useTexture;
uniform sampler2D sampler;

in vec2 texCoord;
in vec3 normal;
in vec3 eye;
in vec3 pos;

out vec4 fragColor;

void main() {

    //if( gl_FragCoord.z  < 0.9996  )
    //    discard;

    vec3 cl = vec3(0.8,0.7,0.6);
    vec3 tc = vec3(0,0,0);

    vec3 Nm = normalize( normal );
    vec3 Vm = normalize( eye );

    for ( int i = 0; i < MAX_LIGHTS; ++i ) {

        if ( enabled[i] ) {

            vec3 lightDirection;
            float attenuation;

            lightDirection = normalize( L[i].direction );
            attenuation    = 1.0;

            //cl += M.Ka*L[i].Ia;

            float angle = dot( Nm, lightDirection );

            if ( angle > 0.0) {

                vec3 Rm = normalize( reflect( -lightDirection, Nm ) );
                cl += attenuation * (  M.Kd*2 * angle * L[i].Id + M.Ks*2 * pow( max( dot( Rm, Vm ), 0 ), M.alpha ) * L[i].Is );
            }
        }
    }

    fragColor = vec4( min( cl, vec3( 1.0 ) ), 1.0 );
}

