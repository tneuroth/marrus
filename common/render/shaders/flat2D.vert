#version 330 core

layout(location = 0) in vec2 pos;

uniform mat4 TR;

void main(void)
{
    vec4 pos = TR * vec4(
        pos.x,
        pos.y,
        0,
        1.0
    );

    gl_Position = pos;

}
