#version 410

uniform mat4 modelView;
uniform mat4 mvp;
uniform mat3 normalMatrix;

layout (location = 0) in vec3 posAttr;
layout (location = 1) in vec3 normalAttr;
layout (location = 2) in int bitFlag;

out vec3  normal;
out vec3  position;
flat out int bf;

void main(void)
{
    vec3 p = posAttr.xyz;
    position = vec4( modelView * vec4( p, 1.0 ) ).xyz;
    normal = normalize( normalMatrix * normalAttr );
    bf = bitFlag;
    gl_Position = mvp * vec4( p, 1.0 );
}

