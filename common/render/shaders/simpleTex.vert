#version 330 core
layout (location = 0) in vec3 ps;
layout (location = 1) in vec2 tc;

out vec2 uv;

void main()
{
    uv = tc;
    gl_Position = vec4( ps, 1.0 );
}