#version 330 core

in  vec2 uv;
out vec4 color;

uniform sampler2D tex;
uniform vec4 cl;

void main()
{
    float f = texture( tex, uv.st ).r;
    if( f == 0 )
    {
        discard;
    }
    else
    {
    	color = cl;
    }
}