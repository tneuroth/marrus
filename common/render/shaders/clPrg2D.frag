#version 130

in  vec3 cl;
out vec4 fragColor;

void main(void)
{    
    fragColor = vec4( cl, 1.0 ); 
}
