#version 130

uniform vec4 color;
uniform sampler1D tf;
uniform vec2 crange;
uniform bool logScale;
uniform bool useFilter;
uniform bool solidColor;

out vec4 fragColor;

flat in int instanceId; 
flat in float v;
flat in float f;

void main(void)
{    
    if( f > 0.5 )
    {
        fragColor = color;
    }
    else
    {
        fragColor = vec4( 0.3, 0.3, 0.3, color.a );
    }
}
