#version 330 core

in  vec2 uv;
out vec4 color;

uniform sampler2D tex;
uniform sampler1D tf;

uniform bool zeroAlpha;

uniform float mx;
uniform float mn;

void main()
{
    float f = texture( tex, uv.st ).r;
    float r = ( f - mn ) / ( mx - mn );
    vec3 cl = texture( tf, r ).rgb;

    if( f == 0 && zeroAlpha )
    {
        discard;
        //color = vec4( 0.0, 0.0, 0.0, 1.0 );
    }
    else
    {
    	color = vec4( cl.xyz, 1.0 );
    }
}