#version 330 core

layout(location = 0) in vec2 pos;
layout(location = 1) in vec4 col;

out vec4 cl;

uniform mat4 TR;

void main(void)
{
    vec4 pos = vec4(
        pos.x,
        pos.y,
        0,
        1.0
    );

    cl = col;
    gl_Position = pos;
}
