#version 330 core

in  vec2 uv;
out vec4 color;
uniform sampler2D tex;
uniform int nColors;

vec4 classColor( int colorCode )
{
    const float norm = 255.0;
    const vec4 CM[ 11 ] = vec4[ 11 ](
        vec4( 166 / norm, 206 / norm, 227 / norm, 0.4 ),
        vec4(  31 / norm, 120 / norm, 180 / norm, 0.4 ),
        vec4( 178 / norm, 223 / norm, 138 / norm, 0.4 ),
        vec4(  51 / norm, 160 / norm,  44 / norm, 0.4 ),
        vec4( 251 / norm, 154 / norm, 153 / norm, 0.4 ),
        vec4( 227 / norm,  26 / norm,  28 / norm, 0.4 ),
        vec4( 253 / norm, 191 / norm, 111 / norm, 0.4 ),
        vec4( 255 / norm, 127 / norm,   0 / norm, 0.4 ),
        vec4( 202 / norm, 178 / norm, 214 / norm, 0.4 ),
        vec4( 106 / norm,  61 / norm, 154 / norm, 0.4 ),
        vec4( 255 / norm, 255 / norm, 153 / norm, 0.4 )
    );

    return CM[ colorCode % 11 ];
}

void main()
{
    float f = texture( tex, uv.st ).r;
    int c = int( f );

    if( c == 0 )
    {
        color = vec4( 0, 0, 0, 1 );
    }
    //else if( c == -1 )
    //{
    //    color = vec4( 1, 0, 0, 1 );
    //}
    //else if( c < -1 )
    //{
    //    color = vec4( 0, 1, 0, 1 );
    //}
    else
    {
        color = classColor( c + 1 );
    }
}