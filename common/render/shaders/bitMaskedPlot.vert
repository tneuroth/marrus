#version 330 core

layout(location = 0) in vec2 glyphPos;
layout(location = 1) in float xOffset;
layout(location = 2) in float yOffset;
layout(location = 3) in int flag;

flat out int bitFlags;

uniform mat4 TR;
uniform float xScale;
uniform float yScale;

void main(void)
{
    bitFlags = flag;   

    vec4 pos = TR * vec4(
        xOffset + glyphPos.x * 0.0033 * xScale,
        yOffset + glyphPos.y * 0.0033 * yScale,
        0,
        1.0
    );

    gl_Position = pos;
}
