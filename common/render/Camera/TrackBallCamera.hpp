#ifndef TN_TRACK_BALL_CAM_HPP
#define TN_TRACK_BALL_CAM_HPP

#include <glm/glm.hpp>
#include <glm/gtx/transform.hpp>

#include <iostream>
#include <cmath>

namespace TN 
{

class TrackBallCamera 
{

public:

    enum ProjMode { PM_Perspective, PM_Orthographic };

private:
	
    static constexpr float initZoom = -.5;

    glm::vec3 m_eye;
    glm::vec3 m_center;
    glm::vec3 m_up;
    float m_zoom;
    ProjMode m_mode;
    float m_aspect;

	float fov() const
	{
	    return ( std::atan( m_zoom ) / M_PI + 0.5f ) * 180.f;
	}

	glm::vec2 focalPlaneSize() const
	{
	    float viewLen = glm::length( (m_center - m_eye) );
	    float height = std::tan( fov() / 2.f / 180.f * M_PI ) * viewLen;
	    return glm::vec2( height * m_aspect * 2.f, height * 2.f );
	}

public:

	bool operator==( const TN::TrackBallCamera & other ) {
	    return 
			m_eye    == other.m_eye
		 && m_center == other.m_center
		 && m_up     == other.m_up
		 && m_zoom   == other.m_zoom
		 && m_mode   == other.m_mode
		 && m_aspect == other.m_aspect;
	}

	bool operator!=( const TN::TrackBallCamera & other ) {
	    return ! ( *this == other );
	}

    void print() 
    {
    	std::cout << "m_eye: { " << m_eye.x << ", " << m_eye.y << ", " << m_eye.z << " }" << std::endl;
    	std::cout << "m_eye: { " << m_center.x << ", " << m_center.y << ", " << m_center.z << " }" << std::endl;
    	std::cout << "m_eye: { " << m_up.x << ", " << m_up.y << ", " << m_up.z << " }" << std::endl;
    	std::cout << m_zoom << std::endl;    	    	
    }

    TrackBallCamera & operator=( const TrackBallCamera & other )
    {
    	if( this == &other )
    	{
    		return *this;
    	}

		m_eye    = other.m_eye;
		m_center = other.m_center;
		m_up     = other.m_up;
		m_zoom   = other.m_zoom;
		m_mode   = other.m_mode;
		m_aspect = other.m_aspect;

    	return *this;
    }

    TrackBallCamera( TrackBallCamera && other )
    {
		m_eye    = other.m_eye;
		m_center = other.m_center;
		m_up     = other.m_up;
		m_zoom   = other.m_zoom;
		m_mode   = other.m_mode;
		m_aspect = other.m_aspect;
    }

    TrackBallCamera( const TrackBallCamera & other )
    {
		m_eye    = other.m_eye;
		m_center = other.m_center;
		m_up     = other.m_up;
		m_zoom   = other.m_zoom;
		m_mode   = other.m_mode;
		m_aspect = other.m_aspect;
    }

    TrackBallCamera()
        : m_eye(0.f, 0.f, 1.f),
	      m_center(0.f, 0.f, 0.f),
	      m_up(0.f, 1.f, 0.f),
	      m_zoom(-10.f),
	      m_mode(PM_Perspective),
	      m_aspect(1.f)
	{
		reset( { -0.5, -0.5, -0.5 }, { 0.5, 0.5, 0.5 } );
	}

    void setCenter( glm::vec3 center )
    {
        m_center = center;
    }

    void setProjMode( const ProjMode& mode )
    {
        m_mode = mode;
    } 
    void setAspectRatio( const float ratio )
    {
        m_aspect = ratio;
    }

    glm::vec3 getEye() const
    {
        return m_eye;
    }

    void set( glm::vec3 eye, glm::vec3 center, glm::vec3 up, float zoom )
    {
        m_eye = eye;
        m_center = center;
        m_up = up;
        m_zoom = zoom;
    }

    void rotate( const std::string & dir )
    {
        if( dir == "up" )
        {
			glm::vec3 u = glm::normalize( m_center - m_eye );
			glm::vec3 s = glm::normalize( glm::cross( u, m_up ) );
        	glm::mat4 tr( 1 );
        	tr = glm::rotate( tr, (float) M_PI / 2.f, s );
            m_eye = glm::vec3( tr * glm::vec4( m_eye, 1.0 ) );
            m_up  = glm::vec3( tr * glm::vec4( m_up, 1.0 ) );
        }
        else if( dir == "down" )
        {
			glm::vec3 u = glm::normalize( m_center - m_eye );
			glm::vec3 s = glm::normalize( glm::cross( u, m_up ) );
        	glm::mat4 tr( 1 );
        	tr = glm::rotate( tr, (float) -M_PI / 2.f, s );
            m_eye = glm::vec3( tr * glm::vec4( m_eye, 1.0 ) );
            m_up  = glm::vec3( tr * glm::vec4( m_up, 1.0 ) );  	
        }
        else if( dir == "left" )
        {
        	glm::mat4 tr( 1 );
        	tr = glm::rotate( tr, (float) -M_PI / 2.f, m_up );
            m_eye = glm::vec3( tr * glm::vec4( m_eye, 1.0 ) );   
        }
        else if( dir == "right" )
        {
        	glm::mat4 tr( 1 );
        	tr = glm::rotate( tr, (float) M_PI / 2.f, m_up );
            m_eye = glm::vec3( tr * glm::vec4( m_eye, 1.0 ) );
        }        
    }

    float getZoom() const
    {
    	return m_zoom;
    }

    void setUpAndEye( const glm::vec3 & up, const glm::vec3 & eye )
    {
		 glm::vec3 bmin = { -0.5, -0.5, -0.5 };
		 glm::vec3 bmax = { 0.5, 0.5, 0.5 };

	    float height = bmax.y - bmin.y;
	    float distance = ( height / 2.f ) / std::tan( fov() / 180.f * M_PI / 2.f );
	    distance = distance * 2.f + bmax.z - m_center.z;

        m_up = up;
	    m_eye = m_center + eye * distance;
    }

    void orbit(const glm::vec2 & dir)
	{
		glm::vec3 u = glm::normalize( m_center - m_eye );
		glm::vec3 s = glm::normalize( glm::cross( u, m_up ) );
		glm::vec3 t = glm::normalize( glm::cross( s, u    ) );

		glm::vec3 rotateDir  = glm::normalize( dir.x * s + dir.y * t );
		glm::vec3 rotateAxis = glm::normalize( glm::cross( rotateDir, u ) );

		float angle = glm::length( dir ) * 360.f;
		
        if( angle == 0 )
        {
            return;
        }

		glm::mat4 matRotate( 1.f );
		matRotate = glm::rotate( matRotate, -angle, rotateAxis );
		glm::vec4 view4 = ( matRotate * glm::vec4( m_eye - m_center, 1.f ) ); // ??
		glm::vec3 view( view4.x, view4.y, view4.z );

		m_eye = m_center + view;
		m_up = glm::normalize( glm::cross( view, s ) );
	}

    void track( const glm::vec2 & dir )
    { 
		glm::vec3 u = glm::normalize( m_center - m_eye );
		glm::vec3 s = glm::normalize( glm::cross( u, m_up ) );
		glm::vec3 t = glm::normalize( glm::cross( s, u    ) );

		glm::vec2 scaleDir = dir *   focalPlaneSize();
		glm::vec3 trackDir = ( scaleDir.x * s + scaleDir.y * t );
		m_eye = m_eye       - trackDir;
		m_center = m_center - trackDir;
	}

    void zoom(const float factor)
	{
	    m_zoom += factor / 1000.f;
	}

    void reset(
    	const glm::vec3 & bmin, 
    	const glm::vec3 & bmax )
	{
	    m_center = ( bmin + bmax ) / 2.f + glm::vec3( 0.f, 0.f, 0.f );
	    
	    m_up = glm::vec3( 0.f, 0.f, 1.f );
	    
	    m_zoom = initZoom;
	    float height = bmax.y - bmin.y;
	    float distance = ( height / 2.f ) / std::tan( fov() / 180.f * M_PI / 2.f );
	    distance = distance * 2.f + bmax.z - m_center.z;

	    m_eye = m_center + glm::vec3( 0.f, -1.f, 0.f ) * distance;
	}

    glm::mat4 matView( float scale ) const
	{
	    return glm::lookAt( m_eye * scale, m_center, m_up );
	}

    glm::mat4 matProj() const
	{
	    glm::mat4 mat;
	    if (PM_Perspective == m_mode)
	    {
	        mat = glm::perspective( fov(), m_aspect, 1.f, 100.f );
	    }
	    else
	    {
	        glm::vec2 size = focalPlaneSize() / 2.f;
	        mat = glm::ortho( -size.x, size.x, -size.y, size.y, 0.001f, 3000.f );
	    }
	    return mat;
	}
};

}

#endif

