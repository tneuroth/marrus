#ifndef RENDERER_2D_HPP
#define RENDERER_2D_HPP

#include "Glyph2D.hpp"

#include "geometry/Vec.hpp"
#include "render/Texture/Texture.hpp"
#include "render/TextRenderer/TextRenderer.hpp"
#include "views/PressButtonWidget.hpp"
#include "views/Viewport/ViewPort.hpp"
#include "views/TabView.hpp"
#include "views/TreeView.hpp"
#include "views/ComboWidget.hpp"
#include "views/SliderWidget.hpp"
#include "views/ExpressionEdit.hpp"
#include "utils/StrUtils.hpp"

#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/gtx/transform.hpp>

#include "render/GLErrorChecking.hpp"

#include <fstream>
#include <cstdlib>
#include <iostream>

#include <chrono> 
using namespace std::chrono;

namespace TN {

class Renderer2D
{
    std::vector< GLFWwindow * > m_windows;

    int windowWidth;
    int windowHeight;

    int currentWindow;
    std::vector< GLuint > m_vaos;

    std::string m_baseDirectory;

    GLuint voronoiBuffer;
    GLuint quadBuffer;
    GLuint glyphBuffer;
    GLuint positionBuffer;
    GLuint scalarBuffer;
    GLuint colorBuffer;    
    GLuint filterBuffer;

    GLuint xBuffer;
    GLuint yBuffer;    
    GLuint flagBuffer;
    GLuint elementbuffer;

    GLuint bitmaskedProgram;
    GLuint bitmaskedScatterProgram;   
    GLuint bitmaskedScatterColorProgram;   
    GLuint shaderProgram;
    GLuint textureProgram;
    GLuint solidTextureProgram;
    GLuint geo2DProgram;
    GLuint categoricalProgram;    
    GLuint plot3dProgram;    
    GLuint rgbTexProgram;     
    GLuint clProgramAlpha2D;    
    GLuint clProgram2D;    
    GLuint flat2DPrg;    

    int m_discardMode;

    TN::TextRenderer m_textRenderer;

    std::vector< TN::Vec2< float > > defaultGlyphGeometry;

    int MAX_GLYPHS = 1000000;

    glm::mat4 transform;

    TN::TextureLayer scalarTexture;

    TN::Texture1D3 tfTexture;
    TN::Texture1D3 tfTexture2;
    TN::Texture1D3 tfTexture3;

    std::string m_tf;
    std::string m_tf2;

    void loadTF( const std::string & path, TN::Texture1D3 & tex )
    {
        std::ifstream inFile( path );
        std::string line;
        std::vector< TN::Vec3< float > > colors;    
        
        while( std::getline( inFile, line ) )
        {
            std::string rgb[ 3 ];
            for( int i = 1, cr = 0; i < line.size() && cr < 3; ++i )
            {
                if( line[i] == ',' )
                {
                    ++cr;
                    continue;
                }
                else if( line[ i ] == ')' )
                {
                    break;
                }
                else if( line[ i ] == ' ' )
                {
                    continue;
                }
                else
                {
                    rgb[ cr ].push_back( line[ i ] );
                }
            }
    
            colors.push_back( TN::Vec3< float >(
                std::stof( rgb[ 0 ] ),
                std::stof( rgb[ 1 ] ),
                std::stof( rgb[ 2 ] ) ) );
    
            
            for( int i = 0; i < m_vaos.size(); ++i )
            {
                setWindow( i );
                tex.load( colors );
            }
            
            setWindow( currentWindow );            
        }
    }

    const char * GetGLErrorStr(GLenum err)
    {
        switch (err)
        {
            case GL_NO_ERROR:          return "No error";
            case GL_INVALID_ENUM:      return "Invalid enum";
            case GL_INVALID_VALUE:     return "Invalid value";
            case GL_INVALID_OPERATION: return "Invalid operation";
            case GL_STACK_OVERFLOW:    return "Stack overflow";
            case GL_STACK_UNDERFLOW:   return "Stack underflow";
            case GL_OUT_OF_MEMORY:     return "Out of memory";
            default:                   return "Unknown error";
        }
    }

    void CheckGLError( const std::string & s )
    {
        while (true)
        {
            const GLenum err = glGetError();
            if (GL_NO_ERROR == err)
                break;

            std::cout << s << "->";
            std::cout << "GL Error: " << GetGLErrorStr(err) << std::endl;
          
            exit( 1 ); 
        }
    }

    void loadShaders( const std::string & vert, const std::string & frag, GLuint & shader )
    {
        std::ifstream tv( vert );
        std::string vertexShaderStr( ( std::istreambuf_iterator<char>( tv ) ),
                         std::istreambuf_iterator<char>( ) );

        std::ifstream tf( frag );
        std::string fragmentShaderStr( ( std::istreambuf_iterator<char>( tf ) ),
                         std::istreambuf_iterator<char>( ) );

        const char * vertexShaderSrc   = vertexShaderStr.c_str();
        const char * fragmentShaderSrc = fragmentShaderStr.c_str();

        // vertex shader
        int vertexShader = GL_CHECK_CALL_ASSIGN( glCreateShader( GL_VERTEX_SHADER ) );
        GL_CHECK_CALL( glShaderSource( vertexShader, 1, & vertexShaderSrc, NULL ) );
        GL_CHECK_CALL( glCompileShader(vertexShader) );

        // check for shader compile errors
        int success;
        char infoLog[512];
        GL_CHECK_CALL( glGetShaderiv( vertexShader, GL_COMPILE_STATUS, &success ) );
        if (!success)
        {
            glGetShaderInfoLog( vertexShader, 512, NULL, infoLog );
            std::cout << "ERROR::SHADER::VERTEX::COMPILATION_FAILED\n" << infoLog << std::endl;
        }

        // fragment shader
        int fragmentShader = GL_CHECK_CALL_ASSIGN( glCreateShader(GL_FRAGMENT_SHADER) );
        GL_CHECK_CALL( glShaderSource(fragmentShader, 1, & fragmentShaderSrc, NULL) );
        GL_CHECK_CALL( glCompileShader(fragmentShader) );

        // check for shader compile errors
        GL_CHECK_CALL( glGetShaderiv(fragmentShader, GL_COMPILE_STATUS, &success) );
        if (!success)
        {
            GL_CHECK_CALL( glGetShaderInfoLog(fragmentShader, 512, NULL, infoLog) );
            std::cout << "ERROR::SHADER::FRAGMENT::COMPILATION_FAILED\n" << infoLog << std::endl;
        }

        // link shaders
        shader = GL_CHECK_CALL_ASSIGN( glCreateProgram() );
        GL_CHECK_CALL( glAttachShader( shader, vertexShader   ) );
        GL_CHECK_CALL( glAttachShader( shader, fragmentShader ) );
        GL_CHECK_CALL( glLinkProgram(  shader) );

        // check for linking errors
        GL_CHECK_CALL( glGetProgramiv( shader, GL_LINK_STATUS, &success) );
        if (!success) 
        {
            GL_CHECK_CALL( glGetProgramInfoLog( shader, 512, NULL, infoLog) );
            std::cout << "ERROR::SHADER::PROGRAM::LINKING_FAILED\n" << infoLog << std::endl;
        }

        GL_CHECK_CALL( glDeleteShader( vertexShader   ) );
        GL_CHECK_CALL( glDeleteShader( fragmentShader ) );

        /***************************************************************************************/

        CheckGLError( "compiled shaders" );     
    }

    void genBuffers()
    {
        GLfloat vertices[] = { 
            // Pos      // Tex
            -1.0f, 1.0f, 0.0f, 1.0f,
            1.0f, -1.0f, 1.0f, 0.0f,
            -1.0f, -1.0f, 0.0f, 0.0f, 
        
            -1.0f, 1.0f, 0.0f, 1.0f,
            1.0f, 1.0f, 1.0f, 1.0f,
            1.0f, -1.0f, 1.0f, 0.0f
        };
 
        m_vaos.resize( m_windows.size() );

        for( int i = 0, end = m_vaos.size(); i < end; ++i )
        {
            setWindow( i );
            GL_CHECK_CALL( glGenVertexArrays( 1, &m_vaos[ i ] ) );
        }
        
        GL_CHECK_CALL( glGenBuffers( 1, & voronoiBuffer  ) );
        GL_CHECK_CALL( glGenBuffers( 1, & quadBuffer     ) );
        GL_CHECK_CALL( glGenBuffers( 1, & glyphBuffer    ) );
        GL_CHECK_CALL( glGenBuffers( 1, & positionBuffer ) );
        GL_CHECK_CALL( glGenBuffers( 1, & scalarBuffer   ) );
        GL_CHECK_CALL( glGenBuffers( 1, & colorBuffer    ) );        
        GL_CHECK_CALL( glGenBuffers( 1, & filterBuffer   ) );

        GL_CHECK_CALL( glGenBuffers( 1, & xBuffer    ) );
        GL_CHECK_CALL( glGenBuffers( 1, & yBuffer    ) );        
        GL_CHECK_CALL( glGenBuffers( 1, & flagBuffer ) );
        GL_CHECK_CALL( glGenBuffers( 1, & elementbuffer ) );

        GL_CHECK_CALL( glBindVertexArray( m_vaos[ 0 ] ) );

        GL_CHECK_CALL( glBindBuffer( GL_ARRAY_BUFFER, quadBuffer ) );     
        GL_CHECK_CALL( glBufferData( GL_ARRAY_BUFFER, 24 * sizeof( float ), vertices, GL_STATIC_DRAW ) );          

        GL_CHECK_CALL( glBindBuffer( GL_ARRAY_BUFFER, glyphBuffer ) );
        GL_CHECK_CALL( glBufferData( GL_ARRAY_BUFFER, defaultGlyphGeometry.size() * 2 * sizeof( float ), defaultGlyphGeometry.data(), GL_STATIC_DRAW ) );

     //    GL_CHECK_CALL( glBindBuffer( GL_ARRAY_BUFFER, voronoiBuffer ) );
     //    GL_CHECK_CALL( glBufferData( GL_ARRAY_BUFFER, MAX_GLYPHS * 24 * 2 * sizeof( float ), NULL, GL_STREAM_DRAW ) );

        // GL_CHECK_CALL( glBindBuffer( GL_ARRAY_BUFFER, positionBuffer ) );
        // GL_CHECK_CALL( glBufferData( GL_ARRAY_BUFFER, MAX_GLYPHS * 3 * sizeof( float ), NULL, GL_STREAM_DRAW ) );

        // GL_CHECK_CALL( glBindBuffer( GL_ARRAY_BUFFER, scalarBuffer) );
        // GL_CHECK_CALL( glBufferData( GL_ARRAY_BUFFER, MAX_GLYPHS * 3 * sizeof( float ), NULL, GL_STREAM_DRAW ) );

        
    }

    void freeBuffers()
    {
        GL_CHECK_CALL( glDeleteBuffers( 1, & voronoiBuffer  ) );
        GL_CHECK_CALL( glDeleteBuffers( 1, & quadBuffer     ) );
        GL_CHECK_CALL( glDeleteBuffers( 1, & glyphBuffer    ) );
        GL_CHECK_CALL( glDeleteBuffers( 1, & positionBuffer ) );
        GL_CHECK_CALL( glDeleteBuffers( 1, & scalarBuffer   ) ); 
        GL_CHECK_CALL( glDeleteBuffers( 1, & colorBuffer    ) );             
        GL_CHECK_CALL( glDeleteBuffers( 1, & filterBuffer   ) );  

        GL_CHECK_CALL( glDeleteBuffers( 1, & xBuffer    ) ); 
        GL_CHECK_CALL( glDeleteBuffers( 1, & yBuffer    ) );             
        GL_CHECK_CALL( glDeleteBuffers( 1, & flagBuffer ) );  
        GL_CHECK_CALL( glDeleteBuffers( 1, & elementbuffer ) );  

        scalarTexture.tex.destroy();
        tfTexture.destroy();
        tfTexture2.destroy();
        tfTexture3.destroy();
    }

    void init()
    {
        m_textRenderer.init();

        defaultGlyphGeometry = circle( 1.0f, { 0.f, 0.f }, 17 );   

        genBuffers();

        compileShaders();

        GL_CHECK_CALL( glEnable(    GL_BLEND ) );
        GL_CHECK_CALL( glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA ) );
        GL_CHECK_CALL( glEnable(    GL_MULTISAMPLE ) );        
        GL_CHECK_CALL( glDisable(   GL_DEPTH_TEST  ) );

        tfTexture.create();
        tfTexture2.create();
        tfTexture3.create();

        //loadTF( m_baseDirectory + "/TF/perceptual/viridis.txt", tfTexture  );     
        
        loadTF(   m_baseDirectory + "/TF/perceptual/magma.txt", tfTexture );     
        loadTF(   m_baseDirectory + "/TF/sequential/Reds.txt", tfTexture2 );     
        loadTF( m_baseDirectory + "/TF/sequential/YlOrBr.txt", tfTexture3 );     

        //loadTF( m_baseDirectory + "/TF/perceptual/plasma.txt", tfTexture );    
        // loadTF( m_baseDirectory + "/TF/sequential/BuPu.txt", tfTexture );                                 
    }    

public:

    TN::Texture1D3 & tex3() {
        return tfTexture3;
    }

    void compileShaders()
    {
        const std::string rootPath = m_baseDirectory + "/shaders/";

        loadShaders( rootPath + "bitMaskedScatter.vert", rootPath + "bitMaskedPlot.frag",  bitmaskedScatterProgram );
        loadShaders( rootPath + "bitMaskedPlotColor.vert", rootPath + "bitMaskedPlotColor.frag",  bitmaskedScatterColorProgram );        
        loadShaders( rootPath + "bitMaskedPlot.vert", rootPath + "bitMaskedPlot.frag",  bitmaskedProgram );
        loadShaders( rootPath + "plot.vert",    rootPath + "plot.frag",     shaderProgram );
        loadShaders( rootPath + "plot3d.vert",  rootPath + "plot3d.frag",   plot3dProgram );        
        loadShaders( rootPath + "texture.vert", rootPath + "texture.frag", textureProgram );
        loadShaders( rootPath + "geo.vert",     rootPath + "geo.frag",      geo2DProgram );
        loadShaders( rootPath + "categoricalTexture.vert", rootPath + "categoricalTexture.frag", categoricalProgram );
        loadShaders( rootPath + "texture.vert", rootPath + "textureRGB.frag", rgbTexProgram );
        loadShaders( rootPath + "clPrg2D.vert",     rootPath + "clPrg2D.frag", clProgram2D );
        loadShaders( rootPath + "clPrgAlpha.vert",     rootPath + "clPrgAlpha.frag", clProgramAlpha2D );
        loadShaders( rootPath + "flat2D.vert",     rootPath + "flat2D.frag", flat2DPrg );

        loadShaders( rootPath + "texture.vert", rootPath + "solidTexture.frag", solidTextureProgram );
    }    

    void setTexture( std::vector< float > & data, int width, int height )
    {
        for( int i = 0; i < m_vaos.size(); ++i )
        {
            setWindow( i );
            scalarTexture.tex.load( data, width, height );
            
        } 
        setWindow( currentWindow );
    }

    void renderSlider( 
        TN::SliderWidget & slider,
        const float windowWidth,
        const float windowHeight )
    {
        if( slider.size().x() <= 0 )
        {
            return;
        }

        GL_CHECK_CALL( glViewport(
            slider.position().x(),
            slider.position().y(),
            slider.size().x(),
            slider.size().y() ) );

        std::vector< Vec2< float > > line( 2, Vec2< float >( 0, 0 ) );

        line[0].x( -1 );
        line[1].x(  1 );
        line[0].y(  0 );
        line[1].y(  0 );

        int N_MARKS = 11;
        std::vector< Vec2< float > > marks( N_MARKS*2, Vec2< float >( 0, 0 ) );
        for( int i = 0; i < N_MARKS; ++i )
        {
            marks[ i*2 + 0 ] = { -1 + ( 2.f * i ) / ( N_MARKS-1 ),  0.0f };
            marks[ i*2 + 1 ] = { -1 + ( 2.f * i ) / ( N_MARKS-1 ), -0.75f };
        }

        GL_CHECK_CALL( glLineWidth( 3 ) );
        GL_CHECK_CALL( glViewport( slider.position().x(), slider.position().y(), slider.size().x(), slider.size().y() ) );
        renderPrimitivesFlat(  line, { 1.0, 1.0, 1.0, 1.0 }, GL_LINES );

        GL_CHECK_CALL( glLineWidth( 1 ) );
        GL_CHECK_CALL( glViewport( slider.position().x()+1, slider.position().y()+1, slider.size().x() - 1, slider.size().y() ) );
        renderPrimitivesFlat( line,  { 0, 0, 0, 1.0       }, GL_LINES );
        renderPrimitivesFlat( marks, { 0.4, 0.4, 0.4, 1.0 }, GL_LINES );

        float pos = slider.sliderPosition();
        GL_CHECK_CALL( glViewport( slider.position().x() + pos * slider.size().x() - 10, slider.position().y(), 20, slider.size().y() ) );
        clear( { .3f, .3f, .3f, 1.f } );
        renderPopSquare();

        m_textRenderer.renderText(
            windowWidth,
            windowHeight,
            slider.text(),
            { slider.textX() - 10.f, slider.textY() },
            { 0.0f, 0.0f, 0.0f, 1.0f },
            false, 
            16 );
    }


    void renderCategoricalTexture( int nClasses )
    {
        CheckGLError( "before render texture" );         

        TN::Texture2D1 & tex = scalarTexture.tex;
        TN::Texture1D3 & tf  = tfTexture; 

        GL_CHECK_CALL( glUseProgram( categoricalProgram ) );

        GL_CHECK_CALL( glBindVertexArray( m_vaos[ currentWindow ] ) );             

        GL_CHECK_CALL( glBindBuffer( GL_ARRAY_BUFFER, quadBuffer ) );  

        GLuint texID = GL_CHECK_CALL_ASSIGN( glGetUniformLocation( categoricalProgram, "nClasses" ) );
        GL_CHECK_CALL( glUniform1i( texID, nClasses ) );

        GL_CHECK_CALL( glActiveTexture( GL_TEXTURE0 ) );
        tex.bind();

        texID = GL_CHECK_CALL_ASSIGN( glGetUniformLocation( categoricalProgram, "tex" ) );
        GL_CHECK_CALL( glUniform1i( texID, 0 ) );
        
        GL_CHECK_CALL( glGenerateMipmap( GL_TEXTURE_2D ) );      

        GL_CHECK_CALL( glEnableVertexAttribArray( 0 ) );
        GL_CHECK_CALL( glVertexAttribPointer( 0, 4, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), (GLvoid*)0 ) );

        GL_CHECK_CALL( glDrawArrays( GL_TRIANGLES, 0, 6 ) );
             
        GL_CHECK_CALL( glBindVertexArray( 0 ) );
    }

    void renderSolidContextTexture( TN::Texture2D1 & tex, TN::Vec4 color )
    {
        CheckGLError( "before render texture" );         

        GL_CHECK_CALL( glUseProgram( solidTextureProgram ) );

        GL_CHECK_CALL( glBindVertexArray( m_vaos[ currentWindow ] ) ); 

        GL_CHECK_CALL( glBindBuffer( GL_ARRAY_BUFFER, quadBuffer ) );  

        GL_CHECK_CALL( glActiveTexture( GL_TEXTURE0 ) );
        tex.bind();

        GLuint texID = GL_CHECK_CALL_ASSIGN( glGetUniformLocation( solidTextureProgram, "tex" ) );
        GL_CHECK_CALL( glUniform1i( texID, 0 ) );

        GL_CHECK_CALL( glGenerateMipmap( GL_TEXTURE_2D ) );

        GLuint cID = GL_CHECK_CALL_ASSIGN( glGetUniformLocation( solidTextureProgram, "cl" ) );
        GL_CHECK_CALL( glUniform4fv( cID, 1, & color.r ) );                

        GL_CHECK_CALL( glEnableVertexAttribArray( 0 ) );
        GL_CHECK_CALL( glVertexAttribPointer( 0, 4, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), (GLvoid*)0 ) );

        GL_CHECK_CALL( glDrawArrays( GL_TRIANGLES, 0, 6 ) );

        GL_CHECK_CALL( glBindVertexArray( 0 ) );
    }

    void renderHeatMapTexture(
        TN::Texture2D1 & tex,
        float mn,
        float mx,
        TN::Texture1D3 & tfTex )
    {
        CheckGLError( "before render texture" );         

        GL_CHECK_CALL( glUseProgram( textureProgram ) );

        GL_CHECK_CALL( glBindVertexArray( m_vaos[ currentWindow ] ) ); 

        GL_CHECK_CALL( glBindBuffer( GL_ARRAY_BUFFER, quadBuffer ) );  

        GL_CHECK_CALL( glActiveTexture( GL_TEXTURE0 ) );
        tex.bind();

        GL_CHECK_CALL( glActiveTexture(GL_TEXTURE1) );
        tfTex.bind();

        GL_CHECK_CALL( glActiveTexture( GL_TEXTURE0 ) );

        GLuint texID = GL_CHECK_CALL_ASSIGN( glGetUniformLocation( textureProgram, "tex" ) );
        GL_CHECK_CALL( glUniform1i( texID, 0 ) );

        GL_CHECK_CALL( glGenerateMipmap( GL_TEXTURE_2D ) );

        GLuint tfID = GL_CHECK_CALL_ASSIGN( glGetUniformLocation( textureProgram, "tf" ) );
        GL_CHECK_CALL( glUniform1i( tfID, 1 ) );        
    
        GLuint mxID = GL_CHECK_CALL_ASSIGN( glGetUniformLocation( textureProgram, "mx" ) );
        GL_CHECK_CALL( glUniform1f( mxID, mx ) );        

        GLuint mnID = GL_CHECK_CALL_ASSIGN( glGetUniformLocation( textureProgram, "mn" ) );
        GL_CHECK_CALL( glUniform1f( mnID, mn ) );               

        GLuint zaID = GL_CHECK_CALL_ASSIGN( glGetUniformLocation( textureProgram, "zeroAlpha" ) );
        GL_CHECK_CALL( glUniform1i( zaID, m_discardMode == 0 ) );               

        GL_CHECK_CALL( glEnableVertexAttribArray( 0 ) );
        GL_CHECK_CALL( glVertexAttribPointer( 0, 4, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), (GLvoid*)0 ) );

        GL_CHECK_CALL( glDrawArrays( GL_TRIANGLES, 0, 6 ) );

        GL_CHECK_CALL( glBindVertexArray( 0 ) );
    }

    void renderHeatMapTexture(
        TN::Texture2D1 & tex,
        float mn,
        float mx,
        bool secondaryTex = false )
    {
        CheckGLError( "before render texture" );         

        GL_CHECK_CALL( glUseProgram( textureProgram ) );

        GL_CHECK_CALL( glBindVertexArray( m_vaos[ currentWindow ] ) ); 

        GL_CHECK_CALL( glBindBuffer( GL_ARRAY_BUFFER, quadBuffer ) );  

        GL_CHECK_CALL( glActiveTexture( GL_TEXTURE0 ) );
        tex.bind();

        GL_CHECK_CALL( glActiveTexture(GL_TEXTURE1) );
        if( secondaryTex )
        {
            tfTexture2.bind();
        }
        else
        {
            tfTexture.bind();
        }

        GL_CHECK_CALL( glActiveTexture( GL_TEXTURE0 ) );

        GLuint texID = GL_CHECK_CALL_ASSIGN( glGetUniformLocation( textureProgram, "tex" ) );
        GL_CHECK_CALL( glUniform1i( texID, 0 ) );

        GL_CHECK_CALL( glGenerateMipmap( GL_TEXTURE_2D ) );

        GLuint tfID = GL_CHECK_CALL_ASSIGN( glGetUniformLocation( textureProgram, "tf" ) );
        GL_CHECK_CALL( glUniform1i( tfID, 1 ) );        
    
        GLuint mxID = GL_CHECK_CALL_ASSIGN( glGetUniformLocation( textureProgram, "mx" ) );
        GL_CHECK_CALL( glUniform1f( mxID, mx ) );        

        GLuint mnID = GL_CHECK_CALL_ASSIGN( glGetUniformLocation( textureProgram, "mn" ) );
        GL_CHECK_CALL( glUniform1f( mnID, mn ) );               

        GLuint zaID = GL_CHECK_CALL_ASSIGN( glGetUniformLocation( textureProgram, "zeroAlpha" ) );
        GL_CHECK_CALL( glUniform1i( zaID, m_discardMode == 0 ) );               

        GL_CHECK_CALL( glEnableVertexAttribArray( 0 ) );
        GL_CHECK_CALL( glVertexAttribPointer( 0, 4, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), (GLvoid*)0 ) );

        GL_CHECK_CALL( glDrawArrays( GL_TRIANGLES, 0, 6 ) );
    
        GL_CHECK_CALL( glBindVertexArray( 0 ) );
    }

    void renderHeatMapTexture(
        float mn,
        float mx )
    {
        CheckGLError( "before render texture" );         

        TN::Texture2D1 & tex = scalarTexture.tex;
        TN::Texture1D3 & tf  = tfTexture; 

        GL_CHECK_CALL( glUseProgram( textureProgram ) );

        GL_CHECK_CALL( glBindVertexArray( m_vaos[ currentWindow ] ) ); 

        GL_CHECK_CALL( glBindBuffer( GL_ARRAY_BUFFER, quadBuffer ) );  

        GL_CHECK_CALL( glActiveTexture( GL_TEXTURE0 ) );
        tex.bind();

        GL_CHECK_CALL( glActiveTexture(GL_TEXTURE1) );
        tf.bind();

        GL_CHECK_CALL( glActiveTexture( GL_TEXTURE0 ) );

        GLuint texID = GL_CHECK_CALL_ASSIGN( glGetUniformLocation( textureProgram, "tex" ) );
        GL_CHECK_CALL( glUniform1i( texID, 0 ) );

        GL_CHECK_CALL( glGenerateMipmap( GL_TEXTURE_2D ) );

        GLuint tfID = GL_CHECK_CALL_ASSIGN( glGetUniformLocation( textureProgram, "tf" ) );
        GL_CHECK_CALL( glUniform1i( tfID, 1 ) );        
    
        GLuint mxID = GL_CHECK_CALL_ASSIGN( glGetUniformLocation( textureProgram, "mx" ) );
        GL_CHECK_CALL( glUniform1f( mxID, mx ) );        

        GLuint mnID = GL_CHECK_CALL_ASSIGN( glGetUniformLocation( textureProgram, "mn" ) );
        GL_CHECK_CALL( glUniform1f( mnID, mn ) );           

        GLuint zaID = GL_CHECK_CALL_ASSIGN( glGetUniformLocation( textureProgram, "zeroAlpha" ) );
        GL_CHECK_CALL( glUniform1i( zaID, false ) );    

        GL_CHECK_CALL( glEnableVertexAttribArray( 0 ) );
        GL_CHECK_CALL( glVertexAttribPointer( 0, 4, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), (GLvoid*)0 ) );

        GL_CHECK_CALL( glDrawArrays( GL_TRIANGLES, 0, 6 ) );

        GL_CHECK_CALL( glBindVertexArray( 0 ) );
    }

    void clearViewPort( const ViewPort & vp )
    {
        GL_CHECK_CALL( glViewport(
            vp.offsetX(),
            vp.offsetY(),
            vp.width(),
            vp.height()
        ) );

        clear({ vp.bkgColor().r, vp.bkgColor().g, vp.bkgColor().b, vp.bkgColor().a } );
    }

    void renderExpressionEdit( 
        const TN::ExpressionEdit & edit,
        const float windowWidth,
        const float windowHeight,
        const TN::Vec3< float > & defaultColor,
        const std::vector< std::string > & tokens,
        const std::map< std::string, TN::Vec3< float > > & keyWordHighlighting = {} )
    {
        GL_CHECK_CALL( glViewport( 0, 0, windowWidth, windowHeight ) );

        m_textRenderer.renderText(
            windowWidth,
            windowHeight,
            edit.label(),
            { edit.labelX(), edit.labelY() },
            { defaultColor.x(), defaultColor.y(), defaultColor.z(), 1.f },
            false );

        GL_CHECK_CALL( glViewport(
            edit.position().x(),
            edit.position().y(),
            edit.size().x(),
            edit.size().y() ) );

        clearViewPort( edit.viewPort() );
        renderViewportBoxShadow( edit.viewPort() );

        const int TEXT_PAD = 8;

        Vec3< float > opColor( 0.2, 0.2, 0.2 );
        Vec3< float > setColor( 0.0, 0.0, 0.0 );

        GL_CHECK_CALL( glViewport( 0, 0, windowWidth, windowHeight ) );

        std::vector< TN::Vec4 > charColors;

        const std::string txt = edit.text();
        std::string currToken = "";
        int currTokenIndex = 0; 
        TN::Vec3< float > currColor = defaultColor;
        
        if( tokens.size() > 0 )
        {
            if( keyWordHighlighting.count( tokens[ 0 ] ) ) 
            {
                currColor = keyWordHighlighting.at( tokens[ 0 ] );
            }
        }
        
        for( size_t i = 0; i < txt.size(); ++i )
        {
            if( txt[ i ] != ' ' && txt[ i ] != '\t' && txt[ i ] != '\n' )
            {
                currToken += txt[ i ];

                charColors.push_back( { 
                    currColor.r(),
                    currColor.g(),
                    currColor.b(),
                    1.f } );
            }
            else
            {
                charColors.push_back( { 
                    defaultColor.r(),
                    defaultColor.g(),
                    defaultColor.b(),
                    1.f } );
            }

            if( currTokenIndex < tokens.size() )
            {
                if( currToken == tokens[ currTokenIndex ] )
                {
                    ++currTokenIndex;
                    currToken = "";
                    
                    if( currTokenIndex < tokens.size() )
                    {
                        if( keyWordHighlighting.count( tokens[ currTokenIndex ] ) )
                        {
                            currColor = keyWordHighlighting.at( tokens[ currTokenIndex ] );
                        }
                        else
                        {
                            currColor = defaultColor;
                        }
                    }
                }
            }
            else
            {
                currColor = defaultColor;
            }
        }

        const float CursorPos = m_textRenderer.getWidthAt(
            edit.text(),
            edit.cursorPos() + 1 );

        float textStart = edit.position().x() + TEXT_PAD - 4;
        float editEnd   = edit.position().x() + edit.size().x() - 4;

        float D = ( textStart + CursorPos ) - editEnd;
        float Scroll = 0.f;

        if( D > 0.f )
        {
             textStart -= D;
        }

        std::vector< Vec2< float > > charPositions;

        m_textRenderer.renderText(
            windowWidth,
            windowHeight,
            edit.text(),
            { textStart, edit.position().y() + TEXT_PAD },
            charColors,
            false,
            20,
            charPositions,
            { 
                { edit.position().x(), edit.position().x() + edit.size().x() },
                { edit.position().y(), edit.position().y() + edit.size().y() }
            } );

        if( edit.hasFocus() )
        {
            float px = edit.position().x(); 

            if( edit.text().size() > 0 )
            {
                int cp = edit.cursorPos();
                if( cp >=0 )
                {
                    px = charPositions[ cp ].x() - 4;
                    if( edit.text()[ cp ] == ' ' && edit.text().size() > 1 )
                    {
                        px += 4;
                    }
                }
            }

            GL_CHECK_CALL( glViewport(
                px,
                edit.position().y() + 2,
                9,
                edit.size().y() - 2
            ) );

            std::vector< Vec2< float > > cur = { { 0, -1 }, { 0, 1 } };
            GL_CHECK_CALL( glLineWidth( 1 ) );
            renderPrimitivesFlat( cur, { 0.4, 0.4, 0.4, 1.0 }, GL_LINES );
        }
    }


    void renderExpressionEdit( 
        const TN::ExpressionEdit & edit,
        const float windowWidth,
        const float windowHeight,
        const TN::Vec3< float > & defaultColor,
        const std::map< std::string, TN::Vec3< float > > & keyWordHighlighting = {} )
    {
        GL_CHECK_CALL( glViewport( 0, 0, windowWidth, windowHeight ) );

        m_textRenderer.renderText(
            windowWidth,
            windowHeight,
            edit.label(),
            { edit.labelX(), edit.labelY() },
            { defaultColor.x(), defaultColor.y(), defaultColor.z(), 1.f },
            false );

        GL_CHECK_CALL( glViewport(
            edit.position().x(),
            edit.position().y(),
            edit.size().x(),
            edit.size().y() ) );

        clearViewPort( edit.viewPort() );
        renderViewportBoxShadow( edit.viewPort() );

        const int TEXT_PAD = 8;

        Vec3< float > opColor( 0.2, 0.2, 0.2 );
        Vec3< float > setColor( 0.0, 0.0, 0.0 );

        GL_CHECK_CALL( glViewport( 0, 0, windowWidth, windowHeight ) );

        std::vector< TN::Vec4 > charColors;
        for( auto & c : edit.text() )
        {
            charColors.push_back( { 
                defaultColor.r(),
                defaultColor.g(),
                defaultColor.b(),
                1.f } );
        }

        const float CursorPos = m_textRenderer.getWidthAt(
            edit.text(),
            edit.cursorPos() + 1 );

        float textStart = edit.position().x() + TEXT_PAD - 4;
        float editEnd   = edit.position().x() + edit.size().x() - 4;

        float D = ( textStart + CursorPos ) - editEnd;
        float Scroll = 0.f;

        if( D > 0.f )
        {
             textStart -= D;
        }

        std::vector< Vec2< float > > charPositions;

        m_textRenderer.renderText(
            windowWidth,
            windowHeight,
            edit.text(),
            { textStart, edit.position().y() + TEXT_PAD },
            charColors,
            false,
            charPositions,
            { 
                { edit.position().x(), edit.position().x() + edit.size().x() },
                { edit.position().y(), edit.position().y() + edit.size().y() }
            } );

        if( edit.hasFocus() )
        {
            float px = edit.position().x(); 

            if( edit.text().size() > 0 )
            {
                int cp = edit.cursorPos();
                if( cp >=0 )
                {
                    px = charPositions[ cp ].x() - 4;
                    if( edit.text()[ cp ] == ' ' && edit.text().size() > 1 )
                    {
                        px += 4;
                    }
                }
            }

            GL_CHECK_CALL( glViewport(
                px,
                edit.position().y() + 2,
                9,
                edit.size().y() - 2
            ) );

            std::vector< Vec2< float > > cur = { { 0, -1 }, { 0, 1 } };
            GL_CHECK_CALL( glLineWidth( 1 ) );
            renderPrimitivesFlat( cur, { 0.4, 0.4, 0.4, 1.0 }, GL_LINES );
        }
    }

    void renderComboItems( 
        const TN::ComboWidget & combo,
        const TN::Vec3< float > & textColor,
        double windowWidth,
        double windowHeight )
    {
        const int PADY = 8;

        GL_CHECK_CALL( glViewport( 0, 0, windowWidth, windowHeight ) );

        const auto & textures = combo.textureButtons();

        m_textRenderer.renderText(
            windowWidth, windowHeight,
            combo.text(),
            { combo.textX(), combo.textY() },
            { textColor.r(), textColor.g(), textColor.b(), 1.f },
            false );

        if ( combo.numItems() > 0 )
        {
            if( textures.count( combo.items()[ combo.selectedItem() ] ) ) 
            {
                TN::TexturedPressButton * b = textures.at( combo.items()[ combo.selectedItem() ] ).get();
                b->setPosition( combo.position().x() + 8, combo.position().y() + 4 );
                renderPressButton( b, { 0.2, 0.2, 0.2 }, windowWidth, windowHeight, true );
            }
            else 
            {
                m_textRenderer.renderText( 
                    windowWidth, windowHeight,
                    combo.items()[ combo.selectedItem() ],
                    { combo.position().x() + 8, combo.position().y() + PADY },
                    { textColor.r(), textColor.g(), textColor.b(), 1.f },
                    false );
            }

            if( combo.isPressed() )
            {
                int offset = 1;

                for( int i = 0; i < combo.numItems(); ++i )
                {

                    if( textures.count( combo.items()[ i ] ) ) 
                    {
                        TN::TexturedPressButton * b = textures.at( combo.items()[ i ] ).get();
                        b->setPosition( combo.position().x() + 8, combo.position().y() - combo.size().y() * offset + 4 );
                        renderPressButton( b, { 0.2, 0.2, 0.2 }, windowWidth, windowHeight, true );
                    }
                    else
                    {
                        m_textRenderer.renderText( 
                            windowWidth, windowHeight,
                            combo.items()[ i ],
                            { combo.position().x() + 8, combo.position().y() - combo.size().y() * offset + PADY },
                            { textColor.r(), textColor.g(), textColor.b(), 1.f },
                            false );
                    }

                    offset += 1;
                }
            }
        }
    }
    
    void renderComboButton( 
        TN::ComboWidget & combo )
    {
        GL_CHECK_CALL( glViewport( combo.position().x(), combo.position().y(), combo.size().x(), combo.size().y() ) );
        if( ! combo.flash() )
        {
            clear( { 1, 1, 1, 1 } );
        }
        else
        {
            Vec3< float > c = combo.flashColor()*.5 + Vec3< float >( 1, 1, 1 ) * .5;
            TN::Vec4 cl1( combo.flashColor().r(), combo.flashColor().g(), combo.flashColor().b(), 1 );
            TN::Vec4 cl2( c.r(), c.g(), c.b(), 1 );
            float r = std::abs( std::cos( combo.flashPhase() ) );
            clear( {
                (1-r)*cl1.r + r*cl2.r,
                (1-r)*cl1.g + r*cl2.g,
                (1-r)*cl1.b + r*cl2.b,
                1.f 
            } );
        }
        renderPopSquare();
        GL_CHECK_CALL( glViewport( combo.position().x() + combo.size().x() - 20, combo.position().y(), 20, combo.size().y() ) );
        clear( { .95, .95, .95, 1 } );
        renderPopSquare();

        if( combo.isPressed() )
        {
            for( int i = 1; i < combo.numItems() + 1; ++i )
            {
                GL_CHECK_CALL( glViewport( combo.position().x(), combo.position().y() - ( combo.size().y() )*i, combo.size().x(), combo.size().y() ) );
                clear({ 1, 1, 1, 1 } );
                renderPopSquare();
            }
            if( combo.hoveredItem() >= 0 )
            {
                GL_CHECK_CALL( glViewport( combo.position().x(), combo.position().y() - ( combo.size().y() )*( combo.hoveredItem()+1 ), combo.size().x(), combo.size().y() ) );
                clear( { .7, .8, .9, 1 } );
                renderPopSquare();
            }
        }
    }

    void renderCombo( 
        TN::ComboWidget & combo, 
        const TN::Vec3< float > & textColor,
        double windowWidth,
        double windowHeight )
    {
        renderComboButton( 
            combo );
       
        renderComboItems( 
            combo, 
            textColor,
            windowWidth,
            windowHeight );

        if( combo.isPressed() )
        {
            auto outline = combo.outline();

            GL_CHECK_CALL( glViewport( 0, 0, windowWidth, windowHeight ) );
            GL_CHECK_CALL( glLineWidth( 3.0 ) );
            GL_CHECK_CALL( glPointSize( 3.0 ) );

            for( auto line : outline )
            {
                renderPrimitivesFlat( 
                    { ( line.first / TN::Vec2< float >( windowWidth, windowHeight ) ) * 2.f - 1.f , ( line.second / TN::Vec2< float >( windowWidth, windowHeight ) ) * 2.f - 1.f },
                    TN::Vec4( 0.2, 0.2, 0.2, 0.7 ),
                    GL_LINES
                );

                renderPrimitivesFlat( 
                    { line.first, line.second },
                    TN::Vec4( 0.2, 0.2, 0.2, 0.7 ),
                    GL_POINTS
                );
            }
        }
    }

    ////////////////////////////////////////

    void clearBox( 
        int x, 
        int y, 
        int w, 
        int h, 
        TN::Vec4 cl = { 1.f, 1.f, 1.f, 1.f } )
    {
        GL_CHECK_CALL( glClearColor( cl.r, cl.g, cl.b, cl.a ) );        
        GL_CHECK_CALL( glEnable( GL_SCISSOR_TEST ) );
        GL_CHECK_CALL( glScissor( x, y, w, h ) );
        GL_CHECK_CALL( glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT ) );        
        GL_CHECK_CALL( glDisable( GL_SCISSOR_TEST ) );    
    }

    void clearScreen( TN::Vec4 cl = { 1.f, 1.f, 1.f, 1.f } )
    {
        GL_CHECK_CALL( glClearColor( cl.r, cl.g, cl.b, cl.a ) );
        GL_CHECK_CALL( glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT ) );
    }

    void render( 
        const std::vector< TN::Vec2< float > > & positions,
        const TN::Vec4 & color,
        unsigned int mode = GL_LINES )
    { 
        // update buffers

        GL_CHECK_CALL( glBindVertexArray( m_vaos[ currentWindow ] ) ); 
        GL_CHECK_CALL( glBindBuffer( GL_ARRAY_BUFFER, voronoiBuffer ) );
        GL_CHECK_CALL( glBufferData( GL_ARRAY_BUFFER, positions.size() * 2 * sizeof( float ), positions.data(), GL_DYNAMIC_DRAW ) );

        //********************************************************************************************

        GL_CHECK_CALL( glUseProgram( geo2DProgram ) );

        GL_CHECK_CALL( glEnableVertexAttribArray( 0 ) );
        GL_CHECK_CALL( glBindBuffer( GL_ARRAY_BUFFER, voronoiBuffer ) );

        GL_CHECK_CALL( glVertexAttribPointer(
            0,          // attribute
            2,          // size
            GL_FLOAT,   // type
            GL_FALSE,   // normalized?
            0,          // stride
            (void*)0    // array buffer offset
        ) );

        TN::Vec2< float > mins(   0.f, 0.f );
        TN::Vec2< float > maxes(  1.f, 1.f );
        TN::Vec2< float > widths( maxes.x() - mins.x(), maxes.y() - mins.y() );

        transform = glm::mat4( 1.0 );

        transform = glm::scale( 
            transform,
            glm::vec3( 
                2.0f / widths.x(), 
                2.0f / widths.y(), 
                0.0f ) );

        transform = glm::translate(
            transform, 
            glm::vec3( 
                -( mins.x() + widths.x() / 2.f ), 
                -( mins.y() + widths.y() / 2.f ), 
                0.0f ) );

        GLuint mID = GL_CHECK_CALL_ASSIGN( glGetUniformLocation( shaderProgram, "TR" ) );
        GL_CHECK_CALL( glUniformMatrix4fv( mID, 1, GL_FALSE, &transform[ 0 ][ 0 ] ) );

        GLuint cID = GL_CHECK_CALL_ASSIGN( glGetUniformLocation( shaderProgram, "color" ) );
        GL_CHECK_CALL( glUniform4fv( cID, 1, & color.r ) );

        GL_CHECK_CALL( glDrawArrays( mode, 0, positions.size() ) );
        
        GL_CHECK_CALL( glBindVertexArray( 0 ) ); 
    }

    void renderGrid(
            const TN::Vec2< float > & position,
            const TN::Vec2< float > & size,
            int rows,
            int cols,
            const TN::Vec4 & gridColor,
            const TN::Vec4 & backgroundColor )
    {
        clearBox( 
            position.x(), position.y(), size.x(), size.y(),
            backgroundColor );
    
        static std::vector< TN::Vec2< float > > lines;
        lines.clear();

        rows *= 2;
        cols *= 2;

        float gridSpacingY = 2 / ( float ) rows;
        float gridSpacingX = 2 / ( float ) cols;

        for( int i = 1; i <= rows; ++i )
        {
            lines.push_back( TN::Vec2< float >( -1, -1 + i * gridSpacingY ) );
            lines.push_back( TN::Vec2< float >(  1, -1 + i * gridSpacingY ) );
        }

        for( int j = 1; j <= cols; ++j )
        {
            lines.push_back( TN::Vec2< float >( -1 + j * gridSpacingX, -1 ) );
            lines.push_back( TN::Vec2< float >( -1 + j * gridSpacingX,  1 ) );
        }

        GL_CHECK_CALL( glLineWidth( 1.0 ) );

        GL_CHECK_CALL( glViewport( position.x(), position.y(), size.x(), size.y() ) );

        render( 
            lines,
            gridColor,
            GL_LINES );
    }

    void swapBuffers()
    {
        GL_CHECK_CALL( glfwSwapBuffers( m_windows[ currentWindow ] ) );   
    }


void renderPrimitivesFlat(
    const std::vector< TN::Vec2< float > > & verts,
    const TN::Vec4 & color,
    double WIDTH, 
    double HEIGHT,
    const TN::Vec2< float > & xRange,
    const TN::Vec2< float > & yRange,    
    unsigned int type )
{
    CheckGLError( "Before render primitives flat, ranged." );

    if( ! InputValidator::validatePrimitiveType( type ) )
    {
        return;
    }

    if( verts.size() < InputValidator::getMinVerticesForType( type ) )
    {
        std::cerr << "Error: Too few vertices for primitive type." << std::endl;
        return;
    }

    if( ! InputValidator::validateRange( xRange ) || ! InputValidator::validateRange( yRange ) )
    {
        return;
    }

    TN::Vec2< float > maxes = { xRange.b(), yRange.b() };
    TN::Vec2< float > mins  = { xRange.a(), yRange.a() };

    TN::Vec2< float > widths( maxes.x() - mins.x(), maxes.y() - mins.y() );

    GL_CHECK_CALL( glBindVertexArray( m_vaos[ currentWindow ] ) ); 
    GL_CHECK_CALL( glBindBuffer( GL_ARRAY_BUFFER, positionBuffer ) );
    GL_CHECK_CALL( glBufferData( GL_ARRAY_BUFFER, verts.size() * 2 * sizeof( float ), verts.data(), GL_DYNAMIC_DRAW ) );

    //

    GL_CHECK_CALL( glUseProgram( flat2DPrg ) );

     /////////////////////////////////////////////////////////////////////

    glm::mat4 transform = glm::mat4( 1.0 );

    transform = glm::scale( 
        transform,
        glm::vec3( 
            2.0f / widths.x(), 
            2.0f / widths.y(), 
            0.0f ) );

    transform = glm::translate(
        transform, 
        glm::vec3( 
            -( mins.x() + widths.x() / 2.f ), 
            -( mins.y() + widths.y() / 2.f ), 
            0.0f ) );

    GLuint mID = GL_CHECK_CALL_ASSIGN( glGetUniformLocation( flat2DPrg, "TR" ) );
    GL_CHECK_CALL( glUniformMatrix4fv( mID, 1, GL_FALSE, &transform[ 0 ][ 0 ] ) );

    /////////////////////////////////////////////////////////////////////

    mID = GL_CHECK_CALL_ASSIGN( glGetUniformLocation( flat2DPrg, "color" ) );
    GL_CHECK_CALL( glUniform4fv( mID, 1, & color.r ) );

    GL_CHECK_CALL( glBindVertexArray( m_vaos[ currentWindow ] ) ); 

    GL_CHECK_CALL( glEnableVertexAttribArray( 0 ) );
    GL_CHECK_CALL( glBindBuffer( GL_ARRAY_BUFFER, positionBuffer ) );
    GL_CHECK_CALL( glVertexAttribPointer(
        0, 
        2, 
        GL_FLOAT, 
        GL_FALSE, 
        0, 
        (void*)0 
    ) );

    GL_CHECK_CALL( glVertexAttribDivisor( 0, 0 ) ); 

    

    GL_CHECK_CALL( glDrawArrays( type, 0, verts.size() ) );

    GL_CHECK_CALL( glBindVertexArray( 0 ) ); 
    
}

void renderPrimitivesFlat(
    const std::vector< TN::Vec2< float > > & verts,
    const TN::Vec4 & color,
    unsigned int type )
{
    CheckGLError( "Before render primitives flat." );

    if( ! InputValidator::validatePrimitiveType( type ) )
    {
        return;
    }

    if( verts.size() < InputValidator::getMinVerticesForType( type ) )
    {
        std::cerr << "Error: Too few vertices for primitive type." << std::endl;
        return;
    }

    GL_CHECK_CALL( glBindVertexArray( m_vaos[ currentWindow ] ) ); 

    GL_CHECK_CALL( glBindBuffer( GL_ARRAY_BUFFER, positionBuffer ) );
    GL_CHECK_CALL( glBufferData( GL_ARRAY_BUFFER, verts.size() * 2 * sizeof( float ), verts.data(), GL_DYNAMIC_DRAW ) );

    //

    GL_CHECK_CALL( glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA) );
    GL_CHECK_CALL( glEnable (GL_BLEND) );
    GL_CHECK_CALL( glEnable (GL_LINE_SMOOTH) );
    GL_CHECK_CALL( glHint (GL_LINE_SMOOTH_HINT, GL_NICEST) );

    GL_CHECK_CALL( glUseProgram( flat2DPrg ) );

    glm::mat4 TR = glm::mat4( 1.0 );
    GLuint mID = GL_CHECK_CALL_ASSIGN( glGetUniformLocation( flat2DPrg, "TR" ) );
    GL_CHECK_CALL( glUniformMatrix4fv( mID, 1, GL_FALSE, &TR[ 0 ][ 0 ] ) );

    mID = GL_CHECK_CALL_ASSIGN( glGetUniformLocation( flat2DPrg, "color" ) );
    GL_CHECK_CALL( glUniform4fv( mID, 1, & color.r ) );

    GL_CHECK_CALL( glBindVertexArray( m_vaos[ currentWindow ] ) ); 

    GL_CHECK_CALL( glEnableVertexAttribArray( 0 ) );
    GL_CHECK_CALL( glBindBuffer( GL_ARRAY_BUFFER, positionBuffer ) );
    GL_CHECK_CALL( glVertexAttribPointer(
        0, 
        2, 
        GL_FLOAT, 
        GL_FALSE, 
        0, 
        (void*)0 
    ) );

    GL_CHECK_CALL( glVertexAttribDivisor( 0, 0 ) ); 

    

    GL_CHECK_CALL( glDrawArrays( type, 0, verts.size() ) );

    GL_CHECK_CALL( glBindVertexArray( 0 ) ); 
    
}

void renderPrimitivesColored(
    const std::vector< TN::Vec2< float > > & verts,
    const std::vector< TN::Vec4 > & colors,
    unsigned int type )
{
    CheckGLError( "Before render primitives vec4 colored." );

    if( ! InputValidator::validatePrimitiveType( type ) )
    {
        return;
    }

    if( verts.size() < InputValidator::getMinVerticesForType( type ) )
    {
        std::cerr << "Error: Too few vertices for primitive type." << std::endl;
        return;
    }

    GL_CHECK_CALL( glBindVertexArray( m_vaos[ currentWindow ] ) ); 

    GL_CHECK_CALL( glBindBuffer( GL_ARRAY_BUFFER, positionBuffer ) );
    GL_CHECK_CALL( glBufferData( GL_ARRAY_BUFFER, verts.size() * 2 * sizeof( float ), verts.data(), GL_DYNAMIC_DRAW ) );

    GL_CHECK_CALL( glBindBuffer( GL_ARRAY_BUFFER, colorBuffer ) );
    GL_CHECK_CALL( glBufferData( GL_ARRAY_BUFFER, colors.size() * 4 * sizeof( float ), colors.data(), GL_DYNAMIC_DRAW ) );

    GL_CHECK_CALL( glUseProgram( clProgramAlpha2D ) );

    glm::mat4 TR = glm::mat4( 1.0 );
    GLuint mID = GL_CHECK_CALL_ASSIGN( glGetUniformLocation( clProgramAlpha2D, "TR" ) );
    GL_CHECK_CALL( glUniformMatrix4fv( mID, 1, GL_FALSE, &TR[ 0 ][ 0 ] ) );

    

    GL_CHECK_CALL( glBindVertexArray( m_vaos[ currentWindow ] ) ); 

    GL_CHECK_CALL( glEnableVertexAttribArray( 0 ) );
    GL_CHECK_CALL( glBindBuffer( GL_ARRAY_BUFFER, positionBuffer ) );
    GL_CHECK_CALL( glVertexAttribPointer(
        0, 
        2, 
        GL_FLOAT, 
        GL_FALSE, 
        0, 
        (void*)0 
    ) );

    GL_CHECK_CALL( glEnableVertexAttribArray( 1 ) );
    GL_CHECK_CALL( glBindBuffer( GL_ARRAY_BUFFER, colorBuffer ) );
    GL_CHECK_CALL( glVertexAttribPointer(
        1, 
        4, 
        GL_FLOAT, 
        GL_FALSE, 
        0, 
        (void*)0 
    ) );

    GL_CHECK_CALL( glVertexAttribDivisor( 0, 0 ) ); 
    GL_CHECK_CALL( glVertexAttribDivisor( 1, 0 ) ); 
    GL_CHECK_CALL( glDrawArrays( type, 0, verts.size() ) );
    GL_CHECK_CALL( glBindVertexArray( 0 ) ); 
}

void renderPrimitivesColored(
    const std::vector< TN::Vec2< float > > & verts,
    const std::vector< TN::Vec3< float > > & colors,
    unsigned int type )
{
    CheckGLError( "Before render primitives vec3 colored." );

    if( ! InputValidator::validatePrimitiveType( type ) )
    {
        return;
    }

    if( verts.size() < InputValidator::getMinVerticesForType( type ) )
    {
        std::cerr << "Error: Too few vertices for primitive type." << std::endl;
        return;
    }

    GL_CHECK_CALL( glBindVertexArray( m_vaos[ currentWindow ] ) ); 

    GL_CHECK_CALL( glBindBuffer( GL_ARRAY_BUFFER, positionBuffer ) );
    GL_CHECK_CALL( glBufferData( GL_ARRAY_BUFFER, verts.size() * 2 * sizeof( float ), verts.data(), GL_DYNAMIC_DRAW ) );

    GL_CHECK_CALL( glBindBuffer( GL_ARRAY_BUFFER, colorBuffer ) );
    GL_CHECK_CALL( glBufferData( GL_ARRAY_BUFFER, colors.size() * 3 * sizeof( float ), colors.data(), GL_DYNAMIC_DRAW ) );

    GL_CHECK_CALL( glUseProgram( clProgram2D ) );

    glm::mat4 TR = glm::mat4( 1.0 );
    GLuint mID = GL_CHECK_CALL_ASSIGN( glGetUniformLocation( clProgram2D, "TR" ) );

    GL_CHECK_CALL( glUniformMatrix4fv( mID, 1, GL_FALSE, &TR[ 0 ][ 0 ] ) );
    GL_CHECK_CALL( glBindVertexArray( m_vaos[ currentWindow ] ) ); 
    GL_CHECK_CALL( glEnableVertexAttribArray( 0 ) );
    GL_CHECK_CALL( glBindBuffer( GL_ARRAY_BUFFER, positionBuffer ) );
    GL_CHECK_CALL( glVertexAttribPointer(
        0, 
        2, 
        GL_FLOAT, 
        GL_FALSE, 
        0, 
        (void*)0 
    ) );

    GL_CHECK_CALL( glEnableVertexAttribArray( 1 ) );
    GL_CHECK_CALL( glBindBuffer( GL_ARRAY_BUFFER, colorBuffer ) );
    GL_CHECK_CALL( glVertexAttribPointer(
        1, 
        3, 
        GL_FLOAT, 
        GL_FALSE, 
        0, 
        (void*)0 
    ) );

    GL_CHECK_CALL( glVertexAttribDivisor( 0, 0 ) ); 
    GL_CHECK_CALL( glVertexAttribDivisor( 1, 0 ) ); 

    GL_CHECK_CALL( glDrawArrays( type, 0, verts.size() ) );

    GL_CHECK_CALL( glBindVertexArray( 0 ) ); 
}


    //////////////////////////////////////////////////////////////////////////////////

    void clear(
        const TN::Vec4 & color )
    {
        std::vector< TN::Vec2< float > > S =
        {
            TN::Vec2< float >( -1.f,  1.f ),
            TN::Vec2< float >( -1.f, -1.f ),
            TN::Vec2< float >(  1.f,  1.f ),
            TN::Vec2< float >(  1.f,  1.f ),
            TN::Vec2< float >( -1.f, -1.f ),
            TN::Vec2< float >(  1.f, -1.f )
        };

        renderPrimitivesFlat(
            S,
            color,
            GL_TRIANGLES );
    }

    void renderPopSquare( double lineWidth )
    {
        std::vector< TN::Vec2< float > > S =
        {
            TN::Vec2< float >( -1.f,  1.f ),
            TN::Vec2< float >(  1.f,  1.f ),
            TN::Vec2< float >( 1.f,  1.f ),
            TN::Vec2< float >( 1.f, -1.f ),
            TN::Vec2< float >( 1.f, -1.f ),
            TN::Vec2< float >( -1.f,  -1.f ),
            TN::Vec2< float >( -1.f, -1.f ),
            TN::Vec2< float >( -1.f,  1.f )
        };

        std::vector< TN::Vec4 > colors =
        {
            { 1.f, 1.f, 1.f,   .4 },
            { 1.f, 1.f, 1.f,   .4 },

            {  0.f, 0.f, 0.f, .25 },
            {  0.f, 0.f, 0.f, .25 },

            {  0.f, 0.f, 0.f, .25 },
            {  0.f, 0.f, 0.f, .25 },

            { 1.f, 1.f, 1.f, .4 },
            { 1.f, 1.f, 1.f, .4 }
        };

        GL_CHECK_CALL( glLineWidth( lineWidth ) );

        renderPrimitivesColored(
            S,
            colors,
            GL_LINES );
    }

    void renderPopSquare()
    {
        renderPopSquare( 2.0 );
    }

    void renderTexture( 
        TN::Texture2D3 & tex,
        const TN::Vec2< float > & pos,
        const TN::Vec2< float > & size )
    {
        GL_CHECK_CALL( glEnable(GL_BLEND) );
        GL_CHECK_CALL( glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA) );

        GL_CHECK_CALL( glViewport( pos.x(), pos.y(), size.x(), size.y() ) );

        GL_CHECK_CALL( glUseProgram( rgbTexProgram ) );

        GL_CHECK_CALL( glBindVertexArray( m_vaos[ currentWindow ] ) ); 

        GL_CHECK_CALL( glActiveTexture(GL_TEXTURE0) );
        tex.bind();

        //GL_CHECK_CALL( glGenerateMipmap( GL_TEXTURE_2D ) );

        unsigned texID = GL_CHECK_CALL_ASSIGN( glGetUniformLocation( rgbTexProgram, "tex" ) );
         
        GL_CHECK_CALL( glUniform1i( texID, 0 ) );
    
        GL_CHECK_CALL( glBindBuffer(  GL_ARRAY_BUFFER, quadBuffer ) );  

        GL_CHECK_CALL( glEnableVertexAttribArray( 0 ) );

        GL_CHECK_CALL( glVertexAttribPointer( 0, 4, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), (GLvoid*)0 ) );

        GL_CHECK_CALL( glDrawArrays( GL_TRIANGLES, 0, 6 ) );
    }

    void renderTexture( unsigned int id )
    {
        GL_CHECK_CALL( glEnable(GL_BLEND) );
        GL_CHECK_CALL( glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA) );

        GL_CHECK_CALL( glUseProgram( rgbTexProgram ) );
        GL_CHECK_CALL( glBindVertexArray( m_vaos[ currentWindow ] ) ); 

        GL_CHECK_CALL( glActiveTexture(GL_TEXTURE0) );
        GL_CHECK_CALL( glBindTexture( GL_TEXTURE_2D, id ) );

        //GL_CHECK_CALL( glGenerateMipmap( GL_TEXTURE_2D ) );

        unsigned texID = GL_CHECK_CALL_ASSIGN( glGetUniformLocation( rgbTexProgram, "tex" ) );
         
        GL_CHECK_CALL( glUniform1i( texID, 0 ) );
         
        GL_CHECK_CALL( glBindBuffer(  GL_ARRAY_BUFFER, quadBuffer ) );  

        GL_CHECK_CALL( glEnableVertexAttribArray( 0 ) );

        GL_CHECK_CALL( glVertexAttribPointer( 0, 4, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), (GLvoid*)0 ) );

        GL_CHECK_CALL( glDrawArrays( GL_TRIANGLES, 0, 6 ) );
    }

    void renderViewportBoxShadow( const TN::ViewPort & vp )
    {
        GL_CHECK_CALL( glViewport( vp.offsetX(), vp.offsetY(), vp.width(), vp.height() ) );
        renderPrimitivesColored(
            vp.boxShadow().verts(),
            vp.boxShadow().colors(),
            GL_TRIANGLES
        );
    }

    void renderTexturedPressButton( 
        TN::TexturedPressButton * button,
        bool clearBKG )
    {
        GL_CHECK_CALL( glViewport( button->position().x(), button->position().y(), button->size().x(), button->size().y() ) );
        
        if( clearBKG && button->isPressed() )
        {
            clear( { 1.0, 1.0, 1.0, 1 } );
        }

        GL_CHECK_CALL( glEnable(GL_BLEND) );
        GL_CHECK_CALL( glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA) );

        GL_CHECK_CALL( glViewport( button->position().x(), button->position().y(), button->size().x(), button->size().y() ) );

        GL_CHECK_CALL( glUseProgram( rgbTexProgram ) );

        GL_CHECK_CALL( glBindVertexArray( m_vaos[ currentWindow ] ) ); 

        GL_CHECK_CALL( glActiveTexture(GL_TEXTURE0) );
        button->bind( button->isPressed() );

        //GL_CHECK_CALL( glGenerateMipmap( GL_TEXTURE_2D ) );

        unsigned texID = GL_CHECK_CALL_ASSIGN( glGetUniformLocation( rgbTexProgram, "tex" ) );
        GL_CHECK_CALL( glUniform1i( texID, 0 ) );
         
        GL_CHECK_CALL( glBindBuffer(  GL_ARRAY_BUFFER, quadBuffer ) );  

        GL_CHECK_CALL( glEnableVertexAttribArray( 0 ) );

        GL_CHECK_CALL( glVertexAttribPointer( 0, 4, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), (GLvoid*)0 ) );

        GL_CHECK_CALL( glDrawArrays( GL_TRIANGLES, 0, 6 ) );

        if( button->isPressed() )
        {
            renderPrimitivesColored(
                button->boxShadow().verts(),
                button->boxShadow().colors(),
                GL_TRIANGLES
            );
        }
        else
        {
            if( button->emboss() )
            {
                renderPopSquare();
            }
            // clear({ 1, 0, 0, 1 } );
        }
    }

    void renderPressButton( 
        TN::PressButton * button,
        TN::Vec4 color )
    {
        GL_CHECK_CALL( glViewport( button->position().x(), button->position().y(), button->size().x(), button->size().y() ) );        
        clear( color );

        if( button->isPressed() )
        {
            renderPrimitivesColored(
                button->boxShadow().verts(),
                button->boxShadow().colors(),
                GL_TRIANGLES
            );
        }
        else
        {
            renderPopSquare();
        }
    }

    void renderPressButton( 
        TN::PressButton * button,
        bool clearBKG )
    {
        if( button->textured() )
        {
            renderTexturedPressButton( dynamic_cast< TN::TexturedPressButton* >( button ), clearBKG );
            return;
        }
        else if( button->isPressed() )
        {
            GL_CHECK_CALL( glViewport( button->position().x(), button->position().y(), button->size().x(), button->size().y() ) );
            if( clearBKG )
            {
                auto c = button->color();
                clear( { c.r(), c.g(), c.b(), 1 } );
            }

            renderPrimitivesColored(
                button->boxShadow().verts(),
                button->boxShadow().colors(),
                GL_TRIANGLES
            );
        }
        else
        {
            GL_CHECK_CALL( glViewport( button->position().x(), button->position().y(), button->size().x(), button->size().y() ) );
            if( button->clearColor() )
            {
                auto c = button->color();
                if( c.r() != 1.f || c.g() != 1.f || c.b() != 1.f )
                {
                    clear( { c.r(), c.g(), c.b(), 1 } );
                }
            }
            renderPopSquare();
        }
    }

    void renderPressButton( 
        TN::PressButton * button, 
        const TN::Vec3< float > & textColor,
        double windowWidth,
        double windowHeight,
        TN::Vec4 color )
    {
        renderPressButton( button, color );
        GL_CHECK_CALL( glViewport( 0, 0, windowWidth, windowHeight ) );

        m_textRenderer.renderText(
            windowWidth,
            windowHeight,
            button->text(),
            { button->textX(), button->textY() },
            { textColor.x(), textColor.y(), textColor.z(), 1.f },
            false );
    }

    void renderPressButton( 
        TN::PressButton * button, 
        const TN::Vec3< float > & textColor,
        double windowWidth,
        double windowHeight,
        bool clearBKG )
    {
        if( button->textured() )
        {
            renderTexturedPressButton( dynamic_cast< TN::TexturedPressButton* >( button ), clearBKG );
            return;
        }

        renderPressButton( button, clearBKG );
        GL_CHECK_CALL( glViewport( 0, 0, windowWidth, windowHeight ) );

        m_textRenderer.renderText(
            windowWidth,
            windowHeight,
            button->text(),
            { button->textX(), button->textY() },
            { textColor.x(), textColor.y(), textColor.z(), 1.f },
            false );
    }




    ///////////////////////////////////////////////////////////////////

    void renderPoints( 
        const std::vector< TN::Vec2< float > > & positions,
        const std::vector< float > & stippleValues,
        double WIDTH, 
        double HEIGHT,
        const TN::Vec2< float > & xRange = { 0.f, 1.f },
        const TN::Vec2< float > & yRange = { 0.f, 1.f },
        TN::Vec4 color = { 0, 0, 0, 1 },
        float scale = 1.f )
    { 
        // update buffers

        if( positions.size() < 1 )
        {
            return;
        }

        TN::Vec2< float > maxes = { xRange.b(), yRange.b() };
        TN::Vec2< float > mins  = { xRange.a(), yRange.a() };
        TN::Vec2< float > widths( maxes.x() - mins.x(), maxes.y() - mins.y() );
        
        if( widths.x() <= 0.0 || widths.y() <= 0.0 )
        {
            return;
        }

        GL_CHECK_CALL( glFinish() );

        auto time = high_resolution_clock::now();

        GL_CHECK_CALL( glBindVertexArray( m_vaos[ currentWindow ] ) ); 

        GL_CHECK_CALL( glBindBuffer( GL_ARRAY_BUFFER, positionBuffer ) );
        GL_CHECK_CALL( glBufferData( GL_ARRAY_BUFFER, positions.size() * 2 * sizeof( float ), positions.data(), GL_DYNAMIC_DRAW ) );

        

        GL_CHECK_CALL( glBindBuffer( GL_ARRAY_BUFFER, scalarBuffer) );
        GL_CHECK_CALL( glBufferData( GL_ARRAY_BUFFER, stippleValues.size() * sizeof( float ), stippleValues.data(), GL_DYNAMIC_DRAW ) );
        

        GL_CHECK_CALL( glFinish() );

         // std::cout << "buffer data took: " 
         //      << duration_cast<milliseconds>( high_resolution_clock::now() - time ).count() 
         //      << " ms" << std::endl;

        time = high_resolution_clock::now(); 

        //********************************************************************************************

        GL_CHECK_CALL( glUseProgram( shaderProgram ) );

        GL_CHECK_CALL( glEnableVertexAttribArray( 0 ) );
        GL_CHECK_CALL( glBindBuffer( GL_ARRAY_BUFFER, glyphBuffer ) );

        GL_CHECK_CALL( glVertexAttribPointer(
            0,          // attribute
            2,          // size
            GL_FLOAT,   // type
            GL_FALSE,   // normalized?
            0,          // stride
            (void*)0    // array buffer offset
        ) );

        GL_CHECK_CALL( glEnableVertexAttribArray( 1 ) );
        GL_CHECK_CALL( glBindBuffer( GL_ARRAY_BUFFER, positionBuffer ) );

        GL_CHECK_CALL( glVertexAttribPointer(
            1,          
            2,          
            GL_FLOAT,   
            GL_FALSE,   
            0,          
            (void*)0    
        ) );

        GL_CHECK_CALL( glEnableVertexAttribArray( 2 ) );
        GL_CHECK_CALL( glBindBuffer( GL_ARRAY_BUFFER, scalarBuffer ) );
        GL_CHECK_CALL( glVertexAttribPointer(
            2, 
            1, 
            GL_FLOAT, 
            GL_FALSE, 
            0, 
            (void*)0 
        ) );
      
        transform = glm::mat4( 1.0 );

        transform = glm::scale( 
            transform,
            glm::vec3( 
                2.0f / widths.x(), 
                2.0f / widths.y(), 
                0.0f ) );

        transform = glm::translate(
            transform, 
            glm::vec3( 
                -( mins.x() + widths.x() / 2.f ), 
                -( mins.y() + widths.y() / 2.f ), 
                0.0f ) );

        scale = scale *  widths.x() / 2.0;

        // glm::mat4 ortho = glm::ortho( -1.f, 1.f, -1.f, 1.f );
        // transform =  ortho * transform;

        GLuint mID = GL_CHECK_CALL_ASSIGN( glGetUniformLocation( shaderProgram, "TR" ) );
        GL_CHECK_CALL( glUniformMatrix4fv( mID, 1, GL_FALSE, &transform[ 0 ][ 0 ] ) );

        GLuint cID = GL_CHECK_CALL_ASSIGN( glGetUniformLocation( shaderProgram, "color" ) );
        GL_CHECK_CALL( glUniform4fv( cID, 1, & color.r ) );

        GLuint sId = GL_CHECK_CALL_ASSIGN( glGetUniformLocation( shaderProgram, "xScale" ) );
        GL_CHECK_CALL( glUniform1f( sId, scale ) );

        sId = GL_CHECK_CALL_ASSIGN( glGetUniformLocation( shaderProgram, "yScale" ) );
        GL_CHECK_CALL( glUniform1f( sId, ( widths.y() / widths.x() ) * scale ) );

        GL_CHECK_CALL( glVertexAttribDivisor( 0, 0 ) ); // particles vertices : always reuse the same 4 vertices -> 0
        GL_CHECK_CALL( glVertexAttribDivisor( 1, 1 ) ); // positions : one per quad (its center) -> 1
        GL_CHECK_CALL( glVertexAttribDivisor( 2, 1 ) ); // color : one per quad -> 1

        GL_CHECK_CALL( glDrawArraysInstanced( GL_TRIANGLES, 0, defaultGlyphGeometry.size(), positions.size() ) );
    
        GL_CHECK_CALL( glBindVertexArray( 0 ) ); 

        GL_CHECK_CALL( glFinish() );

        // std::cout << "render data took: " 
        //     << duration_cast<milliseconds>( high_resolution_clock::now() - time ).count() 
        //     << " ms" << std::endl;
    }

void renderPoints( 
        const std::vector< float > & x,
        const std::vector< float > & y,        
        const std::vector< int   > & flags,
        int bitMask,
        double WIDTH, 
        double HEIGHT,
        const TN::Vec2< float > & xRange = { 0.f, 1.f },
        const TN::Vec2< float > & yRange = { 0.f, 1.f },
        TN::Vec4 color = { 0, 0, 0, 1 },
        float scale = 1.f )
    { 
        // update buffers

        if( x.size() < 1 )
        {
            return;
        }

        TN::Vec2< float > maxes = { xRange.b(), yRange.b() };
        TN::Vec2< float > mins  = { xRange.a(), yRange.a() };

        TN::Vec2< float > widths( maxes.x() - mins.x(), maxes.y() - mins.y() );
        
        if( widths.x() <= 0.0 || widths.y() <= 0.0 )
        {
            return;
        }

        GL_CHECK_CALL( glFinish() );

        auto time = high_resolution_clock::now();

        GL_CHECK_CALL( glBindVertexArray( m_vaos[ currentWindow ] ) ); 

        GL_CHECK_CALL( glBindBuffer( GL_ARRAY_BUFFER, xBuffer ) );
        GL_CHECK_CALL( glBufferData( GL_ARRAY_BUFFER, x.size() * sizeof( float ), x.data(), GL_DYNAMIC_DRAW ) );

        GL_CHECK_CALL( glBindBuffer( GL_ARRAY_BUFFER, yBuffer) );
        GL_CHECK_CALL( glBufferData( GL_ARRAY_BUFFER, y.size() * sizeof( float ), y.data(), GL_DYNAMIC_DRAW ) );

        GL_CHECK_CALL( glBindBuffer( GL_ARRAY_BUFFER, flagBuffer) );
        GL_CHECK_CALL( glBufferData( GL_ARRAY_BUFFER, flags.size() * sizeof( int ), flags.data(), GL_DYNAMIC_DRAW ) );

        GL_CHECK_CALL( glFinish() );

         // std::cout << "buffer data took: " 
         //      << duration_cast<milliseconds>( high_resolution_clock::now() - time ).count() 
         //      << " ms" << std::endl;

        time = high_resolution_clock::now(); 

        //********************************************************************************************

        GL_CHECK_CALL( glUseProgram( bitmaskedProgram ) );

        GL_CHECK_CALL( glEnableVertexAttribArray( 0 ) );
        GL_CHECK_CALL( glBindBuffer( GL_ARRAY_BUFFER, glyphBuffer ) );

        GL_CHECK_CALL( glVertexAttribPointer(
            0,          // attribute
            2,          // size
            GL_FLOAT,   // type
            GL_FALSE,   // normalized?
            0,          // stride
            (void*)0    // array buffer offset
        ) );

        GL_CHECK_CALL( glEnableVertexAttribArray( 1 ) );
        GL_CHECK_CALL( glBindBuffer( GL_ARRAY_BUFFER, xBuffer ) );

        GL_CHECK_CALL( glVertexAttribPointer(
            1,          // attribute
            1,          // size
            GL_FLOAT,   // type
            GL_FALSE,   // normalized?
            0,          // stride
            (void*)0    // array buffer offset
        ) );

        

        GL_CHECK_CALL( glEnableVertexAttribArray( 2 ) );
        GL_CHECK_CALL( glBindBuffer( GL_ARRAY_BUFFER, yBuffer ) );

        GL_CHECK_CALL( glVertexAttribPointer(
            2,          
            1,          
            GL_FLOAT,   
            GL_FALSE,   
            0,          
            (void*)0    
        ) );

        

        GL_CHECK_CALL( glEnableVertexAttribArray( 3 ) );
        GL_CHECK_CALL( glBindBuffer( GL_ARRAY_BUFFER, flagBuffer ) );
        GL_CHECK_CALL( glVertexAttribIPointer(
            3, 
            1, 
            GL_INT, 
            0, 
            (void*)0 
        ) );
      
        transform = glm::mat4( 1.0 );

        transform = glm::scale( 
            transform,
            glm::vec3( 
                2.0f / widths.x(), 
                2.0f / widths.y(), 
                0.0f ) );

        transform = glm::translate(
            transform, 
            glm::vec3( 
                -( mins.x() + widths.x() / 2.f ), 
                -( mins.y() + widths.y() / 2.f ), 
                0.0f ) );

        scale = scale *  widths.x() / 2.0;

        // glm::mat4 ortho = glm::ortho( -1.f, 1.f, -1.f, 1.f );
        // transform =  ortho * transform;

        GLuint mID = GL_CHECK_CALL_ASSIGN( glGetUniformLocation( bitmaskedProgram, "TR" ) );
        GL_CHECK_CALL( glUniformMatrix4fv( mID, 1, GL_FALSE, &transform[ 0 ][ 0 ] ) );

        GLuint cID = GL_CHECK_CALL_ASSIGN( glGetUniformLocation( bitmaskedProgram, "color" ) );
        GL_CHECK_CALL( glUniform4fv( cID, 1, & color.r ) );

        GLuint sId = GL_CHECK_CALL_ASSIGN( glGetUniformLocation( bitmaskedProgram, "xScale" ) );
        GL_CHECK_CALL( glUniform1f( sId, scale ) );

        sId = GL_CHECK_CALL_ASSIGN( glGetUniformLocation( bitmaskedProgram, "yScale" ) );
        GL_CHECK_CALL( glUniform1f( sId, ( widths.y() / widths.x() ) * scale ) );

        sId = GL_CHECK_CALL_ASSIGN( glGetUniformLocation( bitmaskedProgram, "bitMask" ) );
        GL_CHECK_CALL( glUniform1i( sId, bitMask ) );

        GL_CHECK_CALL( glVertexAttribDivisor( 0, 0 ) );
        GL_CHECK_CALL( glVertexAttribDivisor( 1, 1 ) );
        GL_CHECK_CALL( glVertexAttribDivisor( 2, 1 ) );
        GL_CHECK_CALL( glVertexAttribDivisor( 3, 1 ) );        

        GL_CHECK_CALL( glDrawArraysInstanced( GL_TRIANGLES, 0, defaultGlyphGeometry.size(), x.size() ) );
    
        GL_CHECK_CALL( glBindVertexArray( 0 ) ); 

        GL_CHECK_CALL( glFinish() );

        // std::cout << "render data took: " 
        //     << duration_cast<milliseconds>( high_resolution_clock::now() - time ).count() 
        //     << " ms" << std::endl;
    }

void scatterPoints( 
        const std::vector< float > & x,
        const std::vector< float > & y,        
        const std::vector< TN::Vec3< float > > & colors,
        const std::vector< int32_t   > & flags,      
        const std::vector< uint32_t   > & indices,                
        int bitMask,
        const TN::Vec2< float > & xRange = { 0.f, 1.f },
        const TN::Vec2< float > & yRange = { 0.f, 1.f },
        float scale = 1.f )
    { 
        // update buffers
        GL_CHECK_CALL( glFinish() );

        auto time = high_resolution_clock::now();

        GL_CHECK_CALL( glBindVertexArray( m_vaos[ currentWindow ] ) ); 

        GL_CHECK_CALL( glBindBuffer( GL_ARRAY_BUFFER, xBuffer ) );
        GL_CHECK_CALL( glBufferData( GL_ARRAY_BUFFER, x.size() * sizeof( float ), x.data(), GL_DYNAMIC_DRAW ) );

        GL_CHECK_CALL( glBindBuffer( GL_ARRAY_BUFFER, yBuffer) );
        GL_CHECK_CALL( glBufferData( GL_ARRAY_BUFFER, y.size() * sizeof( float ), y.data(), GL_DYNAMIC_DRAW ) );

        GL_CHECK_CALL( glBindBuffer( GL_ARRAY_BUFFER, colorBuffer ) );
        GL_CHECK_CALL( glBufferData( GL_ARRAY_BUFFER, colors.size() * 3 * sizeof( float ), colors.data(), GL_DYNAMIC_DRAW ) );

        GL_CHECK_CALL( glBindBuffer( GL_ARRAY_BUFFER, flagBuffer) );
        GL_CHECK_CALL( glBufferData( GL_ARRAY_BUFFER, flags.size() * sizeof( int ), flags.data(), GL_DYNAMIC_DRAW ) );

        GL_CHECK_CALL( glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementbuffer) );
        GL_CHECK_CALL( glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(uint32_t), indices.data(), GL_DYNAMIC_DRAW ) );

        GL_CHECK_CALL( glFinish() );

         // std::cout << "buffer data took: " 
         //      << duration_cast<milliseconds>( high_resolution_clock::now() - time ).count() 
         //      << " ms" << std::endl;

        time = high_resolution_clock::now(); 

        //********************************************************************************************

        GL_CHECK_CALL( glUseProgram( bitmaskedScatterColorProgram ) );

        GL_CHECK_CALL( glEnableVertexAttribArray( 0 ) );
        GL_CHECK_CALL( glBindBuffer( GL_ARRAY_BUFFER, xBuffer ) );

        GL_CHECK_CALL( glVertexAttribPointer(
            0,          // attribute
            1,          // size
            GL_FLOAT,   // type
            GL_FALSE,   // normalized?
            0,          // stride
            (void*)0    // array buffer offset
        ) );

        GL_CHECK_CALL( glEnableVertexAttribArray( 1 ) );
        GL_CHECK_CALL( glBindBuffer( GL_ARRAY_BUFFER, yBuffer ) );

        GL_CHECK_CALL( glVertexAttribPointer(
            1,          
            1,          
            GL_FLOAT,   
            GL_FALSE,   
            0,          
            (void*)0    
        ) );

        GL_CHECK_CALL( glEnableVertexAttribArray( 2 ) );
        GL_CHECK_CALL( glBindBuffer( GL_ARRAY_BUFFER, colorBuffer ) );
        GL_CHECK_CALL( glVertexAttribPointer(
            2, 
            3, 
            GL_FLOAT, 
            GL_FALSE, 
            0, 
            (void*)0 
        ) );
      
        GL_CHECK_CALL( glEnableVertexAttribArray( 3 ) );
        GL_CHECK_CALL( glBindBuffer( GL_ARRAY_BUFFER, flagBuffer ) );
        GL_CHECK_CALL( glVertexAttribIPointer(
            3, 
            1, 
            GL_INT, 
            0, 
            (void*)0 
        ) );

        TN::Vec2< float > maxes = { xRange.b(), yRange.b() };
        TN::Vec2< float > mins  = { xRange.a(), yRange.a() };

        TN::Vec2< float > widths( maxes.x() - mins.x(), maxes.y() - mins.y() );
        transform = glm::mat4( 1.0 );

        transform = glm::scale( 
            transform,
            glm::vec3( 
                2.0f / widths.x(), 
                2.0f / widths.y(), 
                0.0f ) );

        transform = glm::translate(
            transform, 
            glm::vec3( 
                -( mins.x() + widths.x() / 2.f ), 
                -( mins.y() + widths.y() / 2.f ), 
                0.0f ) );

        // glm::mat4 ortho = glm::ortho( -1.f, 1.f, -1.f, 1.f );
        // transform =  ortho * transform;

        GLuint mID = GL_CHECK_CALL_ASSIGN( glGetUniformLocation( bitmaskedScatterColorProgram, "TR" ) );
        GL_CHECK_CALL( glUniformMatrix4fv( mID, 1, GL_FALSE, &transform[ 0 ][ 0 ] ) );

        GLuint sId = GL_CHECK_CALL_ASSIGN( glGetUniformLocation( bitmaskedScatterColorProgram, "bitMask" ) );
        GL_CHECK_CALL( glUniform1i( sId, bitMask ) );

        // GL_CHECK_CALL( glVertexAttribDivisor( 0, 1 ) );
        // GL_CHECK_CALL( glVertexAttribDivisor( 1, 1 ) );
        // GL_CHECK_CALL( glVertexAttribDivisor( 2, 1 ) );

        GL_CHECK_CALL( glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementbuffer) );
        GL_CHECK_CALL( glDrawElements(
            GL_POINTS,      // mode
            indices.size(),    // count
            GL_UNSIGNED_INT,   // type
            (void*)0           // element array buffer offset
        ) );

        GL_CHECK_CALL( glBindVertexArray( 0 ) ); 

        GL_CHECK_CALL( glFinish() );

        // std::cout << "render data took: " 
        //     << duration_cast<milliseconds>( high_resolution_clock::now() - time ).count() 
        //     << " ms" << std::endl;
    }

void scatterPoints( 
        const std::vector< float > & x,
        const std::vector< float > & y,        
        const std::vector< TN::Vec3< float > > & colors,
        const std::vector< int32_t   > & flags,        
        int bitMask,
        const TN::Vec2< float > & xRange = { 0.f, 1.f },
        const TN::Vec2< float > & yRange = { 0.f, 1.f },
        float scale = 1.f )
    { 

        if( x.size() < 1 )
        {
            return;
        }

        TN::Vec2< float > maxes = { xRange.b(), yRange.b() };
        TN::Vec2< float > mins  = { xRange.a(), yRange.a() };

        TN::Vec2< float > widths( maxes.x() - mins.x(), maxes.y() - mins.y() );
        
        if( widths.x() <= 0.0 || widths.y() <= 0.0 )
        {
            return;
        }

        // update buffers
        GL_CHECK_CALL( glFinish() );

        auto time = high_resolution_clock::now();

        GL_CHECK_CALL( glBindVertexArray( m_vaos[ currentWindow ] ) ); 

        GL_CHECK_CALL( glBindBuffer( GL_ARRAY_BUFFER, xBuffer ) );
        GL_CHECK_CALL( glBufferData( GL_ARRAY_BUFFER, x.size() * sizeof( float ), x.data(), GL_DYNAMIC_DRAW ) );

        GL_CHECK_CALL( glBindBuffer( GL_ARRAY_BUFFER, yBuffer) );
        GL_CHECK_CALL( glBufferData( GL_ARRAY_BUFFER, y.size() * sizeof( float ), y.data(), GL_DYNAMIC_DRAW ) );

        GL_CHECK_CALL( glBindBuffer( GL_ARRAY_BUFFER, colorBuffer ) );
        GL_CHECK_CALL( glBufferData( GL_ARRAY_BUFFER, colors.size() * 3 * sizeof( float ), colors.data(), GL_DYNAMIC_DRAW ) );

        GL_CHECK_CALL( glBindBuffer( GL_ARRAY_BUFFER, flagBuffer) );
        GL_CHECK_CALL( glBufferData( GL_ARRAY_BUFFER, flags.size() * sizeof( int ), flags.data(), GL_DYNAMIC_DRAW ) );

        GL_CHECK_CALL( glFinish() );

         // std::cout << "buffer data took: " 
         //      << duration_cast<milliseconds>( high_resolution_clock::now() - time ).count() 
         //      << " ms" << std::endl;

        time = high_resolution_clock::now(); 

        //********************************************************************************************

        GL_CHECK_CALL( glUseProgram( bitmaskedScatterColorProgram ) );

        GL_CHECK_CALL( glEnableVertexAttribArray( 0 ) );
        GL_CHECK_CALL( glBindBuffer( GL_ARRAY_BUFFER, xBuffer ) );

        GL_CHECK_CALL( glVertexAttribPointer(
            0,          // attribute
            1,          // size
            GL_FLOAT,   // type
            GL_FALSE,   // normalized?
            0,          // stride
            (void*)0    // array buffer offset
        ) );

        GL_CHECK_CALL( glEnableVertexAttribArray( 1 ) );
        GL_CHECK_CALL( glBindBuffer( GL_ARRAY_BUFFER, yBuffer ) );

        GL_CHECK_CALL( glVertexAttribPointer(
            1,          
            1,          
            GL_FLOAT,   
            GL_FALSE,   
            0,          
            (void*)0    
        ) );

        GL_CHECK_CALL( glEnableVertexAttribArray( 2 ) );
        GL_CHECK_CALL( glBindBuffer( GL_ARRAY_BUFFER, colorBuffer ) );
        GL_CHECK_CALL( glVertexAttribPointer(
            2, 
            3, 
            GL_FLOAT, 
            GL_FALSE, 
            0, 
            (void*)0 
        ) );
      
        GL_CHECK_CALL( glEnableVertexAttribArray( 3 ) );
        GL_CHECK_CALL( glBindBuffer( GL_ARRAY_BUFFER, flagBuffer ) );
        GL_CHECK_CALL( glVertexAttribIPointer(
            3, 
            1, 
            GL_INT, 
            0, 
            (void*)0 
        ) );

        transform = glm::mat4( 1.0 );

        transform = glm::scale( 
            transform,
            glm::vec3( 
                2.0f / widths.x(), 
                2.0f / widths.y(), 
                0.0f ) );

        transform = glm::translate(
            transform, 
            glm::vec3( 
                -( mins.x() + widths.x() / 2.f ), 
                -( mins.y() + widths.y() / 2.f ), 
                0.0f ) );

        // glm::mat4 ortho = glm::ortho( -1.f, 1.f, -1.f, 1.f );
        // transform =  ortho * transform;

        GLuint mID = GL_CHECK_CALL_ASSIGN( glGetUniformLocation( bitmaskedScatterColorProgram, "TR" ) );
        GL_CHECK_CALL( glUniformMatrix4fv( mID, 1, GL_FALSE, &transform[ 0 ][ 0 ] ) );

        GLuint sId = GL_CHECK_CALL_ASSIGN( glGetUniformLocation( bitmaskedScatterColorProgram, "bitMask" ) );
        GL_CHECK_CALL( glUniform1i( sId, bitMask ) );

        // GL_CHECK_CALL( glVertexAttribDivisor( 0, 1 ) );
        // GL_CHECK_CALL( glVertexAttribDivisor( 1, 1 ) );
        // GL_CHECK_CALL( glVertexAttribDivisor( 2, 1 ) );

        GL_CHECK_CALL( glDrawArrays( GL_POINTS, 0, x.size() ) );
    
        GL_CHECK_CALL( glBindVertexArray( 0 ) ); 

        GL_CHECK_CALL( glFinish() );

        // std::cout << "render data took: " 
        //     << duration_cast<milliseconds>( high_resolution_clock::now() - time ).count() 
        //     << " ms" << std::endl;
    }

void scatterPoints( 
        const std::vector< float > & x,
        const std::vector< float > & y,        
        const std::vector< int32_t   > & flags,
        int bitMask,
        const TN::Vec2< float > & xRange = { 0.f, 1.f },
        const TN::Vec2< float > & yRange = { 0.f, 1.f },
        TN::Vec4 color = { 0, 0, 0, 1 },
        float scale=1.f )
    { 

        if( x.size() < 1 )
        {
            return;
        }

        TN::Vec2< float > maxes = { xRange.b(), yRange.b() };
        TN::Vec2< float > mins  = { xRange.a(), yRange.a() };

        TN::Vec2< float > widths( maxes.x() - mins.x(), maxes.y() - mins.y() );
        
        if( widths.x() <= 0.0 || widths.y() <= 0.0 )
        {
            return;
        }

        // update buffers
        GL_CHECK_CALL( glFinish() );

        auto time = high_resolution_clock::now();

        GL_CHECK_CALL( glBindVertexArray( m_vaos[ currentWindow ] ) ); 

        GL_CHECK_CALL( glBindBuffer( GL_ARRAY_BUFFER, xBuffer ) );
        GL_CHECK_CALL( glBufferData( GL_ARRAY_BUFFER, x.size() * sizeof( float ), x.data(), GL_DYNAMIC_DRAW ) );

        GL_CHECK_CALL( glBindBuffer( GL_ARRAY_BUFFER, yBuffer) );
        GL_CHECK_CALL( glBufferData( GL_ARRAY_BUFFER, y.size() * sizeof( float ), y.data(), GL_DYNAMIC_DRAW ) );

        GL_CHECK_CALL( glBindBuffer( GL_ARRAY_BUFFER, flagBuffer) );
        GL_CHECK_CALL( glBufferData( GL_ARRAY_BUFFER, flags.size() * sizeof( int ), flags.data(), GL_DYNAMIC_DRAW ) );

        GL_CHECK_CALL( glFinish() );

         // std::cout << "buffer data took: " 
         //      << duration_cast<milliseconds>( high_resolution_clock::now() - time ).count() 
         //      << " ms" << std::endl;

        time = high_resolution_clock::now(); 

        //********************************************************************************************

        GL_CHECK_CALL( glUseProgram( bitmaskedScatterProgram ) );

        GL_CHECK_CALL( glEnableVertexAttribArray( 0 ) );
        GL_CHECK_CALL( glBindBuffer( GL_ARRAY_BUFFER, xBuffer ) );

        GL_CHECK_CALL( glVertexAttribPointer(
            0,          // attribute
            1,          // size
            GL_FLOAT,   // type
            GL_FALSE,   // normalized?
            0,          // stride
            (void*)0    // array buffer offset
        ) );

        GL_CHECK_CALL( glEnableVertexAttribArray( 1 ) );
        GL_CHECK_CALL( glBindBuffer( GL_ARRAY_BUFFER, yBuffer ) );

        GL_CHECK_CALL( glVertexAttribPointer(
            1,          
            1,          
            GL_FLOAT,   
            GL_FALSE,   
            0,          
            (void*)0    
        ) );

        GL_CHECK_CALL( glEnableVertexAttribArray( 2 ) );
        GL_CHECK_CALL( glBindBuffer( GL_ARRAY_BUFFER, flagBuffer ) );
        GL_CHECK_CALL( glVertexAttribIPointer(
            2, 
            1, 
            GL_INT, 
            0, 
            (void*)0 
        ) );
      
        transform = glm::mat4( 1.0 );

        transform = glm::scale( 
            transform,
            glm::vec3( 
                2.0f / widths.x(), 
                2.0f / widths.y(), 
                0.0f ) );

        transform = glm::translate(
            transform, 
            glm::vec3( 
                -( mins.x() + widths.x() / 2.f ), 
                -( mins.y() + widths.y() / 2.f ), 
                0.0f ) );

        // glm::mat4 ortho = glm::ortho( -1.f, 1.f, -1.f, 1.f );
        // transform =  ortho * transform;

        GLuint mID = GL_CHECK_CALL_ASSIGN( glGetUniformLocation( bitmaskedScatterProgram, "TR" ) );
        GL_CHECK_CALL( glUniformMatrix4fv( mID, 1, GL_FALSE, &transform[ 0 ][ 0 ] ) );

        GLuint cID = GL_CHECK_CALL_ASSIGN( glGetUniformLocation( bitmaskedScatterProgram, "color" ) );
        GL_CHECK_CALL( glUniform4fv( cID, 1, & color.r ) );

        GLuint sId = GL_CHECK_CALL_ASSIGN( glGetUniformLocation( bitmaskedScatterProgram, "bitMask" ) );
        GL_CHECK_CALL( glUniform1i( sId, bitMask ) );

        // GL_CHECK_CALL( glVertexAttribDivisor( 0, 1 ) );
        // GL_CHECK_CALL( glVertexAttribDivisor( 1, 1 ) );
        // GL_CHECK_CALL( glVertexAttribDivisor( 2, 1 ) );

        GL_CHECK_CALL( glDrawArrays( GL_POINTS, 0, x.size() ) );

        GL_CHECK_CALL( glBindVertexArray( 0 ) ); 

        GL_CHECK_CALL( glFinish() );

        // std::cout << "render data took: " 
        //     << duration_cast<milliseconds>( high_resolution_clock::now() - time ).count() 
        //     << " ms" << std::endl;
    }

    void renderPressButtonAsPoint( 
        TN::PressButton * button,
        TN::Vec4 colorUnpressed,
        TN::Vec4 colorPressed,        
        TN::Vec4 edgeColor,
        TN::Vec3< float > textColor,
        float windowWidth,
        float windowHeight )
    {
        if( button->size().x() <= 4 
        ||  button->size().y() <= 4 )
        {
            return;
        }

        // renderPrimitivesFlat(
        //     defaultGlyphGeometry,
        //     { 0.2, 0.2, 0.2, 1.0 },
        //     GL_TRIANGLES );

        // GL_CHECK_CALL( glViewport( 
        //     button->position().x() + 2, 
        //     button->position().y() + 2, 
        //     button->size().x()  - 4, 
        //     button->size().y()  - 4 ) );     

        // how to highlight, maybe just need to use edges
        // rather than encode 2 variables

        if( button->isPressed() )
        {
            // GL_CHECK_CALL( glViewport( 
            //     button->position().x()-1, 
            //     button->position().y()-1, 
            //     button->size().x()+2, 
            //     button->size().y()+2 ) );    
            // renderPrimitivesColored(
            //     button->boxShadow().verts(),
            //     button->boxShadow().colors(),
            //     GL_TRIANGLES
            // );

            GL_CHECK_CALL( glViewport( 
                button->position().x() + 1, 
                button->position().y() + 1, 
                button->size().x()-2  , 
                button->size().y()-2 ) );    

            renderPrimitivesFlat(
                defaultGlyphGeometry,
                { 0.0, 0.0, 0.0, 1.0 },
                GL_TRIANGLES );

            GL_CHECK_CALL( glViewport( 
                button->position().x() + 3, 
                button->position().y() + 3, 
                button->size().x() - 6, 
                button->size().y() - 6 ) );    

            renderPrimitivesFlat(
                defaultGlyphGeometry,
                colorPressed,
                GL_TRIANGLES );
        }
        else
        {
 
            GL_CHECK_CALL( glViewport( 
                button->position().x() + 2, 
                button->position().y() + 2, 
                button->size().x() - 4  , 
                button->size().y() - 4 ) );    

            renderPrimitivesFlat(
                defaultGlyphGeometry,
                edgeColor,
                GL_TRIANGLES );

            GL_CHECK_CALL( glViewport( 
                button->position().x() + 3, 
                button->position().y() + 3, 
                button->size().x() - 6, 
                button->size().y() - 6 ) );    

            renderPrimitivesFlat(
                defaultGlyphGeometry,
                colorUnpressed,
                GL_TRIANGLES );
        }

        // m_textRenderer.renderText(
        //     windowWidth,
        //     windowHeight,
        //     button->text(),
        //     { button->textX(), button->textY() },
        //     { textColor.x(), textColor.y(), textColor.z(), 1.f },
        //     false );
    }

void renderPressButtonAsPoint( 
        TN::PressButton * button,
        TN::Vec4 colorUnpressed,
        TN::Vec4 colorPressed,        
        TN::Vec4 edgeColor,
        TN::Vec3< float > textColor,
        float windowWidth,
        float windowHeight,
        float sx,
        float sy )
    {
        if( button->size().x() <= 4 
        ||  button->size().y() <= 4 )
        {
            return;
        }
 

        // renderPrimitivesFlat(
        //     defaultGlyphGeometry,
        //     { 0.2, 0.2, 0.2, 1.0 },
        //     GL_TRIANGLES );

        // GL_CHECK_CALL( glViewport( 
        //     button->position().x() + 2, 
        //     button->position().y() + 2, 
        //     button->size().x()  - 4, 
        //     button->size().y()  - 4 ) );     

        // how to highlight, maybe just need to use edges
        // rather than encode 2 variables

        if( button->isPressed() )
        {
            // GL_CHECK_CALL( glViewport( 
            //     button->position().x()-1, 
            //     button->position().y()-1, 
            //     button->size().x()+2, 
            //     button->size().y()+2 ) );    
            // renderPrimitivesColored(
            //     button->boxShadow().verts(),
            //     button->boxShadow().colors(),
            //     GL_TRIANGLES
            // );

            GL_CHECK_CALL( glViewport( 
                button->position().x() + 1 + sx, 
                button->position().y() + 1 + sy, 
                button->size().x()-2  , 
                button->size().y()-2 ) );    

            renderPrimitivesFlat(
                defaultGlyphGeometry,
                { 0.0, 0.0, 0.0, 1.0 },
                GL_TRIANGLES );

            GL_CHECK_CALL( glViewport( 
                button->position().x() + 3 + sx, 
                button->position().y() + 3 + sy,
                button->size().x() - 6, 
                button->size().y() - 6 ) );    

            renderPrimitivesFlat(
                defaultGlyphGeometry,
                colorPressed,
                GL_TRIANGLES );
        }
        else
        {
 
            GL_CHECK_CALL( glViewport( 
                button->position().x() + 2 + sx, 
                button->position().y() + 2 + sy, 
                button->size().x() - 4  ,  
                button->size().y() - 4 ) );    

            renderPrimitivesFlat(
                defaultGlyphGeometry,
                edgeColor,
                GL_TRIANGLES );

            GL_CHECK_CALL( glViewport( 
                button->position().x() + 3 + sx, 
                button->position().y() + 3 + sy, 
                button->size().x() - 6, 
                button->size().y() - 6 ) );    

            renderPrimitivesFlat(
                defaultGlyphGeometry,
                colorUnpressed,
                GL_TRIANGLES );
        }

        // m_textRenderer.renderText(
        //     windowWidth,
        //     windowHeight,
        //     button->text(),
        //     { button->textX(), button->textY() },
        //     { textColor.x(), textColor.y(), textColor.z(), 1.f },
        //     false );
    }

    void renderView( 
        TN::TreeView * view,
        TN::Vec4 & CL,
        double windowWidth,
        double windowHeight,
        std::vector< TN::Vec3< float > > & layerColors )
    {
        view->applyLayout();

        GL_CHECK_CALL( glViewport( 
            view->position().x(), 
            view->position().y(), 
            view->size().x(), 
            view->size().y() ) );

        // clear( CL );
//        renderPopSquare();

        ///////////////////////////////////////////////////////////

        std::vector< TN::Vec2< float > > layers    = view->layers(); 
        std::vector< TN::Vec2< float > > layerGrid = view->layerGrid();

        if( layerGrid.size() < 2 )
        {
            return;
        }

        for( int i = 0, lidx = layers.size() - 1; i < layerGrid.size(); i += 2, --lidx )
        {
            TN::Vec2< float > pos = layerGrid[ i + 1 ];
            float val;

            if( lidx == -1 )
            {
                val = layers[ 0 ].a();
            } 
            else
            {
                val = layers[ lidx ].b();
            }

            std::string vStr = TN::toStringWFMT( val );
            GL_CHECK_CALL( glViewport( 0, 0, windowWidth, windowHeight ) );

            m_textRenderer.renderText(
                windowWidth,
                windowHeight,
                vStr,
                { 
                    i == 0 ? pos.x() - 10 
                         : i == layerGrid.size() - 2 
                         ? pos.x() - vStr.size() * 7 + 10 
                         : pos.x() - vStr.size() * 7.0 / 2.0, 
                    pos.y() + 24 
                },
                { 0.2f, 0.2f, 0.2f, 1.f },
                false );

            if( i > 0 )
            {
                auto & pos1 = layerGrid[     i ];
                auto & pos2 = layerGrid[ i - 2 ];

                auto & pos1Lower = layerGrid[ i - 1 ];

                GL_CHECK_CALL( glViewport( 
                    pos2.x(), 
                    pos.y() + 2, 
                    pos1.x() - pos2.x(), 
                    12 ) );  

                clear( layerColors[ lidx + 2 ] );

                GL_CHECK_CALL( glViewport( 
                    pos2.x(), 
                    pos2.y(), 
                    pos1.x() - pos2.x(), 
                    ( pos1Lower.y() - pos1.y() ) ) );  

                clear( { 0.9, 0.9, 0.9, 1 } );
            }
        }

        TN::TexturedPressButton * levelSymbol = view->levelSymbol();
        renderTexturedPressButton( 
            levelSymbol,
            false );

        for( auto & p : layerGrid )
        {
            p.x( p.x() * 2.f /  windowWidth - 1 );
            p.y( p.y() * 2.f / windowHeight - 1 );
        }

        GL_CHECK_CALL( glViewport( 0, 0, windowWidth, windowHeight ) );
        GL_CHECK_CALL( glLineWidth( 1 ) );

        renderPrimitivesFlat(
            layerGrid,
            { 0.75f, 0.75f, 0.75f, 1.f },
            GL_LINES );

        ///////////////////////////////////////////////////////////////////////
 
        double width  = 10;
        double margin = 2;

        //////////////////////////////////////////////////////////////////////////

        std::vector< std::vector< TN::Vec2< float > > > edges = view->getEdges();
        for( auto & edge : edges )
        {
            // std::cout << "rendering edge (" << edge[ 0 ].x() << ", " << edge[ 0 ].y() 
                                  // << ")->(" << edge[ 1 ].x() << ", " << edge[ 1 ].y() << std::endl;
            for( auto & p : edge )
            {
                p.x( ( p.x() - width ) * 2.f / windowWidth - 1 );
                p.y( ( p.y() + view->getScroll().y() ) * 2.f / windowHeight - 1  );
            }

            GL_CHECK_CALL( glViewport( 0, 0, windowWidth, windowHeight ) );
            GL_CHECK_CALL( glLineWidth( 2 ) );
            renderPrimitivesFlat(
                edge,
                { 0.2, 0.2, 0.2, 1.0 },
                GL_LINE_STRIP );

            // GL_CHECK_CALL( glPointSize( 6 ) );
            // renderPrimitivesFlat(
            //     edge,
            //     { 0.2f, 0.2f, 0.2f, 1.f },
            //     GL_POINTS );

        }

        /////////////////////////////////////////////////////////////////////

        auto clbmap       = view->getClusterBoundaries();
        auto cln          = view->getNodeClusters();
        auto relableTable = view->getClusterRelableTable();

        // std::cout << "\nrendering cluster nodes" << std::endl;

        TN::Vec3< float > smallSampleColor( 0.65, 0.65, 0.65 );
        TN::Vec4 edgeColor = { 0.65, 0.65, 0.65, 1.0 };

        for( auto & e : clbmap )
        {
            // std::cout << e.first << std::endl;

            auto clusterProperties = 
                cln[ relableTable[ e.first ] ];

            if( clusterProperties.singular )
            {
                // continue;
            }

            auto & cb = e.second;

            TN::Vec2< float > b1 = cb.first;
            TN::Vec2< float > b2 = { 
                cb.first.x(),
                cb.first.y() + cb.second.y() };

            ///////////////////////////////////////////////////////////////////////////
            // edge/background

            GL_CHECK_CALL( glViewport( 
                b1.x() - width-2, 
                b1.y() + view->getScroll().y(),
                width, 
                cb.second.y() ) );  

            clear( edgeColor );

            ///////////////////////////////////////////////////////////////////////////
            // edge/background node ends

            GL_CHECK_CALL( glViewport( 
                b1.x() - width-2, 
                b1.y() + view->getScroll().y() - width/2.0, 
                width  , 
                width ) );    

            renderPrimitivesFlat(
                defaultGlyphGeometry,
                edgeColor,
                GL_TRIANGLES );

            GL_CHECK_CALL( glViewport( 
                b1.x() - width-2, 
                b2.y() + view->getScroll().y() - width / 2.0, 
                width  , 
                width ) );    

            renderPrimitivesFlat(
                defaultGlyphGeometry,
                edgeColor,
                GL_TRIANGLES );

            ///////////////////////////////////////////////////////////////////////////
            // inner

            GL_CHECK_CALL( glViewport( 
                b1.x() - width - 2 + margin, 
                b1.y() + view->getScroll().y(),
                width - 2*margin, 
                cb.second.y() ) );  

            auto clFill = clusterProperties.encodings.sampleTooSmall ? smallSampleColor
                : clusterProperties.encodings.m5Color;

            clear( { clFill.r(), clFill.g(), clFill.b(), 1.f }  );

            ///////////////////////////////////////////////////////////////////////////
            // inner node ends

            GL_CHECK_CALL( glViewport( 
                b1.x() - width - 2 + margin, 
                b1.y() + view->getScroll().y() - ( width - margin*2 ) / 2.0,
                width - 2*margin, 
                width - 2*margin ) );    

            renderPrimitivesFlat(
                defaultGlyphGeometry,
                { clFill.r(), clFill.g(), clFill.b(), 1.f } ,
                GL_TRIANGLES );

            GL_CHECK_CALL( glViewport( 
                b1.x() - width - 2 + margin, 
                b2.y() + view->getScroll().y() - ( width - margin*2 ) / 2.0,
                width - 2*margin, 
                width - 2*margin ) );    

            renderPrimitivesFlat(
                defaultGlyphGeometry,
                { clFill.r(), clFill.g(), clFill.b(), 1.f },
                GL_TRIANGLES );
        }


        int nIdx = 0;
        for( auto & node : view->nodes() )
        {
            // button->setPressed( true );

            //renderPressButton( button, { 1.0, 0.5, 0.3, 1.0 } );
            // renderPressButton( 
            //     button, 
            //     { 0.f, 0.f, 0.f },
            //     windowWidth,
            //     windowHeight,
            //     {  1.0, 1.0, 1.0, 1.0 } );

            // the empty node
            if( nIdx == 0 )
            {
                renderPressButtonAsPoint( 
                    &( node->button ), 
                    { 0, 0, 0, 1 },
                    { 0, 0, 0, 1 },
                    { 0, 0, 0, 1 },
                    { 0.2, 0.2, 0.2 },
                    windowWidth,
                    windowHeight,
                    view->getScroll().x(),
                    view->getScroll().y() );
            }
            else
            {
                renderPressButtonAsPoint( 
                    &( node->button ), 
                    node->encodings.sampleTooSmall ? smallSampleColor : node->encodings.m5Color,
                    node->encodings.sampleTooSmall ? smallSampleColor : node->encodings.m5Color,
                    edgeColor,
                    { 0.2, 0.2, 0.2 },
                    windowWidth,
                    windowHeight,
                    view->getScroll().x(),
                    view->getScroll().y() );
            }
            ++nIdx;
        }

        // std::cout << "rendered nodes " << std::endl;
    }

    void renderTabView( 
        TN::TabView & toolPanel,
        const TN::Vec2< float > & buttonPosition )
    {
        // GL_CHECK_CALL( glViewport( 
        //     toolPanel.position().x(), 
        //     toolPanel.position().y(), 
        //     toolPanel.size().x(), 
        //     toolPanel.size().y() ) );

        std::vector< TN::PanelElement * > e = toolPanel.elements();
        int index = 0;;
        for( auto & pe : e )
        {
            pe->tabButton.setPosition( 
                buttonPosition.x() + index * pe->tabButton.size().x(), 
                buttonPosition.y() );
            renderPressButton( & ( pe->tabButton ), true );
            +index;
        }
    }

    void renderTabView( 
        TN::TabView & toolPanel )
    {
        GL_CHECK_CALL( glViewport( toolPanel.position().x(), toolPanel.position().y(), toolPanel.size().x(), toolPanel.size().y() ) );

        std::vector< TN::PanelElement * > e = toolPanel.elements();
        for( auto & pe : e )
        {
            renderPressButton( & ( pe->tabButton ), true );
        }
    }

void renderPoints( 
        const std::vector< TN::Vec2< float > > & positions,
        const std::vector< float > & stippleValues,
        const std::vector< float > & stippleFilter,        
        double WIDTH, 
        double HEIGHT,
        const TN::Vec2< float > & xRange = { 0.f, 1.f },
        const TN::Vec2< float > & yRange = { 0.f, 1.f },
        TN::Vec4 color = { 0, 0, 0, 1 },
        float scale = 1.f )
    { 
        // update buffers


        if( positions.size() < 1 )
        {
            return;
        }

        TN::Vec2< float > maxes = { xRange.b(), yRange.b() };
        TN::Vec2< float > mins  = { xRange.a(), yRange.a() };
        TN::Vec2< float > widths( maxes.x() - mins.x(), maxes.y() - mins.y() );
        
        if( widths.x() <= 0.0 || widths.y() <= 0.0 )
        {
            return;
        }

        GL_CHECK_CALL( glBindVertexArray( m_vaos[ currentWindow ] ) ); 

        GL_CHECK_CALL( glBindBuffer( GL_ARRAY_BUFFER, positionBuffer ) );
        GL_CHECK_CALL( glBufferData( GL_ARRAY_BUFFER, positions.size() * 2 * sizeof( float ), positions.data(), GL_DYNAMIC_DRAW ) );

        GL_CHECK_CALL( glBindBuffer( GL_ARRAY_BUFFER, scalarBuffer) );
        GL_CHECK_CALL( glBufferData( GL_ARRAY_BUFFER, stippleValues.size() * sizeof( float ), stippleValues.data(), GL_DYNAMIC_DRAW ) );
        
        GL_CHECK_CALL( glBindBuffer( GL_ARRAY_BUFFER, filterBuffer) );
        GL_CHECK_CALL( glBufferData( GL_ARRAY_BUFFER, stippleFilter.size() * sizeof( float ), stippleFilter.data(), GL_DYNAMIC_DRAW ) );
        
        //********************************************************************************************

        GL_CHECK_CALL( glUseProgram( shaderProgram ) );

        GL_CHECK_CALL( glEnableVertexAttribArray( 0 ) );
        GL_CHECK_CALL( glBindBuffer( GL_ARRAY_BUFFER, glyphBuffer ) );

        GL_CHECK_CALL( glVertexAttribPointer(
            0,          // attribute
            2,          // size
            GL_FLOAT,   // type
            GL_FALSE,   // normalized?
            0,          // stride
            (void*)0    // array buffer offset
        ) );

        GL_CHECK_CALL( glEnableVertexAttribArray( 1 ) );
        GL_CHECK_CALL( glBindBuffer( GL_ARRAY_BUFFER, positionBuffer ) );

        GL_CHECK_CALL( glVertexAttribPointer(
            1,          
            2,          
            GL_FLOAT,   
            GL_FALSE,   
            0,          
            (void*)0    
        ) );

        GL_CHECK_CALL( glEnableVertexAttribArray( 2 ) );
        GL_CHECK_CALL( glBindBuffer( GL_ARRAY_BUFFER, scalarBuffer ) );
        GL_CHECK_CALL( glVertexAttribPointer(
            2, 
            1, 
            GL_FLOAT, 
            GL_FALSE, 
            0, 
            (void*)0 
        ) );
      
        GL_CHECK_CALL( glEnableVertexAttribArray( 3 ) );
        GL_CHECK_CALL( glBindBuffer( GL_ARRAY_BUFFER, filterBuffer ) );
        GL_CHECK_CALL( glVertexAttribPointer(
            3, 
            1, 
            GL_FLOAT, 
            GL_FALSE, 
            0, 
            (void*)0 
        ) );

        transform = glm::mat4( 1.0 );

        transform = glm::scale( 
            transform,
            glm::vec3( 
                2.0f / widths.x(), 
                2.0f / widths.y(), 
                0.0f ) );

        transform = glm::translate(
            transform, 
            glm::vec3( 
                -( mins.x() + widths.x() / 2.f ), 
                -( mins.y() + widths.y() / 2.f ), 
                0.0f ) );

        // glm::mat4 ortho = glm::ortho( -1.f, 1.f, -1.f, 1.f );
        // transform =  ortho * transform;

        GLuint mID = GL_CHECK_CALL_ASSIGN( glGetUniformLocation( shaderProgram, "TR" ) );
        GL_CHECK_CALL( glUniformMatrix4fv( mID, 1, GL_FALSE, &transform[ 0 ][ 0 ] ) );

        GLuint cID = GL_CHECK_CALL_ASSIGN( glGetUniformLocation( shaderProgram, "color" ) );
        GL_CHECK_CALL( glUniform4fv( cID, 1, & color.r ) );

        GLuint sId = GL_CHECK_CALL_ASSIGN( glGetUniformLocation( shaderProgram, "xScale" ) );
        GL_CHECK_CALL( glUniform1f( sId, scale ) );

        sId = GL_CHECK_CALL_ASSIGN( glGetUniformLocation( shaderProgram, "yScale" ) );
        GL_CHECK_CALL( glUniform1f( sId, ( widths.y() / widths.x() ) * scale ) );

        GL_CHECK_CALL( glVertexAttribDivisor( 0, 0 ) ); // particles vertices : always reuse the same 4 vertices -> 0
        GL_CHECK_CALL( glVertexAttribDivisor( 1, 1 ) ); // positions : one per quad (its center) -> 1
        GL_CHECK_CALL( glVertexAttribDivisor( 2, 1 ) ); // color : one per quad -> 1
        GL_CHECK_CALL( glVertexAttribDivisor( 3, 1 ) ); // color : one per quad -> 1
        GL_CHECK_CALL( glDrawArraysInstanced( GL_TRIANGLES, 0, defaultGlyphGeometry.size(), positions.size() ) );
        GL_CHECK_CALL( glBindVertexArray( 0 ) ); 
    }

    void resize( int w, int h )
    {
        if( w != windowWidth || h != windowHeight )
        {
            windowWidth = w;
            windowWidth = h;
            // scalarTexture.resize( w, h );
        }
    }

    void setWindow( int w, bool track = false )
    {
        
        if( track )
        {
            currentWindow = w;
        }

        GL_CHECK_CALL( glfwMakeContextCurrent( m_windows[ w ] ) );

        GL_CHECK_CALL( glEnable(GL_BLEND) );
        GL_CHECK_CALL( glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA) );
        GL_CHECK_CALL( glEnable( GL_MULTISAMPLE ) );        
        GL_CHECK_CALL( glDisable( GL_DEPTH_TEST ) );        
        GL_CHECK_CALL( glLineWidth( 2.0f ) );
        GL_CHECK_CALL( glPointSize( 2.f ) );
    } 

    void cycleDiscardMode()
    {
        m_discardMode++;
        if( m_discardMode > 1 )
        {
            m_discardMode = 0;
        }
    }

    void cycleTF2()
    {
        const std::string base = m_baseDirectory + "/TF/";

        const std::vector< std::string > TFS = 
        {
            base + "perceptual/viridis.txt",
            base + "perceptual/plasma.txt",
            base + "perceptual/inferno.txt",     
            base + "perceptual/magma.txt",
            base + "perceptual/cividis.txt",

            base + "sequential/Purples.txt",
            base + "sequential/Blues.txt",
            base + "sequential/Greens.txt",
            base + "sequential/Oranges.txt",                        
            base + "sequential/Reds.txt",
            base + "sequential/YlOrBr.txt",
            base + "sequential/YlOrRd.txt",
            base + "sequential/OrRd.txt",      
            base + "sequential/PuRd.txt",
            base + "sequential/RdPu.txt",
            base + "sequential/BuPu.txt",
            base + "sequential/GnBu.txt",      
            base + "sequential/PuBu.txt",
            base + "sequential/YlGnBu.txt",
            base + "sequential/PuBuGn.txt",
            base + "sequential/BuGn.txt",      
            base + "sequential/YlGn.txt",

            base + "sequential2/bone.txt",
            // base + "sequential2/pink.txt",
            // base + "sequential2/spring.txt",      
            // base + "sequential2/summer.txt",
            // base + "sequential2/autumn.txt",
            // base + "sequential2/winter.txt",
            // base + "sequential2/cool.txt",      
            // base + "sequential2/Wistia.txt",
            base + "sequential2/hot.txt",
            // base + "sequential2/afmhot.txt",
            // base + "sequential2/gist_heat.txt",      
            base + "sequential2/copper.txt",
            base + "sequential/Greys.txt",            

            // base + "misc/flag.txt",
            // base + "misc/prism.txt",      
            // base + "misc/ocean.txt",
            // base + "misc/gist_earth.txt",
            // base + "misc/terrain.txt",
            // base + "misc/gist_stern.txt",      
            // base + "misc/gnuplot.txt",
            // base + "misc/gnuplot2.txt",
            // base + "misc/CMRmap.txt",
            // base + "misc/cubehelix.txt",      
            // base + "misc/brg.txt",
            //base + "misc/rainbow.txt",
            // base + "misc/jet.txt",
            // base + "misc/nipy_spectral.txt",      
            // base + "misc/gist_ncar.txt",
            // base + "misc/twilight_shifted.txt",

            // base + "diverging/PiYG.txt",
            // base + "diverging/PRGn.txt",
            // base + "diverging/BrBG.txt",
            // base + "diverging/PuOr.txt",      
            // base + "diverging/RdGy.txt",
            // base + "diverging/RdBu.txt",
            // base + "diverging/RdYlBu.txt",
            // base + "diverging/RdYlGn.txt",      
            // base + "diverging/Spectral.txt",
            base + "diverging/coolwarm.txt"
            // base + "diverging/bwr.txt",
            // base + "diverging/seismic.txt"
        };
        static int currentTF2 = 9;    
        ++currentTF2;
        currentTF2 = currentTF2 % TFS.size();
        m_tf2 = TFS[ currentTF2 ];

        loadTF( m_tf2, tfTexture2 );
    
        // std::cout << "tf2" << currentTF2 << std::endl; //9
    }

    void cycleTF()
    {
        const std::string base = m_baseDirectory + "/TF/";

        const std::vector< std::string > TFS = 
        {
            base + "perceptual/viridis.txt",
            base + "perceptual/plasma.txt",
            base + "perceptual/inferno.txt",     
            base + "perceptual/magma.txt",
            base + "perceptual/cividis.txt",

            base + "sequential/Purples.txt",
            base + "sequential/Blues.txt",
            base + "sequential/Greens.txt",
            base + "sequential/Oranges.txt",                        
            base + "sequential/Reds.txt",
            base + "sequential/YlOrBr.txt",
            base + "sequential/YlOrRd.txt",
            base + "sequential/OrRd.txt",      
            base + "sequential/PuRd.txt",
            base + "sequential/RdPu.txt",
            base + "sequential/BuPu.txt",
            base + "sequential/GnBu.txt",      
            base + "sequential/PuBu.txt",
            base + "sequential/YlGnBu.txt",
            base + "sequential/PuBuGn.txt",
            base + "sequential/BuGn.txt",      
            base + "sequential/YlGn.txt",

            base + "sequential2/bone.txt",
            base + "sequential/Greys.txt",

            // base + "sequential2/pink.txt",
            // base + "sequential2/spring.txt",      
            // base + "sequential2/summer.txt",
            // base + "sequential2/autumn.txt",
            // base + "sequential2/winter.txt",
            // base + "sequential2/cool.txt",      
            // base + "sequential2/Wistia.txt",
            base + "sequential2/hot.txt",
            // base + "sequential2/afmhot.txt",
            // base + "sequential2/gist_heat.txt",      
            base + "sequential2/copper.txt",

            // base + "misc/flag.txt",
            // base + "misc/prism.txt",      
            // base + "misc/ocean.txt",
            // base + "misc/gist_earth.txt",
            // base + "misc/terrain.txt",
            // base + "misc/gist_stern.txt",      
            // base + "misc/gnuplot.txt",
            // base + "misc/gnuplot2.txt",
            // base + "misc/CMRmap.txt",
            // base + "misc/cubehelix.txt",      
            // base + "misc/brg.txt",
            //base + "misc/rainbow.txt",
            // base + "misc/jet.txt",
            // base + "misc/nipy_spectral.txt",      
            // base + "misc/gist_ncar.txt",
            // base + "misc/twilight_shifted.txt",

            // base + "diverging/PiYG.txt",
            // base + "diverging/PRGn.txt",
            // base + "diverging/BrBG.txt",
            // base + "diverging/PuOr.txt",      
            // base + "diverging/RdGy.txt",
            // base + "diverging/RdBu.txt",
            // base + "diverging/RdYlBu.txt",
            // base + "diverging/RdYlGn.txt",      
            // base + "diverging/Spectral.txt",
            base + "diverging/coolwarm.txt"
            // base + "diverging/bwr.txt",
            // base + "diverging/seismic.txt"
        };

        static int currentTF = 9;    
        ++currentTF;
        currentTF = currentTF % TFS.size();
        m_tf = TFS[ currentTF ];

        loadTF( m_tf, tfTexture );
    
        // std::cout << "tf1" << currentTF << std::endl;
    }

    // void setTF( const std::string & TF )
    // {
    //     m_tf = TF;
    //     loadTF( m_tf, tfTexture );
    // }

    Renderer2D( std::vector< GLFWwindow * > windows, const std::string & _baseDir ) : m_windows( windows ), m_textRenderer( _baseDir )
    {
        m_baseDirectory = _baseDir;
        currentWindow       = 0;
        glyphBuffer         = 0;
        positionBuffer      = 0;
        scalarBuffer        = 0;
        filterBuffer        = 0;        
        shaderProgram       = 0;
        bitmaskedScatterProgram    = 0;
        bitmaskedScatterColorProgram    = 0;        
        bitmaskedProgram    = 0;
        categoricalProgram  = 0;      
        colorBuffer         = 0;  
        rgbTexProgram  = 0;      

        xBuffer    = 0;
        yBuffer    = 0;
        flagBuffer = 0;
        elementbuffer = 0;

        m_discardMode = 1;

        init();
    }

    ~Renderer2D()
    {
        freeBuffers();
    }
};

}

#endif