#ifndef GLYPH_HPP
#define GLYPH_HPP

#include "geometry/Vec.hpp"

#include <vector>
#include <iostream>
#include <cmath>

static inline std::vector< TN::Vec2< float > >  circle( 
	float radius, 
	TN::Vec2< float > center, 
	int nTriangles )
{
	const float PI = std::acos( -1.f );

	float x = radius;
	float y = 0;

    std::vector< TN::Vec2< float > > result( nTriangles * 3 );
    for( int i = 0; i < nTriangles; ++i )
    {
		float theta = 2.0f * PI * float( i + 1 ) / float( nTriangles );
		
		float x2 = radius * std::cos( theta );
		float y2 = radius * std::sin( theta );
     
        result[ i*3     ] = { 0, 0 };
        result[ i*3 + 1 ] = { x, y };
        result[ i*3 + 2 ] = { x2, y2 };        

        x = x2;
        y = y2;
    } 

    return result;
}

#endif
