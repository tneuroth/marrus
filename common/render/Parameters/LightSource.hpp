#ifndef LIGHTSOURCE_H
#define LIGHTSOURCE_H

#include <glm/glm.hpp>
#include <glm/gtx/transform.hpp>

namespace TN {

struct LightSource {

    enum {
        SUN,
        POINT,
        SPOT
    };

    bool enabled;
    int type;

    glm::vec3 direction;

    glm::vec3 Ia;
    glm::vec3 Id;
    glm::vec3 Is;

    float Ia_scale;
    float Id_scale;
    float Is_scale;

    //spot and point

    glm::vec3 position;

    float at1;
    float at2;

    //spot only

    float cutOff;
    float exponent;

    LightSource()
        : enabled( false ), type( SUN )
    {}
};

}

#endif // LIGHTSOURCE_H
