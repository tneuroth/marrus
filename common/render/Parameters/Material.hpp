#ifndef MATERIAL_H
#define MATERIAL_H

#include <glm/glm.hpp>
#include <glm/gtx/transform.hpp>

namespace TN {

struct Material {

    glm::vec3 Ka;
    glm::vec3 Kd;
    glm::vec3 Ks;

    bool overrideAmbient;

    float Ka_scale;
    float Kd_scale;
    float Ks_scale;

    float shininess;
    float opacity;

    Material() {
        Ka = glm::vec3( .3f, .0f, .0f );
        Kd = glm::vec3( .9f, .3f, .0f );
        Ks = glm::vec3( .3f, .5f, .0f );

        Ka_scale = 1.f;
        Kd_scale = 1.f;
        Ks_scale = 0.5f;

        shininess = 4.5f;
        opacity = 1.f;

        overrideAmbient = false;
    }
};

}

#endif // MATERIAL_H
