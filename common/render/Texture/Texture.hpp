#ifndef TEXTURE2D_H
#define TEXTURE2D_H

#include "geometry/Vec.hpp"

#include <glad/glad.h>

#include <iostream>
#include <vector>
#include <unordered_map>
#include <limits>

namespace TN
{

class Texture
{

protected :

size_t m_size;
GLuint m_id;

public:

Texture();
Texture( const Texture & other ) = delete;
Texture( Texture && other )      = default;  

size_t size() const { return m_size; }
GLuint & id();
void create();
void destroy();

virtual ~Texture();
virtual void bind() = 0;

};

///////////////////

class Texture2D : public Texture
{

protected:

int m_width;
int m_height;

public:

int width() const 
{
    return m_width;
}

int height() const 
{
    return m_height;
}

Texture2D();
Texture2D( const Texture2D & other ) = delete;
Texture2D( Texture2D && other )      = default;  
virtual void bind();

};

class Texture1D : public Texture
{

public:

Texture1D();
virtual void bind();

};

/////////////////

class Texture2D1 : public Texture2D
{

public:

    Texture2D1();
    Texture2D1( const Texture2D1 & other ) = delete;
    Texture2D1( Texture2D1 && other )      = default;  
    virtual ~Texture2D1() {}

    void loadZeros( int width, int height );
    void load( std::vector< float > & data, int width, int height );
};

//

class Texture2D3 : public Texture2D
{

public:

    Texture2D3();
    Texture2D3( const Texture2D3 & other ) = delete;
    Texture2D3( Texture2D3 && other )      = default;  
    virtual ~Texture2D3() {}
    void load( std::vector< float > & data, int width, int height, bool alpha = false );
};

////

class Texture1D3 : public Texture1D
{

public:

    Texture1D3();
    Texture1D3( const Texture1D3 & other ) = delete;
    Texture1D3( Texture1D3 && other )      = default;  
    virtual ~Texture1D3() {}
    void load( std::vector< Vec3< float > > & data );
    std::vector< TN::Vec3< float > > getData();
};

///

class Texture1D1 : public Texture1D
{

public:

    Texture1D1();
    Texture1D1( const Texture1D1 & other ) = delete;
    Texture1D1( Texture1D1 && other )      = default;  
    virtual ~Texture1D1() {}
    void load( const std::vector< float > & data );
};

struct TextureLayer
{
    Texture2D1 tex;
    int width;
    int height;

    float maxValue;
    float minValue;

    void computeRange()
    {
        float * v = new float[ width * height ];

        tex.bind();
        glGetTexImage(
            GL_TEXTURE_2D,
            0,
            GL_RED,
            GL_FLOAT,
            ( void * ) v );

        const int END = width * height;

        float mx = -std::numeric_limits< float >::max();
        float mn =  std::numeric_limits< float >::max();

        #pragma omp parallel for reduction(max:mx)
        for( int i = 1; i < END; ++i )
        {
            if( v[ i ] > mx )
            {
                mx = v[ i ];
            }
            if( v[ i ] < mn )
            {
                mn = v[ i ];
            }            
        }

        maxValue = mx;
        minValue = mn;

        delete [] v;

        ///////////////////////////////////////////////
    }

    void resize( int w, int h )
    {
        if( tex.id() == 0 )
        {
            tex.create();
        }

        width = w;
        height = h;

        tex.loadZeros( width, height );
    }

    void load( std::vector< float > & d, int w, int h )
    {
        if( width != w || height != h )
        {
            resize( w, h );
        }

        const int END = width * height;

        float mx = -std::numeric_limits< float >::max();
        float mn =  std::numeric_limits< float >::max();

        for( int i = 0; i < END; ++i )
        {
            if( d[ i ] > mx )
            {
                mx = d[ i ];
            }
            if( d[ i ] < mn )
            {
                mn = d[ i ];
            }            
        }

        maxValue = mx;
        minValue = mn;
        tex.load( d, w, h );
    }

    virtual ~TextureLayer() 
    {
    }
};

struct TextureLayerRGBA
{
    Texture2D3 tex;

    int m_width;
    int m_height;

    float maxValue;
    float minValue;

    void resize( int w, int h )
    {
        if( tex.id() == 0 )
        {
            tex.create();
        }

        m_width = w;
        m_height = h;
    }

    void load( std::vector< float > & d, int w, int h, bool alpha = true )
    {
        if( m_width != w || m_height != h )
        {
            resize( w, h );
        }

        const int END = m_width * m_height;

        float mx = -std::numeric_limits< float >::max();
        float mn =  std::numeric_limits< float >::max();

        for( int i = 1; i < END; ++i )
        {
            if( d[ i ] > mx )
            {
                mx = d[ i ];
            }
            if( d[ i ] < mn )
            {
                mn = d[ i ];
            }            
        }

        maxValue = mx;
        minValue = mn;
        tex.load( d, w, h, true );
    }

    int width() const 
    {
        return m_width;
    }

    int height() const 
    {
        return m_height;
    }

    virtual ~TextureLayerRGBA() 
    {}
};


}


#endif // TEXTURE2D_H


