
#include <glad/glad.h>

#include "render/GLErrorChecking.hpp"
#include "Texture.hpp"

#include <vector>
#include <iostream>

namespace TN
{

Texture::Texture() : m_id( 0 )
{}

GLuint & Texture::id()
{
    return m_id;
}

void Texture::destroy()
{
    if( m_id != 0 )
    {
        GL_CHECK_CALL( glDeleteTextures( 1, &m_id ) );
    }
    m_id = 0;
}

void Texture::create()
{
    GL_CHECK_CALL( glGenTextures( 1, &m_id ) );
}

Texture::~Texture()
{
    destroy();
}

///////////////////////////////

Texture2D::Texture2D() : Texture::Texture()
{}

void Texture2D::bind()
{
    GL_CHECK_CALL( glBindTexture( GL_TEXTURE_2D, m_id ) );
}

//

Texture1D::Texture1D() : Texture::Texture()
{}

void Texture1D::bind()
{
    GL_CHECK_CALL( glBindTexture( GL_TEXTURE_1D, m_id ) );
}

//////////////////////////////////////////////////

Texture2D1::Texture2D1() : Texture2D::Texture2D()
{}

void Texture2D1::loadZeros( int width, int height )
{
    m_width = width;
    m_height = height;
    m_size = width * height;

    GL_CHECK_CALL( glBindTexture( GL_TEXTURE_2D, m_id ) );
    GL_CHECK_CALL( glTexImage2D(GL_TEXTURE_2D, 0, GL_R32F, width, height, 0, GL_RED, GL_FLOAT, NULL ) );
    GL_CHECK_CALL( glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR ) );
    GL_CHECK_CALL( glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR ) );
    GL_CHECK_CALL( glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE ) );
    GL_CHECK_CALL( glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE ) );
}

void Texture2D1::load( std::vector< float > & data, int width, int height )
{
    GL_CHECK_CALL( glBindTexture( GL_TEXTURE_2D, m_id ) );
    GL_CHECK_CALL( glTexImage2D( GL_TEXTURE_2D, 0, GL_R32F, width, height, 0, GL_RED, GL_FLOAT, data.data() ) );

    GL_CHECK_CALL( glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST ) );
    GL_CHECK_CALL( glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST ) );

    // GL_CHECK_CALL( glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR ) );
    // GL_CHECK_CALL( glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR ) );

    GL_CHECK_CALL( glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE ) );
    GL_CHECK_CALL( glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE ) );
}

/////////////////////////////////////////////////////

Texture2D3::Texture2D3() : Texture2D::Texture2D()
{}

void Texture2D3::load( std::vector< float > & data, int width, int height, bool alpha )
{
    // std::cout << "called texture load 2d3" << std::endl;

    m_width = width;
    m_height = height;
    m_size = width * height;

    // std::cout << "texture already non zero ? " << m_id << std::endl;

    if( m_id == 0 )
    {
        // std::cout << "creating texture" << std::endl;
        create();
    }

    // std::cout << "binding texture" << std::endl;
    GL_CHECK_CALL( glBindTexture( GL_TEXTURE_2D, m_id ) );
    
    // std::cout << "texture bound" << std::endl;

    if( alpha )
    {
        // std::cout << "copying into texture with alpha" << std::endl;        
        GL_CHECK_CALL( glTexImage2D( GL_TEXTURE_2D, 0, GL_RGBA32F, width, height, 0, GL_RGBA, GL_FLOAT, data.data() ) );
    }
    else
    {
        GL_CHECK_CALL( glTexImage2D( GL_TEXTURE_2D, 0, GL_RGB32F, width, height, 0, GL_RGB, GL_FLOAT, data.data() ) );        
    }

    // std::cout << "setting texture params" << std::endl;  

    GL_CHECK_CALL( glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR ) );
    GL_CHECK_CALL( glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR ) );
    GL_CHECK_CALL( glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE) );
    GL_CHECK_CALL( glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE) );
}

///////////////////////////////////////////////////////////////////////////////////////////////////////

Texture1D3::Texture1D3() : Texture1D::Texture1D()
{}

std::vector< TN::Vec3< float > > Texture1D3::getData()
{
    std::vector< TN::Vec3< float > > result( m_size );
    GL_CHECK_CALL( glBindTexture( GL_TEXTURE_1D, m_id ) );
    GL_CHECK_CALL( glGetTexImage( GL_TEXTURE_1D,
        0,
        GL_RGB32F,
        GL_FLOAT,
        result.data() ) );

    return result;
}

void Texture1D3::load( std::vector< Vec3< float > > & data )
{
    m_size = data.size();

    GL_CHECK_CALL( glBindTexture( GL_TEXTURE_1D, m_id ) );
    GL_CHECK_CALL( glTexImage1D( GL_TEXTURE_1D, 0, GL_RGB32F, data.size(), 0, GL_RGB, GL_FLOAT, data.data() ) );
    GL_CHECK_CALL( glTexParameteri( GL_TEXTURE_1D, GL_TEXTURE_MAG_FILTER, GL_NEAREST ) );
    GL_CHECK_CALL( glTexParameteri( GL_TEXTURE_1D, GL_TEXTURE_MIN_FILTER, GL_NEAREST ) );
    GL_CHECK_CALL( glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE) );
    GL_CHECK_CALL( glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE) );
}

/////////////////////////////////////////////////////////////////////////

Texture1D1::Texture1D1() : Texture1D::Texture1D()
{}

void Texture1D1::load( const std::vector< float > & data )
{
    m_size = data.size();

    GL_CHECK_CALL( glBindTexture( GL_TEXTURE_1D, m_id ) );
    GL_CHECK_CALL( glTexImage1D( GL_TEXTURE_1D, 0, GL_R32F, data.size(), 0, GL_RED, GL_FLOAT, data.data() ) );
    GL_CHECK_CALL( glTexParameteri( GL_TEXTURE_1D, GL_TEXTURE_MAG_FILTER, GL_LINEAR) );
    GL_CHECK_CALL( glTexParameteri( GL_TEXTURE_1D, GL_TEXTURE_MIN_FILTER, GL_LINEAR ) );
    GL_CHECK_CALL( glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE) );
    GL_CHECK_CALL( glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE) );
}

}

