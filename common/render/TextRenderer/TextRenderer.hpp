#ifndef TN_TEXT_RENDER_HPP
#define TN_TEXT_RENDER_HPP

#include "geometry/Vec.hpp"

#include <glad/glad.h>
#include "render/GLErrorChecking.hpp"

#include <glm/glm.hpp>
#include <glm/gtx/transform.hpp>

#include <ft2build.h>
#include FT_FREETYPE_H

#include <boost/filesystem.hpp>

#include <cmath>
#include <iostream>
#include <map>
#include <string>
#include <fstream>

namespace TN
{

namespace Text {  
    const int NO_CLIP = -9999999;
	const std::pair< TN::Vec2< int >, TN::Vec2< int > > DONT_CLIP = {
    	{ Text::NO_CLIP, Text::NO_CLIP },
    	{ Text::NO_CLIP, Text::NO_CLIP }
    };
}

class TextRenderer
{
    std::string m_baseDirectory;

	struct Character
    {
        GLuint        texId;
        Vec2< float > size;
        Vec2< float > bearing;
        GLuint        advance;
    };

    std::map< std::string, std::map< int, std::map< GLchar, Character > > > m_characters;

    GLuint m_program;

    GLuint m_vbo;
    GLuint m_vao;

    std::string m_defaultFont;
    int m_defaultFontSize;

    bool clipCharacter(
    	const TN::Vec2< float > & pos,
    	const std::pair< TN::Vec2< int >, TN::Vec2< int > > & clip )
    {
    	if( clip == Text::DONT_CLIP ) 
    	{
    		return false;
    	}

    	if( ( clip.first.a()  != Text::NO_CLIP && pos.x() <  clip.first.a() )
    	 || ( clip.first.b()  != Text::NO_CLIP && pos.x() >  clip.first.b() )
    	 || ( clip.second.a() != Text::NO_CLIP && pos.y() > clip.second.b() )
    	 || ( clip.second.b() != Text::NO_CLIP && pos.y() > clip.second.b() ) )
    	{
    		return true;
    	}

    	return false;
    }

    void loadShaders( const std::string & vert, const std::string & frag, GLuint & shader )
    {
		std::ifstream tv( vert );
		std::string vertexShaderStr( ( std::istreambuf_iterator<char>( tv ) ),
		                 std::istreambuf_iterator<char>( ) );

		if( ! tv.is_open() ) {
			std::cerr << "couldn't open shader: " << vert << std::endl;
		}

		std::ifstream tf( frag );
		std::string fragmentShaderStr( ( std::istreambuf_iterator<char>( tf ) ),
		                 std::istreambuf_iterator<char>( ) );

		if( ! tf.is_open() ) {
			std::cerr << "couldn't open shader: " << frag << std::endl;
		}

        const char * vertexShaderSrc   = vertexShaderStr.c_str();
        const char * fragmentShaderSrc = fragmentShaderStr.c_str();

	    // vertex shader
	    int vertexShader = GL_CHECK_CALL_ASSIGN( glCreateShader( GL_VERTEX_SHADER ) );
	    GL_CHECK_CALL( glShaderSource( vertexShader, 1, & vertexShaderSrc, NULL ) );
	    GL_CHECK_CALL( glCompileShader(vertexShader) );

	    // check for shader compile errors
	    int success;
	    char infoLog[512];
	    GL_CHECK_CALL( glGetShaderiv( vertexShader, GL_COMPILE_STATUS, &success ) );
	    if (!success)
	    {
	        GL_CHECK_CALL( glGetShaderInfoLog( vertexShader, 512, NULL, infoLog ) );
	        std::cerr << "Error shader compilation failed \n" << infoLog << std::endl;
	    }

	    // fragment shader
	    int fragmentShader = GL_CHECK_CALL_ASSIGN( glCreateShader(GL_FRAGMENT_SHADER) );
	    GL_CHECK_CALL( glShaderSource(fragmentShader, 1, & fragmentShaderSrc, NULL) );
	    GL_CHECK_CALL( glCompileShader(fragmentShader) );

	    // check for shader compile errors
	    GL_CHECK_CALL( glGetShaderiv(fragmentShader, GL_COMPILE_STATUS, &success) );
	    if (!success)
	    {
	        GL_CHECK_CALL( glGetShaderInfoLog(fragmentShader, 512, NULL, infoLog) );
	        std::cerr << "Error: shader compilation failed \n" << infoLog << std::endl;
	    }

	    // link shaders
	    shader = GL_CHECK_CALL_ASSIGN( glCreateProgram() );
	    GL_CHECK_CALL( glAttachShader( shader, vertexShader   ) );
	    GL_CHECK_CALL( glAttachShader( shader, fragmentShader ) );
	    GL_CHECK_CALL( glLinkProgram(  shader) );

	    // check for linking errors
	    GL_CHECK_CALL( glGetProgramiv( shader, GL_LINK_STATUS, &success) );
	    if (!success) 
	    {
	        GL_CHECK_CALL( glGetProgramInfoLog( shader, 512, NULL, infoLog) );
	        std::cerr << "Error shader linking failed \n" << infoLog << std::endl;
	    }

	    GL_CHECK_CALL( glDeleteShader( vertexShader   ) );
	    GL_CHECK_CALL( glDeleteShader( fragmentShader ) );

        /***************************************************************************************/
    }

    bool loadFont( const std::string & fontName, int size )
    {
        GL_CHECK_CALL( glBindVertexArray( m_vao ) );        

	    FT_Library font;
	    if ( FT_Init_FreeType( &font ) )
	    {
	        std::cerr << "Error Could not initialize FreeType." << std::endl;
	    }

	    FT_Face face;
	    if ( FT_New_Face( font, (  m_baseDirectory + "/Fonts/" + fontName + ".ttf" ).c_str(), 0, &face ) )
	    {
	        std::cerr << "Error: Failed to load font " << fontName << std::endl;
	    }

	    FT_Set_Pixel_Sizes( face, 0, size );
	    GL_CHECK_CALL( glPixelStorei( GL_UNPACK_ALIGNMENT, 1 ) );

	    for ( GLubyte c = 0; c < 128; c++ )
	    {
	        if ( FT_Load_Char( face, c, FT_LOAD_RENDER ) )
	        {
	            std::cerr << "Error: Failed to load font glyph." << std::endl;
	            continue;
	        }

	        GLuint tex;
	        GL_CHECK_CALL( glGenTextures( 1, &tex ) );
	        GL_CHECK_CALL( glBindTexture( GL_TEXTURE_2D, tex ) );
	        GL_CHECK_CALL( glTexImage2D(
	            GL_TEXTURE_2D,
	            0,
	            GL_RED,
	            face->glyph->bitmap.width,
	            face->glyph->bitmap.rows,
	            0,
	            GL_RED,
	            GL_UNSIGNED_BYTE,
	            face->glyph->bitmap.buffer
	        ) );

	        GL_CHECK_CALL( glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER) );
	        GL_CHECK_CALL( glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER) );
	        GL_CHECK_CALL( glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR) );
	        GL_CHECK_CALL( glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR) );

	        Character ch =
	        {
	            static_cast< GLuint >( tex ),
	            Vec2< float >( face->glyph->bitmap.width, face->glyph->bitmap.rows ),
	            Vec2< float >( face->glyph->bitmap_left,  face->glyph->bitmap_top  ),
	            static_cast< GLuint >( face->glyph->advance.x )
	        };

	        if( ! m_characters.count( fontName ) )
	        {
	        	m_characters.insert( { fontName, {} } );    	
	        }
	        if( ! m_characters.at( fontName ).count( size ) )
	        {
	       		m_characters.at( fontName ).insert( { size, {} } );
	        } 
	        if( ! m_characters.at( fontName ).at( size ).count( c ) ) 
	        {
	        	m_characters.at( fontName ).at( size ).insert( { c, ch } );
	        }
	    }

	    FT_Done_Face( face );
	    FT_Done_FreeType( font );

	    return true;
    }

 public:

	bool preloadFont( const std::string & font, int size )
	{
		if( boost::filesystem::exists( m_baseDirectory + "/Fonts/" + font + ".ttf" ) )
		{
			return loadFont( font, size );
		}	
		else {
			std::cerr << "Error: " << font << " not found in " << m_baseDirectory << "/Fonts/" << std::endl; 
			return false;
		}
	}

	void setDefaultFont( const std::string & font, int size )
	{
		if( ! preloadFont( font, size ) )
		{
			std::cerr << "Error: " << font << " not found in " << m_baseDirectory << "/Fonts/" << std::endl; 
		} 
		else 
		{
			m_defaultFont = font;
			m_defaultFontSize = size;
		}
	}

    TextRenderer( const std::string & _baseDir, const std::string & defFont, int defFontSize )
    {
    	m_baseDirectory = _baseDir;
    	m_program = 0;
		m_vbo = 0;
		m_vao = 0;

		setDefaultFont( defFont, defFontSize );
    }

    TextRenderer( const std::string & _baseDir )
    {
    	m_baseDirectory = _baseDir;
    	m_program = 0;
		m_vbo = 0;
		m_vao = 0;

		setDefaultFont( "OpenSans-Regular", 13 );
		bool success = preloadFont( "OpenSans-Semibold",   16 );
		success      = preloadFont( "OpenSans-Semibold",   34 );
    }

    ~TextRenderer()
    {
    	GL_CHECK_CALL( glDeleteBuffers( 1, & m_vbo ) );

    	// textures ...

    }

	void init()
	{
        GL_CHECK_CALL( glGenVertexArrays( 1, &m_vao ) );
        GL_CHECK_CALL( glBindVertexArray( m_vao ) );
        GL_CHECK_CALL( glGenBuffers( 1, & m_vbo ) );

        GL_CHECK_CALL( glBindBuffer( GL_ARRAY_BUFFER, m_vbo ) );     
        GL_CHECK_CALL( glBufferData( GL_ARRAY_BUFFER, 24 * sizeof( float ), NULL, GL_DYNAMIC_DRAW ) );          

	    const float texcoords[] =
	    {
	        0.f, 0.f,
	        0.f, 1.f,
	        1.f, 1.f,
	        0.f, 0.f,
	        1.f, 1.f,
	        1.f, 0.f
	    };

		GL_CHECK_CALL( glBufferSubData( GL_ARRAY_BUFFER, 12 * sizeof(float), 12 * sizeof(float), texcoords ) );

        const std::string rootPath = m_baseDirectory + "/shaders/";
	    loadShaders( rootPath + "text.vert", rootPath + "text.frag",  m_program );

	    GL_CHECK_CALL( glEnableVertexAttribArray( 0 ) );
	    GL_CHECK_CALL( glVertexAttribPointer(
	        0, 
	        2, 
	        GL_FLOAT, 
	        GL_FALSE, 
	        0, 
	        (void*) 0
	    ) );

	    GL_CHECK_CALL( glEnableVertexAttribArray( 1 ) );
	    GL_CHECK_CALL( glVertexAttribPointer(
	        1, 
	        2, 
	        GL_FLOAT, 
	        GL_FALSE, 
	        0, 
	        (void*) ( 12 * sizeof( GL_FLOAT ) )
	    ) );
	}


	void checkError( const std::string & prefix)
	{
	    GLenum glErr = glGetError();

	    while(glErr != GL_NO_ERROR) {
	        std::string error;
	        switch (glErr)
	        {
	        case GL_NO_ERROR:               error="NO_ERROR";               break;
	        case GL_INVALID_OPERATION:      error="INVALID_OPERATION";      break;
	        case GL_INVALID_ENUM:           error="INVALID_ENUM";           break;
	        case GL_INVALID_VALUE:          error="INVALID_VALUE";          break;
	        case GL_OUT_OF_MEMORY:          error="OUT_OF_MEMORY";          break;
	        case GL_INVALID_FRAMEBUFFER_OPERATION:  error="INVALID_FRAMEBUFFER_OPERATION";  break;
	        }

	        if(error != "NO_ERROR") {
	            std::cerr << prefix << ":" << error << std::endl;
	        }
	        glErr = glGetError();
	    }
	}

public:

float getWidthAt( 
	const std::string & text,
    const size_t & IDX,
	std::string & font, 
	int & fontSize,
	bool vertical = false )
{
	if( IDX > text.size() )
	{
		return 0.f;
	}

	bool fontLoaded = false;
	if( m_characters.count( font ) ) 
	{
		if ( m_characters.at( font ).count( fontSize ) )
		{
			fontLoaded = true;
		}
	}

	if( ! fontLoaded )
	{
		if( ! preloadFont( font, fontSize ) )
		{
			font = m_defaultFont;
			fontSize = m_defaultFontSize;
		}
	}

	TN::Vec2< float > nextPos = { 0.f, 0.f };
	for ( size_t i = 0, end = std::min( IDX, text.size() ); i < end; ++i )
    {
    	auto c = text[ i ];
    	const Character & ch = m_characters.at( font ).at( fontSize ).at( text[ i ] );
	    nextPos = renderCharacter( ch, nextPos, vertical, true );
    }
	return nextPos.x();
}

float getWidthAt( 
	const std::string & text,
    const size_t & IDX,
	std::string & font )
{
	return getWidthAt( text, IDX, font, m_defaultFontSize ); 
}

float getWidthAt( 
	const std::string & text,
    const size_t & IDX )
{
	return getWidthAt( text, IDX, m_defaultFont, m_defaultFontSize ); 
}

void renderTextInvariant(
	const int windowWidth, 
	const int windowHeight,
	std::string & font, 
	int         & fontSize )
{
	bool fontLoaded = false;
	if( m_characters.count( font ) ) 
	{
		if ( m_characters.at( font ).count( fontSize ) )
		{
			fontLoaded = true;
		}
	}

	if( ! fontLoaded )
	{
		if( ! preloadFont( font, fontSize ) )
		{
			std::cerr << "Failed to load font: " << font << ", using default" << std::endl; 
			font     = m_defaultFont;
			fontSize = m_defaultFontSize;
		}
	}

	GL_CHECK_CALL( glViewport( 0, 0, windowWidth, windowHeight ) );
	GL_CHECK_CALL( glEnable( GL_BLEND ) );
	GL_CHECK_CALL( glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA ) );  
    GL_CHECK_CALL( glBindVertexArray( m_vao ) );    
    GL_CHECK_CALL( glBindBuffer( GL_ARRAY_BUFFER, m_vbo ) );
	GL_CHECK_CALL( glActiveTexture( GL_TEXTURE0 ) );
	GL_CHECK_CALL( glUseProgram( m_program ) );
	glm::mat4 P = glm::ortho( 0.f, (float) windowWidth, 0.f, (float) windowHeight );
	GLuint id = GL_CHECK_CALL_ASSIGN( glGetUniformLocation( m_program, "P" ) );
	GL_CHECK_CALL( glUniformMatrix4fv( id, 1, GL_FALSE, &P[ 0 ][ 0 ] ) );
}

TN::Vec2< float > renderCharacter( 
	const Character & ch,
	const TN::Vec2< float > & pos,
	bool vertical )
{
	const GLfloat TX = pos.x() + ch.bearing.x();
	const GLfloat TY = vertical ? pos.y() + ch.size.x() : pos.y() - ( ch.size.y() - ch.bearing.y() );

	const GLfloat CW = ch.size.x();
	const GLfloat CH = ch.size.y();

	const float quad[] =
	{
	    TX,     TY + CH,
	    TX,     TY,
	    TX + CW, TY,
	    TX,     TY + CH,
	    TX + CW, TY,
	    TX + CW, TY + CH
	};

	const float quadV[] =
	{
	    TX - CH, TY - CW,
	    TX,     TY - CW,
	    TX,     TY,
	    TX - CH, TY - CW,
	    TX,     TY,
	    TX - CH, TY
	};
	
	GL_CHECK_CALL( glBufferSubData( GL_ARRAY_BUFFER, 0, 12 * sizeof( float ), vertical ? quadV : quad ) );
	GL_CHECK_CALL( glBindTexture(   GL_TEXTURE_2D, ch.texId ) );     
	GL_CHECK_CALL( glDrawArrays(    GL_TRIANGLES, 0, 6 ) );

	if( vertical ) { 
		return { pos.x(), pos.y() + (float) ( ch.advance >> 6 ) };
	}
	else { 
		return { pos.x() + (float) ( ch.advance >> 6 ), pos.y() };
	}
}

TN::Vec2< float > renderCharacter( 
	const Character & ch,
	const TN::Vec2< float > & pos,
	bool vertical,
	bool clip )
{
	if( clip )
	{
		if( vertical ) { 
			return { pos.x(), pos.y() + (float) ( ch.advance >> 6 ) };
		}
		else { 
			return { pos.x ()+ (float) ( ch.advance >> 6 ), pos.y() };
		}
	}
	else {
		return renderCharacter( ch, pos, vertical );
	}
}

TN::Vec2< float > renderCharacter( 
	const Character & ch,
	const TN::Vec2< float > & pos,
	const TN::Vec4 & color,
	bool vertical,
	bool clip = false )
{
    GLuint cID = GL_CHECK_CALL_ASSIGN( glGetUniformLocation( m_program, "textColor" ) );	
    GL_CHECK_CALL( glUniform4fv( cID, 1, & color.r ) );
    return renderCharacter( ch, pos, vertical, clip );
}

////////////////////////////////////////////////////////////////////////////////////////////////////////

void renderText(  
	const int windowWidth, 
	const int windowHeight,
	const std::string & text,
	const TN::Vec2< float > & position,
	const TN::Vec4 & color, 
	const bool vertical,
	std::string font,
	int fontSize ) 
{
	renderTextInvariant( windowWidth, windowHeight, font, fontSize );
    GLuint cID = GL_CHECK_CALL_ASSIGN( glGetUniformLocation( m_program, "textColor" ) );
    GL_CHECK_CALL( glUniform4fv( cID, 1, & color.r ) );

	TN::Vec2< float > nextPos = { 
		std::round( position.x() ),
		std::round( position.y() ) };

	for ( size_t i = 0, end = text.size(); i < end; ++i )
	{
	    const Character & ch = m_characters.at( font ).at( fontSize ).at( text[ i ] );
	    nextPos = renderCharacter( ch, nextPos, vertical );
	}
}

void renderText(  
	const int windowWidth, 
	const int windowHeight,
	const std::string & text,
	const TN::Vec2< float > & position,
	const std::vector< TN::Vec4 > & colors, 
	const bool vertical,
	std::string font,
	int fontSize ) 
{
	renderTextInvariant( windowWidth, windowHeight, font, fontSize );
	TN::Vec2< float > nextPos = { 
		std::round( position.x() ),
		std::round( position.y() ) };
	for ( size_t i = 0, end = text.size(); i < end; ++i )
	{
	    const Character & ch = m_characters.at( font ).at( fontSize ).at( text[ i ] );
	    nextPos = renderCharacter( ch, nextPos, colors[ i ], vertical );
	}
}

void renderText(  
	int w, 
	int h,
	const std::string & text,
	const TN::Vec2< float > & position,
	const TN::Vec4 & color, 
	bool vertical,
	int fontSize ) 
{
	renderText( w, h, text, position, color, vertical, m_defaultFont, fontSize );
}

void renderText(  
	int w, 
	int h,
	const std::string & text,
	const TN::Vec2< float > & position,
	const TN::Vec4 & color, 
	bool vertical ) 
{
	renderText( w, h, text, position, color, vertical, m_defaultFont, m_defaultFontSize );
}

void renderText(  
	int w, 
	int h,
	const std::string & text,
	const TN::Vec2< float > & position,
	const std::vector< TN::Vec4 > & colors, 
	bool vertical,
	int fontSize ) 
{
	renderText( w, h, text, position, colors, vertical, m_defaultFont, fontSize );
}

void renderText(  
	int w, 
	int h,
	const std::string & text,
	const TN::Vec2< float > & position,
	const std::vector< TN::Vec4 > & colors,  
	bool vertical ) 
{
	renderText( w, h, text, position, colors, vertical, m_defaultFont, m_defaultFontSize );
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Versions which accept reference to position array to fill and optionally clip the text on x


void renderText(  
	const int windowWidth, 
	const int windowHeight,
	const std::string & text,
	const TN::Vec2< float > & position,
	const TN::Vec4 & color, 
	const bool vertical,
	std::string font,
	int fontSize,
	std::vector< TN::Vec2< float > > & charPositionsRet,
	std::pair< TN::Vec2< int >, TN::Vec2< int > > clip = Text::DONT_CLIP ) 
{
	renderTextInvariant( windowWidth, windowHeight, font, fontSize );
    GLuint cID = GL_CHECK_CALL_ASSIGN( glGetUniformLocation( m_program, "textColor" ) );
    GL_CHECK_CALL( glUniform4fv( cID, 1, & color.r ) );

	TN::Vec2< float > nextPos = { 
		std::round( position.x() ),
		std::round( position.y() ) };

	for ( size_t i = 0, end = text.size(); i < end; ++i )
	{
	    const Character & ch = m_characters.at( font ).at( fontSize ).at( text[ i ] );
	    nextPos = renderCharacter( ch, nextPos, vertical, clipCharacter( nextPos, clip ) );
	    charPositionsRet.push_back( nextPos );
	}
}

void renderText(  
	const int windowWidth, 
	const int windowHeight,
	const std::string & text,
	const TN::Vec2< float > & position,
	const std::vector< TN::Vec4 > & colors, 
	const bool vertical,
	std::string font,
	int fontSize,
	std::vector< TN::Vec2< float > > & charPositionsRet,
	std::pair< TN::Vec2< int >, TN::Vec2< int > > clip = Text::DONT_CLIP ) 
{
	renderTextInvariant( windowWidth, windowHeight, font, fontSize );
	TN::Vec2< float > nextPos = { 
		std::round( position.x() ),
		std::round( position.y() ) };
	for ( size_t i = 0, end = text.size(); i < end; ++i )
	{
	    const Character & ch = m_characters.at( font ).at( fontSize ).at( text[ i ] );
	    nextPos = renderCharacter( ch, nextPos, colors[ i ], vertical, clipCharacter( nextPos, clip ) );
	    charPositionsRet.push_back( nextPos );
	}
}

void renderText(  
	int w, 
	int h,
	const std::string & tx,
	const TN::Vec2< float > & p,
	const TN::Vec4 & cl, 
	bool vert,
	int sz,
	std::vector< TN::Vec2< float > > & cp, 
	std::pair< TN::Vec2< int >, TN::Vec2< int > > clip = Text::DONT_CLIP )
{
	renderText( w, h, tx, p, cl, vert, m_defaultFont, sz, cp, clip );
}

void renderText(  
	int w, 
	int h,
	const std::string & tx,
	const TN::Vec2< float > & p,
	const TN::Vec4 & cl, 
	bool vert,
	std::vector< TN::Vec2< float > > & cp,
	std::pair< TN::Vec2< int >, TN::Vec2< int > > clip = Text::DONT_CLIP ) 
{
	renderText( w, h, tx, p, cl, vert, m_defaultFont, m_defaultFontSize, cp, clip );
}

void renderText(  
	int w, 
	int h,
	const std::string & tx,
	const TN::Vec2< float > & p,
	const std::vector< TN::Vec4 > & cls, 
	bool vert,
	int sz,
	std::vector< TN::Vec2< float > > & cp,
	std::pair< TN::Vec2< int >, TN::Vec2< int > > clip = Text::DONT_CLIP ) 
{
	renderText( w, h, tx, p, cls, vert, m_defaultFont, sz, cp, clip );
}

void renderText(  
	int w, 
	int h,
	const std::string & tx,
	const TN::Vec2< float > & p,
	const std::vector< TN::Vec4 > & cls, 
	bool vert,
	std::vector< TN::Vec2< float > > & cp,
	std::pair< TN::Vec2< int >, TN::Vec2< int > > clip = Text::DONT_CLIP ) 
{
	renderText( w, h, tx, p, cls, vert, m_defaultFont, m_defaultFontSize, cp, clip);
}

};

}

#endif