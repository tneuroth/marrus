#ifndef TN_DATA_PARAMS_HPP 
#define TN_DATA_PARAMS_HPP

#include <string>

enum class DataComponentType
{
	Hist2D
};

class DataComponentParams 
{
	DataComponentType m_type;

public:

	DataComponentParams( DataComponentType t ) : m_type( t ) {}

    DataComponentType getType() const
    {
 		return m_type;
    }

    virtual      void fromJSON()  = 0;
    virtual std::string toJSON() const = 0;
};

#endif