#ifndef HIST2D_PARAMS_HPP
#define HIST2D_PARAMS_HPP

#include "model/DataComponentParams.hpp"

#include <vector>

enum class Hist2DUpdateMode
{
    Fixed,
    PreserveMoments
};

enum class Hist2DEdgeMode 
{
    Uniform,
    Explicit
};

enum class Hist2DRangeMode 
{
    Fixed,
    Dynamic
};


struct Hist2DParams : public DataComponentParams
{
    int nBinsX;
    int nBinsY;

    std::vector< float > xEdges;
    std::vector< float > yEdges;

    Hist2DUpdateMode updateMode;
    Hist2DEdgeMode   edgeMode;
    Hist2DRangeMode  rangeMode;

    Hist2DParams() : 
        DataComponentParams( DataComponentType::Hist2D ),
        updateMode( Hist2DUpdateMode::Fixed ), 
        edgeMode( Hist2DEdgeMode::Uniform ), 
        rangeMode( Hist2DRangeMode::Dynamic ), 

        nBinsX( 32 ), 
        nBinsY( 32 )
    {}

    void fromJSON() override
    {

    }

    std::string toJSON() const override
    {

    }
};

#endif
