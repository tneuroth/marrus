#ifndef MARRUS_CONFIGURATION_BASE_HPP
#define MARRUS_CONFIGURATION_BASE_HPP

#include <jsonhpp/json.hpp>

#include <fstream>
#include <sstream>
#include <string>
#include <utility>
#include <iostream>
#include <string>

namespace TN {

enum class ConfigStatus {
    Incomplete,
    Invalid,
    Ok
};

struct ConfigurationBase
{
    TN::ConfigStatus status;
    std::string statusReport;

    virtual bool isValid() {
    	return status ==  TN::ConfigStatus::Ok;
    }

    virtual std::string getMessage() {
    	return statusReport;
    }

    virtual TN::ConfigStatus getStatus() {
    	return status;
    }

	virtual void init() = 0;
	virtual void fromJSON( nlohmann::json j, const std::string & baseDirectory = "" ) = 0;

	virtual nlohmann::json toJSON() = 0;
    ConfigurationBase() : status( TN::ConfigStatus::Incomplete ) {};
	virtual ~ConfigurationBase() = default;
};

} // end namespace TN

#endif