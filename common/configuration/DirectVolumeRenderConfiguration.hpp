#ifndef MARRUS_DIRECT_VOL_VIS_CONFIG_HPP
#define MARRUS_DIRECT_VOL_VIS_CONFIG_HPP

#include "configuration/ConfigurationBase.hpp"
#include "configuration/LightingConfiguration.hpp"

#include "geometry/Vec.hpp"
#include "input/JsonUtils.hpp"

#include <jsonhpp/json.hpp>

#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <utility>
#include <cstdint>
#include <iostream>

namespace TN {

struct DirectVolumeRenderConfiguration : public ConfigurationBase
{
	std::string field;
	std::string color_tf;
	TN::Vec2< float > mapping_range;
	LightingConfiguration lighting;
	bool visible;

	virtual void init() override
	{

	}

	virtual void fromJSON( nlohmann::json j, const std::string & baseDirectory = "" ) override
	{

	}

	virtual nlohmann::json toJSON() override
	{
		nlohmann::json j;
		return j;
	}

    DirectVolumeRenderConfiguration() : ConfigurationBase() {}
    virtual ~DirectVolumeRenderConfiguration() {}
};

}

#endif 