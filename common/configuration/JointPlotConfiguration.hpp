#ifndef MARRUS_JOINT_PLOT_CONFIG_HPP
#define MARRUS_JOINT_PLOT_CONFIG_HPP

#include "configuration/ConfigurationBase.hpp"

#include "input/JsonUtils.hpp"
#include <jsonhpp/json.hpp>

#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <utility>
#include <cstdint>
#include <iostream>

namespace TN {

struct JointPlotUpdatesNeeded
{
	bool histogram;
	bool x_marginals;
	bool y_marginals;	
	bool curves;
	bool summary;
	bool kde;
	bool gmm;
	bool cdf;
	bool scatter;
	bool lassoPoints;

	void setAll( bool v )
	{
		histogram   = v;
		x_marginals = v;
		y_marginals	= v;
		curves      = v;
		summary     = v;
		kde         = v;
		gmm         = v;
		scatter     = v;
		lassoPoints = v;
	}

	JointPlotUpdatesNeeded() : 
		histogram(    true ),
		x_marginals(  true ),
		y_marginals	( true ),
		curves(       true ),
		summary(      true ),
		kde(          true ),
		gmm(          true ),
		scatter(      true ),
		lassoPoints(  true )
		{}

	void update( const JointPlotUpdatesNeeded & previous )
	{
		histogram   = histogram   || previous.histogram  ;
		x_marginals = x_marginals || previous.x_marginals; 
		y_marginals	= y_marginals || previous.y_marginals;
		curves      = curves      || previous.curves     ; 
		summary     = summary     || previous.summary    ; 
		kde         = kde         || previous.kde        ; 
		gmm         = gmm         || previous.gmm        ; 
		scatter     = scatter     || previous.scatter    ;
		lassoPoints = lassoPoints || previous.lassoPoints;
	}
};

struct BinningConfiguration 
{
	std::string type;
	std::vector< float > edges;
	int num_bins;
	bool locked;

	BinningConfiguration() :
		type( "uniform" ),
		edges(),
		num_bins( 17 ),
		locked( false ) {}

	bool operator==( const BinningConfiguration & other )
	{
		return type  == other.type 
		&& edges     == other.edges
		&& num_bins  == other.num_bins;
	}

	bool operator!=( const BinningConfiguration & other )
	{
		return ! ( (*this) == other );
	}
};

struct ActiveJointPlotLayersConfiguration 
{
	std::string density_estimation;
	bool curve;
	bool super_marginals;
	bool subselection;
};

struct JointPlotConfiguration : public ConfigurationBase
{
	std::string name;
	std::string description;
	std::string title;

	bool precomputed;

	std::string x;
	std::string y;
	std::string w;

	std::string w_normalization;

	std::string color_transfer_function;
	float diverging_TF_center;
	bool zeroCenteredDiveringMap;

	std::string curve_fitting_mode;
	std::string summary_mode;

	BinningConfiguration x_bins;
	BinningConfiguration y_bins;

	std::string data_path;
	std::string feature_id;

	ActiveJointPlotLayersConfiguration active_layers;

	JointPlotUpdatesNeeded update( const JointPlotConfiguration & newConfiguration )
	{
		JointPlotUpdatesNeeded needsUpdates;

		if( x != newConfiguration.x 
		 || y != newConfiguration.y 
		 || w != newConfiguration.w )
		{
			needsUpdates.setAll( true );
			return needsUpdates;
		}

		if( curve_fitting_mode != newConfiguration.curve_fitting_mode )
		{
			needsUpdates.curves = true;
		}

		if( summary_mode != newConfiguration.summary_mode )
		{
			needsUpdates.summary = true;
		}

		if( x_bins != newConfiguration.x_bins )
		{
			needsUpdates.histogram   = true;
			needsUpdates.x_marginals = true;
			needsUpdates.curves      = true; // could be optimized later because depends on mode
			needsUpdates.summary     = true; // could be optimized later because depends on mode
		}

		if( y_bins != newConfiguration.y_bins )
		{
			needsUpdates.histogram   = true;
			needsUpdates.y_marginals = true;			
			needsUpdates.summary     = true; // could be optimized later because depends on mode
		}

		// should always be
		if( y_bins.edges.size() >= 2 && x_bins.edges.size() >= 2 )
		{
			if( newConfiguration.y_bins.edges.size() >= 2 && newConfiguration.x_bins.edges.size() >= 2  )
			{
				// The outer bin edges are the defacto bounds of the gmm and kde plots 
				if( y_bins.edges.back() != newConfiguration.y_bins.edges.back() 
				 || y_bins.edges.front()!= newConfiguration.y_bins.edges.front()  
				 || x_bins.edges.back() != newConfiguration.x_bins.edges.back() 
				 || x_bins.edges.front()!= newConfiguration.x_bins.edges.front() )
				{
					needsUpdates.gmm    = true;
					needsUpdates.kde    = true;
					needsUpdates.curves = true;
				}
			}
		}

		// TODO : unfinished, incorporate complete logic

		return needsUpdates;
	}

	virtual void init() {
		name        = "";
		description = "";
		title       = "";
		precomputed = false;
		x           = "";
		y           = "";
		w           = "";
		w_normalization = 1.f;
		color_transfer_function = "default";
		diverging_TF_center = 0.f;
		zeroCenteredDiveringMap = false;
		curve_fitting_mode = "bin_means";
		summary_mode       = "moments";
		x_bins             = BinningConfiguration();
		y_bins             = BinningConfiguration();
		data_path          = "";
		feature_id         = "";
		active_layers      = ActiveJointPlotLayersConfiguration();
	}

	virtual void fromJSON( nlohmann::json j, const std::string & baseDirectory = "" ) 
	{
    	statusReport = "";
    	status = TN::ConfigStatus::Incomplete;
    	std::pair< bool, std::string > parseStatus;
		bool found = true;
		
    	////// required parameters ////////////////////////////////////////////////////////////////////////////////////////////

    	int nMissingRequired = 0;

		found = TN::CF::getParameter( j, "precomputed", precomputed, parseStatus, TN::CF::OPTIONAL, false  );

		if( ! precomputed || ! found ) {
			nMissingRequired += ! TN::CF::getParameter( j, "x", x, parseStatus, TN::CF::REQUIRED, std::string( "" )  );
			nMissingRequired += ! TN::CF::getParameter( j, "y", y, parseStatus, TN::CF::REQUIRED, std::string( "" )  );
		}
		else {
			nMissingRequired += ! TN::CF::getParameter( j, "data_path",       data_path, parseStatus, TN::CF::REQUIRED, std::string( "" )  );
			nMissingRequired += ! TN::CF::getParameter( j, "feature_id",     feature_id, parseStatus, TN::CF::REQUIRED, std::string( "" )  );
		}

		if( nMissingRequired )  {
    		parseStatus.second += "Error: missing " + std::to_string( nMissingRequired ) + "required parameters.\n";
			parseStatus.first = false; 
			status = TN::ConfigStatus::Incomplete;
		}
		else
		{
			status = TN::ConfigStatus::Ok;
		}

		/////// optional parameters ////////////////////////////////////////////////////////////////////////////////////////////

		found = TN::CF::getParameter( j, "name", name, parseStatus, TN::CF::OPTIONAL, std::string( "" )  );
		found = TN::CF::getParameter( j, "title", title, parseStatus, TN::CF::OPTIONAL, std::string( "" )  );
		found = TN::CF::getParameter( j, "description", description, parseStatus, TN::CF::OPTIONAL, std::string( "" )  );
		found = TN::CF::getParameter( j, "color_transfer_function", color_transfer_function, parseStatus, TN::CF::OPTIONAL, std::string( "" )  );
		found = TN::CF::getParameter( j, "diverging_TF_center", diverging_TF_center, parseStatus, TN::CF::OPTIONAL, float( 1.0 ) );
		found = TN::CF::getParameter( j, "w", w, parseStatus, TN::CF::OPTIONAL, std::string( "" )  );
		found = TN::CF::getParameter( j, "w_normalization", w_normalization, parseStatus, TN::CF::OPTIONAL, std::string( "" )  );
		found = TN::CF::getParameter( j, "curve_fitting_mode", curve_fitting_mode, parseStatus, TN::CF::OPTIONAL, std::string( "" )  );
		found = TN::CF::getParameter( j, "summary_mode", summary_mode, parseStatus, TN::CF::OPTIONAL, std::string( "" )  );

		////////// Binning Parameters /////////////////////////////////////////////////////////////////////////////////////////////

		nlohmann::json tempJson;
		
		found = TN::CF::getParameter( j, "x_bins", tempJson, parseStatus, TN::CF::OPTIONAL, {} );

		if( found ) {
			found = TN::CF::getParameter( tempJson, "type",      x_bins.type,      parseStatus, TN::CF::OPTIONAL, std::string( "" )  );
			found = TN::CF::getParameter( tempJson, "num_bins",  x_bins.num_bins,  parseStatus, TN::CF::OPTIONAL, int(17) );
			found = TN::CF::getParameter( tempJson, "locked",    x_bins.locked,    parseStatus, TN::CF::OPTIONAL, bool( false ) );
			found = TN::CF::getParameter( tempJson, "edges",     x_bins.edges,     parseStatus, TN::CF::OPTIONAL, {}  );
		}

		found = TN::CF::getParameter( j, "y_bins", tempJson, parseStatus, TN::CF::OPTIONAL, {} );

		if( found ) {
			found = TN::CF::getParameter( tempJson, "type",      y_bins.type,      parseStatus, TN::CF::OPTIONAL, std::string( "" )  );
			found = TN::CF::getParameter( tempJson, "num_bins",  y_bins.num_bins,  parseStatus, TN::CF::OPTIONAL, int(17) );
			found = TN::CF::getParameter( tempJson, "locked",    y_bins.locked,    parseStatus, TN::CF::OPTIONAL, bool( false ) );
			found = TN::CF::getParameter( tempJson, "edges",     y_bins.edges,     parseStatus, TN::CF::OPTIONAL, {}  );
		}

		////////// active layers //////////////////////////////////////////////////////////////////////////////////////////////////

		found = TN::CF::getParameter( j, "active_layers", tempJson, parseStatus, TN::CF::OPTIONAL, {} );

		if( found ) {
			found = TN::CF::getParameter( tempJson, "density_estimation", active_layers.density_estimation, parseStatus, TN::CF::OPTIONAL, std::string( "" ) );
			found = TN::CF::getParameter( tempJson, "curve",              active_layers.curve,              parseStatus, TN::CF::OPTIONAL, bool( false ) );
			found = TN::CF::getParameter( tempJson, "super_marginals",    active_layers.super_marginals,    parseStatus, TN::CF::OPTIONAL, bool( false ) );
			found = TN::CF::getParameter( tempJson, "subselection",       active_layers.subselection,       parseStatus, TN::CF::OPTIONAL, bool( false ) );
		}

		statusReport += parseStatus.second;
	}

	virtual void fromFile( const std::string & path ) 
	{
		init();
		std::ifstream input( path );
		if( !input.is_open() )	{
			std::cerr << "Couldn't open config file: " << path << std::endl;
			exit( 1 );
		}	
		std::stringstream sstr;
		sstr << input.rdbuf();
    	fromJSON( nlohmann::json::parse( sstr.str(), nullptr, true, true ) ); 
	}

	virtual nlohmann::json toJSON()
	{
		nlohmann::json j;
		return j;
	}

    JointPlotConfiguration() : ConfigurationBase() {}
    virtual ~JointPlotConfiguration() {}

    std::string xKey() { return x; }
    std::string yKey() { return y; }
    std::string wKey() { return w; }
};

}

#endif 
