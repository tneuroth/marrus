#ifndef MARRUS_LIGHTING_CONFIG_HPP
#define MARRUS_LIGHTING_CONFIG_HPP

#include "configuration/ConfigurationBase.hpp"

#include "geometry/Vec.hpp"
#include "input/JsonUtils.hpp"

#include <jsonhpp/json.hpp>

#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <utility>
#include <cstdint>
#include <iostream>

namespace TN {

struct LightingConfiguration : public ConfigurationBase
{
	float shininess;
	float specular_amount;
	float diffuse_amount;	
	float ambient_amount;
	TN::Vec2< float > view_plane_translation;
	
	virtual void init() override
	{

	}

	virtual void fromJSON( nlohmann::json j, const std::string & baseDirectory = "" ) override
	{


	}

	virtual nlohmann::json toJSON() override
	{
		nlohmann::json j;
		return j;
	}

    LightingConfiguration() : ConfigurationBase() {}
    virtual ~LightingConfiguration() {}
};

}

#endif 