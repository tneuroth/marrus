#ifndef MARRUS_SPATIAL_VIEW_CONFIG_HPP
#define MARRUS_SPATIAL_VIEW_CONFIG_HPP

#include "configuration/ConfigurationBase.hpp"
#include "configuration/LightingConfiguration.hpp"
#include "configuration/GlyphConfiguration.hpp"

#include "geometry/Vec.hpp"
#include "geometry/SurfaceSetCollection.hpp"
#include "input/JsonUtils.hpp"

#include <jsonhpp/json.hpp>

#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <utility>
#include <cstdint>
#include <iostream>
#include <memory>

namespace TN {

struct GlyphOrentation
{
	static constexpr int None   = 0;
	static constexpr int Viewer = 1;
	static constexpr int Normal = 2;
};

struct GlyphColorMode
{
	static constexpr int Solid   = 0;
	static constexpr int Scalar  = 1;
	static constexpr int Texture = 2;
};

struct SpatialViewConfiguration : public ConfigurationBase
{
	std::string name;
	std::string description;
	std::string title;

	std::map< std::string,  bool > surface_set_visibility;
	std::map< std::string, std::map< std::string, bool > > surface_visibility;

	bool glyphSetVisibility;
	std::vector< std::uint8_t > glyphLayerVisibility;

	TN::GlyphConfiguration glyphConfiguration;
	bool active;

	bool show_bounding_box;
	bool show_axis;
	bool show_axis_coordinates;

	bool show_band_selection;
	bool show_component_selection;
	bool show_cell_selection;
	bool show_voxel_selection;

	bool perBandNormalization;
	bool perScaleBinNormalization;
	bool perGlyphNormalization;
	bool normalizeGlyphsFromZero;
	bool allAxisNormalization;

	bool showGlyphBorders;

	int scaleMode;

	bool defined;

	int colorMode;
	int orientation;

	float glyphScale;

	bool light_background;

	virtual void init() override
	{
		glyphScale = 2.2f;
		scaleMode  = 0;

		name        = "";
		description = "";
		title       = "";

		glyphSetVisibility = true;

		normalizeGlyphsFromZero  = true;
		perGlyphNormalization    = true;
		perBandNormalization     = false;
		perScaleBinNormalization = false;
		allAxisNormalization     = false;

		show_bounding_box     = true;
		show_axis             = true;
		show_axis_coordinates = true;

		show_band_selection      = false;
		show_component_selection = false;
		show_cell_selection      = false;
		show_voxel_selection     = false;

		showGlyphBorders = false;

		defined = true;

		orientation = TN::GlyphOrentation::Normal;
		colorMode   = TN::GlyphColorMode::Texture;

		light_background = true;

		active = false;
	}

	bool visuallyEquivalent( const SpatialViewConfiguration & other ) const
	{
		// return show_bounding_box      == other.show_bounding_box
		//     && show_axis              == other.show_axis
		//     && show_axis_coordinates  == other.show_axis_coordinates
		//     && defined                == other.defined
		//     && surface_set_visibility == other.surface_set_visibility 
		//     && surface_visibility     == other.surface_visibility
		//     && surface_visibility     == other.surface_visibility 
		//     && glyphLayerVisibility   == other.glyphLayerVisibility
		//     && colorMode              == other.colorMode
		//     && orientation            == other.orientation
		//     && glyphScale             == other.glyphScale
		//     && show_voxel_selection   == other.show_voxel_selection
		//     && glyphConfiguration     == other.glyphConfiguration;

		    return false;
	}

	virtual void fromJSON( nlohmann::json j, const std::string & baseDirectory = "" ) override
	{
    	statusReport = "";
    	status = TN::ConfigStatus::Incomplete;
    	std::pair< bool, std::string > parseStatus;
		bool found = true;

		/////////////////////////////////////////////

    	int nMissingRequired = 0;

		nMissingRequired = TN::CF::getParameter( j, "name", name, parseStatus, TN::CF::REQUIRED, std::string( "" ) );

		if( nMissingRequired )
		{
    		parseStatus.second += "Error: missing " + std::to_string( nMissingRequired ) + "required parameters.\n";
			parseStatus.first = false; 
			status = TN::ConfigStatus::Incomplete;		
		}

		///////////////////////////////////////////////////////////////////////////////////////////

		found = TN::CF::getParameter( j,      "description",      description, parseStatus, TN::CF::OPTIONAL, std::string( "" ) );
		found = TN::CF::getParameter( j,            "title",            title, parseStatus, TN::CF::OPTIONAL, std::string( "" ) );
		
		found = TN::CF::getParameter( j,     "show_bounding_box",     show_bounding_box, parseStatus, TN::CF::OPTIONAL, bool( true ) );
		found = TN::CF::getParameter( j,             "show_axis", 	          show_axis, parseStatus, TN::CF::OPTIONAL, bool( true ) );
		found = TN::CF::getParameter( j, "show_axis_coordinates", show_axis_coordinates, parseStatus, TN::CF::OPTIONAL, bool( true ) );

		///////////////////////////////////////////////////////////////////////////////////////

		// TODO : missing surface sets, glyph layers, ...

		///////////////////////////////////////////////////////////////////////////////////////


		status = TN::ConfigStatus::Ok;			
		defined = true;
	}

	virtual nlohmann::json toJSON() override
	{
		nlohmann::json j;
		return j;
	}

	void updateInformation( 
		const TN::SurfaceSetCollection & surfaceInformation,
		int nLayers )
	{
		const auto & surfaceSets = surfaceInformation.surfaceSets();
		for( const auto & surfaceSet : surfaceSets )
		{
			// if surface set doesn't exist, then create a new visibility structure for it

			if( ! surface_visibility.count( surfaceSet.first ) )
			{
				surface_visibility.insert( { surfaceSet.first, {} } );
				surface_set_visibility.insert( { surfaceSet.first, false } );

				auto & elems   = surface_visibility.at( surfaceSet.first );

				for( auto & surface : surfaceSet.second->surfaces )
				{
					elems.insert( { surface.first, false } );
				}
			}

			// TODO: handle case where a surface set has been modified
			// should consider using a static unique id to identify / map so that
			// the name can be modified without hassle
		}

		while( glyphLayerVisibility.size() > nLayers )
		{
			glyphLayerVisibility.pop_back();
		}
		while( glyphLayerVisibility.size() < nLayers )
		{
			glyphLayerVisibility.push_back( 0 );
		}
	}

    SpatialViewConfiguration() : ConfigurationBase() { init(); }
    virtual ~SpatialViewConfiguration() {}
};

}

#endif 