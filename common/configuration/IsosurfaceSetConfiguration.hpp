#ifndef MARRUS_ISOSURFACE_SET_CONFIG_HPP
#define MARRUS_ISOSURFACE_SET_CONFIG_HPP

#include "configuration/ConfigurationBase.hpp"

#include "geometry/Vec.hpp"
#include "input/JsonUtils.hpp"
#include "utils/ColorUtils.hpp"

#include <jsonhpp/json.hpp>

#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <utility>
#include <cstdint>
#include <iostream>

namespace TN {

struct IsosurfaceSetConfiguration : public ConfigurationBase
{
	std::string name;
	std::string field;
	std::vector< float > isovalues;
    std::string color_map;
	std::vector< TN::Vec4 > colors;

	std::string color_field_tf;
	std::string color_field;
	TN::Vec2< float > mapping_range;

	LightingConfiguration lighting;

	virtual void init() override
	{
		color_map = "Spectral";
	}

	virtual void fromJSON( nlohmann::json j, const std::string & baseDirectory = "" ) override
	{
    	status = TN::ConfigStatus::Incomplete;
    	std::pair< bool, std::string > parseStatus;
		bool found = true;

		/////////////////////////////////////////////

    	int nMissingRequired = 0;

		nMissingRequired  = ! TN::CF::getParameter( j, "name",            name, parseStatus, TN::CF::REQUIRED, std::string( "" ) );
		nMissingRequired += ! TN::CF::getParameter( j, "field",          field, parseStatus, TN::CF::REQUIRED, std::string( "" ) );
		nMissingRequired += ! TN::CF::getParameter( j, "isovalues", isovalues, parseStatus, TN::CF::REQUIRED, {} );

		if( nMissingRequired )
		{
    		parseStatus.second += "Error: missing " + std::to_string( nMissingRequired ) + "required parameters.\n";
			parseStatus.first = false; 
			status = TN::ConfigStatus::Incomplete;		
		}
		else
		{
			status = TN::ConfigStatus::Ok;
		}

		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		// TODO : need to handle colors if array instead of string

		nlohmann::json color_obj;
		found = TN::CF::getParameter( j, "colors", color_map, parseStatus, TN::CF::OPTIONAL, std::string("") );

		if ( ! found )
		{
			color_map = "Spectral";
		}
		
		if( ! TN::haveColorMap( color_map ) ) 
		{
			parseStatus.second += "Error: colors parameter, invalid colormap key, use matplotlib colormaps, case sensative. Defaulting to Spectral.\n";
			color_map = "Spectral";
		}

		colors.resize( isovalues.size() );
		auto cmPathRelative = TN::colorMapPathRelative( color_map );
		TN::mapColors( 
			baseDirectory + "/../TF/" + cmPathRelative, 
			isovalues, 
			colors );

		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		found = TN::CF::getParameter( j, "color_field_tf", color_field_tf, parseStatus, TN::CF::OPTIONAL, std::string( "" ) );
		found = TN::CF::getParameter( j, "color_field",       color_field, parseStatus, TN::CF::OPTIONAL, std::string( "" ) );
		found = TN::CF::getParameter( j, "mapping_range",   mapping_range, parseStatus, TN::CF::OPTIONAL, TN::Vec2<float>() );

		nlohmann::json lightingConfig;
		found = TN::CF::getParameter( j, "lighting", lightingConfig, parseStatus, TN::CF::OPTIONAL, nlohmann::json() );
		if( found ) { lighting.fromJSON( lightingConfig ); }
	}

	virtual nlohmann::json toJSON() override
	{
		nlohmann::json j;
		return j;
	}

    IsosurfaceSetConfiguration() : ConfigurationBase() { init(); }
    virtual ~IsosurfaceSetConfiguration() {}
};

}

#endif 