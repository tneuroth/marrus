#ifndef TN_GLYPH_CONFIGURATION_HPP
#define TN_GLYPH_CONFIGURATION_HPP

#include <string>
#include <vector>
#include <cstdint>
#include <iostream>

#include <jsonhpp/json.hpp>

#include "configuration/ConfigurationBase.hpp"
#include "input/JsonUtils.hpp"

namespace TN {

struct GlyphFeature {

	std::string name;
	std::string latex;
	std::string data_file; 
	int file_offset; 
	std::string data_type;
	std::vector< int > dims;

	int size() const
	{
		int result = 1;
		for( int i = 0; i < dims.size(); ++i )
		{
			result = result*dims[ i ];
		}
		return result;
	}
};

struct GlyphAxis{
	std::string name;
	std::string latex;
	std::string encoding;
	std::vector< GlyphFeature > components;
};

struct GlyphVector{
	std::string name;
	std::string latex;
	std::string encoding;
	GlyphFeature x;
	GlyphFeature y;
	GlyphFeature z;
};

struct GlyphConfiguration : public ConfigurationBase
{
	std::string name;
	std::string glyph_type;
	std::string normalization;
	std::vector< GlyphAxis > axes;

	std::vector< int > active_axes;
	std::vector< int > active_components;

	std::vector< TN::Vec3< float > > component_colors;

	std::vector< GlyphVector > vectors;
	std::vector< TN::Vec3< float >  > vectorColors;

	std::string obj_file;
	std::string tex_file;

	std::vector< std::string > getAxesLatexPaths( const std::string & D ) const
	{
		std::vector< std::string > result;
		
		if( glyph_type == "radial" || glyph_type == "star" )
		{
			for( auto & a : axes )
			{
				result.push_back( D + "/" + a.name + ".png" );
			}
		}
		else if ( glyph_type == "directional" )
		{
			for( auto & v : vectors )
			{
				result.push_back( D + "/" + v.name + ".png" );
			}
		}

		return result;
	}

	std::vector< std::string > getCompLatexPaths( const std::string & D ) const
	{
		std::vector< std::string > result;

		if( glyph_type == "radial" || glyph_type == "star" )
		{
			if( axes.size() > 0 )
			{
				auto & first = axes[ 0 ];
				for( auto & c : first.components )
				{
					result.push_back( D + "/" + c.name + ".png" );
				}
			}
		}
		else if ( glyph_type == "directional" )
		{
			
		}

		return result;
	}


	size_t componentsPerAxis() const
	{
		size_t result = 0;

		if( glyph_type == "radial" || glyph_type == "star" )
		{
			if( axes.size() == 0 )
			{
				result = 0;
			}
			else 
			{
				result = axes[ 0 ].components.size();
			}
        }
        else if ( glyph_type == "directional" )
        {
        	result = 3;
        }
        return result;
	}

	size_t totalNumFields() const 
	{
		size_t result = 0;
		
		if( glyph_type == "radial" || glyph_type == "star" )
		{
			result = componentsPerAxis() * axes.size();
        }
        else if ( glyph_type == "directional" )
        {
        	result = vectors.size() * 3;
        }

        return result;
	}

	virtual void fromJSON( nlohmann::json j, const std::string & baseDirectory = "" ) 
	{
    	status = TN::ConfigStatus::Incomplete;
    	std::pair< bool, std::string > parseStatus;
		bool found = true;
        std::string tmpStr;
        std::vector< nlohmann::json > tmpJsonVec;

		///////////////////////////////////////////////////////////////////////

        found = TN::CF::getParameter( j, "name", tmpStr, parseStatus, TN::CF::OPTIONAL, std::string("") );
        if( found )
        {
            name = tmpStr;
        }

        found = TN::CF::getParameter( j, "glyph_type", tmpStr, parseStatus, TN::CF::OPTIONAL, std::string("") );
        if( found )
        {
            glyph_type = tmpStr;
        }

        if( glyph_type == "directional" )
        {
        	obj_file = "vector2.obj";
        	tex_file = "color_texture.png";

        	// For now we add color maps for each vector automatically, so we can plug them directly into the 
        	// existing rendering pipeline, and also have the option to encode values onto them based on color
        	// in the future. Assumes there are less than 7 vectors.

	        bool foundActiveVectors = TN::CF::getParameter( j, "active_vectors", active_axes,  parseStatus, TN::CF::OPTIONAL, { 0 } );

	        std::vector< nlohmann::json > vectors_json;
	        found = TN::CF::getParameter( j, "vectors", vectors_json, parseStatus, TN::CF::OPTIONAL, {} );

	        std::vector< std::string > defaultCMS = 
	        {
	        	"sequential/Blues",
	        	"sequential/Reds",
	        	"sequential/Greens",
	        	"sequential/Oranges",
	        	"sequential/Purples",
	        	"sequential/Greys"
	        };

	        if( found )
	        {
	        	size_t cm_idx = 0;
		        for( auto & vector_json : vectors_json ) {

		        	vectors.push_back({});
		        	vectorColors.push_back( { 1.0f, 0.2f, 0.2f } );

		        	auto & vector = vectors.back();
		        	auto & color  = vectorColors.back();

		        	vector.encoding = defaultCMS[ cm_idx % defaultCMS.size() ];
		        	++cm_idx;

		            found = TN::CF::getParameter( vector_json,  "name", vector.name,  parseStatus, TN::CF::OPTIONAL, std::string( "" ) );
		            found = TN::CF::getParameter( vector_json, "latex", vector.latex, parseStatus, TN::CF::OPTIONAL, std::string( "" ) ); 

		            std::vector< nlohmann::json > color_json;
		            std::vector< float > cl;
		            found = TN::CF::getParameter( vector_json, "color", cl, parseStatus, TN::CF::OPTIONAL, { 0.f, 0.f, 0.f } );
		            color = { cl[ 0 ], cl[ 1 ], cl[ 2 ] };

					nlohmann::json x_json;

	        		found = TN::CF::getParameter( vector_json, "x", x_json, parseStatus, TN::CF::OPTIONAL, {} );
					found = TN::CF::getParameter( x_json, "name",   vector.x.name,        parseStatus, TN::CF::OPTIONAL, std::string( "" ) ); 
					found = TN::CF::getParameter( x_json, "latex",  vector.x.latex,       parseStatus, TN::CF::OPTIONAL, std::string( "" ) ); 
					found = TN::CF::getParameter( x_json, "file",   vector.x.data_file,   parseStatus, TN::CF::OPTIONAL, std::string( "" ) );
					found = TN::CF::getParameter( x_json, "offset", vector.x.file_offset, parseStatus, TN::CF::OPTIONAL, int(0) );
					found = TN::CF::getParameter( x_json, "type",   vector.x.data_type,   parseStatus, TN::CF::OPTIONAL, std::string( "float" ) );

					nlohmann::json y_json;

	        		found = TN::CF::getParameter( vector_json, "y", y_json, parseStatus, TN::CF::OPTIONAL, {} );
					found = TN::CF::getParameter( y_json, "name",   vector.y.name,        parseStatus, TN::CF::OPTIONAL, std::string( "" ) ); 
					found = TN::CF::getParameter( y_json, "latex",  vector.y.latex,       parseStatus, TN::CF::OPTIONAL, std::string( "" ) ); 
					found = TN::CF::getParameter( y_json, "file",   vector.y.data_file,   parseStatus, TN::CF::OPTIONAL, std::string( "" ) );
					found = TN::CF::getParameter( y_json, "offset", vector.y.file_offset, parseStatus, TN::CF::OPTIONAL, int(0) );
					found = TN::CF::getParameter( y_json, "type",   vector.y.data_type,   parseStatus, TN::CF::OPTIONAL, std::string( "float" ) );

					nlohmann::json z_json;

	        		found = TN::CF::getParameter( vector_json, "z", z_json, parseStatus, TN::CF::OPTIONAL, {} );
					found = TN::CF::getParameter( z_json, "name",   vector.z.name,        parseStatus, TN::CF::OPTIONAL, std::string( "" ) ); 
					found = TN::CF::getParameter( z_json, "latex",  vector.z.latex,       parseStatus, TN::CF::OPTIONAL, std::string( "" ) ); 
					found = TN::CF::getParameter( z_json, "file",   vector.z.data_file,   parseStatus, TN::CF::OPTIONAL, std::string( "" ) );
					found = TN::CF::getParameter( z_json, "offset", vector.z.file_offset, parseStatus, TN::CF::OPTIONAL, int(0) );
					found = TN::CF::getParameter( z_json, "type",   vector.z.data_type,   parseStatus, TN::CF::OPTIONAL, std::string( "float" ) );
		        }
	        }
        }
        else if( glyph_type == "experimental" )
        {
			found = TN::CF::getParameter( tmpStr, "obj_file", obj_file, parseStatus, TN::CF::OPTIONAL, std::string( "" ) );
			found = TN::CF::getParameter( tmpStr, "tex_file", tex_file, parseStatus, TN::CF::OPTIONAL, std::string( "" ) ); 
        }
        else if( glyph_type == "radial" || glyph_type == "star"  )
        {
        	obj_file = "radial.obj";
        	tex_file = "radial.png";

	        found = TN::CF::getParameter( j, "normalization", tmpStr, parseStatus, TN::CF::OPTIONAL, std::string("") );
	        if( found )
	        {
	            normalization = tmpStr;
	        }

	        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////

	        bool foundActiveAxes = TN::CF::getParameter( j, "active_axes",            active_axes,  parseStatus, TN::CF::OPTIONAL, { 0 } );
	        bool foundActiveCmps = TN::CF::getParameter( j, "active_components", active_components, parseStatus, TN::CF::OPTIONAL, { 0 } );

	        std::vector< nlohmann::json > component_colors_json;
	        bool foundComponentColors = TN::CF::getParameter( j, "component_colors", component_colors_json, parseStatus, TN::CF::OPTIONAL, { {} } );
	        
	        std::cout << "found component colors? " << foundComponentColors << " " << component_colors_json.size() << std::endl;

	        std::vector< TN::Vec3< float > > DEFAULT_CMP_COLORS = 
	        {
	            { 0.f, 114 / 255.f, 178 / 255.f      },  // blue 
	            { 0.f, 158 / 255.f, 115 / 255.f      },  // green
	            { 213/ 255.f, 94 / 255.f, 0.f        },  // red
	            { 230/ 255.f, 159 / 255.f, 0.f       },  // orange
	            { 183/ 255.f, 167 / 255.f, 44/ 255.f },  // gold
	            { 204/ 255.f, 121 / 255.f, 67/ 255.f },  // pink
	            { 86/ 255.f, 180 / 255.f, 233/ 255.f },  // sky blue
	            { 10/ 255.f, 10 / 255.f, 10/ 255.f   },  // black
	            { 70/ 255.f, 70 / 255.f, 70/ 255.f   }   // gray     
	        };

	        for ( auto & component__color_json : component_colors_json ) 
	        {
	        	auto val = component__color_json.get< std::vector< float> > ();
	        	component_colors.push_back( {  val[ 0 ], val[ 1 ], val[ 2 ] } );

	        	std::cout << "adding color " << val[ 0 ] << " " << val[ 1 ] << " " << val[ 2 ] << std::endl;
	        }

	        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////

	        std::vector< nlohmann::json > axes_json;
	        found = TN::CF::getParameter( j, "axes", axes_json, parseStatus, TN::CF::OPTIONAL, {} );

	        if( found )
	        {
		        for( auto & axis_json : axes_json ) {

		        	axes.push_back({});
		        	auto & axis = axes.back();

		            found = TN::CF::getParameter( axis_json,     "name", axis.name,     parseStatus, TN::CF::OPTIONAL, std::string( "" ) );
		            found = TN::CF::getParameter( axis_json,    "latex", axis.latex,    parseStatus, TN::CF::OPTIONAL, std::string( "" ) ); 
		            found = TN::CF::getParameter( axis_json, "encoding", axis.encoding, parseStatus, TN::CF::OPTIONAL, std::string( "" ) ); 

					std::vector< nlohmann::json > components_json;
	        		found = TN::CF::getParameter( axis_json, "components", components_json, parseStatus, TN::CF::OPTIONAL, {} );

	        		int cmp_idx = 0;

		        	for( auto & component_json : components_json ) {

		        		axis.components.push_back({});
		        		auto & component = axis.components.back();

		        		// for now assume it is a univariate feature 
		        		component.dims = { 1 };

		            	found = TN::CF::getParameter( component_json, "name",   component.name,        parseStatus, TN::CF::OPTIONAL, std::string( "" ) ); 
		            	found = TN::CF::getParameter( component_json, "latex",  component.latex,       parseStatus, TN::CF::OPTIONAL, std::string( "" ) ); 
			            found = TN::CF::getParameter( component_json, "file",   component.data_file,   parseStatus, TN::CF::OPTIONAL, std::string( "" ) );
			            found = TN::CF::getParameter( component_json, "offset", component.file_offset, parseStatus, TN::CF::OPTIONAL, int(0) );
			            found = TN::CF::getParameter( component_json, "type",   component.data_type,   parseStatus, TN::CF::OPTIONAL, std::string( "float" ) );

			            // default component colors if none are specified
			            if( ! foundComponentColors )
			            {
			            	component_colors.push_back( DEFAULT_CMP_COLORS[ cmp_idx % DEFAULT_CMP_COLORS.size() ] );
			            	++cmp_idx;
			            }
		        	}

		        	foundComponentColors = true;
		        }
	        }
    	}

		status = TN::ConfigStatus::Ok;
	}

	virtual nlohmann::json toJSON()
	{
		nlohmann::json js;
		return js;
	}

	virtual void init() {
		name       = "default";
		glyph_type = "experimental";
		obj_file   = "domino_t.obj";
		tex_file   = "domino_texture.png";
	}
    
    GlyphConfiguration() : ConfigurationBase() { init(); }
    virtual ~GlyphConfiguration() {}
};

}

#endif