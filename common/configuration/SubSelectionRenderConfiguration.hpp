#ifndef MARRUS_SUB_SELECTION_RENDER_CONFIG_HPP
#define MARRUS_SUB_SELECTION_RENDER_CONFIG_HPP

#include "configuration/ConfigurationBase.hpp"
#include "configuration/LightingConfiguration.hpp"

#include "geometry/Vec.hpp"
#include "input/JsonUtils.hpp"

#include <jsonhpp/json.hpp>

#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <utility>
#include <cstdint>
#include <iostream>

namespace TN {

struct SubSelectionRenderConfiguration : public ConfigurationBase
{
	std::string color_tf;
	std::string color_field;
	TN::Vec2< float > mapping_range;
	bool log_map;
	bool visible;
	LightingConfiguration lighting;
	TN::Vec4 solid_color;
	std::string color_mode;
	bool visible;
	LightingConfiguration lighting;

	virtual void init() override
	{

	}

	virtual void fromJSON( nlohmann::json j, const std::string & baseDirectory = "" ) override
	{


	}

	virtual nlohmann::json toJSON() override
	{
		nlohmann::json j;
		return j;
	}

    SubSelectionRenderConfiguration() : ConfigurationBase() {}
    virtual ~SubSelectionRenderConfiguration() {}
};

}

#endif 