#ifndef MARRUS_CLUSTER_MAP_CONFIG_HPP
#define MARRUS_CLUSTER_MAP_CONFIG_HPP

#include "configuration/ConfigurationBase.hpp"

#include "geometry/Vec.hpp"
#include "input/JsonUtils.hpp"

#include <jsonhpp/json.hpp>

#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <utility>
#include <cstdint>
#include <iostream>

namespace TN {

struct NumericFeatureConfiguration {

};

struct ClusterMapConfiguration : public ConfigurationBase
{
	std::vector< NumericFeatureConfiguration > features;
	
	virtual void init() override
	{

	}

	virtual void fromJSON( nlohmann::json j, const std::string & baseDirectory = "" ) override
	{

	}

	virtual nlohmann::json toJSON() override
	{
		nlohmann::json j;
		return j;
	}

    ClusterMapConfiguration() : ConfigurationBase() {}
    virtual ~ClusterMapConfiguration() {}
};

}

#endif 