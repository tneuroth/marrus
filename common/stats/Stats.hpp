// NOTES

// physical distance is important as well because it implies higher causal relationships
// however it isn't that interesting to cluster based on similarity+distance because 
// nearby cells should be similar anyways, and that feature doesn't imply anything anomalous
// instead, it would be interesting to combine distance+dissimilarity, so that we see which 
// cells are nearby to dissimilar ones which is more anomalous 
// however, dissimilarityXdistance is sensative to decomposition method and it's affect
// on local statistical coherency of the cells
// so the system should decompose in a precise and reproducable optimized way so that the 
// resulting measures are better interetable and more reliable

// (proximityXdissimilarity)

// could also cluster the cells based on similarity
// and then project based on (proximityXdissimilarity)
// so that patterns between them show a repeated phenomena 

// also, euclidean vs layer traversal distance is interesting, because 
// if two cells have close euclidean distance, yet far away level-manifold
// distance, then it represents the interaction of a single suface with itself
// for example, flame-flame interaction

// how to make a generalizable mixture distance function, that operates on the 
// available data, (e.g. from collections of pdfs, moments, or raw data)

// local scale distribution averaging is important (locally spatial/temporally) 
// as (1) it is a fluctuating/periodic quantity, which when averaged,
// shows a better representation
// (2) because each voxel would have a 1d histogram, which is a lot of data,
// by spatial averaging, each cell has a 1d histogram, which is way smaller

// add cross view critical value marks

// KLD
// MI 

// histograms

// moments
// uncertainty of pdf based on moment estimation error

// could make an argument that could be done one node using the shared memory, which has 22 cpus on Summit, because the data is 1/22 the size 
// of the full domain, and the simulation uses the GPU and not the cpus, so could do both in parallel, with 1 ts lag.

// need to be able to verify the spatial decomposition, and tune/update it 
// interactivally 

// background texture for voxel plot context
// maybe also per-layer?
// pre-rendered

// from the starting point, should let user choose subselection
// should estimate memory requirements, for CPU and GPU
// should be able to switch chunks with the same settings

// in the end, should estimate the cost of the in situ generated
// summaries as well

// might as well compute the decomposition data from the 
// start based on default parameters

// also a default clustering

// in the cluster view / tree view, should be able to 
// switch between the metrics for clustering and 
// for colormapping 

// need to read the other paper on usibility wth cart. stat decompos

// need tree view auto scale

#ifndef TN_STATS_HPP
#define TN_STATS_HPP

#include "model/Hist2DParams.hpp"
#include "utils/IndexUtils.hpp"

#include <cmath>
#include <iostream>
#include <vector>
#include <string>
#include <fstream>

namespace TN
{

namespace STATS
{

struct Moments1D
{
    double mean;
    double variance;
    double skewness;    
    double kurtosis;
};

struct Moments2D
{
    Moments1D x;
    Moments1D y;

    double coVariance;
    double coSkewness;
    double coKurtosis;
};

struct DecomposedJointStatistics
{
    std::string              xKey;
    std::string              yKey;
    std::vector< float     > hist2ds;
    TN::Vec2< int >          histDims;
    std::vector< float     > counts; 
    std::vector< float     > outliersL; 
    std::vector< float     > outliersR; 
    std::vector< float     > outliersT; 
    std::vector< float     > outliersB;
    std::vector< float     > outliersLT;
    std::vector< float     > outliersLB;
    std::vector< float     > outliersRT;
    std::vector< float     > outliersRB;

    std::vector< Moments2D > sampleMoments;
    std::vector< Moments2D > estimatedMoments;

    static constexpr double MIN_SAMPLE = 169.f;
    bool haveRawMoments;

    void setKeys( const std::string & xk, const std::string & yk )
    {
        xKey = xk;
        yKey = yk;
    }
};

inline void estimateMoments(

    // inputs

    const float * hist2D,
    const float * histX,
    const float * histY,        
    const TN::Vec2< int > & dims,
    const TN::Vec2< float > & xR,
    const TN::Vec2< float > & yR, 

    // outputs

    Moments2D & moments )
{
    /***************************  x ******************************/ 

    const int XD = dims.x();

    moments.x.mean     = 0.0;
    moments.x.variance = 0.0;
    moments.x.skewness = 0.0;    
    moments.x.kurtosis = 0.0;

    double m2x = 0.0;
    double m3x = 0.0;
    double m4x = 0.0;

    float dx = ( xR.b() - xR.a() ) / XD;

    for( int i = 0; i < XD; ++i )
    {
        double binCenter = xR.a() + dx * ( i + 1 / 2.0 );
        moments.x.mean += histX[ i ] * binCenter;
    }
    
    for( int i = 0; i < XD; ++i )
    {
        double binCenter = xR.a() + dx * ( i + 1 / 2.0 );
        double diffX = binCenter - moments.x.mean;

        m2x += std::pow( diffX, 2.f ) * histX[ i ];
        m3x += std::pow( diffX, 3.f ) * histX[ i ];
        m4x += std::pow( diffX, 4.f ) * histX[ i ];
    }    

    moments.x.variance = m2x;
    double sdevXH = std::sqrt( moments.x.variance );
    moments.x.skewness = m3x / std::pow( sdevXH, 3.0 ); 
    moments.x.kurtosis = m4x / std::pow( m2x, 2.0 ) - 3.0; 

    /***************************  y ******************************/ 

    const int YD = dims.y();

    moments.y.mean = 0;
    moments.y.variance  = 0;
    moments.y.skewness  = 0;
    moments.y.kurtosis  = 0;

    double m2y = 0.0;
    double m3y = 0.0;
    double m4y = 0.0;  

    float dy = ( yR.b() - yR.a() ) / YD;

    for( int i = 0; i < YD; ++i )
    {
        double binCenter = yR.a() + dy * ( i + 1 / 2.0 );
        moments.y.mean += histY[ i ] * binCenter;
    }

    for( int i = 0; i < YD; ++i )
    {
        double binCenter = yR.a() + dy * ( i + 1 / 2.0 );
        double diffY = binCenter - moments.y.mean;

        m2y += std::pow( diffY, 2.f ) * histY[ i ];
        m3y += std::pow( diffY, 3.f ) * histY[ i ];
        m4y += std::pow( diffY, 4.f ) * histY[ i ];  
    }    

    moments.y.variance = m2y;
    double sdevYH = std::sqrt( moments.y.variance );
    moments.y.skewness = m3y / std::pow( sdevYH, 3.0 ); 
    moments.y.kurtosis = m4y / std::pow( m2y, 2.0 ) - 3.0; 

    /*************************** xy *****************************/

    moments.coVariance = 0.0;
    moments.coSkewness = 0.0;
    moments.coKurtosis = 0.0;

    for( int i = 0; i < XD; ++i )
    {
        for( int j = 0; j < YD; ++ j )
        {
            double cX = xR.a() + dx * ( i + 1 / 2.0 );
            double cY = yR.a() + dy * ( j + 1 / 2.0 );        

            double diffX = cX - moments.x.mean;
            double diffY = cY - moments.y.mean;        

            double p = hist2D[ flatIndex2dCM( i, j, XD, YD ) ]; 

            moments.coVariance += diffX * diffY * p;
            moments.coSkewness += diffX * diffX * diffY * p;
            moments.coKurtosis += diffX * diffX * diffY * diffY * p;
        }
    }

    moments.coSkewness /=  moments.x.variance 
                         * std::sqrt( moments.y.variance );
    moments.coKurtosis /=  moments.x.variance 
                         * moments.y.variance;
}

double entropy(
    const float * p1,
    const int64_t NP )
{
    double ent = 0.0;
    for( int64_t i = 0; i < NP; ++i )
    {
        if( p1[ i ] > 0 )
        {
            ent += p1[ i ] * std::log2( p1[ i ] );
        }
    }
    return -ent;
}

void tsne1( 
    const std::string & projDir,
    const int64_t N_HISTS,
    std::vector< float > & projX,
    std::vector< float > & projY )
{
    std::ifstream inFile(  projDir + "/projection.1.bin", std::ios::in | std::ios::binary );
    if( ! inFile.is_open() ) { 
        std::cerr << "Error: couldn't open file: " << projDir + "/projection.1.bin" << std::endl;
    }

    projX.resize( N_HISTS );
    projY.resize( N_HISTS );

    inFile.read( (char*) projX.data(), N_HISTS * sizeof( float ) );
    inFile.read( (char*) projY.data(), N_HISTS * sizeof( float ) );
    inFile.close();
}

void tsne0( 
    const std::string & projDir,    
    const int64_t N_HISTS,
    std::vector< float > & projX,
    std::vector< float > & projY )
{
    std::ifstream inFile(  projDir +  "/projection.0.bin", std::ios::in | std::ios::binary );
    if( ! inFile.is_open() ) { 
        std::cerr << "Error: couldn't open file: " << projDir + "/projection.0.bin" << std::endl;
    }    

    projX.resize( N_HISTS );
    projY.resize( N_HISTS );

    inFile.read( (char*) projX.data(), N_HISTS * sizeof( float ) );
    inFile.read( (char*) projY.data(), N_HISTS * sizeof( float ) );
    inFile.close();
}

void tsne2et( 
    const std::string & projDir,    
    int64_t nsites,
    std::vector< float > & projX,
    std::vector< float > & projY )
{
    std::ifstream inFile( projDir + "/projection.2.bin", std::ios::in | std::ios::binary );
    if( ! inFile.is_open() ) { 
        std::cerr << "Error: couldn't open file: " << projDir + "/projection.2.bin" << std::endl;
    }    

    projX.resize( nsites );
    projY.resize( nsites );
    
    inFile.read( (char*) projX.data(), nsites * sizeof( float ) );
    inFile.read( (char*) projY.data(), nsites * sizeof( float ) );
    inFile.close();
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////


void tsne2et( 
    const std::string & baseDir,   
    const std::string & projDir,   
    const std::vector< float >   & d_euclidean,
    const std::vector< float >   & d_topological,
    const std::vector< int32_t > & siteComponents,
    const std::vector< int16_t > & siteLayers,     
    const std::map< int32_t, std::vector< int32_t > > & componentHierarchy, 
    double a,
    double b,
    double cP,
    double lP,
    int64_t nsites,
    std::vector< float > & projX,
    std::vector< float > & projY )
{
    std::vector< float > d( d_euclidean.size(), 0.f );

    for( int i = 0; i < nsites; ++i )
    for( int j = 0; j < nsites; ++j )
    {
        const int64_t flatId = i * nsites + j;
        if( i == j )
        {
            d[ flatId ] = 0;
            continue;
        }
        else
        {
            const auto & containedI = componentHierarchy.at( siteComponents[ i ] );
            const auto & containedJ = componentHierarchy.at( siteComponents[ j ] );

            const int C = ( siteLayers[  i ] == siteLayers[     j ] )
                    && ( siteComponents[ i ] == siteComponents[ j ] ) ? 0
                    :  ( siteLayers[     i ] == siteLayers[     j ] )
                    && ( siteComponents[ i ] != siteComponents[ j ] ) ? 1 
                    :  ( std::find( containedI.begin(), containedI.end(), siteComponents[ j ] ) != containedI.end() )
                    || ( std::find( containedJ.begin(), containedJ.end(), siteComponents[ i ] ) != containedJ.end() ) ? 2
                    : 3;

            // same layer, same component 
            if( C == 0 )
            {
                if( d_topological[ flatId ] > 0 )
                {
                    d[ flatId ] = ( a * d_euclidean[ flatId ] ) / ( b * d_topological[ flatId ] ); 
                }
                else
                {
                    d[ flatId ] = a * d_euclidean[ flatId ];
                }
            }
            // same layer, different component
            else if( C == 1 )
            {
                d[ flatId ] = ( a * d_euclidean[ flatId ] ) * cP; 
            }
            // different layer, same layer same topological sphere
            else if( C == 2 )
            {
                d[ flatId ] = ( a * d_euclidean[ flatId ] ); 
            }
            // different layer, different topological sphere            
            else
            {
                d[ flatId ] = ( a * d_euclidean[ flatId ] ) * lP; 
            }
        }
    }

    std::ofstream outFile( projDir + "distances.2.bin", std::ios::out | std::ios::binary );
    outFile.write( (char*) d.data(), d.size() * sizeof( float ) );
    outFile.close();

    outFile.open( projDir + "dims_counts.2.txt" );
    outFile << nsites << " " << nsites;
    outFile.close();

    system( ( "conda run -n cuda python " + baseDir + "/python/tsne2_cuda.py " + projDir ).c_str() );

    projX.resize( nsites );
    projY.resize( nsites );

    std::ifstream inFile( projDir + "projection.2.bin", std::ios::in | std::ios::binary );
    inFile.read( (char*) projX.data(), nsites * sizeof( float ) );
    inFile.read( (char*) projY.data(), nsites * sizeof( float ) );
    inFile.close();
}


// void tsne2( 
//     const std::string & projDir,       
//     const std::vector< float > & distances,
//     int64_t nsites,
//     std::vector< float > & projX,
//     std::vector< float > & projY )
// {
//     std::ofstream outFile( projDir + "distances.2.bin", std::ios::out | std::ios::binary );
//     outFile.write( (char*) distances.data(), distances.size() * sizeof( float ) );
//     outFile.close();

//     outFile.open( projDir + "dims_counts.2.txt" );
//     outFile << nsites << " " << nsites;
//     outFile.close();

//     system( ( "conda run -n cuda python " + baseDir + "/python/tsne2_cuda.py " + projDir ).c_str() );

//     projX.resize( nsites );
//     projY.resize( nsites );

//     std::ifstream inFile(  projDir + "projection.2.bin", std::ios::in | std::ios::binary );
//     inFile.read( (char*) projX.data(), nsites * sizeof( float ) );
//     inFile.read( (char*) projY.data(), nsites * sizeof( float ) );
//     inFile.close();
// }



void tsne2( 
    const std::string & baseDir,        
    const std::string & projDir,       
    std::vector< float > & histograms,
    const TN::Vec2< int > & dims,
    const int64_t N_HISTS,
    std::vector< float > & projX,
    std::vector< float > & projY )
{
    // for now just use python, which currently requires going through the filesystem
    // https://pot.readthedocs.io/en/gromov/all.html#module-ot

    std::ofstream outFile( projDir + "histograms.2.bin", std::ios::out | std::ios::binary );
    outFile.write( (char*) histograms.data(), histograms.size() * sizeof( float ) );
    outFile.close();

    outFile.open( projDir + "dims_counts.2.txt" );
    outFile << std::to_string( dims.a() ) << " " << std::to_string( dims.b() ) << " " << std::to_string( N_HISTS );
    outFile.close();

    system( ( "conda run -n cuda python " + baseDir + "/python/tsne1_cuda-2.py " + projDir ).c_str() );

    projX.resize( N_HISTS );
    projY.resize( N_HISTS );

    std::ifstream inFile(  projDir + "projection.2.bin", std::ios::in | std::ios::binary );
    inFile.read( (char*) projX.data(), N_HISTS * sizeof( float ) );
    inFile.read( (char*) projY.data(), N_HISTS * sizeof( float ) );
    inFile.close();
}

void tsne1( 
    const std::string & baseDir,   
    const std::string & projDir,       
    std::vector< float > & histograms,
    const TN::Vec2< int > & dims,
    const int64_t N_HISTS,
    std::vector< float > & projX,
    std::vector< float > & projY )
{
    // for now just use python, which currently requires going through the filesystem
    // https://pot.readthedocs.io/en/gromov/all.html#module-ot

    std::ofstream outFile( projDir + "histograms.1.bin", std::ios::out | std::ios::binary );
    outFile.write( (char*) histograms.data(), histograms.size() * sizeof( float ) );
    outFile.close();

    outFile.open( projDir + "dims_counts.1.txt" );
    outFile << std::to_string( dims.a() ) << " " << std::to_string( dims.b() ) << " " << std::to_string( N_HISTS );
    outFile.close();

    system( ( "conda run -n cuda python " + baseDir + "/python/tsne1_cuda.py " + projDir ).c_str() );

    projX.resize( N_HISTS );
    projY.resize( N_HISTS );

    std::ifstream inFile(  projDir + "projection.1.bin", std::ios::in | std::ios::binary );
    inFile.read( (char*) projX.data(), N_HISTS * sizeof( float ) );
    inFile.read( (char*) projY.data(), N_HISTS * sizeof( float ) );
    inFile.close();
}

void tsne0( 
    const std::string & baseDir,   
    const std::string & projDir,       
    std::vector< float > & histograms,
    const TN::Vec2< int > & dims,
    const int64_t N_HISTS,
    std::vector< float > & projX,
    std::vector< float > & projY )
{
    // for now just use python, which currently requires going through the filesystem
    // https://pot.readthedocs.io/en/gromov/all.html#module-ot

    std::ofstream outFile( projDir + "histograms.0.bin", std::ios::out | std::ios::binary );
    outFile.write( (char*) histograms.data(), histograms.size() * sizeof( float ) );
    outFile.close();

    outFile.open( projDir + "dims_counts.0.txt" );
    outFile << std::to_string( dims.a() ) << " " << std::to_string( dims.b() ) << " " << std::to_string( N_HISTS );
    outFile.close();

    system( ( "conda run -n cuda python " + baseDir + "/python/tsne0_cuda.py " + projDir ).c_str() );

    projX.resize( N_HISTS );
    projY.resize( N_HISTS );

    std::ifstream inFile(  projDir + "projection.0.bin", std::ios::in | std::ios::binary );
    inFile.read( (char*) projX.data(), N_HISTS * sizeof( float ) );
    inFile.read( (char*) projY.data(), N_HISTS * sizeof( float ) );
    inFile.close();
}

//optimize this later
 
template< typename T >
inline void computeCellHistograms2D(
    
    // inputs

    const TN::Vec2< int32_t >    & histDims,
    const TN::Vec2< float   >    & xR,
    const TN::Vec2< float   >    & yR,
    const std::vector< float   > & x, 
    const std::vector< float   > & y,
    const std::vector< T > & cellIds,
    const int64_t NC,

    //outputs

    DecomposedJointStatistics & stats )
{

    stats.histDims = histDims;
    std::vector< float >     & hist2ds     = stats.hist2ds;
    std::vector< float >     & counts      = stats.counts; 
    std::vector< float >     & outliersL   = stats.outliersL; 
    std::vector< float >     & outliersR   = stats.outliersR; 
    std::vector< float >     & outliersT   = stats.outliersT;
    std::vector< float >     & outliersB   = stats.outliersB;
    std::vector< float >     & outliersLT  = stats.outliersLT;
    std::vector< float >     & outliersLB  = stats.outliersLB; 
    std::vector< float >     & outliersRT  = stats.outliersRT; 
    std::vector< float >     & outliersRB  = stats.outliersRB;  
    std::vector< Moments2D > & sampleMoments    = stats.sampleMoments; 
    std::vector< Moments2D > & estimatedMoments = stats.estimatedMoments;

    const int64_t HX = histDims.x();
    const int64_t HY = histDims.y();    
    const int64_t HN = HX*HY;

    hist2ds    = std::vector< float >( NC * HN, 0.f );
    counts     = std::vector< float >( NC, 0.f );
    outliersL  = std::vector< float >( NC, 0.f );
    outliersR  = std::vector< float >( NC, 0.f );
    outliersT  = std::vector< float >( NC, 0.f );
    outliersB  = std::vector< float >( NC, 0.f );
    outliersLT = std::vector< float >( NC, 0.f );
    outliersLB = std::vector< float >( NC, 0.f );
    outliersRT = std::vector< float >( NC, 0.f );
    outliersRB = std::vector< float >( NC, 0.f );

    const float xMN = xR.a();
    const float XW  = xR.b() - xR.a();

    const float yMN = yR.a();
    const float YW  = yR.b() - yR.a();

    // moments

    sampleMoments.resize(    NC );
    estimatedMoments.resize( NC );

    for( int cell = 0; cell < NC; ++cell )
    {
        sampleMoments[ cell ].x.mean = 0.0;
        sampleMoments[ cell ].y.mean = 0.0;
    }

    //

    int32_t minCell = 999999;
    int32_t maxCell = -9;    

    const int64_t NV = x.size();
    for( int64_t idx = 0; idx < NV; ++idx ) {
        const int32_t cell = cellIds[ idx ];    
        if( cell >= NC || cell < 0 ) {
            continue;
        }

        minCell = std::min( minCell,  cell );
        maxCell = std::max( maxCell, cell );

        const int64_t hoffset = cell * HN;

        int64_t xBin = std::floor( ( ( x[ idx ] - xMN ) / XW ) * HX );
        int64_t yBin = std::floor( ( ( y[ idx ] - yMN ) / YW ) * HY );

        sampleMoments[ cell ].x.mean += x[ idx ];
        sampleMoments[ cell ].y.mean += y[ idx ];

        if( xBin >= 0 && xBin < HX 
         && yBin >= 0 && yBin < HY ) {
            int32_t flatBinIdx 
                = flatIndex2dCM( xBin, yBin, HX, HY );

            hist2ds[ hoffset + flatBinIdx ] += 1.f;
            counts[  cell ] += 1.f;
        }
        else {
            if( xBin < 0 && yBin < 0)
            {
                outliersLB[ cell ] += 1.f;
            }
            else if( xBin < 0 && yBin >= HY )
            {
                outliersLT[ cell ] += 1.f; 
            }
            else if( xBin >= HX && yBin < 0 )
            {
                outliersRB[ cell ] += 1.f;
            }
            else if( xBin >= HX && yBin >= HY )
            {
                outliersRT[ cell ] += 1.f;
            }
            else if( xBin < 0 )
            {
                outliersL[ cell ] += 1.f;
            }
            else if( xBin >= HX )
            {
                outliersR[ cell ] += 1.f;
            }
            else if( xBin < 0 )
            {
                outliersB[ cell ] += 1.f; 
            }
            else if( xBin >= HX )
            {
                outliersT[ cell ] += 1.f;
            }
        }
    }

    std::cout << "done binning" << std::endl;

    /************** finish computing moments ***********/

    std::cout << "NC=" << NC << std::endl;

    std::cout << "minCell " << minCell << " maxCell " << maxCell << std::endl; 

    std::vector< float > m2x( NC, 0.f );
    std::vector< float > m3x( NC, 0.f );
    std::vector< float > m4x( NC, 0.f );

    std::vector< float > m2y( NC, 0.f );
    std::vector< float > m3y( NC, 0.f );
    std::vector< float > m4y( NC, 0.f ); 

    for( int cell = 0; cell < NC; ++cell )
    {
        if( cell >= NC || cell < 0 )
        {
            // std::cout << "cell " << cell << " out of range " << NC << std::endl;
            continue;
        }

        if( counts[ cell ] > 0.f )
        {
            sampleMoments[ cell ].x.mean /= counts[ cell ];
            sampleMoments[ cell ].y.mean /= counts[ cell ];
        }

        sampleMoments[ cell ].x.variance = 0.0;
        sampleMoments[ cell ].x.skewness = 0.0;
        sampleMoments[ cell ].x.kurtosis = 0.0;

        sampleMoments[ cell ].y.variance = 0.0;
        sampleMoments[ cell ].y.skewness = 0.0;
        sampleMoments[ cell ].y.kurtosis = 0.0;

        sampleMoments[ cell ].coVariance = 0.0;
        sampleMoments[ cell ].coSkewness = 0.0;
        sampleMoments[ cell ].coKurtosis = 0.0;

        // need these temporary per cell

        m2x[ cell ] = 0.0;
        m3x[ cell ] = 0.0;
        m4x[ cell ] = 0.0;      

        m2y[ cell ] = 0.0;
        m3y[ cell ] = 0.0;
        m4y[ cell ] = 0.0;
    }

    std::cout << "normalize" << std::endl;

    for( int64_t idx = 0; idx < NV; ++idx )
    {
        const int32_t cell = cellIds[ idx ]; 

        if( cell >= NC || cell < 0 )
        {
            // std::cout << "cell " << cell << " out of range " << NC << std::endl;
            continue;
        }

        double diffX = x[ idx ] - sampleMoments[ cell ].x.mean;
        double diffY = y[ idx ] - sampleMoments[ cell ].y.mean;

        sampleMoments[ cell ].coVariance += diffX * diffY;
        sampleMoments[ cell ].coSkewness += diffX * diffX * diffY;
        sampleMoments[ cell ].coKurtosis += diffX * diffX * diffY * diffY;

        m2x[ cell ] += std::pow( diffX, 2.f );
        m3x[ cell ] += std::pow( diffX, 3.f );
        m4x[ cell ] += std::pow( diffX, 4.f );

        m2y[ cell ] += std::pow( diffY, 2.f );
        m3y[ cell ] += std::pow( diffY, 3.f );
        m4y[ cell ] += std::pow( diffY, 4.f );
    }

    // std::cout << "variance done" << std::endl;

    for( int cell = 0; cell < NC; ++cell )
    {
        if( cell >= NC || cell < 0 )
        {
            // std::cout << "cell " << cell << " out of range " << NC << std::endl;
            continue;
        }

        if( counts[ cell ] > 1 )
        {
            m2x[ cell ] /= counts[ cell ];
            m3x[ cell ] /= counts[ cell ];
            m4x[ cell ] /= counts[ cell ];

            m2y[ cell ] /= counts[ cell ];
            m3y[ cell ] /= counts[ cell ];
            m4y[ cell ] /= counts[ cell ];

            sampleMoments[ cell ].x.variance = m2x[ cell ];
            double sdevX = std::sqrt( sampleMoments[ cell ].x.variance );
            sampleMoments[ cell ].x.skewness = m3x[ cell ] 
                / std::pow( sdevX, 3.0 ); 
            sampleMoments[ cell ].x.kurtosis = m4x[ cell ] 
                / std::pow( m2x[ cell ], 2.0 ) - 3.0; 

            sampleMoments[ cell ].y.variance = m2y[ cell ];
            double sdevY = std::sqrt( sampleMoments[ cell ].y.variance );
            sampleMoments[ cell ].y.skewness = m3y[ cell ] 
                / std::pow( sdevY, 3.0 ); 
            sampleMoments[ cell ].y.kurtosis = m4y[ cell ] 
                / std::pow( m2y[ cell ], 2.0 ) - 3.0; 

            sampleMoments[ cell ].coVariance /= counts[ cell ];
            sampleMoments[ cell ].coSkewness /= counts[ cell ] 
                * sampleMoments[ cell ].x.variance * sdevY;
            sampleMoments[ cell ].coKurtosis /= counts[ cell ] 
                * sampleMoments[ cell ].x.variance * sampleMoments[ cell ].y.variance;
        }
    }

 //    std::cout << "moments complete" << std::endl;    

    /******************* estimated ********************/

    std::vector< float > xHist( HX );
    std::vector< float > yHist( HY );

    for( int cell = 0; cell < NC; ++cell )
    {
        if( cell >= NC || cell < 0 )
        {
            // std::cout << "cell " << cell << " out of range " << NC << std::endl;
            continue;
        }

        std::fill( xHist.begin(), xHist.end(), 0.f );
        std::fill( yHist.begin(), yHist.end(), 0.f );

        int64_t cell_offset = cell * HN;

        if( counts[ cell ] > 1 )
        {
            for( int x = 0; x < HX; ++x )
            for( int y = 0; y < HY; ++y )
            {
                int index = cell_offset + flatIndex2dCM( x, y, HX, HY );

                hist2ds[ index ] /= counts[ cell ];
                xHist[ x ] += hist2ds[ index ];
                yHist[ y ] += hist2ds[ index ];         
            } 

            estimateMoments(
                hist2ds.data() + cell_offset,
                xHist.data(),
                yHist.data(),
                histDims,
                xR,
                yR,
                estimatedMoments[ cell ] );
        }
        else
        {
            estimatedMoments[ cell ] = sampleMoments[ cell ];
        }
    }   

    std::cout << "done computing id dependent stats" << std::endl;
}

inline void computeCellHistograms2D(
    
    // inputs

    const TN::Vec2< int32_t >    & histDims,
    const TN::Vec2< float   >    & xR,
    const TN::Vec2< float   >    & yR,
    const std::vector< float   > & x, 
    const std::vector< float   > & y,
    const int64_t NC,

    //outputs

    DecomposedJointStatistics & stats )
{
    stats.histDims = histDims;
    std::vector< float >     & hist2ds     = stats.hist2ds;
    std::vector< float >     & counts      = stats.counts; 
    std::vector< float >     & outliersL   = stats.outliersL; 
    std::vector< float >     & outliersR   = stats.outliersR; 
    std::vector< float >     & outliersT   = stats.outliersT;
    std::vector< float >     & outliersB   = stats.outliersB;
    std::vector< float >     & outliersLT  = stats.outliersLT;
    std::vector< float >     & outliersLB  = stats.outliersLB; 
    std::vector< float >     & outliersRT  = stats.outliersRT; 
    std::vector< float >     & outliersRB  = stats.outliersRB;  
    std::vector< Moments2D > & sampleMoments    = stats.sampleMoments; 
    std::vector< Moments2D > & estimatedMoments = stats.estimatedMoments;

    const int64_t HX = histDims.x();
    const int64_t HY = histDims.y();    
    const int64_t HN = HX*HY;

    hist2ds    = std::vector< float >( NC * HN, 0.f );
    counts     = std::vector< float >( NC, 0.f );
    outliersL  = std::vector< float >( NC, 0.f );
    outliersR  = std::vector< float >( NC, 0.f );
    outliersT  = std::vector< float >( NC, 0.f );
    outliersB  = std::vector< float >( NC, 0.f );
    outliersLT = std::vector< float >( NC, 0.f );
    outliersLB = std::vector< float >( NC, 0.f );
    outliersRT = std::vector< float >( NC, 0.f );
    outliersRB = std::vector< float >( NC, 0.f );

    const float xMN = xR.a();
    const float XW  = xR.b() - xR.a();

    const float yMN = yR.a();
    const float YW  = yR.b() - yR.a();

    // moments

    sampleMoments.resize(    NC );
    estimatedMoments.resize( NC );

    for( int cell = 0; cell < NC; ++cell )
    {
        sampleMoments[ cell ].x.mean = 0.0;
        sampleMoments[ cell ].y.mean = 0.0;
    }

    //

    std::cout << "binning" << std::endl;

    int32_t minCell = 999999;
    int32_t maxCell = -9;    

    const int64_t NV = x.size();
    for( int64_t idx = 0; idx < NV; ++idx )
    {
        const int32_t cell = 0;    
     
        if( cell >= NC || cell < 0 )
        {
            // std::cout << "cell " << cell << " out of range " << NC << std::endl;
            continue;
        }

        minCell = std::min( minCell,  cell );
        maxCell = std::max( maxCell, cell );

        const int64_t hoffset = cell * HN;

        int64_t xBin = std::floor( ( ( x[ idx ] - xMN ) / XW ) * HX );
        int64_t yBin = std::floor( ( ( y[ idx ] - yMN ) / YW ) * HY );

        sampleMoments[ cell ].x.mean += x[ idx ];
        sampleMoments[ cell ].y.mean += y[ idx ];

        if( xBin >= 0 && xBin < HX 
         && yBin >= 0 && yBin < HY )
        {
            int32_t flatBinIdx 
                = flatIndex2dCM( xBin, yBin, HX, HY );

            hist2ds[ hoffset + flatBinIdx ] += 1.f;
            counts[  cell ] += 1.f;
        }
        else
        {
            if( xBin < 0 && yBin < 0)
            {
                outliersLB[ cell ] += 1.f;
            }
            else if( xBin < 0 && yBin >= HY )
            {
                outliersLT[ cell ] += 1.f; 
            }
            else if( xBin >= HX && yBin < 0 )
            {
                outliersRB[ cell ] += 1.f;
            }
            else if( xBin >= HX && yBin >= HY )
            {
                outliersRT[ cell ] += 1.f;
            }
            else if( xBin < 0 )
            {
                outliersL[ cell ] += 1.f;
            }
            else if( xBin >= HX )
            {
                outliersR[ cell ] += 1.f;
            }
            else if( xBin < 0 )
            {
                outliersB[ cell ] += 1.f; 
            }
            else if( xBin >= HX )
            {
                outliersT[ cell ] += 1.f;
            }
        }
    }

    std::cout << "done binning" << std::endl;

    /************** finish computing moments ***********/

    std::cout << "NC=" << NC << std::endl;

    std::cout << "minCell " << minCell << " maxCell " << maxCell << std::endl; 

    std::vector< float > m2x( NC, 0.f );
    std::vector< float > m3x( NC, 0.f );
    std::vector< float > m4x( NC, 0.f );

    std::vector< float > m2y( NC, 0.f );
    std::vector< float > m3y( NC, 0.f );
    std::vector< float > m4y( NC, 0.f ); 

    for( int cell = 0; cell < NC; ++cell )
    {
        if( cell >= NC || cell < 0 )
        {
            // std::cout << "cell " << cell << " out of range " << NC << std::endl;
            continue;
        }

        if( counts[ cell ] > 0.f )
        {
            sampleMoments[ cell ].x.mean /= counts[ cell ];
            sampleMoments[ cell ].y.mean /= counts[ cell ];
        }

        sampleMoments[ cell ].x.variance = 0.0;
        sampleMoments[ cell ].x.skewness = 0.0;
        sampleMoments[ cell ].x.kurtosis = 0.0;

        sampleMoments[ cell ].y.variance = 0.0;
        sampleMoments[ cell ].y.skewness = 0.0;
        sampleMoments[ cell ].y.kurtosis = 0.0;

        sampleMoments[ cell ].coVariance = 0.0;
        sampleMoments[ cell ].coSkewness = 0.0;
        sampleMoments[ cell ].coKurtosis = 0.0;

        // need these temporary per cell

        m2x[ cell ] = 0.0;
        m3x[ cell ] = 0.0;
        m4x[ cell ] = 0.0;      

        m2y[ cell ] = 0.0;
        m3y[ cell ] = 0.0;
        m4y[ cell ] = 0.0;
    }

    std::cout << "normalize" << std::endl;

    for( int64_t idx = 0; idx < NV; ++idx )
    {
        const int32_t cell = 0; 

        if( cell >= NC || cell < 0 )
        {
            // std::cout << "cell " << cell << " out of range " << NC << std::endl;
            continue;
        }

        double diffX = x[ idx ] - sampleMoments[ cell ].x.mean;
        double diffY = y[ idx ] - sampleMoments[ cell ].y.mean;

        sampleMoments[ cell ].coVariance += diffX * diffY;
        sampleMoments[ cell ].coSkewness += diffX * diffX * diffY;
        sampleMoments[ cell ].coKurtosis += diffX * diffX * diffY * diffY;

        m2x[ cell ] += std::pow( diffX, 2.f );
        m3x[ cell ] += std::pow( diffX, 3.f );
        m4x[ cell ] += std::pow( diffX, 4.f );

        m2y[ cell ] += std::pow( diffY, 2.f );
        m3y[ cell ] += std::pow( diffY, 3.f );
        m4y[ cell ] += std::pow( diffY, 4.f );
    }

    // std::cout << "variance done" << std::endl;

    for( int cell = 0; cell < NC; ++cell )
    {
        if( cell >= NC || cell < 0 )
        {
            // std::cout << "cell " << cell << " out of range " << NC << std::endl;
            continue;
        }

        if( counts[ cell ] > 1 )
        {
            m2x[ cell ] /= counts[ cell ];
            m3x[ cell ] /= counts[ cell ];
            m4x[ cell ] /= counts[ cell ];

            m2y[ cell ] /= counts[ cell ];
            m3y[ cell ] /= counts[ cell ];
            m4y[ cell ] /= counts[ cell ];

            sampleMoments[ cell ].x.variance = m2x[ cell ];
            double sdevX = std::sqrt( sampleMoments[ cell ].x.variance );
            sampleMoments[ cell ].x.skewness = m3x[ cell ] 
                / std::pow( sdevX, 3.0 ); 
            sampleMoments[ cell ].x.kurtosis = m4x[ cell ] 
                / std::pow( m2x[ cell ], 2.0 ) - 3.0; 

            sampleMoments[ cell ].y.variance = m2y[ cell ];
            double sdevY = std::sqrt( sampleMoments[ cell ].y.variance );
            sampleMoments[ cell ].y.skewness = m3y[ cell ] 
                / std::pow( sdevY, 3.0 ); 
            sampleMoments[ cell ].y.kurtosis = m4y[ cell ] 
                / std::pow( m2y[ cell ], 2.0 ) - 3.0; 

            sampleMoments[ cell ].coVariance /= counts[ cell ];
            sampleMoments[ cell ].coSkewness /= counts[ cell ] 
                * sampleMoments[ cell ].x.variance * sdevY;
            sampleMoments[ cell ].coKurtosis /= counts[ cell ] 
                * sampleMoments[ cell ].x.variance * sampleMoments[ cell ].y.variance;
        }
    }

 //    std::cout << "moments complete" << std::endl;    

    /******************* estimated ********************/

    std::vector< float > xHist( HX );
    std::vector< float > yHist( HY );

    for( int cell = 0; cell < NC; ++cell )
    {
        if( cell >= NC || cell < 0 )
        {
            // std::cout << "cell " << cell << " out of range " << NC << std::endl;
            continue;
        }

        std::fill( xHist.begin(), xHist.end(), 0.f );
        std::fill( yHist.begin(), yHist.end(), 0.f );

        int64_t cell_offset = cell * HN;

        if( counts[ cell ] > 1 )
        {
            for( int x = 0; x < HX; ++x )
            for( int y = 0; y < HY; ++y )
            {
                int index = cell_offset + flatIndex2dCM( x, y, HX, HY );

                hist2ds[ index ] /= counts[ cell ];
                xHist[ x ] += hist2ds[ index ];
                yHist[ y ] += hist2ds[ index ];         
            } 

            estimateMoments(
                hist2ds.data() + cell_offset,
                xHist.data(),
                yHist.data(),
                histDims,
                xR,
                yR,
                estimatedMoments[ cell ] );
        }
        else
        {
            estimatedMoments[ cell ] = sampleMoments[ cell ];
        }
    }   

    std::cout << "done computing no id stats" << std::endl;
}

}
}


#endif