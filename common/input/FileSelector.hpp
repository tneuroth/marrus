#ifndef TN_FILE_SELECTOR_HPP
#define TN_FILE_SELECTOR_HPP

#include <string>

namespace TN {
	
class FileSelector 
{

	public:

	virtual std::string getFile( 
		const std::string & startingPath ) = 0;

	virtual std::string getFile( 
		const std::string & startingPath, const std::string & extention ) = 0;

	virtual ~FileSelector() {}
};

}

#endif