#ifndef TN_DATA_FIELD_HPP
#define TN_DATA_FIELD_HPP

#include <string>

namespace TN {

enum class DataSource
{ 
    Disk,             // data is on disk 
    CompiledFunction, // computed using a built in function (hard coded in data manager, or plugin)
    MetaFunction      // user dynamically generates using a script (eventually DIVA?)
};

struct DataField
{
    DataSource dataSource;

    // universal parameters
 
	std::string key;
    double conversionFactor;
    std::string latex;
    std::string description;

    // parameters used only when it's on Disk

    std::string filePath;
    size_t byteOffset;
    size_t index;

    // parameters used only when it's a compiled function

    std::string func;

    // parameters used only when it's user generated 

    std::string script;
};

// abstraction layer to couple data field with it's state in a data manager
struct ManagedDataField
{
    TN::DataField dataField;
    bool persistent;
};


}

#endif