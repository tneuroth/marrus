#ifndef TN_SIMPLE_DATA_MANAGER_HPP
#define TN_SIMPLE_DATA_MANAGER_HPP

#include "geometry/Vec.hpp"
#include "algorithms/IsoContour/CubeMarcher.hpp"
#include "algorithms/Standard/MyAlgorithms.hpp"
#include "geometry/TrilinearInterpolator.hpp"
#include "geometry/SurfaceGenerator.hpp"
#include "latex/runpdflatex.hpp"
#include "latex/LatexTableGenerator.hpp"
#include "render/Texture/Texture.hpp"
#include "geometry/SurfaceGenerator.hpp"
#include "utils/StrUtils.hpp"

// #include <boost/filesystem.hpp>

#include <sstream>
#include <memory>
#include <unordered_map>
#include <map>
#include <set>
#include <string>
#include <fstream>
#include <type_traits>
#include <limits>
#include <cmath>
#include <atomic>

// #include <filesystem>

namespace TN {

static inline void getSpatialScaling(
    const std::string & pathX,
    const std::string & pathY,
    const std::string & pathZ,    
    std::vector< float > & x,
    std::vector< float > & y,
    std::vector< float > & z,
    const TN::Vec3< int > & volumeDims )
{
    x.resize( volumeDims.x() );
    y.resize( volumeDims.y() );
    z.resize( volumeDims.z() );

    std::ifstream inFile( pathX );
    inFile.read( (char*) x.data(), x.size() * sizeof( float ) );
    inFile.close();

    inFile.open( pathY );
    inFile.read( (char*) y.data(), y.size() * sizeof( float ) );
    inFile.close();
    
    inFile.open( pathZ );        
    inFile.read( (char*) z.data(), z.size() * sizeof( float ) );
    inFile.close();
}

template < typename T >
struct TypeTag {
    static T tag() { return T(); }
};

struct SimpleDerivedVariableInfo
{
    std::string name;
    std::string expression;
    std::string latex;
};

struct SimpleConstantInfo
{
    std::string name;
    double value;
    std::string latex;
};

struct SimpleDerivedConstantInfo
{
    std::string name;
    std::string expression;
    std::string latex;
};

struct SimpleVariableInfo {
    std::string name;
    std::string filePath;
    size_t offset;    
    double normalizationFactor;
    std::string type;
    std::string latex;
};

struct SimpleDataInfoSet
{
    std::map< std::string, long double  > constants;
    std::map< std::string, std::string  > derivedConstants;
    std::set< std::string > variables;
    std::map< std::string, std::string  > derivedVariables;
};

inline std::string formatValue(double value) {
    std::stringstream ss;
    
    // For very small numbers or very large numbers, use scientific notation
    if (std::abs(value) < 0.01 || std::abs(value) >= 10000.0) {
        ss << std::scientific << std::setprecision(2) << value;
        // Convert e-00N to e-N and e+00N to eN for cleaner look
        std::string str = ss.str();
        size_t e_pos = str.find('e');
        if (e_pos != std::string::npos) {
            // Remove leading plus sign after 'e' if present
            if (str[e_pos + 1] == '+') {
                str.erase(e_pos + 1, 1);
            }
            // Remove leading zeros in exponent
            size_t exp_start = e_pos + 1;
            if (str[exp_start] == '-') exp_start++;
            while (exp_start < str.length() - 1 && str[exp_start] == '0') {
                str.erase(exp_start, 1);
            }
            return str;
        }
        return ss.str();
    } 
    // For numbers close to integers, don't show unnecessary decimals
    else if (std::abs(value - std::round(value)) < 1e-10) {
        ss << std::fixed << std::setprecision(0) << value;
        return ss.str();
    }
    // For regular numbers, use fixed notation with appropriate precision
    else {
        // Determine number of decimal places needed (max 4)
        int precision = 2;
        double testVal = std::abs(value);
        if (testVal < 1.0) precision = 3;
        if (testVal < 0.1) precision = 4;
        
        ss << std::fixed << std::setprecision(precision) << value;
        return ss.str();
    }
}

template< class CONVERTED_TYPE >
class SimpleDataManager
{
    std::map< std::string, SimpleVariableInfo > variableInfo;
    std::map< std::string, std::unique_ptr< std::vector< CONVERTED_TYPE > > > data;  
    std::map< std::string, TN::Vec2< CONVERTED_TYPE > > ranges;

    TN::Vec3< int > volumeDims;

    std::vector< float > xVolumeCoordinates;
    std::vector< float > yVolumeCoordinates;
    std::vector< float > zVolumeCoordinates;

public:

    TN::Vec3< int > getVolumeDims() const { return volumeDims; }

    SimpleDataManager( SimpleDataManager && ) = default;
    SimpleDataManager()  {}
    ~SimpleDataManager() {}

    void init(
        const TN::Vec3< int > dims,        
        const std::array< std::string, 3 > coordiateFilePaths,
        const std::map< std::string, SimpleVariableInfo > & varInfo,
        const std::string projectDir,
        const std::string baseDir,        
        const std::string pdflatexDir,
        const std::string pdftocairoDir,
        const std::string convertDir,
        std::atomic< int > & status )
    {
        status = 0;

        volumeDims = dims;
        variableInfo = varInfo;

        if( coordiateFilePaths[ 0 ] != "" 
         && coordiateFilePaths[ 1 ] != ""
         && coordiateFilePaths[ 2 ] != "" ) 
        {
            getSpatialScaling(
                coordiateFilePaths[ 0 ],
                coordiateFilePaths[ 1 ] ,
                coordiateFilePaths[ 2 ],
                xVolumeCoordinates,
                yVolumeCoordinates,            
                zVolumeCoordinates,
                volumeDims );
        }
        else 
        {
            xVolumeCoordinates.resize( dims.x() );
            yVolumeCoordinates.resize( dims.y() );       
            zVolumeCoordinates.resize( dims.z() );
            for( int i = 0; i < dims.x(); ++i ) { xVolumeCoordinates[ i ] = (float) i; }
            for( int i = 0; i < dims.y(); ++i ) { yVolumeCoordinates[ i ] = (float) i; }
            for( int i = 0; i < dims.z(); ++i ) { zVolumeCoordinates[ i ] = (float) i; }        
        }

        status = 1;

        std::cout << "stage 1" << std::endl;

        for( auto & v : variableInfo )
        {
            data.insert( { v.first, nullptr } );
            ranges.insert( { v.first, TN::Vec2< CONVERTED_TYPE >() } );
        }

        std::cout << "stage 2" << std::endl;

        const std::string dir = projectDir;
 
        status = 2;

        std::cout << "stage 3 gen symbols" << std::endl;

        for( auto v : variableInfo )
        {
            std::cout << "compiling: " << v.first << std::endl;

            TN::generateEquationPNG(
                baseDir + "/../common/latex/templates/cropped_equation_template.tex",
                v.second.latex,
                dir + "/latex_symbols/",
                v.first,
                pdflatexDir,
                pdftocairoDir,
                convertDir,
                true, // also generate a rotated / vertical version
                true  // only regenerate the png if an identical version doesn't already exist
            );
        }
    }

    template< typename RAW_TYPE >
    void loadVariable( 
        const std::string & variable,
        RAW_TYPE tag )
    {
        std::cout << "loading " << variable << std::endl;

        const size_t SZ = volumeDims.x() * volumeDims.y() * volumeDims.z();
        data.at( variable ).reset( new std::vector< CONVERTED_TYPE >( SZ ) );
        std::vector< CONVERTED_TYPE > & storage = *data.at( variable );

        bool invert = false;

        // is it a variable in the file, or a derived one?
        if( variableInfo.count( variable ) )
        {
            const SimpleVariableInfo & varInfo = variableInfo.at( variable );
            std::ifstream inFile( varInfo.filePath );

            if( ! inFile.is_open() )
            {
                std::cerr << "failed to load file: " << varInfo.filePath << std::endl;
                exit( 0 );
                // should be an exception
            }

            const RAW_TYPE SCALE_FACTOR = varInfo.normalizationFactor;

            if( std::is_same< RAW_TYPE, CONVERTED_TYPE >::value )
            {
                inFile.seekg( SZ * varInfo.offset * sizeof( RAW_TYPE ) );
                inFile.read( (char *) storage.data(), SZ * sizeof( RAW_TYPE ) );
            
                if( SCALE_FACTOR != 1.0 )
                {
                    #pragma omp parallel for
                    for( size_t i = 0; i < SZ; ++i )
                    {
                        storage[ i ] *= SCALE_FACTOR;
                    }
                }
            }
            else
            {
                std::vector< RAW_TYPE > swap( SZ ); 
                inFile.seekg( SZ * varInfo.offset * sizeof( RAW_TYPE ) );
                inFile.read( (char *) swap.data(), SZ * sizeof( RAW_TYPE ) );                                
            
                std::cout << "converting type " << std::endl;

                #pragma omp parallel for
                for( size_t i = 0; i < SZ; ++i )
                {
                    storage[ i ] =  swap[ i ] * SCALE_FACTOR;
                }
            }

            inFile.close();
        }
        else
        {
            // should be an exception
            std::cout << "something wrong" << std::endl;
            exit( 0 );
        }

        ranges.at( variable ) = TN::Sequential::getRange( storage );

        const double width = ranges.at( variable ).b() - ranges.at( variable ).a();
        const double mn    = ranges.at( variable ).a();
        const double mx    = ranges.at( variable ).b();

        std::cout << variable << "range = " << mn << ", " << mx << std::endl;
        std::cout << variable << "range width = " << width << std::endl;

        if( width > 0 )
        {
            #pragma omp parallel for
            for( size_t i = 0; i < SZ; ++i )
            {
                storage[ i ] = ( storage[ i ] - mn ) / width;
            }
        }

        if( invert )
        {
            std::cout << "inverting" << std::endl;
            #pragma omp parallel for
            for( size_t i = 0; i < SZ; ++i )
            {
                storage[ i ] = 1.0 - storage[ i ];
            }
        }

        std::cout << "done loading" << std::endl;
    }

    std::vector< CONVERTED_TYPE > * get( const std::string & variable )
    {
        if( data.count( variable ) == 0 )
        {
            return nullptr;
        }
        else if( data.at( variable ) == nullptr )
        {
            if( variableInfo.at( variable ).type == "float" ) {
                loadVariable( variable, TypeTag<float>::tag() );     
            }
            else if( variableInfo.at( variable ).type == "double" ) {
                loadVariable( variable, TypeTag<double>::tag() );    
            }
            else {
                // error ...
            }   
        }
        else 
        {
            //std::cout << "already stored" << std::endl;
        }

        return data.at( variable ).get();
    }

    double getNormalizationFactor( const std::string & var ) const
    {
        if( variableInfo.count( var ) == 0 )
        {
            std::cout << "error tried accessing info for " << var << " which was not found" << std::endl;
            exit( 0 );
        }
        return variableInfo.at( var ).normalizationFactor;
    }

    TN::Vec2< CONVERTED_TYPE > getRange( const std::string & var ) const
    {
        if( ranges.count( var ) == 0 )
        {
            std::cout << "error tried accessing range for " << var << " which was not found" << std::endl;
            exit( 0 );
        }
        return ranges.at( var );
    }

    void free( const std::string & variable )
    {
        if( data.count( variable ) != 0 )
        {
            data.at( variable ) = nullptr;
        }
    }

    std::string label( const std::string & key ) const 
    {
        std::cout << "getting label from key " << key << std::endl;

        if( variableInfo.count( key ) )
        {
            return variableInfo.at( key ).latex;
        }
        else
        {
            return "";
        }
    }

    std::pair< TN::Vec3< float >, TN::Vec3< float > > extent() const
    {
        return { 
            { xVolumeCoordinates.front(), yVolumeCoordinates.front(), zVolumeCoordinates.front() },
            { xVolumeCoordinates.back(),  yVolumeCoordinates.back(),  zVolumeCoordinates.back()  } };
    }

    void getSurface(
        double isovalue,
        const std::vector< float > & d,
        std::vector< TN::Vec3< float >  > & verts,
        std::vector< TN::Vec3< float >  > & normals )
    {
        // std::cout << "computing isosurface " << d.size() << std::endl;
        // std::cout << "dims=" << volumeDims.x() << ", " << volumeDims.y() << ", " << volumeDims.z() << std::endl;
        // std::cout << "isovalue=" << isovalue << std::endl;
        
        // std::cout << xVolumeCoordinates.size() << " "
        //           << yVolumeCoordinates.size() << " "
        //           << zVolumeCoordinates.size() << std::endl;

        auto datR = TN::Sequential::getRange( d );

        computeIsosurface( 
            isovalue,    
            d, 
            volumeDims, 
            xVolumeCoordinates,
            yVolumeCoordinates,
            zVolumeCoordinates,
            verts, 
            normals );
    }

    void getSurface(
        double isovalue,
        const std::string & varKey,
        std::vector< TN::Vec3< float >  > & verts,
        std::vector< TN::Vec3< float >  > & normals )
    {
        getSurface( 
            trueValueToScaledValue( isovalue, varKey ),    
            *get( varKey ), 
            verts, 
            normals );
    }

    void interpolateToSurface(
        const std::vector< float > & d,
        const std::vector< TN::Vec3< float >  > & verts,
        std::vector< float > & result )
    {
        const double NORM = std::max( std::max( volumeDims.x(), volumeDims.y() ), volumeDims.z() );

        triInt( 
            d, 
            volumeDims.x(),
            volumeDims.y(),
            volumeDims.z(),
            extent(),
            verts,
            result );
    }

    void interpolateToSurface(
        const std::string & varKey,
        const std::vector< TN::Vec3< float >  > & verts,
        std::vector< float > & result )
    {
        interpolateToSurface( *get( varKey ), verts, result );
    }

    double scaleFactor( const std::string & key ) const
    {
        if( variableInfo.count( key ) == 0 )
        {
            return 1.0;
        }

        return variableInfo.at( key ).normalizationFactor;
    }

    inline float scaledValueToTrueValue( float v, const TN::Vec2< float > & rng )
    {
        return v * ( rng.b() - rng.a() ) + rng.a();
    }

    float scaledValueToTrueValue( float v, const std::string & varKey )
    {
        const auto & rng  = ranges.at( varKey );
        return scaledValueToTrueValue( v, rng );
    }

    TN::Vec2< float > scaledValuesToTrueValues( const TN::Vec2< float > & v, const std::string & varKey, double factor=1.0 )
    {
        const auto & rng  = ranges.at( varKey );
        return 
        {
            v.a() * ( rng.b() - rng.a() ) + rng.a(),
            v.b() * ( rng.b() - rng.a() ) + rng.a()
        };
    }

    float trueValueToScaledValue( float v, const std::string & varKey )
    {
        const auto rng = ranges.at( varKey );
        return ( ( v - rng.a() ) / ( rng.b() - rng.a() ) );
    }

    const std::vector< float > & xCoords() const
    {
        return xVolumeCoordinates;
    }

    const std::vector< float > & yCoords() const
    {
        return yVolumeCoordinates;
    }

    const std::vector< float > & zCoords() const
    {
        return zVolumeCoordinates;
    }

    std::map< std::string, std::pair< std::string, TN::Vec2< double > > > getVariableInformation() const {
        std::map< std::string, std::pair< std::string, TN::Vec2< double > > > vars;
        for( const auto & ve : variableInfo ) 
        {   const auto & v = ve.second;
            vars.insert( { v.name, { v.latex, getRange( v.name ) } } );
        }
        return vars;
    }

    const std::string generateConditionalStatisticsTable( 
        const std::string & conditionVariable, 
        const std::vector< std::array<float,2 > > & bins,
        bool binValuesAreUnscaled ) 
    {
        std::vector<std::vector<std::string>> table;
        if ( data.count(conditionVariable) == 0 )
        {
            std::cerr << "Tried using undefined variable: " << conditionVariable << std::endl;
            exit(1);
        }

        const double BIN_SCALE_FACTOR = binValuesAreUnscaled ? variableInfo.at( conditionVariable ).normalizationFactor : 1.0;

        bool conditionVariableWasLoaded = data.at(conditionVariable) != nullptr;
        auto conditionalValues = *get(conditionVariable);

        // Create header row
        std::vector<std::string> header;
        header.push_back( "Bins Edges" ); // Empty cell for variable names
        
        const size_t N_BINS = bins.size();

        // Add headers for each bin
        for (size_t i = 0; i < N_BINS; ++i) {
            header.push_back( 
                "\\shortstack{" + 
                formatValue( bins[i][ 0 ] * BIN_SCALE_FACTOR ) +
                " \\\\ to \\\\" + 
                formatValue( bins[i][ 1 ] * BIN_SCALE_FACTOR ) +
                "}"
            );
        }

        header.push_back("All Data"); // Column for overall statistics
        table.push_back(header);

        // get indices for the bin elements //////////////////////////////////////////////////

        const size_t NVALS = conditionalValues.size();
        const auto & cv_range = getRange( conditionVariable );  

        std::vector< std::vector< size_t > > binElements( bins.size(), std::vector< size_t >() );   

        for (size_t i = 0; i < NVALS; ++i) 
        {
            const float cVal = scaledValueToTrueValue( conditionalValues[ i ], cv_range );
            for ( size_t binIdx = 0; binIdx <  N_BINS; ++binIdx ) 
            {
                if ( cVal >= bins[ binIdx ][ 0 ]*BIN_SCALE_FACTOR && cVal < bins[ binIdx ][ 1 ]*BIN_SCALE_FACTOR ) 
                {
                    binElements[ binIdx ].push_back( i );
                }
            }
        }

        //////////////////////////////////////////////////////////////////////////////////////

        std::vector<CONVERTED_TYPE> binValues;

        int count = 0;

        // Generate statistics for each variable
        for (const auto& ve : variableInfo)
        {
            if( ve.first == conditionVariable )
            {
                continue;
            }

            const auto& var = ve.second;
            bool dataWasLoaded = data.at(var.name) != nullptr;
            std::vector<CONVERTED_TYPE> values = *get(var.name);
            std::string varLatex = var.latex;

            std::string cleanLatex = varLatex;

            // Create rows for min, max, and mean for this variable
            std::vector<std::string> minRow, maxRow, meanRow;
            
            // Add variable name with subscript for each statistic
            minRow.push_back(  cleanLatex + ".\\textbf{min}"  );
            maxRow.push_back(  cleanLatex + ".\\textbf{max}"  );
            meanRow.push_back( cleanLatex + ".\\textbf{mean}" );

            const auto & v_range = getRange( var.name );

            // Calculate statistics for each bin
            for ( size_t binIdx = 0; binIdx <  N_BINS; ++binIdx ) 
            {
                const auto & indices = binElements[ binIdx ];
                const size_t N_ELEMS = indices.size();
                binValues.resize( N_ELEMS );

                #pragma omp parallel for
                for ( size_t eIdx = 0; eIdx < N_ELEMS; ++ eIdx ) 
                {
                    binValues[ eIdx ] = scaledValueToTrueValue( values[ indices[ eIdx ] ], v_range );
                }

                // Calculate statistics for this bin
                if ( ! binValues.empty() ) 
                {
                    auto [min, max] = std::minmax_element( binValues.begin(), binValues.end() );
                    double mean     = std::accumulate( binValues.begin(), binValues.end(), 0.0 ) / binValues.size();

                    minRow.push_back(  formatValue(*min) );
                    maxRow.push_back(  formatValue(*max) );
                    meanRow.push_back( formatValue(mean) );
                } 
                else {
                    // No data in this bin
                    minRow.push_back("--");
                    maxRow.push_back("--");
                    meanRow.push_back("--");
                }
            }

            // Calculate overall statistics

            auto overallRange = getRange(  var.name );
            double overallMean = 0.0;
            for( const auto & v : values )
            {
                overallMean += scaledValueToTrueValue( v, v_range );
            }

            overallMean /= values.size();

            minRow.push_back(  formatValue( overallRange.a() ) );
            maxRow.push_back(  formatValue( overallRange.b() ) );
            meanRow.push_back( formatValue( overallMean ) );

            // Add rows to table
            table.push_back(minRow);
            table.push_back(maxRow);
            table.push_back(meanRow);

            if (dataWasLoaded == false) {
                free(var.name);
            }
        }

        if( ! conditionVariableWasLoaded )
        {
            free( conditionVariable );
        }

        // Generate the LaTeX table
        return TN::generateLatexTable(
            "Conditional Statistics Over " + variableInfo.at( conditionVariable ).latex, // Use condition variable name as title
            table,
            true,  // Include row headers
            true,   // Include column headers
            3
        );
    }

    void clear()
    {
        variableInfo.clear();
        data.clear();  
        ranges.clear();
        xVolumeCoordinates.clear();
        yVolumeCoordinates.clear();
        zVolumeCoordinates.clear();
    }
};

}

#endif
