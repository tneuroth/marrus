#ifndef TN_JSON_UTILS_HPP
#define TN_JSON_UTILS_HPP

#include "geometry/Vec.hpp"

#include <jsonhpp/json.hpp>

#include <array>
#include <vector>
#include <cstdint>
#include <utility>
#include <string>
#include <iostream>
#include <set>

namespace TN {

namespace CF {

const bool OPTIONAL = true;
const bool REQUIRED = false;

const std::string WRONG_TYPE_MSG = "Error: Wrong type in LSRCVD configuration file for key ";

static inline
bool getValue( const nlohmann::json & js, 
	const std::string & key, 
	bool & val, 
	std::pair< bool, std::string > & status ) {
	if( ! js.is_boolean() ) {
		status.first = false;
		status.second += WRONG_TYPE_MSG + key + ", expected bool\n";
		return false;
	} else {
		val = js.get<bool>();
	}
	return true;
}


static inline
bool getValue( const nlohmann::json & js, 
	const std::string & key, 
	std::array< float, 4 > & val, 
	std::pair< bool, std::string > & status ) {
	if( ! js.is_array() ) {
		status.first = false;
		status.second += WRONG_TYPE_MSG + key + ", expected array< float, 3 >\n";
		return false;
	} else {	
		std::vector< float > arr = js.get< std::vector< float  > >();
		if( arr.size() != 4 ) {
			status.first = false;
			status.second += WRONG_TYPE_MSG + key + ", expected array< float, 3 >\n";
		}
		val = js.get<std::array< float, 4 > >();
	}
	return true;
}

static inline
bool getValue( const nlohmann::json & js, 
	const std::string & key, 
	std::array< float, 2 > & val, 
	std::pair< bool, std::string > & status ) {
	if( ! js.is_array() ) {
		status.first = false;
		status.second += WRONG_TYPE_MSG + key + ", expected array< float, 2 >\n";
		return false;
	} else {	
		std::vector< float > arr = js.get< std::vector< float  > >();
		if( arr.size() != 2 ) {
			status.first = false;
			status.second += WRONG_TYPE_MSG + key + ", expected array< float, 2 >\n";
		}
		val = js.get<std::array< float, 2 > >();
	}
	return true;
}

static inline
bool getValue( const nlohmann::json & js, 
	const std::string & key, 
	std::vector< int > & val, 
	std::pair< bool, std::string > & status ) {
	if( ! js.is_array() ) {
		status.first = false;
		status.second += WRONG_TYPE_MSG + key + ", expected array< integer, 3 >\n";
		return false;
	} else {	
		val = js.get< std::vector< int  > >();
	}
	return true;
}


static inline
bool getValue( const nlohmann::json & js, 
	const std::string & key, 
	std::array< int64_t, 3 > & val, 
	std::pair< bool, std::string > & status ) {
	if( ! js.is_array() ) {
		status.first = false;
		status.second += WRONG_TYPE_MSG + key + ", expected array< integer, 3 >\n";
		return false;
	} else {	
		std::vector< int64_t > arr = js.get< std::vector< int64_t  > >();
		if( arr.size() != 3 ) {
			status.first = false;
			status.second += WRONG_TYPE_MSG + key + ", expected array< integer, 3 >\n";
		}
		val = js.get<std::array< int64_t, 3 > >();
	}
	return true;
}

static inline
bool getValue( const nlohmann::json & js, 
	const std::string & key, 
	TN::Vec4 & val, 
	std::pair< bool, std::string > & status ) {
	std::array< float, 4 > a;
	bool found = getValue( js, key, a, status );
	if( found ) {
		val = { a[ 0 ], a[ 1 ], a[ 2 ], a[ 3 ] };
	}
	return found;
}

static inline
bool getValue( const nlohmann::json & js, 
	const std::string & key, 
	TN::Vec2< float > & val, 
	std::pair< bool, std::string > & status ) {
	std::array< float, 2 > a;
	bool found = getValue( js, key, a, status );
	if( found ) {
		val = { a[ 0 ], a[ 1 ] };
	}
	return found;
}

static inline
bool getValue( const nlohmann::json & js, 
	const std::string & key, 
	std::set< std::string > & val,
	const std::set< std::string > & valid, 
	std::pair< bool, std::string > & status ) {
	if( ! js.is_array() ) {
		status.first = false;
		status.second += WRONG_TYPE_MSG + key + ", expected array< string >\n";
		return false;
	} else {	
		std::vector< std::string > arr = js.get< std::vector< std::string  > >();
		for( auto & v : arr ) {
			if( ! valid.count( v ) ) {
				status.first = false;
				status.second += "Error: Found invalid string " + v +  "\n.";
				return false;	
			}
		}
		val = std::set< std::string >( arr.begin(), arr.end() );
	}
	return true;
}

static inline
bool getValue( const nlohmann::json & js, 
	const std::string & key, int64_t & val, 
	std::pair< bool, std::string > & status ) {
	if( ! js.is_number_integer() ) {
		status.first = false;
		status.second += WRONG_TYPE_MSG + key + ", expected integer\n";
		return false;
	} else {
		val = js.get<int64_t>();
	}
	return true;
}


static inline
bool getValue( const nlohmann::json & js, 
	const std::string & key, 
	int32_t & val, std::pair< bool, std::string > & status ) {
	if( ! js.is_number_integer() ) {
		status.first = false;
		status.second += WRONG_TYPE_MSG + key + ", expected integer\n";
		return false;
	} else {
		val = js.get<int32_t>();
	}
	return true;
}

static inline
bool getValue( const nlohmann::json & js, 
	const std::string & key, 
	double & val, 
	std::pair< bool, std::string > & status ) {
	if( ! js.is_number() ) {
		status.first = false;
		status.second += WRONG_TYPE_MSG + key + ", expected integer\n";
		return false;
	} else {
		val = js.get<double>();
	}
	return true;
}

static inline
bool getValue( const nlohmann::json & js, 
	const std::string & key, 
	nlohmann::json & val, 
	std::pair< bool, std::string > & status ) {
	if( ! js.is_object() ) {
		status.first = false;
		status.second += WRONG_TYPE_MSG + key + ", expected object\n";
		return false;
	} else {
		val = js.get<nlohmann::json>();
	}
	return true;
}

static inline
bool getValue( const nlohmann::json & js, 
	const std::string & key, 
	std::vector< double > & val, 
	std::pair< bool, std::string > & status ) {
	if( ! js.is_array() ) {
		status.first = false;
		status.second += WRONG_TYPE_MSG + key + ", expected array of numbers\n";
		return false;
	} else {
		for( auto & v : js )  {
			if( ! v.is_number() ) {
				status.first = false;
				status.second += WRONG_TYPE_MSG + key + ", expected array of numbers\n";
				return false;
			}
		}
		val = js.get< std::vector< double > > ();
	}
	return true;
}


static inline
bool getValue( const nlohmann::json & js, 
	const std::string & key, 
	std::vector< float > & val, 
	std::pair< bool, std::string > & status ) {
	if( ! js.is_array() ) {
		status.first = false;
		status.second += WRONG_TYPE_MSG + key + ", expected array of numbers\n";
		return false;
	} else {
		for( auto & v : js )  {
			if( ! v.is_number() ) {
				status.first = false;
				status.second += WRONG_TYPE_MSG + key + ", expected array of numbers\n";
				return false;
			}
		}
		val = js.get< std::vector< float> > ();
	}
	return true;
}

static inline
bool getValue( const nlohmann::json & js, 
	const std::string & key, 
	std::vector< nlohmann::json > & val, 
	std::pair< bool, std::string > & status ) {

	if( ! js.is_array() ) {
		status.first = false;
		status.second += WRONG_TYPE_MSG + key + ", expected array of numbers\n";
		return false;
	} else {
		for( auto & v : js )  {
			if( ! v.is_object() ) {
				status.first = false;
				status.second += WRONG_TYPE_MSG + key + ", expected array of numbers\n";
				return false;
			}
		}

		val = js.get< std::vector< nlohmann::json> > ();
	}
	return true;
}

static inline
bool getValue( const nlohmann::json & js, 
	const std::string & key, 
	float & val, 
	std::pair< bool, std::string > & status ) {
	if( ! js.is_number() ) {
		status.first = false;
		status.second += WRONG_TYPE_MSG + key + ", expected integer\n";
		return false;
	} else {
		val = js.get<float>();
	}
	return true;
}

static inline
bool getValue( const nlohmann::json & js, 
	const std::string & key, 
	std::string & val, 
	std::pair< bool, std::string > & status ) {
	if( ! js.is_string() ) {
		status.first = false;
		status.second += WRONG_TYPE_MSG + key + ", expected integer\n";
		return false;
	} else {
		val = js.get<std::string>();
	}
	return true;
}

static inline
bool getValue( const nlohmann::json & js, 
	const std::string & key, 
	std::vector< std::array< float, 2 > > & val, 
	std::pair< bool, std::string > & status ) {
		if( ! js.is_array() ) {
		status.first = false;
		status.second += WRONG_TYPE_MSG + key + ", expected array< ( a, b ), n >, n>0\n";
		return false;
	} else {	
		auto arr = js.get< std::vector< std::array< float, 2 > > >();
		if( arr.size() == 0 ) {
			status.first = false;
			status.second += WRONG_TYPE_MSG + key + ", expected array< ( a, b ), n>0, a0 < b0 < a1 < b1 ... \n";
			return false;
		}
		else {
 			val.resize( arr.size() );
			float aPrev = arr[ 0 ][ 0 ];
			for( size_t i = 0; i < arr.size(); ++i ) {
				float a = arr[ i ][ 0 ];
				float b = arr[ i ][ 1 ];
				if( a >= b || a < aPrev ) {
					status.first = false;
					status.second += WRONG_TYPE_MSG + key + key + ", expected array< ( a, b ), n>0, a0 < b0 < a1 < b1 ... \n";
					return false;
				}
			}
		}
		val = arr;
	} 

	return true;
}

static inline
bool getValue( const nlohmann::json & js, 
	const std::string & key, 
	std::pair< std::string, double > & val, 
	std::pair< bool, std::string > & status ) {
	
	std::string condition;
	double conditionValue;

	bool exists = false;
	for( auto & validKey : std::vector< std::string >( { "maxDelta", "meanDelta", "none" } ) ) {
		if( js.find( validKey ) != js.end() ) {
			exists = true;
			condition = validKey;
		}
	}
	if( exists ) {
		auto v = js[ condition ];
		if( ! v.is_number() ) {
			status.first = false;
			status.second += WRONG_TYPE_MSG + key + ", expected { \"key\" : number } \n";
			return false;
		} else {
			v = js.get<double>();
		}
	} else {
		status.first = false;
		status.second += WRONG_TYPE_MSG + key + ", expected { \"key\" : number } \n";		
		return false;
	}
	val = { condition, conditionValue };
	return true;
}

static inline
bool getParameter( 
	const nlohmann::json & json, 
	const std::string & key,
	std::set< std::string > & val,
	std::pair< bool, std::string > & status,
	bool isOptional,
	const std::set< std::string > & def,
	const std::set< std::string > & valid )
{
	const auto it = json.find( key );
	if( it == json.end() ) {
		status.second += ( isOptional ? "Optional" : "Required" ) 
			+  std::string( " parameter: " ) + key + " not found, using defaults\n";
		if( ! isOptional ){
			status.first = false;
		}
		val = def; 
		return false;
	}
	return TN::CF::getValue( *it, key, val, valid, status );
}

template< typename T >
static inline
bool getParameter( 
	const nlohmann::json & json, 
	const std::string & key,
	T & val,
	std::pair< bool, std::string > & status,
	const bool isOptional,
	const T & def  )
{
	const auto it = json.find( key );
	if( it == json.end() ) {
		status.second += ( isOptional ? "Optional" : "Required" ) 
			+  std::string( " parameter: " ) + key + " not found, using defaults\n";
		if( ! isOptional ){
			status.first = false;
		}
		val = def; 
		return false;
	}
	return TN::CF::getValue( *it, key, val, status );
}


} // end namespace CF

} // end namespace TN

#endif