#ifndef TN_Z_FILE_SELECTOR_HPP
#define TN_Z_FILE_SELECTOR_HPP

#include "input/FileSelector.hpp"

#include <string>
#include <stdio.h>

namespace TN {
	
class ZFileSelector : public TN::FileSelector
{
	std::string getZenityFile( const std::string & cmd )
	{
	    char result[1024];
	    
	    FILE *f = popen( cmd.c_str(), "r" );
	    fgets(result, 1024, f);
	    auto err = pclose( f );

	    std::string r( result );
	    if( r.back() == '\n' )
	    {
	        r.pop_back();
	    }
	    return r;
	}

	public:

	std::string getFile( const std::string & startingPath ) override 
	{
		std::string zenityConfDlg = "zenity --file-selection --title=\"Select File\""
			   + std::string( "--filename=\"" ) + startingPath + "\" 2> /dev/null";

	   std::string filePath = getZenityFile( zenityConfDlg );

	   return filePath;
	}

	std::string getFile( const std::string & startingPath, const std::string & extention ) override 
	{
		std::string zenityConfDlg = "zenity --file-selection --title=\"Select File\""
			   + std::string( "--filer=\"*." + extention + "\" --filename=\"" ) + startingPath + "\" 2> /dev/null";

	   std::string filePath = getZenityFile( zenityConfDlg );

	   return filePath;
	}

	virtual ~ZFileSelector() {}
};

}

#endif