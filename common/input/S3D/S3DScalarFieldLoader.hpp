#ifndef S3D_SCALAR_FIELD_LOADER
#define S3D_SCALAR_FIELD_LOADER

#include "algorithms/Standard/MyAlgorithms.hpp"

#include <boost/filesystem.hpp>

#include <cstdlib>
#include <iostream>
#include <vector>
#include <string>
#include <fstream>
#include <sstream>
#include <map>

struct TimeVaryingScalarFieldParams
{
    std::string location;
    std::vector< std::string > stepStrs;
    std::vector< double > realTime;
    int width;
    int height;
    std::vector< std::string > variables;
    std::map< std::string, TN::Vec2< float > > ranges;
    std::map< std::string, int > offsets;
    std::map< std::string, float > scaleFactors;
};

static inline void loadTimeStepInfo(
	const std::string & location,
    std::vector< std::string > & simulationTimeStepsStrs,
    std::vector< double > & realTime )
{
    using namespace boost;

    const std::string BASE_PATH = location + "/data";
    filesystem::path p( BASE_PATH );

    simulationTimeStepsStrs.clear();
    realTime.clear();

    if( ! filesystem::is_directory( p ) )
    {
        std::cerr << p << " is not a directory";
    }
    else
    {
        std::cerr << "going through directory " << p.c_str() << std::endl;

        filesystem::directory_iterator end_itr;
        for ( filesystem::directory_iterator itr( p ); itr != end_itr; ++itr )
        {
            std::string current_file = itr->path().filename().string();            
            if ( is_regular_file( itr->path() ) )
            {
                std::string current_file = itr->path().filename().string();
        
                std::stringstream sstr( current_file );
                std::string a,b,c;

                std::getline( sstr, a, '.' );
                std::getline( sstr, b, '.' );
                std::getline( sstr, c, '.' );

                simulationTimeStepsStrs.push_back( b + "." + c );
                realTime.push_back( std::stod( b + "." + c ) );
            }
        }
    }

    std::cerr << "done, sorting" << std::endl;

    TN::Parallel::sortTogether( realTime, simulationTimeStepsStrs );
}

static inline void loadVariableInfo(
    const std::string & location,
    std::vector< std::string > & variables, 
    std::map< std::string, int > & offsets,
    std::map< std::string, TN::Vec2< float > > & ranges,
    std::map< std::string, float > & scaleFactors )
{
    std::ifstream varFile( location + "/variables.txt" );

    std::string varString;
    int vIdx = 0;
    while( std::getline( varFile, varString ) )
    {
        variables.push_back( varString );
        offsets.insert( { varString, vIdx } );
        ++vIdx;
    }
    varFile.close();

    varFile.open( location + "/matlab/ref.txt" );
    vIdx = 0;
    while( std::getline( varFile, varString ) )
    {
        std::stringstream sstr( varString );
        std::string a, b, c, d;
        sstr >> a >> b >> c >> d;
        double scaleFactor = std::stod( d );
        scaleFactors.insert( { variables[ vIdx ], scaleFactor } );
        ++vIdx;
    }

    varFile.close();
}

static inline void loadTimeStep(
    const std::string & location,
    const TimeVaryingScalarFieldParams & info,
    const std::string & variable,
    int step,
    std::vector< float > & field )
{
    std::string t = info.stepStrs[ step ];
    std::string fileName = location + "/data/hcci." + t + ".field.mpi";
    std::ifstream inFile( fileName );

    if( ! inFile.is_open() )
    {
           std::cerr << "failed to load file: " << fileName << std::endl;
    }

    const int SIZE = info.width * info.height;   

    static std::vector< double > swap; 
    
    swap.resize( SIZE );
    field.resize( SIZE );

    inFile.seekg( SIZE * info.offsets.at( variable ) * sizeof( double ) );
    inFile.read( (char *) swap.data(), SIZE * sizeof( double ) );

    const double SCALE_FACTOR = info.scaleFactors.at( variable );

    for( int i = 0; i < SIZE; ++i )
    {
        field[ i ] = swap[ i ] * SCALE_FACTOR;
    }

    inFile.close();
}

static inline void loadDataInfo( 
    const std::string & location,
    TimeVaryingScalarFieldParams & info,
    bool computeRange = true )
{
    info.location = location;
    info.width  = 672;
    info.height = 672;    
    loadTimeStepInfo( location, info.stepStrs, info.realTime );
    loadVariableInfo( location, info.variables, info.offsets, info.ranges, info.scaleFactors );

    std::vector< float > field;

    for( auto v : info.variables )
    {
        if ( computeRange )
        {
            std::cout << "computing global range for variable " << v << std::endl;
            float mn =  std::numeric_limits< float >::max();
            float mx = -std::numeric_limits< float >::max();

            for( int i = 0; i < info.stepStrs.size(); ++i )
            {
                loadTimeStep( location, info, v, i, field );
                mn = std::min( mn, TN::Sequential::min( field ) );
                mx = std::max( mx, TN::Sequential::max( field ) );
            }
            info.ranges.insert( { v, { mn, mx } } );
        }
    }
} 

#endif