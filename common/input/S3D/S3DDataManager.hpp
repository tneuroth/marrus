#ifndef TN_S3D_DATA_MANAGER
#define TN_S3D_DATA_MANAGER

#include "geometry/Vec.hpp"
#include "input/S3D/S3DIn.hpp"
#include "input/DataField.hpp"

#include <vector>
#include <map>
#include <string>

// #include "algorithms/IsoContour/CubeMarcher.hpp"
// #include "algorithms/Standard/MyAlgorithms.hpp"
// #include "algorithms/DistanceFunction/DistanceFunction.hpp"
// #include "geometry/TrilinearInterpolator.hpp"
// #include "geometry/SurfaceGenerator.hpp"
// #include "latex/runpdflatex.hpp"
// #include "render/Texture.hpp"
// #include "geometry/SurfaceGenerator.hpp"
// #include "utils/StrUtils.hpp"
// #include "input/S3D/S3DMetaDataParser.hpp"
// #include "input/S3D/S3DIn.hpp"

// #include <boost/filesystem.hpp>
// #include <memory>
// #include <unordered_map>
// #include <map>
// #include <set>
// #include <string>
// #include <fstream>
// #include <type_traits>
// #include <limits>
// #include <cmath>
// #include <atomic>

namespace TN
{
    template< class RAW_TYPE, class CONVERTED_TYPE >	
    class S3DDataManager
    {
    	// data info

    	TN::S3DIn m_s3dIn;
    	std::map< std::string, TN::DataField > m_dataFields;

        // time step info

	    std::vector< double >      m_simTimeSteps;
	    std::vector< std::string > m_timeStepPaths;
	    std::vector< std::string > m_simTimeStrs;
	    size_t m_selectedStep;

	    // classification heuristics

	    std::string progressVariable;
	    double progressVariableThreshold;
	    
	    std::string extinctionVariable;
	    double extinctionVariableThreshold;

        // data in memory

        std::map< std::string, std::unique_ptr< std::vector< CONVERTED_TYPE > > > data;  

        // flame surface

    	std::vector< TN::Vec3< CONVERTED_TYPE > > m_flameVertsUnstretched;
    	std::vector< TN::Vec3< CONVERTED_TYPE > > m_flameVertsStreched;    	
        std::vector< TN::Vec3< CONVERTED_TYPE > > m_flameNormalsStretched;    
        std::vector< int                        > m_flameSurfaceBitFlags;
	    
	    bool m_flameExists;
	    bool m_extinctionHolesExist;

        // coordinates after stretching

	    std::vector< double > m_xVolumeCoordinates;
	    std::vector< double > m_yVolumeCoordinates;
	    std::vector< double > m_zVolumeCoordinates;

        
    };
}

#endif