#ifndef TN_SIMPLE_S3D_DATA_MANAGER
#define TN_SIMPLE_S3D_DATA_MANAGER

#include "geometry/Vec.hpp"
#include "algorithms/IsoContour/CubeMarcher.hpp"
#include "algorithms/Standard/MyAlgorithms.hpp"
#include "algorithms/DistanceFunction/DistanceFunction.hpp"
#include "geometry/TrilinearInterpolator.hpp"
#include "geometry/SurfaceGenerator.hpp"
#include "latex/runpdflatex.hpp"
#include "latex/LatexTableGenerator.hpp"

#include "render/Texture.hpp"
#include "geometry/SurfaceGenerator.hpp"
#include "utils/StrUtils.hpp"
#include "input/S3D/S3DMetaDataParser.hpp"
// #include "input/S3D/S3DIn.hpp"

#include <boost/filesystem.hpp>

#include <memory>
#include <unordered_map>
#include <map>
#include <set>
#include <string>
#include <fstream>
#include <type_traits>
#include <limits>
#include <cmath>
#include <atomic>

// #include <filesystem>

static inline void getSpatialScaling(
    const std::string & pathX,
    const std::string & pathY,
    const std::string & pathZ,    
    std::vector< float > & x,
    std::vector< float > & y,
    std::vector< float > & z,
    const TN::Vec3< int > & volumeDims )
{
    x.resize( volumeDims.x() );
    y.resize( volumeDims.y() );
    z.resize( volumeDims.z() );

    if( pathX == "" || true )
    {
        // hard coded for now to do testing
        for( int i = 0; i < x.size(); ++i ) { x[ i ] = (float) i; }
        for( int i = 0; i < y.size(); ++i ) { y[ i ] = (float) i; }
        for( int i = 0; i < z.size(); ++i ) { z[ i ] = (float) i; }

        // std::ofstream inFile( pathX, std::ios::out | std::ios::binary );
        // inFile.write( (char*) x.data(), x.size() * sizeof( float ) );
        // inFile.close();

        // inFile.open( pathY, std::ios::out | std::ios::binary );
        // inFile.write( (char*) y.data(), y.size() * sizeof( float ) );
        // inFile.close();
        
        // inFile.open( pathZ, std::ios::out | std::ios::binary );        
        // inFile.write( (char*) z.data(), z.size() * sizeof( float ) );
        // inFile.close();
    }
    else
    {
        std::ifstream inFile( pathX );
        inFile.read( (char*) x.data(), x.size() * sizeof( float ) );
        inFile.close();

        inFile.open( pathY );
        inFile.read( (char*) y.data(), y.size() * sizeof( float ) );
        inFile.close();
        
        inFile.open( pathZ );        
        inFile.read( (char*) z.data(), z.size() * sizeof( float ) );
        inFile.close();
    }
}

static inline void getTimeStepInfo(
    const std::string & location,
    std::vector< std::string > & simulationTimeStepsStrs,
    std::vector< double > & simSteps,
    std::vector< std::string > & filePaths,
    std::string matchedPrefix )
{
    using namespace boost;

    
    if ( ! filesystem::exists( location ) )
    {
        std::cout << "file: " << location << " doesn't exist" << std::endl;
    }

    std::cout << "opening file: " << location << std::endl;
    filesystem::path p( location );

    simulationTimeStepsStrs.clear();
    simSteps.clear();

    std::cout << "opened file" << std::endl;

    if( ! filesystem::is_directory( p ) )
    {
        std::cerr << p << " is not a directory";
    }
    else
    {
        std::cerr << "going through directory " << p.c_str() << std::endl;

        filesystem::directory_iterator end_itr;
        for ( filesystem::directory_iterator itr( p ); itr != end_itr; ++itr )
        {
            std::string current_file = itr->path().filename().string();            
            if ( is_regular_file( itr->path() ) )
            {
                std::string current_file = itr->path().filename().string();
        
                std::stringstream sstr( current_file );
                std::string a,b,c;

                std::getline( sstr, a, '.' );
                
                if( a == matchedPrefix )
                {
                    std::getline( sstr, b, '.' );
                    std::getline( sstr, c, '.' );

                    simulationTimeStepsStrs.push_back( b + "." + c );
                    simSteps.push_back( std::stod( b + "." + c ) );
                    filePaths.push_back( current_file );
                }
            }
        }
    }

    TN::Parallel::sortTogether( simSteps, simulationTimeStepsStrs );
    TN::Parallel::sortTogether( simSteps, filePaths );
}


template< class RAW_TYPE, class CONVERTED_TYPE >
class SimpleS3dDataManager
{
    void extractFlameSurface()
    {
        bool progressVariableWasStored = data.at( progressVariable ) != nullptr;
        if( ! progressVariableWasStored )
        {
            loadVariable( progressVariable );
        }
        
        const size_t X = volumeDims.x();
        const size_t Y = volumeDims.y();
        const size_t Z = volumeDims.z();

        auto rng = ranges.at( progressVariable );

        getSurface(
            trueValueToScaledValue( progressVariableThreshold, progressVariable ),
            *data.at( progressVariable ),
            flameSurfaceMesh,
            flameSurfaceNormals );

        flameSurfaceBitFlags = std::vector< int >( flameSurfaceMesh.size(), 0 );

        if( ! progressVariableWasStored )
        {
            free( progressVariable );
        }

        if( flameSurfaceMesh.size() == 0 )
        {
            return;
        }

        // check for extinction holes ////////////////////////////////////////////////////////////

        std::cout << "checking for extinction holes" << std::endl;

        bool extinctionVariableStored = data.at( extinctionVariable ) != nullptr;
        
        if( ! extinctionVariableStored )
        {
            loadVariable( extinctionVariable );
        }

        auto evRange = ranges.at( extinctionVariable );
 
        std::vector< CONVERTED_TYPE > scalars( flameSurfaceMesh.size() );
        CONVERTED_TYPE NORM = std::max( std::max( X, Y ), Z );

        std::cout << "interpolating heuristic variable" << std::endl;

        triInt( 
            *get( extinctionVariable ), 
            X, 
            Y, 
            Z, 
            extent(),
            flameSurfaceMesh,
            scalars );

        std::cout << "applying bitflag" << std::endl;

        const int N_VERTS = flameSurfaceMesh.size();

        float scaledExtinctionThreshold = trueValueToScaledValue( extinctionVariableThreshold, extinctionVariable );
        for( size_t i = 0; i < N_VERTS; ++i )
        {
            flameSurfaceBitFlags[ i ] = 0;

            // for now only flag is first bit to indicate that the part of the surface is part of an extinction hole
            if( scalars[ i ] < extinctionVariableThreshold  )
            {
                flameSurfaceBitFlags[ i ] |= 1; 
                extinctionHolesExist = true;
            }
        }

        if( ! extinctionVariableStored )
        {
            free( extinctionVariable );
        }

        flameSurfaceMeshNormalized = flameSurfaceMesh;
    }

    void deriveFlameDistance()
    {
        const size_t X = volumeDims.x();
        const size_t Y = volumeDims.y();
        const size_t Z = volumeDims.z();

        const size_t SZ = X*Y*Z;

        std::vector< TN::Vec3< CONVERTED_TYPE > > verts;
        std::vector< int > flags;

        std::unordered_map< TN::Vec3< CONVERTED_TYPE >, size_t > vts;

        std::cout << "deriving flame distance" << std::endl;

        // removes duplicates

        for( size_t i = 0; i < flameSurfaceMesh.size(); ++i )
        {
            vts.insert( { flameSurfaceMesh[ i ], i } );
        }

        for( const auto & v : vts )
        {
            flags.push_back( flameSurfaceBitFlags[ v.second ] );
            verts.push_back( v.first );
        }

        const size_t N_VERTS = verts.size();
        std::vector< CONVERTED_TYPE > & result = *data.at( "flameDistance" );

        // todo handle voxel to position translation generally
 
        // std::vector< float > xCoords( X );  
        // std::vector< float > yCoords( Y );  
        // std::vector< float > zCoords( Z );  

        // hard coded for now to do testing
        // for( int i = 0; i < X; ++i ) 
        // {
        //     xCoords[ i ] = (float) i; 
        // }
        // for( int i = 0; i < Y; ++i ) { yCoords[ i ] = (float) i; }
        // for( int i = 0; i < Z; ++i ) { zCoords[ i ] = (float) i; }

        m_distanceFunction.compute(
            verts,
            flags,
            xVolumeCoordinates,
            yVolumeCoordinates,
            zVolumeCoordinates,
            *get( progressVariable ),
            (float) trueValueToScaledValue( progressVariableThreshold, progressVariable ),
            result,
            false );
    }

    void deriveFlameProximity()
    {
        bool flameDistanceWasLoaded = data.at( progressVariable ) != nullptr;
        if( ! flameDistanceWasLoaded )
        {
            loadVariable( "flameDistance" );
        }
        auto rng = ranges.at( "flameDistance" );
        std::vector< float > & fd = *data.at( "flameDistance" );
        const size_t NV = fd.size();

        std::vector< CONVERTED_TYPE > & result = * data.at( "flameProximity" );

        #pragma omp parallel for
        for( size_t i = 0; i < NV; ++i )
        {
            float trueValue = fd[ i ] * ( rng.b() - rng.a() ) + rng.a();
            result[ i ] = std::abs( trueValue );
        }

        if( ! flameDistanceWasLoaded )
        {
            // free( "flameDistance" );
        }
    }

    void deriveVM()
    {
        bool vxWasLoaded = data.at( "vu" ) != nullptr;
        bool vyWasLoaded = data.at( "vv" ) != nullptr;
        bool vzWasLoaded = data.at( "vw" ) != nullptr;                
    
        if( ! vxWasLoaded )
        {
            loadVariable( "vu" );
        }

        if( ! vyWasLoaded )
        {
            loadVariable( "vv" );
        }

        if( ! vzWasLoaded )
        {
            loadVariable( "vw" );
        }        

        std::cout << "loaded vu vv vw" << std::endl;

        const std::vector< CONVERTED_TYPE > & vx = * data.at( "vu" );
        const std::vector< CONVERTED_TYPE > & vy = * data.at( "vv" );
        const std::vector< CONVERTED_TYPE > & vz = * data.at( "vw" );        

        const size_t SZ = volumeDims.x() * volumeDims.y() * volumeDims.z();
        std::vector< CONVERTED_TYPE > & vm = *data.at( "vm" );

        std::cout << "lreset vm" << std::endl;

        #pragma omp parallel for
        for( size_t i = 0; i < SZ; ++i )
        {
            vm[ i ] = TN::Vec3< CONVERTED_TYPE >( vx[ i ], vy[ i ], vz[ i ] ).length();
        }

        if( ! vxWasLoaded )
        {
            free( "vu" );
        }

        if( ! vyWasLoaded )
        {
            free( "vv" );
        }

        if( ! vzWasLoaded )
        {
            free( "vw" );
        }   

        std::cout << "free data" << std::endl;
    }
    
    void deriveVorticity()
    {
        // TODO
    }
    
    void derivePreferentialDiffusion()
    {
        // TODO
    }

    std::map< std::string, std::unique_ptr< std::vector< CONVERTED_TYPE > > > data;  

    // currently hard coded
    std::set< std::string > derivedVariables;

    std::map< std::string, size_t > volumeVariables;
    std::map< std::string, RAW_TYPE > conversionFactors;
    std::map< std::string, std::string > latexLabels;

    std::string progressVariable;
    double progressVariableThreshold;
    
    std::string extinctionVariable;
    double extinctionVariableThreshold;

    std::string basePath;

    std::vector< double > simTimeSteps;
    std::vector< std::string > timeStepPaths;
    std::vector< std::string > simTimeStrs;

    size_t selectedStep;
    TN::Vec3< int > volumeDims;

    std::vector< float > xVolumeCoordinates;
    std::vector< float > yVolumeCoordinates;
    std::vector< float > zVolumeCoordinates;

    // TODO ( and how: over all files, over each file as its loaded, optional? etc)
    std::map< std::string, TN::Vec2< CONVERTED_TYPE > > ranges;

    std::vector< TN::Vec3< CONVERTED_TYPE > > flameSurfaceMeshNormalized;
    std::vector< TN::Vec3< CONVERTED_TYPE > > flameSurfaceMesh;
    std::vector< TN::Vec3< CONVERTED_TYPE > > flameSurfaceNormals;    
    std::vector< int > flameSurfaceBitFlags;    
    bool flameExists;
    bool extinctionHolesExist;

    TN::DistanceFunction m_distanceFunction;

    TN::S3DIn s3dIn;

public:

    SimpleS3dDataManager() {}

    TN::Vec3< int > getVolumeDims() const { return volumeDims; }

    void init( 
        const std::string & dataPath,
        const std::string & dataFilePrefix,
        const std::string & varFile,
        const std::string & initialFile,
        const std::string workingDir,
        const std::string pdflatexDir,
        const std::string pdftocairoDir,
        const std::string convertDir,
        std::atomic< int > & status )
    {
        basePath = dataPath;

        status = 0;

        getFieldAndConversionInfo( 
            varFile + "/info.txt", 
            volumeVariables, 
            conversionFactors, 
            latexLabels,
            progressVariable, 
            progressVariableThreshold, 
            extinctionVariable, 
            extinctionVariableThreshold,
            volumeDims,
            s3dIn );

        getSpatialScaling(
            varFile + "/x_coords.bin",
            varFile + "/y_coords.bin",
            varFile + "/z_coords.bin",
            xVolumeCoordinates,
            yVolumeCoordinates,            
            zVolumeCoordinates,
            volumeDims );

        status = 1;

        // only flame distance and vm are implemented
        derivedVariables = { "flameDistance", "flameProximity", "vm", "vorticity", "preferentialDiffusion" };

        for( auto & v : volumeVariables )
        {
            data.insert( { v.first, nullptr } );
            ranges.insert( { v.first, TN::Vec2< CONVERTED_TYPE >() } );
        }

        for( auto & v : derivedVariables )
        {
            latexLabels.insert( { v, "$" + v + "$" } );
            data.insert( { v, nullptr } );
            ranges.insert( { v, TN::Vec2< CONVERTED_TYPE >() } );
        }
 
        if( latexLabels.count( progressVariable ) )
        {
            std::string flameDefinition = 
                "$\\textbf{flame }";
            
            std::string progLatex = latexLabels.at( progressVariable );
            if( progLatex.front() == '$' )
            {
                progLatex = progLatex.substr(1);
            }
            if( progLatex.back() == '$' )
            {
                progLatex.pop_back();
            }

            flameDefinition += progLatex + " = " + TN::toStringWFMT( progressVariableThreshold ) + "$";
            latexLabels.insert( { "flameDefinition", flameDefinition } );
        }

        if( latexLabels.count( extinctionVariable ) )
        {
            std::string extinctionDefinition = 
                "$\\textbf{extinction }";
            
            std::string extLatex = latexLabels.at( extinctionVariable );
            if( extLatex.front() == '$' )
            {
                extLatex = extLatex.substr(1);
            }
            if( extLatex.back() == '$' )
            {
                extLatex.pop_back();
            }

            extinctionDefinition += extLatex + " < " + TN::toStringWFMT( extinctionVariableThreshold ) + "$";
            latexLabels.insert( { "extinctionDefinition", extinctionDefinition } );
        }

        const std::string dir = workingDir;

        simTimeStrs  = { "0" };
        simTimeSteps = {  0  };        
        timeStepPaths = { initialFile };
        selectedStep = 0;
 
        // getTimeStepInfo( 
        //     dataPath, 
        //     simTimeStrs, 
        //     simTimeSteps, 
        //     timeStepPaths, 
        //     dataFilePrefix );

        // TODO: how they want to specifiy selected step
        // if( initialFile == "" )
        // {
        //     selectedStep = 0;
        // }
        // else
        // {
        //     selectedStep = 0;
        //     for( size_t i = 0; i < timeStepPaths.size(); ++i )
        //     {
        //         if( timeStepPaths[ i ] == initialFile )
        //         {
        //             selectedStep = i;
        //             break;
        //         }
        //     }
        // }

        extinctionHolesExist = false;
        extractFlameSurface();
        flameExists = flameSurfaceMesh.size() > 0;

        status = 2;

        latexLabels.insert( { "xaxis", "x-axis" } ); 
        latexLabels.insert( { "yaxis", "y-axis" } );

        for( auto v : latexLabels )
        {
            bool forceRecompilation = true;
            const std::string outPath = dir + "/textures/latex/" + v.first + ".pdf";
            if( forceRecompilation || ( ! std::ifstream( outPath ).good() ) )
            {
                TN::generateEquationPNG(
                    dir + "common/latex/templates/cropped_equation_template.tex",
                    v.second,
                    dir + "/textures/latex/",
                    v.first,
                    pdflatexDir,
                    pdftocairoDir,
                    convertDir,
                    true,
                    true
                );
            }
        }
    }

    void loadVariable( const std::string & variable )
    {
        const size_t SZ = volumeDims.x() * volumeDims.y() * volumeDims.z();
        data.at( variable ).reset( new std::vector< CONVERTED_TYPE >( SZ ) );
        std::vector< CONVERTED_TYPE > & storage = *data.at( variable );

        bool invert = false;

        // is it a variable in the file, or a derived one?
        if( volumeVariables.count( variable ) )
        {
            std::ifstream inFile( basePath + timeStepPaths[ selectedStep ] );

            if( ! inFile.is_open() )
            {
                std::cerr << "failed to load file: " << ( basePath + timeStepPaths[ selectedStep ] ) << std::endl;
                exit( 0 );
                // should be an exception
            }

            const RAW_TYPE SCALE_FACTOR = conversionFactors.at( variable ); 

            if( std::is_same< RAW_TYPE, CONVERTED_TYPE >::value )
            {
                inFile.seekg( SZ * volumeVariables.at( variable ) * sizeof( RAW_TYPE ) );
                inFile.read( (char *) storage.data(), SZ * sizeof( RAW_TYPE ) );
            
                if( SCALE_FACTOR != 1.0 )
                {
                    #pragma omp parallel for
                    for( size_t i = 0; i < SZ; ++i )
                    {
                        storage[ i ] *= SCALE_FACTOR;
                    }
                }
            }
            else
            {
                std::vector< RAW_TYPE > swap( SZ ); 
                inFile.seekg( SZ * volumeVariables.at( variable ) * sizeof( double ) );
                inFile.read( (char *) swap.data(), SZ * sizeof( double ) );                                
            
                #pragma omp parallel for
                for( size_t i = 0; i < SZ; ++i )
                {
                    storage[ i ] =  swap[ i ] * SCALE_FACTOR;
                }
            }

            inFile.close();
        }
        // for the moment, just hard code the derivation of specific variables
        // later some other mechanism (plugins, etc can be used)
        else if( derivedVariables.count( variable ) )
        {
            if( variable == "flameDistance" )
            {
                deriveFlameDistance();
            }
            else if( variable == "flameProximity" )
            {
                deriveFlameProximity();
                invert = true;
            } 
            else if( variable == "vm" )
            {
                std::cout << "deriving vm" << std::endl;
                deriveVM();
                std::cout << "done deriving vm" << std::endl;
            }
            else if( variable == "vorticity" )
            {
                deriveVorticity();
            }
            else if ( variable == "preferentialDiffusion" )
            {
                derivePreferentialDiffusion();
            }
        }
        else
        {
            // should be an exception
            std::cout << "something wrong" << std::endl;
            exit( 0 );
        }

        if( ranges.count( "vm" ) == 0 )
        {
            std::cout << "range is not mapped" << std::endl;  
        }

        ranges.at( variable ) = TN::Sequential::getRange( storage );

        const double width = ranges.at( variable ).b() - ranges.at( variable ).a();
        const double mn = ranges.at( variable ).a();

        if( width > 0 )
        {
            #pragma omp parallel for
            for( size_t i = 0; i < SZ; ++i )
            {
                storage[ i ] = ( storage[ i ] - mn ) / width;
            }
        }

        if( invert )
        {
            std::cout << "inverting" << std::endl;
            #pragma omp parallel for
            for( size_t i = 0; i < SZ; ++i )
            {
                storage[ i ] = 1.0 - storage[ i ];
            }
        }
    }

    std::vector< CONVERTED_TYPE > * get( const std::string & variable )
    {
        if( data.count( variable ) == 0 )
        {
            return nullptr;
        }
        else if( data.at( variable ) == nullptr )
        {
             loadVariable( variable );      
        }
        else 
        {
            //std::cout << "already stored" << std::endl;
        }

        return data.at( variable ).get();
    }

    TN::Vec2< CONVERTED_TYPE > getRange( const std::string & var ) const
    {
        if( ranges.count( var ) == 0 )
        {
            std::cout << "error tried accessing range for " << var << " which is not found" << std::endl;
            exit( 0 );
        }
        return ranges.at( var );
    }

    void free( const std::string & variable )
    {
        if( data.count( variable ) != 0 )
        {
            data.at( variable ) = nullptr;
        }
    }

    std::string label( const std::string & key ) const 
    {
        if( latexLabels.count( key ) )
        {
            return latexLabels.at( key );
        }
        else
        {
            return "";
        }
    }

    std::pair< TN::Vec3< float >, TN::Vec3< float > > extent() const
    {
        return { 
            { xVolumeCoordinates.front(), yVolumeCoordinates.front(), zVolumeCoordinates.front() },
            { xVolumeCoordinates.back(),  yVolumeCoordinates.back(),  zVolumeCoordinates.back()  } };
    }

    bool hasFlame() const { return flameExists; }
    bool hasExtinctionHoles() const { return flameExists; }    

    const std::vector< TN::Vec3< CONVERTED_TYPE > > & getFlameSurfaceMesh() const {
        return flameSurfaceMeshNormalized;
    }
    const std::vector< TN::Vec3< CONVERTED_TYPE > > & getFlameSurfaceNormals() const {
        return flameSurfaceNormals;
    }
    const std::vector< int > & getFlameSurfaceFlags() const {
        return flameSurfaceBitFlags;
    }

    void getSurface(
        double isovalue,
        const std::vector< float > & d,
        std::vector< TN::Vec3< float >  > & verts,
        std::vector< TN::Vec3< float >  > & normals )
    {
        computeIsosurface( 
            isovalue,    
            d, 
            volumeDims, 
            xVolumeCoordinates,
            yVolumeCoordinates,
            zVolumeCoordinates,
            verts, 
            normals );
    }

    void getSurface(
        double isovalue,
        const std::string & varKey,
        std::vector< TN::Vec3< float >  > & verts,
        std::vector< TN::Vec3< float >  > & normals )
    {
        getSurface( 
            isovalue,    
            *get( varKey ), 
            verts, 
            normals );
    }

    void interpolateToSurface(
        const std::vector< float > & d,
        const std::vector< TN::Vec3< float >  > & verts,
        std::vector< float > & result )
    {
        const double NORM = std::max( std::max( volumeDims.x(), volumeDims.y() ), volumeDims.z() );

        triInt( 
            d, 
            volumeDims.x(),
            volumeDims.y(),
            volumeDims.z(),
            extent(),
            verts,
            result );
    }

    void interpolateToSurface(
        const std::string & varKey,
        const std::vector< TN::Vec3< float >  > & verts,
        std::vector< float > & result )
    {
        interpolateToSurface( *get( varKey ), verts, result );
    }

    float scaledValueToTrueValue( float v, const std::string & varKey )
    {
        auto rng = ranges.at( varKey );
        return v * ( rng.b() - rng.a() ) + rng.a();
    }

    float trueValueToScaledValue( float v, const std::string & varKey )
    {
        auto rng = ranges.at( varKey );
        return ( v - rng.a() ) / ( rng.b() - rng.a() );
    }

    const std::vector< float > & xCoords() const
    {
        return xVolumeCoordinates;
    }

    const std::vector< float > & yCoords() const
    {
        return yVolumeCoordinates;
    }

    const std::vector< float > & zCoords() const
    {
        return zVolumeCoordinates;
    }
};

#endif
