#ifndef TN_S3D_IN_HPP
#define TN_S3D_IN_HPP

#include <string>
#include <iostream>
#include <fstream>
#include <sstream>

namespace TN {

struct S3DIn
{
	void skipN( std::ifstream & f, int n )
	{
        std::string line;
        for( int i = 0; i < n; ++i ) 
        { 
        	if( ! std::getline( inFile, line ) )
        	{
	        	std::cerr << "Error parsing S3D input, skip N" << std::endl;
	        	exit( 1 );        	
        	} 
        }
	}

    template< class T >
	void getParam( std::ifstream & f, T & param )
	{
        std::string line;
        if( ! std::getline( inFile, line ) )
        {
	    	std::cerr << "Error parsing S3D input, getParam, getLine" << std::endl;
	    	exit( 1 );        	
        }
        std::stringstream sstr( line );
        sstr >> param;
        if( sstr.fail() )
        {
	    	std::cerr << "Error parsing S3D input, getParam, sstream" << std::endl;
	    	exit( 1 );     
        }
	}

    void parse( const std::string & filePath )
    {    
        std::ifstream inFile( filePath );
        if( ! inFile.is_open() )
        {
        	std::cerr << "Couldn't open file: " << filePath << std::endl;
        	exit( 1 );
        }

		// ==========================================================================================
		// GRID DIMENSION PARAMETERS
		// ==========================================================================================
		// 3456                   - global number of grid points in the x-direction    (nx_g)
		// 1280                   - global number of grid points in the y-direction    (ny_g)
		// 2560                   - global number of grid points in the z-direction    (nz_g)
		// 36                     - number of processors in x-direction                (npx)
		// 8                      - number of processors in y-direction                (npy)
		// 20                     - number of processors in z-direction                (npz)
        
        skipN( inFile, 3 );
		getParam( inFile, nx_g );
		getParam( inFile, ny_g );
		getParam( inFile, nz_g );
		getParam( inFile, npx  );
		getParam( inFile, npy  );
		getParam( inFile, npz  );


		// ==========================================================================================
		// RUN-TIME PARAMETERS
		// ==========================================================================================
		// 0                   - 0 for write output to screen, 1 for write to file  (i_write)
		// 1                   - 0 for new run, 1 for restart                       (i_restart)
		// 2000                  - ending time step                                   (i_time_end)
		// 500                 - frequency to save fields in restart files          (i_time_save)
		// 1.0e+5              - time period to save fields in restart files        (time_save_inc)

        skipN( inFile, 3 );
		getParam( inFile, i_write       );
		getParam( inFile, i_restart     );
		getParam( inFile, i_time_end    );
		getParam( inFile, i_time_save   );
		getParam( inFile, time_save_inc );

		// ==========================================================================================
		// GEOMETRY PARAMETERS
		// ==========================================================================================
		// ptj_turb_prod       - title of run, sets initialiation of flow field     (run_title) // this is the prefix from my config file
		// 1                   - 0 for no x-direction dependence, 1 for so          (vary_in_x)
		// 1                   - 0 for no y-direction dependence, 1 for so          (vary_in_y)
		// 1                   - 0- for no z-direction dependence, 1 for so          (vary_in_z)
		// 1                   - 0 for non-periodic in x-direction, 1 for periodic  (periodic_x)
		// 0                   - 0 for non-periodic in y-direction, 1 for periodic  (periodic_y)
		// 1                   - 0 for non-periodic in z-direction, 1 for periodic  (periodic_z)
		// 1                   - 0 for stretched edges in x-dir, 1 for uniform      (unif_grid_x)
		// 0                   - 0 for stretched edges in y-dir, 1 for uniform      (unif_grid_y)
		// 1                   - 0 for stretched edges in z-dir, 1 for uniform      (unif_grid_z)
		// 40.0e-6             - minimum grid spacing for streching in x direction  (min_grid_x)
		// 3.74e-5             - minimum grid spacing for streching in y direction  (min_grid_y)
		// 40.0e-6             - minimum grid spacing for streching in z direction  (min_grid_z)
		// 1                   - 0 for no mean flow, 1,2,3 for mean flow in x,y,z   (i_flowdir)
		// 1                   - 0 for no turbulence, 1 for isotropic turbulence    (i_turbulence)
		// 1                   - BC at x=0 boundary; 1 nonreflecting, 0 hard inflow (nrf_x0)
		// 1                   - BC at x=L boundary; 1 nonreflecting, 0 hard inflow (nrf_xl)
		// 1                   - BC at y=0 boundary; 1 nonreflecting, 0 hard inflow (nrf_y0)
		// 1                   - BC at y=L boundary; 1 nonreflecting, 0 hard inflow (nrf_yl)
		// 1                   - BC at z=0 boundary; 1 nonreflecting, 0 hard inflow (nrf_z0)
		// 1                   - BC at z=L boundary; 1 nonreflecting, 0 hard inflow (nrf_zl)
		// 0.2                 - fix factor for pressure drift                      (relax_ct)

        skipN( inFile, 3 );
		getParam( inFile, run_title    );
		getParam( inFile, vary_in_x    );  
		getParam( inFile, vary_in_y    );
		getParam( inFile, vary_in_z    );
		getParam( inFile, periodic_x   );
		getParam( inFile, periodic_y   );
		getParam( inFile, periodic_z   );
		getParam( inFile, unif_grid_x  );
		getParam( inFile, unif_grid_y  );
		getParam( inFile, unif_grid_z  );
		getParam( inFile, min_grid_x   );
		getParam( inFile, min_grid_y   );
		getParam( inFile, min_grid_z   );
		getParam( inFile, i_flowdir    );
		getParam( inFile, i_turbulence );
		getParam( inFile, nrf_x0       );
		getParam( inFile, nrf_xl       );
		getParam( inFile, nrf_y0       );
		getParam( inFile, nrf_yl       );
		getParam( inFile, nrf_z0       );
		getParam( inFile, nrf_zl       );
		getParam( inFile, relax_ct     );

		// ==========================================================================================
		// PHYSICAL PARAMETERS
		// ==========================================================================================
		// 0.0                 - minimum value of grid in x-direction in cm         (xmin)
		// 0.0                 - minimum value of grid in y-direction in cm         (ymin)
		// 0.0                 - minimum value of grid in z-direction in cm         (zmin)
		// 12.0                - maximum value of grid in x-direction in cm         (xmax)
		// 7.5                 - maximum value of grid in y-direction in cm         (ymax)
		// 9.0                 - minimum value of grid in z-direction in cm         (zmax)
		// 0.001               - Mach number where re_real/mach_no = re_acoustic    (mach_no)
		// 100.0               - real convective Reynolds number                    (re_real)
		// 0.708               - Prandtl number                                     (pr)

        skipN( inFile, 3 );
		getParam( inFile, xmin    );
		getParam( inFile, ymin    );
		getParam( inFile, zmin    );
		getParam( inFile, xmax    );
		getParam( inFile, ymax    );
		getParam( inFile, zmax    );
		getParam( inFile, mach_no );
		getParam( inFile, re_real );
		getParam( inFile, pr      );

		// ==========================================================================================
		// NUMERICS PARAMETERS
		// ==========================================================================================
		// 1                   - 0 for no reaction, 1 for reaction                   (i_react)
		// 8                   - order of spatial derivatives: 6th or 8th only       (iorder)
		// 10                 - frequency to monitor min/max and active              (i_time_mon)
		// -1                  - frequency to check spatial resolution               (i_time_res)
		// 5000                - frequency to write tecplot file                     (i_time_tec)
		// 10                   - order of spatial filter                            (i_filter)
		// 10                   - frequency to filter solution vector                (i_time_fil)

        skipN( inFile, 3 );
		getParam( inFile, i_react    );
		getParam( inFile, iorder     );
		getParam( inFile, i_time_mon );
		getParam( inFile, i_time_res );
		getParam( inFile, i_time_tec );
		getParam( inFile, i_filter   );
		getParam( inFile, i_time_fil );

		// ==========================================================================================
		// REQUIRED REFERENCE VALUES
		// ==========================================================================================
		// 1.4                 - reference ratio of specific heats                    (g_ref)
		// 347.2               - reference speed of sound (m/s)                       (a_ref)
		// 300.0               - freestream temperature (K)                           (to)
		// 1.1766              - reference density (kg/m^3)                           (rho_ref)
		// 26.14e-3            - reference thermal conductivity (W/m-s)               (lambda_ref) 

        skipN( inFile, 3 );
		getParam( inFile, g_ref      );
		getParam( inFile, a_ref      );
		getParam( inFile, to         );
		getParam( inFile, rho_ref    );
		getParam( inFile, lambda_ref ); 
    }

	// ==========================================================================================
	// GRID DIMENSION PARAMETERS
	// ==========================================================================================

	int nx_g; // global number of grid points in the x-direction 
	int ny_g; // global number of grid points in the y-direction 
	int nz_g; // global number of grid points in the z-direction 
	int npx;  // number of processors in x-direction             
	int npy;  // number of processors in y-direction             
	int npz;  // number of processors in z-direction             

	// ==========================================================================================
	// RUN-TIME PARAMETERS
	// ==========================================================================================

	int i_write;       // 0 for write output to screen, 1 for write to file 
	int i_restart;     // 0 for new run, 1 for restart                      
	int i_time_end;    // ending time step                                
	int i_time_save;   // frequency to save fields in restart files         
	int time_save_inc; // time period to save fields in restart files       

	// ==========================================================================================
	// GEOMETRY PARAMETERS
	// ==========================================================================================

	std::string run_title;  // title of run, sets initialiation of flow field  
	int vary_in_x;          // 0 for no x-direction dependence, 1 for so          
	int vary_in_y;          // 0 for no y-direction dependence, 1 for so          
	int vary_in_z;          // 0 for no z-direction dependence, 1 for so         
	int periodic_x;         // 0 for non-periodic in x-direction, 1 for periodic  
	int periodic_y;         // 0 for non-periodic in y-direction, 1 for periodic  
	int periodic_z;         // 0 for non-periodic in z-direction, 1 for periodic  
	int unif_grid_x;        // 0 for stretched edges in x-dir, 1 for uniform      
	int unif_grid_y;        // 0 for stretched edges in y-dir, 1 for uniform      
	int unif_grid_z;        // 0 for stretched edges in z-dir, 1 for uniform      
	double min_grid_x;      // minimum grid spacing for streching in x direction  
	double min_grid_y;      // minimum grid spacing for streching in y direction  
	double min_grid_z;      // minimum grid spacing for streching in z direction  
	int i_flowdir;          // 0 for no mean flow, 1,2,3 for mean flow in x,y,z   
	int i_turbulence;       // 0 for no turbulence, 1 for isotropic turbulence    
	int nrf_x0;             // BC at x=0 boundary; 1 nonreflecting, 0 hard inflow 
	int nrf_xl;             // BC at x=L boundary; 1 nonreflecting, 0 hard inflow 
	int nrf_y0;             // BC at y=0 boundary; 1 nonreflecting, 0 hard inflow 
	int nrf_yl;             // BC at y=L boundary; 1 nonreflecting, 0 hard inflow 
	int nrf_z0;             // BC at z=0 boundary; 1 nonreflecting, 0 hard inflow 
	int nrf_zl;             // BC at z=L boundary; 1 nonreflecting, 0 hard inflow 
	double relax_ct;        // fix factor for pressure drift                      

	// ==========================================================================================
	// PHYSICAL PARAMETERS
	// ==========================================================================================

	double xmin;    // minimum value of grid in x-direction in cm      
	double ymin;    // minimum value of grid in y-direction in cm      
	double zmin;    // minimum value of grid in z-direction in cm      
	double xmax;    // maximum value of grid in x-direction in cm      
	double ymax;    // maximum value of grid in y-direction in cm      
	double zmax;    // minimum value of grid in z-direction in cm      
	double mach_no; // Mach number where re_real/mach_no = re_acoustic 
	double re_real; // real convective Reynolds number                 
	double pr;      // Prandtl number                                  

	// ==========================================================================================
	// NUMERICS PARAMETERS
	// ==========================================================================================

	int i_react;      //  0 for no reaction, 1 for reaction             
	int iorder;       //  order of spatial derivatives: 6th or 8th only 
	int i_time_mon;   // frequency to monitor min/max and active        
	int i_time_res;   //  frequency to check spatial resolution         
	int i_time_tec;   //  frequency to write tecplot file               
	int i_filter;     // - order of spatial filter                      
	int i_time_fil;   // - frequency to filter solution vector          

	// ==========================================================================================
	// REQUIRED REFERENCE VALUES
	// ==========================================================================================

	double g_ref;      // reference ratio of specific heats 
	double a_ref;      // reference speed of sound (m/s) 
	double to;         // freestream temperature (K)            
	double rho_ref;    // reference density (kg/m^3)            
	double lambda_ref; // reference thermal conductivity (W/m-s)
};

}

#endif

// ==========================================================================================
// GRID DIMENSION PARAMETERS
// ==========================================================================================
// 3456                   - global number of grid points in the x-direction    (nx_g)
// 1280                   - global number of grid points in the y-direction    (ny_g)
// 2560                   - global number of grid points in the z-direction    (nz_g)
// 36                     - number of processors in x-direction                (npx)
// 8                      - number of processors in y-direction                (npy)
// 20                     - number of processors in z-direction                (npz)
// ==========================================================================================
// RUN-TIME PARAMETERS
// ==========================================================================================
// 0                   - 0 for write output to screen, 1 for write to file  (i_write)
// 1                   - 0 for new run, 1 for restart                       (i_restart)
// 2000                  - ending time step                                   (i_time_end)
// 500                 - frequency to save fields in restart files          (i_time_save)
// 1.0e+5              - time period to save fields in restart files        (time_save_inc)
// ==========================================================================================
// GEOMETRY PARAMETERS
// ==========================================================================================
// ptj_turb_prod       - title of run, sets initialiation of flow field     (run_title) // this is the prefix from my config file
// 1                   - 0 for no x-direction dependence, 1 for so          (vary_in_x)
// 1                   - 0 for no y-direction dependence, 1 for so          (vary_in_y)
// 1                   - 0- for no z-direction dependence, 1 for so          (vary_in_z)
// 1                   - 0 for non-periodic in x-direction, 1 for periodic  (periodic_x)
// 0                   - 0 for non-periodic in y-direction, 1 for periodic  (periodic_y)
// 1                   - 0 for non-periodic in z-direction, 1 for periodic  (periodic_z)
// 1                   - 0 for stretched edges in x-dir, 1 for uniform      (unif_grid_x)
// 0                   - 0 for stretched edges in y-dir, 1 for uniform      (unif_grid_y)
// 1                   - 0 for stretched edges in z-dir, 1 for uniform      (unif_grid_z)
// 40.0e-6             - minimum grid spacing for streching in x direction  (min_grid_x)
// 3.74e-5             - minimum grid spacing for streching in y direction  (min_grid_y)
// 40.0e-6             - minimum grid spacing for streching in z direction  (min_grid_z)
// 1                   - 0 for no mean flow, 1,2,3 for mean flow in x,y,z   (i_flowdir)
// 1                   - 0 for no turbulence, 1 for isotropic turbulence    (i_turbulence)
// 1                   - BC at x=0 boundary; 1 nonreflecting, 0 hard inflow (nrf_x0)
// 1                   - BC at x=L boundary; 1 nonreflecting, 0 hard inflow (nrf_xl)
// 1                   - BC at y=0 boundary; 1 nonreflecting, 0 hard inflow (nrf_y0)
// 1                   - BC at y=L boundary; 1 nonreflecting, 0 hard inflow (nrf_yl)
// 1                   - BC at z=0 boundary; 1 nonreflecting, 0 hard inflow (nrf_z0)
// 1                   - BC at z=L boundary; 1 nonreflecting, 0 hard inflow (nrf_zl)
// 0.2                 - fix factor for pressure drift                      (relax_ct)
// ==========================================================================================
// PHYSICAL PARAMETERS
// ==========================================================================================
// 0.0                 - minimum value of grid in x-direction in cm         (xmin)
// 0.0                 - minimum value of grid in y-direction in cm         (ymin)
// 0.0                 - minimum value of grid in z-direction in cm         (zmin)
// 12.0                - maximum value of grid in x-direction in cm         (xmax)
// 7.5                 - maximum value of grid in y-direction in cm         (ymax)
// 9.0                 - minimum value of grid in z-direction in cm         (zmax)
// 0.001               - Mach number where re_real/mach_no = re_acoustic    (mach_no)
// 100.0               - real convective Reynolds number                    (re_real)
// 0.708               - Prandtl number                                     (pr)
// ==========================================================================================
// NUMERICS PARAMETERS
// ==========================================================================================
// 1                   - 0 for no reaction, 1 for reaction                    (i_react)
// 8                   - order of spatial derivatives: 6th or 8th only        (iorder)
// 10                 - frequency to monitor min/max and active              (i_time_mon)
// -1                  - frequency to check spatial resolution                (i_time_res)
// 5000                - frequency to write tecplot file                      (i_time_tec)
// 10                   - order of spatial filter                              (i_filter)
// 10                   - frequency to filter solution vector                  (i_time_fil)
// ==========================================================================================
// REQUIRED REFERENCE VALUES
// ==========================================================================================
// 1.4                 - reference ratio of specific heats                    (g_ref)
// 347.2               - reference speed of sound (m/s)                       (a_ref)
// 300.0               - freestream temperature (K)                           (to)
// 1.1766              - reference density (kg/m^3)                           (rho_ref)
// 26.14e-3            - reference thermal conductivity (W/m-s)               (lambda_ref) 