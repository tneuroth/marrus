#ifndef TN_S3D_META_DATA_PARSER
#define TN_S3D_META_DATA_PARSER

// #include "input/S3D/S3DIn.hpp"
#include "geometry/Vec.hpp"

#include <string>
#include <map>

static inline void getFieldAndConversionInfo(
    const std::string & infoFile,
    std::map< std::string, size_t > & variableOffsets,
    std::map< std::string, double > & conversionFactors,
    std::map< std::string, std::string > & latex,    
    std::string & progressVar,
    double & progressVarThreshold,
    std::string & extVar,
    double & extVarThreshold,
    TN::Vec3< int > & dims )
{
    std::string line;
    std::string varString;
    std::string conversionFactorString;

    std::ifstream inFile( infoFile );
    
    if( ! inFile.is_open() )
    {
        std::cout << "couldn't open file: " << infoFile << std::endl;
        exit( 0 );
    }

    std::string tmp; 

    {
        // dims
        std::getline( inFile, line );
        std::stringstream sstr( line );
        sstr >> tmp; // label
        sstr >> tmp; // x
        dims.x( std::stoi( tmp ) );
        sstr >> tmp; // y 
        dims.y( std::stoi( tmp ) );    
        sstr >> tmp; // z      
        dims.z( std::stoi( tmp ) );
    }

    {
        // progress
        std::getline( inFile, line );
        std::stringstream sstr( line );

        sstr >> tmp; // label
        sstr >> progressVar; // field
        sstr >> tmp; // threshold       
        progressVarThreshold = std::stod( tmp );
    }

    {
        // extinction
        std::getline( inFile, line );
        std::stringstream sstr( line );

        sstr >> tmp; // label
        sstr >> extVar; // field
        sstr >> tmp; // threshold       
        extVarThreshold = std::stod( tmp );
    }

    // space and header
    std::getline( inFile, line );
    std::getline( inFile, line );    

    // fields and conversions
    size_t vIdx = 0;
    while( std::getline( inFile, line ) )
    {
        std::stringstream sstr( line );
        sstr >> varString >> conversionFactorString;
        conversionFactors.insert( { varString, std::stod( conversionFactorString ) } );
        variableOffsets.insert(   { varString, vIdx } );

        std::string latexStr;
        std::string rest;
        std::getline( sstr, rest );

        for( int i = 0, c = 0; i < rest.size(); ++i )
        {
            if( rest[ i ] == '$' && c == 0 )
            {
                c = 1;
            }
            else if ( rest[ i ] == '$' && c == 1 )
            {
                latexStr.push_back( rest[ i ] );
                break;
            }
            
            if( c == 1 )
            {
                latexStr.push_back( rest[ i ] );
            }
        }

        if( latexStr.size() > 0 )
        {
            if( latexStr[ 0 ] == '$' )
            {
                latex.insert( { varString, latexStr } );
                std::cout << "latex is " << latexStr << std::endl;
            }
        }
        else
        {
            std::cout << "missing or invalid latex for " << varString << std::endl;
            latex.insert( { varString, "$" + varString + "$" } );
        }

        ++vIdx;
    }

    inFile.close();

    std::cout << "got info" << std::endl;
}

#endif