#ifndef TN_BASIC_FILTER_HPP
#define TN_BASIC_FILTER_HPP

#include "geometry/Vec.hpp"
#include "geometry/GeometryUtils.hpp"

#include <vector>
#include <cstdlib>
#include <cmath>
#include <string>
#include <iostream>

namespace TN {

static constexpr uint32_t NULL_ELEMENT           = 0;
static constexpr uint32_t BACKGROUND_ELEMENT     = 1;
static constexpr uint32_t PRE_LEVEL_ELEMENT      = 2;
static constexpr uint32_t SUBSELECTION_ELEMENT   = 4;
static constexpr uint32_t VOXEL_IN_DECOMPOSITION = 8;

}

enum class FilterMode 
{
    New,
    Intersect,
    Union,
    Subtract
};


// component selection ////////////////////////////////////////////////////////

inline void filterLasso( 
    const std::vector< float > & x, 
    const std::vector< float > & y,     
    const std::vector< TN::Vec2< float > > & lasso,
    const TN::Vec2< float >    & xRange,
    const TN::Vec2< float >    & yRange, 
    std::vector< int >         & flags,
    std::vector< int >         & flagsLast,    
    FilterMode mode )
{
    const size_t NP = x.size();
    #pragma omp parallel for
    for( size_t i = 0; i < NP; ++i )
    {
        unsigned int highlightMask = int( 
            x[ i ] >= xRange.a()
         && x[ i ] <= xRange.b()
         && y[ i ] >= yRange.a()
         && y[ i ] <= yRange.b() ) * TN::SUBSELECTION_ELEMENT;

        if( highlightMask )
        {
            highlightMask *= pnpoly( lasso, { x[ i ], y[ i ] } );
        }

        flags[ i ] = TN::BACKGROUND_ELEMENT | TN::PRE_LEVEL_ELEMENT;
    
        if( mode == FilterMode::New )
        {
            flags[ i ] = flags[ i ] & ~ highlightMask; // turn the bit off
            flags[ i ] |= highlightMask;              
        }
        else if( mode == FilterMode::Intersect )
        {
            highlightMask &= flagsLast[ i ];   
            flags[ i ]    &= ~ TN::SUBSELECTION_ELEMENT;
            flags[ i ] |= highlightMask;
        }
        else if( mode == FilterMode::Union )
        {
            flags[ i ] |= highlightMask;      
        }
        else if( mode == FilterMode::Subtract )
        {
            flags[ i ] &= ~highlightMask;   
        }
    }
}


inline void filter( 
    const std::vector< float > & x, 
    const std::vector< float > & y,     
    const TN::Vec2< float >    & xRange,
    const TN::Vec2< float >    & yRange, 
    std::vector< int >         & flags,
    std::vector< int >         & flagsLast,    
    FilterMode mode )
{
    const size_t NP = x.size();
    #pragma omp parallel for
    for( size_t i = 0; i < NP; ++i )
    {
        unsigned int highlightMask = int( 
            x[ i ] >= xRange.a()
         && x[ i ] <= xRange.b()
         && y[ i ] >= yRange.a()
         && y[ i ] <= yRange.b() ) * TN::SUBSELECTION_ELEMENT;

        flags[ i ] = TN::BACKGROUND_ELEMENT | TN::PRE_LEVEL_ELEMENT;
    
        if( mode == FilterMode::New )
        {
            flags[ i ] = flags[ i ] & ~ highlightMask; // turn the bit off
            flags[ i ] |= highlightMask;              
        }
        else if( mode == FilterMode::Intersect )
        {
            highlightMask &= flagsLast[ i ];   
            flags[ i ]    &= ~ TN::SUBSELECTION_ELEMENT;
            flags[ i ] |= highlightMask;
        }
        else if( mode == FilterMode::Union )
        {
            flags[ i ] |= highlightMask;      
        }
        else if( mode == FilterMode::Subtract )
        {
            flags[ i ] &= ~highlightMask;   
        }
    }
}

// cell selection //////////////////////////////////////////////////////////

inline void filterLasso( 
    const std::vector< int32_t > & parentBitFlags,   
    const std::vector< int32_t > & parentTable,
    const std::vector< float > & x, 
    const std::vector< float > & y, 
    const std::vector< TN::Vec2< float > > & lasso,     
    const TN::Vec2< float > & xRange,
    const TN::Vec2< float > & yRange, 
    std::vector< int >      & flags,
    std::vector< int >      & flagsLast,    
    FilterMode mode )
{
    const size_t NP = x.size();
    #pragma omp parallel for
    for( size_t i = 0; i < NP; ++i )
    {
        unsigned int highlightMask = int( 
            x[ i ] >= xRange.a()
         && x[ i ] <= xRange.b()
         && y[ i ] >= yRange.a()
         && y[ i ] <= yRange.b() ) * TN::SUBSELECTION_ELEMENT; // using 2nd bit

        if( highlightMask )
        {
            highlightMask *= pnpoly( lasso, { x[ i ], y[ i ] } );
        }

        if( !( parentBitFlags[ parentTable[ i ] ] & TN::SUBSELECTION_ELEMENT ) )
        {
            flags[ i ] = TN::BACKGROUND_ELEMENT;
            continue;
        }

        flags[ i ] |= TN::BACKGROUND_ELEMENT; 
        flags[ i ] |= TN::PRE_LEVEL_ELEMENT; 

        if( mode == FilterMode::New )
        {
            flags[ i ] &= ~ TN::SUBSELECTION_ELEMENT;
            flags[ i ] |=   highlightMask;      
        }
        else if( mode == FilterMode::Intersect )
        {
            highlightMask &= flagsLast[ i ];   
            flags[ i ]    &= ~ TN::SUBSELECTION_ELEMENT;
            flags[ i ] |= highlightMask;
        }
        else if( mode == FilterMode::Union )
        {
            flags[ i ] |= highlightMask;      
        }
        else if( mode == FilterMode::Subtract )
        {
            flags[ i ] &= ~highlightMask;   
        }
    }
}


inline void filter( 
    const std::vector< int32_t > & parentBitFlags,   
    const std::vector< int32_t > & parentTable,
    const std::vector< float > & x, 
    const std::vector< float > & y,     
    const TN::Vec2< float > & xRange,
    const TN::Vec2< float > & yRange, 
    std::vector< int >      & flags,
    std::vector< int >      & flagsLast,    
    FilterMode mode )
{
    const size_t NP = x.size();
    #pragma omp parallel for
    for( size_t i = 0; i < NP; ++i )
    {
        unsigned int highlightMask = int( 
            x[ i ] >= xRange.a()
         && x[ i ] <= xRange.b()
         && y[ i ] >= yRange.a()
         && y[ i ] <= yRange.b() ) * TN::SUBSELECTION_ELEMENT; // using 2nd bit

        if( !( parentBitFlags[ parentTable[ i ] ] & TN::SUBSELECTION_ELEMENT ) )
        {
            flags[ i ] = TN::BACKGROUND_ELEMENT;
            continue;
        }

        flags[ i ] |= TN::BACKGROUND_ELEMENT; 
        flags[ i ] |= TN::PRE_LEVEL_ELEMENT; 

        if( mode == FilterMode::New )
        {
            flags[ i ] &= ~ TN::SUBSELECTION_ELEMENT;
            flags[ i ] |=   highlightMask;      
        }
        else if( mode == FilterMode::Intersect )
        {
            highlightMask &= flagsLast[ i ];   
            flags[ i ]    &= ~ TN::SUBSELECTION_ELEMENT;
            flags[ i ] |= highlightMask;
        }
        else if( mode == FilterMode::Union )
        {
            flags[ i ] |= highlightMask;      
        }
        else if( mode == FilterMode::Subtract )
        {
            flags[ i ] &= ~highlightMask;   
        }
    }
}

// version where just want selection of background, and prelevel /////////////

inline void filter( 
    const std::vector< int32_t > & parentBitFlags,   
    const std::vector< int32_t > & parentTable,
    std::vector< int32_t > & flags,
    FilterMode mode )
{
    const size_t NP = flags.size();
    #pragma omp parallel for
    for( size_t i = 0; i < NP; ++i )
    {
        flags[ i ] = TN::BACKGROUND_ELEMENT;
        if( parentBitFlags[ parentTable[ i ] ] & TN::SUBSELECTION_ELEMENT )
        {
            flags[ i ] |= TN::PRE_LEVEL_ELEMENT; 
        }
    }
}

/////////////////////////////////////////////////////////////////////////////

// new version

inline void gatherVoxelsLasso( 
    const std::vector< int >   & cellBitFlags,
    const std::vector< float > & voxelCellMap,
    const size_t NV,    
    const std::vector< float > & x, 
    const std::vector< float > & y, 
    const std::vector< TN::Vec2< float > > & lasso,
    const TN::Vec2< float > & xRange,
    const TN::Vec2< float > & yRange, 
    const std::vector< std::vector< float > * > & valsCopy,   
    std::vector< int >      & flags,
    std::vector< int >      & flagsLast,
    const std::vector< std::vector< float > * > & g_vals,  
    std::vector< int >      & g_f,    
    FilterMode mode )
{
    const size_t NUM_VARS = valsCopy.size();

    for( auto & gv : g_vals )
    {
        gv->clear();
    }
    g_f.clear();

    for( size_t i = 0; i < NV; ++i )
    {
        // exists voronoi tesselation
        if( voxelCellMap[ i ] >= 0 ) 
        { 
            flags[ i ] |= TN::VOXEL_IN_DECOMPOSITION;

            // exists inside some voronoi cell within component selection
            if( cellBitFlags[ voxelCellMap[ i ] ] & TN::PRE_LEVEL_ELEMENT )
            {
                flags[ i ] |= TN::BACKGROUND_ELEMENT;

                // part of voronoi site selection 
                if( cellBitFlags[ voxelCellMap[ i ] ] & TN::SUBSELECTION_ELEMENT )
                {
                    flags[ i ] |= TN::PRE_LEVEL_ELEMENT;

                    unsigned int highlightMask = unsigned( 
                               x[ i ] >= xRange.a()
                            && x[ i ] <= xRange.b()
                            && y[ i ] >= yRange.a()
                            && y[ i ] <= yRange.b() ) * TN::SUBSELECTION_ELEMENT;  
                    
                    if( highlightMask )
                    {
                        highlightMask *= pnpoly( lasso, { x[ i ], y[ i ] } );
                    }

                    if( mode == FilterMode::New )
                    {
                        flags[ i ] &= ~TN::SUBSELECTION_ELEMENT;
                        flags[ i ] |= highlightMask;      
                    }
                    else if( mode == FilterMode::Intersect )
                    {
                        highlightMask &= flagsLast[ i ];   
                        flags[ i ]    &= ~ TN::SUBSELECTION_ELEMENT;
                        flags[ i ]    |= highlightMask;
                    }
                    else if( mode == FilterMode::Union )
                    {
                        flags[ i ] |= highlightMask;      
                    }
                    else if( mode == FilterMode::Subtract )
                    {
                        flags[ i ] &= ~highlightMask;   
                    }

                    g_f.push_back( flags[ i ] );
                    for( size_t k = 0; k < NUM_VARS; ++k )
                    {
                        g_vals[ k ]->push_back( ( * ( valsCopy[ k ] ) )[ i ] );
                    }
                }
            }
        }
        else
        {
            flags[ i ] = TN::NULL_ELEMENT;
        }
    }
}



inline void gatherVoxels( 
    const std::vector< int >   & cellBitFlags,
    const std::vector< float > & voxelCellMap,
    const size_t NV,    
    const std::vector< float > & x, 
    const std::vector< float > & y, 
    const TN::Vec2< float > & xRange,
    const TN::Vec2< float > & yRange, 
    const std::vector< std::vector< float > * > & valsCopy,   
    std::vector< int >      & flags,
    std::vector< int >      & flagsLast,
    const std::vector< std::vector< float > * > & g_vals,  
    std::vector< int >      & g_f,    
    FilterMode mode )
{
    const size_t NUM_VARS = valsCopy.size();

    for( auto & gv : g_vals )
    {
        gv->clear();
    }
    g_f.clear();

    for( size_t i = 0; i < NV; ++i )
    {
        // exists voronoi tesselation
        if( voxelCellMap[ i ] >= 0 ) 
        { 
            flags[ i ] |= TN::VOXEL_IN_DECOMPOSITION;

            // exists inside some voronoi cell within component selection
            if( cellBitFlags[ voxelCellMap[ i ] ] & TN::PRE_LEVEL_ELEMENT )
            {
                flags[ i ] |= TN::BACKGROUND_ELEMENT;

                // part of voronoi site selection 
                if( cellBitFlags[ voxelCellMap[ i ] ] & TN::SUBSELECTION_ELEMENT )
                {
                    flags[ i ] |= TN::PRE_LEVEL_ELEMENT;

                    unsigned int highlightMask = unsigned( 
                               x[ i ] >= xRange.a()
                            && x[ i ] <= xRange.b()
                            && y[ i ] >= yRange.a()
                            && y[ i ] <= yRange.b() ) * TN::SUBSELECTION_ELEMENT;  
                    
                    if( mode == FilterMode::New )
                    {
                        flags[ i ] &= ~TN::SUBSELECTION_ELEMENT;
                        flags[ i ] |= highlightMask;      
                    }
                    else if( mode == FilterMode::Intersect )
                    {
                        highlightMask &= flagsLast[ i ];   
                        flags[ i ]    &= ~ TN::SUBSELECTION_ELEMENT;
                        flags[ i ]    |= highlightMask;
                    }
                    else if( mode == FilterMode::Union )
                    {
                        flags[ i ] |= highlightMask;      
                    }
                    else if( mode == FilterMode::Subtract )
                    {
                        flags[ i ] &= ~highlightMask;   
                    }

                    g_f.push_back( flags[ i ] );
                    for( size_t k = 0; k < NUM_VARS; ++k )
                    {
                        g_vals[ k ]->push_back( ( * ( valsCopy[ k ] ) )[ i ] );
                    }
                }
            }
        }
        else
        {
            flags[ i ] = TN::NULL_ELEMENT;
        }
    }
}

////////////////////////////////////////////////////////////////////////////////


inline void gatherVoxels( 
    const std::vector< int >   & cellBitFlags,
    const std::vector< float > & voxelCellMap,
    const size_t NV,
    const std::vector< std::vector< float > * > & vals, 
    std::vector< int >      & flags,
    const std::vector< std::vector< float > * > & g_vals,    
    std::vector< int >      & g_f,
        const std::vector< std::vector< float > * > & gSub_vals,    
    std::vector< int >      & gSub_f )
{
    for( auto & gv : g_vals )
    {
        gv->clear();
    }
    g_f.clear();

    for( auto & gv : gSub_vals )
    {
        gv->clear();
    }
    gSub_f.clear();

    const size_t NUM_VARS = vals.size();

    for( size_t i = 0; i < NV; ++i )
    {
        flags[ i ] = TN::NULL_ELEMENT;

        // exists voronoi tesselation
        if( voxelCellMap[ i ] >= 0 ) 
        { 
            flags[ i ] |= TN::VOXEL_IN_DECOMPOSITION;

            // exists inside some voronoi cell within component selection
            if( cellBitFlags[ voxelCellMap[ i ] ] & TN::PRE_LEVEL_ELEMENT )
            {
                flags[ i ] |= TN::BACKGROUND_ELEMENT;

                // part of voronoi site selection 
                if( cellBitFlags[ voxelCellMap[ i ] ] & TN::SUBSELECTION_ELEMENT )
                {
                    flags[ i ] |= TN::PRE_LEVEL_ELEMENT;
                    gSub_f.push_back( flags[ i ] );
                    for( size_t k = 0; k < NUM_VARS; ++k )
                    {
                        gSub_vals[ k ]->push_back( ( * ( vals[ k ] ) )[ i ] );
                    }
                }

                // TODO: should gather all in the layers,
                // because those are the elements which may be merged, thus
                // having this range as context makes sense for choosing parameters
                // however, then it will be slow. But the background will be statics,
                // so I can render it to texture, so that recomputation isn't necessary

                g_f.push_back( flags[ i ] );
                for( size_t k = 0; k < NUM_VARS; ++k )
                {
                    g_vals[ k ]->push_back( ( * ( vals[ k ] ) )[ i ] );
                }
            }
        }
    }
}

////////////////////////////////////////////////////////////////////////////////

#endif