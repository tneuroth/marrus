
#ifndef TN_CONN_COMP_CPU_HPP
#define TN_CONN_COMP_CPU_HPP

#include <geometry/Vec.hpp>

#include <map>
#include <cstdint>
#include <vector>
#include <iostream>

namespace TN {

inline 
int64_t flatIndex3D( 
    const int64_t x,
    const int64_t y,
    const int64_t z,
    const int64_t X,
    const int64_t Y,
    const int64_t Z )
{
     return x + y*X + z*Y*X;
}

template< typename T >
inline void mapLayers( 
	const std::vector< T > & lfield,
    const int64_t NV,  
	const TN::Vec2< T > & range,
	const std::vector< TN::Vec2< T > > & layers,
	std::vector< int16_t > & layerMap )
{
	layerMap.resize( NV );
    const int NL = (int) layers.size();

	#pragma omp parallel for
	for( size_t idx = 0; idx < NV; ++idx )
	{
	    T lf = lfield[ idx ];
	    layerMap[ idx ] = 0; 

	    for( int l = 0; l < NL; ++l )
	    {
	        T la = layers[ l ].a();
	        T lb = layers[ l ].b();        
	        if( lf >= la && lf < lb )
	        {
	            layerMap[ idx ] = l + 1;
	            break;
	        }
	    }
	}
}

template < typename FT, typename IDT >
inline
void mapLayers(
    const int64_t N,
    const FT * fieldData,
    const std::vector< std::pair< float, float > > & layers,
    IDT * result )
{
    for( int64_t idx = 0; idx < N; ++ idx ) {
        const FT s = fieldData[ idx ];
        result[ idx ] = 0;
        for( size_t l = 0, end = layers.size(); l < end; ++l ) {
            const FT la = layers[ l ].first;
            const FT lb = layers[ l ].second;
            if( s >= la && s < lb  ) {
                result[ idx ] = l + 1;
                break;
            }
        }
    }
}
 
inline
int uf_find( int idx, int32_t * eqc, const int E0 )
{
    int tmp = idx;
    while ( eqc[ E0 + tmp ] != tmp ) 
    { 
        tmp = eqc[ E0 + tmp ]; 
    }
    
    while ( eqc[ E0 + idx ] != idx ) 
    {
      const int z = eqc[ E0 + idx ];
      eqc[ E0 + idx ] = tmp;
      idx = z;
    }

    return tmp;
}

inline
int uf_union( const int idx1, const int idx2, int32_t * eqc, int & count, const int E0  )
{
    const int tmp1 = uf_find( idx1, eqc, E0 );
    const int tmp2 = uf_find( idx2, eqc, E0 );
    if( tmp1 != tmp2 )
    {
        --count;
    }
    return eqc[ E0 + tmp1 ] = tmp2;
}

template< typename LIDT, typename CIDT >
inline
int cclabelPass(
    CIDT * componentIds,
    const LIDT * layerIds,
    CIDT * eqcTMP,
    CIDT * eqc,
    const int64_t E0,    
    const int xI,
    const int yI,
    const int zI,
    const int xEnd,
    const int yEnd, 
    const int zEnd,
    const int XD,
    const int YD,
    const int ZD )
{ 
    bool hasBackground = false;

    const int YX = YD * XD;
    int numComponents = 0;
    for ( int x = xI; x < xEnd; ++x ) { 
    for ( int y = yI; y < yEnd; ++y ) { const int IDX2D = x + y*XD;
    for ( int z = zI; z < zEnd; ++z ) {      
    {
        const int64_t IDX = IDX2D + z*YX;
        if( layerIds[ IDX ] != 0 )
        {
            const int64_t IUP = ( x == xI ) ? 0 : IDX -  1;
            const int64_t ILF = ( y == yI ) ? 0 : IDX - XD;
            const int64_t IBK = ( z == zI ) ? 0 : IDX - YX;

            const uint8_t GO = uint8_t( x != 0 ? layerIds[ IUP ] == layerIds[ IDX ] : 0u ) 
                             | uint8_t( y != 0 ? layerIds[ ILF ] == layerIds[ IDX ] : 0u ) << 1
                             | uint8_t( z != 0 ? layerIds[ IBK ] == layerIds[ IDX ] : 0u ) << 2;

            /////////////////////////////////////////////////////////////////////////////////////////

            int32_t cID;
            if( ! GO )
            {
                ++numComponents;
                eqcTMP[ E0 ] += 1;
                eqcTMP[ E0 + eqcTMP[ E0 ] ] = eqcTMP[ E0 ];
                cID = eqcTMP[ E0 ];
            } 
            else
            {
                const int32_t up   = GO & 1u ? componentIds[ IUP ] : 0;
                const int32_t left = GO & 2u ? componentIds[ ILF ] : 0;
                const int32_t back = GO & 4u ? componentIds[ IBK ] : 0;

                const int32_t MLB = std::max( left, back );
                const bool B1 = GO & 1u;

                if( ( B1 && GO != 1u ) || GO == 6u )
                {
                    if( GO > 5u )
                    {
                        cID = uf_union( left, back, eqcTMP, 
                                numComponents, E0 ); 
                    }
                    if( B1 ) 
                    {            
                        cID = uf_union( up, MLB, eqcTMP, 
                                numComponents, E0 ); 
                    }
                }
                else
                {           
                    cID = std::max( up, MLB );
                }
            }
            componentIds[ IDX ] = cID;
        }
        else
        {
            hasBackground = true;
        }
    } } } }

    for ( int x = xI; x < xEnd; x++ ) {
    for ( int y = yI; y < yEnd; y++ ) { const int IDX2D = x + y*XD;
    for ( int z = zI; z < zEnd; z++ ) {
    {
        const int64_t IDX = IDX2D + z*YX; //flatIndex3D( x, y, z, XD, YD, ZD );
        if ( layerIds[ IDX ] != 0 )
        {
            const int OFFSET = E0 + uf_find( componentIds[ IDX ], eqcTMP, E0 );
            if( eqc[ OFFSET ] == 0 )
            {
                ++eqc[ E0 ];
                eqc[ OFFSET ] = eqc[ E0 ];
            }
            componentIds[ IDX ] = eqc[ OFFSET ];
        }
    } } } }

    return numComponents + int( hasBackground );
}

template< typename LIDT, typename CIDT >
inline void connectedComponents( 
    const LIDT * layerIds,
    const std::array< int64_t, 3 > & dims,  
    const int32_t maxLabels,
    CIDT * componentIds,
    int32_t & NC )
{
    std::vector< CIDT > eqTmp( maxLabels, 0 );
    std::vector< CIDT > eq(    maxLabels, 0 );    

    const size_t N = dims[ 0 ] * dims[ 1 ] * dims[ 2 ];
    std::fill( componentIds, componentIds + N, 0 );

    const int E0 = 0;
    const int FXD = dims[ 0 ];
    const int FYD = dims[ 1 ];

    const int nBlockDims = dims.size();

    NC = 0;

    const int FZD = dims[ 2 ];

    const int fxI = 0;
    const int fyI = 0;
    const int fzI = 0;

    const int xEnd = FXD;
    const int yEnd = FYD;
    const int zEnd = FZD;

    NC = cclabelPass( 
        componentIds, 
        layerIds,            
        eqTmp.data(),
        eq.data(),
        E0,
        fxI, 
        fyI, 
        fzI, 
        xEnd,
        yEnd, 
        zEnd, 
        FXD, 
        FYD, 
        FZD );
}


template< typename LIDT, typename CIDT >
inline void connectedComponents( 
	const std::vector< LIDT > & layerIds,
    const std::array< int64_t, 3 > & dims,	
	const int32_t maxLabels,
	std::vector< CIDT > & componentIds,
	int32_t & NC )
{
    std::vector< CIDT > eqTmp( maxLabels, 0 );
    std::vector< CIDT > eq(    maxLabels, 0 );    

    const int E0 = 0;
    const int FXD = dims[ 0 ];
    const int FYD = dims[ 1 ];

    const int nBlockDims = dims.size();

    NC = 0;

    componentIds.resize( dims[ 0 ] * dims[ 1 ] * dims[ 2 ] );
    std::fill( componentIds.begin(), componentIds.end(), 0 );

    const int FZD = dims[ 2 ];

    const int fxI = 0;
    const int fyI = 0;
    const int fzI = 0;

    const int xEnd = FXD;
    const int yEnd = FYD;
    const int zEnd = FZD;
    
    // std::cout << "cclabelPass" 
    //     << " " << componentIds.size()
    //     << " " << layerIds.size()        
    //     << " " << eqTmp.size()
    //     << " " << eq.size()
    //     << " " << E0
    //     << " " << fxI
    //     << " " << fyI
    //     << " " << fzI
    //     << " " << xEnd
    //     << " " << yEnd
    //     << " " << zEnd
    //     << " " << FXD
    //     << " " << FYD
    //     << " " << FZD << std::endl;

    NC = cclabelPass( 
        componentIds.data(), 
        layerIds.data(),            
        eqTmp.data(),
        eq.data(),
        E0,
        fxI, 
        fyI, 
        fzI, 
        xEnd,
        yEnd, 
        zEnd, 
        FXD, 
        FYD, 
        FZD );
}

inline void getHierarchy(
    const std::vector< int16_t > & layerIds,
    const std::vector< int32_t > & componentIds,    
    const std::vector< int32_t > & dims,
    std::map< int32_t, std::vector< int32_t > > & hierarchy,
    int NC )
{
    int X = dims[ 0 ];
    int Y = dims[ 1 ];
    int Z = dims[ 2 ];
    
    std::vector< int32_t > enclosingComponentMap( NC, -1 );

    for( int x1 = 1; x1 < X; ++x1 )
    for( int y1 = 1; y1 < Y; ++y1 )
    for( int z1 = 1; z1 < Z; ++z1 )
    {
        int64_t idx1 = flatIndex3D( x1, y1, z1, X, Y, Z );
        int32_t c1 = componentIds[ idx1 ];
        int32_t l1 = layerIds[ idx1 ];        

        if( l1 <= 0 )
        {
            continue;
        }

        for( int x2 = x1-1; x2 < x1; ++x2 )
        for( int y2 = y1-1; y2 < y1; ++y2 )
        for( int z2 = z1-1; z2 < z1; ++z2 )
        {
            int64_t idx2 = flatIndex3D( x2, y2, z2, X, Y, Z );
            int32_t c2 = componentIds[ idx2 ];
            int32_t l2 = layerIds[ idx2 ];    

            if( l2 <= 0 )
            {
                continue;
            }

            if( l1 < l2 )
            {
                enclosingComponentMap[ c1 ] = c2;
            }
            else if( l2 > l1 )
            {
                enclosingComponentMap[ c2 ] = c1;                
            }
        }
    }

    for( int i = 0; i < enclosingComponentMap.size(); ++i )
    {
        hierarchy.insert( { i, std::vector< int32_t >() } );
    }

    for( int i = 0; i < enclosingComponentMap.size(); ++i )
    {
        if( enclosingComponentMap[ i ] >= 0 )
        {
            hierarchy.at( enclosingComponentMap[ i ] ).push_back( i );
        }
    }    
}

inline void mapHierarchy( 
    const std::vector< int32_t > & voxelToComponent,  
    const std::vector< int32_t > & componentToParent,
    const std::vector< int32_t > & relableTable,
    const int64_t NV,
    std::vector< int32_t > & labelResult )
{
    labelResult.resize( NV );
    #pragma omp parallel for
    for( int64_t i = 0; i < NV; ++i )
    {
        labelResult[ i ] = relableTable[
            componentToParent[ 
                voxelToComponent[ i ] ] ];
    }
}

}

#endif