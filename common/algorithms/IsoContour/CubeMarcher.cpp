#include <array>
#include "algorithms/IsoContour/CubeMarcher.hpp"

namespace TN
{

using namespace MarcherConsts;

float CubeMarcher::fGetOffset( float fValue1, float fValue2, float fValueDesired)
{
    double fDelta = fValue2 - fValue1;

    if( fDelta == 0.0 )
    {
        return 0.5;
    }
    return ( fValueDesired - fValue1 ) / fDelta;
}

void CubeMarcher::addNormal( const Vec3< float > & vertex, const Vec3< float > & normal )
{
    std::unordered_map< Vec3< float >, Vec3< float > >::iterator it;
    if( ( it = m_adjacentFaceNormals.find( vertex ) ) != m_adjacentFaceNormals.end() )
    {
        it->second = it->second + normal;
    }
    else
    {
        m_adjacentFaceNormals.insert( std::pair< Vec3< float >, Vec3< float >  >( vertex, normal ) );
    }
}

void CubeMarcher::marchCube( 
    const std::vector< float > & volume, 
    const int x, 
    const int y, 
    const int z )
{
    Vec3< float > asEdgeVertex[12];

    const std::array< float, 8 > cubeValues = m_indexOrder == true ? 
    std::array< float, 8 > {
        volume[ ( x     ) + ( y     ) * m_dimX + ( z     ) * m_dimY * m_dimX ],
        volume[ ( x + 1 ) + ( y     ) * m_dimX + ( z     ) * m_dimY * m_dimX ],
        volume[ ( x + 1 ) + ( y + 1 ) * m_dimX + ( z     ) * m_dimY * m_dimX ],
        volume[ ( x     ) + ( y + 1 ) * m_dimX + ( z     ) * m_dimY * m_dimX ],
        volume[ ( x     ) + ( y     ) * m_dimX + ( z + 1 ) * m_dimY * m_dimX ],
        volume[ ( x + 1 ) + ( y     ) * m_dimX + ( z + 1 ) * m_dimY * m_dimX ],
        volume[ ( x + 1 ) + ( y + 1 ) * m_dimX + ( z + 1 ) * m_dimY * m_dimX ],
        volume[ ( x     ) + ( y + 1 ) * m_dimX + ( z + 1 ) * m_dimY * m_dimX ]
    } :

    std::array< float, 8 > {
        volume[ m_dimY * m_dimZ * ( x     ) + m_dimZ * ( y     ) + z     ],
        volume[ m_dimY * m_dimZ * ( x + 1 ) + m_dimZ * ( y     ) + z     ],
        volume[ m_dimY * m_dimZ * ( x + 1 ) + m_dimZ * ( y + 1 ) + z     ],
        volume[ m_dimY * m_dimZ * ( x     ) + m_dimZ * ( y + 1 ) + z     ],
        volume[ m_dimY * m_dimZ * ( x     ) + m_dimZ * ( y     ) + z + 1 ],
        volume[ m_dimY * m_dimZ * ( x + 1 ) + m_dimZ * ( y     ) + z + 1 ],
        volume[ m_dimY * m_dimZ * ( x + 1 ) + m_dimZ * ( y + 1 ) + z + 1 ],
        volume[ m_dimY * m_dimZ * ( x     ) + m_dimZ * ( y + 1 ) + z + 1 ]
    };

    int insideFlags = 0;
    for( int i = 0; i < 8; ++i )
    {
        if( cubeValues[ i ] <= m_targetValue )
        {
            insideFlags |= 1 << i;
        }
    }

    const int edgeFlags = aiCubeEdgeFlags[ insideFlags ];

    if( edgeFlags == 0 )
    {
        return;
    }

    for( int edge = 0; edge < 12; ++edge )
    {
        if( edgeFlags & ( 1 << edge ) )
        {
            const float offset = fGetOffset( 
                cubeValues[ a2iEdgeConnection[ edge ][ 0 ] ],
                cubeValues[ a2iEdgeConnection[ edge ][ 1 ] ], m_targetValue );

            asEdgeVertex[ edge ].x(
                x + ( a2fVertexOffset[ a2iEdgeConnection[ edge ][ 0 ] ][ 0 ]  +  offset * a2fEdgeDirection[ edge ][ 0 ] ) );

            asEdgeVertex[ edge ].y(
                y + ( a2fVertexOffset[ a2iEdgeConnection[ edge ][ 0 ] ][ 1 ]  +  offset * a2fEdgeDirection[ edge ][ 1 ] ) );

            asEdgeVertex[ edge ].z(
                z + ( a2fVertexOffset[ a2iEdgeConnection[ edge ][ 0 ] ][ 2 ]  +  offset * a2fEdgeDirection[ edge ][ 2 ] ) );
        }
    }

    for( int t = 0; t < 5; ++t )
    {
        const int t3 = 3*t;

        if( a2iTriangleConnectionTable[ insideFlags ][ t3 ] < 0 ) {
            break;
        }

        const Vec3< float > v1 = asEdgeVertex[ a2iTriangleConnectionTable[ insideFlags ][ t3     ] ];
        const Vec3< float > v2 = asEdgeVertex[ a2iTriangleConnectionTable[ insideFlags ][ t3 + 1 ] ];
        const Vec3< float > v3 = asEdgeVertex[ a2iTriangleConnectionTable[ insideFlags ][ t3 + 2 ] ];

        m_vertices.push_back( v1 );
        m_vertices.push_back( v2 );
        m_vertices.push_back( v3 );
    }
}

void CubeMarcher::marchCubes( const std::vector< float > & volume )
{
    const int X_END = m_dimX-1;
    const int Y_END = m_dimY-1;
    const int Z_END = m_zend-1;
    const int Z_START = m_zstart;

    for( int z = Z_START; z < Z_END; ++z )
    {
        for( int y = 0; y < Y_END; ++y )
        {
            for( int x = 0; x < X_END; ++x )
            {
                marchCube( volume, x, y, z );
            }
        }
    }
}

void CubeMarcher::marchCubes( 
    const std::vector< float > & volume, 
    const std::vector< uint8_t > & mask,
    const int x0, 
    const int y0, 
    const int z0, 
    const int xe, 
    const int ye, 
    const int ze )
{
    const int XE = std::min( m_dimX - 1, xe );
    const int YE = std::min( m_dimY - 1, ye );
    const int ZE = std::min( m_dimZ - 1, ze );

    for( int x = x0; x < XE; ++x )
    {
        for( int y = y0; y < YE; ++y )
        {
            for( int z = z0; z < ZE; ++z )
            {
                if( mask[ x + y * m_dimX + z * m_dimX * m_dimY  ] ) {
                    marchCube( volume, x, y, z );
                }
            }
        }
    }
}

void CubeMarcher::marchCubes( 
    const std::vector< float > & volume, 
    const std::vector< uint8_t > & mask )
{
    for( int x = 0; x < m_dimX-1; ++x )
    {
        for( int y = 0; y < m_dimY-1; ++y )
        {
            for( int z = 0; z < m_dimZ-1; ++z )
            {
                if( mask[ x + y * m_dimX + z * m_dimX * m_dimY  ] ) {
                    marchCube( volume, x, y, z );
                }
            }
        }
    }
}

void CubeMarcher::scaleCoordinates( 
    const std::vector< float > & xCoords,
    const std::vector< float > & yCoords,
    const std::vector< float > & zCoords )
{
    const size_t NV = m_vertices.size();
    #pragma omp parallel for
    for( size_t i = 0; i < NV; ++i )
    {
        // int xIdx = std::min( std::max( (int) std::round( m_vertices[ i ].x() ), 0 ), ( (int) xCoords.size() ) - 1 );
        // int yIdx = std::min( std::max( (int) std::round( m_vertices[ i ].y() ), 0 ), ( (int) yCoords.size() ) - 1 );
        // int zIdx = std::min( std::max( (int) std::round( m_vertices[ i ].z() ), 0 ), ( (int) zCoords.size() ) - 1 );    
        // m_vertices[ i ] = { xCoords[ xIdx ], yCoords[ yIdx ], zCoords[ zIdx ] };

        int xi = std::max( std::min( ( int ) std::floor( m_vertices[ i ].x() ),  (int) xCoords.size() - 2 ), (int) 0 );
        int yi = std::max( std::min( ( int ) std::floor( m_vertices[ i ].y() ),  (int) yCoords.size() - 2 ), (int) 0 );
        int zi = std::max( std::min( ( int ) std::floor( m_vertices[ i ].z() ),  (int) zCoords.size() - 2 ), (int) 0 );
        
        double xw = xCoords[ xi + 1 ] - xCoords[ xi ];
        double yw = yCoords[ yi + 1 ] - yCoords[ yi ];
        double zw = zCoords[ zi + 1 ] - zCoords[ zi ];

        double rx = ( m_vertices[ i ].x() - xCoords[ xi ] ) / xw;
        double ry = ( m_vertices[ i ].y() - yCoords[ yi ] ) / yw;
        double rz = ( m_vertices[ i ].z() - zCoords[ zi ] ) / zw;

        m_vertices[ i ] = 
        { 
            static_cast<float>( ( 1.0 - rx ) * xCoords[ xi ] + rx * ( xCoords[ xi + 1 ] ) ), 
            static_cast<float>( ( 1.0 - ry ) * yCoords[ yi ] + ry * ( yCoords[ yi + 1 ] ) ), 
            static_cast<float>( ( 1.0 - rz ) * zCoords[ zi ] + rz * ( zCoords[ zi + 1 ] ) )
        };
    }
}

void CubeMarcher::computeNormals()
{
    const size_t NT = m_vertices.size() / 3;
    for( int i = 0; i < NT; ++i )
    {
        TN::Vec3< float > v1 = m_vertices[ i*3 + 0 ];
        TN::Vec3< float > v2 = m_vertices[ i*3 + 1 ];
        TN::Vec3< float > v3 = m_vertices[ i*3 + 2 ];

        const Vec3< float > normal = Vec3< float >::surfaceNormal( v3, v2, v1 ) * m_nMdirection;

        v1 = v1.rounded( 1 );
        v2 = v2.rounded( 1 );
        v3 = v3.rounded( 1 );

        addNormal( v1, normal );
        addNormal( v2, normal );
        addNormal( v3, normal );
    }

    const int SZ = static_cast< int >( m_vertices.size() );
    m_normals.resize( SZ );

    #pragma omp parallel for
    for( int i = 0; i < SZ; ++i )
    {
        std::unordered_map< Vec3< float >, Vec3< float > >::iterator it;
        if( ( it = m_adjacentFaceNormals.find( m_vertices[ i ].rounded( 1 ) ) ) != m_adjacentFaceNormals.end() )
        {
            m_normals[ i ] = it->second;
            m_normals[ i ].normalize();
        }
    }
}


void CubeMarcher::init(
    int z_start,
    int z_end,
    int dimX,
    int dimY,
    int dimZ,
    double targetValue,
    bool indexOrder )
{
    m_indexOrder = indexOrder;

    m_nMdirection = targetValue < 0 ? 1 : -1;
    m_targetValue = targetValue;

    m_vertices.clear();
    m_normals.clear();
    m_adjacentFaceNormals.clear();

    m_dimX = dimX;
    m_dimY = dimY;
    m_dimZ = dimZ;

    m_zstart = z_start;
    m_zend = z_end;
}

void CubeMarcher::init(
    int dimX,
    int dimY,
    int dimZ,
    double targetValue,
    bool indexOrder )
{
    m_indexOrder = indexOrder;

    m_nMdirection = targetValue < 0 ? 1 : -1;
    m_targetValue = targetValue;

    m_vertices.clear();
    m_normals.clear();
    m_adjacentFaceNormals.clear();

    m_dimX = dimX;
    m_dimY = dimY;
    m_dimZ = dimZ;

    m_zstart = 0;
    m_zend = dimZ;
}

CubeMarcher::CubeMarcher() {}

void CubeMarcher::operator()(
    const std::vector< float > & volume,
    int z_start,
    int z_end,
    int dimX,
    int dimY,
    int dimZ,
    double targetValue ) 
{
    init( z_start, z_end,dimX, dimY, dimZ, targetValue, true );
    marchCubes( volume );
}

void CubeMarcher::operator()(
    std::string mode,
    const std::vector< float > & volume,
    int dimX,
    int dimY,
    int dimZ,
    double targetValue )
{
    init( dimX, dimY, dimZ, targetValue, true );
    marchCubes( volume );
}

void CubeMarcher::operator()(
    const std::vector< float > & volume,
    int dimX,
    int dimY,
    int dimZ,
    double targetValue,
    bool indexOrder )
{
    init( dimX, dimY, dimZ, targetValue, indexOrder );
    marchCubes( volume );
    computeNormals();
}

void CubeMarcher::operator()(
    const std::vector< float > & volume,
    const std::vector< uint8_t > & mask,
    int dimX,
    int dimY,
    int dimZ,
    double targetValue,
    bool indexOrder )
{
    init( dimX, dimY, dimZ, targetValue, indexOrder );
    marchCubes( volume, mask );
    computeNormals();
}

void CubeMarcher::operator()(
    const std::vector< float > & volume,
    const std::vector< uint8_t > & mask,
    int dimX,
    int dimY,
    int dimZ,
    int x0,
    int y0,
    int z0,
    int xe,
    int ye,
    int ze,
    double targetValue,
    bool indexOrder )
{
    init( dimX, dimY, dimZ, targetValue, indexOrder );
    marchCubes( volume, mask, x0, y0, z0, xe, ye, ze );
    computeNormals();
}


void CubeMarcher::operator()(
    const std::vector< float > & volume,
    int dimX,
    int dimY,
    int dimZ,
    const std::vector< float > & xCoords,
    const std::vector< float > & yCoords,
    const std::vector< float > & zCoords,        
    double targetValue,
    bool indexOrder )
{
    init( dimX, dimY, dimZ, targetValue, indexOrder );
    marchCubes( volume );
    scaleCoordinates( xCoords, yCoords, zCoords );
    computeNormals();
}

const std::vector< Vec3< float > > & CubeMarcher::getVerts() const
{
    return m_vertices;
}

const std::vector< Vec3< float > > & CubeMarcher::getNormals() const
{
    return m_normals;
}

}
