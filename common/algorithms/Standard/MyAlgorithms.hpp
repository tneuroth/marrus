#ifndef PARALLELALGORITHM_CPP
#define PARALLELALGORITHM_CPP

#include "geometry/Vec.hpp"
#include "utils/IndexUtils.hpp"

#include <vector>
#include <algorithm>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <limits>

namespace TN
{

///////////////////////////////////////////////////////////////////////////////

// Reference implementations for std:: version

template<class InputIt1, class InputIt2, class OutputIt>
OutputIt set_intersection(InputIt1 first1, InputIt1 last1,
                          InputIt2 first2, InputIt2 last2, OutputIt d_first)
{
    while (first1 != last1 && first2 != last2)
    {
        if (*first1 < *first2)
            ++first1;
        else
        {
            if (!(*first2 < *first1))
                *d_first++ = *first1++; // *first1 and *first2 are equivalent.
            ++first2;
        }
    }
    return d_first;
}

template<class InputIt1, class InputIt2, class OutputIt>
OutputIt set_difference(InputIt1 first1, InputIt1 last1,
                        InputIt2 first2, InputIt2 last2, OutputIt d_first)
{
    while (first1 != last1)
    {
        if (first2 == last2)
            return std::copy(first1, last1, d_first);
 
        if (*first1 < *first2)
            *d_first++ = *first1++;
        else
        {
            if (! (*first2 < *first1))
                ++first1;
            ++first2;
        }
    }
    return d_first;
}

/////////////////////////////////////////////////////////////////////////////////

template <typename T>
std::string to_string_with_precision(const T a_value, const int n = 4 )
{
    std::ostringstream out;
    out << std::setprecision(n) << a_value;
    return out.str();
}

template <typename T >
bool arraysEqual( 
    T * a1, 
    T * a2,
    const size_t SZ )
{
    bool equal = true;
    for( int64_t idx = 0; idx < SZ; ++idx )
    {
        if( a1[ idx ] != a2[ idx ] ) {
            equal = false;
            break;
        } 
    }
    return equal;
}

namespace Parallel
{

template< typename T >
T max( const std::vector< T > & v );

template< typename T >
T min( const std::vector< T > & v );

template< typename T >
T maxMag( const std::vector< T > & v );


template < typename T1, typename T2 >
void sortTogetherInBlocks( 
    T1 * v1,
    T2 * v2,
    const size_t SZ,
    const TN::Vec3< int64_t > & dims,
    const TN::Vec3< int64_t > & blkDims,
    const int64_t blockSize )
{
    std::vector< std::size_t > indices( SZ );

    std::vector< T1 > copy1( SZ );
    std::vector< T2 > copy2( SZ );

    #pragma omp parallel for
    for( std::size_t i = 0; i < SZ; ++i )
    {
        indices[ i ] = i;
        copy1[ i ] = v1[ i ];
        copy2[ i ] = v2[ i ];
    }

    std::sort(
        indices.begin(),
        indices.end(),
        [ & ]( const size_t & a, const size_t & b )
    {
        if( v1[ a ] == v1[ b ] )
        {
            auto xyzA = index3dCM( a, dims.x(), dims.y(), dims.z() );
            auto xyzB = index3dCM( b, dims.x(), dims.y(), dims.z() );

            size_t blk1 = flatIndex3dCM( 
                xyzA.x() / blockSize, 
                xyzA.y() / blockSize, 
                xyzA.z() / blockSize,
                blkDims.x(),
                blkDims.y(),
                blkDims.z() );

            size_t blk2 = flatIndex3dCM( 
                xyzB.x() / blockSize, 
                xyzB.y() / blockSize, 
                xyzB.z() / blockSize,                
                blkDims.x(),
                blkDims.y(),
                blkDims.z() );

            return blk1 < blk2;
        }
        else
        {
            return ( v1[ a ] < v1[ b ] );
        }
    }
    );

    #pragma omp parallel for
    for( std::size_t i = 0; i < SZ; ++i )
    {
        v1[ i ] = copy1[ indices[ i ] ];
        v2[ i ] = copy2[ indices[ i ] ];
    }
}

template < typename T1, typename T2 >
void sortTogether( T1 * v1,
                   T2 * v2,
                   const size_t SZ )
{
    std::vector< std::size_t > indices( SZ );

    std::vector< T1 > copy1( SZ );
    std::vector< T2 > copy2( SZ );

    #pragma omp parallel for
    for( std::size_t i = 0; i < SZ; ++i )
    {
        indices[ i ] = i;
        copy1[ i ] = v1[ i ];
        copy2[ i ] = v2[ i ];
    }

    std::sort(
        indices.begin(),
        indices.end(),
        [ & ]( const size_t & a, const size_t & b )
    {
        return ( v1[ a ] < v1[ b ] );
    }
    );

    #pragma omp parallel for
    for( std::size_t i = 0; i < SZ; ++i )
    {
        v1[ i ] = copy1[ indices[ i ] ];
        v2[ i ] = copy2[ indices[ i ] ];
    }
}


template < typename T1, typename T2 >
void sortTogether( std::vector< T1 > & v1,
                   std::vector< T2 > & v2 )
{
    const int SZ = v1.size();

    std::vector< std::size_t > indices( SZ );
    std::vector< T1 > copy1 = v1;
    std::vector< T2 > copy2 = v2;

    #pragma omp parallel for
    for( std::size_t i = 0; i < SZ; ++i )
    {
        indices[ i ] = i;
    }

    std::sort(
        indices.begin(),
        indices.end(),
        [ & ]( const size_t & a, const size_t & b )
    {
        return ( v1[ a ] < v1[ b ] );
    }
    );

    #pragma omp parallel for
    for( std::size_t i = 0; i < SZ; ++i )
    {
        v1[ i ] = copy1[ indices[ i ] ];
        v2[ i ] = copy2[ indices[ i ] ];
    }
}

template < typename POINT_TYPE, typename ID_TYPE >
inline void sortedIndicesXY(
    const std::vector< POINT_TYPE > & x,
    const std::vector< POINT_TYPE > & y,
    std::vector< ID_TYPE > & indices )
{
    const int SZ = x.size();
    indices.resize( SZ );

    #pragma omp parallel for
    for( std::size_t i = 0; i < SZ; ++i )
    {
        indices[ i ] = i;
    }

    std::sort(
        indices.begin(),
        indices.end(),
        [ & ]( const size_t & a, const size_t & b )
    {
        return ( ( x[ a ] < x[ b ] ) || ( x[ a ] == x[ b ] && y[ a ] < y[ b ] ) );
    }
    );
}

}

namespace Sequential
{
    
template< typename T >
T sum( const std::vector< T > & v )
{
    T s( 0 );
    for( size_t i = 0, end = v.size(); i < end; ++i )
    {
        s += v[ i ];
    }
    return s;
}

template< typename T >
TN::Vec2< double > getRange( const T * values, const int64_t N )
{
    Vec2< double > result( std::numeric_limits< double >::max(), -std::numeric_limits< double >::max()  );
    for( size_t i = 0; i < N; ++i )
    {
        result.a( std::min( result.a(), ( double ) values[ i ] ) );
        result.b( std::max( result.b(), ( double ) values[ i ] ) );
    }
    return result;
}

template< typename T >
TN::Vec2< double > getRange( const std::vector< T > & values )
{
    return getRange(  values.data(), values.size() );
}

template< typename T >
T max( const std::vector< T > & v );

template< typename T >
T min( const std::vector< T > & v );

template< typename T >
T maxMag( const std::vector< T > & v );

}

namespace THIS_FILE {
inline TN::Vec3< size_t > location( 
        const size_t idx,
        const size_t X,
        const size_t Y,
        const size_t Z )
{ 
    size_t tmp = idx;
    size_t x = tmp % X;
    tmp = ( tmp - x ) / X;
    size_t y = tmp % Y;
    size_t z  = ( tmp - y ) / Y;
    return { x, y, z };
}
}

template< class T >
inline void smoothVolume(
    std::vector< T >   & voxels,
    size_t X, size_t Y, size_t Z, 
    int radius )
{
    std::vector< T > tmp = voxels;

    #pragma omp parallel for    
    for( size_t idx = 0; idx < X*Y*Z; ++idx )
    {
        voxels[ idx ] = 0;

        const TN::Vec3< size_t > xyz = THIS_FILE::location( idx, X, Y, Z );
        
        const size_t x0 = ( size_t ) std::max( int( xyz.x() ) - radius, 0 );
        const size_t y0 = ( size_t ) std::max( int( xyz.y() ) - radius, 0 );
        const size_t z0 = ( size_t ) std::max( int( xyz.z() ) - radius, 0 );             
         
        const size_t xe = ( size_t ) std::min( int( xyz.x() ) + radius, int( X - 1 ) ); 
        const size_t ye = ( size_t ) std::min( int( xyz.y() ) + radius, int( Y - 1 ) );
        const size_t ze = ( size_t ) std::min( int( xyz.z() ) + radius, int( Z - 1 ) );

        double COUNT = ( ( xe - x0 ) * ( ye - y0 ) * ( ze - z0 ) - 1 ) * 0.5f + 1.f;

        //s = 0.5 / 27 + 1.f / ( 26 * .5 + 1.f )

        for( size_t x = x0; x < xe; ++x )
        {
            for( size_t y = y0; y < ye; ++y )
            {
                for( size_t z = z0; z < ze; ++z )
                {
                    const size_t jdx = x + y*X + z*Y*X;
                    float weight = idx == jdx ? 1.f : 0.5f;
                    voxels[ idx ] += tmp[ x + y*X + z*Y*X ] * weight;                
                }
            }
        }
        voxels[ idx ] /= COUNT;
    }
}

template < class T >
inline void aggregateMeans( 
    const std::vector< T >   & voxels,              // raw field data
    const std::vector< int > & binMap,              // voxel to site index
    std::vector< T >         & binsZeroInitialized, // sites 
    bool normalize )
{
    std::vector< float > counts( binsZeroInitialized.size(), 0.f );
    auto & r = binsZeroInitialized;
    const size_t NV = voxels.size();
    
    for( size_t i = 0; i < NV; ++i )
    {
        if( binMap[ i ] >= 0 )
        {
            r[ binMap[ i ] ] += voxels[ i ];
            counts[ binMap[ i ] ] += 1.f; 
        }
    }

    const size_t N_BINS = binsZeroInitialized.size();

    #pragma omp parallel for
    for( size_t i = 0; i < N_BINS; ++i )
    {
        if( counts[ i ] > 0.f )
        {
            r[ i ] = r[ i ] / counts[ i ];
        }
    }

    if( normalize )
    {
        TN::Vec2< double > range = TN::Sequential::getRange( r );

        if( range.b() > range.a() )
        {
            #pragma omp parallel for
            for( size_t i = 0; i < N_BINS; ++i )
            {
                r[ i ] = ( r[ i ] - range.a() ) / ( range.b() - range.a() );
            }
        }
    }
}

}

#endif // PARALLELALGORITHM_CPP
