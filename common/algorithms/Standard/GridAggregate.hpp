#ifndef TN::GRID_AGGREGATE_HPP
#define TN::GRID_AGGREGATE_HPP

#include <cstdint>
#include <vector>
#include <unordered_set>

namespace TN
{

inline float aggregateSum(
    const std::unordered_set< int64_t > & indices,
    const std::vector< float > & values,
    const std::vector< float > & counts )
{
    double sum = 0;
    double totalCount = 0.0;
    for( const auto & i : indices )
    {
        sum += values[ i ];
        totalCount += 1.0;
    }
    return sum / totalCount;
}

inline float aggregateMin(
    const std::unordered_set< int64_t > & indices,
    const std::vector< float > & values,
    const std::vector< float > & counts )
{
    float mn = std::numeric_limits<float>::max();
    if( indices.size() > 0 )
    {
        for( const auto & i : indices )
        {
            mn = std::min( mn, values[ i ] );
        }
    }
    return mn;
}

inline float aggregateMax(
    const std::unordered_set< int64_t > & indices,
    const std::vector< float > & values,
    const std::vector< float > & counts )
{
    float mx = -std::numeric_limits<float>::max();
    if( indices.size() > 0 )
    {
        for( const auto & i : indices )
        {
            mx = std::max( mx, values[ i ] );
        }
    }
    return mx;
}
inline float aggregateVariance(
    const std::unordered_set< int64_t > & indices,
    const std::vector< float > & values,
    const std::vector< float > & counts )
{
    double sum = 0;
    double totalCount = 0.0;
    for( const auto & i : indices )
    {
        sum += values[ i ] * ( counts[ i ] - 1 );
        totalCount += counts[ i ];
    }
    return sum / totalCount;
}

inline float aggregateRMS(
    const std::unordered_set< int64_t > & indices,
    const std::vector< float > & values,
    const std::vector< float > & counts )
{
    double sum = 0;
    double totalCount = 0.0;
    for( const auto & i : indices )
    {
        sum += values[ i ] * values[ i ] * counts[ i ];
        totalCount += counts[ i ];
    }
    return std::sqrt( sum ) / totalCount;
}

inline float aggregateMean(
    const std::unordered_set< int64_t > & indices,
    const std::vector< float > & values,
    const std::vector< float > & counts )
{
    double sum = 0;
    double totalCount = 0.0;
    for( const auto & i : indices )
    {
        sum += values[ i ] * counts[ i ];
        totalCount += counts[ i ];
    }
    return sum / totalCount;
}

inline float aggregateCount(
    const std::unordered_set< int64_t > & indices,
    const std::vector< float > & values,
    const std::vector< float > & counts )
{
    double totalCount = 0.0;
    for( const auto & i : indices )
    {
        totalCount += counts[ i ];
    }
    return totalCount / indices.size();
}

}

#endif // TN::GRID_AGGREGATE_HPP
