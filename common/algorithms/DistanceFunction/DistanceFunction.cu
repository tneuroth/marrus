
#include "DistanceFunction.hpp"
#include "cuda/utility.h"

#include <cuda.h>

#include <iostream>

__global__ 
void labelSign( 
	const int64_t NV,  
	const float * field,
	const float isovalue,
    float       * result )
{
    const int idx = blockIdx.x * blockDim.x + threadIdx.x;

    if( idx >= NV )
    {
        return;
    }
	
    if( field[ idx ] < isovalue )
    {
    	result[ idx ] = -1;
    }
    else
    {
    	result[ idx ] = 1;
    }
}

__device__ 
long3 index3D( 
    const int64_t idx,
    const int64_t X,
    const int64_t Y,
    const int64_t Z )
{ 
    const long x = idx % X;
    const long tmp = ( idx - x ) / X;
    const long y = tmp % Y;
    const long z  = ( tmp - y ) / Y;

    return 
    { x, y, z };
}

__global__ 
void computeDistance( 
	const int64_t NV,
	const int64_t NX,
	const int64_t NY,
	const int64_t NZ,
	const int64_t NP,	
    const float * verts, 
    const int   * flags,
    const float * xCoords, 
    const float * yCoords, 
    const float * zCoords,        
    float       * result )
{
    const int idx = blockIdx.x * blockDim.x + threadIdx.x;

    if( idx >= NV )
    {
        return;
    }

	long3 vIdx = index3D( idx, NX, NY, NZ );
	
	const float3 vPos = make_float3( 
		xCoords[ vIdx.x ], 
		yCoords[ vIdx.y ], 
		zCoords[ vIdx.z ] ); 

    float minDistSqr = 99999999.0;

    for( int pIdx = 0; pIdx < NP; ++pIdx )
    {
        if( flags[ pIdx ] & 1 == 0 )
        {
        	continue;
        }

		const float3 mPos = make_float3( 
				verts[ pIdx * 3 + 0 ], 
				verts[ pIdx * 3 + 1 ], 
				verts[ pIdx * 3 + 2 ] ); 

        float dsqr = vPos.x*mPos.x + vPos.y*mPos.y + vPos.z*mPos.z;

        if( dsqr < minDistSqr )
        {
            minDistSqr = dsqr;
        }
    }

    result[ idx ] = result[ idx ] * sqrt( minDistSqr );
}

namespace TN
{
    void DistanceFunction::free()
    {
        cudaFree( d_verts );
        cudaFree( d_flags );
        cudaFree( d_xCoords );
        cudaFree( d_yCoords );
        cudaFree( d_zCoords );
        cudaFree( d_reference );
        cudaFree( d_result );

        d_verts = 0;
        d_flags = 0;
        d_xCoords = 0;
        d_yCoords = 0;
        d_zCoords = 0;
        d_reference = 0;
        d_result = 0;
    }

    void DistanceFunction::initialize(
        const std::vector< TN::Vec3< float > > & meshVerts,   
	    const std::vector< int >               & meshBitFlags,
        const std::vector< float > & xCoords,  
	    const std::vector< float > & yCoords,    
        const std::vector< float > & zCoords,
        const std::vector< float > & reference )
    {
    	const size_t NP = meshVerts.size();
    	const size_t NV = xCoords.size() * yCoords.size() * zCoords.size();

        if( m_nPointsAllocated != NP )
        {
            cudaFree( d_verts );
            cudaFree( d_flags );

            gpuErrchk( cudaPeekAtLastError() );

            cudaMalloc( (void **) &d_verts, NP * sizeof( TN::Vec3< float > ) );
            cudaMalloc( (void **) &d_flags, NP * sizeof( int ) );
          

            gpuErrchk( cudaPeekAtLastError() );

            m_nPointsAllocated = NP;
        }

        if( m_nVoxAllocated != NV )
        {
            cudaFree( d_xCoords );
            cudaFree( d_yCoords );
            cudaFree( d_zCoords );
            cudaFree( d_reference );
            cudaFree( d_result  );

            cudaMalloc( (void **) &d_xCoords, xCoords.size() * sizeof( float ) );
            cudaMalloc( (void **) &d_yCoords, yCoords.size() * sizeof( float ) );
            cudaMalloc( (void **) &d_zCoords, zCoords.size() * sizeof( float ) );
            cudaMalloc( (void **) &d_reference, NV * sizeof( float ) );
            cudaMalloc( (void **) &d_result, NV * sizeof( float ) );

            m_nVoxAllocated = NV;

            gpuErrchk( cudaPeekAtLastError() );
        }

        cudaMemcpy( d_verts,    meshVerts.data(), NP * sizeof( TN::Vec3< float > ),    cudaMemcpyHostToDevice );
        cudaMemcpy( d_flags, meshBitFlags.data(), NP * sizeof( int ), cudaMemcpyHostToDevice );          

        cudaMemcpy( d_xCoords, xCoords.data(), xCoords.size() * sizeof( float ), cudaMemcpyHostToDevice );
        cudaMemcpy( d_yCoords, yCoords.data(), yCoords.size() * sizeof( float ), cudaMemcpyHostToDevice );
        cudaMemcpy( d_zCoords, zCoords.data(), zCoords.size() * sizeof( float ), cudaMemcpyHostToDevice );

        cudaMemcpy( d_reference, reference.data(), NV * sizeof( float ), cudaMemcpyHostToDevice );

        gpuErrchk( cudaPeekAtLastError() );   

        cudaDeviceSynchronize();
    }

    void DistanceFunction::compute( 
        const std::vector< TN::Vec3< float > > & meshVerts,    
        const std::vector< int >      & meshBitFlags,
        const std::vector< float > & xCoords,  
        const std::vector< float > & yCoords,  
        const std::vector< float > & zCoords,  
        const std::vector< float > & referenceField,
        float isovalue,
        std::vector< float >       & result,
        bool persist )
    {
    	m_dims = { 
    		static_cast< float >( xCoords.size() ),
    		static_cast< float >( yCoords.size() ),  
    		static_cast< float >( zCoords.size() ) };

        initialize( meshVerts, meshBitFlags, xCoords, yCoords, zCoords, referenceField );

        const int64_t NV = m_dims.x() * m_dims.y() * m_dims.z();

        labelSign<<< gridSize( 256, NV ), 256 >>>(
        	NV,
        	d_reference,
        	isovalue,
        	d_result );

        gpuErrchk( cudaDeviceSynchronize() );

        const int64_t NP = meshVerts.size();

        computeDistance<<< gridSize( 256, NV ), 256 >>>(
            NV,
            m_dims.x(),
            m_dims.y(),
            m_dims.z(),
            NP,
            d_verts,
            d_flags,
            d_xCoords,
            d_yCoords,
            d_zCoords,
            d_result );

        result.resize( NV );

        gpuErrchk( cudaDeviceSynchronize() );

        cudaMemcpy( 
            result.data(), 
            d_result,       
            NV * sizeof( float ), 
            cudaMemcpyDeviceToHost );

        gpuErrchk( cudaDeviceSynchronize() );
 
        if( ! persist )
        {
            free();
        }
    }

    DistanceFunction::~DistanceFunction()
    {
        cudaFree( d_verts );
        cudaFree( d_flags );
        cudaFree( d_xCoords );
        cudaFree( d_yCoords );
        cudaFree( d_zCoords );
        cudaFree( d_reference );
        cudaFree( d_result );
    }
}