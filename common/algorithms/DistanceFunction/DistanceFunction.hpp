#ifndef TN_DISTANCE_FUNCTION_HPP
#define TN_DISTANCE_FUNCTION_HPP

#include "geometry/Vec.hpp"

#include <vector>

namespace TN
{

class DistanceFunction
{
    size_t m_nPointsAllocated;
    size_t m_nVoxAllocated;

    float * d_verts;
    int   * d_flags;
    
    float * d_xCoords;
    float * d_yCoords;    
    float * d_zCoords;

    float * d_reference;
    float * d_result;

    TN::Vec3< unsigned int > m_dims;

    void initialize(
        const std::vector< TN::Vec3< float > > & meshVerts,  
	    const std::vector< int >   & meshBitFlags,
        const std::vector< float > & xCoords,  
	    const std::vector< float > & yCoords,    
        const std::vector< float > & zCoords,
        const std::vector< float > & referenceField );

    public: 

    // assume the positions of the points are in target unit coordinate space
    // flags are potentially needed to account for extinction
    	
    void compute( 
        const std::vector< TN::Vec3< float > > & meshVerts,   
        const std::vector< int >   & meshBitFlags,
        const std::vector< float > & xCoords,  
        const std::vector< float > & yCoords,  
        const std::vector< float > & zCoords,  
        const std::vector< float > & referenceField,
        float isovalue,
        std::vector< float >       & result,
        bool persist = false );

    void free();

    DistanceFunction() : 
        d_verts(     0 ), 
        d_flags(     0 ),
        d_xCoords(   0 ), 
        d_yCoords(   0 ), 
        d_zCoords(   0 ),
        d_reference( 0 ),
        d_result(    0 ) {}

    ~DistanceFunction();
};

}

#endif