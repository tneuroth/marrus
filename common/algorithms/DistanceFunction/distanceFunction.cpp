
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <cstdint>
#include <limits>
#include <atomic>

#include "utils/IndexUtils.hpp"

using namespace std;


void computeDistance( 
	const int64_t NX,
	const int64_t NY,
	const int64_t NZ,
    const std::vector< float > & verts, // normalized to 0-1      
    std::vector< float > & result )
{

	const int64_t NG = X*Y*Z;

	// (1) initialize distance function

	result.resize( NG );
	result.int64_to_fit();
	std::fill( result.begin(), result.end(), std::numeric_limits<float>::max() );

	// (2) assign distance to grid points instersecting the surface 

	const int64_t NV = verts.size() / 3; 
	for( int64_t i = 0; i < NV; ++i )
	{	
		const float x = verts[ i*3 + 0 ];
		const float y = verts[ i*3 + 1 ];
		const float z = verts[ i*3 + 2 ];
		const int64_t ix = std::min( x * X, X-1 );
		const int64_t iy = std::min( y * Y, Y-1 );
		const int64_t iz = std::min( z * Z, Z-1 );				
		const int64_t idx = index3dCM( ix, iy, iz, X, Y, Z );
		result[ idx ] = 0.f;
	}	

	// (3) perform updates through region growing

	const uint8_t UNCLASSIFIED = 0;
	const uint8_t ACTIVE       = 1;
	const uint8_t RESOLVED     = 2;

	std::vector< std::atomic< uint8_t > > status;
	std::vector< std::atomic< int64_t > > nearestPoint;

	const int64_t ntable[ 27 * 3 ] =
	{
	     0,0,0,   0,0,-1,   0,-1,-1,   0,-1,0,   0,-1,1,   0,0,1,   0,1,1,   0,1,0,   0,1,-1,
	    -1,0,0,  -1,0,-1,  -1,-1,-1,  -1,-1,0,  -1,-1,1,  -1,0,1,  -1,1,1,  -1,1,0,  -1,1,-1,
	     1,0,0,   1,0,-1,   1,-1,-1,   1,-1,0,   1,-1,1,   1,0,1,   1,1,1,   1,1,0,   1,1,-1
	};

	bool finished = false;
	int64_t iter = 0;


	while( ! finished )
	{	
		#pragma omp parallel for
		for( int64_t xi = 0; xi < X; ++xi )
		for( int64_t yi = 0; yi < Y; ++yi )
		for( int64_t zi = 0; zi < X; ++zi )
		{
			const int64_t idx = index3dCM( ix, iy, iz, X, Y, Z );

			if( status[ idx ] == RESOLVED )
			{
				continue;
			}
			else if( result[ idx ] < ( iter + 1) * ( iter + 1 ) )
			{
				status[ idx ] = RESOLVED;
				continue;
			}

			for( int64_t i = 0; i < 27; ++i )
			{
				int64_t nx = xi + ntable[ i * 3 + 0 ];
				int64_t ny = yi + ntable[ i * 3 + 1 ];
				int64_t nz = zi + ntable[ i * 3 + 2 ];								
			}

			if( nx < 0 || nx >= X 
			 || ny < 0 || ny >= Y 
			 || nz < 0 || nz >= Z  )

			{
				continue;
			}

			int64_t nidx = index3dCM( nx, ny, nz, X, Y, Z );
			if( status[ nidx ] == UNCLASSIFIED )
			{
				continue;
			}

			const int64_t nvid = nearestPoint[ nidx ];
		
			// distance to point

			float dx = nx - verts[ nvid * 3 + 0 ];
			float dy = ny - verts[ nvid * 3 + 1 ];
			float dz = nz - verts[ nvid * 3 + 2 ];					

			float distq = dx*dx + dy*dy + dz*dz;

			if( distq < result[ idx ] )
			{
				result[ idx ]       = distq;
				status[ idx ]       = ACTIVE;
				nearestPoint[ idx ] = nvid; 
			}
		}

		finished = true;
		for( int64_t i = 0; i < NG; ++i )
		{
			if( status[ i ] != RESOLVED )
			{
				finished = false;
				break;
			}
		}
	}
}