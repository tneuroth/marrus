
#ifndef TN_CC_TEST_HPP_C
#define TN_CC_TEST_HPP_C

#include "algorithms/Standard/MyAlgorithms.hpp"
#include "utils/IndexUtils.hpp"

#include <vector>
#include <array>
#include <set>
#include <cstdint>
#include <iostream>
#include <algorithm>
#include <map>

namespace TN
{

template < typename F_ID_TYPE, typename CC_ID_TYPE >
inline bool verifyComponents( 
    const std::vector< CC_ID_TYPE > & componentIds,
    const std::vector< F_ID_TYPE > & layerIds,
    const std::array< int64_t, 3 > & dims,
    bool verbose = true )
{
    if( verbose ) { std::cout << "verifying connected components result" << std::endl; }

    bool isCorrect = true;

    const int64_t NV = componentIds.size();

    if( componentIds.size() != layerIds.size() )
    {
        if( verbose ) { std::cout << "\tlayer component id size missmatch" << std::endl; }
        exit( 1 );
    }

    auto cmpRange = TN::Sequential::getRange( componentIds );

    if( verbose ) { std::cout << "\tcmp range is " << cmpRange.a() << " " << cmpRange.b() << std::endl; }

    // verify the components are consecutive and add up to NCC

    std::set< int64_t > uniqueCMPs;
    for( size_t i = 0; i < NV; ++i )
    {
        uniqueCMPs.insert( componentIds[ i ] );
    }

    int NCC = uniqueCMPs.size();
    
    
    if( verbose ) {  std::cout << "\tnumber of unique components including 0: " << uniqueCMPs.size() << std::endl; }

    // if( NCC != uniqueCMPs.size() )
    // {
    //     std::cout << "\tNCC != u.size(), " << NCC << " vs " << uniqueCMPs.size() + 1 << std::endl;
    //     isCorrect = false;
    // }

    std::vector< int32_t > cmps;
    cmps.reserve( uniqueCMPs.size() );
    for( auto & cmp : uniqueCMPs )
    {
        cmps.push_back( cmp );
    }

    for( int i = 1; i < cmps.size(); ++i )
    {
        if( ( cmps[ i - 1 ] + 1 ) != cmps[ i ] )
        {
            if( verbose ) {  std::cout << "\tcmps not consecutive" << std::endl; };
            isCorrect = false;
        }
    }

    if( cmps[ 0 ] < 0 )
    {
        if( verbose ) {  std::cout << "\tnegative cmp ids" << std::endl; }
        isCorrect = false;
    }

    if( cmps[ 0 ] != 0 )
    {
        if( verbose ) {  std::cout << "\tNote the domain is all forground" << std::endl; }
    }

    // verify background is not misclassified

    if( verbose ) { std::cout << "\tchecking background" << std::endl; }

    for( size_t i = 0; i < NV; ++i )
    {
        if( layerIds[ i ] == 0 && componentIds[ i ] != 0 )
        {
            std::cout << "\tError: component is not equal to voxel" << std::endl;
            isCorrect = false;
        }

        if( layerIds[ i ] != 0 && componentIds[ i ] == 0 )
        {
            std::cout << "\tError: component is not equal to voxel" << std::endl;
            isCorrect = false;
        }

        if( componentIds[ i ]  < 0 )
        {
            std::cout << "\tError: component id is negative" << std::endl; 
            isCorrect = false;
        }

        if( layerIds[ i ]  < 0 )
        {
            std::cout << "\tError: layer id is negative" << std::endl;
            isCorrect = false;
        }
    }

    if( verbose ) { std::cout << "\tchecking neighbors" << std::endl; }

    for( int64_t i = 0; i < dims[ 0 ]; ++i )
    {
        for( int64_t j = 0; j < dims[ 1 ]; ++j )
        {
            for( int64_t k = 0; k < dims[ 2 ]; ++k )
            {
                int64_t flatIdx = 
                    TN::flatIndex3dCM( 
                        i, j, k,  
                        dims[ 0 ],
                        dims[ 1 ],
                        dims[ 2 ] );
            
                int64_t up = 
                    TN::flatIndex3dCM( 
                        std::max( i-1, 0L ), j, k,  
                        dims[ 0 ],
                        dims[ 1 ],
                        dims[ 2 ] );

                int64_t left = 
                    TN::flatIndex3dCM( 
                        i, std::max( j-1,0L), k,  
                        dims[ 0 ],
                        dims[ 1 ],
                        dims[ 2 ] );                    

                int64_t back = 
                    TN::flatIndex3dCM( 
                        i, j, std::max(k-1,0L ),  
                        dims[ 0 ],
                        dims[ 1 ],
                        dims[ 2 ] );

                // same layer different component

                if( componentIds[ up ] != componentIds[ flatIdx ] 
                &&      layerIds[ up ] == layerIds[ flatIdx ] )
                {
                    std::cout << "\terror: adjacent voxel (up) in same layer has different cmp id" 
                       << i <<  " " << " " << j << " " << k << std::endl;
                    isCorrect = false;
                }
                if( componentIds[ left ] != componentIds[ flatIdx ] 
                &&      layerIds[ left ] == layerIds[ flatIdx ] )
                {
                    std::cout << "\terror: adjacent voxel (left) in same layer has different cmp id"
                       << i <<  " " << " " << j << " " << k << std::endl; 
                    isCorrect = false;
                }
                if( componentIds[ back ] != componentIds[ flatIdx ] 
                &&      layerIds[ back ] == layerIds[ flatIdx ] )
                {
                    std::cout << "\terror: adjacent voxel (back) in same layer has different cmp id"
                       << i <<  " " << " " << j << " " << k << std::endl;
                    isCorrect = false;
                }

                // same component different layer

                if( componentIds[ up ] == componentIds[ flatIdx ] 
                &&      layerIds[ up ] != layerIds[ flatIdx ] )
                {
                    std::cout << "\terror: adjacent voxel (up) in same layer has different cmp id"
                       << i <<  " " << " " << j << " " << k << std::endl; 
                    isCorrect = false;
                }
                if( componentIds[ left ] == componentIds[ flatIdx ] 
                &&      layerIds[ left ] != layerIds[ flatIdx ] )
                {
                    std::cout << "\terror: adjacent voxel (left) in same layer has different cmp id"
                       << i <<  " " << " " << j << " " << k << std::endl;
                    isCorrect = false;
                }
                if( componentIds[ back ] == componentIds[ flatIdx ] 
                &&      layerIds[ back ] != layerIds[ flatIdx ] )
                {
                    std::cout << "\terror: adjacent voxel (back) in same layer has different cmp id"
                       << i <<  " " << " " << j << " " << k << std::endl;  
                    isCorrect = false;                  
                }
            }
        }
    }
    
    if( verbose ) { 
        std::cout << std::string( "Test " ) + ( isCorrect ? "Passed" : "Failed" ) << std::endl; 
    }

    return isCorrect;
}


template < typename F_ID_TYPE, typename CC_ID_TYPE >
inline bool verifyComponents( 
    const std::vector< CC_ID_TYPE > & componentIds,
    const std::vector< F_ID_TYPE > & layerIds,
    const int NCC,
    const std::array< int64_t, 3 > & dims,
    bool verbose = true )
{
    if( verbose ) { std::cout << "verifying connected components result" << std::endl; }

    bool isCorrect = true;

    const int64_t NV = componentIds.size();

    if( componentIds.size() != layerIds.size() )
    {
        if( verbose ) { std::cout << "\tlayer component id size missmatch" << std::endl; }
        exit( 1 );
    }

    auto cmpRange = TN::Sequential::getRange( componentIds );
    
    if( verbose ) { std::cout << "\tcmp range is " << cmpRange.a() << " " << cmpRange.b() << std::endl; }
    if( verbose ) { std::cout << "\tNCC="<< NCC << std::endl; }

    // verify the components are consecutive and add up to NCC

    std::set< int64_t > uniqueCMPs;
    for( size_t i = 0; i < NV; ++i )
    {
        uniqueCMPs.insert( componentIds[ i ] );
    }
    
    if( verbose ) {  std::cout << "\tnumber of unique components including 0: " << uniqueCMPs.size() << std::endl; }

    if( NCC != uniqueCMPs.size() )
    {
        std::cout << "\tNCC != u.size(), " << NCC << " vs " << uniqueCMPs.size() + 1 << std::endl;
        isCorrect = false;
    }

    std::vector< int32_t > cmps;
    cmps.reserve( uniqueCMPs.size() );
    for( auto & cmp : uniqueCMPs )
    {
        cmps.push_back( cmp );
    }

    for( int i = 1; i < cmps.size(); ++i )
    {
        if( ( cmps[ i - 1 ] + 1 ) != cmps[ i ] )
        {
            if( verbose ) {  std::cout << "\tcmps not consecutive" << std::endl; };
            isCorrect = false;
        }
    }

    if( cmps[ 0 ] < 0 )
    {
        if( verbose ) {  std::cout << "\tnegative cmp ids" << std::endl; }
        isCorrect = false;
    }

    if( cmps[ 0 ] != 0 )
    {
        if( verbose ) {  std::cout << "\tNote the domain is all forground" << std::endl; }
    }

    // verify background is not misclassified

    if( verbose ) { std::cout << "\tchecking background" << std::endl; }

    for( size_t i = 0; i < NV; ++i )
    {
        if( layerIds[ i ] == 0 && componentIds[ i ] != 0 )
        {
            std::cout << "\tError: component is not equal to voxel" << std::endl;
            isCorrect = false;
        }

        if( layerIds[ i ] != 0 && componentIds[ i ] == 0 )
        {
            std::cout << "\tError: component is not equal to voxel" << std::endl;
            isCorrect = false;
        }

        if( componentIds[ i ]  < 0 )
        {
            std::cout << "\tError: component id is negative" << std::endl; 
            isCorrect = false;
        }

        if( layerIds[ i ]  < 0 )
        {
            std::cout << "\tError: layer id is negative" << std::endl;
            isCorrect = false;
        }
    }

    if( verbose ) { std::cout << "\tchecking neighbors" << std::endl; }

    for( int64_t i = 0; i < dims[ 0 ]; ++i )
    {
        for( int64_t j = 0; j < dims[ 1 ]; ++j )
        {
            for( int64_t k = 0; k < dims[ 2 ]; ++k )
            {
                int64_t flatIdx = 
                    TN::flatIndex3dCM( 
                        i, j, k,  
                        dims[ 0 ],
                        dims[ 1 ],
                        dims[ 2 ] );
            
                int64_t up = 
                    TN::flatIndex3dCM( 
                        std::max( i-1, 0L ), j, k,  
                        dims[ 0 ],
                        dims[ 1 ],
                        dims[ 2 ] );

                int64_t left = 
                    TN::flatIndex3dCM( 
                        i, std::max( j-1,0L), k,  
                        dims[ 0 ],
                        dims[ 1 ],
                        dims[ 2 ] );                    

                int64_t back = 
                    TN::flatIndex3dCM( 
                        i, j, std::max(k-1,0L ),  
                        dims[ 0 ],
                        dims[ 1 ],
                        dims[ 2 ] );

                // same layer different component

                if( componentIds[ up ] != componentIds[ flatIdx ] 
                &&      layerIds[ up ] == layerIds[ flatIdx ] )
                {
                    std::cout << "\terror: adjacent voxel (up) in same layer has different cmp id" 
                       << i <<  " " << " " << j << " " << k << std::endl;
                    isCorrect = false;
                }
                if( componentIds[ left ] != componentIds[ flatIdx ] 
                &&      layerIds[ left ] == layerIds[ flatIdx ] )
                {
                    std::cout << "\terror: adjacent voxel (left) in same layer has different cmp id"
                       << i <<  " " << " " << j << " " << k << std::endl; 
                    isCorrect = false;
                }
                if( componentIds[ back ] != componentIds[ flatIdx ] 
                &&      layerIds[ back ] == layerIds[ flatIdx ] )
                {
                    std::cout << "\terror: adjacent voxel (back) in same layer has different cmp id"
                       << i <<  " " << " " << j << " " << k << std::endl;
                    isCorrect = false;
                }

                // same component different layer

                if( componentIds[ up ] == componentIds[ flatIdx ] 
                &&      layerIds[ up ] != layerIds[ flatIdx ] )
                {
                    std::cout << "\terror: adjacent voxel (up) in same layer has different cmp id"
                       << i <<  " " << " " << j << " " << k << std::endl; 
                    isCorrect = false;
                }
                if( componentIds[ left ] == componentIds[ flatIdx ] 
                &&      layerIds[ left ] != layerIds[ flatIdx ] )
                {
                    std::cout << "\terror: adjacent voxel (left) in same layer has different cmp id"
                       << i <<  " " << " " << j << " " << k << std::endl;
                    isCorrect = false;
                }
                if( componentIds[ back ] == componentIds[ flatIdx ] 
                &&      layerIds[ back ] != layerIds[ flatIdx ] )
                {
                    std::cout << "\terror: adjacent voxel (back) in same layer has different cmp id"
                       << i <<  " " << " " << j << " " << k << std::endl;  
                    isCorrect = false;                  
                }
            }
        }
    }
    
    if( verbose ) { 
        std::cout << std::string( "Test " ) + ( isCorrect ? "Passed" : "Failed" ) << std::endl; 
    }

    return isCorrect;
}

template < typename F_ID_TYPE, typename CC_ID_TYPE >
inline bool verifyComponents( 
    const CC_ID_TYPE * componentIds,
    const F_ID_TYPE  * layerIds,
    const int NCC,
    const std::array< int64_t, 3 > & dims,
    bool verbose = false )
{
    if( verbose ) { std::cout << "verifying connected components result" << std::endl; }

    bool isCorrect = true;

    const int64_t NV = dims[ 0 ] * dims[ 1 ] * dims[ 2 ];

    auto cmpRange = TN::Sequential::getRange( componentIds, NV );
    
    if( verbose ) { std::cout << "\tcmp range is " << cmpRange.a() << " " << cmpRange.b() << std::endl; }
    if( verbose ) { std::cout << "\tcmp range is " << cmpRange.a() << " " << cmpRange.b() << std::endl; }
    if( verbose ) { std::cout << "\tNCC="<< NCC << std::endl; }

    // verify the components are consecutive and add up to NCC

    std::set< int64_t > uniqueCMPs;
    for( size_t i = 0; i < NV; ++i )
    {
        uniqueCMPs.insert( componentIds[ i ] );
    }
    
    if( verbose ) { std::cout << "\tnumber of unique components including 0: " << uniqueCMPs.size() << std::endl; }

    if( NCC != uniqueCMPs.size() )
    {
        if( verbose ) { std::cout << "\tNCC != u.size(), " << NCC << " vs " << uniqueCMPs.size() + 1 << std::endl; }
        isCorrect = false;
    }

    std::vector< int32_t > cmps;
    cmps.reserve( uniqueCMPs.size() );
    for( auto & cmp : uniqueCMPs )
    {
        cmps.push_back( cmp );
    }

    for( int i = 1; i < cmps.size(); ++i )
    {
        if( ( cmps[ i - 1 ] + 1 ) != cmps[ i ] )
        {
            if( verbose ) { std::cout << "\tcmps not consecutive" << std::endl; }
            isCorrect = false;
        }
    }

    if( cmps[ 0 ] < 0 )
    {
        if( verbose ) { std::cout << "\tnegative cmp ids" << std::endl; }
        isCorrect = false;
    }

    if( cmps[ 0 ] != 0 )
    {
        if( verbose ) { std::cout << "\tNote the domain is all forground" << std::endl; }
    }

    // verify background is not misclassified

    if( verbose ) { std::cout << "\tchecking background" << std::endl; }

    for( size_t i = 0; i < NV; ++i )
    {
        if( layerIds[ i ] == 0 && componentIds[ i ] != 0 )
        {
            std::cout << "\tError: component is not equal to voxel" << std::endl;
            isCorrect = false;
        }

        if( layerIds[ i ] != 0 && componentIds[ i ] == 0 )
        {
            std::cout << "\tError: component is not equal to voxel" << std::endl;
            isCorrect = false;
        }

        if( componentIds[ i ]  < 0 )
        {
            std::cout << "\tError: component id is negative" << std::endl; 
            isCorrect = false;
        }

        if( layerIds[ i ]  < 0 )
        {
            std::cout << "\tError: layer id is negative" << std::endl;
            isCorrect = false;
        }
    }

    if( verbose ) { std::cout << "\tchecking neighbors" << std::endl; }

    for( int64_t i = 0; i < dims[ 0 ]; ++i )
    {
        for( int64_t j = 0; j < dims[ 1 ]; ++j )
        {
            for( int64_t k = 0; k < dims[ 2 ]; ++k )
            {
                int64_t flatIdx = 
                    TN::flatIndex3dCM( 
                        i, j, k,  
                        dims[ 0 ],
                        dims[ 1 ],
                        dims[ 2 ] );
            
                int64_t up = 
                    TN::flatIndex3dCM( 
                        std::max( i-1, 0L ), j, k,  
                        dims[ 0 ],
                        dims[ 1 ],
                        dims[ 2 ] );

                int64_t left = 
                    TN::flatIndex3dCM( 
                        i, std::max( j-1,0L), k,  
                        dims[ 0 ],
                        dims[ 1 ],
                        dims[ 2 ] );                    

                int64_t back = 
                    TN::flatIndex3dCM( 
                        i, j, std::max(k-1,0L ),  
                        dims[ 0 ],
                        dims[ 1 ],
                        dims[ 2 ] );

                // same layer different component

                if( componentIds[ up ] != componentIds[ flatIdx ] 
                &&      layerIds[ up ] == layerIds[ flatIdx ] )
                {
                    std::cout << "\terror: adjacent voxel (up) in same layer has different cmp id" 
                       << i <<  " " << " " << j << " " << k << std::endl;
                    isCorrect = false;
                }
                if( componentIds[ left ] != componentIds[ flatIdx ] 
                &&      layerIds[ left ] == layerIds[ flatIdx ] )
                {
                    std::cout << "\terror: adjacent voxel (left) in same layer has different cmp id"
                       << i <<  " " << " " << j << " " << k << std::endl; 
                    isCorrect = false;
                }
                if( componentIds[ back ] != componentIds[ flatIdx ] 
                &&      layerIds[ back ] == layerIds[ flatIdx ] )
                {
                    std::cout << "\terror: adjacent voxel (back) in same layer has different cmp id"
                       << i <<  " " << " " << j << " " << k << std::endl;
                    isCorrect = false;
                }

                // same component different layer

                if( componentIds[ up ] == componentIds[ flatIdx ] 
                &&      layerIds[ up ] != layerIds[ flatIdx ] )
                {
                    std::cout << "\terror: adjacent voxel (up) in same layer has different cmp id"
                       << i <<  " " << " " << j << " " << k << std::endl; 
                    isCorrect = false;
                }
                if( componentIds[ left ] == componentIds[ flatIdx ] 
                &&      layerIds[ left ] != layerIds[ flatIdx ] )
                {
                    std::cout << "\terror: adjacent voxel (left) in same layer has different cmp id"
                       << i <<  " " << " " << j << " " << k << std::endl;
                    isCorrect = false;
                }
                if( componentIds[ back ] == componentIds[ flatIdx ] 
                &&      layerIds[ back ] != layerIds[ flatIdx ] )
                {
                    std::cout << "\terror: adjacent voxel (back) in same layer has different cmp id"
                       << i <<  " " << " " << j << " " << k << std::endl;  
                    isCorrect = false;                  
                }
            }
        }
    }
    
    if( verbose ) { 
        std::cout << std::string( "Test " ) + ( isCorrect ? "Passed" : "Failed" ) << std::endl; 
    }

    return isCorrect;
}

}

#endif