#ifndef TN_IMAGE_LOADER_HPP
#define TN_IMAGE_LOADER_HPP

#include "geometry/Vec.hpp"

#include <opencv2/opencv.hpp>

#include <string>
#include <vector>
#include <limits>
#include <cmath>
#include <iostream>

namespace TN
{

inline void loadRGBA( 
    const std::string & filePath,
    std::vector< float > & data,
    TN::Vec2< int > & dims )
{
    cv::Mat image = cv::imread( filePath, cv::IMREAD_UNCHANGED );

    data.resize( image.cols * image.rows * 4 );
   
    dims.x( image.cols );
    dims.y( image.rows );

    if( image.channels() == 4 )
    {
        for (int i = 0; i < image.cols; ++i)
        {
            for ( int j = 0; j < image.rows; ++j )
            {
                int row = image.rows - j - 1;
                data[ image.cols * row * 4 + i * 4 + 2  ] = image.at<cv::Vec4b>( j, i )[ 0 ] / 255.f;
                data[ image.cols * row * 4 + i * 4 + 1 ] = image.at<cv::Vec4b>( j, i )[ 1 ] / 255.f;
                data[ image.cols * row * 4 + i * 4 + 0 ] = image.at<cv::Vec4b>( j, i )[ 2 ] / 255.f;            
                data[ image.cols * row * 4 + i * 4 + 3 ] = image.at<cv::Vec4b>( j, i )[ 3 ] / 255.f;
            }
        }
    }
    else if( image.channels() == 3 )
    {
        for (int i = 0; i < image.cols; ++i)
        {
            for ( int j = 0; j < image.rows; ++j )
            {
                int row = image.rows - j - 1;
                data[ image.cols * row * 4 + i * 4 + 2 ] = image.at<cv::Vec3b>( j, i )[ 0 ] / 255.f;
                data[ image.cols * row * 4 + i * 4 + 1 ] = image.at<cv::Vec3b>( j, i )[ 1 ] / 255.f;
                data[ image.cols * row * 4 + i * 4 + 0 ] = image.at<cv::Vec3b>( j, i )[ 2 ] / 255.f;            
                data[ image.cols * row * 4 + i * 4 + 3 ] = 1.f;
            }
        }  
    }
}

// static inline void loadImage( 
// 	const std::string & filePath,
// 	std::vector< float > & data,
// 	TN::Vec2< int > & dims,
// 	int rescale = 1 )
// {
//     cv::Mat image = cv::imread( filePath );
//     cv::cvtColor(image, image, cv::COLOR_BGR2GRAY);

//     data.resize( image.cols*image.rows );
   
//     dims.x( image.cols );
//     dims.y( image.rows );

//     std::cout << dims.x() << ", " << dims.y() << std::endl;

//     float mx = 0;
//     for (int i = 0; i < image.cols; ++i)
//     {
//         for ( int j = 0; j < image.rows; ++j )
//         {
//         	int row = image.rows - j - 1;
//             data[ image.cols * row + i ] = (float)image.at<uchar>( j, i );
//             mx = std::max( mx, data[ image.cols * j + i ] );
//         }
//     }
 
//     if( mx > 0 )
//     {
// 	    for( auto & val : data )
// 	    {
// 	    	val = val / mx;
// 	    }
//     }

//     if( rescale != 1 )
//     {
//         std::vector< float > rescaled;
//     	TN::rescale(
// 	       data, 
// 	       dims.x(), 
// 	       dims.y(),
// 	       rescale,
// 	       rescaled );
    
// 	    data = rescaled;
// 	    dims.x( dims.x() * rescale );
// 	    dims.y( dims.y() * rescale );
//     }
// }

}

#endif