#ifndef TN_PROCESS_HPP
#define TN_PROCESS_HPP

#include <opencv2/opencv.hpp>
#include <vector>

namespace TN
{

static inline void rescale(
    const std::vector< float > & image, 
    const int width, 
    const int height,
    const int scaleFactor,
    std::vector< float > & rescaled )
{
    cv::Mat cvImage( cv::Size(  width, height ), CV_32FC1 );
    cv::Mat cvImage2( cv::Size( scaleFactor*width, scaleFactor*height ), CV_32FC1 );    

    for (int i = 0; i < width; ++i)
    {
        for ( int j = 0; j < height; ++j )
        {
            cvImage.at< float >( j, i ) = image[ width * j + i ];
        }
    }

    cv::resize( cvImage, cvImage2, cv::Size( width * scaleFactor, height * scaleFactor ), 0, 0, cv::INTER_CUBIC );
    rescaled.resize( width * scaleFactor * height * scaleFactor );
 
    for (int i = 0; i < width * scaleFactor; ++i)
    {
        for ( int j = 0; j < height * scaleFactor; ++j )
        {
            rescaled[ width * scaleFactor * j + i ] = cvImage2.at< float >( j, i );
        }
    }
}

}

#endif