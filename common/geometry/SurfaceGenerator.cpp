
#include "algorithms/IsoContour/CubeMarcher.hpp"
#include "geometry/Vec.hpp"
#include "utils/IndexUtils.hpp"

#include <iomanip>
#include <fstream>
#include <string>
#include <iostream>
#include <limits>
#include <cmath>
#include <cstring>
#include <cstdint>

void indexTriangles(
    const std::vector< TN::Vec3< float > > & verts,
    std::vector< TN::Vec3< float > > & indexed,
    std::vector< int > & indices )
{
    indexed.resize( verts.size() );
    indices.resize( verts.size() );

    for( int i = 0; i < verts.size(); ++i )
    {
        indexed[ i ] = verts[ i ];
    }

    indices = std::vector< int >( verts.size() );
    for( int i = 0; i < verts.size(); i )
    {
        indices[ i ] = i;
    }
}

void writeSurfaceMeshOBJ(
    const std::vector< TN::Vec3< float > > & verts )//,
//const std::vector< TN::Vec3< float > > & normals )
{
    std::cout << "saving mesh" << std::endl;

    std::vector< TN::Vec3< float > > indexed;
    std::vector< int > indices;

    indexTriangles( verts, indexed, indices );

    std::ofstream outFile( "surface.obj" );
    for( int i = 0; i < verts.size(); ++i )
    {
        outFile << "v ";
        outFile << indexed[ i ].x() << " ";
        outFile << indexed[ i ].y() << " ";
        outFile << indexed[ i ].z() << "\n";
    }

    outFile << "\n";

    for( int i = 0; i < indices.size(); i += 3 )
    {
        outFile << "f ";
        outFile << indices[ i     ] << " ";
        outFile << indices[ i + 1 ] << " ";
        outFile << indices[ i + 2 ] << "\n";
    }
    outFile.close();
}

void writeSurfaceMeshSTL(
    const std::vector< TN::Vec3< float > > & verts,
    std::vector< TN::Vec3< float > > & normals,
    const std::string & name )
{
// UINT8[80] – Header
// UINT32 – Number of triangles

// foreach triangle
// REAL32[3] – Normal vector
// REAL32[3] – Vertex 1
// REAL32[3] – Vertex 2
// REAL32[3] – Vertex 3
// UINT16 – Attribute byte count
// end

    std::cout << "saving mesh" << std::endl;

    std::string header_info = "solid surface.stl-output";
    char header[80];
    std::strncpy( header, header_info.c_str(), sizeof( header ) - 1 );

    uint32_t N_TRI = verts.size() / 3;

    auto outFile = std::fstream( name + ".stl", std::ios::out | std::ios::binary );
    outFile.write( (char*) header, sizeof( uint8_t ) * 80 );
    outFile.write( (char*) & N_TRI, sizeof( uint32_t ) );

    uint16_t attributeCount = 0;

    for( int i = 0; i < verts.size(); i += 3 )
    {
        TN::Vec3< float > surfaceNormal = ( normals[ i ] + normals[ i + 1 ] + normals[ i + 3 ] ) * ( 1.0 / 3.0 );
        outFile.write( (char*) & surfaceNormal,  sizeof( float )  * 3 );
        outFile.write( (char*) & verts[ i     ], sizeof( float )  * 3 );
        outFile.write( (char*) & verts[ i + 1 ], sizeof( float )  * 3 );
        outFile.write( (char*) & verts[ i + 2 ], sizeof( float )  * 3 );
        outFile.write( (char*) & attributeCount, sizeof( uint16_t ) );
    }

    outFile.close();
}

void computeIsosurface(
    const float isoValue,
    const std::vector< float > & volume,
    const std::vector< uint8_t > & mask,    
    const TN::Vec3< int >      & dims,
    const TN::Vec3< int >      & start, 
    const TN::Vec3< int >      & end,        
    std::vector< TN::Vec3< float > > & verts,
    std::vector< TN::Vec3< float > > & norms,
    bool normalize )
{
    TN::CubeMarcher marcher;
    
    marcher( volume, mask, 
        dims.x(), dims.y(), dims.z(), 
        start.x(), start.y(), start.z(),
        end.x(), end.y(), end.z(),        
        isoValue, true );

    verts = std::vector< TN::Vec3< float > >( marcher.getVerts() );
    norms = std::vector< TN::Vec3< float > >( marcher.getNormals() );

    const double NM = std::max( std::max( dims.x(), dims.y() ), dims.z() );
    const size_t NV = verts.size();

    // scale the volume so that it fits in -1,1 bounding box

    if( normalize ) 
    {
        #pragma omp parallel for
        for( size_t i = 0; i < NV; ++i )
        {
            TN::Vec3< float > & v = verts[ i ];
            v =
            {
                static_cast<float>( v.x() / ( double ) ( NM ) - 0.5 * ( dims.x() / NM ) ),
                static_cast<float>( v.y() / ( double ) ( NM ) - 0.5 * ( dims.y() / NM ) ),
                static_cast<float>( v.z() / ( double ) ( NM ) - 0.5 * ( dims.z() / NM ) )
            };
        }
    }
}

void computeIsosurface(
    const float isoValue,
    const std::vector< float > & volume,
    const std::vector< uint8_t > & mask,    
    const TN::Vec3< int >      & dims,
    const TN::Vec3< int >      & start, 
    const TN::Vec3< int >      & end,        
    const TN::Vec3< int >      & scaling, 
    std::vector< TN::Vec3< float > > & verts,
    std::vector< TN::Vec3< float > > & norms,
    bool normalize )
{
    TN::CubeMarcher marcher;
    
    marcher( volume, mask, 
        dims.x(), dims.y(), dims.z(), 
        start.x(), start.y(), start.z(),
        end.x(), end.y(), end.z(),        
        isoValue, true );

    verts = std::vector< TN::Vec3< float > >( marcher.getVerts()   );
    norms = std::vector< TN::Vec3< float > >( marcher.getNormals() );

    const double NM = std::max( std::max( scaling.x(), scaling.y() ), scaling.z() );
    const size_t NV = verts.size();

    // scale the volume so that it fits in -1,1 bounding box

    if( normalize ) 
    {
        #pragma omp parallel for
        for( size_t i = 0; i < NV; ++i )
        {
            TN::Vec3< float > & v = verts[ i ];
            v =
            {
                static_cast<float>( v.x() / ( double ) ( NM ) - 0.5 * ( dims.x() / NM ) ),
                static_cast<float>( v.y() / ( double ) ( NM ) - 0.5 * ( dims.y() / NM ) ),
                static_cast<float>( v.z() / ( double ) ( NM ) - 0.5 * ( dims.z() / NM ) )
            };
        }
    }
}

void computeIsosurface(
    const float isoValue,
    const std::vector< float > & volume,
    const std::vector< uint8_t > & mask,    
    const TN::Vec3< int >      & dims,
    std::vector< TN::Vec3< float > > & verts,
    std::vector< TN::Vec3< float > > & norms,
    bool normalize )
{
    TN::CubeMarcher marcher;
    
    marcher( volume, mask, dims.x(), dims.y(), dims.z(), isoValue, true );

    verts = std::vector< TN::Vec3< float > >( marcher.getVerts() );
    norms = std::vector< TN::Vec3< float > >( marcher.getNormals() );

    const double NM = std::max( std::max( dims.x(), dims.y() ), dims.z() );
    const size_t NV = verts.size();

    // scale the volume so that it fits in -1,1 bounding box

    if( normalize ) 
    {
        #pragma omp parallel for
        for( size_t i = 0; i < NV; ++i )
        {
            TN::Vec3< float > & v = verts[ i ];
            v =
            {
                static_cast<float>( v.x() / ( double ) ( NM ) - 0.5 * ( dims.x() / NM ) ),
                static_cast<float>( v.y() / ( double ) ( NM ) - 0.5 * ( dims.y() / NM ) ),
                static_cast<float>( v.z() / ( double ) ( NM ) - 0.5 * ( dims.z() / NM ) )
            };
        }
    }
}

void computeIsosurface(
    const float isoValue,
    const std::vector< float > & volume,
    const TN::Vec3< int >      & dims,
    std::vector< TN::Vec3< float > > & verts,
    bool normalize )
{
    TN::CubeMarcher marcher;
    
    marcher( volume, dims.x(), dims.y(), dims.z(), isoValue, true );
    verts = std::vector< TN::Vec3< float > >( marcher.getVerts() );

    const double NM = std::max( std::max( dims.x(), dims.y() ), dims.z() );
    const size_t NV = verts.size();

    // scale the volume so that it fits in -1,1 bounding box

    if( normalize ) 
    {
        #pragma omp parallel for
        for( size_t i = 0; i < NV; ++i )
        {
            TN::Vec3< float > & v = verts[ i ];
            v =
            {
                static_cast<float>( v.x() / ( double ) ( NM ) - 0.5 * ( dims.x() / NM ) ),
                static_cast<float>( v.y() / ( double ) ( NM ) - 0.5 * ( dims.y() / NM ) ),
                static_cast<float>( v.z() / ( double ) ( NM ) - 0.5 * ( dims.z() / NM ) )
            };
        }
    }
}

void computeIsosurface(
    const float isoValue,
    const std::vector< float > & volume,
    const TN::Vec3< int >      & dims,
    std::vector< TN::Vec3< float > > & verts,
    std::vector< TN::Vec3< float > > & norms,
    bool normalize )
{
    TN::CubeMarcher marcher;
    
    marcher( volume, dims.x(), dims.y(), dims.z(), isoValue, true );

    verts = std::vector< TN::Vec3< float > >( marcher.getVerts() );
    norms = std::vector< TN::Vec3< float > >( marcher.getNormals() );

    const double NM = std::max( std::max( dims.x(), dims.y() ), dims.z() );
    const size_t NV = verts.size();

    // scale the volume so that it fits in -1,1 bounding box

    if( normalize ) 
    {
        #pragma omp parallel for
        for( size_t i = 0; i < NV; ++i )
        {
            TN::Vec3< float > & v = verts[ i ];
            v =
            {
                static_cast<float>( v.x() / ( double ) ( NM ) - 0.5 * ( dims.x() / NM ) ),
                static_cast<float>( v.y() / ( double ) ( NM ) - 0.5 * ( dims.y() / NM ) ),
                static_cast<float>( v.z() / ( double ) ( NM ) - 0.5 * ( dims.z() / NM ) )
            };
        }
    }
}

void computeIsosurface(
    const float isoValue,
    const std::vector< float > & volume,
    const TN::Vec3< int > & dims,
    const std::vector< float > & xCoords,
    const std::vector< float > & yCoords,
    const std::vector< float > & zCoords,
    std::vector< TN::Vec3< float > > & verts,
    std::vector< TN::Vec3< float > > & norms )
{
    TN::CubeMarcher marcher;
    marcher( volume, dims.x(), dims.y(), dims.z(), xCoords, yCoords, zCoords, isoValue, true );
    verts = std::vector< TN::Vec3< float > >( marcher.getVerts() );
    norms = std::vector< TN::Vec3< float > >( marcher.getNormals() );
}
