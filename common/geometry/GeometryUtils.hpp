#ifndef TN_GEOMETRY_UTILS
#define TN_GEOMETRY_UTILS

#include "geometry/Vec.hpp"
#include "geometry/SurfaceGenerator.hpp"
#include "geometry/TrilinearInterpolator.hpp"
#include "utils/IndexUtils.hpp"
#include "algorithms/Standard/MyAlgorithms.hpp"

#include <fstream>
#include <vector>


// #include "voro++.hh"

// inline void extractEdges(
//     const std::vector< TN::Vec3< float > > & samplePoints,
//     TN::Vec2< float > XR,
//     TN::Vec2< float > YR,
//     TN::Vec2< float > ZR,
//     std::vector< float > & eX,
//     std::vector< float > & eY,
//     std::vector< float > & eZ,
//     std::vector< int   > & cellOffsets,
//     std::vector< int   > & cellSizes )
// {
//     eX.clear();
//     eY.clear();
//     eZ.clear();
//     cellOffsets.clear();
//     cellSizes.clear();

//     voro::container con(
//         XR.a(),
//         XR.b(),
//         YR.a(),
//         YR.b(),
//         ZR.a(),
//         ZR.b(),
//         1,
//         1,
//         1,
//         false,
//         false,
//         false,
//         8 );

//     for( size_t i = 0; i < samplePoints.size(); ++i )
//     {
//         con.put(
//             i,
//             samplePoints[ i ].x(),
//             samplePoints[ i ].y(),
//             samplePoints[ i ].z() );
//     }

//     voro::c_loop_all cla( con );
//     voro::voronoicell c;

//     double x, y, z, r, rx, ry, rz;
//     int cellId;

//     // FILE *f2 = voro::safe_fopen( "./find_voro_cell_v.gnu", "w" );

//     std::cout << "writing voronoi File" << std::endl;

//     std::vector< float > edgePoints;
//     std::vector< int   > lines;
//     int next = 0;

//     int idx = 0;


//     if( cla.start() )
//     {
//         do
//         {
//             if ( con.compute_cell( c,cla ) )
//             {
//                 cla.pos( cellId, x, y, z,  r );
//                 c.getEdges( x, y, z, next, edgePoints, lines, idx==0 );

//                 if( idx == 0 )
//                 {
//                     for( int i = 0; i < lines.size(); ++i )
//                     {
//                         std::cout << lines[ i ] << " ";
//                     }
//                     std::cout << std::endl;
//                 }

//                 ++idx;
//             }
//         } while ( cla.inc() );
//     }

//     std::ofstream edgeFile( "edges.obj" );
//     for( int i = 0; i < edgePoints.size() / 3; ++i )
//     {
//         edgeFile << "v " << edgePoints[ i*3 ] << " " << edgePoints[ i*3+1 ] << " " << edgePoints[ i*3+2 ] << "\n";
//     }
//     edgeFile << "\n";

//     for( int i = 0; i < lines.size() / 2; ++i )
//     {
//         edgeFile << "l " << lines[ i*2 ] << " " << lines[ i*2 + 1 ] << "\n";
//     }
//     edgeFile.close();


//     FILE *f2 = voro::safe_fopen( "./edges.gnu", "w" );
//     if( cla.start() )
//     {
//         do
//         {
//             if ( con.compute_cell( c,cla ) )
//             {
//                 cla.pos( cellId, x, y, z,  r );
//                 c.draw_gnuplot( x, y, z, f2 );
//             }
//         } while ( cla.inc() );
//     }
//     fclose( f2 );
// }

inline void extractOutlineVolume(
    const std::vector< int32_t > & classifiction,
    const std::vector< int16_t > & layerIds,
    int layerIndex,
    const std::vector< float > & field,
    int X,
    int Y,
    int Z,
    std::vector< float > & result )
{
    result = std::vector< float >( classifiction.size(), 0.f );

    #pragma omp parallel for
    for( int i = 1; i < X-1; i+=1 )
    {
        for( int j = 1; j < Y-1; j+=1 )
        {
            for( int k = 1; k < Z-1; k+=1 )
            {
                int adj = 1;

                bool classEdge = false;
                bool layerEdge = false;

                int cl  = classifiction[ TN::flatIndex3dCM( i, j, k, X, Y, Z ) ];
                int lid = layerIds[ TN::flatIndex3dCM( i, j, k, X, Y, Z ) ];

                if( lid == layerIndex )
                {
                    for( int ii = std::max(i-1,0); ii < std::min(i+1,X-1); ++ii )
                    {
                        for( int jj = std::max(j-1,0); jj < std::min(j+1,Y-1); ++jj )
                        {
                            for( int kk = std::max(k-1,0); kk < std::min(k+1,Z-1); ++kk )
                            {
                                int a = classifiction[ TN::flatIndex3dCM( ii, jj, kk, X, Y, Z ) ];
                                int l = layerIds[      TN::flatIndex3dCM( ii, jj, kk, X, Y, Z ) ];

                                if( l <= 0 )
                                {
                                    continue;
                                }

                                if( a != cl )
                                {
                                    classEdge = true;
                                }

                                if( l != lid )
                                {
                                    layerEdge = true;
                                }
                            }
                        }
                    }
                    if( classEdge )
                    {
                        result[ TN::flatIndex3dCM( i, j, k, X, Y, Z ) ] = 0.0;
                    }
                    else
                    {
                        result[ TN::flatIndex3dCM( i, j, k, X, Y, Z ) ] = 1.0;
                    }
                }
            }
        }
    }

    // TN::smoothVolume( tmp, X, Y, Z, 1 );
}

inline void extractOutlineVolume(
    const std::vector< int32_t > & classifiction,
    const std::vector< int16_t > & layerIds,
    const std::vector< float > & field,
    int X,
    int Y,
    int Z,
    std::vector< float > & result )
{
    result = std::vector< float >( classifiction.size(), 0.f );

    #pragma omp parallel for
    for( int i = 1; i < X-1; i+=1 )
    {
        for( int j = 1; j < Y-1; j+=1 )
        {
            for( int k = 1; k < Z-1; k+=1 )
            {
                int adj = 1;

                bool classEdge = false;
                bool layerEdge = false;

                int cl  = classifiction[ TN::flatIndex3dCM( i, j, k, X, Y, Z ) ];
                int lid = layerIds[ TN::flatIndex3dCM( i, j, k, X, Y, Z ) ];

                if( lid >= 1 )
                {
                    for( int ii = std::max(i-1,0); ii < std::min(i+1,X-1); ++ii )
                    {
                        for( int jj = std::max(j-1,0); jj < std::min(j+1,Y-1); ++jj )
                        {
                            for( int kk = std::max(k-1,0); kk < std::min(k+1,Z-1); ++kk )
                            {
                                int a = classifiction[ TN::flatIndex3dCM( ii, jj, kk, X, Y, Z ) ];
                                int l = layerIds[      TN::flatIndex3dCM( ii, jj, kk, X, Y, Z ) ];

                                if( l <= 0 )
                                {
                                    continue;
                                }

                                if( a != cl )
                                {
                                    classEdge = true;
                                }

                                if( l != lid )
                                {
                                    layerEdge = true;
                                }
                            }
                        }
                    }
                    if( layerEdge )
                    {
                        result[ TN::flatIndex3dCM( i, j, k, X, Y, Z ) ] = lid + 1;
                    }
                    else if( classEdge )
                    {
                        result[ TN::flatIndex3dCM( i, j, k, X, Y, Z ) ] = lid + 1 + 0.5;
                    }
                    else
                    {
                        result[ TN::flatIndex3dCM( i, j, k, X, Y, Z ) ] = lid + 1;
                    }
                }
            }
        }
    }

    // TN::smoothVolume( tmp, X, Y, Z, 1 );
}

inline void extractDiscreteGeometry(
    const std::vector< int32_t > & classifiction,
    const std::vector< int16_t > & layerIds,
    int layer,
    const std::vector< float > & field,
    size_t X,
    size_t Y,
    size_t Z,
    std::vector< TN::Vec3< float > > & verts,
    std::vector< TN::Vec3< float > > & norms,
    bool save )
{
    auto tmp = std::vector< float >( classifiction.size(), 1.f );

    #pragma omp parallel for
    for( size_t i = 1; i < X - 1; i+=1 )
    {
        for( size_t j = 1; j < Y - 1; j+=1 )
        {
            for( size_t k = 1; k < Z - 1; k+=1 )
            {
                int adj = 1;
                bool edge = false;
                int cl = classifiction[ TN::flatIndex3dCM( i, j, k, X, Y, Z ) ];
                for( size_t ii = i-1; ii < i+1; ++ii )
                {
                    for( size_t jj = j - 1; jj < j+1; ++jj )
                    {
                        for( size_t kk = k-1; kk < k + 1; ++kk )
                        {

                            if( ( layerIds[ TN::flatIndex3dCM(  i,  j,  k, X, Y, Z )  ] - 1 ) != layer
                                    && ( layerIds[ TN::flatIndex3dCM( ii, jj, kk, X, Y, Z )  ] - 1 ) != layer )
                            {
                                continue;
                            }

                            int a = classifiction[ TN::flatIndex3dCM( ii, jj, kk, X, Y, Z ) ];
                            if( a != cl )
                            {
                                // if( adj >= -1 )
                                // {
                                //     if(  a != adj )
                                //     {
                                edge = true;
                                // }
                                // }
                                // else
                                // {
                                //     adj = a;
                                // }
                            }
                        }
                    }
                }
                if( edge )
                {
                    tmp[ TN::flatIndex3dCM( i, j, k, X, Y, Z ) ] = 0.f;
                }
            }
        }
    }

    // TN::smoothVolume( tmp, X, Y, Z, 1 );

    verts.clear();
    norms.clear();

    computeIsosurface(
        0.5,
        tmp,
        { static_cast<int>( X ), static_cast<int>( Y ), static_cast<int>( Z ) },
        verts,
        norms );

    if( save )
    {
        writeSurfaceMeshSTL( verts, norms, "results2/cells." + std::to_string( layer ) );
    }
}

inline void extractDiscreteGeometry(
    const std::vector< int32_t > & classifiction,
    const std::vector< int16_t > & layerIds,
    int layer,
    const std::vector< float > & field,
    size_t X,
    size_t Y,
    size_t Z,
    const std::pair< TN::Vec3< float >, TN::Vec3< float > > & extent,
    const std::vector< float > & xCoords,
    const std::vector< float > & yCoords,
    const std::vector< float > & zCoords,
    std::vector< TN::Vec3< float > > & verts,
    std::vector< TN::Vec3< float > > & norms,
    std::vector< float >             & scalars,
    bool save )
{
    auto tmp = std::vector< float >( classifiction.size(), 1.f );

    #pragma omp parallel for
    for( size_t i = 1; i < X - 1; i+=1 )
    {
        for( size_t j = 1; j < Y - 1; j+=1 )
        {
            for( size_t k = 1; k < Z - 1; k+=1 )
            {
                int adj = 1;
                bool edge = false;
                int cl = classifiction[ TN::flatIndex3dCM( i, j, k, X, Y, Z ) ];
                for( size_t ii = i-1; ii < i+1; ++ii )
                {
                    for( size_t jj = j - 1; jj < j+1; ++jj )
                    {
                        for( size_t kk = k-1; kk < k + 1; ++kk )
                        {

                            if( ( layerIds[ TN::flatIndex3dCM(  i,  j,  k, X, Y, Z )  ] - 1 ) != layer
                                    && ( layerIds[ TN::flatIndex3dCM( ii, jj, kk, X, Y, Z )  ] - 1 ) != layer )
                            {
                                continue;
                            }

                            int a = classifiction[ TN::flatIndex3dCM( ii, jj, kk, X, Y, Z ) ];
                            if( a != cl )
                            {
                                // if( adj >= -1 )
                                // {
                                //     if(  a != adj )
                                //     {
                                edge = true;
                                // }
                                // }
                                // else
                                // {
                                //     adj = a;
                                // }
                            }
                        }
                    }
                }
                if( edge )
                {
                    tmp[ TN::flatIndex3dCM( i, j, k, X, Y, Z ) ] = 0.f;
                }
            }
        }
    }

    // TN::smoothVolume( tmp, X, Y, Z, 1 );

    verts.clear();
    norms.clear();

    computeIsosurface(
        0.5,
        tmp,
    { static_cast<int>( X ), static_cast<int>( Y ), static_cast<int>( Z ) },
    xCoords,
    yCoords,
    zCoords,
    verts,
    norms );

    double NORM = std::max( std::max( X, Y ), Z );
    triInt(
        field,
        X,
        Y,
        Z,
        extent,
        verts,
        scalars );

    if( save )
    {
        writeSurfaceMeshSTL( verts, norms, "results2/cells." + std::to_string( layer ) );
    }
}

inline void savePointsOBJ(
    const std::vector< TN::Vec3< float > > & points,
    const std::vector< int16_t > & layerIds,
    int whichLayer,
    const std::string & name )
{
    std::ofstream file( name );
    file << "o points\n";
    for( int i = 0; i < points.size(); ++i )
    {
        if( layerIds[ i ] == whichLayer || whichLayer == -1 )
        {
            file << "v " << points[ i ].y() << " " << points[ i ].x() << " "  << points[ i ].z() << std::endl;
        }
    }
    file.close();
}

inline void loadColors( std::vector< TN::Vec3< float > > & result, int nColors, const std::string & tf )
{
    std::cout << "loading colors: " << tf << ", n=" << nColors << std::endl;

    std::ifstream inFile( tf );
    std::string line;
    std::vector< TN::Vec3< float > > colors;

    while( std::getline( inFile, line ) )
    {
        std::string rgb[ 3 ];
        for( int i = 1, cr = 0; i < line.size() && cr < 3; ++i )
        {
            if( line[i] == ',' )
            {
                ++cr;
                continue;
            }
            else if( line[ i ] == ')' )
            {
                break;
            }
            else if( line[ i ] == ' ' )
            {
                continue;
            }
            else
            {
                rgb[ cr ].push_back( line[ i ] );
            }
        }

        colors.push_back( TN::Vec3< float >(
                              std::stof( rgb[ 0 ] ),
                              std::stof( rgb[ 1 ] ),
                              std::stof( rgb[ 2 ] ) ) );
    }

    result.resize( nColors );
    for( int i = 0; i < nColors; ++i )
    {
        result[ i ] = colors[ i * ( colors.size() / nColors ) ];
    }
}

template < typename T >
bool inline insideTri( TN::Vec2< T > v1, TN::Vec2< T > v2, TN::Vec2< T > v3, TN::Vec2< T > p )
{

    bool b1, b2, b3;

    b1 = sign( p.x(), p.y(), v1, v2 ) <= 0.f;
    b2 = sign( p.x(), p.y(), v2, v3 ) <= 0.f;
    b3 = sign( p.x(), p.y(), v3, v1 ) <= 0.f;

    return ( ( b1 == b2 ) && (b2 == b3 ) );
}


template < typename T >
inline int intersect( TN::Vec2< T > A1, TN::Vec2< T > B1, TN::Vec2< T > A2, TN::Vec2< T > B2, TN::Vec2< T > &intersection )
{
    double denom = ((B2.y() - A2.y() )*( B1.x() - A1.x())) -
                   ((B2.x() -  A2.x() ) *( B1.y() - A1.y()));

    double nume_a = ((B2.x() - A2.x())*(A1.y() - A2.y() ) ) -
                    ((B2.y() - A2.y())*(A1.x() - A2.x() ) );

    double nume_b = ((B1.x() - A1.x())*(A1.y() - A2.y() ) ) -
                    ((B1.y() - A1.y())*(A1.x() - A2.x() ) );

    if( std::abs( denom ) < 1.f )
    {
        if(nume_a == 0.0f && nume_b == 0.0f)
        {
            return -1;
        }
        return -1;
    }

    double ua = nume_a / denom;
    double ub = nume_b / denom;

    if(ua >= 0.0f && ua <= 1.0f && ub >= 0.0f && ub <= 1.0f)
    {
        // Get the intersection point.
        intersection.x( A1.x() + ua*(B1.x() - A1.x() ) );
        intersection.y( A1.y() + ua*(B1.y() - A1.y() ) );

        return 1;
    }

    return -1;
}

template < typename T >
inline int pnpoly( const std::vector< TN::Vec2< T > > & vert, TN::Vec2< T > test )
{
    int i, j, c = 0, nvert = vert.size();
    for (i = 0, j = nvert-1; i < nvert; j = i++)
    {
        if ( ( ( vert[i].y() > test.y() ) != (vert[j].y() > test.y() ) ) &&
                ( test.x() < ( vert[j].x() - vert[i].x() ) * ( test.y() - vert[i].y() ) / ( vert[j].y() - vert[i].y() ) + vert[i].x() ) )
            c = !c;
    }
    return c;
}


#endif