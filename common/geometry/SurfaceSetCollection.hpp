#ifndef TN_ISOSURFACE_COLLECTION_HPP
#define TN_ISOSURFACE_COLLECTION_HPP

#include "configuration/IsosurfaceSetConfiguration.hpp"
#include "geometry/Vec.hpp"
#include "geometry/SurfaceGenerator.hpp"
#include "algorithms/Standard/MyAlgorithms.hpp"

#include <vector>
#include <map>
#include <string>
#include <iostream>

namespace TN {
	
struct SurfaceMesh {

	std::string name;
	float value;
	
	std::vector< TN::Vec3< float > > verts;
	std::vector< TN::Vec3< float > > norms;

	std::vector< float > scalars;
	TN::Vec2<    float > scalarExtrema;
	
	TN::Vec4 color;

	SurfaceMesh() : 
		color( { 0.5, 0.5, 0.5, 1.0 } ) {}
};

class SurfaceSet {

public:
	std::map< std::string, SurfaceMesh > surfaces;
	virtual ~SurfaceSet() = default;
};

class IsosurfaceSet : public SurfaceSet {

public :

	IsosurfaceSetConfiguration config;

	void compute(
		const std::vector< float > & data,
		const TN::Vec3<      int > & dims,
	 	const float valueShift = 0.f,
		const float valueNorm  = 1.f )
	{
		surfaces.clear();
		
		for( size_t i = 0, end = config.isovalues.size(); i < end; ++i ) 
		{
			// adding zeros ensures order is preserved in the map
			std::string zeros = std::to_string( i ); 
			zeros = std::string( 2 - zeros.size(), '0' ) + zeros;

			std::string key   = zeros + " isovalue " + TN::to_string_with_precision( config.isovalues[ i ], 4 );

			surfaces.insert( { key, SurfaceMesh() } );
			SurfaceMesh & surface = surfaces.at( key );

			surface.value         = config.isovalues[ i ];
			surface.color         = config.colors[ i ]; 
			surface.name          = key;

			float val = ( surface.value - valueShift ) / valueNorm;
        	auto datR = TN::Sequential::getRange( data );

			computeIsosurface(
		    	val,
		    	data,
		    	dims,
		    	surface.verts,
		    	surface.norms,
		    	false );
		}
	}

	IsosurfaceSet() {}
	IsosurfaceSet( const IsosurfaceSetConfiguration & cf ) 
		: config( cf )
	{}

	virtual ~IsosurfaceSet() = default;
};

class SurfaceSetCollection {
	
	std::map< std::string, std::unique_ptr< SurfaceSet > > m_surfaceSets;

	public :

	void addIsosurfaceSet( 
		const IsosurfaceSetConfiguration & config, 
		const std::vector< float > & data,
		const TN::Vec3< int > & dims,
		const float valueShift = 0.f,
		const float valueNorm  = 1.f)
	{
		std::string key = config.name;
		if( m_surfaceSets.count( key ) )
		{
			std::cerr << "Error: Map Key Conflict, Isosurface Set key: " << key << ", is already in use. Ignoring." << std::endl;
			return;
		}
		else 
		{
			m_surfaceSets.insert( { key, std::unique_ptr< IsosurfaceSet >( new IsosurfaceSet( config ) ) } ); 

			if( m_surfaceSets.count( key ) == 0 )
			{
				std::cout << "Error: isosurface set key " << key << "not found" << std::endl;
			}

			auto ist = static_cast< IsosurfaceSet * >( m_surfaceSets.at( key ).get() ); 
			ist->compute( data, dims, valueShift, valueNorm );
		}
	}

	std::unique_ptr< SurfaceSet > & createSurfaceSet( const std::string & name )
	{
		m_surfaceSets.insert( { name, std::unique_ptr< SurfaceSet >( new SurfaceSet() ) } );
		return m_surfaceSets.at( name );
	}

	void removeSurfaceSet( const std::string & key  )
	{
		if( m_surfaceSets.count( key ) )
		{
			auto it = m_surfaceSets.find( key );
			m_surfaceSets.erase( it );
		}
	}

	bool hasSet( const std::string & key ) const { 
		return m_surfaceSets.count( key ) != 0;
	}

	const std::unique_ptr< SurfaceSet > & getSurfaceSet( const std::string & key ) const
	{
		if( m_surfaceSets.count( key ) )
		{
			return m_surfaceSets.at( key );
		}
		else
		{
			std::cerr << "Error in SurfaceSetCollection: requested surface set: " << key << ", which was not found." << std::endl;
			exit( 1 );
		}
	}

	const std::map< std::string, std::unique_ptr< SurfaceSet > > & surfaceSets() const 
	{
		return m_surfaceSets;
	}

	void clear()
	{
		m_surfaceSets.clear();
	}

	~SurfaceSetCollection() {}
};

}

#endif