#ifndef TN_SURF_GEN_HPP
#define TN_SURF_GEN_HPP

#include <vector>
#include <cstdint>
#include <string>

#include "geometry/Vec.hpp"

void indexTriangles(
    const std::vector< TN::Vec3< float > > & verts,
    std::vector< TN::Vec3< float > > & indexed,
    std::vector< int > & indices );

void writeSurfaceMeshOBJ(
    const std::vector< TN::Vec3< float > > & verts );

void writeSurfaceMeshSTL(
    const std::vector< TN::Vec3< float > > & verts,
    std::vector< TN::Vec3< float > > & normals,
    const std::string & name );

void computeIsosurface(
    const float relativeIsoValue,
    const std::vector< float > & volume,
    const TN::Vec3< int > & dims,
    std::vector< TN::Vec3< float > > & verts,
    bool normalize = false );

void computeIsosurface(
    const float relativeIsoValue,
    const std::vector< float > & volume,
    const TN::Vec3< int > & dims,
    std::vector< TN::Vec3< float > > & verts,
    std::vector< TN::Vec3< float > > & norms,
    bool normalize = false );

void computeIsosurface(
    const float relativeIsoValue,
    const std::vector< float > & volume,
    const std::vector< uint8_t > & mask,    
    const TN::Vec3< int > & dims,
    std::vector< TN::Vec3< float > > & verts,
    std::vector< TN::Vec3< float > > & norms,
    bool normalize = false );

void computeIsosurface(
    const float relativeIsoValue,
    const std::vector< float > & volume,
    const TN::Vec3< int > & dims,
    const std::vector< float > & xCoords,
    const std::vector< float > & yCoords,
    const std::vector< float > & zCoords,
    std::vector< TN::Vec3< float > > & verts,
    std::vector< TN::Vec3< float > > & norms );

void computeIsosurface(
    const float isoValue,
    const std::vector< float > & volume,
    const std::vector< uint8_t > & mask,    
    const TN::Vec3< int >      & dims,
    const TN::Vec3< int >      & start, 
    const TN::Vec3< int >      & end,        
    std::vector< TN::Vec3< float > > & verts,
    std::vector< TN::Vec3< float > > & norms,
    bool normalize = false );

void computeIsosurface(
    const float isoValue,
    const std::vector< float > & volume,
    const std::vector< uint8_t > & mask,    
    const TN::Vec3< int >      & dims,
    const TN::Vec3< int >      & start, 
    const TN::Vec3< int >      & end,        
    const TN::Vec3< int >      & scaling, 
    std::vector< TN::Vec3< float > > & verts,
    std::vector< TN::Vec3< float > > & norms,
    bool normalize = false );


#endif