#ifndef TN_TRILINEAR_INTERPOLATOR_HPP
#define TN_TRILINEAR_INTERPOLATOR_HPP

#include "geometry/Vec.hpp"
#include "utils/IndexUtils.hpp"

#include <vector>
#include <cmath>
#include <limits>
#include <cstdlib>

// vec4 resampleGradientAndDensity( vec3 position )
// {
//     float sample = texture( samplerUnit, position ).r;

//     vec3 step = 1.0 / size;
//     vec3 sample0, sample1;

//     sample0.x = texture( samplerUnit,vec3( position.x - step.x, position.y, position.z ) ).r;
//     sample1.x = texture( samplerUnit,vec3( position.x + step.x, position.y, position.z ) ).r;
//     sample0.y = texture( samplerUnit,vec3( position.x, position.y - step.y, position.z ) ).r;
//     sample1.y = texture( samplerUnit,vec3( position.x, position.y + step.y, position.z ) ).r;
//     sample0.z = texture( samplerUnit,vec3( position.x, position.y, position.z - step.z ) ).r;
//     sample1.z = texture( samplerUnit,vec3( position.x, position.y, position.z + step.z ) ).r;

//     vec3 scaledPosition = position * size - 0.5;
//     vec3 fraction = scaledPosition - floor(scaledPosition);
//     vec3 correctionPolynomial = (fraction * (fraction - 1.0)) / 2.0;

//     sample += dot((sample0 - sample * 2.0 + sample1),correctionPolynomial);

//     return vec4( normalize( sample1 - sample0 ), sample );
// }


template <typename T>
inline double triInt(
    const std::vector< T > & volume,
    const size_t X,
    const size_t Y,
    const size_t Z,
    const std::pair< TN::Vec3< float >, TN::Vec3< float > > & extent,
    const TN::Vec3< double > &  point )
{
    T x = ( (  point.x() - extent.first.x() ) / ( extent.second.x() - extent.first.x() ) ) * X;
    T y = ( (  point.y() - extent.first.y() ) / ( extent.second.y() - extent.first.y() ) ) * Y;
    T z = ( (  point.z() - extent.first.z() ) / ( extent.second.z() - extent.first.z() ) ) * Z;

    int xi = std::max( std::min( ( int ) std::floor( x ),  (int) X-2 ), (int) 0 );
    int yi = std::max( std::min( ( int ) std::floor( y ),  (int) Y-2 ), (int) 0 );
    int zi = std::max( std::min( ( int ) std::floor( z ),  (int) Z-2 ), (int) 0 );

    double dx = x - xi;
    double dy = y - yi;
    double dz = z - zi;

    return
          ( 1.0 - dx ) * (  1.0 - dy ) * ( 1.0 - dz ) * volume[ TN::flatIndex3dCM( xi,     yi,     zi,     X, Y, Z ) ]
        + (       dx ) * (  1.0 - dy ) * ( 1.0 - dz ) * volume[ TN::flatIndex3dCM( xi + 1, yi,     zi,     X, Y, Z ) ]
        + ( 1.0 - dx ) * (        dy ) * ( 1.0 - dz ) * volume[ TN::flatIndex3dCM( xi,     yi + 1, zi,     X, Y, Z ) ]
        + (       dx ) * (        dy ) * ( 1.0 - dz ) * volume[ TN::flatIndex3dCM( xi + 1, yi + 1, zi,     X, Y, Z ) ]
        + ( 1.0 - dx ) * (  1.0 - dy ) * (       dz ) * volume[ TN::flatIndex3dCM( xi,     yi,     zi + 1, X, Y, Z ) ]
        + (       dx ) * (  1.0 - dy ) * (       dz ) * volume[ TN::flatIndex3dCM( xi + 1, yi,     zi + 1, X, Y, Z ) ]
        + ( 1.0 - dx ) * (        dy ) * (       dz ) * volume[ TN::flatIndex3dCM( xi,     yi + 1, zi + 1, X, Y, Z ) ]
        + (       dx ) * (        dy ) * (       dz ) * volume[ TN::flatIndex3dCM( xi + 1, yi + 1, zi + 1, X, Y, Z ) ];
}

template <typename T>
inline void triInt(
    const std::vector< T > & volume,
    const size_t X,
    const size_t Y,
    const size_t Z,
    const std::pair< TN::Vec3< float >, TN::Vec3< float > > & extent,
    const std::vector< TN::Vec3< T > > & points,
    std::vector< T > & results )
{
    const size_t NPTS = points.size();
    results.resize( NPTS );

    TN::Vec3< double > d = { 1.0 / X, 1.0 / Y, 1.0 / Z  };
    TN::Vec3< double > sample0;
    TN::Vec3< double > sample1;

    #pragma omp parallel for
    for( size_t i = 0; i < NPTS; ++i )
    {
        const TN::Vec3< double > p = { points[ i ].x(), points[ i ].y(), points[ i ].z() };
        double sample = ( double ) triInt( volume, X, Y, Z, extent, p );
        results[ i ] = sample;
    }
}

template <typename T>
inline void triInt(
    const std::vector< T > & volume,
    const size_t X,
    const size_t Y,
    const size_t Z,
    const std::vector< float > & xCoords,
    const std::vector< float > & yCoords,
    const std::vector< float > & zCoords,
    const std::pair< TN::Vec3< float >, TN::Vec3< float > > & extent,
    const std::vector< TN::Vec3< T > > & points,
    std::vector< T > & results )
{
    const size_t NPTS = points.size();
    results.resize( NPTS );

    TN::Vec3< double > d = { 1.0 / X, 1.0 / Y, 1.0 / Z  };
    TN::Vec3< double > sample0;
    TN::Vec3< double > sample1;

    #pragma omp parallel for
    for( size_t i = 0; i < NPTS; ++i )
    {
        const TN::Vec3< double > p = { points[ i ].x(), points[ i ].y(), points[ i ].z() };
        double sample = ( double ) triInt( volume, X, Y, Z, extent, p );
        results[ i ] = sample;
    }
}

#endif