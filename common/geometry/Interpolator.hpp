#ifndef TN_INTERPOLATOR_HPP
#define TN_INTERPOLATOR_HPP

#include <vector>
#include <cstdint>

template < class T >
static inline void interpolate(
    int N_INTERMEDIATES,
    const std::vector< T > & values1,
    const std::vector< T > & values2,
    std::vector< std::vector< T > > & intermediates )
{
    const int64_t SIZE = values1.size();
    intermediates.resize( N_INTERMEDIATES );

    double dr = 1.0 / ( N_INTERMEDIATES + 1 );
    for( int i = 1; i < N_INTERMEDIATES + 1; ++i )
    {
        double r = dr * i;
        intermediates[ i - 1 ].resize( SIZE );

        #pragma omp parallel for
        for( int64_t j = 0; j < SIZE; ++j )
        {
            intermediates[ i -  1] [ j ] = values1[ j ] *( 1 - r )  + values2[ i ] * r;
        }
    }
}

template < class T >
static inline void interpolate(
    int N_INTERMEDIATES,
    const std::vector< T > & values1,
    const std::vector< T > & values2,
    std::vector< std::vector< T > > & intermediates,
    int64_t startIndex ) // assumed to be allocated already
{
    const int64_t SIZE = values1.size();
    intermediates.resize( N_INTERMEDIATES );

    double dr = 1.0 / ( N_INTERMEDIATES + 1 );
    for( int i = 1; i < N_INTERMEDIATES + 1; ++i )
    {
        double r = dr * i;
        intermediates[ i - 1 ].resize( SIZE );

        #pragma omp parallel for
        for( int64_t j = 0; j < SIZE; ++j )
        {
            intermediates[ i -  1] [ j ] = values1[ j ] *( 1 - r )  + values2[ i ] * r;
        }
    }
}

template < class T >
static inline void interpolate(
    int N_INTERMEDIATES,
    const std::vector< T> & values1,
    const std::vector< T > & values2,
    const std::vector< int64_t > & permutation,
    std::vector< std::vector< T > > & intermediates, // assumed to be allocated already
    int64_t startIndex )
{
    const int64_t SIZE = values1.size();

    double dr = 1.0 / ( N_INTERMEDIATES + 1 );
    for( int i = 1, rIdx = startIndex; i < N_INTERMEDIATES + 1; ++i, ++rIdx )
    {
        double r = dr * i;
        intermediates[ rIdx ].resize(  SIZE );

        #pragma omp parallel for
        for( int64_t j = 0; j < SIZE; ++j )
        {
            intermediates[ rIdx ][ j ] = values1[ j ] * ( 1.0 - r ) + values2[ permutation[ j ] ] * ( r );
        }
    }
}

#endif