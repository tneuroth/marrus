#ifndef TN_COMPONENT_SURFACE_EXTRACTOR_HPP
#define TN_COMPONENT_SURFACE_EXTRACTOR_HPP

#include "geometry/SurfaceGenerator.hpp"

template< typename D_TYPE, typename CMP_ID_TYPE, typename BAND_ID_TYPE >
static inline void extractComponentMeshes(
	float isovalue,
	D_TYPE * data,
	CMP_ID_TYPE  * cmp_voxel_ids,
	BAND_ID_TYPE * band_voxel_ids )
{
	
}

#endif