#!/bin/bash

#################################################################################################
# gets the path that this script is in

customrealpath() {
    [[ $1 = /* ]] && echo "$1" || echo "$PWD/${1#./}"
}
SCRIPT=`customrealpath $0`
THIS_PATH=`dirname $SCRIPT`
BASE_PATH=$THIS_PATH

g++ -std=c++17 -O3 -fopenmp -frounding-math -w -fPIC \
    -I$BASE_PATH/src/ \
    -I$BASE_PATH/../thirdParty/ \
    -I$BASE_PATH/../common/ \
    -I$BASE_PATH/../tessellation/include \
    -I$BASE_PATH/../summarize/ \
    -o $BASE_PATH/lib/libmarruswkfl.so --shared \
    $BASE_PATH/src/MarrusWorkflowParallelLib.cpp

cp $BASE_PATH/src/MarrusWorkflowParallelLib.hpp $BASE_PATH/include/
