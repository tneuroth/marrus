#ifndef TN_WORKFLOW_PROTO_UTILS_HPP
#define TN_WORKFLOW_PROTO_UTILS_HPP

#include <string>
#include <cstdint>
#include <vector>
#include <fstream>
#include <iostream>

inline
std::string makePath( const std::string & DIR, const int ts, const int nzero )
{
	std::string tstr = std::to_string( ts );
    tstr = std::string( nzero - tstr.size(), '0' ) + tstr;

	return DIR + "ptj_nr." 
			+ tstr
			+ "_temp_432x480x320_float32.raw";
}

template< typename FT >
inline
void writeData( 
    const std::string & path,
    const int64_t N, 
    const FT * data )
{
    std::ofstream outFile( path );

    if( ! outFile.is_open() )
    {
        std::cerr << "Couldn't open file: " << path << std::endl;
        exit( 1 );
    }

    outFile.write( (char*) data, N * sizeof( FT ) );
    outFile.close();
}

template< typename FT >
inline
void loadData( 
    const std::string & DIR,
	const int ts, 
    const int nzero,
	const int64_t N, 
	const int offset,
	FT * result )
{
    const std::string path = makePath( DIR, ts, nzero );
	std::ifstream inFile( path );

    if( ! inFile.is_open() )
    {
        std::cerr << "Couldn't open file: " << path << std::endl;
        exit( 1 );
    }

	inFile.seekg( N * sizeof( FT )  * offset );
	inFile.read( (char*) result, N * sizeof( FT ) );
	inFile.close();
}

template < typename FT, typename IDT >
inline
void mapLayers(
    const int64_t N,
    const FT * fieldData,
    const std::vector< std::pair< float, float > > & layers,
    IDT * result )
{
    for( int64_t idx = 0; idx < N; ++ idx ) {
        const FT s = fieldData[ idx ];
        result[ idx ] = 0;
        for( size_t l = 0, end = layers.size(); l < end; ++l ) {
            const FT la = layers[ l ].first;
            const FT lb = layers[ l ].second;
            if( s >= la && s < lb  ) {
                result[ idx ] = l + 1;
                break;
            }
        }
    }
}

#endif