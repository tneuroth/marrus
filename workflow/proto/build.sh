#!/bin/bash

mpic++ -O3 -fopenmp -I../../common/ -I../../ -I../ main_serial.cpp   -o ./bin/proto_track_serial
mpic++ -O3 -fopenmp -I../../common/ -I../../ -I../ main_parallel.cpp -o ./bin/proto_track_parallel
mpic++ -O3 -fopenmp -I../../common/ -I../../ -I../ main_ooc.cpp		 -o ./bin/proto_track_ooc
