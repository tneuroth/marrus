
#include "proto_utils.hpp"
#include "algorithms/ConnectedComponentsCPU.hpp"
#include "feature_tracking/src/ParallelRegionBasedFeatureTrackingCartesianGrid.hpp"

#include <mpi.h>

#include <memory>
#include <string>
#include <cstdint>
#include <iostream>
#include <chrono>

using namespace std;

#define timef(f, task) { \
    auto start = chrono::steady_clock::now(); \
    f; \
    auto time = chrono::duration_cast< chrono::milliseconds>( chrono::steady_clock::now() - start ); \
    cout << task << " took " << time.count() << " ms" << endl; \
}

/*___________________________________________________________________________________*/

// Parameters

typedef float  FieldType;
typedef int32_t CcIdType;
typedef int32_t LayerIdType;

const int DIMS[ 3 ]     = { 432, 480, 320 };
const int64_t N         = DIMS[ 0 ] * DIMS[ 1 ] * DIMS[ 2 ];
const string INPUT_DIR  = "/media/ubuntu/LaCie/feature_tracking_data/extracted/";
const int FIELD_OFFSET  = 0;
const string OUTPUT_DIR =  "/media/ubuntu/LaCie/feature_tracking_data/output_serial/";

const double NORM_FACTOR = 120.0;

const vector< pair< float, float> > LAYERS = { 
	// {    0,               1000 / NORM_FACTOR },
	// { 1000 / NORM_FACTOR, 1500 / NORM_FACTOR },
	// { 1500 / NORM_FACTOR, 1800 / NORM_FACTOR },
	{ 1800 / NORM_FACTOR, 9999 / NORM_FACTOR }
};

/*___________________________________________________________________________________*/

int main( int argc, char ** argv ) 
{
    /*_______________________________________________________________________________*/

    MPI_Init( NULL, NULL );

    int nRanks;
    int rank;    
    const MPI_Comm COMM = MPI_COMM_WORLD;

    MPI_Comm_size( COMM, & nRanks );
    MPI_Comm_rank( COMM, & rank   );

    /*_______________________________________________________________________________*/

	unique_ptr<   FieldType[] >      data( new   FieldType[ N ] );
    unique_ptr< LayerIdType[] >  layerIds( new LayerIdType[ N ] );
    unique_ptr<    CcIdType[] >     ccIds( new    CcIdType[ N ] );

    TN::ParallelRegionBasedFeatureTrackingCartesianGrid< CcIdType > featureTracker;

    featureTracker.init( 
    	nRanks,            // nRanks
    	rank,              // my rank
    	MPI_COMM_WORLD,    // COMM
    	true );            // log timings ?

	for( int ts = 1; ts <= 549; ++ts )
	{
        /////////////////////////////////////////////////////////////////////////////
        // load data 

        cout << "ts=" << ts << endl;

        timef( loadData( INPUT_DIR, ts, 10, N, FIELD_OFFSET, data.get() ), "Loading data" );

        /////////////////////////////////////////////////////////////////////////////

        timef( mapLayers( N, data.get(), LAYERS, layerIds.get() ), "Map layers" );

        /////////////////////////////////////////////////////////////////////////////
        // compute components

		int32_t num_components;

        timef(
            TN::connectedComponents( 
                layerIds.get(),
                { DIMS[ 0 ], DIMS[ 1 ], DIMS[ 2 ] },    
                N / 512, // expected upper bounds on number of components
                ccIds.get(),
                num_components ),
            "Connected components" );

        /////////////////////////////////////////////////////////////////////////////
        // Write components

        timef(
            writeData( OUTPUT_DIR + "ccids." + to_string( ts ) + ".bin", N, ccIds.get() ), 
            "Write component ids" );

        /////////////////////////////////////////////////////////////////////////////
        // Verify components

        timef(
        bool valid = TN::verifyComponents(
            ccIds.get(),
            layerIds.get(),
            num_components,
            { DIMS[ 0 ], DIMS[ 1 ], DIMS[ 2 ] },
            true ), 
            "Verify components " );

        /////////////////////////////////////////////////////////////////////////////
        // Track components

        timef(
        featureTracker.onStep( N, ccIds.get() ),
        "Tracking step" );

        if( ts > 1 ) 
        {

        /////////////////////////////////////////////////////////////////////////////
        // Write tracking data

            timef(
        		featureTracker.writeGlobal( 
        			OUTPUT_DIR, 
        			"cellular", 
        			ts ), "Write tracking data" );
        }

        cout << endl;
	}
}
