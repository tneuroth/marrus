
import numpy as np
from collections import Counter

INPUT_DIR =  "/media/ubuntu/LaCie/feature_tracking_data/output_serial/";
threshold = 6

for t in range( 3, 548 ):

	from_t           = np.fromfile( INPUT_DIR + "tracking.cellular." + str( t ) + ".from.bin",   dtype="int32" )
	to_t             = np.fromfile( INPUT_DIR + "tracking.cellular." + str( t ) + ".to.bin",     dtype="int32" )
	counts_t         = np.fromfile( INPUT_DIR + "tracking.cellular." + str( t ) + ".counts.bin", dtype="f8" )

	# get the indices of the correspondences where the overlap/count is greater than our threshold

	filterd_indicies = np.where( counts_t < threshold )[ 0 ]

	# filter out correspondences not meeting threshold

	from_t   = from_t[   filterd_indicies ]
	to_t     = to_t[     filterd_indicies ]
	counts_t = counts_t[ filterd_indicies ]

	fr = Counter( from_t ).most_common()
	tr = Counter( to_t   ).most_common()

    # extract merge events --- multiple from entries with same to entry

	n_merges = 0
	for element in tr:
		n_merges += element[ 1 ] - 1

	# extract split events --- implies multiple to   entries with same from entry

	n_splits = 0
	for element in fr:
		n_splits += element[ 1 ] - 1

	# extract created events --- implies cc_id in current step with no from entry

	

	# extract destruction events --- implies cc_id in previous step with no to entry 

	

	print( n_merges )
	print( n_splits )
	print()
