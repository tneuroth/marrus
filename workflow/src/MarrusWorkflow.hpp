
#ifndef TN_MARRUS_WORKFLOW_HPP
#define TN_MARRUS_WORKFLOW_HPP

#include "lsrcvt/liblsrcvt.hpp"
#include "lsrcvt/dhffdbs.hpp"
#include "feature_tracking/CCParallel.hpp"
#include "feature_tracking/ParallelFeatureTracking.hpp"
#include "stats/MarrusParallelStatsModule.hpp"

#include <vector>
#include <ctsdint>
#include <string>
#include <iostream>
#include <array>
#include <memory>

/* Notes: 

	Going to need to change the lsrcvt class to be able to set the parameters
and reinitialize dynamically, considering the dimensions might change. As the 
dimensions change, could be able to associate one data step from another based
on real coordinates. Maybe could use inheritance, and make a module for data 
with different coordinate systems, and for dynamic grids, or different grid 
types. To start can implement a version assuming a static uniform regular 
cartesian grid. 

	The lsrcvt module should be also configured from simpler config files,
or the configuration needs to come from a marrus processing configuration.

	A statistics, or analysis module should be included. And a rendering
module that renders to file should be included.

	Everything needs to be configurable. Could write a GUI based configuration
tool. Could connect that configuration tool with the Marrus visualization system
so that you can automatically transfer parameters from the active marrus visualization
to input to the configuration tool, to then export.

In the early prototyping phase, the necessary parameters can be hard coded.

	Abstracting the grid structure for long term adapatablility may be a 
good idea.

*/

struct MarrusWorkflowConfig
{

};

namespace TN
{

class MarrusProcessingWorkflow
{
	std::unique_ptr< TN::CCParallel >                m_connectedComponentsModule;
	std::unique_ptr< TN::CentroidalSampler >         m_lsrcvtModule;
	std::unique_ptr< TN::ParallelFeatureTracking >   m_featureTrackingModule;
	std::unique_ptr< TN::MarrusParallelStatsModule > m_statisticsModule;
	std::unique_ptr< TN::DHFF_Module >               m_dhffModule;

	MarrusProcessingConfig m_currentConfig;

	// ========================================================

	std::array< 3, int64_t > m_gridDims;

	// ========================================================

public:

	void configure( const MarrusProcessingConfig & config )
	{
		m_currentConfig = config;
	}

	// ========================================================

	template < typename T, class CONFIG_TYPE >
	void onStep( const std::map< std::string, T * > dataPtrs )
	{

	}

	template < typename T, class CONFIG_TYPE >
	void onStep( 
		const std::map< std::string, T * > dataPtrs,
		const MarrusProcessingConfig     & config )
	{
		configure( config );
		onStep( dataPtrs )
	}

	// ========================================================

	// When you have no coords but dynamic dims

	template < typename T >
	void onStep( 
		const std::map< std::string, T * > dataPtrs,
		const std::array< int64_t, 3 >   & gridDims )
	{

	}

	template < typename T >
	void onStep( 
		const std::map< std::string, T * > dataPtrs,
		const std::array< int64_t, 3 >   & gridDims,
		const MarrusProcessingConfig     & config )
	{
		configure( config );
		onStep(  dataPtrs );
	}

	// ========================================================

	// When you have dynamic coordinates and dims

	template < typename T >
	void onStep( 
		const std::map< std::string, T * >     dataPtrs,
		const std::array< COORDS_TYPE *, 3 > & coords,
		const std::array< int64_t, 3 >       & gridDims )
	{
		onStep(  dataPtrs, coords, gridDims );
	}

	template < typename T >
	void onStep( 
		const std::map< std::string, T * >   & dataPtrs,
		const std::array< COORDS_TYPE *, 3 > & coords,
		const std::array< int64_t, 3 >       & gridDims,
		const MarrusProcessingConfigc        & config )
	{
		configure( config );
		onStep(  dataPtrs, coords, gridDims );
	}

	// ========================================================
};

}

#endif