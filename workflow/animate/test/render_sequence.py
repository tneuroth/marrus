
import os

data_directory    = "/media/ubuntu/LaCie/feature_tracking_data/extracted/"
v3d_path          = "/home/ubuntu/dev/webgl-volvis/V3D/build/"
img_directory     = "/media/ubuntu/LaCie/feature_tracking_data/animation/frames/"
config_directory  = "/media/ubuntu/LaCie/feature_tracking_data/animation/config_sequence/"

os.environ['LD_LIBRARY_PATH'] = os.environ['LD_LIBRARY_PATH']  + ":" + v3d_path
print( os.environ['LD_LIBRARY_PATH'] )

for i in range( 1, 549 ):
	ts = str( i ).zfill(5)
	config_path = config_directory + "c." + ts + ".json"
	img_path = img_directory + "img." + ts + ".png"
	cmd = v3d_path + "vidi3d --batch --no-gui --image-size 2560 1440 --screenshot " +  img_path + " " + config_path 
	os.system( cmd )


