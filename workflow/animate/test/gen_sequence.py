
f = open( "extracted_template.json", "rt")
template = f.read()
f.close()

data_directory = "/media/ubuntu/LaCie/feature_tracking_data/extracted/"
out_path = "/media/ubuntu/LaCie/feature_tracking_data/animation/config_sequence/"

for i in range( 1, 550 ):
	ts = str( i ).zfill( 10 )
	f = "\"" + data_directory + "ptj_nr." + ts + "_temp_432x480x320_float32.raw\""
	generated = open( out_path + "c." + str( i ).zfill(5) + ".json", "wt")
	generated.write( template.replace( '$1', f ) )
	generated.close()


