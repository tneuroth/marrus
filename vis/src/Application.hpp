#ifndef TN_MARRUS_VISUALIZATION_SYS_HPP
#define TN_MARRUS_VISUALIZATION_SYS_HPP

// Marrus

#include "ProjectConfiguration.hpp"
// #include "configure.hpp"
#include "Theme.hpp"
#include "Tessellation.hpp"
#include "SummaryDataManager.hpp"
#include "expressions/Expressions.hpp"

// common / TN

#include "rendering/renderTabView.hpp"
#include "rendering/configPanelRendering.hpp"
#include "rendering/PlotView/JointPlotRender.hpp"
#include "rendering/SpatialView/rendering3D.hpp"
#include "rendering/FilterGroupRendering.hpp"

#include "render/GLErrorChecking.hpp"

#include "render/RenderModule3D/RenderModule.hpp"
#include "geometry/SurfaceSetCollection.hpp"
#include "input/ZFileSelector.hpp"
#include "utils/io.hpp"
#include "render/Camera/TrackBallCamera.hpp"
#include "geometry/Vec.hpp"
#include "render/RenderModule2D/Renderer2D.hpp"
#include "views/JointPlot2D.hpp"
#include "views/InputState.hpp"
#include "views/PressButtonWidget.hpp"
#include "views/ClusterView.hpp"
#include "utils/StrUtils.hpp"
#include "input/SimpleDataManager.hpp"
#include "lsrcvt/liblsrcvt.hpp"
#include "algorithms/Standard/MyAlgorithms.hpp"
#include "geometry/TrilinearInterpolator.hpp"
#include "geometry/GeometryUtils.hpp"
#include "render/TextRenderer/TextRenderer.hpp"
#include "algorithms/Filter/BasicFilter.hpp"
#include "utils/IndexUtils.hpp"
#include "algorithms/ConnectedComponentsCPU.hpp"
#include "stats/Stats.hpp"
#include "views/InputState.hpp"
#include "views/TabView.hpp"
#include "views/TreeView.hpp"
#include "views/RenderGroup3D.hpp"
#include "views/HierarchicalFilterGroup.hpp"
#include "views/PlotGroup2D.hpp"
#include "views/ConfigurationPanelSet.hpp"
#include "views/DataDefinitionPanel.hpp"
#include "views/SystemConfigurationPanel.hpp"
#include "views/ExportPanel.hpp"
#include "views/JointPlotConfigurationPanel.hpp"
#include "views/View3DConfigurationPanel.hpp"
#include "views/MarrusProjectConfigurationPanel.hpp"

// System

#include <sstream>
#include <fstream>
#include <vector>
#include <set>
#include <atomic>
#include <thread>
#include <stdio.h>

// #include <filesystem>

#include <chrono>
using namespace std::chrono;

std::array< int64_t, 3 > coordsToIndices( 
    const TN::Vec3< float > & p, 
    const std::array< int64_t, 3 > & DIMS )
{
    return { 
        std::min( std::max( int( p.x() ), 0 ), int( DIMS[ 0 ] - 1 ) ), 
        std::min( std::max( int( p.y() ), 0 ), int( DIMS[ 1 ] - 1 ) ),  
        std::min( std::max( int( p.z() ), 0 ), int( DIMS[ 2 ] - 1 ) ) };
}

TN::Vec3< float > estimateNormalPrincipleDirecton( 
    const TN::Vec3< float >        & p,
    const std::vector< float >     & volume,
    const std::array< int64_t, 3 > & DIMS )
{
    float h = 2.0;
    float dx = h;
    float dy = h;
    float dz = h;

    float av = volume[ TN::flatIndex3dCM( coordsToIndices( p + TN::Vec3< float >( dx, 0, 0 ), DIMS ), DIMS ) ]; 
    float bv = volume[ TN::flatIndex3dCM( coordsToIndices( p - TN::Vec3< float >( dx, 0, 0 ), DIMS ), DIMS ) ]; 
    float cv = volume[ TN::flatIndex3dCM( coordsToIndices( p + TN::Vec3< float >( 0, dy, 0 ), DIMS ), DIMS ) ]; 
    float dv = volume[ TN::flatIndex3dCM( coordsToIndices( p - TN::Vec3< float >( 0, dy, 0 ), DIMS ), DIMS ) ]; 
    float ev = volume[ TN::flatIndex3dCM( coordsToIndices( p + TN::Vec3< float >( 0, 0, dz ), DIMS ), DIMS ) ]; 
    float fv = volume[ TN::flatIndex3dCM( coordsToIndices( p - TN::Vec3< float >( 0, 0, dz ), DIMS ), DIMS ) ]; 

    TN::Vec3< float > normal = 
    {
        ( av - bv ),
        ( cv - dv ),        
        ( ev - fv )
    };

    normal.normalize();

    return normal;
}

struct FilterSelectionState
{
        std::vector< int > bandBitFlagsLast;
        std::vector< int > bandBitFlags;

        std::vector< int > componentBitFlagsLast;
        std::vector< int > componentBitFlags;

        std::vector< int > cellBitFlagsLast;
        std::vector< int > cellBitFlags;

        std::vector< int > voxelBitFlagsLast;
        std::vector< int > voxelBitFlags;

        std::vector< int32_t > componentLayerMap; // TODO: factor out

        std::map< std::string, std::vector< float > > voxelGathered;
        std::map< std::string, TN::Vec2< float > > voxelGatheredRange;
        std::vector< int > voxelGatheredFlags;
        std::vector< std::int64_t > voxelGatheredSubselectionIndices;

        std::map< std::string, std::vector< float > > voxelGatheredSubselection;
        std::map< std::string, TN::Vec2< float > > voxelGatheredSubselectionRange;
        std::vector< int > voxelGatheredFlagsSubselection;

        bool active;

        FilterSelectionState() : active( false ) {}

        void clear() {
            bandBitFlagsLast = {};
            bandBitFlags = {};
            componentBitFlagsLast = {};
            componentBitFlags = {};
            cellBitFlagsLast = {};
            cellBitFlags = {};
            voxelBitFlagsLast = {};
            voxelBitFlags = {};
            componentLayerMap = {}; 
            voxelGathered = {};
            voxelGatheredRange = {};
            voxelGatheredFlags = {};
            voxelGatheredSubselectionIndices = {};
            voxelGatheredSubselection = {};
            voxelGatheredSubselectionRange = {};
            voxelGatheredFlagsSubselection = {};  
            active = false;
        }
};

struct SelectionSurfaces {
    std::vector< TN::Vec3< float > > voxelSelectionSurfaceMesh;
    std::vector< TN::Vec3< float > > voxelSelectionSurfaceNormals;
    std::vector< float >             voxelSelectionSurfaceScalars;    

    std::vector< TN::Vec3< float > > cellSelectionSurfaceMesh;
    std::vector< TN::Vec3< float > > cellSelectionSurfaceNormals;
    std::vector< float >             cellSelectionSurfaceScalars;    

    std::vector< TN::Vec3< float > > componentSelectionSurfaceMesh;
    std::vector< TN::Vec3< float > > componentSelectionSurfaceNormals;
    std::vector< float >             componentSelectionSurfaceScalars;  

    void clear()
    {
        voxelSelectionSurfaceMesh = {};
        voxelSelectionSurfaceNormals = {};
        voxelSelectionSurfaceScalars = {};    

        cellSelectionSurfaceMesh = {};
        cellSelectionSurfaceNormals = {};
        cellSelectionSurfaceScalars = {};    

        componentSelectionSurfaceMesh = {};
        componentSelectionSurfaceNormals = {};
        componentSelectionSurfaceScalars = {};  
    }
};

class Application
{
    // === Application =============================================================================

    GLFWwindow * m_window;
    TN::ZFileSelector m_fileSelector;
    Marrus::Theme m_theme;
    std::string m_workingDir   ;
    std::string m_pdflatexDir  ;
    std::string m_pdftocairoDir;
    std::string m_convertDir   ;

    int m_windowWidth;
    int m_windowHeight;

//------------------------------------Processing-------------------------------------------------

    Marrus::Expressions m_expressionProcessor;

    //---------------------------------Rendering-------------------------------------------------

    TN::TextRenderer    m_textRenderer;
    TN::Renderer2D      m_renderer;    
    TN::RenderModule    m_renderModule;

    //-----------------------------boolean combination-----------------------------------------

    std::vector< TN::TexturedPressButton * > m_selectionModeButtonGroup;
    TN::TexturedPressButton m_setSelectButton;
    TN::TexturedPressButton m_setUnionButton;
    TN::TexturedPressButton m_setIntersectionButton;
    TN::TexturedPressButton m_setDifferenceButton;
    TN::TexturedPressButton m_setClearButton;

    Marrus::FilterOp::Type m_filterMode;

    //---------------------View toggling and application level buttons--------------------------

    TN::TexturedPressButton m_show2DViewButton;
    TN::TexturedPressButton m_show3DViewButton;

    TN::TexturedPressButton m_helpButton;
    TN::TextureLayerRGBA m_keyMapTexture;
    TN::TexturedPressButton m_clusterButton;
    TN::TextureLayer m_01IntervalTexture;

    //------------------------------------Side Panel---------------------------------------------

    TN::TabView m_sidePanel;

    TN::ExportPanel                     m_exportPanel;
    TN::ConfigurationPanelSet           m_configurationPanelSet;
    TN::MarrusProjectConfigurationPanel m_projectConfigurationPanel;
    TN::SystemConfigurationPanel        m_systemConfigurationPanel;
    TN::DataDefinitionPanel             m_dataDefinitionPanel;
    TN::JointPlotConfigurationPanel     m_jointPlotConfigurationPanel;
    TN::View3DConfigurationPanel        m_3DViewConfigurationPanel;

    // Was work in progress, but needs to be reconsidered 
    // and integrated within the new more general framework eventually
    TN::HierarchicalFilterGroup     m_filterGroup; 

    //-------------------------------Filter/Selection State-------------------------------------

    FilterSelectionState m_filterSelectionState;

    //----------------------------3D and 2D Visualization Views----------------------------------

    TN::RenderGroup3D  m_renderGroup3D;
    TN::PlotGroup2D    m_plotGroup;

    int m_activeJointPlot; // TODO: should be factored out, already part of plot group

    // === Project =============================================================================

    TN::ProjectConfiguration  m_projectConfiguration;
    std::string               m_projectDirectory;

    // -----------------------Global Visualization Parameters-----------------------------------

    int m_pointSize;

    //------------------------------------Latex-------------------------------------------------

    std::map< std::string, TN::Texture2D3 > m_mathTextures;
    std::map< std::string, std::string >    m_mathLatex;

    // Data Flow and Filtering

    Marrus::Tessellation m_tessellation;
    std::vector< float > m_siteNormals; 

    TN::SimpleDataManager<      float > m_dataManager;
    Marrus::SummaryDataManager< float > m_summaryDataManager;

    //----------------------------- Glyph Data (temporary) ------------------------------------

    // glyph -> band -> data
    std::map< std::string, std::vector< std::vector< float > > > m_glyphData;
    std::map< std::string, std::vector< std::vector< float > > > m_glyphDataAverage;
    std::map< std::string, std::vector< std::vector< TN::Vec2< float > > > > m_glyphDataLims;
    std::map< std::string, std::vector< std::vector< TN::Vec2< float > > > > m_directionalGlyphMagnitudeLims;

    bool m_glyphPreviewNeedsUpdate;

    // active glyph configuration

    TN::GlyphConfiguration m_glyphConfiguration;

    //-----------------------------Visualization Elements---------------------------------------

    TN::SurfaceSetCollection m_surfaceCollection;

    // TODO: Fold these into the surface collection?

    SelectionSurfaces m_selectionSurfaces;

    //===================================================================================

    void renderKeyMap( double w, double h )
    {
        float mH = 802;
        float mW = 601;

        m_renderer.clearBox(
            w-mW-2,
            h-mH-38,
            mW+2,
            mH+4,
        { 0.4f, 0.4f, 0.4f, 0.93f } );

        GL_CHECK_CALL( glViewport( w-mW, h-mH-38+2, mW, mH ) );
        m_renderer.renderTexture( m_keyMapTexture.tex.id() );
    }

    void applyLayout( double w, double h, const double BTN_HT = 30 )
    {
        m_helpButton.setPosition(
            w-m_helpButton.size().x(),
            h - BTN_HT - 2 );

        m_sidePanel.setPosition( 0, 0 );
        m_sidePanel.resize( 
            std::min( 
                m_sidePanel.size().x(),
                std::min( 
                    (float ) w -     m_plotGroup.minSize().x(),
                    (float ) w - m_renderGroup3D.minSize().x() ) ),
            h
        );

        m_show3DViewButton.setPosition(   m_sidePanel.isOpen() ? m_sidePanel.position().x() + m_sidePanel.size().x() + 10 : 10 + BTN_HT + 4, h - BTN_HT - 2 );
        m_show2DViewButton.setPosition( m_show3DViewButton.position().x() + m_show3DViewButton.size().x() + 2,  h - BTN_HT - 2 );

        float sOffsetX = m_show2DViewButton.position().x() + m_show2DViewButton.size().x() + 20;
        for( size_t i = 0; i < m_selectionModeButtonGroup.size(); ++i )
        {
            TN::TexturedPressButton * button = m_selectionModeButtonGroup[ i ];
            button->setPosition( sOffsetX, h - BTN_HT - 2 );
            sOffsetX = button->position().x() + button->size().x();
        }

        /////////////////////////////////////////////////////////////////////////

        const bool Show3DView = m_show3DViewButton.isPressed();
        const bool Show2DView = m_show2DViewButton.isPressed();

        // 2D Plot grid

        if( Show2DView ) 
        {
            m_plotGroup.setPosition(
                m_sidePanel.isOpen() ? m_sidePanel.position().x() + m_sidePanel.size().x() : 0, 
                0 );

            m_plotGroup.resize( 
                m_sidePanel.isOpen() ?  w - m_sidePanel.size().x() : w, 
                Show3DView ? 
                        std::min( 
                            m_plotGroup.midHeight(), 
                            ( float ) ( h - BTN_HT - 2 - m_renderGroup3D.minSize().x() ) )
                    : ( float ) ( h - BTN_HT - 2 ) );
        }

        // 3D Plot Grid 

        if( Show3DView ) {

            m_renderGroup3D.setPosition(
                m_sidePanel.isOpen() ? m_sidePanel.position().x() + m_sidePanel.size().x() : 0, 
                Show2DView           ? m_plotGroup.size().y() : 0 );

            m_renderGroup3D.setSize( 
                m_sidePanel.isOpen() ? w - m_sidePanel.size().x() : w, 
                Show2DView           ? h - BTN_HT - 2 - m_plotGroup.size().y() : h - BTN_HT - 2  );

            m_renderGroup3D.applyLayout();
        }
    }

    bool updateVisSpace( const InputState & inputState ) 
    {
        auto & pos = inputState.mouseState.eventPosition ;
        if( m_show3DViewButton.pointInViewPort( pos ) ) {
            if( inputState.mouseState.event == MouseEvent::LeftPressed ) {
                m_show3DViewButton.setPressed( ! m_show3DViewButton.isPressed() );
                return true;
            }
        }
        else if ( m_show2DViewButton.pointInViewPort( pos ) )
        {
            if( inputState.mouseState.event == MouseEvent::LeftPressed ) {
                m_show2DViewButton.setPressed( ! m_show2DViewButton.isPressed() ); 
                return true;
            }
        }

        return false;
    }

    bool updateControlSpace( const InputState & inputState )
    {
        for( size_t i = 0; i < m_selectionModeButtonGroup.size(); ++i )
        {
            auto & button = *m_selectionModeButtonGroup[ i ];

            if( button.pointInViewPort( inputState.mouseState.eventPosition ) )
            {
                if( inputState.mouseState.event == MouseEvent::LeftPressed )
                {
                    // last one is independent
                    if( i != m_selectionModeButtonGroup.size() -1 )
                    {
                        button.setPressed( true );
                        for( int j = 0; j < m_selectionModeButtonGroup.size() -1; ++j )
                        {
                            if( i != j )
                            {
                                m_selectionModeButtonGroup[ j ]->setPressed( false );
                            }
                        }

                        m_filterMode =   i == 0 ? Marrus::FilterOp::Type::New
                                       : i == 1 ? Marrus::FilterOp::Type::Union
                                       : i == 2 ? Marrus::FilterOp::Type::Intersect
                                       :          Marrus::FilterOp::Type::SetMinus;

                        m_activeJointPlot = -1;
                    }
                    else
                    {
                        // TODO: clear selection 
                    }
                }
            }
        }

        bool updateLayout = false;
        return updateLayout;
    }

    void renderControlSpace( float windowWidth, float windowHeight )
    {
        // m_renderer.renderPressButton( &m_clusterButton, { 0.2, 0.2, 0.2 }, windowWidth, windowHeight, true );

        m_renderer.renderPressButton( & m_show3DViewButton,   { 0.2, 0.2, 0.2 }, windowWidth, windowHeight, true );
        m_renderer.renderPressButton( & m_show2DViewButton, { 0.2, 0.2, 0.2 }, windowWidth, windowHeight, true );

        for( auto button : m_selectionModeButtonGroup )
        {
            m_renderer.renderPressButton( button, { 0.2, 0.2, 0.2 }, windowWidth, windowHeight, true );
        }
        // m_renderer.renderPressButton( & m_helpButton, { 0.2, 0.2, 0.2 }, windowWidth, windowHeight, true );
    }

    float renderMath(
        const std::string & key,
        const TN::Vec2< float > & pos )
    {
        if( ! m_mathTextures.count( key ) )
        {
            std::cerr << "math texure not found: " << key << std::endl;
            exit( 1 );
        }

        auto & tex = m_mathTextures.at( key );
        float th = 18;
        float tw = th * ( tex.width() / ( double ) tex.height() );

        m_renderer.renderTexture(
            tex,
        { pos.x(), pos.y() - 5 },
        { tw, th } );

        return tw;
    }

    // NOTE: Text sizeing may need to be adjusted since scale factors have been disabled
    // while switching to supporting user specified font sizes

    void renderPlotSummary(
        TN::JointPlot2D & plt,
        double windowWidth,
        double windowHeight )
    {
        const float LEFTMARGIN = 90;

        const float WDTH = LEFTMARGIN + plt.size().x() +
                           ( ( plt.yMarginal().position().x() + plt.yMarginal().size().x() )
                             - ( plt.position().x()             +             plt.size().x() ) ) - 4;

        const float VSPACE = 8;
        const float COLWDTH = ( WDTH - LEFTMARGIN ) / 4.f;
        const float ROWHGHT = 22;

        float P2HEIGHT = 94;

        const float HGHT = 28 + 6 * VSPACE + ROWHGHT * 10 + P2HEIGHT;

        TN::Vec2< float > ll( plt.position().x() - 48, plt.xMarginal().position().y() + plt.xMarginal().size().y() + 10 );

        bool moveToBottom = false;
        if( ll.y() + HGHT > windowHeight )
        {
            moveToBottom = true;
        }

        if( moveToBottom )
        {
            ll = { ll.x(), plt.position().y() - 100 - HGHT };
        }

        m_renderer.clearBox(
            ll.x(),
            ll.y(),
            WDTH,
            HGHT,
        { 0.4f, 0.4f, 0.4f, 1.f } );

        if( moveToBottom )
        {
            m_renderer.clearBox(
                ll.x()+2,
                ll.y()+2,
                WDTH - 4,
                HGHT - 2,
            { 1.0f, 1.0f, 1.0f, 1.f } );
        }
        else
        {
            m_renderer.clearBox(
                ll.x()+2,
                ll.y(),
                WDTH - 4,
                HGHT - 2,
            { 1.0f, 1.0f, 1.0f, 1.f } );
        }

        GL_CHECK_CALL( glViewport( 0, 0, windowWidth, windowHeight ) );

        float offsetY = plt.xMarginal().position().y() + plt.xMarginal().size().y() + HGHT - 18 - 28;

        if( moveToBottom )
        {
            offsetY = ll.y() + HGHT - 18 - 28;
        }

        float charW = 7.f;
        m_textRenderer.renderText(
            windowWidth,
            windowHeight,
            "Moments",
            { ll.x() + 20, offsetY + 28 },
            { 0.0f, 0.0f, 0.0f, 1.f },
            false );

        std::vector< std::string > colLabels = { "m1", "m2", "m3", "m4" };

        for( int i = 0; i < colLabels.size(); ++i )
        {
            float texRWidth = renderMath(
                                  colLabels[ i ],
            { ll.x() + LEFTMARGIN + i * COLWDTH + 4 + COLWDTH / 2 - 7 * colLabels.size(), offsetY } );
        }

        GL_CHECK_CALL( glViewport( 0, 0, windowWidth, windowHeight ) );

        std::vector< std::string > rowLabels =
        {
            "px1rs",   "hx1rs",   "error",
            "px2rs",   "hx2rs",   "error",
            "px1x2rs", "hx1x2rs", "error"
        };

        // for (X|r,s)

        std::vector< std::vector< float > > table =
        {
            { (float) plt.meanX(),  (float) plt.varianceX(),  (float) plt.skewnessX(),  (float) plt.kurtosisX()  },
            { (float) plt.meanXH(), (float) plt.varianceXH(), (float) plt.skewnessXH(), (float) plt.kurtosisXH() },
            { (float) plt.meanXE(), (float) plt.varianceXE(), (float) plt.skewnessXE(), (float) plt.kurtosisXE() },

            { (float) plt.meanY(),  (float) plt.varianceY(),  (float) plt.skewnessY(),  (float) plt.kurtosisY() },
            { (float) plt.meanYH(), (float) plt.varianceYH(), (float) plt.skewnessYH(), (float) plt.kurtosisYH() },
            { (float) plt.meanYE(), (float) plt.varianceYE(), (float) plt.skewnessYE(), (float) plt.kurtosisYE() },

            { 0.f, (float) plt.covariance(),  (float) plt.coskewness(),  (float) plt.cokurtosis() },
            { 0.f, (float) plt.covarianceH(), (float) plt.coskewnessH(), (float) plt.cokurtosisH() },
            { 0.f, (float) plt.covarianceE(), (float) plt.coskewnessE(), (float) plt.cokurtosisE() }
        };

        offsetY -= ROWHGHT;

        float yp;
        for( int i = 0; i < rowLabels.size(); ++i )
        {
            yp = offsetY - ( i * ROWHGHT + (i / 3) * VSPACE );

            float texRWidth = renderMath(
                                  rowLabels[ i ],
            { ll.x() + 8, yp } );

            for( int j = 0; j < table[ i ].size(); ++j )
            {
                if( j < 1 && i >= 6 )
                {
                    continue;
                }


                m_textRenderer.renderText(
                    windowWidth,
                    windowHeight,
                    TN::toStringWFMT( table[ i ][ j ] ),
                    { ll.x() + LEFTMARGIN + j * COLWDTH + 4, yp },
                    ( i % 3 == 2 ? TN::Vec4( { 0.9f, 0.2f, 0.1f, 1.f } ) : TN::Vec4( { 0.3f, 0.3f, 0.3f, 1.f } ) ),
                    false );

            }
        }

        offsetY = yp - ROWHGHT * 2;

        float texRWidth = renderMath( "countX", { ll.x() + 10, offsetY } );

        m_textRenderer.renderText(
            windowWidth,
            windowHeight,
            std::to_string( (int64_t) plt.fullCount() ),
            { ll.x() + LEFTMARGIN, offsetY },
            { 0.3f, 0.3f, 0.3f, 1.f },
            false );

        offsetY -= ROWHGHT;

        texRWidth = renderMath(
                        "countXr",
        { ll.x() + 10, offsetY } );

        m_textRenderer.renderText(
            windowWidth,
            windowHeight,
            std::to_string( (int64_t) plt.rCount() ),
            { ll.x() + LEFTMARGIN, offsetY },
            { 0.3f, 0.3f, 0.3f, 1.f },
            false );

        offsetY -= ROWHGHT;

        texRWidth = renderMath(
                        "countXrs",
        { ll.x() + 10, offsetY } );

        m_textRenderer.renderText(
            windowWidth,
            windowHeight,
            std::to_string( (int64_t) plt.subCount() ),
            { ll.x() + LEFTMARGIN, offsetY },
            { 0.3f, 0.3f, 0.3f, 1.f },
            false );

        offsetY -= ROWHGHT;

        texRWidth = renderMath(
                        "countXrg",
        { ll.x() + 10, offsetY } );

        GL_CHECK_CALL( glViewport( 0, 0, windowWidth, windowHeight ) );
        m_textRenderer.renderText(
            windowWidth,
            windowHeight,
            std::to_string( (int64_t) plt.gCount() ),
            { ll.x() + LEFTMARGIN, offsetY },
            { 0.3f, 0.3f, 0.3f, 1.f },
            false );

        // need 4 rows for each moment
        // columns in groups of 2 with vertical spaces
    }

    void renderProjection(
        const TN::ProjectionPlot & plt,
        const std::vector< float > & x,
        const std::vector< float > & y,
        const std::vector< TN::Vec3< float > > & colors,
        const std::vector< int32_t > & flags,
        bool renderBox,
        const std::string & label,
        double windowWidth,
        double windowHeight )
    {
        GL_CHECK_CALL( glDisable( GL_DEPTH_TEST ) );
        GL_CHECK_CALL( glViewport(
            plt.position().x(),
            plt.position().y(),
            plt.size().x(),
            plt.size().y() ) );

        m_renderer.clear( { 0.9, 0.9, 0.9, 1 } );
        m_renderer.renderViewportBoxShadow( plt.viewPort() );

        GL_CHECK_CALL( glPointSize( 5 ) );
        m_renderer.scatterPoints(
            x,
            y,
            flags,
            TN::BACKGROUND_ELEMENT,
            plt.xRange(),
            plt.yRange(),
        { 0.5, 0.5, 0.5, 1.0 },
        7.f );

        GL_CHECK_CALL( glPointSize( 10 ) );
        m_renderer.scatterPoints(
            x,
            y,
            flags,
            TN::PRE_LEVEL_ELEMENT,
            plt.xRange(),
            plt.yRange(),
        { 0.3, 0.3, 0.3, 1.0 },
        12.f );

        GL_CHECK_CALL( glPointSize( 7 ) );
        m_renderer.scatterPoints(
            x,
            y,
            colors,
            flags,
            TN::PRE_LEVEL_ELEMENT,
            plt.xRange(),
            plt.yRange(),
            7.f );

        GL_CHECK_CALL( glPointSize( 12 ) );
        m_renderer.scatterPoints(
            x,
            y,
            flags,
            TN::SUBSELECTION_ELEMENT,
            plt.xRange(),
            plt.yRange(),
        { 0.0, 0.0, 0.0, 1.0 },
        12.f );

        GL_CHECK_CALL( glPointSize( 8 ) );
        m_renderer.scatterPoints(
            x,
            y,
            colors,
            flags,
            TN::SUBSELECTION_ELEMENT,
            plt.xRange(),
            plt.yRange(),
            8.f );

        if( plt.probeActive() && renderBox )
        {
            if( plt.probeType() == TN::ProbeType::Box )
            {
                m_renderer.renderPrimitivesFlat(
                    plt.getBoxLines(),
                    { 0.2, 0.2, 0.2, 1.0 },
                    100,
                    100,
                    plt.xRange(),
                    plt.yRange(),
                    GL_LINE_STRIP );
            }
            else if( plt.probeType() == TN::ProbeType::Lasso )
            {
                auto lassoPoints = plt.lasso();
                if( lassoPoints.size() > 2 )
                {
                    lassoPoints.push_back( lassoPoints.front() );
                    m_renderer.renderPrimitivesFlat(
                        lassoPoints,
                        { 0.2, 0.2, 0.2, 1.0 },
                        100,
                        100,
                        plt.xRange(),
                        plt.yRange(),
                        GL_LINE_STRIP );
                }
            }
        }

        m_textRenderer.renderText(
            windowWidth,
            windowHeight,
            label,
            { plt.position().x(), plt.position().y() + plt.size().y() + 2 },
            { 0.5f, 0.5f, 0.5f, 1.f },
            false );
    }

    void renderPlot(
        TN::JointPlot2D & plt,
        double windowWidth,
        double windowHeight,
        const std::vector< float > & x,
        const std::vector< float > & y,
        const TN::Vec2< float > & xRange,
        const TN::Vec2< float > & yRange,
        const std::vector< int   > & flags,
        bool boxSelectorActivated,
        TN::Vec4 color,
        bool renderBackground,
        bool outLinePoints,
        float pointSize,
        bool supressUpdate )
    {
        Marrus::renderJointPlot2D(
            m_renderer,
            m_textRenderer,
            m_dataManager,
            plt,
            windowWidth,
            windowHeight,
            x,
            y,
            xRange,
            yRange,
            flags,
            {},
            {},
            {},
            boxSelectorActivated,
            { 0.8,  0.9,  0.7,  0.5 },
            { 0.8,  0.9,  0.7,  0.5 },
            { 0.85, 0.85, 0.85, 1.0 }, 
            { 0.2,  0.2,  0.2,  1.0 },                                
            renderBackground,
            outLinePoints,
            pointSize,
            supressUpdate );
    }


    void renderPlot(
        TN::JointPlot2D & plt,
        double windowWidth,
        double windowHeight,
        const std::vector< float > & x,
        const std::vector< float > & y,
        const TN::Vec2< float > & xRange,
        const TN::Vec2< float > & yRange,
        const std::vector< int   > & flags,        
        const std::vector< float > & xSubSelection,
        const std::vector< float > & ySubSelection,
        const std::vector< int   > & flagsSubSelection,        
        bool boxSelectorActivated,
        TN::Vec4 color,
        bool renderBackground,
        bool outLinePoints,
        float pointSize,
        bool supressUpdate)
    {
        Marrus::renderJointPlot2D(
            m_renderer,
            m_textRenderer, 
            m_dataManager,              
            plt,
            windowWidth,
            windowHeight,
            x,
            y,
            xRange,
            yRange,
            flags,
            xSubSelection,
            ySubSelection,
            flagsSubSelection, 
            boxSelectorActivated,
            color,
            { 0.2, 0.2, 0.2, 0.2 },
            { 0.85, 0.85, 0.85, 1.0 }, 
            { 0.2, 0.2, 0.2, 1.0 },  
            renderBackground,
            outLinePoints,
            pointSize,
            supressUpdate );
    }

    void setGlyph( const TN::GlyphConfiguration & glyph )
    {
        m_glyphConfiguration = glyph;

        m_renderModule.setGlyph( 
            m_workingDir + "/models/" + glyph.obj_file, 
            m_workingDir + "/models/" + glyph.tex_file );

        if( glyph.glyph_type == "radial" ||  glyph.glyph_type == "star" )
        {
            std::vector< std::string > TFs;

            // each axis has its own TF
            for( int i = 0; i < glyph.axes.size(); ++i ) 
            {
                TFs.push_back( glyph.axes[ i ].encoding );
            }

            m_renderModule.createGlyphTF( TFs );
        }
        else if (  glyph.glyph_type == "directional" )
        {
            std::vector< std::string > TFs;

            // each vector has its own TF
            for( int i = 0; i < glyph.vectors.size(); ++i ) 
            {
                TFs.push_back( glyph.vectors[ i ].encoding );
            }

            m_renderModule.createGlyphTF( TFs ); 
        }
    }

public:

    void renderLoadingScreen(
        int screenWidth,
        int screenHeight,
        const std::string & mssg,
        int percentDone,
        bool clearScreen,
        float clearScreenOpacity )
    {
        const int XDIM = 800;
        const int YDIM = 70;


        // GL_CHECK_CALL( glViewport( screenWidth / 2.0 - XDIM / 2, screenHeight / 2.0 - 20, XDIM, YDIM ) );

        GL_CHECK_CALL( glDisable( GL_DEPTH_TEST ) );

        if( clearScreen )
        {
            // m_renderer.clearScreen( { 1.0, 1.0, 1.0, clearScreenOpacity } );

            GL_CHECK_CALL( glViewport( 0, 0, screenWidth, screenHeight ) );
            m_renderer.clear( { 0.95, 0.96, 1.0, clearScreenOpacity } );
        }

        m_textRenderer.renderText(
            screenWidth,
            screenHeight,
            mssg,
            { screenWidth / 2.0 - mssg.size() * 20 / 2, screenHeight / 2.0 },
            { 0.3f, 0.3f, 0.3f, 1.f },
            false, 
            34 );

        m_renderer.swapBuffers();
    }

    void registerPlotVariable( const std::string & v )
    {
        if( ! m_filterSelectionState.voxelGathered.count( v ) )
        {
            m_filterSelectionState.voxelGathered.insert( { v, std::vector< float >( -1.1, 1.1 ) } ); 
            m_filterSelectionState.voxelGatheredRange.insert( { v, TN::Vec2< float >( -1.1, 1.1 ) 
            } );             
        }
        if( ! m_filterSelectionState.voxelGatheredSubselection.count( v ) )
        {
            m_filterSelectionState.voxelGatheredSubselection.insert( { v, std::vector< float >( -1.1, 1.1 ) } );
            m_filterSelectionState.voxelGatheredSubselectionRange.insert( { v, TN::Vec2< float >( -1.1, 1.1 ) } );   
        }
    }

    void configure()
    {
        for( auto & eq : m_mathLatex )
        {
            std::vector< float > texData;
            TN::Vec2< int > texDims;
            TN::loadRGBA(
                m_projectDirectory + "/latex_symbols/" + eq.first + ".png",
                texData,
                texDims );

            m_mathTextures.insert(
            { eq.first, TN::Texture2D3() } );

            m_mathTextures.at( eq.first ).create();
            m_mathTextures.at( eq.first ).load( texData, texDims.a(), texDims.b(), true );
        }

        //////////////////////////////////////////////////////////

        m_renderGroup3D.setDefaultGlyphConfig( m_glyphConfiguration );

        for( auto & spatialView : m_projectConfiguration.spatialViews ) 
        {
            spatialView.glyphConfiguration = m_glyphConfiguration;
            m_renderGroup3D.addView( spatialView );
        }

        ////////////////////////////////////////////////////////////

        const size_t N_SITES = m_tessellation.getSiteLayers().size();
        const auto vDims = m_dataManager.getVolumeDims();
        const size_t NV = vDims.x() * vDims.y() * vDims.z();

        m_plotGroup.clear();

        std::cout << "creating joint plots" << std::endl;

        for( auto & plt : m_projectConfiguration.jointPlots )
        {
            TN::JointPlot2D & p = *( m_plotGroup.addView() );
            p.initialize( plt );
            registerPlotVariable( plt.x );
            registerPlotVariable( plt.y );
            if( plt.w != "" && plt.w != "None" ) {
                registerPlotVariable( plt.w );
            }
        }

        m_filterSelectionState.bandBitFlags = std::vector< int >( m_tessellation.getNLayers() + 1, 0 );
        m_filterSelectionState.bandBitFlagsLast = m_filterSelectionState.bandBitFlags;

        m_filterSelectionState.componentBitFlags = std::vector< int >( m_tessellation.getNComponents() + ( ! m_tessellation.hasBackground() ), 0 );
        m_filterSelectionState.componentBitFlagsLast = m_filterSelectionState.componentBitFlags;

        m_filterSelectionState.cellBitFlags = std::vector< int >( N_SITES, 0 );
        m_filterSelectionState.cellBitFlagsLast = m_filterSelectionState.cellBitFlags;

        m_filterSelectionState.voxelBitFlags = std::vector< int >( NV, 0 );
        m_filterSelectionState.voxelBitFlagsLast = m_filterSelectionState.voxelBitFlags;
    }

    void initialize(
        const std::string & configurationFile,
        std::atomic< int > & status,
        std::atomic< int > & percentDone )
    {
        m_projectConfiguration.fromFile( configurationFile, m_workingDir );

        // TODO: should be part of joint plot configuration
        m_pointSize = 3;

        if( m_projectConfiguration.status != TN::ConfigStatus::Ok )
        {
            status = 6;
            std::cout << m_projectConfiguration.statusReport << std::endl;
            return;
        }

        ///////////////////////////////////////////////////////////////

        std::map< std::string, TN::SimpleVariableInfo > varInfo;
        for( auto & item : m_projectConfiguration.variables ) {
            varInfo.insert( { item.name, item } );
        }

        m_dataManager.init(
            
            m_projectConfiguration.dims,
            { 
                m_projectConfiguration.xCoordsPath,
                m_projectConfiguration.yCoordsPath,
                m_projectConfiguration.zCoordsPath  
            },

            varInfo,
            m_projectDirectory,
            m_workingDir,
            m_pdflatexDir,
            m_pdftocairoDir,
            m_convertDir,
            status ); 

        m_mathLatex =
        {
            { "px1x2",    "$p(x_1,x_2)$"     },
            { "px1x2rs",  "$p(x_1,x_2|r,s)$" },
            { "hx1x2",    "$h(x_1,x_2)$"     },
            { "hx1x2rs",  "$h(x_1,x_2|r,s)$" },
            { "px1",      "$p(x_1)$"         },
            { "px1r",     "$p(x_1|r)$"       },
            { "px1rs",    "$p(x_1|r,s)$"     },
            { "px2",      "$p(x_2)$"         },
            { "px2r",     "$p(x_2|r)$"       },
            { "px2rs",    "$p(x_2|r,s)$"     },
            { "hx1",      "$h(x_1)$"         },
            { "hx1r",     "$h(x_1|r)$"       },
            { "hx1rs",    "$h(x_1|r,s)$"     },
            { "hx2",      "$h(x_2)$"         },
            { "hx2r",     "$h(x_2|r)$"       },
            { "hx2rs",    "$h(x_2|r,s)$"     },
            { "countX",   "$c(X)$"           },
            { "countXr",  "$c(X|r)$"         },
            { "countXrs", "$c(X|r,s)$"       },
            { "countXrg", "$c(X|r,g)$"       },
            { "error",    "$|f_p-f_h|$"      },
            { "m1",       "$\\mu_1'$"        },
            { "m2",       "$\\mu_2'$"        },
            { "m3",       "$\\mu_3$"         },
            { "m4",       "$\\mu_4$"         },
            { "xaxis", "x-axis"              },
            { "yaxis", "y-axis"              }
        };

        ///////////////////////////////////////////////////////////////////

        std::vector< TN::GlyphFeature > features;
        for( const auto & glyph_config : m_projectConfiguration.glyphs ) {

            if( glyph_config.glyph_type == "radial" || glyph_config.glyph_type == "star" )
            {
                for( const auto & axis : glyph_config.axes ) {

                    std::cout << "adding axis " << axis.name << std::endl; 
                    m_mathLatex.insert( { axis.name, axis.latex } );
                    
                    for( const auto & feature : axis.components ) {
                        features.push_back( feature );
                        m_mathLatex.insert( { feature.name, feature.latex } );
                    }
                }
            }
            else if( glyph_config.glyph_type == "directional" )
            {
                for( const auto & vec : glyph_config.vectors ) {

                    m_mathLatex.insert( { vec.name, vec.latex } );

                    m_mathLatex.insert( { vec.x.name, vec.x.latex } );
                    m_mathLatex.insert( { vec.y.name, vec.y.latex } );
                    m_mathLatex.insert( { vec.z.name, vec.z.latex } );

                    features.push_back( vec.x );
                    features.push_back( vec.y );
                    features.push_back( vec.z );
                }
            }
        }

        m_summaryDataManager.init( 
            features,
            m_projectDirectory,
            m_workingDir,
            m_projectConfiguration.tessellationDirectory,
            m_pdflatexDir,
            m_pdftocairoDir,
            m_convertDir );


        std::cout << "initilized summary data manager" << std::endl;

        // status = 6;

        ////////////////////////////////////////////////////////////////////////////////////////////////////

        // TODO: this was for generating the statistical moments report
        // which is currently depreciated, the label function in data manager 
        // probably isn't needed

        // for( auto & p : m_projectConfiguration.jointPlots )
        // {
        //     std::string lx = m_dataManager.label( p.x );
        //     lx.pop_back();
        //     lx.erase( 0, 1 );

        //     std::string ly = m_dataManager.label( p.y );
        //     ly.pop_back();
        //     ly.erase( 0, 1 );

        //     m_mathLatex.insert( {    
        //         "mu1_" + p.x + "," + p.y,
        //         "$\\mu_1( " + lx + "," + ly + ")$"         
        //     } );

        //     m_mathLatex.insert( {    
        //         "mu2_" + p.x + "," + p.y,
        //         "$\\mu_2( " + lx + "," + ly + ")$"         
        //     } );

        //     m_mathLatex.insert( {    
        //         "mu3_" + p.x + "," + p.y,
        //         "$\\mu_3( " + lx + "," + ly + ")$"         
        //     } );

        //     m_mathLatex.insert( {    
        //         "mu4_" + p.x + "," + p.y,
        //         "$\\mu_4( " + lx + "," + ly + ")$"         
        //     } );
        // }

        for( auto & eq : m_mathLatex )
        {
            // bool forceRecompilation = false;
         
            // const std::string outPath = m_projectDirectory + "/latex_symbols/" + eq.first + ".pdf";\

            // if( ( ! std::ifstream( outPath ).good() ) || forceRecompilation )
            // {
                std::cout << "compiling: " << eq.first << std::endl;

                TN::generateEquationPNG(
                    m_workingDir + "/../common/latex/templates/cropped_equation_template.tex",
                    eq.second,
                    m_projectDirectory + "/latex_symbols/",
                    eq.first,
                    m_pdflatexDir,
                    m_pdftocairoDir,
                    m_convertDir,
                    true,
                    true
                );
            // }
        }

        status = 3;

        //////////////////////////////////////////////////////////////////////////////

        m_tessellation.fromFile( m_projectConfiguration.tessellationDirectory );

        status = 4;
        status = 5;

        /////////////////////////////////////////////////////////////////////////////////////

        // Summary Data (no ui so for summary data -> glyph, so for now just prepare the data)
        // for each configuration at startup

        // get glyph data organized by glyph name and then band id ////////////////////////

        // instanceID*nAxes*nComp + nComp*axis + comp

        std::cout << "initilized copying glyph data from summary data" << std::endl;

        for( const auto & glyph_config : m_projectConfiguration.glyphs ) {

            m_glyphData.insert(                     { glyph_config.name, {} } );
            m_glyphDataAverage.insert(              { glyph_config.name, {} } );
            m_glyphDataLims.insert(                 { glyph_config.name, {} } );
            m_directionalGlyphMagnitudeLims.insert( { glyph_config.name, {} } );

            std::vector< std::vector< float > > & gdata   = m_glyphData.at( glyph_config.name );
            std::vector< std::vector< float > > & gdataAv = m_glyphDataAverage.at( glyph_config.name );
            std::vector< std::vector< TN::Vec2< float > > > & gdata_lims = m_glyphDataLims.at( glyph_config.name );
            std::vector< std::vector< TN::Vec2< float > > > & vector_mag_lims = m_directionalGlyphMagnitudeLims.at( glyph_config.name );

            gdata.resize(      m_summaryDataManager.numBands() );
            gdataAv.resize(    m_summaryDataManager.numBands() );
            gdata_lims.resize( m_summaryDataManager.numBands() );
            vector_mag_lims.resize( m_summaryDataManager.numBands() );

            // gives a list of the cell ids in the band for each band

            auto & cellIdsPerBand = m_tessellation.getSiteLayerIndices();

            // each axis can have multiple components/features
            // need to know how many in total we need per cell/glyph

            int total_n_features    = glyph_config.totalNumFields();
            int components_per_axis = glyph_config.componentsPerAxis();

            // allocate the data, each band has a certain amount of cells, and for each cell 
            // we need data for some number of features 

            size_t N_AX = glyph_config.glyph_type == "directional" ? glyph_config.vectors.size() 
                        : glyph_config.axes.size();

            for( size_t i = 0; i < cellIdsPerBand.size(); ++i )
            {
                gdata[ i ].resize( cellIdsPerBand[ i ].size() * total_n_features );
                gdataAv[ i ].resize(    total_n_features );
                gdata_lims[ i ].resize( total_n_features );
                vector_mag_lims[ i ].resize( N_AX );
            }

            if( glyph_config.glyph_type == "radial" || glyph_config.glyph_type == "star" || glyph_config.glyph_type == "directional" )
            {
                // for each axis
                for( size_t axis_idx = 0; axis_idx < N_AX; ++axis_idx ) {

                    std::vector< TN::GlyphFeature > components =  glyph_config.glyph_type == "directional" ? 
                        std::vector< TN::GlyphFeature > ( {  
                            glyph_config.vectors[ axis_idx ].x,
                            glyph_config.vectors[ axis_idx ].y,
                            glyph_config.vectors[ axis_idx ].z
                        } )
                        :  glyph_config.axes[ axis_idx ].components;

                    // for x,y,z
                    for( size_t comp_idx = 0; comp_idx < components.size(); ++comp_idx ) {

                        const auto & feature = components[ comp_idx ];

                        // get the data of the feature for each cell
                        const auto & data = *( m_summaryDataManager.get( Marrus::SummaryLevel::Cells, feature.name ) );
                        
                        auto dataRange = TN::Sequential::getRange( data );
                        std::cout << feature.name << " " << dataRange.a() << " " << dataRange.y() << std::endl;

                        // get the data of the feature for each band
                        const auto & bData = *( m_summaryDataManager.get( Marrus::SummaryLevel::Bands, feature.name ) );

                        // now organize it by band id

                        // for each band
                        for( size_t band_idx = 0; band_idx < cellIdsPerBand.size(); ++band_idx )
                        {
                            const size_t N_SITES_BAND = cellIdsPerBand[ band_idx ].size();

                            size_t feature_offset = axis_idx * components_per_axis + comp_idx;

                            gdata_lims[ band_idx ][ feature_offset ] = 
                                TN::Vec2< float >( 
                                    std::numeric_limits<float>::max(),
                                   -std::numeric_limits<float>::max() );

                            gdataAv[ band_idx ][ feature_offset ] = bData[ band_idx ];

                            // for each cell in the band
                                
                            for( size_t i = 0; i < N_SITES_BAND; ++ i )
                            {
                                const int64_t CELL_IDX = cellIdsPerBand[ band_idx ][ i ];

                                const int64_t DATA_OFFSET = i * total_n_features + axis_idx * components_per_axis + comp_idx;
                                gdata[ band_idx ][ DATA_OFFSET ] = data[ CELL_IDX ];

                                // tracking min and max value for each feature per band

                                gdata_lims[ band_idx ][ feature_offset ].a( 
                                    std::min( gdata_lims[ band_idx ][ feature_offset ].a(),
                                    gdata[ band_idx ][ DATA_OFFSET ] )
                                );  

                                gdata_lims[ band_idx ][ feature_offset ].b( 
                                    std::max( gdata_lims[ band_idx ][ feature_offset ].b(),
                                    gdata[ band_idx ][ DATA_OFFSET ] )
                                );
                            }
                        }
                    }
                }
            }
            if( glyph_config.glyph_type == "directional" )
            {
                // For each axis/vector
                for( size_t axis_idx = 0; axis_idx < N_AX; ++axis_idx ) {

                    // For each band
                    for( size_t band_idx = 0; band_idx < cellIdsPerBand.size(); ++band_idx )
                    {
                        const size_t N_SITES_BAND = cellIdsPerBand[ band_idx ].size();

                        vector_mag_lims[ band_idx ][ axis_idx ] = 
                            TN::Vec2< float >( 
                                std::numeric_limits<float>::max(),
                               -std::numeric_limits<float>::max() );

                        // For each cell in the band
                        for( size_t i = 0; i < N_SITES_BAND; ++ i )
                        {
                            const double x_val = gdata[ band_idx ][ i * total_n_features + axis_idx * 3 + 0 ];
                            const double y_val = gdata[ band_idx ][ i * total_n_features + axis_idx * 3 + 1 ];
                            const double z_val = gdata[ band_idx ][ i * total_n_features + axis_idx * 3 + 2 ];

                            const float magnitude = std::sqrt( x_val*x_val + y_val*y_val + z_val*z_val );

                            vector_mag_lims[ band_idx ][ axis_idx ].a( 
                                std::min( vector_mag_lims[ band_idx ][ axis_idx ].a(),
                                magnitude )
                            );  

                            vector_mag_lims[ band_idx ][ axis_idx ].b( 
                                std::max( vector_mag_lims[ band_idx ][ axis_idx ].b(),
                                magnitude )
                            );  
                        }
                    }
                }
            }
        }

        std::cout << "Done initilized copying glyph data from summary data" << std::endl;

        ///////////////////////////////////////////////////////////////////

        // TODO : factor out, should be able to save space by storing only 
        // voronoi cell ids for each voxel, then hierarchically getting the 
        // component, and layer ids for each voxel

        const auto & vxComponents = m_tessellation.getVoxelComponentIds();
        const auto & vxLayers     = m_tessellation.getVoxelLayerIds();
        const int64_t NCC         = m_tessellation.getNComponents() + ( ! m_tessellation.hasBackground() );
        const auto vDims = m_dataManager.getVolumeDims();
        const size_t NV = vDims.x() * vDims.y() * vDims.z();

        m_filterSelectionState.componentLayerMap.resize( NCC );
        const auto & voxelComp = m_tessellation.getVoxelComponentIds();
        for( size_t i = 0; i < NV; ++i )
        {
            int32_t cIdx = vxComponents[ i ];

            if( cIdx < 0 || cIdx  >= NCC ) {
                std::cerr << "component id out of range " << cIdx << std::endl;
                exit( 1 );
            }

            m_filterSelectionState.componentLayerMap[ cIdx ] = vxLayers[ i ];
        }

        ////////////////////////////////////////////////////////////////////////////

        // createVoronoiCellMesh(
        //     m_tessellation.getClassification(),
        //     m_tessellation.getVoxelLayerIds(),
        //     TN::Vec2<float>::fromArrays( m_tessellation.getLayers() ),
        //     m_dataManager.getVolumeDims() );

        // Isosurfaces

        for( const auto & config : m_projectConfiguration.isosurfaceVisualizations )
        {
            const std::string dataKey = config.field;
            std::vector< float > * data = m_dataManager.get( dataKey );
            if( data == nullptr ) 
            {
                std::cerr << "Error: isosurface set: " << config.name << " defined on noexistent field " << dataKey << ". Ignoring." << std::endl;
                continue;
            }
            auto rng = m_dataManager.getRange( dataKey );
            double normalizationFactor = m_dataManager.getNormalizationFactor( dataKey );
            
            m_surfaceCollection.addIsosurfaceSet(  
                config,
                *data,
                m_dataManager.getVolumeDims(),
                rng.a(),
                ( rng.b() - rng.a() )
            );
        }

        /////////////////////////////////////////////////////////////////////////////////

        // site normals and gradients

        std::vector< float > * data = m_dataManager.get( m_projectConfiguration.lField );

        if( data == nullptr ) 
        {
            std::cerr << "Error: site normals: " << m_projectConfiguration.lField << " defined on noexistent field " << m_projectConfiguration.lField << ". Exiting." << std::endl;
            exit(1);
        }
        else
        {
            const auto & SP = m_tessellation.getSitePositions();
            const auto & DIMS = m_dataManager.getVolumeDims();
            
            m_siteNormals.resize(    SP.size() );

            for( int i = 0; i < SP.size() / 3; ++i )
            {
                auto r = estimateNormalPrincipleDirecton( 
                    { SP[ i*3 ], SP[ i*3+1 ], SP[ i*3+2 ] },
                    *data,
                    { DIMS.x(), DIMS.y(), DIMS.z() }
                );

                m_siteNormals[ i * 3     ] = r.x();
                m_siteNormals[ i * 3 + 1 ] = r.y();
                m_siteNormals[ i * 3 + 2 ] = r.z();
            }
        }

        /////////////////////////////////////////////////////////////////////////////////

        // Conditional Statistics Table

        std::string conditionalStatisticsTable = m_dataManager.generateConditionalStatisticsTable( 
            m_projectConfiguration.lField,
            m_tessellation.getLayers(),
            true );

        TN::generateEquationPNG(
            m_workingDir + "/../common/latex/templates/cropped_equation_template.tex",
            conditionalStatisticsTable,
            m_projectDirectory + "/latex_symbols/",
            "conditional_statistics",
            m_pdflatexDir,
            m_pdftocairoDir,
            m_convertDir,
            false,
            true
        );

        /////////////////////////////////////////////////////////////////////////////////

        status = 6;
    }

    Application(
        GLFWwindow * window,
        const std::string & workingDir,
        const std::string & pdflatexDir,
        const std::string & pdftocairoDir,
        const std::string & convertDir,
        const std::string & projectDirectory  ) :
            m_window( window ),
            m_windowWidth( 1920 ),
            m_windowHeight( 1080 ),
            m_renderer( { window }, workingDir + "/../common/render/" ),
            m_renderModule( { 100.f, 100.f }, { 100.f, 100.f }, workingDir + "/../common/render/" ),
            m_renderGroup3D( workingDir, workingDir + "/../common/render/" ),
            m_plotGroup( workingDir, projectDirectory ),
            m_filterGroup( workingDir ),
            m_textRenderer( workingDir + "/../common/render/"),
            m_sidePanel( workingDir, TN::TabViewOrientation::OpenRight ),
            m_projectDirectory( projectDirectory ),
            m_fileSelector(),
            m_projectConfigurationPanel( workingDir, & m_fileSelector )
    {
        m_3DViewConfigurationPanel.setFileSelector( & m_fileSelector, workingDir );

        ////////////////////////////////////
        // default 3d views

        m_renderGroup3D.minSize( 100, 200 );
        m_plotGroup.minSize(     100, 200 );

        ///////////////////////////////////////////

        m_workingDir =     workingDir;
        m_pdflatexDir =    pdflatexDir;
        m_pdftocairoDir =  pdftocairoDir;
        m_convertDir =     convertDir;

        m_activeJointPlot = -1;
        m_filterMode = Marrus::FilterOp::Type::New;

        m_textRenderer.init();

        // doc image

        m_helpButton.setTexFromPNG(
            m_workingDir + "/textures/keyboard.png",
            m_workingDir + "/textures/keyboard.png" );

        m_helpButton.resizeByHeight( 30 );

        m_show3DViewButton.setTexFromPNG(
            m_workingDir + "/textures/show3D.png",
            m_workingDir + "/textures/show3D.png" );

        m_show3DViewButton.resizeByHeight( 30 );  

        m_show2DViewButton.setTexFromPNG(
            m_workingDir + "/textures/plot30.png",
            m_workingDir + "/textures/plot30.png" );

        m_show2DViewButton.resizeByHeight( 30 );

        m_show2DViewButton.setPressed( true );
        m_show3DViewButton.setPressed(   true );

        std::vector< float > tData;
        TN::Vec2< int > tDims;

        TN::loadRGBA(
            m_workingDir + "/textures/keyMap2.png",
            tData,
            tDims );

        m_keyMapTexture.load( tData, tDims.a(), tDims.b(), false );

        std::vector< float > interval01(256);
        for( size_t i = 0, end = interval01.size(); i < end; ++i )
        {
            interval01[ i ] = ( double ) i / ( end - 1 );
        }
        m_01IntervalTexture.load( interval01, interval01.size(), 1 );

        // selection mode button group ///////////////////////////////

        m_setSelectButton.setTexFromPNG(
            m_workingDir + "/textures/set.png",
            m_workingDir + "/textures/setPressed.png" );
        m_setSelectButton.resizeByHeight( 30 );

        m_setUnionButton.setTexFromPNG(
            m_workingDir + "/textures/union.png",
            m_workingDir + "/textures/unionPressed.png" );
        m_setUnionButton.resizeByHeight( 30 );

        m_setIntersectionButton.setTexFromPNG(
            m_workingDir + "/textures/intersection.png",
            m_workingDir + "/textures/intersectionPressed.png" );
        m_setIntersectionButton.resizeByHeight( 30 );

        m_setDifferenceButton.setTexFromPNG(
            m_workingDir + "/textures/subtraction.png",
            m_workingDir + "/textures/subtractionPressed.png" );
        m_setDifferenceButton.resizeByHeight( 30 );

        m_setClearButton.setTexFromPNG(
            m_workingDir + "/textures/clear.png",
            m_workingDir + "/textures/clearPressed.png" );
        m_setClearButton.resizeByHeight( 30 );

        m_clusterButton.setTexFromPNG(
            m_workingDir + "/textures/clusters.png",
            m_workingDir + "/textures/clusters.png" );
        m_clusterButton.resizeByHeight( 30 );

        m_selectionModeButtonGroup =
        {
            & m_setSelectButton,
            & m_setUnionButton,
            & m_setIntersectionButton,
            & m_setDifferenceButton,
            & m_setClearButton
        };

        m_setSelectButton.setPressed( true );

        // panels

        m_sidePanel.addElement( 
            "systemConfigurationPanel",
            "System",
            & m_systemConfigurationPanel );

        m_sidePanel.addElement( 
            "projectConfigurationPanel",
            "Project",
            & m_projectConfigurationPanel );

        m_sidePanel.addElement( 
            "dataDefinitionPanel",
            " Data",
            & m_dataDefinitionPanel );

        m_sidePanel.addElement(
            "filterView",
            "Drilldown",
            & m_filterGroup );

        m_sidePanel.addElement( 
            "configurationPanelSet",
            "Configure",
            & m_configurationPanelSet );

        m_configurationPanelSet.addElement( 
            "jointPlotConfigurationPanel",
            & m_jointPlotConfigurationPanel );

        m_configurationPanelSet.addElement( 
            "view3DConfigurationPanel",
            & m_3DViewConfigurationPanel );

        m_sidePanel.addElement( 
            "exportPanel",
            "Export",
            & m_exportPanel );


        m_sidePanel.setActive( "projectConfigurationPanel" );
    }

    // void createVoronoiCellMesh(
    //     const std::vector< int32_t > & classifiction,
    //     const std::vector< int16_t > & voxelLayerIds,
    //     const std::vector< TN::Vec2< float > > & layers,
    //     const TN::Vec3< int > & volDims )
    // {
    //     auto & voronoiSurfaceSet = m_surfaceCollection.createSurfaceSet( "voronoi cells walls" );
    //     auto & surfaces = voronoiSurfaceSet->surfaces;

    //     std::cout << "extracting voronoi cell geometry " << std::endl;  

    //     float normFactor = m_dataManager.getNormalizationFactor( m_projectConfiguration.lField );
    //     for( int i = 0; i < layers.size(); ++i )
    //     {
    //         float va = layers[ i ].a() * normFactor;
    //         float vb = layers[ i ].b() * normFactor;

    //         std::string key = m_projectConfiguration.lField  + " band "
    //          + "[ " + TN::to_string_with_precision( va, 4 ) + ", " 
    //          +        TN::to_string_with_precision( vb, 4 ) + " )";

    //         surfaces.insert( { key, TN::SurfaceMesh() } );
            
    //         auto & mesh = surfaces.at( key );
    //         mesh.name  = key;
    //         mesh.color = TN::DEFAULT_ISO_COLORS[ i ];

    //         extractDiscreteGeometry(
    //             classifiction,
    //             voxelLayerIds,
    //             i,
    //             *m_dataManager.get( m_projectConfiguration.mField ),
    //             volDims.x(),
    //             volDims.y(),
    //             volDims.z(),
    //             m_dataManager.extent(),
    //             m_dataManager.xCoords(),
    //             m_dataManager.yCoords(),
    //             m_dataManager.zCoords(),
    //             mesh.verts,
    //             mesh.norms,
    //             mesh.scalars,
    //             false );

    //         mesh.scalarExtrema = TN::Sequential::getRange( mesh.scalars );
    //     }
    // }

    void clearProject()
    {
        //-----------------------------------------------------------------

        for( auto & b : m_selectionModeButtonGroup ) {
            b->setPressed( false );
        }
        m_setSelectButton.setPressed( true );
        m_filterMode = Marrus::FilterOp::Type::New;

        // m_show2DViewButton.setPressed( true );
        // m_show3DViewButton.setPressed( true );


        //-----------------------------------------------------------------
    
        m_surfaceCollection.clear();
        m_selectionSurfaces.clear();

        //-----------------------------------------------------------------

        m_mathTextures.clear();
        m_mathLatex.clear();

        //-----------------------------------------------------------------

        m_tessellation.clear();

        //-----------------------------------------------------------------

        m_dataManager.clear();

        //-----------------------------------------------------------------

        m_pointSize = 3; // TODO: factor

        //----------------------------------------------------------------
    
        m_renderGroup3D.clear();
        m_plotGroup.clear();

        //-----------------------------------------------------------------

        m_filterSelectionState.clear();

        //-----------------------------------------------------------------

        m_exportPanel.clear();
        m_projectConfigurationPanel.clear();
        m_dataDefinitionPanel.clear();
        m_jointPlotConfigurationPanel.clear();
        m_3DViewConfigurationPanel.clear();
        m_filterGroup.clear();
    }

    void loadProject( const std::string & configurationFile ) 
    {
        clearProject();

        ///////////////////////////////////////////////////////////////////

        std::string projDirectory = configurationFile;

        char c;
        while( projDirectory.size() && ( c = projDirectory.back() ) != '/' ) 
        { 
            projDirectory.pop_back(); 
        }

        setProjectDirectory( projDirectory );

        std::atomic< int > status( 0 );
        std::atomic< int > percent( 0 );

        std::thread td( 
            & Application::initialize,
            & (*this),
            std::ref( configurationFile ),
            std::ref( status  ),
            std::ref( percent ) );

        std::cout << "initialized application" << std::endl;

        // Setup the color TFs in the renderer ////////////////////////////
        // For now, since there is no UI, we will just select the first glyph

        while( status < 6 )
        {
            GL_CHECK_CALL( glfwMakeContextCurrent( m_window ) );  
            GL_CHECK_CALL( glfwPollEvents() );
            if( GL_CHECK_CALL_ASSIGN( glfwWindowShouldClose( m_window ) ) )
            {
                exit( 0 );
            }

            std::string message = 
                status == 0 ? "Initializing data manager..."
              : status == 1 ? "Extracting flame surface..."
              : status == 2 ? "Compiling latex..."
              : status == 3 ? "Computing distance function..."
              : status == 4 ? "Computing spatial decomposition..."
              : status == 5 ? "Computing geometry..."
              : "Aggregating and finalizing...";

            renderLoadingScreen( m_windowWidth, m_windowHeight, message, status, true, 0.7f );
            std::this_thread::sleep_for( std::chrono::milliseconds( 2 ) );
        }

        td.join();
    
        std::cout << "initialized" << std::endl;

        if( m_projectConfiguration.status != TN::ConfigStatus::Ok )
        {
            std::cout << "incomplete project configuration" << std::endl;
            return;
        }
        else
        {
            // Set the glyph based on the last one defined for now.
            // avoids starting with the default one which is at the front

            setGlyph( m_projectConfiguration.glyphs.back() );
        }

        std::cout << "configuring" << std::endl;

        m_dataDefinitionPanel.setTable( m_projectDirectory + "/latex_symbols/conditional_statistics.png" );

        configure();

        std::cout << "configured, now running system" << std::endl;
    }

    void setProjectDirectory( const std::string & projDirectory )
    {
        m_projectDirectory = projDirectory;
        m_plotGroup.setProjectDirectory( projDirectory );
    }

    void regatherElements(         
        double w,
        double h,
        bool updatePlots )
    {
        // Gather Voxels
 
        std::cout << "prepare to gather voxels" << std::endl;

        std::vector< std::vector< float > * > valsCopy;
        std::vector< std::vector< float > * > valsGathered;
        std::vector< std::vector< float > * > valsGatheredSub;

        for( auto & var : m_filterSelectionState.voxelGathered )
        {
            valsCopy.push_back( m_dataManager.get( var.first ) );
            valsGathered.push_back( & var.second );
            valsGatheredSub.push_back( &( m_filterSelectionState.voxelGatheredSubselection.at( var.first ) ) );
        }

        renderLoadingScreen( w, h, "gathering ...", 0, true, 0.7 );

        Marrus::FilterOp::gatherElements(
            m_filterSelectionState.voxelBitFlags,
            valsCopy,
            valsGathered,
            m_filterSelectionState.voxelGatheredFlags,
            m_filterSelectionState.voxelGatheredSubselectionIndices,
            valsGatheredSub,
            m_filterSelectionState.voxelGatheredFlagsSubselection );

        for( auto & var : m_filterSelectionState.voxelGathered )
        {
            // values and range for full selection
           const std::vector< float > & vf  = m_filterSelectionState.voxelGathered.at( var.first );
           const std::vector< float > & vs  = m_filterSelectionState.voxelGatheredSubselection.at( var.first );

            // values and range for sub selection
            TN::Vec2< float > & rf = m_filterSelectionState.voxelGatheredRange.at( var.first );
            TN::Vec2< float > & rs = m_filterSelectionState.voxelGatheredSubselectionRange.at( var.first );

            auto r = TN::Sequential::getRange( vf );
            rf = { r.x(), r.y() };   

            r = TN::Sequential::getRange( vs );  
            rs = { r.x(), r.y() };              
        }

        if( updatePlots ) {
            
            m_renderGroup3D.setAllViewsNeedUpdates();

            for( auto & plt : m_plotGroup.views() )
            {
                if( plt->undefined() )
                {
                    continue;
                }

                auto & xLims = m_filterSelectionState.voxelGatheredRange.at( plt->xKey() );
                auto & yLims = m_filterSelectionState.voxelGatheredRange.at( plt->yKey() );

                plt->setLims( xLims, yLims );
                plt->setUpdateAll();
            }
        }
    }

    std::pair< int, std::string >  update(
        double w,
        double h,
        const InputState & inputState,
        const std::pair< int, std::string > & status )
    {
        m_windowWidth  = w;
        m_windowHeight = h;

        //////////////////////////////////////////////////////////////////////////

        m_renderer.setWindow( 0, true );
        m_renderer.resize( w, h );

        //////////////////////////////////////////////////////////////////////////

        static double lastW = 0.0;
        static double lastH = 0.0;

        bool resized = false;

        bool sidePanelChanged     = m_sidePanel.handleInput(     inputState );
        bool visViewsShownChanged = updateVisSpace(              inputState ); 
        bool selectionModeChanged = updateControlSpace(          inputState );
        bool vewGroup3DChanged    = m_renderGroup3D.handleInput( inputState );

        bool view3DSelected       = m_renderGroup3D.activePlot() >= 0;
        if( view3DSelected ) 
        {
            m_plotGroup.deselect();
        }

        bool plotGroupChanged = false;
        if( m_show2DViewButton.isPressed() )
        {
            plotGroupChanged = m_plotGroup.handleInput( inputState );
        }

        bool plotViewSelected = m_plotGroup.activePlot() >= 0;
        if( plotViewSelected ) {
            m_renderGroup3D.deselect();
        }

        if( ! m_show2DViewButton.isPressed() )
        {
            m_plotGroup.deselect();
        }

        if( ! m_show3DViewButton.isPressed() )
        {
            m_renderGroup3D.deselect();
        }

        static TN::JointPlot2D * activeViewStatic = NULL;
        static TN::SpatialView3D * activeSpatialViewStatic = NULL;        

        if( m_sidePanel.active( "configurationPanelSet" ) )
        {
            if( plotViewSelected )
            {
                m_configurationPanelSet.set( "jointPlotConfigurationPanel" );

                m_jointPlotConfigurationPanel.updateVariableInformation(
                    m_dataManager.getVariableInformation(),
                    m_projectDirectory );

                TN::JointPlot2D * activeView = m_plotGroup.activeView();

                if( activeView != NULL )
                {
                    if(  activeView != activeViewStatic )
                    {
                        if( ! activeView->undefined() ) 
                        {
                            m_jointPlotConfigurationPanel.configure( 
                                activeView->xKey(), 
                                activeView->yKey(), 
                                activeView->wKey() );
                        }

                        activeViewStatic = activeView;
                    }
                    else if( m_jointPlotConfigurationPanel.waitingToProcess() )
                    {
                        TN::JointPlotConfiguration jcf 
                            = m_jointPlotConfigurationPanel.processChanges( activeView->configuration() );

                        bool newVariable = false;

                        if( ! m_filterSelectionState.voxelGathered.count( jcf.x ) 
                        ||  ! m_filterSelectionState.voxelGathered.count( jcf.y ) 
                        || ( jcf.w != "" && jcf.w != "None" && ! m_filterSelectionState.voxelGathered.count( jcf.w ) ) )
                        {
                            newVariable = true;
                        }

                        registerPlotVariable( jcf.x );
                        registerPlotVariable( jcf.y );

                        if( jcf.w != "" && jcf.w != "None" ) 
                        {
                            registerPlotVariable( jcf.w );
                        }

                        if( m_filterSelectionState.active && newVariable ) {
                            regatherElements( w, h, false );
                        }

                        if( activeView->undefined() )
                        {
                            activeView->initialize( jcf );
                        }
                        else 
                        {
                            activeView->updateConfiguration( jcf );
                        }

                        auto & xLims = m_filterSelectionState.voxelGatheredRange.at( jcf.x );
                        auto & yLims = m_filterSelectionState.voxelGatheredRange.at( jcf.y );

                        activeView->setLims( xLims, yLims );
                        activeView->setUpdateAll();
                    }
                }
            }
            else if ( view3DSelected )
            {
                m_renderGroup3D.updateInformation( m_surfaceCollection, m_tessellation.getNLayers() );

                m_configurationPanelSet.set( "view3DConfigurationPanel" );

                TN::SpatialView3D * activeView = m_renderGroup3D.activeView();

                if( activeView != NULL )
                {
                    auto layersScaled = m_tessellation.getLayers();
                    double scaleFactor = m_dataManager.scaleFactor( m_projectConfiguration.lField );
                    
                    for( size_t ib = 0; ib < layersScaled.size(); ++ib )
                    {
                        layersScaled[ ib ][ 0 ] *= scaleFactor;
                        layersScaled[ ib ][ 1 ] *= scaleFactor;
                    }

                    m_3DViewConfigurationPanel.updateInformation( 
                        m_surfaceCollection, 
                        m_workingDir, 
                        m_projectDirectory,
                        layersScaled,
                        activeView->configuration().glyphConfiguration,
                        m_projectConfiguration.glyphs );

                    if(  activeView != activeSpatialViewStatic )
                    {
                        m_3DViewConfigurationPanel.configure( 
                            activeView->configuration(), 
                            m_workingDir );

                        activeSpatialViewStatic = activeView;
                    }
                    else
                    {
                        if( m_3DViewConfigurationPanel.waitingToProcess() ) {
                            TN::SpatialViewConfiguration svc 
                                = m_3DViewConfigurationPanel.processChanges( activeView->configuration() );
                            activeView->configure( svc );
                        }

                        bool updateGlyphPreview = false;

                        if( m_3DViewConfigurationPanel.requestLoadGlyph() )
                        {
                            m_renderModule.setGlyph( m_3DViewConfigurationPanel.glyphPath() );
                            updateGlyphPreview = true;
                        }
                        else if( m_3DViewConfigurationPanel.requestLoadGlyphTexture() )
                        {
                            m_renderModule.setGlyphTexture( m_3DViewConfigurationPanel.glyphTexPath() );
                            updateGlyphPreview = true;
                        }

                        updateGlyphPreview |= m_3DViewConfigurationPanel.requestUpdateGlyphPreview();

                        if( updateGlyphPreview && m_glyphConfiguration.glyph_type == "experimental" )
                        {
                            TN::SpatialViewConfiguration config 
                                = m_3DViewConfigurationPanel.processChanges( activeView->configuration() );
                            
                            activeView->configure( config );

                            std::cout << "render glyph preview experimental" << std::endl;
                            // m_renderModule.renderGlyphPreview( 
                            //     config.colorMode,
                            //     m_3DViewConfigurationPanel.glyphPreviewSize() );

                            // std::vector< std::array< std::uint8_t, 3 > > data;
                            // int width, height;
                            // m_renderModule.getGlyphPreviewData( data, width, height );
                            // m_3DViewConfigurationPanel.setGlyphPreviewData( data, width, height );
                        }

                        m_3DViewConfigurationPanel.resolveRequests();
                        m_glyphPreviewNeedsUpdate = true;
                    }
                }
            }
        }
        else if ( m_sidePanel.active( "projectConfigurationPanel" ) )
        {
            if( m_projectConfigurationPanel.requestLoadProject() )
            {
                const std::string configurationFile = m_projectConfigurationPanel.getProjectFile();
                loadProject( configurationFile );

                m_projectConfigurationPanel.resolveRequests();     

                m_sidePanel.setActive( "filterView" );

                plotGroupChanged     = true;          
                vewGroup3DChanged    = true;
                sidePanelChanged     = true;
                visViewsShownChanged = true;
            }
        }

        // ========================================================

        TN::SimpleDataInfoSet rawDataInfo = m_projectConfiguration.getRawDataInfo();
        Marrus::MultiLevelSummarization summaryDataInfo;

        m_filterGroup.updateInformation( 
            m_expressionProcessor,
            rawDataInfo,
            summaryDataInfo,
            m_tessellation );

        if( m_filterGroup.requestApply() )
        {
            std::cout << "request apply" << std::endl;
 
            m_filterSelectionState.bandBitFlagsLast      = m_filterSelectionState.bandBitFlags;
            m_filterSelectionState.componentBitFlagsLast = m_filterSelectionState.componentBitFlags;
            m_filterSelectionState.cellBitFlagsLast      = m_filterSelectionState.cellBitFlags;
            m_filterSelectionState.voxelBitFlagsLast     = m_filterSelectionState.voxelBitFlags;

            m_filterGroup.apply(
                m_expressionProcessor,
                & m_dataManager,
                rawDataInfo,
                summaryDataInfo,
                m_filterSelectionState.componentLayerMap, // TODO refactor
                m_tessellation.getSiteComponentIds(),     // TODO refactor
                m_tessellation.getClassification(),       // TODO refactor
                m_filterSelectionState.bandBitFlags,
                m_filterSelectionState.componentBitFlags,
                m_filterSelectionState.cellBitFlags,
                m_filterSelectionState.voxelBitFlags );
            
            m_filterSelectionState.active = true;

            // ===============================================================================
            
            regatherElements( w, h, true );
        }

        // ========================================================

        if( std::abs( lastW - w ) >= 1 
         || std::abs( lastH - h ) >= 1 
         || sidePanelChanged 
         || vewGroup3DChanged
         || visViewsShownChanged
         || plotGroupChanged )
        {
            applyLayout( w, h );
            lastW = w;
            lastH = h;
            resized = true;
        }

        //////////////////////////////////////////////////////////////////////////        

        // interior ...

        m_renderer.clearScreen( { m_theme.backgroundColor.r(), m_theme.backgroundColor.g(), m_theme.backgroundColor.b(), 1.f } );

        Marrus::renderTabView( 
            m_sidePanel,
            m_renderer,
            m_textRenderer,
            w,
            h );

        renderControlSpace( w, h );

        if( m_sidePanel.isOpen() )
        {
            if( m_sidePanel.active( "configurationPanelSet" ) )
            {   
                if( plotViewSelected )
                {
                    m_configurationPanelSet.set( "jointPlotConfigurationPanel" );
                }
                else if( view3DSelected ) 
                {
                    m_configurationPanelSet.set( "view3DConfigurationPanel" );
                }
                else {
                    m_configurationPanelSet.deactivate();
                }

                if( m_configurationPanelSet.hasActivePanel() ) 
                {
                    Marrus::renderConfigurationPanel( 
                        m_configurationPanelSet.activePanel(),
                        m_renderer,
                        m_textRenderer,
                        w,
                        h );
                }
            }
            if( m_sidePanel.active( "projectConfigurationPanel" ) )
            {
                Marrus::renderConfigurationPanel( 
                    & m_projectConfigurationPanel,
                    m_renderer,
                    m_textRenderer,
                    w,
                    h );  
            }
            if( m_sidePanel.active( "filterView" ) )
            {
                Marrus::renderHierarchicalFilterGroup( 
                    m_filterGroup,
                    m_renderer,
                    m_textRenderer,
                    w,
                    h );
            }
            if( m_sidePanel.active( "dataDefinitionPanel" ) )
            {
                Marrus::renderConfigurationPanel( 
                    & m_dataDefinitionPanel,
                    m_renderer,
                    m_textRenderer,
                    w,
                    h );
            }
        }

        // 2D joint plot Views ///////////////////////////////////////////////////

        if( m_show2DViewButton.isPressed() ) {

            if( m_plotGroup.resizing() ) {
                m_renderer.clearBox(
                    m_plotGroup.position().x(),
                    m_plotGroup.position().y() + m_plotGroup.size().y() - 10.f,
                    m_plotGroup.size().x(),
                    10,
                    TN::Vec4( 1.0f, 0.6f, 0.4f, 1.0f ) );
            }
            else if( m_plotGroup.resizeHovered() ) {
                m_renderer.clearBox(
                    m_plotGroup.position().x(),
                    m_plotGroup.position().y() + m_plotGroup.size().y() - 10.f,
                    m_plotGroup.size().x(),
                    10,
                    TN::Vec4( 1.0f, 0.9f, 0.5f, 1.0f ) );
            }

            std::vector< TN::TexturedPressButton * > view2DButtons = m_plotGroup.buttons();
            for( TN::TexturedPressButton * b : view2DButtons ) {
                m_renderer.renderPressButton( b, { 0.2, 0.2, 0.2 }, w, h, true );
            }

            auto & plotViews = m_plotGroup.views();

            for( size_t i = 0; i < plotViews.size(); ++i )
            {
                auto & plt = *( plotViews[ i ] );

                if( plt.undefined() )
                {
                    continue;
                }

                if( plt.mouseClickedEvent() )
                {
                    m_activeJointPlot = i;
                }

                if( plt.leftMouseReleaseEvent() )
                {
                    Marrus::FilterOp::applyLasso( 
                        m_filterSelectionState.voxelGatheredFlagsSubselection,
                        m_filterSelectionState.voxelGatheredSubselection.at( plt.xKey() ),
                        m_filterSelectionState.voxelGatheredSubselection.at( plt.yKey() ),
                        plt.lasso(),
                        plt.lassoBBX(),
                        plt.lassoBBY(),
                        m_filterMode );

                    // Construct a 3D mesh encapsulating the voxels in the subselection //////////////

                    const auto & fs = m_filterSelectionState.voxelBitFlags;
                    
                    std::vector< float > tmp( m_filterSelectionState.voxelBitFlags.size(), 0 );
                    
                    const auto & vfs = m_filterSelectionState.voxelGatheredFlagsSubselection;
                    const auto & vis = m_filterSelectionState.voxelGatheredSubselectionIndices;

                    const size_t END = vis.size();

                    #pragma omp parallel for
                    for( size_t i = 0; i < END; ++i ) 
                    {
                        const auto IDX = vis[ i ];
                        tmp[ IDX ] = float( ( vfs[ i ] & Marrus::FilterOp::WITHIN_VOXEL_SUB_SELECTION ) != 0 );
                    }

                    m_dataManager.getSurface(
                        1/27.f,
                        tmp,
                        m_selectionSurfaces.voxelSelectionSurfaceMesh,
                        m_selectionSurfaces.voxelSelectionSurfaceNormals );

                    for( auto & pltT : plotViews )
                    {
                        pltT->setLassoPointsNeedsUpdate( true );
                    }

                    for( size_t vidx = 0; vidx < m_renderGroup3D.views().size(); ++vidx ) {
                        if( m_renderGroup3D.views()[ vidx ]->configuration().show_voxel_selection )
                        {
                            m_renderGroup3D.views()[ vidx ]->setNeedsUpdate();
                        }
                    }

                    ////////////////////////////////////////////////////////////////////////////////////
                }
            }

            for( size_t i = 0; i < plotViews.size(); ++i )
            {
                auto & plt = *( plotViews[ i ] );

                if( plt.undefined() || m_projectConfiguration.status != TN::ConfigStatus::Ok ) 
                {
                    GL_CHECK_CALL( glViewport( 0, 0, w, h ) );
                    m_renderer.clearBox(
                        plt.container().position().x() + 10,
                        plt.container().position().y() + 10,
                        plt.container().size().x() - 20,
                        plt.container().size().y() - 20,
                        TN::Vec4( 0.296, 0.33, 0.882, 0.9 ) );

                    std::string message = plt.undefined() ? "{ define me }" : "Waiting.";

                    m_textRenderer.renderText(
                        w,
                        h,
                        message,
                        {
                            plt.container().position().x() + plt.container().size().x() / 2.0 - 7.2f * message.size(),
                            plt.container().position().y() + plt.container().size().y() / 2.f - 10.f
                        },
                        { 0.85, 0.85, 0.85, 1.f }, 
                        false,
                        28 );

                    continue;
                }

                else
                {
                    bool supressUpdate = m_sidePanel.resizing() 
                                      || m_plotGroup.resizing() 
                                      || plt.size().x() < 10.0 
                                      || plt.size().y() < 10.0
                                      || ! m_filterSelectionState.active;

                    auto xR = m_filterSelectionState.voxelGatheredRange.at( plt.xKey() );
                    auto yR = m_filterSelectionState.voxelGatheredRange.at( plt.yKey() );

                    renderPlot(
                        plt,
                        w,
                        h,
                        m_filterSelectionState.voxelGathered.at( plt.xKey() ),
                        m_filterSelectionState.voxelGathered.at( plt.yKey() ),
                        m_dataManager.scaledValuesToTrueValues( xR, plt.xKey() ),
                        m_dataManager.scaledValuesToTrueValues( yR, plt.yKey() ),
                        m_filterSelectionState.voxelGatheredFlags,
                        m_filterSelectionState.voxelGatheredSubselection.at( plt.xKey() ),
                        m_filterSelectionState.voxelGatheredSubselection.at( plt.yKey() ), 
                        m_filterSelectionState.voxelGatheredFlagsSubselection,                                       
                        i == m_activeJointPlot && plt.probeActive(),
                        { 0.2, 0.2, 0.2, 0.5 },
                        true,
                        true,
                        m_pointSize,
                        supressUpdate ); 

                    plt.processChanges();
                }
            }

            auto pviewSwitchGlyph = m_plotGroup.getViewSwitchGlyph();
            if( pviewSwitchGlyph.size() > 2 ) {
                for( auto & p : pviewSwitchGlyph ) {
                    p = { p.x() * 2.f / w - 1.f, p.y() * 2.f / h - 1.f };
                }
                m_renderer.renderPrimitivesFlat(
                    pviewSwitchGlyph,
                    TN::Vec4( 0.2, 0.2, 0.2, 1.0 ),
                    GL_TRIANGLES
                );    
            }

            GL_CHECK_CALL( glLineWidth( 3 ) );
            GL_CHECK_CALL( glPointSize( 3 ) );

            auto activePlotViewLines = m_plotGroup.getActiveViewBorder();

            for( auto & line : activePlotViewLines ) {

                m_renderer.renderPrimitivesFlat(
                    { 
                        { ( line[ 0 ].x() - 1 ) * 2.f / w - 1.f, ( line[ 0 ].y() - 1 ) * 2.f / h - 1.f, },
                        { ( line[ 1 ].x() - 1 ) * 2.f / w - 1.f, ( line[ 1 ].y() - 1 ) * 2.f / h - 1.f, }            
                    },
                    TN::Vec4( 0.95, 0.75, 0.5, 1.0 ),
                    GL_LINES
                );
                
                m_renderer.renderPrimitivesFlat(
                    { 
                        { ( line[ 0 ].x() - 1 ) * 2.f / w - 1.f, ( line[ 0 ].y() - 1 ) * 2.f / h - 1.f, },
                        { ( line[ 1 ].x() - 1 ) * 2.f / w - 1.f, ( line[ 1 ].y() - 1 ) * 2.f / h - 1.f, }            
                    },
                    TN::Vec4( 0.95, 0.75, 0.5, 1.0 ),
                    GL_POINTS
                );    
            }

            m_renderer.renderPrimitivesFlat( 
                { 
                    { 
                        ( m_plotGroup.position().x()                          - 1 ) * 2.f / 2 - 1.f, 
                        ( m_plotGroup.position().y() + m_plotGroup.size().y() - 1 ) * 2.f / h - 1.f, 
                    },
                    { 
                        ( m_plotGroup.position().x() + m_plotGroup.size().x() - 1 ) * 2.f / w - 1.f, 
                        ( m_plotGroup.position().y() + m_plotGroup.size().y() - 1 ) * 2.f / h - 1.f, 
                    }            
                },
                TN::Vec4( 0.2, 0.2, 0.2, 0.1 ),
                GL_LINES
            );

            m_renderer.renderPrimitivesFlat( 
                { 
                    { 
                        ( m_plotGroup.position().x()                          - 1   ) * 2.f / w - 1.f, 
                        ( m_plotGroup.position().y() + m_plotGroup.size().y() - 10  ) * 2.f / h - 1.f, 
                    },
                    { 
                        ( m_plotGroup.position().x() + m_plotGroup.size().x() - 1  ) * 2.f / w - 1.f, 
                        ( m_plotGroup.position().y() + m_plotGroup.size().y() - 10 ) * 2.f / h - 1.f, 
                    }            
                },
                TN::Vec4( 0.2, 0.2, 0.2, 0.1 ),
                GL_LINES
            );
        }

        // /////////////////////////////////////////////////////////////
        // 3D view

        if( m_show3DViewButton.isPressed() ) {

            std::vector< TN::TexturedPressButton * > view3DButtons = m_renderGroup3D.buttons();
            for( TN::TexturedPressButton * b : view3DButtons ) {
                m_renderer.renderPressButton( b, { 0.2, 0.2, 0.2 }, w, h, true );
            }

            // Check if any view has changed and if so which one

            bool someViewStateChanged = false;
            size_t idxOfChangedView = 0;

            for( size_t vidx = 0; vidx < m_renderGroup3D.views().size(); ++vidx ) {
                bool vChanged = m_renderGroup3D.views()[ vidx ]->handleInput( inputState );
                if( vChanged ) {
                    idxOfChangedView = vidx;
                    someViewStateChanged = true;
                }
            }

            // check for key states to change global rendering parameters

            bool changedRenderingParameters = false;

            if( inputState.keyState.justPressed.count( "t" ) )
            {
                m_renderModule.cycleTF();
                changedRenderingParameters = true;
            }

            if( inputState.keyState.justPressed.count( "o" ) )
            {
                m_renderModule.cycleOcclusionRadius();
                changedRenderingParameters = true;
            }

            if( inputState.keyState.justPressed.count( "b" ) )
            {
                m_renderModule.cycleOcclusionBias();
                changedRenderingParameters = true;
            }

            if( inputState.keyState.justPressed.count( "e" ) )
            {
                m_renderModule.cycleSpecularExponent();
                changedRenderingParameters = true;
            }

            if( inputState.keyState.justPressed.count( "s" ) )
            {
                m_renderModule.cycleSpecular();
                changedRenderingParameters = true;
            }
            if( inputState.keyState.justPressed.count( "d" ) )
            {
                m_renderModule.cycleDiffuse();
                changedRenderingParameters = true;
            }

            if( inputState.keyState.justPressed.count( "a" ) )
            {
                m_renderModule.cycleAmbient();
                changedRenderingParameters = true;
            }

            if( inputState.keyState.justPressed.count( "k" ) )
            {
                m_renderModule.cycleKernelSize();
                changedRenderingParameters = true;
            }

            if( inputState.keyState.justPressed.count( "c" ) )
            {
                m_renderModule.compileShaders();
                changedRenderingParameters = true;
            }

            if( inputState.keyState.justPressed.count( "q" ) )
            {
                m_renderModule.cycleOcclusionScale();
                changedRenderingParameters = true;
            }

            TN::TrackBallCamera cam;
            TN::SpatialView3D * activeView = m_renderGroup3D.activeView();
            bool tryModifyCamera = false;

            bool haveActiveView =  activeView != NULL;
            if( haveActiveView )
            {
                cam = activeView->getCamera();
            }

            if( inputState.keyState.justPressed.count( "up" )    )
            {
                tryModifyCamera = true;
                cam.rotate( "up"    );    // rotate view 45 degrees
            }
            else if( inputState.keyState.justPressed.count( "down" ) )
            {
                tryModifyCamera = true;
                cam.rotate( "down"  );    // rotate view 45 degrees
            }
            else if( inputState.keyState.justPressed.count( "left" ) )
            {
                tryModifyCamera = true;
                cam.rotate( "right"  );    // rotate view 45 degrees
            }
            else if( inputState.keyState.justPressed.count( "right" ) )
            {
                tryModifyCamera = true;
                cam.rotate( "left" );    // rotate view 45 degrees
            }

            if( haveActiveView && tryModifyCamera )
            {
                activeView->setCamera( cam );
                activeView->setNeedsUpdate();

                for( size_t vidx = 0; vidx < m_renderGroup3D.views().size(); ++vidx ) {
                    if( m_renderGroup3D.views()[ vidx ].get() == activeView )
                    {
                        idxOfChangedView = vidx;
                        someViewStateChanged = true;
                        break;
                    }
                }
            }

            // Render Each View, link camera if in linked mode to the view that changed

            for( size_t vidx = 0; vidx < m_renderGroup3D.views().size(); ++vidx ) {

                auto & view = *m_renderGroup3D.views()[ vidx ];

                std::set< int > renderLayers;
                auto viewConfig = view.configuration(); 

                if( someViewStateChanged && m_renderGroup3D.linked() ) {
                    if( vidx != idxOfChangedView ) {
                        view.setCamera( 
                            m_renderGroup3D.views()[ idxOfChangedView ]->getCamera()
                        );
                    }
                }

                if( ! viewConfig.defined || m_projectConfiguration.status != TN::ConfigStatus::Ok ) 
                {
                    GL_CHECK_CALL( glViewport( 0, 0, w, h ) );
                    m_renderer.clearBox(
                        view.position().x() + 10,
                        view.position().y() + 10,
                        view.size().x() - 20,
                        view.size().y() - 20,
                        TN::Vec4( 0.8f, 0.81f, 0.9f, 0.2f ) );

                    std::string message = ! viewConfig.defined ? "Define me!" : "Waiting.";

                    m_textRenderer.renderText(
                        w,
                        h,
                        message,
                        {
                            view.position().x() + view.size().x() / 2.0 - message.size() * ( 18 / 2.0 ),
                            view.position().y() + view.size().y() / 2.f - 14.f
                        },
                        { 0.2, 0.2, 0.5, 1.f }, 
                        false,
                        16 );

                    std::cout << bool( m_projectConfiguration.status == TN::ConfigStatus::Ok ) << " " << bool( viewConfig.defined )  << std::endl;
                    exit(1);
                    continue;
                }

                if( m_projectConfiguration.status == TN::ConfigStatus::Ok && viewConfig.defined )
                {
                    if( changedRenderingParameters )
                    {
                        view.setNeedsUpdate();
                    }

                    //////////////////////////////////////////////////////////////////////////

                    bool suppressUpdate = 
                             m_sidePanel.resizing() 
                          || m_plotGroup.resizing() 
                          || view.size().x() < 20.0
                          || view.size().y() < 20.0;

                    // Marrus::renderSpatialView(
                    //     view,
                    //     m_renderModule,
                    //     m_renderer,
                    //     m_textRenderer,
                    //     m_surfaceCollection,
                    //     m_selectionSurfaces.componentSelectionSurfaceMesh,
                    //     m_selectionSurfaces.componentSelectionSurfaceNormals,
                    //     m_selectionSurfaces.componentSelectionSurfaceScalars,
                    //     m_selectionSurfaces.cellSelectionSurfaceMesh,
                    //     m_selectionSurfaces.cellSelectionSurfaceNormals,
                    //     m_selectionSurfaces.cellSelectionSurfaceScalars,
                    //     m_selectionSurfaces.voxelSelectionSurfaceMesh,
                    //     m_selectionSurfaces.voxelSelectionSurfaceNormals,
                    //     m_selectionSurfaces.voxelSelectionSurfaceScalars,
                    //     { 0.5, 0.5, 0.5 },
                    //     { 0.9, 0.7, 0.3 },
                    //     { 0.3, 0.6, 1.0 },
                    //     m_dataManager.extent(),
                    //     w,
                    //     h,
                    //     m_dataManager.getVolumeDims(),
                    //     suppressUpdate );

                    Marrus::renderSpatialViewWithGlyphs(

                        view,
                        
                        m_renderModule,
                        m_renderer,
                        m_textRenderer,
                        m_surfaceCollection,

                        view.configuration().glyphConfiguration,
                        m_glyphData,
                        m_glyphDataAverage,
                        m_glyphDataLims,
                        m_directionalGlyphMagnitudeLims,

                        m_3DViewConfigurationPanel.glyphPreviewSize(),
                        m_glyphPreviewNeedsUpdate,

                        m_tessellation.getSitePointsGrouped(),
                        m_tessellation.getSiteDirection1Grouped(),
                        m_tessellation.getSiteDirection2Grouped(),

                        // m_siteNormals,
                        m_tessellation.getSiteNormalGrouped(),
                        m_tessellation.getSiteGuassianCurvatureGrouped(),
                        m_tessellation.getSiteMeanCurvatureGrouped(),
                        
                        m_selectionSurfaces.componentSelectionSurfaceMesh,
                        m_selectionSurfaces.componentSelectionSurfaceNormals,
                        m_selectionSurfaces.componentSelectionSurfaceScalars,
                        
                        m_selectionSurfaces.cellSelectionSurfaceMesh,
                        m_selectionSurfaces.cellSelectionSurfaceNormals,
                        m_selectionSurfaces.cellSelectionSurfaceScalars,
                        
                        m_selectionSurfaces.voxelSelectionSurfaceMesh,
                        m_selectionSurfaces.voxelSelectionSurfaceNormals,
                        m_selectionSurfaces.voxelSelectionSurfaceScalars,
                        
                        { 250 / 255.0, 120  / 255.0, 80  / 255.0 },
                        { 250 / 255.0, 120  / 255.0, 80  / 255.0 },
                        { 250 / 255.0, 120  / 255.0, 80  / 255.0 },
                        
                        m_dataManager.extent(),
                        w,
                        h,
                        m_dataManager.getVolumeDims(),
                        suppressUpdate );

                    if( m_glyphConfiguration.glyph_type != "experimental" )
                    {
                        std::vector< std::array< std::uint8_t, 3 > > data;
                        int width, height;
                        m_renderModule.getGlyphPreviewData( data, width, height );
                        m_3DViewConfigurationPanel.setGlyphPreviewData( data, width, height );
                    }
                }
            }

            GL_CHECK_CALL( glDisable( GL_DEPTH_TEST ) );
            GL_CHECK_CALL( glClear( GL_DEPTH_BUFFER_BIT ) );

            bool render3DLinesOfSeparation = true;
            if( render3DLinesOfSeparation ) {

                GL_CHECK_CALL( glViewport( 0, 0, w, h ) );
                GL_CHECK_CALL( glLineWidth( 2 ) );
                GL_CHECK_CALL( glPointSize( 2 ) );

                // get lines 

                auto horizontelLines = m_renderGroup3D.getHorizontelLinesOfSeparation();
                auto verticalLines   = m_renderGroup3D.getVerticalLinesOfSeparation();

                for( auto line : horizontelLines ) {
                    m_renderer.renderPrimitivesFlat(
                        { 
                            { line[ 0 ].x() * 2.f / w - 1.f, ( line[ 0 ].y() - 2 )* 2.f / h - 1.f, },
                            { line[ 1 ].x() * 2.f / w - 1.f, ( line[ 1 ].y() - 2 )* 2.f / h - 1.f, }            
                        },
                        TN::Vec4( 0.9, 0.9, 0.9, 1.0 ),
                        GL_LINES
                    );

                    m_renderer.renderPrimitivesFlat(
                        { 
                            { line[ 0 ].x() * 2.f / w - 1.f, ( line[ 0 ].y() - 2 )* 2.f / h - 1.f, },
                            { line[ 1 ].x() * 2.f / w - 1.f, ( line[ 1 ].y() - 2 )* 2.f / h - 1.f, }            
                        },
                        TN::Vec4( 0.9, 0.9, 0.9, 1.0 ),
                        GL_POINTS
                    );

                    m_renderer.renderPrimitivesFlat(
                        { 
                            { line[ 0 ].x() * 2.f / w - 1.f, line[ 0 ].y() * 2.f / h - 1.f, },
                            { line[ 1 ].x() * 2.f / w - 1.f, line[ 1 ].y() * 2.f / h - 1.f, }            
                        },
                        TN::Vec4( 0.75, 0.75, 0.75, 1.0 ),
                        GL_LINES
                    );
                    
                    m_renderer.renderPrimitivesFlat(
                        { 
                            { line[ 0 ].x() * 2.f / w - 1.f, line[ 0 ].y() * 2.f / h - 1.f, },
                            { line[ 1 ].x() * 2.f / w - 1.f, line[ 1 ].y() * 2.f / h - 1.f, }            
                        },
                        TN::Vec4( 0.75, 0.75, 0.75, 1.0 ),
                        GL_POINTS
                    );
                }

                for( auto line : verticalLines ) {
                    m_renderer.renderPrimitivesFlat(
                        { 
                            { line[ 0 ].x() * 2.f / w - 1.f, line[ 0 ].y()* 2.f / h - 1.f, },
                            { line[ 1 ].x() * 2.f / w - 1.f, line[ 1 ].y()* 2.f / h - 1.f, }            
                        },
                        TN::Vec4( 0.9, 0.9, 0.9, 1.0 ),
                        GL_LINES
                    );

                    m_renderer.renderPrimitivesFlat(
                        { 
                            { line[ 0 ].x() * 2.f / w - 1.f, line[ 0 ].y()* 2.f / h - 1.f, },
                            { line[ 1 ].x() * 2.f / w - 1.f, line[ 1 ].y()* 2.f / h - 1.f, }            
                        },
                        TN::Vec4( 0.9, 0.9, 0.9, 1.0 ),
                        GL_POINTS
                    );

                    m_renderer.renderPrimitivesFlat(
                        { 
                            { ( line[ 0 ].x() - 2 ) * 2.f / w - 1.f, line[ 0 ].y() * 2.f / h - 1.f, },
                            { ( line[ 1 ].x() - 2 ) * 2.f / w - 1.f, line[ 1 ].y() * 2.f / h - 1.f, }            
                        },
                        TN::Vec4( 0.75, 0.75, 0.75, 1.0 ),
                        GL_LINES
                    );
                    
                    m_renderer.renderPrimitivesFlat(
                        { 
                            { ( line[ 0 ].x() - 2 ) * 2.f / w - 1.f, line[ 0 ].y() * 2.f / h - 1.f, },
                            { ( line[ 1 ].x() - 2 ) * 2.f / w - 1.f, line[ 1 ].y() * 2.f / h - 1.f, }            
                        },
                        TN::Vec4( 0.75, 0.75, 0.75, 1.0 ),
                        GL_POINTS
                    );
                }

                GL_CHECK_CALL( glLineWidth( 3 ) );
                GL_CHECK_CALL( glPointSize( 3 ) );

                auto activeViewLines = m_renderGroup3D.getActiveViewBorder();

                for( auto & line : activeViewLines ) {

                    m_renderer.renderPrimitivesFlat(
                        { 
                            { ( line[ 0 ].x() - 1 ) * 2.f / w - 1.f, ( line[ 0 ].y() - 1 ) * 2.f / h - 1.f, },
                            { ( line[ 1 ].x() - 1 ) * 2.f / w - 1.f, ( line[ 1 ].y() - 1 ) * 2.f / h - 1.f, }            
                        },
                        TN::Vec4( 0.95, 0.75, 0.5, 1.0 ),
                        GL_LINES
                    );
                    
                    m_renderer.renderPrimitivesFlat(
                        { 
                            { ( line[ 0 ].x() - 1 ) * 2.f / w - 1.f, ( line[ 0 ].y() - 1 ) * 2.f / h - 1.f, },
                            { ( line[ 1 ].x() - 1 ) * 2.f / w - 1.f, ( line[ 1 ].y() - 1 ) * 2.f / h - 1.f, }            
                        },
                        TN::Vec4( 0.95, 0.75, 0.5, 1.0 ),
                        GL_POINTS
                    );    
                }

                GL_CHECK_CALL( glLineWidth( 2 ) );
                GL_CHECK_CALL( glPointSize( 2 ) );

            }

            auto viewSwitchGlyph = m_renderGroup3D.getViewSwitchGlyph();
            if( viewSwitchGlyph.size() > 2 ) {
                for( auto & p : viewSwitchGlyph ) {
                    p = { p.x() * 2.f / w - 1.f, p.y() * 2.f / h - 1.f };
                }
                m_renderer.renderPrimitivesFlat(
                    viewSwitchGlyph,
                    TN::Vec4( 0.2, 0.2, 0.2, 1.0 ),
                    GL_TRIANGLES
                );    
            }
        }

        ////////////////////////////////////////////

        if( m_helpButton.pointInViewPort( inputState.mouseState.eventPosition ) )
        {
            if( inputState.mouseState.event == MouseEvent::LeftPressed )
            {
                m_helpButton.setPressed( ! m_helpButton.isPressed() );
            }
        }

        if( m_helpButton.isPressed() )
        {
            // renderKeyMap( w, h );
        }

        m_renderer.swapBuffers();

        return { 0, "" };
    }

    ~Application() {}
};

#endif