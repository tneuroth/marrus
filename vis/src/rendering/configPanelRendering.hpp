#ifndef TN_MARRUS_CONFIGURATION_PANEL_RENDERER_HPP
#define TN_MARRUS_CONFIGURATION_PANEL_RENDERER_HPP

#include "views/ConfigurationPanel.hpp"
#include "views/PressButtonWidget.hpp"
#include "views/ComboWidget.hpp"
#include "render/RenderModule2D/Renderer2D.hpp"
#include "render/TextRenderer/TextRenderer.hpp"
#include "geometry/Vec.hpp"

#include "render/GLErrorChecking.hpp"

#include <cstdint>
#include <vector>
#include <string>
#include <iostream>

namespace Marrus {

static inline void renderConfigurationPanel(
    TN::ConfigurationPanel * cp,
    TN::Renderer2D   & renderer,
    TN::TextRenderer & textRenderer,
    const float windowWidth,
    const float windowHeight )
{
    std::vector< TN::ComboWidget * >    combos          = cp->combos();
    std::vector< TN::PressButton * >    buttons         = cp->buttons();
    std::vector< TN::Label *       >    labels          = cp->labels();
    std::vector< TN::ExpressionEdit * > expressionEdits = cp->expressionEdits();
    std::vector< TN::SliderWidget   * > sliders         = cp->sliders();

    // Enable scissor test
    GL_CHECK_CALL( glEnable(GL_SCISSOR_TEST) );

    // Define the scissor box based on cp's position and size
    int scissorX = static_cast<int>(cp->position().x());
    int scissorY = static_cast<int>(cp->position().y());
    int scissorWidth = static_cast<int>(cp->size().x());
    int scissorHeight = static_cast<int>(cp->size().y());
    GL_CHECK_CALL( glScissor(scissorX, scissorY, scissorWidth, scissorHeight) );


    if( ! cp->isTransparent() )
    {
        GL_CHECK_CALL( glViewport( cp->position().x(), cp->position().y(), cp->size().x(), cp->size().y() ) );
        renderer.clear( { 1.0,1.0, 1.0, 1.0 } );
    }

    GL_CHECK_CALL( glViewport( 0, 0, windowWidth, windowHeight ) );

    textRenderer.renderText(
        windowWidth,
        windowHeight,
        cp->title(),
        { cp->position().x() + 20, cp->position().y() + cp->size().y() - 54 }, 
        { 0.55, 0.55, 0.55, 1.f },
        false,
        24 );

    for( auto & b : buttons ) 
    {
        renderer.renderPressButton( b, { 0.2, 0.2, 0.2 }, windowWidth, windowHeight, true );
    }

    for( auto & e : expressionEdits ) 
    {
        renderer.renderExpressionEdit( *e, windowWidth, windowHeight, { 0.2, 0.2, 0.2 } );
    }

    for( auto & l : labels ) 
    {
        if( l->bold )
        {
            textRenderer.renderText(
                windowWidth, 
                windowHeight,
                l->text,
                { l->position.x(), l->position.y() }, 
                { 0.0, 0.0, 0.0, 1.f },
                false,
                16 );
        }
        else
        {
            textRenderer.renderText(
                windowWidth, 
                windowHeight,
                l->text,
                { l->position.x(), l->position.y() }, 
                { 0.2, 0.2, 0.2, 1.f },
                false,
                16 );    
        }
    }

    for( auto & slider :  sliders )
    {
        renderer.renderSlider( *slider, windowWidth, windowHeight );
    }

    GL_CHECK_CALL( glDisable(GL_SCISSOR_TEST) );

    // first render the ones which aren't open, so that they are not rendered on top of the open one
    for( auto & c : combos ) 
    {
        if( ! c->isPressed() ) {
            renderer.renderCombo( *c, { 0.2, 0.2, 0.2 }, windowWidth, windowHeight );
        }
    }

    // now render the open one (should just be one that can be open at a time)
    for( auto & c : combos ) 
    {
        if( c->isPressed() ) {
            renderer.renderCombo( *c, { 0.2, 0.2, 0.2 }, windowWidth, windowHeight );
        }
    }
}

}

#endif
