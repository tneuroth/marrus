#ifndef TN_MARRUS_RENDERING_3D_HPP
#define TN_MARRUS_RENDERING_3D_HPP

#include "views/RenderGroup3D.hpp"
#include "render/RenderModule2D/Renderer2D.hpp"
#include "render/TextRenderer/TextRenderer.hpp"
#include "geometry/Vec.hpp"
#include "geometry/SurfaceSetCollection.hpp"
#include "configuration/GlyphConfiguration.hpp"

#include <cstdint>
#include <vector>
#include <string>
#include <iostream>

namespace Marrus {

static inline void renderAxis(
    TN::RenderModule & rm,
    float x0, float x, 
    float y0, float y, 
    float z0, float z,
    float lineWidth ) 
{

    std::vector< TN::Vec3< float > > xAxis =
    {
        { x0, y0, z0 }, { x,  y0, z0 },
    };

    std::vector< TN::Vec3< float > > yAxis =
    {
        { x0, y0, z0 }, { x0,  y, z0 },
    };

    std::vector< TN::Vec3< float > > zAxis =
    {
        { x0, y0, z0 }, { x0,  y0, z },
    };

    glLineWidth( 8 );

    rm.renderPrimitives(
        xAxis,
    { 238.f / 255.f, 136.f / 255.f, 40.f / 255.f },
    GL_LINES );

    rm.renderPrimitives(
        yAxis,
    { 40.f / 255.f, 187.f / 255.f, 150.f / 255.f },
    GL_LINES );

    rm.renderPrimitives(
        zAxis,
    { 40.f / 255.f, 150.f / 255.f, 221.f / 255.f },
    GL_LINES );

    glLineWidth( 2 );
}

static inline void renderBoundingBox(  
    TN::RenderModule & rm,
    float x0, float x, 
    float y0, float y, 
    float z0, float z,
    float lineWidth,
    bool renderAxis ) {

    std::vector< TN::Vec3< float > > box = renderAxis ? 
        std::vector< TN::Vec3< float > >( {
            { x,  y0, z0 }, { x,   y, z0 },
            { x,   y, z0 }, { x0,  y, z0 },
            { x0, y0, z },  { x,   y0, z },
            { x,  y0, z },  { x,    y, z },
            { x,   y, z },  { x0,   y, z },
            { x0,  y, z },  { x0,  y0, z },
            { x,  y0, z0 }, {  x,  y0, z },
            { x,   y, z0 }, {  x,   y, z },
            { x0,  y, z0 }, { x0,   y, z } } )
        : 
         std::vector< TN::Vec3< float > >( {
            { x0, y0, z0 }, { x,  y0, z0 },
            { x,  y0, z0 }, { x,   y, z0 },
            { x,   y, z0 }, { x0,  y, z0 },
            { x0,  y, z0 }, { x0, y0, z0 },
            { x0, y0, z },  { x,   y0, z },
            { x,  y0, z },  { x,    y, z },
            { x,   y, z },  { x0,   y, z },
            { x0,  y, z },  { x0,  y0, z },
            { x0, y0, z0 }, { x0,  y0, z },
            { x,  y0, z0 }, {  x,  y0, z },
            { x,   y, z0 }, {  x,   y, z },
            { x0,  y, z0 }, { x0,   y, z } } );

    glLineWidth( lineWidth );

    // rm.renderPrimitives(
    //     box,
    //     { 0.5, 0.5, 0.5 },
    //     GL_LINES );

    Marrus::renderAxis( rm, x0, x, y0, y, z0, z, lineWidth );
}

// static inline void render3DViewLegend(
//         TN::RenderViewConfigurable & view,
//         const TN::Renderer2D & renderer2D,
//         const TN::TextRenderer & textRenderer,  
//         float w,
//         float h,
//         const TN::Vec2< float > & rng )
//     {
        // float xSZ  = 490;
        // float ySZ  = 14;

        // float xM   = 18;
        // float yM   = 18;

        // float xPos = w - xSZ - xM;
        // float yPos = view.renderer()->position().y() + view.renderer()->size().y() - ySZ - yM;

        // float legendHeight = ySZ + yM;

        // renderer2D.clearBox(
        //     xPos  - xM,
        //     view.renderer()->position().y() + view.renderer()->size().y() - legendHeight,
        //     xSZ + 2*xM,
        //     legendHeight,
        //     view.renderer()->darkMode() ? TN::Vec4( 0.3f, 0.3f, 0.3f, 0.2f ) : TN::Vec4( 1.0f, 1.0f, 1.0f, 0.2f ) );

        // glViewport(
        //     xPos,
        //     yPos,
        //     xSZ,
        //     ySZ );

        // renderer2D.renderHeatMapTexture(
        //     m_01IntervalTexture.tex,
        //     0.0,
        //     1.0,
        //     view.renderer()->getTF() );

        // const std::string varKey = m_colorByLayer ? config.lField : config.mField;

        // float texRWidth = renderMath(
        //     varKey,
        //     { xPos, yPos + ySZ + 4 } );

        // std::string minValStr = TN::toStringWFMT(
        //                             m_dataManager.scaledValueToTrueValue( rng.a(), varKey ) );

        // glViewport( 0, 0, w, h );
        // textRenderer.renderText(
        //     false,
        //     w,
        //     h,
        //     minValStr,
        //     xPos,
        //     yPos - 16,
        //     1.0f,
        //     view.renderer()->darkMode() ? TN::Vec3<float>( 0.95f, 0.95f, 0.95f ) : TN::Vec3<float>( 0.2f, 0.2f, 0.2f ),
        //     false );

        // std::string maxValStr = TN::toStringWFMT(
        //                             m_dataManager.scaledValueToTrueValue( rng.b(), varKey ) );

        // float charW = 6.5;

        // glViewport( 0, 0, w, h );
        // textRenderer.renderText(
        //     false,
        //     w,
        //     h,
        //     maxValStr,
        //     xPos + xSZ - maxValStr.size() * charW,
        //     yPos - 16,
        //     1.0f,
        //     view.renderer()->darkMode() ? TN::Vec3<float>( 0.95f, 0.95f, 0.95f ) : TN::Vec3<float>( 0.2f, 0.2f, 0.2f ),
        //     false );
    // }

// static inline void render3DView(
//     TN::RenderViewConfigurable & view,

//     const TN::Renderer2D & renderer2D,
//     const TN::TextRenderer & textRenderer,    

//     const std::vector< std::uint8_t > & layersActiveFlags, 
    
//     const std::vector< std::vector< TN::Vec3< float > > > & levelVerts,
//     const std::vector< std::vector< TN::Vec3< float > > > & levelNorms,
//     const std::vector< std::vector< float > >             & levelScalars, 

//     const std::vector< std::vector< TN::Vec3< float > > > & cellVerts,
//     const std::vector< std::vector< TN::Vec3< float > > > & cellNorms,
//     const std::vector< std::vector< float > >             & cellScalars,   

//     const std::vector< TN::Vec3< float > > & componentSelectionVerts,
//     const std::vector< TN::Vec3< float > > & componentSelectionNorms,
//     const std::vector< float >             & componentSelectionScalars,   

//     const std::vector< TN::Vec3< float > > & cellSelectionVerts,
//     const std::vector< TN::Vec3< float > > & cellSelectionNorms,
//     const std::vector< float >             & cellSelectionScalars,   

//     const std::vector< TN::Vec3< float > > & voxelSelectionVerts,
//     const std::vector< TN::Vec3< float > > & voxelSelectionNorms,
//     const std::vector< float >             & voxelSelectionScalars,   

//     const TN::Vec3< float > & componentSelectionColor,
//     const TN::Vec3< float > & cellSelectionColor,
//     const TN::Vec3< float > & voxelSelectionColor,    

//     const std::vector< TN::Vec3< float > > & surfaceColorsCategorical,
//     const std::pair< TN::Vec3< float >, TN::Vec3< float > > & extent,
//     const TN::Vec2< float > & scalarMappingRange,
//     const std::string       & scalarKey, 
//     bool colorByScalar,
//     double windowWidth,
//     double windowHeight,
//     const TN::Vec3< int > & volDims,
//     bool update )
// {
//     auto config = view.configuration();

//     if( update ) {

//         view.renderer()->clear();

//         // levels and voronoi layers

//         for( int i = 0; i < layersActiveFlags.size(); ++i )
//         {
//             if( layersActiveFlags[ i ] )
//             {
//                 if( colorByScalar )
//                 {
//                     if( config.showLevels ) {
//                         view.renderer()->renderGeometry(
//                             levelVerts[ i ],
//                             levelNorms[ i ],
//                             levelScalars[ i ],
//                             scalarMappingRange,
//                             extent );
//                     }
//                     if( config.showCells ) { 
//                         view.renderer()->renderGeometry(
//                             cellVerts[ i ],
//                             cellNorms[ i ],
//                             cellScalars[ i ],
//                             scalarMappingRange,
//                             extent );
//                     }
//                     if( config.showLevels && i == (int) layersActiveFlags.size() - 1 ) {
//                         view.renderer()->renderGeometry(
//                             levelVerts[ i + 1 ],
//                             levelNorms[ i + 1 ],
//                             levelScalars[ i + 1 ],
//                             scalarMappingRange,
//                             extent );
//                     }
//                 }
//                 else
//                 {
//                     if( config.showLevels ) {
//                         view.renderer()->renderGeometry(
//                             levelVerts[ i ],
//                             levelNorms[ i ],
//                             surfaceColorsCategorical[ i ],
//                             extent );
//                     }
//                     if( config.showCells ) { 
//                         view.renderer()->renderGeometry(
//                             cellVerts[ i ],
//                             cellNorms[ i ],
//                             surfaceColorsCategorical[ i + 1 ],
//                             extent );
//                     }
//                     if( config.showLevels && i == (int) layersActiveFlags.size() - 1 ) {
//                         view.renderer()->renderGeometry(
//                             levelVerts[    i + 1 ],
//                             levelNorms[    i + 1 ],
//                             surfaceColorsCategorical[ i + 1 ],
//                             extent );
//                     }
//                 }   
//             }
//         }

//         // selections 

//         if( voxelSelectionVerts.size() > 0 && config.showVoxelSelection )
//         {
//             // view.renderer()->renderGeometry(
//             //     voxelSelectionVerts,
//             //     voxelSelectionNorms,
//             //     voxelSelectionScalars,
//             //     globalRange,
//             //     extent );

//             view.renderer()->renderGeometry(
//                 voxelSelectionVerts,
//                 voxelSelectionNorms,
//                 voxelSelectionColor,
//                 extent );

//         }
//         if( cellSelectionVerts.size() > 0 && config.showCellSelection )
//         {
//             // view.renderer()->renderGeometry(
//             //     cellSelectionVerts,
//             //     cellSelectionNorms,
//             //     cellSelectionScalars,
//             //     globalRange,
//             //     extent );

//             view.renderer()->renderGeometry(
//                 cellSelectionVerts,
//                 cellSelectionNorms,
//                 cellSelectionColor,
//                 extent );
//         }
//         if( componentSelectionVerts.size() > 0 && config.showComponentSelection )
//         {
//             // view.renderer()->renderGeometry(
//             //     componentSelectionVerts,
//             //     componentSelectionNorms,
//             //     componentSelectionScalars,
//             //     globalRange,
//             //     extent );

//             view.renderer()->renderGeometry(
//                 componentSelectionVerts,
//                 componentSelectionNorms,
//                 componentSelectionColor,
//                 extent );
//         }

//         // annotations

//         if( config.showBoundingBox || config.showAxis )
//         {
//             float NORM = std::max( std::max( volDims.x(), volDims.y() ), volDims.z() );

//             float x = volDims.x() / NORM - 0.5 * volDims.x() / NORM;
//             float y = volDims.y() / NORM - 0.5 * volDims.y() / NORM;
//             float z = volDims.z() / NORM - 0.5 * volDims.z() / NORM;

//             float x0 = -0.5 * volDims.x() / NORM;
//             float y0 = -0.5 * volDims.y() / NORM;
//             float z0 = -0.5 * volDims.z() / NORM;

//             if( config.showBoundingBox ) {
//                 Marrus::renderBoundingBox( view, x0, x, y0, y, z0, z, 6.f, renderAxis );
//             }
//         }
//     }

//     view.renderer()->renderSSAO();
// }

static inline void renderSpatialView(
    
    TN::SpatialView3D & view,

    TN::RenderModule & renderModule,
    const TN::Renderer2D & renderer2D,
    const TN::TextRenderer & textRenderer,    
    
    const TN::SurfaceSetCollection & isosurfaceCollection,

    const std::vector< TN::Vec3< float > > & componentSelectionVerts,
    const std::vector< TN::Vec3< float > > & componentSelectionNorms,
    const std::vector< float >             & componentSelectionScalars,   

    const std::vector< TN::Vec3< float > > & cellSelectionVerts,
    const std::vector< TN::Vec3< float > > & cellSelectionNorms,
    const std::vector< float >             & cellSelectionScalars,   

    const std::vector< TN::Vec3< float > > & voxelSelectionVerts,
    const std::vector< TN::Vec3< float > > & voxelSelectionNorms,
    const std::vector< float >             & voxelSelectionScalars,   

    const TN::Vec3< float > & componentSelectionColor,
    const TN::Vec3< float > & cellSelectionColor,
    const TN::Vec3< float > & voxelSelectionColor,    

    const std::pair< TN::Vec3< float >, TN::Vec3< float > > & extent,

    const double windowWidth,
    const double windowHeight,
    const TN::Vec3< int > & volDims,
    const bool supressUpdate )
{
    const TN::SpatialViewConfiguration & config = view.configuration();
    renderModule.setSize(     view.renderFrameSize().x(),     view.renderFrameSize().y()     );
    renderModule.setPosition( view.renderFramePosition().x(), view.renderFramePosition().y() );    
    renderModule.setCamera(   view.getCamera() );
    renderModule.clear();

    if( view.needsUpdate() && ! supressUpdate )
    {
        for( const auto & s : config.surface_set_visibility )
        {
            if( ! s.second ) 
            {
                continue;
            }

            if( ! isosurfaceCollection.hasSet( s.first ) )
            {
                std::cerr << "Error: Spatial view has isosurface set which doesn't exist in the collection: " << s.first << std::endl;
                continue;
            }

            const auto & surfaceSet = isosurfaceCollection.getSurfaceSet( s.first );
            const auto & surfaces = surfaceSet->surfaces;

            for( auto & sf : config.surface_visibility.at( s.first ) )
            {
                if( ! sf.second )
                {
                    continue;
                } 

                if( ! surfaces.count( sf.first ) )
                {
                    std::cerr << "Error: Spatial view has isosurface set which doesn't exist in the collection: " << sf.first  << std::endl;
                    continue;
                }

                const auto & mesh = surfaces.at( sf.first );
                renderModule.renderGeometry(
                    mesh.verts,
                    mesh.norms,
                    { mesh.color.r, mesh.color.g, mesh.color.b },
                    extent );
            }
        }

        if( config.show_bounding_box || config.show_axis )
        {
            float NORM = std::max( std::max( volDims.x(), volDims.y() ), volDims.z() );

            float x = volDims.x() / NORM - 0.5 * volDims.x() / NORM;
            float y = volDims.y() / NORM - 0.5 * volDims.y() / NORM;
            float z = volDims.z() / NORM - 0.5 * volDims.z() / NORM;

            float x0 = -0.5 * volDims.x() / NORM;
            float y0 = -0.5 * volDims.y() / NORM;
            float z0 = -0.5 * volDims.z() / NORM;

            if( config.show_bounding_box ) {
                Marrus::renderBoundingBox( renderModule, x0, x, y0, y, z0, z, 6.f, config.show_axis );
            }
        }

        renderModule.renderSSAO( view.uniqueId() );
        view.updated();
    }

    renderModule.renderViewTexture( view.uniqueId() );
}


static inline void renderSpatialViewWithGlyphs(
    
    TN::SpatialView3D & view,

    TN::RenderModule & renderModule,
    const TN::Renderer2D & renderer2D,
    const TN::TextRenderer & textRenderer,    
    
    const TN::SurfaceSetCollection & isosurfaceCollection,

    const TN::GlyphConfiguration & glyphConfiguration,
    const std::map< std::string, std::vector< std::vector< float > > > & glyphData,
    const std::map< std::string, std::vector< std::vector< float > > > & glyphDataAv,
    const std::map< std::string, std::vector< std::vector< TN::Vec2< float > > > >  & glyphDataLims,
    const std::map< std::string, std::vector< std::vector< TN::Vec2< float > > > >  & vectorMagLims,

    const TN::Vec2< float > & previewSize,
    bool & updatePreview,

    const std::vector< std::vector< float > > & sitePoints,
    
    const std::vector< std::vector< float > > & siteDirection1,
    const std::vector< std::vector< float > > & siteDirection2,
    const std::vector< std::vector< float > > & siteNormal,

    const std::vector< std::vector< float > > & siteGuassianCurvature,
    const std::vector< std::vector< float > > & siteMeanCurvature,

    const std::vector< TN::Vec3< float > > & componentSelectionVerts,
    const std::vector< TN::Vec3< float > > & componentSelectionNorms,
    const std::vector< float >             & componentSelectionScalars,   

    const std::vector< TN::Vec3< float > > & cellSelectionVerts,
    const std::vector< TN::Vec3< float > > & cellSelectionNorms,
    const std::vector< float >             & cellSelectionScalars,   

    const std::vector< TN::Vec3< float > > & voxelSelectionVerts,
    const std::vector< TN::Vec3< float > > & voxelSelectionNorms,
    const std::vector< float >             & voxelSelectionScalars,   

    const TN::Vec3< float > & componentSelectionColor,
    const TN::Vec3< float > & cellSelectionColor,
    const TN::Vec3< float > & voxelSelectionColor,    

    const std::pair< TN::Vec3< float >, TN::Vec3< float > > & extent,

    const double windowWidth,
    const double windowHeight,
    const TN::Vec3< int > & volDims,
    const bool supressUpdate )
{
    const TN::SpatialViewConfiguration & config = view.configuration();

    renderModule.setSize(     view.renderFrameSize().x(),     view.renderFrameSize().y() );
    renderModule.setPosition( view.renderFramePosition().x(), view.renderFramePosition().y() );    
    renderModule.setCamera(   view.getCamera() );
    
    if( config.light_background )
    {
        renderModule.clear( { 1.f, 1.f, 1.f } );
    }
    else
    {
        renderModule.clear();  
    }

    std::vector< TN::Vec2< float > > axes_lims(
        glyphConfiguration.axes.size(),
        TN::Vec2< float >( 
            std::numeric_limits<float>::max(),
           -std::numeric_limits<float>::max() )
    );

    TN::Vec2< float > vector_lims = 
        TN::Vec2< float >( 
            std::numeric_limits<float>::max(),
           -std::numeric_limits<float>::max() );

    int b_id = 0;
    bool doUpdate = view.needsUpdate() && ! supressUpdate;

    if( doUpdate )
    {
        for( const auto & s : config.surface_set_visibility )
        {
            if( ! s.second ) 
            {
                continue;
            }

            if( ! isosurfaceCollection.hasSet( s.first ) )
            {
                std::cerr << "Error: Spatial view has isosurface set which doesn't exist in the collection: " << s.first << std::endl;
                continue;
            }

            const auto & surfaceSet = isosurfaceCollection.getSurfaceSet( s.first );
            const auto & surfaces = surfaceSet->surfaces;

            for( auto & sf : config.surface_visibility.at( s.first ) )
            {
                if( ! sf.second )
                {
                    continue;
                } 

                if( ! surfaces.count( sf.first ) )
                {
                    std::cerr << "Error: Spatial view has isosurface set which doesn't exist in the collection: " << sf.first  << std::endl;
                    continue;
                }

                const auto & mesh = surfaces.at( sf.first );
                renderModule.renderGeometry(
                    mesh.verts,
                    mesh.norms,
                    { mesh.color.r, mesh.color.g, mesh.color.b },
                    extent );
            }
        }

        if( sitePoints.size() == config.glyphLayerVisibility.size() )
        {
            auto & gdata     = glyphData.at(     glyphConfiguration.name );
            auto & gdataLims = glyphDataLims.at( glyphConfiguration.name );
            auto & vMagLims  = vectorMagLims.at( glyphConfiguration.name );

            const size_t N_BANDS = gdata.size();

            const auto & surfaceSet = isosurfaceCollection.getSurfaceSet( "Bands" );
            const auto & surfaces   = surfaceSet->surfaces;

            for( int band_id = 0; band_id < N_BANDS; ++band_id )
            {
                if( config.glyphLayerVisibility[ band_id ] )
                {
                    axes_lims = std::vector< TN::Vec2< float > >(
                        glyphConfiguration.axes.size(),
                        TN::Vec2< float >( 
                            std::numeric_limits<float>::max(),
                           -std::numeric_limits<float>::max() )
                    );

                    vector_lims = 
                        TN::Vec2< float >( 
                            std::numeric_limits<float>::max(),
                           -std::numeric_limits<float>::max() );

                    if ( band_id+1 < surfaces.size() ) {
                        
                        auto it = surfaces.begin();
                        std::advance( it, band_id+1 ); 
                        const auto & mesh = (*it).second;
                        
                        renderModule.renderGeometry(
                            mesh.verts,
                            mesh.norms,
                            { mesh.color.r, mesh.color.g, mesh.color.b },
                            extent );
                    } 
                    else 
                    {
                        std::cout << "Voronoi Surface Index out of bounds!" << std::endl;
                    }
    
                    b_id = band_id;

                    if( glyphConfiguration.glyph_type == "radial" 
                     || glyphConfiguration.glyph_type == "star"
                     || glyphConfiguration.glyph_type == "directional" )
                    {
                        const size_t N_SITES = sitePoints[ band_id ].size() / 3;

                        // TODO: set data only when necessary

                        // std::cout << "setting data for " << gdata[ band_id ].size() / N_SITES << "features" << std::endl;

                        const size_t NTX = gdata[ band_id ].size();
                        const size_t NAV = glyphDataAv.at( glyphConfiguration.name )[ band_id ].size();
                        const auto & AV_DAT = glyphDataAv.at( glyphConfiguration.name )[ band_id ];

                        std::vector< float > texBufData( NTX + NAV );

                        for( size_t i = 0; i < NTX; ++i )
                        {
                            texBufData[ i ] = gdata[ band_id ][ i ];
                        }

                        for( size_t i = 0; i < NAV; ++i )
                        {
                            texBufData[ NTX + i ] = AV_DAT[ i ]; 
                        }

                        renderModule.setGlyphTextureData( 
                            texBufData,
                            texBufData.size() / ( N_SITES + 1 ),
                            ( N_SITES + 1 ) );

                        // renderModule.setGlyphTextureData( 
                        //     gdata[ band_id ],
                        //     gdata[ band_id ].size() / N_SITES,
                        //     N_SITES );

                        const size_t NA = glyphConfiguration.glyph_type == "directional" ? glyphConfiguration.vectors.size() 
                                        : glyphConfiguration.axes.size();
                        
                        const size_t NC = glyphConfiguration.glyph_type == "directional" ? 3 
                                        : glyphConfiguration.componentsPerAxis();

                        int bandNormStart = config.perBandNormalization ? band_id : 0;
                        int bandNormEnd   = config.perBandNormalization ? band_id + 1 : N_BANDS;

                        for( size_t ba = bandNormStart; ba < bandNormEnd; ++ba )
                        {
                            auto & dat = gdata[ ba ];

                            for( size_t ia = 0; ia < NA; ++ia )
                            {
                                if( glyphConfiguration.glyph_type == "directional" )
                                {
                                    vector_lims.a( 
                                        std::min( vector_lims.a(),
                                        vMagLims[ ba ][ ia ].a() ) ); 

                                    vector_lims.b( 
                                        std::max( vector_lims.b(),
                                        vMagLims[ ba ][ ia ].b() ) ); 
                                }
                                else
                                {
                                    const auto & axis = glyphConfiguration.axes[ ia ];

                                    // In this case, we will normalize each axis so that its bins (or the selected ones if that option were active) 
                                    // sum to 1,
                                    // and compute the min and max over all glyphs for mapping color

                                    if( config.perGlyphNormalization  )
                                    {
                                        for( size_t i = 0; i < N_SITES; ++i )
                                        {

                                            // get the max sum over all axis within the site, which we will normalize by in the case 
                                            // that local normalization and all axis normalization are selected
                                            
                                            float maxSum = 0.f;
                                            float minSum = 999999999999.f;

                                            if(  glyphConfiguration.glyph_type == "star" || glyphConfiguration.glyph_type == "radial" )
                                            {
                                                for( auto a_ : glyphConfiguration.active_axes )
                                                {
                                                    float sum = 0;

                                                    for( int c_idx = 0; c_idx < NC; ++c_idx )
                                                    {
                                                        sum += dat[ i * NA*NC + a_*NC + c_idx ];
                                                    }

                                                    //std::cout << "sum " << a_ << " " << sum << std::endl;

                                                    maxSum = std::max( sum, maxSum );
                                                    minSum = std::min( sum, minSum );
                                                }
                                            }

                                            //std::cout << "min max sums: " << minSum << " " << maxSum << std::endl;

                                            if( config.perScaleBinNormalization && glyphConfiguration.glyph_type == "radial" )
                                            {
                                                float sum = 0;

                                                for( auto c_idx : glyphConfiguration.active_components )
                                                {
                                                    sum += dat[ i * NA*NC + ia*NC + c_idx ];
                                                }

                                                if( sum > 0 )
                                                {
                                                    for( auto c_idx : glyphConfiguration.active_components )
                                                    {
                                                        axes_lims[ ia ].a( 
                                                            std::min( axes_lims[ ia ].a(),
                                                            dat[ i * NA*NC + ia*NC + c_idx ] / sum )
                                                        );  

                                                        axes_lims[ ia ].b( 
                                                            std::max( axes_lims[ ia ].b(),
                                                            dat[ i * NA*NC + ia*NC + c_idx ] / sum )
                                                        );  
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                if( config.allAxisNormalization && maxSum > 0 )
                                                {
                                                    for( size_t c_idx = 0; c_idx < NC; ++c_idx )
                                                    {
                                                        axes_lims[ ia ].a( 
                                                            std::min( axes_lims[ ia ].a(),
                                                            dat[ i * NA*NC + ia*NC + c_idx ] / maxSum )
                                                        );  

                                                        axes_lims[ ia ].b( 
                                                            std::max( axes_lims[ ia ].b(),
                                                            dat[ i * NA*NC + ia*NC + c_idx ] / maxSum )
                                                        );  
                                                    }    
                                                }
                                                else
                                                {
                                                    float sum = 0;
                                                    for( size_t c_idx = 0; c_idx < NC; ++c_idx )
                                                    {
                                                        sum += dat[ i * NA*NC + ia*NC + c_idx ];
                                                    }

                                                    // std::cout << "sum " << ia << " " << sum << std::endl;

                                                    if( sum > 0 ) {
                                                        for( size_t c_idx = 0; c_idx < NC; ++c_idx )
                                                        {
                                                            axes_lims[ ia ].a( 
                                                                std::min( axes_lims[ ia ].a(),
                                                                dat[ i * NA*NC + ia*NC + c_idx ] / sum )
                                                             );  

                                                            axes_lims[ ia ].b( 
                                                                std::max( axes_lims[ ia ].b(),
                                                                dat[ i * NA*NC + ia*NC + c_idx ] / sum )
                                                             );  
                                                        }   
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    
                                    else if( config.perScaleBinNormalization )
                                    {
                                        for( auto ic : glyphConfiguration.active_components )
                                        {
                                            axes_lims[ ia ].a( 
                                                std::min( axes_lims[  ia ].a(),
                                                gdataLims[ ba ][ ia*NC + ic ].a() )
                                            );

                                            axes_lims[ ia ].b( 
                                                std::max( axes_lims[  ia ].b(),
                                                gdataLims[ ba ][ ia*NC + ic ].b() )
                                            );  
                                        }
                                    }
                                    else
                                    {
                                        for( size_t ic = 0; ic < NC; ++ic )
                                        {
                                            axes_lims[ ia ].a( 
                                                std::min( axes_lims[  ia ].a(),
                                                gdataLims[ ba ][ ia*NC + ic ].a() )
                                            );  

                                            axes_lims[ ia ].b( 
                                                std::max( axes_lims[  ia ].b(),
                                                gdataLims[ ba ][ ia*NC + ic ].b() )
                                            );  
                                        }
                                    }
                                }
                            }
                        }

                        renderModule.renderRadialGlyphs(
                            glyphConfiguration,
                            config.glyphScale,
                            axes_lims,
                            vector_lims,
                            sitePoints[        band_id ],
                            siteDirection1[    band_id ],
                            siteDirection2[    band_id ],
                            siteNormal[        band_id ],
                            siteMeanCurvature[ band_id ],
                            { 0.9, 0.7, 0.6 },
                            config.colorMode,
                            extent,
                            config.orientation,
                            config.scaleMode,
                            config.perGlyphNormalization,
                            config.perScaleBinNormalization,
                            config.normalizeGlyphsFromZero,
                            config.allAxisNormalization,
                            config.showGlyphBorders,
                            false );
                    }
                    else if( glyphConfiguration.glyph_type == "experimental" )
                    {
                        renderModule.renderGlyphs3DTextured( 
                            sitePoints[        band_id ],
                            siteDirection1[    band_id ],
                            siteDirection2[    band_id ],
                            siteNormal[        band_id ],
                            siteMeanCurvature[ band_id ],
                            { 0.9, 0.7, 0.6 },
                            config.colorMode,
                            extent,
                            config.orientation );
                    }
                }
            }
        }

        if( voxelSelectionVerts.size() > 0 && config.show_voxel_selection )
        {
            // view.renderer()->renderGeometry(
            //     voxelSelectionVerts,
            //     voxelSelectionNorms,
            //     voxelSelectionScalars,
            //     globalRange,
            //     extent );

            renderModule.renderGeometry(
                voxelSelectionVerts,
                voxelSelectionNorms,
                voxelSelectionColor,
                extent );

        }
        if( cellSelectionVerts.size() > 0 && config.show_cell_selection )
        {
            // view.renderer()->renderGeometry(
            //     cellSelectionVerts,
            //     cellSelectionNorms,
            //     cellSelectionScalars,
            //     globalRange,
            //     extent );

            renderModule.renderGeometry(
                cellSelectionVerts,
                cellSelectionNorms,
                cellSelectionColor,
                extent );
        }
        if( componentSelectionVerts.size() > 0 && config.show_component_selection )
        {
            // view.renderer()->renderGeometry(
            //     componentSelectionVerts,
            //     componentSelectionNorms,
            //     componentSelectionScalars,
            //     globalRange,
            //     extent );

            renderModule.renderGeometry(
                componentSelectionVerts,
                componentSelectionNorms,
                componentSelectionColor,
                extent );
        }

        if( config.show_bounding_box || config.show_axis )
        {
            float NORM = std::max( std::max( volDims.x(), volDims.y() ), volDims.z() );

            float x = volDims.x() / NORM - 0.5 * volDims.x() / NORM;
            float y = volDims.y() / NORM - 0.5 * volDims.y() / NORM;
            float z = volDims.z() / NORM - 0.5 * volDims.z() / NORM;

            float x0 = -0.5 * volDims.x() / NORM;
            float y0 = -0.5 * volDims.y() / NORM;
            float z0 = -0.5 * volDims.z() / NORM;

            if( config.show_axis ) {
                Marrus::renderBoundingBox( renderModule, x0, x, y0, y, z0, z, 6.f, config.show_axis );
            }
        }

        renderModule.renderSSAO( view.uniqueId(), 
            config.light_background ? TN::Vec3<float>(   { 1.f, 1.f, 1.f } ) 
                                    : TN::Vec3< float >( { 0.1f, 0.1f, 0.1f } ) );

        view.updated();
    }

    renderModule.renderViewTexture( view.uniqueId() );

    if( updatePreview && doUpdate )
    {
        if( glyphConfiguration.glyph_type != "experimental" )
        {
            renderModule.renderGlyphPreview( 
                glyphConfiguration,
                previewSize,
                glyphDataAv.at( glyphConfiguration.name )[ b_id ],
                axes_lims,
                vector_lims,
                config.perGlyphNormalization,
                config.perScaleBinNormalization,
                config.normalizeGlyphsFromZero,
                config.allAxisNormalization,
                config.showGlyphBorders,
                config.scaleMode );
        }

        updatePreview = false;
    }
}

} // end namespace Marrus

#endif
