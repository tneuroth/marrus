#ifndef TN_MARRUS_TAB_VIEW_RENDERER_HPP
#define TN_MARRUS_TAB_VIEW_RENDERER_HPP

#include "views/TabView.hpp"
#include "render/RenderModule2D/Renderer2D.hpp"
#include "render/TextRenderer/TextRenderer.hpp"
#include "geometry/Vec.hpp"

#include "render/GLErrorChecking.hpp"

#include <cstdint>
#include <vector>
#include <string>
#include <iostream>

namespace Marrus {

static inline void renderTabView( 
    TN::TabView & tv,
    TN::Renderer2D & renderer,
    const TN::TextRenderer & textRenderer,

    const float windowWidth,
    const float windowHeight ) 
{
    GL_CHECK_CALL( glViewport( 0, 0, windowWidth, windowHeight ) );
    std::vector< TN::PressButton * > bts = tv.buttons();
    for( auto & b : bts ) {
        // renderer.renderPressButton( b, false );
        renderer.renderPressButton( b, { 0.2, 0.2, 0.2 }, windowWidth, windowHeight, true );
    } 

    if( tv.isOpen() ) { 
        // renderer.clearBox(
        //     tv.position().x(),
        //     tv.position().y(),
        //     tv.size().x(),
        //     tv.size().y() - 34,
        //     TN::Vec4( 1.0f, 1.0f, 0.7f, 0.2f ) );

        renderer.renderPrimitivesFlat( 
            { 
                { 
                    ( tv.position().x() + tv.size().x() - 1 ) * 2.f / windowWidth - 1.f, 
                    ( tv.position().y()                 - 1 ) * 2.f / windowHeight - 1.f, 
                },
                { 
                    ( tv.position().x() + tv.size().x() - 1 ) * 2.f / windowWidth - 1.f, 
                    ( tv.position().y() + tv.size().y() - 1 ) * 2.f / windowHeight - 1.f, 
                }            
            },
            TN::Vec4( 0.2, 0.2, 0.2, 0.1 ),
            GL_LINES
        );


        renderer.renderPrimitivesFlat( 
            { 
                { 
                    ( tv.position().x() + tv.size().x() - 10 ) * 2.f / windowWidth - 1.f, 
                    ( tv.position().y()                 - 1  ) * 2.f / windowHeight - 1.f, 
                },
                { 
                    ( tv.position().x() + tv.size().x() - 10 ) * 2.f / windowWidth - 1.f, 
                    ( tv.position().y() + tv.size().y() - 1 ) * 2.f / windowHeight - 1.f, 
                }            
            },
            TN::Vec4( 0.2, 0.2, 0.2, 0.1 ),
            GL_LINES
        );

        if( tv.resizing() ) {
            renderer.clearBox(
                tv.position().x() + tv.size().x() - 10,
                tv.position().y(),
                10,
                tv.size().y() - 34,
                TN::Vec4( 1.0f, 0.6f, 0.4f, 1.0f ) );
        }
        else if( tv.resizeHovered() ) {
            renderer.clearBox(
                tv.position().x() + tv.size().x() - 10,
                tv.position().y(),
                10,
                tv.size().y() - 34,
                TN::Vec4( 1.0f, 0.9f, 0.5f, 1.0f ) );
        }
    }
}

}

#endif
