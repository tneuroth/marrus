#ifndef TN_MARRUS_HIERARCHICAL_FILTER_RENDERING_HPP
#define TN_MARRUS_HIERARCHICAL_FILTER_RENDERING_HPP

#include "views/HierarchicalFilterGroup.hpp"
#include "render/RenderModule2D/Renderer2D.hpp"
#include "render/TextRenderer/TextRenderer.hpp"
#include "geometry/Vec.hpp"

#include "render/GLErrorChecking.hpp"

#include <cstdint>
#include <vector>
#include <string>
#include <iostream>

namespace Marrus {

    static inline void renderFilterModule(
        TN::FilterModuleContainer & v,
        TN::Renderer2D & renderer,
        TN::TextRenderer & textRenderer,
        const float windowWidth,
        const float windowHeight ) 
    {
        TN::Vec3< float > hc = v.headerColor();

        renderer.clearBox(
            v.position().x(),
            v.position().y() + v.size().y() - 36,
            v.size().x() - 2,
            34,
            TN::Vec4( hc.r(), hc.g(), hc.b(), 0.9f ) );

        GL_CHECK_CALL( glViewport( 0, 0, windowWidth, windowHeight ) );

        float top  = v.position().y() + v.size().y() - 24;
        float left = v.position().x() + 20;

        textRenderer.renderText(
            windowWidth,
            windowHeight,
            v.label(),
            { left + 30, top },
            { 0.0f, 0.0f, 0.0f, 1.f },
            false,
            18 );

        std::vector< TN::TexturedPressButton * > buttons = v.buttons();
        for( auto b : buttons ) {
            renderer.renderPressButton( b, { 0.2, 0.2, 0.2 }, windowWidth, windowHeight, true );
        }

        if( ! v.isCollapsed() )
        {
            auto & modules = v.modules();
            for( auto & m : modules )
            {
                m->render( renderer, textRenderer, windowWidth, windowHeight );
            }
        }
    }

    static inline void renderHierarchicalFilterGroup( 
        TN::HierarchicalFilterGroup & v,
        TN::Renderer2D & renderer,
        TN::TextRenderer & textRenderer,
        const float windowWidth,
        const float windowHeight )
    {
        renderer.renderPressButton( v.applyButton(), { 0.2, 0.2, 0.2 }, windowWidth, windowHeight, true ); 

        GL_CHECK_CALL( glViewport( 0, 0, windowWidth, windowHeight ) );

        textRenderer.renderText(
            windowWidth, 
            windowHeight,
            "Filter Pipeline Status",
            { v.position().x() + 12, v.position().y() + v.size().y() - 28 }, 
            { 0.2, 0.2, 0.2, 1.f },
            false,
            16 );

        // TODO: integrate status once pipeline is fully operational
        auto status = v.status();

        textRenderer.renderText(
            windowWidth, 
            windowHeight,
            status == 0 ? "Inactive" : "Active",
            { v.position().x() + 200, v.position().y() + v.size().y() - 28 }, 
            status == 0
              ? TN::Vec4( { 0.5, 0.3, 0.0, 1.f } ) :
                TN::Vec4( { 0.0, 0.5, 0.3, 1.f } ),
            false,
            16 );

        textRenderer.renderText(
            windowWidth, 
            windowHeight,
            "Filter Pipeline Status",
            { v.position().x() + 12, v.position().y() + v.size().y() - 28 }, 
            { 0.2, 0.2, 0.2, 1.f },
            false,
            16 );

        renderFilterModule( 
            v.bandFilterContainer(), 
            renderer,
            textRenderer,
            windowWidth,
            windowHeight );

        renderFilterModule( 
            v.componentFilterContainer(), 
            renderer,
            textRenderer,
            windowWidth,
            windowHeight );

        renderFilterModule( 
            v.cellFilterContainer(), 
            renderer,
            textRenderer,
            windowWidth,
            windowHeight );

        renderFilterModule( 
            v.voxelFilterContainer(), 
            renderer,
            textRenderer,
            windowWidth,
            windowHeight );

        // Combos need to be last because one may occlude other elements
        std::vector< TN::ComboWidget * > combos = v.visibleCombos();

        // Need to render unopened ones first
        for( TN::ComboWidget * c : combos )
        {
            if( ! c->isPressed() )
            {
                renderer.renderCombo( *c, { 0.2, 0.2, 0.2 }, windowWidth, windowHeight );
            }
        }

        // Then the open one
        for( TN::ComboWidget * c : combos )
        {
            if( c->isPressed() )
            {
                renderer.renderCombo( *c, { 0.2, 0.2, 0.2 }, windowWidth, windowHeight );
            }
        }
    }
}

#endif
