#ifndef MARRUS_PLOT_VIEW_MARGINALS_RENDER_HPP
#define MARRUS_PLOT_VIEW_MARGINALS_RENDER_HPP

#include "views/JointPlot2D.hpp"
#include "render/RenderModule2D/Renderer2D.hpp"
#include "render/TextRenderer/TextRenderer.hpp"
#include "geometry/Vec.hpp"
#include "filter/FilterOps.hpp"

#include "render/GLErrorChecking.hpp"

namespace Marrus {

static inline void renderMarginals(
    TN::Renderer2D & renderer,
    TN::JointPlot2D & plt ) 
{
    GL_CHECK_CALL( glViewport(
        plt.xMarginal().position().x(),
        plt.xMarginal().position().y(),
        plt.xMarginal().size().x(),
        plt.xMarginal().size().y() ) );

    renderer.renderPrimitivesColored(
        plt.xMarginal().geometry(),
        plt.xMarginal().colors(),
        GL_TRIANGLES );

    GL_CHECK_CALL( glViewport(
        plt.yMarginal().position().x(),
        plt.yMarginal().position().y(),
        plt.yMarginal().size().x(),
        plt.yMarginal().size().y() ) );

    renderer.renderPrimitivesColored(
        plt.yMarginal().geometry(),
        plt.yMarginal().colors(),
        GL_TRIANGLES );
}

static inline void renderMomentsOnMarginals(
    TN::Renderer2D & renderer,
    TN::JointPlot2D & plt ) 
{
	GL_CHECK_CALL( glLineWidth( 2 ) );

    float xMean = plt.xMarginal().mean();
    float xsdev = plt.xMarginal().sdev();

    float yMean = plt.yMarginal().mean();
    float ysdev = plt.yMarginal().sdev();

    {
        TN::Vec2< float > mrX = plt.xRange();
        float xsdev1 = ( ( ( xMean - xsdev ) - mrX.a() ) / ( mrX.b() - mrX.a() ) ) * 2 - 1;
        float xsdev2 = ( ( ( xMean + xsdev ) - mrX.a() ) / ( mrX.b() - mrX.a() ) ) * 2 - 1;
        float xmeanv = ( ( xMean - mrX.a() ) / ( mrX.b() - mrX.a() ) ) * 2 - 1;

        std::vector< TN::Vec2< float > > xMeanLine =
        {
            { xmeanv, -1.0 },
            { xmeanv,  1.0 }
        };

        std::vector< TN::Vec2< float > > xsdevLine1 =
        {
            { xsdev1, -1.0 },
            { xsdev1,  1.0 }
        };

        std::vector< TN::Vec2< float > > xsdevLine2 =
        {
            { xsdev2, -1.0 },
            { xsdev2,  1.0 }
        };

        GL_CHECK_CALL( glViewport(
            plt.xMarginal().position().x(),
            plt.xMarginal().position().y(),
            plt.xMarginal().size().x(),
            plt.xMarginal().size().y() ) );

        // std::cout << "rendering mean and std curves" << std::endl;

        renderer.renderPrimitivesFlat(
            xMeanLine,
            TN::Vec4( 0.7, 0.4, 0.41, 1 ),
            GL_LINES
        );

        renderer.renderPrimitivesFlat(
            xsdevLine1,
            TN::Vec4( 0.3, 0.5, 1.0, 1 ),
            GL_LINES
        );

        renderer.renderPrimitivesFlat(
            xsdevLine2,
            TN::Vec4( 0.3, 0.5, 1.0, 1 ),
            GL_LINES
        );
    }

    {
        TN::Vec2< float > mrY = plt.yRange();

        float ysdev1 = ( ( ( yMean - ysdev ) - mrY.a() ) / ( mrY.b() - mrY.a() ) ) * 2 - 1;
        float ysdev2 = ( ( ( yMean + ysdev ) - mrY.a() ) / ( mrY.b() - mrY.a() ) ) * 2 - 1;
        float ymeanv = ( (   yMean - mrY.a() ) / ( mrY.b() - mrY.a() ) ) * 2 - 1;

        std::vector< TN::Vec2< float > > yMeanLine =
        {
            { -1.0, ymeanv },
            {  1.0, ymeanv }
        };
        std::vector< TN::Vec2< float > > ysdevLine1 =
        {
            { -1.0, ysdev1 },
            {  1.0, ysdev1 }
        };
        std::vector< TN::Vec2< float > > ysdevLine2 =
        {
            { -1.0, ysdev2 },
            {  1.0, ysdev2 }
        };

        GL_CHECK_CALL( glViewport(
            plt.yMarginal().position().x(),
            plt.yMarginal().position().y(),
            plt.yMarginal().size().x(),
            plt.yMarginal().size().y() ) );

        renderer.renderPrimitivesFlat(
            yMeanLine,
            TN::Vec4( 0.7, 0.4, 0.41, 1 ),
            GL_LINES
        );

        renderer.renderPrimitivesFlat(
            ysdevLine1,
            TN::Vec4( 0.3, 0.5, 1.0, 1 ),
            GL_LINES
        );

        renderer.renderPrimitivesFlat(
            ysdevLine2,
            TN::Vec4( 0.3, 0.5, 1.0, 1 ),
            GL_LINES
        );
    }
}

static inline void renderSubMarginals(
    TN::Renderer2D & renderer,
    TN::JointPlot2D & plt,
    const TN::Vec4 & gridFGColor,
    const TN::Vec4 & gridBGColor,
    const TN::Vec2< int > & gridDims ) 
{
    if( plt.showSubMarginals() )
    {
        float sMin = std::min( plt.xsmTex().minValue, plt.ysmTex().minValue );
        float sMax = std::max( plt.xsmTex().maxValue, plt.ysmTex().maxValue );

        renderer.renderGrid(
            { plt.position().x(), plt.position().y() + plt.size().y() + 3 },
            { plt.size().x(), plt.xMarginal().position().y() - ( plt.position().y() + plt.size().y() ) - 5 },
            1,
            gridDims.x(),
            gridFGColor,
            gridBGColor );

        GL_CHECK_CALL( glViewport(
            plt.position().x(),
            plt.position().y() + plt.size().y() + 3,
            plt.size().x(),
            plt.xMarginal().position().y() - ( plt.position().y() + plt.size().y() ) - 5 ) );

        renderer.renderHeatMapTexture( plt.xsmTex().tex, sMin, sMax, true );

        renderer.renderGrid(
			{ plt.position().x() + plt.size().x() + 3, plt.position().y() },
			{ plt.yMarginal().position().x() - ( plt.position().x() + plt.size().x() ) - 5, plt.size().y() },
			gridDims.y(),
			1,
			gridFGColor,
			gridBGColor );

        GL_CHECK_CALL( glViewport(
            plt.position().x() + plt.size().x() + 3,
            plt.position().y(),
            plt.yMarginal().position().x() - ( plt.position().x() + plt.size().x() ) - 5,
            plt.size().y() ) );

        renderer.renderHeatMapTexture( plt.ysmTex().tex, sMin, sMax, true );
    }
}

}

#endif