#ifndef MARRUS_PLOT_CURVES_RENDER_HPP
#define MARRUS_PLOT_CURVES_RENDER_HPP

#include "views/JointPlot2D.hpp"
#include "render/RenderModule2D/Renderer2D.hpp"
#include "render/TextRenderer/TextRenderer.hpp"
#include "geometry/Vec.hpp"
#include "filter/FilterOps.hpp"

#include "render/GLErrorChecking.hpp"

namespace Marrus {

static inline void renderCurves(
    TN::Renderer2D & renderer,
    TN::JointPlot2D & plt,
    const double windowWidth,
    const double windowHeight ) 
{

        GL_CHECK_CALL( glDisable( GL_DEPTH_TEST ) );
        GL_CHECK_CALL( glViewport( plt.position().x(), plt.position().y(), plt.size().x(), plt.size().y() ) );

        std::vector< TN::Vec2< float > > meanLines;
        std::vector< TN::Vec2< float > > plusSdevLines;
        std::vector< TN::Vec2< float > > minusSdevLines;

        std::vector< TN::Vec2< float > > meanPoints;
        std::vector< TN::Vec2< float > > plusSdevPoints;
        std::vector< TN::Vec2< float > > minusSdevPoints;


//_____________________________________Mean Point_______________________________________

    GL_CHECK_CALL( glLineWidth( 2 ) );
    GL_CHECK_CALL( glPointSize( 2 ) );

    bool renderMeanPoint = true;
    if( renderMeanPoint )
    {
        const float xMean = plt.xMarginal().mean();
        const float yMean = plt.yMarginal().mean();

        GL_CHECK_CALL( glViewport(
            plt.position().x(),
            plt.position().y(),
            plt.size().x(),
            plt.size().y() ) );

        renderer.renderPrimitivesFlat(
        {
            { plt.xSubRange().a(), yMean }, { plt.xSubRange().b(), yMean },
            { xMean, plt.ySubRange().a() }, { xMean, plt.ySubRange().b() } },
            { 0.9, 0.2, 0.1, 0.5 },
            windowWidth,
            windowHeight,
            plt.xSubRange(),
            plt.ySubRange(),
            GL_LINES );
    }

//_____________________________________curves _______________________________________

        const TN::ConditionalCurves & curves = plt.conditionalCurves();

        for( int i = 0; i < curves.meanCurve.size(); ++i )
        {
            if( curves.counts[ i ] > 0 )
            {
                meanPoints.push_back(      { curves.binCenters[ i ], curves.meanCurve[ i ] } );
                plusSdevPoints.push_back(  { curves.binCenters[ i ], curves.meanCurve[ i ] + curves.standardDeviationCurve[ i ] } );
                minusSdevPoints.push_back( { curves.binCenters[ i ], curves.meanCurve[ i ] - curves.standardDeviationCurve[ i ] } );
            }

            if( i > 0 )
            {
                if( curves.counts[ i ] > 0 && curves.counts[ i - 1 ] > 0 )
                {
                    meanLines.push_back(      { curves.binCenters[ i ], curves.meanCurve[ i ] } );
                    plusSdevLines.push_back(  { curves.binCenters[ i ], curves.meanCurve[ i ] + curves.standardDeviationCurve[ i ] } );
                    minusSdevLines.push_back( { curves.binCenters[ i ], curves.meanCurve[ i ] - curves.standardDeviationCurve[ i ] } );

                    meanLines.push_back(      { curves.binCenters[ i - 1 ], curves.meanCurve[ i - 1 ] } );
                    plusSdevLines.push_back(  { curves.binCenters[ i - 1 ], curves.meanCurve[ i - 1 ] + curves.standardDeviationCurve[ i - 1 ] } );
                    minusSdevLines.push_back( { curves.binCenters[ i - 1 ], curves.meanCurve[ i - 1 ] - curves.standardDeviationCurve[ i - 1 ] } );
                }
            }
        }

        GL_CHECK_CALL( glLineWidth( 7 ) );
        GL_CHECK_CALL( glPointSize( 7 ) );

        renderer.renderPrimitivesFlat(
            meanLines,
            { 0.2, 0.2, 0.2, 1 },
            windowWidth,
            windowHeight,
            plt.xSubRange(),
            plt.ySubRange(),
            GL_LINES );

        renderer.renderPrimitivesFlat(
            meanLines,
            { 0.2, 0.2, 0.2, 1 },
            windowWidth,
            windowHeight,
            plt.xSubRange(),
            plt.ySubRange(),
            GL_POINTS );

        renderer.renderPrimitivesFlat(
            plusSdevLines,
            { 0.2, 0.2, 0.2, 1 },
            windowWidth,
            windowHeight,
            plt.xSubRange(),
            plt.ySubRange(),
            GL_LINES );


        renderer.renderPrimitivesFlat(
            plusSdevLines,
            { 0.2, 0.2, 0.2, 1 },
            windowWidth,
            windowHeight,
            plt.xSubRange(),
            plt.ySubRange(),
            GL_POINTS );

        renderer.renderPrimitivesFlat(
            minusSdevLines,
            { 0.2, 0.2, 0.2, 1 },
            windowWidth,
            windowHeight,
            plt.xSubRange(),
            plt.ySubRange(),
            GL_LINES );

        renderer.renderPrimitivesFlat(
            minusSdevLines,
            { 0.2, 0.2, 0.2, 1 },
            windowWidth,
            windowHeight,
            plt.xSubRange(),
            plt.ySubRange(),
            GL_POINTS );

        GL_CHECK_CALL( glLineWidth( 2 ) );
        GL_CHECK_CALL( glPointSize( 2 ) );

        renderer.renderPrimitivesFlat(
            meanLines,
            { 0.9, 0.8, 0.4, 1 },
            windowWidth,
            windowHeight,
            plt.xSubRange(),
            plt.ySubRange(),
            GL_LINES );

        renderer.renderPrimitivesFlat(
            meanLines,
            { 0.2, 0.2, 0.2, 1 },
            windowWidth,
            windowHeight,
            plt.xSubRange(),
            plt.ySubRange(),
            GL_POINTS );

        GL_CHECK_CALL( glLineWidth( 2 ) );
        GL_CHECK_CALL( glPointSize( 2 ) );
        // renderer.renderPrimitivesFlat(
        //     meanLines,
        // { 0.7, 0.4, 0.41, 1 },
        // windowWidth,
        // windowHeight,
        // plt.xSubRange(),
        // plt.ySubRange(),
        // GL_LINES );

        renderer.renderPrimitivesFlat(
            plusSdevLines,
            { 0.6, 0.8, 1.0, 1 },
            windowWidth,
            windowHeight,
            plt.xSubRange(),
            plt.ySubRange(),
            GL_LINES );

        renderer.renderPrimitivesFlat(
            plusSdevLines,
            { 0.2, 0.2, 0.2, 1 },
            windowWidth,
            windowHeight,
            plt.xSubRange(),
            plt.ySubRange(),
            GL_POINTS );

        renderer.renderPrimitivesFlat(
            minusSdevLines,
            { 0.6, 0.8, 1.0, 1 },
            windowWidth,
            windowHeight,
            plt.xSubRange(),
            plt.ySubRange(),
            GL_LINES );

        renderer.renderPrimitivesFlat(
            minusSdevLines,
            { 0.2, 0.2, 0.2, 1 },
            windowWidth,
            windowHeight,
            plt.xSubRange(),
            plt.ySubRange(),
            GL_POINTS );

        GL_CHECK_CALL( glPointSize( 6 ) );

        // renderer.renderPrimitivesFlat(
        //     meanPoints,
        //     { 0.7, 0.4, 0.41, 1 },
        //     windowWidth,
        //     windowHeight,
        //     plt.xSubRange(),
        //     plt.ySubRange(),
        //     GL_POINTS );

        // renderer.renderPrimitivesFlat(
        //     plusSdevPoints,
        //     { 0.3, 0.5, 1.0, 1 },
        //     windowWidth,
        //     windowHeight,
        //     plt.xSubRange(),
        //     plt.ySubRange(),
        //     GL_POINTS );

        // renderer.renderPrimitivesFlat(
        //     minusSdevPoints,
        //     { 0.3, 0.5, 1.0, 1 },
        //     windowWidth,
        //     windowHeight,
        //     plt.xSubRange(),
        //     plt.ySubRange(),
        //     GL_POINTS );

}

}

#endif