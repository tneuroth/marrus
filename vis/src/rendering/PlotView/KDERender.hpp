#ifndef MARRUS_PLOT_KDE_RENDER_HPP
#define MARRUS_PLOT_KDE_RENDER_HPP

#include <fftw3.h> 

#include "views/JointPlot2D.hpp"
#include "render/RenderModule2D/Renderer2D.hpp"

#include "render/GLErrorChecking.hpp"

namespace Marrus {

static inline void renderKde(
    TN::Renderer2D & renderer,
    TN::JointPlot2D & plt,
    const double windowWidth,
    const double windowHeight )
{
    GL_CHECK_CALL( glDisable( GL_DEPTH_TEST ) );
    GL_CHECK_CALL( glViewport( plt.position().x(), plt.position().y(), plt.size().x(), plt.size().y() ) );
    renderer.renderHeatMapTexture( plt.kdeTexture().tex,   plt.kdeRange().a(),  plt.kdeRange().b() );
}

}

#endif