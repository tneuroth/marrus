#ifndef MARRUS_PLOT_ZOOM_LEVEL_RENDER_HPP
#define MARRUS_PLOT_ZOOM_LEVEL_RENDER_HPP

#include "views/JointPlot2D.hpp"
#include "render/RenderModule2D/Renderer2D.hpp"
#include "geometry/Vec.hpp"

#include "render/GLErrorChecking.hpp"

namespace Marrus {

void renderZoomLevelIndicators( 
	TN::Renderer2D & renderer,
    TN::JointPlot2D & plt,
    double windowWidth,
    double windowHeight,
    const TN::Vec4 & fadeColor )
{
    TN::Vec2< float > linePosX = plt.xMarginal().getRangeSelectionPositions();

    std::vector< TN::Vec2< float > > lineIndicatorsX;
    std::vector< TN::Vec2< float > > lineIndicatorsArrowsX;

    constructLineIndicators(
        linePosX,
        plt.xMarginal().position(),
        plt.xMarginal().size(),
        plt.position(),
        plt.size(),
        { windowWidth, windowHeight },
        lineIndicatorsX,
        lineIndicatorsArrowsX,
        "x" );

    GL_CHECK_CALL( glViewport( 0, 0, windowWidth, windowHeight ) );
    GL_CHECK_CALL( glLineWidth( 1 ) );
    GL_CHECK_CALL( glPointSize( 1 ) );

    renderer.renderPrimitivesFlat(
        lineIndicatorsX,
        TN::Vec4( .0, .0, .0, 1 ),
        GL_POINTS
    );
    renderer.renderPrimitivesFlat(
        lineIndicatorsX,
        TN::Vec4( .0, .0, .0, 1 ),
        GL_LINES
    );

    renderer.renderPrimitivesFlat(
        lineIndicatorsArrowsX,
        TN::Vec4( .0, .0, .0, 1 ),
        GL_TRIANGLES
    );

    GL_CHECK_CALL( glViewport(
        plt.xMarginal().position().x(),
        plt.xMarginal().position().y(),
        linePosX.a() - plt.xMarginal().position().x(),
        plt.xMarginal().size().y() ) );

    renderer.clear( fadeColor );

    GL_CHECK_CALL( glViewport(
        linePosX.b(),
        plt.xMarginal().position().y(),
        plt.xMarginal().position().x() + plt.xMarginal().size().x() - linePosX.b(),
        plt.xMarginal().size().y() ) );

    renderer.clear( fadeColor );

    // y

    TN::Vec2< float > linePosY = plt.yMarginal().getRangeSelectionPositions();

    std::vector< TN::Vec2< float > > lineIndicatorsY;
    std::vector< TN::Vec2< float > > lineIndicatorsArrowsY;

    constructLineIndicators(
        linePosY,
        plt.yMarginal().position(),
        plt.yMarginal().size(),
        plt.position(),
        plt.size(),
        { windowWidth, windowHeight },
        lineIndicatorsY,
        lineIndicatorsArrowsY,
        "y" );

    GL_CHECK_CALL( glViewport( 0, 0, windowWidth, windowHeight ) );
    GL_CHECK_CALL( glLineWidth( 1 ) );
    GL_CHECK_CALL( glPointSize( 1 ) );

    renderer.renderPrimitivesFlat(
        lineIndicatorsY,
        TN::Vec4( .0, .0, .0, 1 ),
        GL_POINTS
    );
    renderer.renderPrimitivesFlat(
        lineIndicatorsY,
        TN::Vec4( .0, .0, .0, 1 ),
        GL_LINES
    );

    renderer.renderPrimitivesFlat(
        lineIndicatorsArrowsY,
        TN::Vec4( .0, .0, .0, 1 ),
        GL_TRIANGLES
    );

    GL_CHECK_CALL( glViewport(
        plt.yMarginal().position().x(),
        plt.yMarginal().position().y(),
        plt.yMarginal().size().x(),
        linePosY.a() - plt.yMarginal().position().y() ) );

    renderer.clear( fadeColor );

    GL_CHECK_CALL( glViewport(
        plt.yMarginal().position().x(),
        linePosY.b(),
        plt.yMarginal().size().x(),
        plt.yMarginal().position().y() + plt.yMarginal().size().y() - linePosY.b() ) );

    renderer.clear( fadeColor );
}

}

#endif