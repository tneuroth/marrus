#ifndef MARRUS_PLOT_VIEW_PLOT_SUB_SELECTION_RENDER_HPP
#define MARRUS_PLOT_VIEW_PLOT_SUB_SELECTION_RENDER_HPP

#include "views/JointPlot2D.hpp"
#include "render/RenderModule2D/Renderer2D.hpp"
#include "render/TextRenderer/TextRenderer.hpp"
#include "geometry/Vec.hpp"
#include "filter/FilterOps.hpp"

#include "render/GLErrorChecking.hpp"

namespace Marrus {

static inline void renderSelectionBoundaries(
    TN::Renderer2D & renderer,
    TN::TextRenderer & textRenderer,  
    TN::JointPlot2D & plt,
    const double windowWidth,
    const double windowHeight,
    const std::vector< float > & xSub,
    const std::vector< float > & ySub,
    const std::vector< int   > & flagsSub,
    const TN::Vec2< float > & xRange,
    const TN::Vec2< float > & yRange,
    const TN::Vec4 & color,
    const float pointSize,
    const bool selectorActive,
    const bool supressUpdate )
{
    const int CHARWIDTH = 7;
//______________________________Selection Boundaries_______________________________________

    if( selectorActive )
    {
        TN::Vec2< float > rX = plt.xSubRange();
        TN::Vec2< float > rY = plt.ySubRange();

        auto vxR = rX * ( xRange.b() - xRange.a() ) + xRange.a();
        auto vyR = rY * ( yRange.b() - yRange.a() ) + yRange.a();

        if( plt.probeType() == TN::ProbeType::Box )
        {
            GL_CHECK_CALL( glViewport(
                plt.position().x(),
                plt.position().y(),
                plt.size().x(),
                plt.size().y() ) );

            GL_CHECK_CALL( glLineWidth( 2 ) );
            GL_CHECK_CALL( glPointSize( 2 ) );
            renderer.render( { plt.boxLines( false ) }, { 0.2, 0.2, 0.2, 1.0 }, GL_LINES  );
            GL_CHECK_CALL( glLineWidth( 1 ) );

            // x axis part
            auto box = plt.xBoxRange();
            for( int i = 0; i < 2; ++i )
            {
                float p = i == 0 ? plt.xBoxRange().a() : plt.xBoxRange().b();
                float value = vxR.a() * ( 1.0 - p ) + vxR.b() *  p;

                std::string text = TN::toStringWFMT( value );
                float offset = i == 0 ? text.length() * CHARWIDTH : 0;
                textRenderer.renderText(
                    windowWidth,
                    windowHeight,
                    text,
                    { plt.position().x() + plt.size().x() * p - offset, plt.position().y() - 30 },
                    { 0.55, 0.31, 0.15, 1.f },
                    false );
            }

            for( int i = 0; i < 2; ++i )
            {
                float p = i == 0 ? plt.yBoxRange().a() : plt.yBoxRange().b();
                float value = vyR.a() * ( 1.0 - p ) + vyR.b() *  p;
                std::string text = TN::toStringWFMT( value );
                float offset = i == 0 ? text.length() * CHARWIDTH : 0;
                textRenderer.renderText(
                    windowWidth,
                    windowHeight,
                    text,
                    { plt.position().x() - 24, plt.position().y() + plt.size().y() * p - offset },
                    { 0.55, 0.31, 0.15, 1.f },
                    true );
            }
        }
        
        if( plt.probeType() == TN::ProbeType::Lasso )
        {
            auto lassoPoints = plt.lasso();
         
            if( lassoPoints.size() > 2 )
            {
                GL_CHECK_CALL( glDisable( GL_DEPTH_TEST ) );
                GL_CHECK_CALL( glLineWidth( 2 ) );
                GL_CHECK_CALL( glViewport(
                    plt.position().x(),
                    plt.position().y(),
                    plt.size().x(),
                    plt.size().y() ) );

                lassoPoints.push_back( lassoPoints.front() );
                renderer.renderPrimitivesFlat(
                    lassoPoints,
                    { 0.2, 0.2, 0.2, 1.0 },
                    windowWidth,
                    windowHeight,
                    plt.xSubRange(),
                    plt.ySubRange(),
                    GL_LINE_STRIP );
            }
        }
    }
}

}

#endif