
#ifndef TN_MARRUS_JOINT_PLOT_RENDERING_HPP
#define TN_MARRUS_JOINT_PLOT_RENDERING_HPP

#include "GridRender.hpp"
#include "HistRender.hpp"
#include "MarginalsRender.hpp"
#include "ScatterPlotRender.hpp"
#include "PlotBorderRender.hpp"
#include "KDERender.hpp"
#include "GMMRender.hpp"
#include "ZoomLevelRender.hpp"
#include "CurvesRender.hpp"
#include "SubselectionBoundaries.hpp"
#include "AnnotationRender.hpp"

#include "views/JointPlot2D.hpp"
#include "render/RenderModule2D/Renderer2D.hpp"
#include "render/TextRenderer/TextRenderer.hpp"
#include "geometry/Vec.hpp"
#include "filter/FilterOps.hpp"

#include "render/GLErrorChecking.hpp"

#include <vector>

namespace Marrus {

static inline void renderJointPlot2D(
    TN::Renderer2D & renderer,
    TN::TextRenderer & textRenderer,    
    TN::SimpleDataManager< float > & dataManager,
    TN::JointPlot2D & plt,
    const double windowWidth,
    const double windowHeight,
    const std::vector< float > & x,
    const std::vector< float > & y,
    const TN::Vec2< float > & xRange,
    const TN::Vec2< float > & yRange,
    const std::vector< int   > & flags,
    const std::vector< float > & xSub,
    const std::vector< float > & ySub,
    const std::vector< int   > & flagsSub,        
    const bool selectorActive,
    const TN::Vec4 & color,
    const TN::Vec4 & fadeColor,
    const TN::Vec4 & backGroundColor,
    const TN::Vec4 & axisColor,    
    const bool renderBackground,
    const bool outLinePoints,
    const float pointSize,
    const bool supressUpdate )
{
    const int CHARWIDTH = 7;

    TN::Vec4 gridFGCOlor = { 0.80, 0.80, 0.80, 1.0 };
    TN::Vec4 gridBGCOlor = { 0.90, 0.90, 0.90, 1.0 };

    const TN::Vec4 contextColor    = { 
        150  /255.0, 
        150  /255.0, 
        150  /255.0, 0.9 };

    const TN::Vec4 selectionColor  = { 
        70 / 255.0, 
        70 / 255.0, 
        70 / 255.0, 
        0.7 };

    const TN::Vec4 lassoPointColor = { 
         250 / 255.0, 
        120  / 255.0, 
         80  / 255.0, 
        0.9 };

    Marrus::renderPlotBorder( renderer, plt, windowWidth, windowHeight );
    Marrus::renderGrid(       renderer, plt, gridFGCOlor, gridBGCOlor, plt.binDims() );

    if( x.size() > 0 )
    {
        Marrus::renderGridAxis(   renderer, dataManager, plt, xRange, yRange, axisColor );

        if( plt.type() == "cdf2d" )
        {
            if( ! supressUpdate )
            {
                plt.updateHist2D( x, y, flags );
            }

            Marrus::renderCDF2D( renderer, plt, windowWidth, windowHeight );
        }
        else if( plt.type() == "hist2d" )
        {   
            if( ! supressUpdate )
            {
                plt.updateHist2D( x, y, flags );
            }

            Marrus::renderHist2D( renderer, plt, windowWidth, windowHeight );
        }
        else if( plt.type() == "kde" )
        {
            if( ! supressUpdate )
            {
                plt.updateKde( x, y, flags );
            }

            Marrus::renderKde( renderer, plt, windowWidth, windowHeight );
        }
        else if( plt.type() == "gmm" )
        {
            if( ! supressUpdate )
            {
                plt.updateGMM( x, y, flags );
            }

            renderGMM(
                renderer,
                plt,
                windowWidth,
                windowHeight,
                xSub,
                ySub,
                flagsSub,
                supressUpdate );
        }
        else if ( plt.type() == "scatter" )
        {
            TN::Texture2D & texture = plt.getScatterPlotTexture();

            if( ! supressUpdate && plt.scatterPlotNeedsUpdate() )
            {
                // generate and bind framebuffer

                unsigned int frameBuffer;
                GL_CHECK_CALL( glGenFramebuffers( 1, & frameBuffer ) );
                GL_CHECK_CALL( glBindFramebuffer( GL_FRAMEBUFFER, frameBuffer ) );
                GL_CHECK_CALL( glBindTexture( GL_TEXTURE_2D, texture.id() ) );
                GL_CHECK_CALL( glTexImage2D(  GL_TEXTURE_2D, 0, GL_RGB, plt.size().x(), plt.size().y(), 0, GL_RGB, GL_UNSIGNED_BYTE, NULL ) );
                GL_CHECK_CALL( glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR ) );
                GL_CHECK_CALL( glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR ) );
                GL_CHECK_CALL( glFramebufferTexture2D( GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, texture.id(), 0 ) ); 

                Marrus::renderGrid( renderer, { 0.f, 0.f }, plt.size(), gridFGCOlor, gridBGCOlor, plt.binDims() );

                Marrus::renderGridAxis( 
                    renderer, 
                    dataManager,
                    plt.xKey(),
                    plt.yKey(),
                    { 0.f, 0.f }, 
                    plt.size(), 
                    xRange, 
                    yRange, 
                    plt.xSubRange(),
                    plt.ySubRange(),
                    axisColor );
                
                GL_CHECK_CALL( glPointSize( pointSize ) );

                renderer.scatterPoints(
                    x,
                    y,
                    flags,
                    Marrus::FilterOp::WITHIN_COMPONENT_SELECTION,
                    plt.xSubRange(),
                    plt.ySubRange(),
                    contextColor,
                    10.f );

                if( xSub.size() > 0 ) {

                    renderer.scatterPoints(
                        xSub,
                        ySub,
                        flagsSub,
                        Marrus::FilterOp::WITHIN_VOXEL_SELECTION,
                        plt.xSubRange(),
                        plt.ySubRange(),
                        selectionColor,
                        10.f );
                }

                // release and delete frame buffer

                GL_CHECK_CALL( glBindFramebuffer( GL_FRAMEBUFFER, 0 ) );
                GL_CHECK_CALL( glDeleteFramebuffers( 1, & frameBuffer ) );

                plt.setScatterPlotNeedsUpdate( false );
            }

            if( ! supressUpdate )
            {
                GL_CHECK_CALL( glViewport( plt.position().x(), plt.position().y(), plt.size().x(), plt.size().y() ) );
                renderer.renderTexture( texture.id() );
            }
        }
    }

//____________________________________Marginals_____________________________________

    if( x.size() > 0 )
    {
        if( ! supressUpdate )
        {
            plt.updateXMarginal( x, flags );
            plt.updateYMarginal( y, flags );    
        }

        Marrus::renderMarginals( renderer, plt );
        Marrus::renderMomentsOnMarginals( renderer, plt );  
        Marrus::renderSubMarginals( renderer, plt, gridFGCOlor, gridBGCOlor, plt.binDims() ); 
        Marrus::renderZoomLevelIndicators( renderer, plt, windowWidth, windowHeight, fadeColor );
    }

//____________________________________Plot Sub Selection_____________________________________


    if( plt.showSelectedPoints() && xSub.size() > 0 )
    {
        TN::Texture2D & texture = plt.getLassoPointsTexture();

        if( ! supressUpdate && plt.lassoPointsNeedsUpdate() )
        {

            unsigned int frameBuffer;
            GL_CHECK_CALL( glGenFramebuffers( 1, & frameBuffer ) );
            GL_CHECK_CALL( glBindFramebuffer( GL_FRAMEBUFFER, frameBuffer ) );
            GL_CHECK_CALL( glBindTexture( GL_TEXTURE_2D, texture.id() ) );
            GL_CHECK_CALL( glTexImage2D(  GL_TEXTURE_2D, 0, GL_RGBA, plt.size().x(), plt.size().y(), 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL ) );
            GL_CHECK_CALL( glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR ) );
            GL_CHECK_CALL( glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR ) );
            GL_CHECK_CALL( glFramebufferTexture2D( GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, texture.id(), 0 ) ); 

            GL_CHECK_CALL( glClearColor( 1.0f, 1.0f, 0.0f, 0.0f ) );

            GL_CHECK_CALL( glDisable( GL_DEPTH_TEST ) );
            GL_CHECK_CALL( glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT ) );
            GL_CHECK_CALL( glEnable( GL_BLEND ) );
            GL_CHECK_CALL( glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA ) );

            GL_CHECK_CALL( glViewport( 0, 0, plt.size().x(), plt.size().y() ) );

            GL_CHECK_CALL( glPointSize( pointSize + 3 ) );
            renderer.scatterPoints(
                xSub,
                ySub,
                flagsSub,
                Marrus::FilterOp::WITHIN_VOXEL_SUB_SELECTION,
                plt.xSubRange(),
                plt.ySubRange(),
                { 0,0,0,1 },
                10.f );

            GL_CHECK_CALL( glPointSize( pointSize ) );
            renderer.scatterPoints(
                xSub,
                ySub,
                flagsSub,
                Marrus::FilterOp::WITHIN_VOXEL_SUB_SELECTION,
                plt.xSubRange(),
                plt.ySubRange(),
                lassoPointColor,
                10.f );

            GL_CHECK_CALL( glBindFramebuffer( GL_FRAMEBUFFER, 0 ) );
            GL_CHECK_CALL( glDeleteFramebuffers( 1, & frameBuffer ) );

            plt.setLassoPointsNeedsUpdate( false );
        }

        if( ! supressUpdate )
        {
            GL_CHECK_CALL( glViewport( plt.position().x(), plt.position().y(), plt.size().x(), plt.size().y() ) );
            renderer.renderTexture( texture.id() );
        }
    }

        //////////////////////////////////////////////////////////////////////////////////////////

    Marrus::renderSelectionBoundaries(
        renderer,
        textRenderer,
        plt,
        windowWidth,
        windowHeight,
        xSub,
        ySub,
        flagsSub,
        xRange,
        yRange,
        color,
        pointSize,
        selectorActive,
        supressUpdate );

//__________________________________________Curves___________________________________________

    if( ! supressUpdate && plt.showConditionalCurves() && x.size() > 0 )
    {
        plt.updateConditional2D( x, y, flags );
    }

    if( plt.showConditionalCurves() && x.size() > 0 )
    {
        Marrus::renderCurves( renderer, plt, windowWidth, windowHeight );
    }

//___________________________________________buttons____________________________________________________

    std::vector< TN::PressButton * > buttons = plt.buttons();
    for( auto b : buttons )
    {
        renderer.renderPressButton( b, { 0.2, 0.2, 0.2 }, windowWidth, windowHeight, true );
    }

//_____________________________________________________________________________________________________

    if( x.size() > 0 )
    {
        Marrus::renderAnnotation(
            renderer,
            textRenderer,
            dataManager,
            plt,
            windowWidth,
            windowHeight,
            xRange,
            yRange );
    }
}

}

#endif
