#ifndef MARRUS_PLOT_VIEW_SCATTERPLOT_RENDER_HPP
#define MARRUS_PLOT_VIEW_SCATTERPLOT_RENDER_HPP

#include "views/JointPlot2D.hpp"
#include "render/RenderModule2D/Renderer2D.hpp"
#include "render/TextRenderer/TextRenderer.hpp"
#include "geometry/Vec.hpp"
#include "filter/FilterOps.hpp"

#include "render/GLErrorChecking.hpp"

namespace Marrus {

static inline void renderScatterPoints(
    TN::Renderer2D & renderer,
    TN::JointPlot2D & plt,
    const double windowWidth,
    const double windowHeight,
    const std::vector< float > & x,
    const std::vector< float > & y,
    const std::vector< int   > & flags,
    const std::vector< float > & xSub,
    const std::vector< float > & ySub,
    const std::vector< int   > & flagsSub,
    const float pointSize,
    TN::Texture2D & texture )
{
    GL_CHECK_CALL( glDisable( GL_DEPTH_TEST ) );
    GL_CHECK_CALL( glViewport( 0, 0, plt.size().x(), plt.size().y() ) );
    GL_CHECK_CALL( glPointSize( pointSize ) );

    // generate and bind framebuffer

    unsigned int frameBuffer;
    GL_CHECK_CALL( glGenFramebuffers( 1, & frameBuffer ) );
    GL_CHECK_CALL( glBindFramebuffer( GL_FRAMEBUFFER, frameBuffer ) );
    GL_CHECK_CALL( glBindTexture( GL_TEXTURE_2D, texture.id() ) );
    GL_CHECK_CALL( glTexImage2D(  GL_TEXTURE_2D, 0, GL_RGB, plt.size().x(), plt.size().y(), 0, GL_RGB, GL_UNSIGNED_BYTE, NULL ) );
    GL_CHECK_CALL( glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR ) );
    GL_CHECK_CALL( glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR ) );
    GL_CHECK_CALL( glFramebufferTexture2D( GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, texture.id(), 0 ) ); 

    const TN::Vec4 contextColor   = { 0.5, 0.5, 0.5, 0.5 };
    const TN::Vec4 selectionColor = { 0.8, 0.9, 0.67, 0.9 };

    renderer.scatterPoints(
        x,
        y,
        flags,
        Marrus::FilterOp::WITHIN_COMPONENT_SELECTION,
        plt.xSubRange(),
        plt.ySubRange(),
        contextColor,
        10.f );

    renderer.scatterPoints(
        xSub,
        ySub,
        flagsSub,
        Marrus::FilterOp::WITHIN_VOXEL_SELECTION,
        plt.xSubRange(),
        plt.ySubRange(),
        selectionColor,
        10.f );

    // release and delete frame buffer

    GL_CHECK_CALL( glBindFramebuffer( GL_FRAMEBUFFER, 0 ) );
    GL_CHECK_CALL( glDeleteBuffers( 1, & frameBuffer ) );
}

}

#endif