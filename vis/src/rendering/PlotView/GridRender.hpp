#ifndef MARRUS_PLOT_VIEW_GRID_RENDER_HPP
#define MARRUS_PLOT_VIEW_GRID_RENDER_HPP

#include "views/JointPlot2D.hpp"
#include "render/RenderModule2D/Renderer2D.hpp"
#include "geometry/Vec.hpp"

#include "render/GLErrorChecking.hpp"

namespace Marrus {

static inline void renderGrid(
    TN::Renderer2D & renderer,
    const TN::Vec2< float > & pos,
    const TN::Vec2< float > & size,
    const TN::Vec4 & gridFGCOlor,
    const TN::Vec4 & gridBGCOlor,
    const TN::Vec2< int > & gridDims )
{
    renderer.renderGrid(
        pos,
        size,
        gridDims.x(),
        gridDims.y(),
        gridFGCOlor,
        gridBGCOlor );
}

static inline void renderGrid(
    TN::Renderer2D & renderer,
    TN::JointPlot2D & plt,
    const TN::Vec4 & gridFGCOlor,
	const TN::Vec4 & gridBGCOlor,
	const TN::Vec2< int > & gridDims )
{
    renderer.renderGrid(
        plt.position(),
        plt.size(),
        gridDims.x(),
        gridDims.y(),
        gridFGCOlor,
        gridBGCOlor );
}

static inline void renderGridAxis(
    TN::Renderer2D & renderer,
    TN::SimpleDataManager< float > & dataManager,
    const std::string & xKey,
    const std::string & yKey,
    const TN::Vec2< float > & pos,
    const TN::Vec2< float > & size,
    const TN::Vec2< float > & xRange,
    const TN::Vec2< float > & yRange,
    const TN::Vec2< float > & rX,
    const TN::Vec2< float > & rY,
    const TN::Vec4 & axisColor )
{
    // convert the selected plot subrange from normalized to true range
    auto vxR = rX * ( xRange.b() - xRange.a() ) + xRange.a();
    auto vyR = rY * ( yRange.b() - yRange.a() ) + yRange.a();

    GL_CHECK_CALL( glLineWidth( 2 ) );

    if( true ) //vxR.a() <= 0 && vxR.b() >= 0 )
    {
        float zero = dataManager.trueValueToScaledValue( 0.0, xKey );

        if( zero > rX.a() && zero < rX.b() && rX.b() > rX.a() )
        {
            float relPos = ( zero - rX.a() ) / ( rX.b() - rX.a() ) * 2.0 - 1.0;

            std::vector< TN::Vec2< float > > yAxis =
            {
                { relPos, -1.0 },
                { relPos,  1.0 }
            };

            GL_CHECK_CALL( glViewport( pos.x(), pos.y(), size.x(), size.y() ) );
            renderer.renderPrimitivesFlat(
                yAxis,
                axisColor,
                GL_LINES
            );
        }
    }

    if( true ) //vyR.a() <= 0 && vyR.b() >= 0 )
    {
        float zero = dataManager.trueValueToScaledValue( 0.0, yKey );

        if( zero > rY.a() && zero < rY.b() && rY.b() > rY.a() )
        {
            float relPos = ( zero - rY.a() ) / ( rY.b() - rY.a() ) * 2.0 - 1.0;

            std::vector< TN::Vec2< float > > xAxis =
            {
                {-1.0, relPos },
                { 1.0, relPos }
            };

            GL_CHECK_CALL( glViewport( pos.x(), pos.y(), size.x(), size.y() ) );
            renderer.renderPrimitivesFlat(
                xAxis,
                axisColor,
                GL_LINES
            );
        }
    }
}

static inline void renderGridAxis(
    TN::Renderer2D & renderer,
    TN::SimpleDataManager< float > & dataManager,
    TN::JointPlot2D & plt,
    const TN::Vec2< float > & xRange,
    const TN::Vec2< float > & yRange,
    const TN::Vec4 & axisColor )
{
    renderGridAxis( 
        renderer, 
        dataManager,
        plt.xKey(),
        plt.yKey(),
        plt.position(), 
        plt.size(), 
        xRange, 
        yRange, 
        plt.xSubRange(), 
        plt.ySubRange(),
        axisColor );
}

}

#endif