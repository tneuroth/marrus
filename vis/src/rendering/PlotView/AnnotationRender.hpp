#ifndef MARRUS_PLOT_ANNOTATION_RENDER_HPP
#define MARRUS_PLOT_ANNOTATION_RENDER_HPP

#include "views/JointPlot2D.hpp"
#include "render/RenderModule2D/Renderer2D.hpp"
#include "geometry/Vec.hpp"

#include "render/GLErrorChecking.hpp"

namespace Marrus {

void renderAnnotation( 
	TN::Renderer2D & renderer,
    TN::TextRenderer & textRenderer,
    TN::SimpleDataManager< float > & dataManager,
    TN::JointPlot2D & plt,
    const double windowWidth,
    const double windowHeight,
    const TN::Vec2< float > & xRange,
    const TN::Vec2< float > & yRange )
{
    const int CHARWIDTH = 7;

	TN::Vec2< float > rX = plt.xSubRange();
    TN::Vec2< float > rY = plt.ySubRange();

    // text

    GL_CHECK_CALL( glViewport( 0, 0, windowWidth, windowHeight ) );

    if( plt.wKey() != "" )
    {
        const std::string tle = "conditional mean " + plt.wKey();
        textRenderer.renderText(
            windowWidth,
            windowHeight,
            tle,
            { 
                plt.position().x() + plt.size().x() / 2.0 - tle.size() * CHARWIDTH / 2.f,
                ( plt.xMarginal().position().y() + plt.xMarginal().size().y() + 10 ) 
            },
            { 0.5f, 0.5f, 0.5f, 1.f },
            false );
    }

    // // x moments
    // textRenderer.renderText(
    //     false,
    //     windowWidth,
    //     windowHeight,
    //     plt.xMarginal().statsStr(),
    //     plt.position().x(),
    //     ( plt.xMarginal().position().y() + plt.xMarginal().size().y() + 14 ),
    //     0.8f,
    //     { 0.5f, 0.5f, 0.5f },
    //     false );

    // // y moments
    // textRenderer.renderText(
    //     false,
    //     windowWidth,
    //     windowHeight,
    //     plt.yMarginal().statsStr(),
    //     plt.position().x() + plt.size().x() + plt.yMarginal().size().x() + 18,
    //     plt.position().y(),
    //     0.8f,
    //     { 0.5f, 0.5f, 0.5f },
    //     true );
    
    for( int c = 0; c <= 2; ++c )
    {
        float p = c / 2.f;

        // ticks

        std::vector< TN::Vec2< float > > ticks =
        {
            { p * 2.0 - 1, -1 },
            { p * 2.0 - 1,  1 }
        };

        GL_CHECK_CALL( glViewport(
            plt.position().x(),
            plt.position().y() - 6,
            plt.size().x(),
            6 ) );

        if( c == 0 || c == 2 )
        {
            GL_CHECK_CALL( glLineWidth( 2 ) );
        }
        else
        {
            GL_CHECK_CALL( glLineWidth( 1 ) );
        }

        renderer.renderPrimitivesFlat(
            ticks,
            TN::Vec4( 0.2, 0.2, 0.2, 1 ),
            GL_LINES
        );

        // text

        float normalizedValue = rX.a() * ( 1.0 - p ) + rX.b() *  p;
        float trueValue = dataManager.scaledValueToTrueValue( normalizedValue, plt.xKey() );
        std::string text = TN::toStringWFMT( trueValue );

        float offset = c == 2 ? text.length() * CHARWIDTH : c == 0 ? 0 : text.length() * CHARWIDTH / 2.0;
        textRenderer.renderText(
            windowWidth,
            windowHeight,
            text,
            { 
                plt.position().x() + plt.size().x() * p - offset,
                plt.position().y() - 17
            },
            { 0.2f, 0.2f, 0.2f, 1.f },
            false,
            18 );
    }

    for( int r = 0; r <= 2; ++r )
    {
        float p = r / 2.f;

        // tick

        std::vector< TN::Vec2< float > > ticks =
        {
            { -1, p * 2.0 - 1 },
            {  1, p * 2.0 - 1 }
        };

        GL_CHECK_CALL( glViewport(
            plt.position().x() - 6,
            plt.position().y(),
            6,
            plt.size().y() ) );

        if( r == 0 || r == 2 )
        {
            GL_CHECK_CALL( glLineWidth( 2 ) );
        }
        else
        {
            GL_CHECK_CALL( glLineWidth( 1 ) );
        }

        renderer.renderPrimitivesFlat(
            ticks,
            TN::Vec4( 0.2, 0.2, 0.2, 1 ),
            GL_LINES
        );

        float normalizedValue = rY.a() * ( 1.0 - p ) + rY.b() *  p;
        float trueValue = dataManager.scaledValueToTrueValue( normalizedValue, plt.yKey() );

        std::string text = TN::toStringWFMT( trueValue );
        float offset = r == 2.f ? text.length() * CHARWIDTH : r == 0 ? 0 : text.length() * CHARWIDTH / 2.0;
        textRenderer.renderText(
            windowWidth,
            windowHeight,
            text,
            { plt.position().x() - 9, plt.position().y() + plt.size().y() * p - offset },
            { 0.3, 0.3, 0.3, 1.f },
            true,
            18 );
    }

//___________________________________Info From Hover Interaction_______________________________________

    if( plt.hovered() && ( plt.type() == "hist2d" || plt.type() == "cdf2d" ) )
    {
        auto value = plt.hoveredPosition();

        float trueXValue = dataManager.scaledValueToTrueValue( value.x(), plt.xKey() );
        float trueYValue = dataManager.scaledValueToTrueValue( value.y(), plt.yKey() );

        std::string valText = 
            "( "+ TN::toStringWFMT( trueXValue ) + ", " + TN::toStringWFMT( trueXValue ) + " )";

        GL_CHECK_CALL( glViewport( 
            plt.position().x() + plt.size().x() - valText.size() * 9 - 10, 
            plt.position().y() + plt.size().y() - 56,
            valText.size() * 9 + 9,
            55 ) );

        renderer.clear( { 1.0, 1.0, 1.0, 0.7 } );

        GL_CHECK_CALL( glViewport( 0, 0, windowWidth, windowHeight ) );

        textRenderer.renderText(
            windowWidth,
            windowHeight,
            valText,
            {
                plt.position().x() + plt.size().x() - valText.size() * 9,
                plt.position().y() + plt.size().y() - 32
            },
            { 0.0, 0.0, 0.0, 1.f },
            false,
            20 );

        /////////////////////////////////////////////////////////////////////


        double val = plt.hoveredBinValue();
        std::vector< TN::Vec2< float > > binBox = plt.hoveredBinBox();

        GL_CHECK_CALL( glViewport( 0, 0, windowWidth, windowHeight ) );
        GL_CHECK_CALL( glLineWidth( 2 ) );
        GL_CHECK_CALL( glPointSize( 2 ) );

        renderer.renderPrimitivesFlat(
            binBox,
            { 0.2, 0.2, 0.2, 1 },
            windowWidth,
            windowHeight,
            { 0, windowWidth  },
            { 0, windowHeight },
            GL_POINTS );

        renderer.renderPrimitivesFlat(
            binBox,
            { 0.2, 0.2, 0.2, 1 },
            windowWidth,
            windowHeight,
            { 0, windowWidth  },
            { 0, windowHeight },
            GL_LINES );

        std::string bvS = "p = " + TN::toStringWFMT( val );
        double count = plt.subCount();
        std::string bCt = "c = " + TN::toStringWFMT( val * count );

        if( plt.wKey() == "" )
        {
            textRenderer.renderText(
                windowWidth,
                windowHeight,
                bvS,
                { plt.position().x() + plt.size().x() - bvS.size() * 7, plt.position().y() + plt.size().y() - 48 },
                { 0.0, 0.0, 0.0, 1.f },
                false );

            textRenderer.renderText(
                windowWidth,
                windowHeight,
                bCt,
                {
                    plt.position().x() + plt.size().x() - bvS.size() * 7 - 8 - bCt.size() * 7,
                    plt.position().y() + plt.size().y() - 48
                },
                { 0.0, 0.0, 0.0, 1.f },
                false );
        }
        else
        {
            textRenderer.renderText(
                windowWidth,
                windowHeight,
                "w =" + TN::toStringWFMT( val ),
                {
                    plt.position().x() + plt.size().x() - bvS.size() * 7,
                    plt.position().y() + plt.size().y() - 48
                },
                { 0.0, 0.0, 0.0, 1.f },
                false );
        }
    }

    else if ( plt.hovered() && ( plt.type() == "scatter" || plt.type() == "kde" ) )
    {
        auto value = plt.hoveredPosition();

        float trueXValue = dataManager.scaledValueToTrueValue( value.x(), plt.xKey() );
        float trueYValue = dataManager.scaledValueToTrueValue( value.y(), plt.yKey() );

        std::string valText = 
            "( "+ TN::toStringWFMT( trueXValue ) + ", " + TN::toStringWFMT( trueXValue ) + " )";

        GL_CHECK_CALL( glViewport( 
            plt.position().x() + plt.size().x() - valText.size() * 9 - 10, 
            plt.position().y() + plt.size().y() - 40,
            valText.size() * 9 + 9,
            39 ) );

        renderer.clear( { 1.0, 1.0, 1.0, 0.7 } );

        GL_CHECK_CALL( glViewport( 0, 0, windowWidth, windowHeight ) );

        textRenderer.renderText(
            windowWidth,
            windowHeight,
            valText,
            {
                plt.position().x() + plt.size().x() - valText.size() * 9,
                plt.position().y() + plt.size().y() - 32
            },
            { 0.0, 0.0, 0.0, 1.f },
            false,
            20 );
    }   
}

}

#endif