#ifndef MARRUS_PLOT_VIEW_HIST_RENDER_HPP
#define MARRUS_PLOT_VIEW_HIST_RENDER_HPP

#include "views/JointPlot2D.hpp"
#include "render/RenderModule2D/Renderer2D.hpp"
#include "render/TextRenderer/TextRenderer.hpp"
#include "geometry/Vec.hpp"
#include "filter/FilterOps.hpp"

#include "render/GLErrorChecking.hpp"

namespace Marrus {

static inline void renderHist2D(
    TN::Renderer2D & renderer,
    TN::JointPlot2D & plt,
    const double windowWidth,
    const double windowHeight )
{
    GL_CHECK_CALL( glDisable( GL_DEPTH_TEST ) );
    GL_CHECK_CALL( glViewport( plt.position().x(), plt.position().y(), plt.size().x(), plt.size().y() ) );
    renderer.renderSolidContextTexture( plt.contextTexture().tex, { 0.5f, 0.5f, 0.5f, 0.5f } );
    renderer.renderHeatMapTexture( plt.texture().tex,   plt.pdfRange().a(),  plt.pdfRange().b() );
}

static inline void renderCDF2D(
    TN::Renderer2D & renderer,
    TN::JointPlot2D & plt,
    double windowWidth,
    double windowHeight )
{
    GL_CHECK_CALL( glDisable( GL_DEPTH_TEST ) );
    GL_CHECK_CALL( glViewport( plt.position().x(), plt.position().y(), plt.size().x(), plt.size().y() ) );
    renderer.renderSolidContextTexture( plt.contextTexture().tex, { 0.5f, 0.5f, 0.5f, 0.5f } );
    renderer.renderHeatMapTexture( plt.cdfTexture().tex,   plt.cdfRange().a(),  plt.cdfRange().b() );
}

}

void func(
    int p1, 
    int p2 )
{
    int a;
    int b;

    for( int i = 0; i < 10; ++i )
    {
        if( i ) 
        {
            a = 1;
        }
    }
}

#endif