#ifndef MARRUS_PLOT_VIEW_BORDER_RENDER_HPP
#define MARRUS_PLOT_VIEW_BORDER_RENDER_HPP

#include "views/JointPlot2D.hpp"
#include "render/RenderModule2D/Renderer2D.hpp"
#include "render/TextRenderer/TextRenderer.hpp"
#include "geometry/Vec.hpp"
#include "filter/FilterOps.hpp"

#include "render/GLErrorChecking.hpp"

namespace Marrus {

static inline void renderPlotBorder(
    TN::Renderer2D & renderer,
    TN::JointPlot2D & plt,
    double windowWidth,
    double windowHeight )
{
    GL_CHECK_CALL( glViewport( 0, 0, windowWidth, windowHeight ) );
    GL_CHECK_CALL( glLineWidth( 2 ) );
    GL_CHECK_CALL( glPointSize( 2 ) );

    renderer.renderPrimitivesFlat(
        { 
            TN::Vec2< float >( ( plt.position().x() - 55 ) * 2.f / windowWidth - 1.f, ( plt.position().y()                  - 80 ) * 2.f / windowHeight - 1.f ),
            TN::Vec2< float >( ( plt.position().x() - 55 ) * 2.f / windowWidth - 1.f, ( plt.position().y() + plt.size().y() + 70 ) * 2.f / windowHeight - 1.f )              
        },
        TN::Vec4( 0.91, 0.91, 0.91, 1.0 ),
        GL_LINES
    );

    renderer.renderPrimitivesFlat(
        { 
            TN::Vec2< float >( ( plt.position().x() - 55 ) * 2.f / windowWidth - 1.f, ( plt.position().y()                  - 80 ) * 2.f / windowHeight - 1.f ),
            TN::Vec2< float >( ( plt.position().x() - 55 ) * 2.f / windowWidth - 1.f, ( plt.position().y() + plt.size().y() + 70 ) * 2.f / windowHeight - 1.f )              
        },
        TN::Vec4( 0.91, 0.91, 0.91, 1.0 ),
        GL_POINTS
    );

    renderer.renderPrimitivesFlat(
        { 
            TN::Vec2< float >( ( plt.position().x() - 57 ) * 2.f / windowWidth - 1.f, ( plt.position().y()                  - 80 ) * 2.f / windowHeight - 1.f ),
            TN::Vec2< float >( ( plt.position().x() - 57 ) * 2.f / windowWidth - 1.f, ( plt.position().y() + plt.size().y() + 70 ) * 2.f / windowHeight - 1.f )              
        },
        TN::Vec4( 0.75, 0.75, 0.75, 1.0 ),
        GL_LINES
    );
    
    renderer.renderPrimitivesFlat(
        { 
            TN::Vec2< float >( ( plt.position().x() - 57 ) * 2.f / windowWidth - 1.f, ( plt.position().y()                  - 80 ) * 2.f / windowHeight - 1.f ),
            TN::Vec2< float >( ( plt.position().x() - 57 ) * 2.f / windowWidth - 1.f, ( plt.position().y() + plt.size().y() + 70 ) * 2.f / windowHeight - 1.f )              
        },
        TN::Vec4( 0.75, 0.75, 0.75, 1.0 ),
        GL_POINTS
    );
}

}

#endif