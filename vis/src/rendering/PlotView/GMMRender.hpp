#ifndef MARRUS_PLOT_GMM_RENDER_HPP
#define MARRUS_PLOT_GMM_RENDER_HPP

#include "views/JointPlot2D.hpp"
#include "render/RenderModule2D/Renderer2D.hpp"
#include "render/TextRenderer/TextRenderer.hpp"
#include "geometry/Vec.hpp"
#include "filter/FilterOps.hpp"

#include "render/GLErrorChecking.hpp"

namespace Marrus {

static inline void renderGMM(
    TN::Renderer2D & renderer,
    TN::JointPlot2D & plt,
    const double windowWidth,
    const double windowHeight,
    const std::vector< float > & xSub,
    const std::vector< float > & ySub,
    const std::vector< int   > & flagsSub,
    const bool supressUpdate )
{

    static std::vector< float > xGMM;
    static std::vector< float > yGMM;

    static std::vector< float > imResult( plt.size().x() * plt.size().y() );

    if( ! supressUpdate )
    {
        imResult.resize( plt.size().x() * plt.size().y() );

        xGMM.clear();
        yGMM.clear();

        const int64_t numPoints = ySub.size();

        for( int64_t i = 0; i < numPoints; ++i )
        {
            if( ( flagsSub[ i ] & Marrus::FilterOp::WITHIN_VOXEL_SELECTION ) != 0 )
            {
                xGMM.push_back( xSub[ i ] );
                yGMM.push_back( ySub[ i ] );
            }
        }

        std::ofstream outDat( "gmm.x.dat", std::ios::out | std::ios::binary );
        outDat.write( (char*) xGMM.data(), xGMM.size() * sizeof( float ) );
        outDat.close();
        outDat.open( "gmm.y.dat", std::ios::out | std::ios::binary );
        outDat.write( (char*) yGMM.data(), yGMM.size() * sizeof( float ) );
        outDat.close();

        system( ( "python gmm.py " 
            + std::to_string( plt.size().x()      ) + " "
            + std::to_string( plt.size().y()      ) + " "                    
            + std::to_string( plt.xSubRange().a() ) + " " 
            + std::to_string( plt.xSubRange().b() ) + " " 
            + std::to_string( plt.ySubRange().a() ) + " " 
            + std::to_string( plt.ySubRange().b() ) ).c_str() );                    

        std::ifstream inDat( "gmm.dat", std::ios::in | std::ios::binary );
        inDat.read( (char*) imResult.data(), imResult.size() * sizeof( float ) );
        inDat.close();
    }

    TN::TextureLayer texLayer;
    texLayer.load( imResult,  plt.size().x(),  plt.size().y() );
    auto texRange = TN::Sequential::getRange( imResult );

    GL_CHECK_CALL( glViewport( plt.position().x(), plt.position().y(), plt.size().x(), plt.size().y() ) );
    renderer.renderHeatMapTexture(
        texLayer.tex,
        texRange.a(),
        texRange.b(),
        false );
}

}

#endif