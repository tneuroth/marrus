#include "geometry/Vec.hpp"

#include "Application.hpp"
#include "views/InputState.hpp"

#include "glad/glad.h"
#include "GLFW/glfw3.h"

#include <vector>
#include <string>
#include <cmath>
#include <string>
#include <iostream>
#include <thread>
#include <atomic>
#include <stdio.h>
#include <memory>

void processInput(GLFWwindow *window)
{
    if( glfwGetKey( window, GLFW_KEY_ESCAPE ) == GLFW_PRESS )
    {
        glfwSetWindowShouldClose( window, true );
    }
}

void framebuffer_size_callback(GLFWwindow* window, int w, int h )
{}

const std::unordered_map< int, std::pair< std::string, std::string > > KEY_MAP =
{
    { GLFW_KEY_GRAVE_ACCENT  , { "`", "~" } },
    { GLFW_KEY_0             , { "0", ")" } }, 
    { GLFW_KEY_1             , { "1", "!" } }, 
    { GLFW_KEY_2             , { "2", "@" } }, 
    { GLFW_KEY_3             , { "3", "#" } }, 
    { GLFW_KEY_4             , { "4", "$" } }, 
    { GLFW_KEY_5             , { "5", "%" } }, 
    { GLFW_KEY_6             , { "6", "^" } }, 
    { GLFW_KEY_7             , { "7", "&" } }, 
    { GLFW_KEY_8             , { "8", "*" } }, 
    { GLFW_KEY_9             , { "9", "(" } },   
    { GLFW_KEY_MINUS         , { "-", "_" } }, 
    { GLFW_KEY_EQUAL         , { "=", "+" } }, 

    { GLFW_KEY_Q             , { "q", "Q" } }, 
    { GLFW_KEY_W             , { "w", "W" } }, 
    { GLFW_KEY_E             , { "e", "E" } },  
    { GLFW_KEY_R             , { "r", "R" } },   
    { GLFW_KEY_T             , { "t", "T" } }, 
    { GLFW_KEY_Y             , { "y", "Y" } },      
    { GLFW_KEY_U             , { "u", "U" } }, 
    { GLFW_KEY_I             , { "i", "I" } },  
    { GLFW_KEY_O             , { "o", "O" } }, 
    { GLFW_KEY_P             , { "p", "P" } }, 
    { GLFW_KEY_LEFT_BRACKET  , { "[", "{" } },
    { GLFW_KEY_RIGHT_BRACKET , { "]", "}" } },
    { GLFW_KEY_BACKSLASH     , { "\\", "|" } },

    { GLFW_KEY_A             , { "a", "A" } }, 
    { GLFW_KEY_S             , { "s", "S" } },
    { GLFW_KEY_D             , { "d", "D" } }, 
    { GLFW_KEY_F             , { "f", "F" } }, 
    { GLFW_KEY_G             , { "g", "G" } }, 
    { GLFW_KEY_H             , { "h", "H" } }, 
    { GLFW_KEY_J             , { "j", "J" } }, 
    { GLFW_KEY_K             , { "k", "K" } },  
    { GLFW_KEY_L             , { "l", "L" } }, 
    { GLFW_KEY_SEMICOLON     , { ";", ":" } },  
    { GLFW_KEY_APOSTROPHE    , { "'", "\"" } }, 

    { GLFW_KEY_Z             , { "z", "Z" } }, 
    { GLFW_KEY_X             , { "x", "X" } },  
    { GLFW_KEY_C             , { "c", "C" } }, 
    { GLFW_KEY_V             , { "v", "V" } },  
    { GLFW_KEY_B             , { "b", "B" } }, 
    { GLFW_KEY_N             , { "n", "N" } },             
    { GLFW_KEY_M             , { "m", "M" } },  
    { GLFW_KEY_COMMA         , { ",", "<" } },
    { GLFW_KEY_PERIOD        , { ".", ">" } },
    { GLFW_KEY_SLASH         , { "/", "?" } }, 
           
    { GLFW_KEY_UP            , { "up",      "up" } },           
    { GLFW_KEY_DOWN          , { "down",  "down" } },   
    { GLFW_KEY_LEFT          , { "left",  "left" } }, 
    { GLFW_KEY_RIGHT         , { "right","right" } },  

    { GLFW_KEY_SPACE         , { " ", " " } },
    { GLFW_KEY_BACKSPACE     , { "backspace", "backspace" } },  
    { GLFW_KEY_ENTER         , { "\n", "\n" } },
    { GLFW_KEY_TAB           , { "\t", "\t" } }
};

InputState inputState;

void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods )
{
    if( action == GLFW_PRESS )
    {
        for( auto & ke : KEY_MAP )
        {
            if( ke.first == key ) 
            {
                if( mods & ( GLFW_MOD_SHIFT | GLFW_MOD_CAPS_LOCK ) )  
                {
                    inputState.keyState.press( ke.second.second );
                } else 
                {
                    inputState.keyState.press( ke.second.first );
                }
            }
        }
    }
    else if ( action == GLFW_RELEASE )
    {
        for( auto & ke : KEY_MAP )
        {
            if( ke.first == key ) 
            {
                inputState.keyState.release( ke.second.second );
                inputState.keyState.release( ke.second.first );
            }
        }   
    }
}

void mouse_callback( GLFWwindow* window, int button, int action, int mods )
{
	if( action == GLFW_PRESS )
	{
	    if ( button == GLFW_MOUSE_BUTTON_LEFT ) 
	    {
	    	inputState.mouseState.buttonState = MouseButtonState::LeftPressed;

            if( inputState.mouseState.event == MouseEvent::None )
            {
                inputState.mouseState.event = MouseEvent::LeftPressed; 
            }
	    }
	    else if ( button == GLFW_MOUSE_BUTTON_RIGHT ) 
	    {
	    	inputState.mouseState.buttonState = MouseButtonState::RightPressed;
            if( inputState.mouseState.event == MouseEvent::None )
            {
                inputState.mouseState.event = MouseEvent::RightPressed; 
            }        
	    }
	    else if ( button == GLFW_MOUSE_BUTTON_MIDDLE ) 
	    {
	    	inputState.mouseState.buttonState = MouseButtonState::MiddlePressed;
            if( inputState.mouseState.event == MouseEvent::None )
            {
                inputState.mouseState.event = MouseEvent::MiddlePressed; 
            }      
	    }    
	}
    else if( action == GLFW_RELEASE )
    {
        inputState.mouseState.buttonState = MouseButtonState::NonePressed;

        if ( button == GLFW_MOUSE_BUTTON_LEFT ) 
        {
            if( inputState.mouseState.event == MouseEvent::None )
            {
                inputState.mouseState.event = MouseEvent::LeftReleased; 
            }      
        }
        else if ( button == GLFW_MOUSE_BUTTON_RIGHT ) 
        {
            if( inputState.mouseState.event == MouseEvent::None )
            {
                inputState.mouseState.event = MouseEvent::RightReleased; 
            }               
        }
        else if ( button == GLFW_MOUSE_BUTTON_MIDDLE ) 
        {
            if( inputState.mouseState.event == MouseEvent::None )
            {
                inputState.mouseState.event = MouseEvent::MiddleReleased; 
            }                 
        }    
    }

    int width, height;
    double mouseX, mouseY;
    glfwGetCursorPos(  window, & mouseX, & mouseY );
    glfwGetWindowSize( window, &  width, & height );
    mouseY = height - mouseY;
    
    inputState.mouseState.updatePosition( { mouseX, mouseY } );
    inputState.mouseState.eventPosition = inputState.mouseState.position;
}

void scroll_callback(GLFWwindow* window, double xoffset, double yoffset)
{
    inputState.mouseState.updateScroll( yoffset );
}

std::string getZenityFile( const std::string & cmd )
{
    char result[1024];
    
    FILE *f = popen( cmd.c_str(), "r" );
    fgets(result, 1024, f);
    auto err = pclose( f );

    std::string r( result );
    if( r.back() == '\n' )
    {
        r.pop_back();
    }
    return r;
}

int main( int argc, char *argv[] )
{
    if( argc != 5 && argc != 6 )
    {
        std::cerr << "error: application requires arguments : \n <working dir> <pdflatex dir> <pdftocairo dir> <convert dir> optional(<config file>) " << std::endl;
        exit( 1 );
    }

    const std::string workingDir        = argv[ 1 ];
    const std::string pdflatexDir       = argv[ 2 ];
    const std::string pdftocairoDir     = argv[ 3 ];
    const std::string convertDir        = argv[ 4 ];

    std::string configurationFile;
    if( argc == 6 ) 
    {
        configurationFile = argv[ 5 ];
    }
    else     
    {
        configurationFile = workingDir + "/projects/.temporary/config.json";
    }

    // extract directory of config file

    std::string projDirectory = configurationFile;
    char c;
    while( projDirectory.size() && ( c = projDirectory.back() ) != '/' ) 
    { 
        projDirectory.pop_back(); 
    }

 // glfw: initialize and configure

    glfwInit();
    glfwWindowHint( GLFW_CONTEXT_VERSION_MAJOR, 4 );
    glfwWindowHint( GLFW_CONTEXT_VERSION_MINOR, 2 );
    glfwWindowHint( GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE );
    glfwWindowHint( GLFW_SAMPLES, 16 );
    
    GLFWwindow * window = glfwCreateWindow( 
        2000, 
        1000, 
        "Marrus",  
        NULL, 
        NULL );

    glfwSetInputMode(window, GLFW_LOCK_KEY_MODS, GLFW_TRUE);

    glfwSetFramebufferSizeCallback( window, framebuffer_size_callback );
    glfwSetScrollCallback( window, scroll_callback );
    glfwSetMouseButtonCallback( window, mouse_callback );
    glfwSetKeyCallback( window, key_callback);
    glfwMakeContextCurrent( window );

    float xscale, yscale;
    glfwGetWindowContentScale( window, & xscale, & yscale );

    if (! gladLoadGLLoader( ( GLADloadproc ) glfwGetProcAddress ) )
    {
        std::cout << "Failed to initialize GLAD" << std::endl;
        return -1;
    }    

    glfwMaximizeWindow( window );

    std::unique_ptr< Application > visSystem( new Application( 
        window, 
        workingDir,
        pdflatexDir,
        pdftocairoDir,
        convertDir,
        projDirectory ) );

    visSystem->loadProject( configurationFile );

    float last = glfwGetTime();

    // updateloop

    std::pair< int, std::string > sys_status;

    while ( 1 )
    {
        int width, height;

        glfwMakeContextCurrent( window );  
        processInput( window );
        glfwPollEvents();
        glfwGetWindowSize( window, & width, & height );

        if( glfwWindowShouldClose( window ) )
        {
            break;
        }

        double time = glfwGetTime();
        double delta = time - last;

        double mouseX, mouseY;
        glfwGetCursorPos(  window, & mouseX, & mouseY );
        mouseY = height - mouseY;
        inputState.mouseState.updatePosition( { mouseX, mouseY } );

        // TODO: if no input, don't update

        if( delta >= 0.015 )
        {           
            auto t0 = glfwGetTime();

            sys_status = visSystem->update( width, height, inputState, sys_status );
            inputState.next();
            last = time;

            auto tend = glfwGetTime();

            //std::cout << tend - t0 << std::endl;
        }
    }

    // destroy the vis system so that resources are freed while opengl context still valid
    visSystem.reset( nullptr );

    glfwDestroyWindow( window );
    glfwTerminate();

    return 0;
}
