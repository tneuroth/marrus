#ifndef MARRUS_PROJECT_CONFIG_HPP
#define MARRUS_PROJECT_CONFIG_HPP

#include "lsrcvt/configuration.hpp"

#include "configuration/DirectVolumeRenderConfiguration.hpp"
#include "configuration/IsosurfaceSetConfiguration.hpp"
#include "configuration/SpatialViewConfiguration.hpp"
#include "configuration/JointPlotConfiguration.hpp"
#include "configuration/ClusterMapConfiguration.hpp"
#include "configuration/GlyphConfiguration.hpp"

#include "input/SimpleDataManager.hpp"
#include "input/JsonUtils.hpp"
#include "geometry/Vec.hpp"

#include <jsonhpp/json.hpp>

// #include <filesystem>
#include <vector>
#include <string>
#include <fstream>
#include <iostream>
#include <array>

namespace TN {

const std::vector< TN::Vec4 > DEFAULT_ISO_COLORS = 
{
    { 255  / 255.f,  255 / 255.f, 212 / 255.f, 1.f },
    { 254 / 255.f, 227  / 255.f, 145 / 255.f, 1.f },
    { 254 / 255.f, 196  / 255.f, 79 / 255.f, 1.f },
    { 254 / 255.f, 153  / 255.f, 41 / 255.f, 1.f },
    { 236  / 255.f, 112  / 255.f, 20 / 255.f, 1.f },
    { 204  / 255.f, 76  / 255.f, 2 / 192.f, 1.f },
    { 140  / 255.f, 45   / 255.f, 4 / 168.f, 1.f }       
};

struct ProjectConfiguration : public ConfigurationBase
{
    std::string tessellationDirectory;
    LSRCVD_Configuration lsrcvtConfig;

    std::vector< TN::SimpleVariableInfo        > variables;
    std::vector< TN::SimpleDerivedVariableInfo > derivedVariables;
    std::vector< TN::SimpleConstantInfo        > constants;
    std::vector< TN::SimpleDerivedConstantInfo > derivedConstants;

    TN::Vec3< int > dims;

    std::string xCoordsPath;
    std::string yCoordsPath;
    std::string zCoordsPath;

    std::string lField;
    std::string mField;

    bool layerValuesNormalized;
    std::vector< TN::Vec2< float > > layers;

    // views
    std::vector<  JointPlotConfiguration  > jointPlots;
    std::vector< SpatialViewConfiguration > spatialViews;
    std::vector< GlyphConfiguration       > glyphs;

    // visualizations which can be used within the views
    std::vector< DirectVolumeRenderConfiguration > directVolumeVisualizations;
    std::vector< IsosurfaceSetConfiguration >      isosurfaceVisualizations;
    std::vector< ClusterMapConfiguration >         clusterMaps;


    TN::SimpleDataInfoSet getRawDataInfo() 
    {
        TN::SimpleDataInfoSet result;

        for( auto & v : variables )
        {
            result.variables.insert( { v.name } );
        }

        for( auto & v : derivedVariables )
        {
            result.derivedVariables.insert( { v.name, v.expression } );
        }

        for( auto & c : constants )
        {
            result.constants.insert( { c.name, c.value } );
        }

        for( auto & c : derivedConstants )
        {
            result.derivedConstants.insert( { c.name, c.expression } );
        }

        return result;
    }

    virtual void init() override
    {
        lsrcvtConfig = LSRCVD_Configuration();

        variables                  = {};
        derivedVariables           = {};
        constants                  = {};
        derivedConstants           = {};
        layers                     = {};
        jointPlots                 = {};
        spatialViews               = {};
        directVolumeVisualizations = {};
        isosurfaceVisualizations   = {};
        clusterMaps                = {};
    }

    virtual void fromJSON( nlohmann::json j, const std::string & baseDirectory = "" ) override
    {
        statusReport = "";
        status = TN::ConfigStatus::Incomplete;        
        std::pair< bool, std::string > parseStatus;
        bool found;

        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        int csts;
        found = TN::CF::getParameter( j, "status", csts, parseStatus, false, 0 );
        if( j.size() == 0 || found && csts == 0 ) 
        {
            std::cout << "Creating empty project." << std::endl;
            status = TN::ConfigStatus::Incomplete;
            statusReport = "Incomplete";
            return;
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        // TODO: add derived features for layers, components, and cells

        if( found = TN::CF::getParameter( j, "tessellationDirectory", tessellationDirectory, parseStatus, TN::CF::REQUIRED, std::string() ) ) {
            // if( std::filesystem::exists( std::filesystem::path( tessellationDirectory + "/configuration.json" ) ) )
            // {
                lsrcvtConfig = LSRCVD_Configuration( tessellationDirectory + "/configuration.json" );
                if( ! lsrcvtConfig.isValid() ) {
                    std::cerr << "ERROR: invalid tessellation configuration" << std::endl;
                    std::cerr << lsrcvtConfig.getMessage() << std::endl;
                    exit( 1 );
                }
                auto & lyrs = lsrcvtConfig.layers;
                layers.resize( lyrs.size() );
                for( size_t i = 0, end = layers.size(); i < end; ++i ) {
                    layers[ i ] = { lyrs[ i ][ 0 ], lyrs[ i ][ 1 ] };
                }
        }

        found = TN::CF::getParameter( j, "lField", lField, parseStatus, TN::CF::OPTIONAL, std::string( "" ) );
        found = TN::CF::getParameter( j, "mField", mField, parseStatus, TN::CF::OPTIONAL, std::string( "" ) );
        found = TN::CF::getParameter( j, "layerValuesNormalized", layerValuesNormalized, parseStatus, TN::CF::OPTIONAL, bool( false ) );

        std::array< int64_t, 3 > dms;
        found = TN::CF::getParameter( j, "dims", dms, parseStatus, TN::CF::OPTIONAL, {}  );
        if( found ) {
            dims = { dms[ 0 ], dms[ 1 ], dms[ 2 ] };
        }

        found = TN::CF::getParameter( j, "xCoordsPath", xCoordsPath, parseStatus, TN::CF::OPTIONAL, std::string( "" ) );
        found = TN::CF::getParameter( j, "yCoordsPath", yCoordsPath, parseStatus, TN::CF::OPTIONAL, std::string( "" ) );
        found = TN::CF::getParameter( j, "yCoordsPath", yCoordsPath, parseStatus, TN::CF::OPTIONAL, std::string( "" ) );

        //=============================================================================================================

        // Variables

        std::map< std::string, double > vn; // TODO: refactor

        std::vector< nlohmann::json > tmpJsonVec;
        found = TN::CF::getParameter( j, "variables", tmpJsonVec, parseStatus, TN::CF::OPTIONAL, {} );

        for( auto & v : tmpJsonVec ) {
            TN::SimpleVariableInfo vc;
            found = TN::CF::getParameter( v, "name", vc.name, parseStatus, TN::CF::OPTIONAL, std::string( "" ) );
            found = TN::CF::getParameter( v, "filePath", vc.filePath, parseStatus, TN::CF::OPTIONAL, std::string( "" ) );
            int64_t offset;
            found = TN::CF::getParameter( v, "offset", offset, parseStatus, TN::CF::OPTIONAL, int64_t( 0 ) ); 
            vc.offset = static_cast<size_t>( offset );
            found = TN::CF::getParameter( v, "type", vc.type, parseStatus, TN::CF::OPTIONAL, std::string( "float" ) );
            found = TN::CF::getParameter( v, "normalizationFactor", vc.normalizationFactor, parseStatus, TN::CF::OPTIONAL, double( 1.0 ) );
            found = TN::CF::getParameter( v, "latex", vc.latex, parseStatus, TN::CF::OPTIONAL, std::string( "" ) ); 
            variables.push_back( vc );
            vn.insert( { vc.name, vc.normalizationFactor } );
        }

        // Derived Variables

        found = TN::CF::getParameter( j, "derivedVariables", tmpJsonVec, parseStatus, TN::CF::OPTIONAL, {} );

        for( auto & v : tmpJsonVec ) {
            TN::SimpleDerivedVariableInfo vc;
            found = TN::CF::getParameter( v, "name",       vc.name,       parseStatus, TN::CF::OPTIONAL, std::string( "" ) );
            found = TN::CF::getParameter( v, "expression", vc.expression, parseStatus, TN::CF::OPTIONAL, std::string( "" ) );
            found = TN::CF::getParameter( v, "latex",      vc.latex,      parseStatus, TN::CF::OPTIONAL, std::string( "" ) ); 
            derivedVariables.push_back( vc );
        }

        // constants

        found = TN::CF::getParameter( j, "constants", tmpJsonVec, parseStatus, TN::CF::OPTIONAL, {} );

        for( auto & c : tmpJsonVec ) {
            TN::SimpleConstantInfo cc;
            found = TN::CF::getParameter( c, "name",  cc.name,  parseStatus, TN::CF::OPTIONAL,   std::string( "" ) );
            found = TN::CF::getParameter( c, "value", cc.value, parseStatus, TN::CF::OPTIONAL,      double( 0.0  ) );
            found = TN::CF::getParameter( c, "latex", cc.latex, parseStatus, TN::CF::OPTIONAL, std::string( ""   ) ); 
            constants.push_back(   cc );
        }

        // Derived Constants

        found = TN::CF::getParameter( j, "derivedConstants", tmpJsonVec, parseStatus, TN::CF::OPTIONAL, {} );

        for( auto & c : tmpJsonVec ) {
            TN::SimpleDerivedConstantInfo cc;
            found = TN::CF::getParameter( c, "name",       cc.name,       parseStatus, TN::CF::OPTIONAL, std::string( "" ) );
            found = TN::CF::getParameter( c, "expression", cc.expression, parseStatus, TN::CF::OPTIONAL, std::string( "" ) );
            found = TN::CF::getParameter( c, "latex",      cc.latex,      parseStatus, TN::CF::OPTIONAL, std::string( "" ) ); 
            derivedConstants.push_back( cc );
        }

        // ============================================================================================================

        found = TN::CF::getParameter( j, "directVolumeVisualizations", tmpJsonVec, parseStatus, TN::CF::OPTIONAL, {} );
        for( auto & dv : tmpJsonVec ) {
            DirectVolumeRenderConfiguration cf;
            cf.fromJSON( dv, baseDirectory );
            if( ! cf.isValid() ) {
                std::cerr << "Error: invalid volume visualization configuration" << std::endl;
                std::cerr << cf.getMessage() << std::endl;
                exit( 1 );
            }
            directVolumeVisualizations.push_back( cf );
        }

        // Default isosurface set, based on the bands of the LSRCVT
    

        if( layers.size() > 0 ) {

            IsosurfaceSetConfiguration iscf;
            iscf.field = lField;
            iscf.name  = "Bands";

            if( ! vn.count( lField ) )
            {
                std::cerr << "Error parsing project configuration, variable " << lField << " from tesselation configuration not found in project configuration " 
                          << " make sure the names are consistent between tesselation configuration file and project configuration file" << std::endl;  
                exit( 0 );
            }

            for( size_t i = 0, end = layers.size(); i < end; ++i ) {
                iscf.isovalues.push_back(  layers[ i ].a() * vn.at( lField ) );
                // iscf.colors.push_back( { DEFAULT_ISO_COLORS[ i % DEFAULT_ISO_COLORS.size() ] } );
                iscf.colors.push_back( { 0.8f, 0.8f, 0.8f, 1.f } );
            }
            
            iscf.isovalues.push_back( layers.back().b() * vn.at( lField ) );
            // iscf.colors.push_back( { DEFAULT_ISO_COLORS[ layers.size() % DEFAULT_ISO_COLORS.size() ] } );
            iscf.colors.push_back( { 0.8f, 0.8f, 0.8f, 1.f } );
            isosurfaceVisualizations.push_back( iscf );
        }

        found = TN::CF::getParameter( j, "IsosurfaceVisualizations", tmpJsonVec, parseStatus, TN::CF::OPTIONAL, {} );

        for( auto & isv : tmpJsonVec ) {
            IsosurfaceSetConfiguration cf;
            cf.fromJSON( isv, baseDirectory );
            if( ! cf.isValid() ) {
                std::cerr << "Error: invalid isosurface set configuration" << std::endl;
                std::cerr << cf.getMessage() << std::endl;
                exit( 1 );
            }
            isosurfaceVisualizations.push_back( cf );   
        }

        found = TN::CF::getParameter( j, "ClusterMaps", tmpJsonVec, parseStatus, TN::CF::OPTIONAL, {} );
        for( auto & cm : tmpJsonVec ) {
            ClusterMapConfiguration cf;
            cf.fromJSON( cm, baseDirectory );
            if( ! cf.isValid() ) {
                std::cerr << "Error: invalid cluster map configuration" << std::endl;
                std::cerr << cf.getMessage() << std::endl;
                exit( 1 );
            }
            clusterMaps.push_back( cf );
        }

        // cannot have a default joint plot because there may not be more than 1 variable

        found = TN::CF::getParameter( j, "jointPlots", tmpJsonVec, parseStatus, TN::CF::OPTIONAL, {} );

        if( found ) {
            for( auto & jp : tmpJsonVec ) {
                JointPlotConfiguration cf;
                cf.fromJSON( jp, baseDirectory );
                if( ! cf.isValid() ) {
                    std::cerr << "Error: invalid joint plot configuration" << std::endl;
                    std::cerr << cf.getMessage() << std::endl;
                    exit( 1 );
                }
                jointPlots.push_back( cf );
            }
        }

        // default spatial views
        
        found = TN::CF::getParameter( j, "spatialViews", tmpJsonVec, parseStatus, TN::CF::OPTIONAL, {} );
        for( auto & sv : tmpJsonVec ) {
            SpatialViewConfiguration cf;
            cf.fromJSON( sv, baseDirectory );
            if( ! cf.isValid() ) {
                std::cerr << "Error: invalid spatial view configuration, " << std::endl;
                std::cerr << cf.getMessage() << std::endl;
                exit( 1 );
            }
            if( cf.name == "Primary View" 
             || cf.name == "Highlight L1" 
             || cf.name == "Highlight L2" 
             || cf.name == "Highlight L3" )
            {
                std::cerr << "Name conflict in spatial view with one of the defaults:"
                << " \"Primary View\", \"Highlight L1\", \"Highlight L2\", or \"Highlight L3\". Skipping entry." 
                << std::endl;
            }
            else 
            {
                spatialViews.push_back( cf );
            }
        }

        // some default views
        
        if( ! found ) {

            SpatialViewConfiguration svcf;

            // primary
            svcf.name = "View 1";
            spatialViews.push_back( svcf );

            // // level 1 selection
            // svcf.name = "View 2";
            // spatialViews.push_back( svcf );

            // // level 2 selection
            // svcf.name = "View 3";
            // spatialViews.push_back( svcf );

            // // level 3 selection
            // svcf.name = "View 4";
            // spatialViews.push_back( svcf );
        }

        // Glyph configurations

        // built in default glyph
        glyphs.push_back( GlyphConfiguration() );


        found = TN::CF::getParameter( j, "glyphs", tmpJsonVec, parseStatus, TN::CF::OPTIONAL, {} );
        if( found ) 
        {
            for( auto & c : tmpJsonVec ) {

                GlyphConfiguration cf;

                cf.fromJSON( c, baseDirectory );

                if( ! cf.isValid() ) {
                    std::cerr << "Error: invalid joint plot configuration" << std::endl;
                    std::cerr << cf.getMessage() << std::endl;
                    exit( 1 );
                }

                glyphs.push_back( cf );
            }
        }

        /************************************************************************************************************/

        status = TN::ConfigStatus::Ok;
    }

    virtual nlohmann::json toJSON() override
    {
        nlohmann::json j;
        return j;
    }

    virtual void fromFile( const std::string & path, const std::string & baseDirectory = "" ) 
    {
        init();
        std::ifstream input( path );
        if( !input.is_open() )  {
            std::cerr << "Couldn't open config file: " << path << std::endl;
            exit( 1 );
        }   
        std::stringstream sstr;
        sstr << input.rdbuf();
        
        fromJSON( nlohmann::json::parse( sstr.str(), nullptr, true, true ), baseDirectory ); 
    }   

    ProjectConfiguration() : ConfigurationBase() {}
    virtual ~ProjectConfiguration() {}
};

}

#endif
