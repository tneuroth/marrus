
#ifndef MARRUS_SUMMARY_DATA_MANAGER_HPP
#define MARRUS_SUMMARY_DATA_MANAGER_HPP

#include "geometry/Vec.hpp"
#include "algorithms/Standard/MyAlgorithms.hpp"
#include "latex/runpdflatex.hpp"
#include "utils/io.hpp"

#include <cstdint>
#include <memory>
#include <vector>
#include <iostream>
#include <map>
#include <string>
#include <fstream>
#include <type_traits>
#include <limits>

namespace Marrus
{

template < typename T >
struct TypeTag {
    static T tag() { return T(); }
};

template <typename T >
struct SummaryData 
{
	int64_t n_regions;

    std::map< std::string, std::unique_ptr< std::vector< T > > > data;  
    std::map< std::string, TN::Vec2< T > > ranges;
    std::map< std::string, TN::GlyphFeature > info;

    SummaryData( SummaryData && ) = default;
    SummaryData()  {}
    ~SummaryData() {}

    void registerFeature( 
    	const std::string & directory,
    	const std::string & level, 
    	const TN::GlyphFeature & feature )
    {
    	TN::GlyphFeature f = feature;
    
    	// add the full path to the feature data
    	f.data_file = directory + level + "." + f.data_file;

 		data.insert(   { feature.name, nullptr         } );
 		ranges.insert( { feature.name, TN::Vec2< T >() } );
 		info.insert(   { feature.name, f               } );
    }

    void clear()
    {
    	data.clear();  
    	ranges.clear();
    	info.clear();
    }

	template< typename RAW_TYPE >
    void loadFeature( 
        const std::string & feature_name,
        RAW_TYPE tag )
    {
    	if( ! data.count( feature_name ) )
    	{
    		std::cerr << "Unmapped feature in SummaryDataManager: " << feature_name << std::endl;
    		exit(1);
    	}

    	const auto & f_info = info.at( feature_name );

    	const int64_t SZ = n_regions * f_info.size();

        data.at( feature_name ).reset( new std::vector< T >( n_regions ) );
        std::vector< T > & storage = *data.at( feature_name );

		std::ifstream inFile( f_info.data_file );
		if( ! inFile.is_open() )
		{
		    std::cerr << "failed to load file: " << f_info.data_file << std::endl;
		    exit( 1 );
		}

		if( std::is_same< RAW_TYPE, T >::value )
		{
		    inFile.seekg( SZ * f_info.file_offset * sizeof( RAW_TYPE ) );
		    inFile.read( (char *) storage.data(), SZ * sizeof( RAW_TYPE ) );
		}
		else
		{
		    std::vector< RAW_TYPE > swap( SZ ); 
		    inFile.seekg( SZ * f_info.file_offset * sizeof( RAW_TYPE ) );
		    inFile.read( (char *) swap.data(), SZ * sizeof( RAW_TYPE ) );

		    for( size_t i = 0; i < SZ; ++i )
		    {
		        storage[ i ] = static_cast< T >( swap[ i ] );
		    }
		}

		inFile.close();
        ranges.at( feature_name ) = TN::Sequential::getRange( storage );
    }
};

enum class SummaryLevel
{
	Cells,
	Bands,
	Components
};

template <typename T >
class SummaryDataManager 
{
	SummaryData<T> cellSummary;
	SummaryData<T> compSummary;
	SummaryData<T> bandSummary;

public:

    SummaryDataManager( SummaryDataManager && ) = default;
    SummaryDataManager()  {}
    ~SummaryDataManager() {}

    void init(
    	const std::vector< TN::GlyphFeature > & features,
        const std::string projectDir,
        const std::string baseDir,  
        const std::string tessellationDirectory,  
        const std::string pdflatexDir,
        const std::string pdftocairoDir,
        const std::string convertDir )
    {
    	std::vector< int64_t > tmp( 7 );
        loadData( tessellationDirectory + "n_lyr_comp_sites_x_y_z.bin",  tmp, 0, static_cast< int64_t >( tmp.size() ) );

        bandSummary.n_regions = tmp[ 0 ];
        compSummary.n_regions = tmp[ 1 ];
        cellSummary.n_regions = tmp[ 2 ];

        for( auto & feature : features )
        {
        	const auto & td = tessellationDirectory;

        	cellSummary.registerFeature( td + "/summary/data/", "cells",      feature );
        	compSummary.registerFeature( td + "/summary/data/", "components", feature );
        	bandSummary.registerFeature( td + "/summary/data/", "bands",      feature );

            // bool forceRecompilation = false;
            // const std::string outPath = projectDir + "/latex_symbols/" + feature.name + ".pdf";

            // if( ( ! std::ifstream( outPath ).good() ) || forceRecompilation  )
            // {
                std::cout << "compiling: " << feature.latex << std::endl;

                TN::generateEquationPNG(
                    baseDir + "/../common/latex/templates/cropped_equation_template.tex",
                    feature.latex,
                    projectDir + "/latex_symbols/",
                    feature.name,
                    pdflatexDir,
                    pdftocairoDir,
                    convertDir,
                    true,
                    true
                );
            // }	
        }
    }

    std::vector< T > * get(
    	SummaryLevel level, 
    	const std::string & feature_name )
    {
    	auto & summary = level == SummaryLevel::Cells      ? cellSummary
    	               : level == SummaryLevel::Components ? compSummary
    	               : bandSummary;

        if( summary.data.count( feature_name ) == 0 )
        {
            std::cout << "Tried accessing feature: " << feature_name << " which was not found" << std::endl;
            exit( 0 );
        }
        else if( summary.data.at( feature_name ) == nullptr )
        {
        	const auto & f_info = summary.info.at( feature_name );

            if( f_info.data_type == "float" ) {
                summary.loadFeature( feature_name, TypeTag<float>::tag() );     
            }
            else if( f_info.data_type == "double" ) {
                summary.loadFeature( feature_name, TypeTag<double>::tag() );    
            }
            else {
            	std::cout << "Unsported feature data type: " << feature_name << " which was not found" << std::endl;
            	exit( 0 );
            }
        }

        return summary.data.at( feature_name ).get();
    }

    TN::Vec2< T > getRange( 
    	SummaryLevel level, 
    	const std::string & feature_name ) const
    {
    	auto & summary = level == SummaryLevel::Cells      ? cellSummary
    	               : level == SummaryLevel::Components ? compSummary
    	               : bandSummary;

        if( summary.ranges.count( feature_name ) == 0 )
        {
            std::cout << "error tried accessing range for " << feature_name << " which was not found" << std::endl;
            exit( 0 );
        }

        return summary.ranges.at( feature_name );
    }

    void clear()
    {
    	cellSummary.clear();
    	compSummary.clear();
    	bandSummary.clear();
    }

    int64_t numCells()      const { return cellSummary.n_regions; }
    int64_t numComponents() const { return compSummary.n_regions; }
    int64_t numBands()      const { return bandSummary.n_regions; }
};

} // end namespace Marrus

#endif