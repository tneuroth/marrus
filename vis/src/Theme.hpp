#ifndef TN_MARRUS_THEME_HPP
#define TN_MARRUS_THEME_HPP

#include "geometry/Vec.hpp"

namespace Marrus {

struct Theme {

	TN::Vec3< float > backgroundColor;

	Theme() 
	{
		backgroundColor = { 0.80f,  0.82f, 0.84f };
	}
};

} // end namespace Marrus

#endif