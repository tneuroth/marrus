#ifndef TN_MARRUS_LASSO_FILTER_HPP
#define TN_MARRUS_LASSO_FILTER_HPP

#include "Filter.hpp"
#include "geometry/Vec.hpp"

#include <jsonhpp/json.hpp>

#include <vector>
#include <iostream>
#include <cstdint>
#include <string>
#include <map>

namespace Marrus {

class PolygonalSpatialFilter : public Marrus::Filter
{
	std::vector< TN::Vec2< float > > m_polygone;

public :

	virtual void configure( const nlohmann::json & j ) 
	{

	}

	virtual void apply( 
		const uint8_t level,
		std::vector< uint8_t > & bitflags,
		const Marrus::SelectionOperand operand, 
		const std::map< Any > & features ) 
	{
		if( m_polygone.size() <= 2 )
		{	
			// clear flags for this level and all levels above 
			Marrus::clearFilterSuper( bitflags, level );
			return;
		}

		// features are expected to be separate contiguous 2D non-interleaved coordinates
		if( features.count( "x" ) == 0 || features.count( "y" ) == 0 )
		{
			std::cerr << "Error: PolygonalSpatialFilter expected x and y coordinates as features" << std::endl;
			exit( 1 );
		}

		std::vector< float > & x = * std::any_cast< std::vector< float > * >( features.at( "x" ) );
		std::vector< float > & y = * std::any_cast< std::vector< float > * >( features.at( "y" ) );

		const size_t N_POINTS = bitflags.size();

		if( operand == Marrus::SelectionOperand::Intersect )
		{
			std::vector< uint8_t > last = bitflags;
		}
	    else if( mode == FilterMode::Union )
        {
            flags[ i ] |= highlightMask;      
        }
        else if( mode == FilterMode::Subtract )
        {
            flags[ i ] &= ~highlightMask;   
        }
	};

	virtual ~PolygonalSpatialFilter() = default;
};

}

#endif