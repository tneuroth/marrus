#ifndef TN_SPATIAL_EMBEDDING_VIEW_HPP
#define TN_SPATIAL_EMBEDDING_VIEW_HPP

#include "FilterView.hpp"
#include <jsonhpp/json.hpp>

namespace Marrus {

class SpatialEmbeddingView : public Marrus::FilterView {

public:
	
	virtual void configure( const nlohmann::json & j ) = 0;
	virtual ~SpatialEmbeddingView() = default;
};

}

#endif