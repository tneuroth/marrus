#ifndef TN_MARRUS_FILTER_HPP
#define TN_MARRUS_FILTER_HPP

#include "filter/FilterOps.hpp"

#include "views/Widget.hpp"

#include <jsonhpp/json.hpp>

#include <vector>
#include <iostream>
#include <cstdint>
#include <string>

#if (__cplusplus >= 201703L) || (defined(_MSVC_LANG) && (_MSVC_LANG >= 201703L) && (_MSC_VER >= 1913))
	#if __has_include(<any>)
		#include <any>
	 using Any = std::any;
	#else
		#include <boost/any.hpp>
		using Any = boost::any;
	#endif
#else
	 #include <boost/any.hpp>
	 using Any = boost::any;
#endif

namespace Marrus {

class Filter
{

protected :

	std::pair< uint8_t, std::string > status;
	virtual std::pair< uint8_t, std::string > commit() = 0;
	virtual std::pair< uint8_t, std::string >  apply() = 0;

public :

	static constexpr uint8_t NOT_READY = 0;
	static constexpr uint8_t READY     = 1;
	static constexpr uint8_t APPLIED   = 2;

	virtual std::pair< uint8_t, std::string > attemptCommit() 
	{
		status = commit();
		return status;
	}

	virtual std::pair< uint8_t, std::string > attemptApply(
		std::vector< uint8_t > & bitflags,
		const uint8_t & level,
		const Marrus::FilterOp::Type operand, 
		const std::map< std::string, Any > features ) 
	{
		status = apply();
		return status;
	}

	virtual void fromJSON( const nlohmann::json & j, const std::string & baseDirectory = "" ) = 0;
	virtual ~Filter() = default;
};

}

#endif