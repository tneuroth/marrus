#ifndef TN_MARRUS_PROTOTYPE_DATA_FLOW__ENGINE_HPP
#define TN_MARRUS_PROTOTYPE_DATA_FLOW__ENGINE_HPP

// This is a work in progress class utilized for intermediate progress towards
// refactoring the system to use a DataFlowEngine and to integrate or replace existing
// data structures and methods into the DataFlowEngine model.

#include "filter/DataFlowEngine.hpp"

namespace Marrus 
{

class PrototypeDataFlowEngine : public NestedHierachicalDataFlowEngine 
{

public:

	virtual std::vector< int64_t > extractLowerLevelSubset(
		int l,
		std::vector< int64_t > superSetIds ) override
	{
		return {};
	};

	virtual const std::vector< int64_t > & topLevelIds() override 
	{
		return {};
	}

	virtual ~PrototypeDataFlowEngine() = default;

};

} // end namespace Marrus

#endif