#ifndef TN_UNDEFINED_FILTER_VIEW_HPP
#define TN_UNDEFINED_FILTER_VIEW_HPP

#include "input/SimpleDataManager.hpp"
#include "expressions/Expressions.hpp"
#include "filter/FilterOps.hpp"
#include "filter/FilterView.hpp"
#include "render/RenderModule2D/Renderer2D.hpp"
#include "render/TextRenderer/TextRenderer.hpp"
#include "views/ComboWidget.hpp"
#include "views/PressButtonWidget.hpp"
#include "Tessellation.hpp"

#include <jsonhpp/json.hpp>
#include <memory>

namespace Marrus {
	
class UndefinedFilterView : public Marrus::FilterView {

		TN::ComboWidget m_filterCombo;
    	TN::PressButton m_applyButton;
    	bool m_requestDefine;

public:
	
	virtual void apply(
		Marrus::Expressions & exprProcessor,
       	TN::SimpleDataManager< float > * dm,
        const std::map< std::string, long double  > & constants,
        const std::map< std::string, std::string  > & derivedConstants,
        const std::set< std::string >               & variables,
        const std::map< std::string, std::string  > & derivedVariables,
        const ConstIdPtrVariant & comp_band_ids,
        const ConstIdPtrVariant & cell_comp_ids,
        const ConstIdPtrVariant & elem_cell_ids,
		const int64_t N,
        const uint8_t filterLevel,
        FlagPtrVariant bitFlags,
        Marrus::FilterOp::Type op,
        int BIT_MASK ) override
	{}

	virtual bool valid() const override 
	{
		return false;
	}

	virtual void configure( const nlohmann::json & j ) override
	{}

	virtual void updateInformation(  
		Marrus::Expressions & exprProcessor,
        const std::map< std::string, long double  > & constants,
        const std::map< std::string, std::string  > & derivedConstants,
        const std::set< std::string >               & variables,
        const std::map< std::string, std::string  > & derivedVariables,
        const Marrus::Tessellation & tesselation ) override
	{} 

	UndefinedFilterView( 
		const std::string & resourceDir,
		std::vector< std::string > & options ) 
		: FilterView( resourceDir )
	{
		m_label = "choose filter type";

		m_filterCombo.text( "type " );
		m_filterCombo.setKeys( options );

		m_applyButton.text( "Select" );
		m_requestDefine = false;
		m_headerColor = { 0.95, 0.5, 0.4 };
	}

    virtual std::vector< TN::ComboWidget * > visibleCombos() override {

    	if( isCollapsed() )
    	{
    		return {};
    	}

        return {
        	& m_filterCombo
        };
    }

	virtual bool requestDefine() const override
	{
		return m_requestDefine;
	}

    virtual void clearRequests() override
    {
    	m_requestDefine = false;
    }

    virtual std::string defineType() const override
    {
        return m_filterCombo.selectedItemText();
    }

    virtual bool handleInput( const InputState & input ) override
    {
    	bool changed = FilterView::handleInput( input );

        TN::Vec2< double > p = input.mouseState.position;
        MouseButtonState mouseButtonState = input.mouseState.buttonState;

        // =============================================================

        // TODO: abstract combo logic

	    // if( input.mouseState.event == MouseEvent::LeftPressed ) {
		// 	if( m_filterCombo.pointInViewPort( p ) )
		// 	{    
		// 	    if( ! m_filterCombo.isPressed() )
		// 		{
		// 		    m_filterCombo.setPressed( true );
		// 		}
		// 	    else
		// 	    {
		// 	        int idx = m_filterCombo.pressed( p );
		// 	        m_filterCombo.setPressed( false );
		// 	    }
		// 	}
		// }
		// else
		// {
		// 	if( m_filterCombo.isPressed() )
		// 	{
		// 	    int id = m_filterCombo.mousedOver( p );
		// 	    if( id == -1 )
		// 	    {
		// 	        m_filterCombo.setPressed( false );
		// 	    }
		// 	}
		// }

        // =============================================================

        if( m_applyButton.pointInViewPort( p )  ) {                
            if( input.mouseState.event == MouseEvent::LeftReleased )
            {
                if( m_applyButton.isPressed() ) 
                {
                    m_requestDefine = true;
                }

                changed = true;
                m_applyButton.setPressed( false );
            }
            else if ( input.mouseState.event == MouseEvent::LeftPressed ) {
                m_applyButton.setPressed( true );
                changed = true;
            }
        }

    	if( ! isCollapsed() )
    	{
        
    	} 

        return changed;
    }

    virtual void render( 
        TN::Renderer2D & renderer,
        TN::TextRenderer & textRenderer,
        const float windowWidth,
        const float windowHeight) override 
    {
        renderer.clearBox(
            position().x(),
            position().y(),
            size().x(),
            size().y(),
            TN::Vec4( 0.9f, 0.9f, 0.9f, 1.0f ) );

    	FilterView::renderHeader( renderer, textRenderer, windowWidth, windowHeight );

    	if( ! isCollapsed() )
    	{
    		renderer.renderPressButton( & m_applyButton, { 0.2, 0.2, 0.2 }, windowWidth, windowHeight, true );
    	}
    }

    virtual void applyLayout( float X0, float YT, float W ) override 
    {
    	FilterView::applyLayout( X0, YT, W );
    	const float Y = isCollapsed() ? 34 : 68;
    	setSize( W, Y );
    	setPosition( X0, YT - Y );

        m_applyButton.setSize( 100, 30 );
        m_applyButton.setPosition( position().x() + W - 100, YT - 68 );

		m_filterCombo.setSize( W - 200, 30 );
		m_filterCombo.setPosition( position().x() + 100, YT - 68 );
    }

	virtual ~UndefinedFilterView() = default;
};

}

#endif