#ifndef TN_BAND_FILTER_VIEW_HPP
#define TN_BAND_FILTER_VIEW_HPP

#include "input/SimpleDataManager.hpp"
#include "expressions/Expressions.hpp"
#include "utils/ParseUtils.hpp"
#include "filter/FilterOps.hpp"
#include "filter/FilterView.hpp"
#include "views/ExpressionEdit.hpp"
#include "BooleanExpressionFilter.hpp"
#include "render/RenderModule2D/Renderer2D.hpp"
#include "render/TextRenderer/TextRenderer.hpp"
#include "Tessellation.hpp"

#include <jsonhpp/json.hpp>

#include <memory>

namespace Marrus {
	
struct BandSelectorModule
{
	TN::PressButton button;
	float valueA;
	float valueB;	

	BandSelectorModule( 
		const TN::Vec3< float > & color, 
		const float & vA, 
		const float & vB ) : valueA( vA ), valueB( vB )
	{
		button.color( color );
		button.resize( 60, 30 );
		button.setClearColor( true );
	}	
};

class BandSelectorFilterView : public Marrus::FilterView {

	bool m_valid;
	std::vector< BandSelectorModule > m_bandSelectors;

public:
	
	virtual void apply(
		Marrus::Expressions & exprProcessor,
       	TN::SimpleDataManager< float > * dm,
        const std::map< std::string, long double  > & constants,
        const std::map< std::string, std::string  > & derivedConstants,
        const std::set< std::string >               & variables,
        const std::map< std::string, std::string  > & derivedVariables,
        const ConstIdPtrVariant & comp_band_ids,
        const ConstIdPtrVariant & cell_comp_ids,
        const ConstIdPtrVariant & elem_cell_ids,
        const int64_t N,
        const uint8_t filterLevel,        
        FlagPtrVariant bitFlags,
        Marrus::FilterOp::Type op,
        int BIT_MASK ) override
	{
		std::vector< uint8_t > cond( N, 0 );
		if( filterLevel == Marrus::FilterOp::FILTER_LEVEL_BAND )
		{
			if( N != m_bandSelectors.size() + 1 )
			{
				throw std::runtime_error( "Tried to apply band selector with incorrect length correspondence." );
			}

			for( size_t i = 1; i < N; ++i )
			{
				cond[ i ] = ( uint8_t ) m_bandSelectors[ i-1 ].button.isPressed();
			}

			Marrus::FilterOp::Apply(
				bitFlags,
				cond.data(),
				N,
				BIT_MASK,
				op );
		} 
		else
		{
			throw std::runtime_error( "Band Selector can only be used at level 0 (on bands)." );
		} 
	}

	virtual bool valid() const override 
	{
		return m_valid;
	}

	virtual void configure( const nlohmann::json & j ) override
	{

	}

	virtual void updateInformation(  
		Marrus::Expressions & exprProcessor,
        const std::map< std::string, long double  > & constants,
        const std::map< std::string, std::string  > & derivedConstants,
        const std::set< std::string >               & variables,
        const std::map< std::string, std::string  > & derivedVariables,
        const Marrus::Tessellation & tesselation ) override
	{
		// TODO: needs to know global color coding information

		std::vector< std::array< float, 2 > > bands = tesselation.getLayers();

		if( bands.size() != m_bandSelectors.size() )
		{
			m_bandSelectors.clear();
			for( auto & band : bands )
			{
				m_bandSelectors.push_back( 
					BandSelectorModule( { 1.f, 0.f, 0.f }, band[ 0 ], band[ 1 ] )
				);
			}
		}
		else {

			const auto & colors = TN::DEFAULT_ISO_COLORS;
			const int N_COLORS = colors.size();

			for( size_t i = 0; i <  m_bandSelectors.size(); ++i )
			{
				m_bandSelectors[ i ].valueA = bands[ i ][ 0 ];
				m_bandSelectors[ i ].valueB = bands[ i ][ 1 ];		
				const auto & c = colors[ i % N_COLORS ]; 
				m_bandSelectors[ i ].button.color( { c.r, c.g, c.b } );
			}
		}
	} 

	BandSelectorFilterView( const std::string & resourceDir ) : FilterView( resourceDir )
	{
		m_label = "Band Selection";
		m_valid = false;
	}

    virtual bool handleInput( const InputState & input ) override
    {
    	bool changed = FilterView::handleInput( input );
    	TN::Vec2< double > pos = input.mouseState.position;

    	for( auto & module : m_bandSelectors )
    	{
			if( module.button.pointInViewPort( pos ) && input.mouseState.event == MouseEvent::LeftPressed ) {
				module.button.setPressed( ! module.button.isPressed() );
			}
    	}

        return changed;
    }

    virtual void render( 
        TN::Renderer2D & renderer,
        TN::TextRenderer & textRenderer,
        const float windowWidth,
        const float windowHeight) override 
    {
        renderer.clearBox(
            position().x(),
            position().y(),
            size().x(),
            size().y(),
            TN::Vec4( 0.9f, 0.9f, 0.9f, 1.0f ) );

    	FilterView::renderHeader( renderer, textRenderer, windowWidth, windowHeight );
    
    	if( ! isCollapsed() )
    	{
	    	for( auto & module : m_bandSelectors )
	    	{
				renderer.renderPressButton( & module.button, { 0.2, 0.2, 0.2 }, windowWidth, windowHeight, true );

				std::string vA = TN::to_string_with_precision( module.valueA );
				std::string vB = TN::to_string_with_precision( module.valueB );

		        textRenderer.renderText(
		            windowWidth,
		            windowHeight,
		            "[ " + vA + ", " + vB + " )",
		            { module.button.position().x() + module.button.size().x() + 10, module.button.position().y() + 4 },
		            { 0.0f, 0.0f, 0.0f, 1.f },
		            false,
		            18 );

		        textRenderer.renderText(
		            windowWidth,
		            windowHeight,
		            ( module.button.isPressed() ? "selected" : "unselected" ),
		            { 
		            	module.button.position().x() + module.button.size().x() + 10 + ( vA.size() + vB.size() + 6 ) * 8, 
		             	module.button.position().y() + 4 
		            },
		            ( module.button.isPressed() ? TN::Vec4( { 0.0f, 0.6f, 0.1f, 1.f } ): TN::Vec4( { 0.6f, 0.1f, 0.0f, 1.f } ) ),
		            false,
		            18 );
	    	}
    	}
    }

    virtual void applyLayout( float X0, float YT, float W ) override 
    {
    	FilterView::applyLayout( X0, YT, W );

    	float Y = 34;
    	
    	if( ! isCollapsed() )
    	{
	    	for( auto & module : m_bandSelectors )
	    	{
	    		Y += 34;
	    		module.button.setPosition( position().x() + 10, YT - Y );
	    	}

	    	Y +=8;
    	}

    	setSize( W, Y );
    	setPosition( X0, YT - Y );
    }

	virtual ~BandSelectorFilterView() = default;
};

}

#endif