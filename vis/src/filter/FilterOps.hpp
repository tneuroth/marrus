#ifndef TN_FILTER_OPS_HPP
#define TN_FILTER_OPS_HPP

#include "geometry/Vec.hpp"
#include "geometry/GeometryUtils.hpp"

#include <cstdint>
#include <vector>

#if (__cplusplus >= 201703L) || (defined(_MSVC_LANG) && (_MSVC_LANG >= 201703L) && (_MSC_VER >= 1913))
    #if __has_include(<variant>)
        #include <variant>
        namespace MarLib = std;
    #else
        #include <boost/variant.hpp>
        namespace MarLib = boost;
    #endif
#else
     #include <boost/variant.hpp>
     namespace MarLib = boost;
#endif

using FlagPtrVariant = MarLib::variant< int8_t*, int16_t*, int32_t*, uint8_t*, uint16_t*, uint32_t* >;
using ConstIdPtrVariant   = MarLib::variant< const int8_t*, const int16_t*, const int32_t*, const int64_t*, const uint8_t*, const uint16_t*, const uint32_t*, const uint64_t*>;

namespace Marrus
{

namespace FilterOp
{

enum class Type
{
    New,
    Intersect,
    Union,
    SetMinus
};

static constexpr uint8_t FILTER_LEVEL_BAND      = 0;
static constexpr uint8_t FILTER_LEVEL_COMPONENT = 1;
static constexpr uint8_t FILTER_LEVEL_CELL      = 2;
static constexpr uint8_t FILTER_LEVEL_ELEMENT   = 3;

static constexpr uint32_t WITHIN_DECOMPOSITION        = 1;
static constexpr uint32_t WITHIN_BAND_SELECTION       = 2;
static constexpr uint32_t WITHIN_COMPONENT_SELECTION  = 4;
static constexpr uint32_t WITHIN_CELL_SELECTION       = 8;
static constexpr uint32_t WITHIN_VOXEL_SELECTION      = 16;
static constexpr uint32_t WITHIN_VOXEL_SUB_SELECTION  = 32;

// =================================================================================

template< typename T >
static inline void New( 
	T & bitflag,
	const bool boolCond,
	const T BIT_MASK )
{	
	bitflag = bitflag & ~ BIT_MASK;        // turn the bit off
	bitflag |= BIT_MASK * ( T ) boolCond;  // exists in new selection
}

template< typename T >
static inline void Intersect( 
	T & bitflag,
	const bool boolCond,
	const T BIT_MASK )
{	
	bool wasSet = bitflag & BIT_MASK;
	bitflag = bitflag & ~ BIT_MASK;       // turn the bit off
	bitflag |= BIT_MASK * ( T ) boolCond * ( T ) wasSet; // exists in existing selection and new selection
}

template< typename T >
static inline void Union( 
	T & bitflag,
	const bool boolCond,
	const T BIT_MASK )
{	
	bitflag |= BIT_MASK * ( T ) boolCond; // exists in existing selection or new selection
}

template< typename T >
static inline void SetMinus( 
	T & bitflag,
	const bool boolCond,
	const T BIT_MASK )
{	
	bool wasSet = bitflag & BIT_MASK;
	bitflag     = bitflag & ~ BIT_MASK;                     // turn the bit off
	bitflag    |= BIT_MASK * ( T ) ( ! ( bool) boolCond ) * ( T ) wasSet; // exists in existing selection and not in new selection
}

// ========================================================================================================


template< typename T >
static inline void New( 
	T * bitflags,
	uint8_t * boolCond,
	const int64_t N,	
	const T BIT_MASK )
{
	#pragma omp parallel for simd
	for( size_t i = 0; i < N; ++i )
	{
		New( bitflags[ i ], boolCond[ i ], BIT_MASK );
	}
}

template< typename T >
static inline void Intersect( 
	T * bitflags,
	uint8_t * boolCond,
	const int64_t N,	
	const T BIT_MASK )
{	
	#pragma omp parallel for simd
	for( size_t i = 0; i < N; ++i )
	{
		Intersect( bitflags[ i ], boolCond[ i ], BIT_MASK );
	}
}

template< typename T >
static inline void Union( 
	T * bitflags,
	uint8_t * boolCond,
	const int64_t N,	
	const T BIT_MASK )
{	
	#pragma omp parallel for simd
	for( size_t i = 0; i < N; ++i )
	{
		Union( bitflags[ i ], boolCond[ i ], BIT_MASK );
	}
}

template< typename T >
static inline void SetMinus( 
	T * bitflags,
	uint8_t * boolCond,
	const int64_t N,	
	const T BIT_MASK )
{	
	#pragma omp parallel for simd
	for( size_t i = 0; i < N; ++i )
	{
		SetMinus( bitflags[ i ], boolCond[ i ], BIT_MASK );
	}
}

template< typename FLAG_T, typename MASK_T >
static inline void Apply( 
	FLAG_T * bitflags,
	uint8_t * boolCond,
	const int64_t N,
	const MASK_T BIT_MASK,
	FilterOp::Type type )
{
	if ( type == FilterOp::Type::New )
	{
		FilterOp::New( bitflags, boolCond, N, static_cast<FLAG_T>( BIT_MASK ) );
	}
	else if ( type == FilterOp::Type::Intersect )
	{
		FilterOp::Intersect( bitflags, boolCond, N, static_cast<FLAG_T>( BIT_MASK ) );
	}
	else if ( type == FilterOp::Type::Union )
	{
		FilterOp::Union( bitflags, boolCond, N, static_cast<FLAG_T>( BIT_MASK ) );
	}
	else if ( type == FilterOp::Type::SetMinus )
	{
		FilterOp::SetMinus( bitflags, boolCond, N, static_cast<FLAG_T>( BIT_MASK ) );	
	}	
}

template< typename FLAG_T, typename MASK_T  >
static inline void Apply( 
	FlagPtrVariant bitFlags,
	FLAG_T * boolCond,
	const int64_t N,
	const MASK_T BIT_MASK,
	FilterOp::Type type )
{
	if( MarLib::holds_alternative<int8_t*>( bitFlags ) ) {
		Marrus::FilterOp::Apply(
			MarLib::get<int8_t*>( bitFlags ),
			boolCond,
			N,
			BIT_MASK,
			type );
	}
	else if( MarLib::holds_alternative<int16_t*>( bitFlags ) ) {
		Marrus::FilterOp::Apply(
			MarLib::get<int16_t*>( bitFlags ),
			boolCond,
			N,
			BIT_MASK,
			type );
	}
	else if( MarLib::holds_alternative<int32_t*>( bitFlags ) ) {
		Marrus::FilterOp::Apply(
			MarLib::get<int32_t*>( bitFlags ),
			boolCond,
			N,
			BIT_MASK,
			type );
	}
	else if( MarLib::holds_alternative<uint8_t*>( bitFlags ) ) {
		Marrus::FilterOp::Apply(
			MarLib::get<uint8_t*>( bitFlags ),
			boolCond,
			N,
			BIT_MASK,
			type );
	}
	else if( MarLib::holds_alternative<uint16_t*>( bitFlags ) ) {
		Marrus::FilterOp::Apply(
			MarLib::get<uint16_t*>( bitFlags ),
			boolCond,
			N,
			BIT_MASK,
			type );
	}
	else if( MarLib::holds_alternative<uint32_t*>( bitFlags ) ) {
		Marrus::FilterOp::Apply(
			MarLib::get<uint32_t*>( bitFlags ),
			boolCond,
			N,
			BIT_MASK,
			type );
	}
}

template< typename T >
inline void applyLasso( 
    std::vector< T > & flags,
    const std::vector< float > & x, 
    const std::vector< float > & y, 
    const std::vector< TN::Vec2< float > > & lasso,
    const TN::Vec2< float > & xRange,
    const TN::Vec2< float > & yRange, 
    FilterOp::Type mode )
{
    const size_t NV = flags.size();

    #pragma omp parallel for
    for( size_t i = 0; i < NV; ++i )
    {
        // exists with outer selection
        if( flags[ i ] & WITHIN_VOXEL_SELECTION ) 
        { 
        	bool within_lasso = false;

			if( x[ i ] >= xRange.a()
			 && x[ i ] <= xRange.b()
			 && y[ i ] >= yRange.a()
			 && y[ i ] <= yRange.b() )
			{
				if( pnpoly( lasso, { x[ i ], y[ i ] } ) )
				{
					within_lasso = true;
				}
			}	
		
			if( mode == FilterOp::Type::New )
			{
				FilterOp::New( flags[ i ], within_lasso, static_cast<T>(  WITHIN_VOXEL_SUB_SELECTION ) );
			}
			else if( mode == FilterOp::Type::Intersect )
			{
				FilterOp::Intersect( flags[ i ], within_lasso, static_cast<T>(  WITHIN_VOXEL_SUB_SELECTION ) );
			}
			else if( mode == FilterOp::Type::Union )
			{
				FilterOp::Union( flags[ i ], within_lasso, static_cast<T>(  WITHIN_VOXEL_SUB_SELECTION ) );
			}
			else if( mode == FilterOp::Type::SetMinus )
			{
				FilterOp::SetMinus( flags[ i ], within_lasso, static_cast<T>(  WITHIN_VOXEL_SUB_SELECTION ) );
			}
		}
    }
}

template< typename BIT_MASK_T, typename D_TYPE >
static inline void gatherElements( 
    const std::vector< BIT_MASK_T >              & elemBitFlags,
    const std::vector< std::vector< D_TYPE > * > & vals, 
    const std::vector< std::vector< D_TYPE > * > & g_vals,    
    std::vector< BIT_MASK_T >                    & g_f,
    std::vector< std::int64_t >                  & gsub_indicies,
    const std::vector< std::vector< D_TYPE > * > & gSub_vals,    
    std::vector< BIT_MASK_T >                    & gSub_f )
{
	const size_t NV = elemBitFlags.size();

    for( auto & gv : g_vals )
    {
        gv->clear();
    }
    
    g_f.clear();

    for( auto & gv : gSub_vals )
    {
        gv->clear();
    }
    gSub_f.clear();
    gsub_indicies.clear();

    const size_t NUM_VARS = vals.size();

    for( size_t i = 0; i < NV; ++i )
    {
		if( elemBitFlags[ i ] & WITHIN_COMPONENT_SELECTION )
		{
		    g_f.push_back( elemBitFlags[ i ] );
		    for( size_t k = 0; k < NUM_VARS; ++k )
		    {
		        g_vals[ k ]->push_back( ( * ( vals[ k ] ) )[ i ] );
		    }

		    if( elemBitFlags[ i ] & WITHIN_VOXEL_SELECTION )
		    {
		    	gsub_indicies.push_back( i );
		        gSub_f.push_back( elemBitFlags[ i ] );
		        for( size_t k = 0; k < NUM_VARS; ++k )
		        {
		            gSub_vals[ k ]->push_back( ( * ( vals[ k ] ) )[ i ] );
		        }
		    }
		}
    }
}

} // end namespace FilterOp

} // end namespace Marrus

#endif