#ifndef TN_MARRUS_FILTER_VIEW_C_HPP
#define TN_MARRUS_FILTER_VIEW_C_HPP

#include "input/SimpleDataManager.hpp"
#include "views/Widget.hpp"
#include "views/InputState.hpp"
#include "views/PressButtonWidget.hpp"
#include "geometry/Vec.hpp"
#include "render/RenderModule2D/Renderer2D.hpp"
#include "render/TextRenderer/TextRenderer.hpp"
#include "expressions/Expressions.hpp"
#include "filter/FilterOps.hpp"
#include "Tessellation.hpp"

#include <jsonhpp/json.hpp>

#include <vector>
#include <memory>
#include <iostream>
#include <cmath>

namespace Marrus {

class FilterView : public TN::Widget {

    TN::TexturedPressButton m_activateButton;
    TN::TexturedPressButton m_removeButton;
    TN::TexturedPressButton m_expandButton;
    bool m_removalRequested;

    protected :

    TN::Vec3< float > m_headerColor;
    std::string m_label;
    std::string m_resourceDir;

    virtual void configure( const nlohmann::json & j ) = 0;

    virtual void init() 
    {
        m_activateButton.setTexFromPNG(
            m_resourceDir + "/textures/active.png",
            m_resourceDir + "/textures/activePressed.png" );

        m_removeButton.setTexFromPNG(
            m_resourceDir + "/textures/remove.png",
            m_resourceDir + "/textures/removePressed.png" );

        m_expandButton.setTexFromPNG(
            m_resourceDir + "/textures/upArrow.png",
            m_resourceDir + "/textures/downArrow.png" );

        m_activateButton.resizeByHeight(   30.f );
        m_removeButton.resizeByHeight(     30.f );
        m_expandButton.resizeByHeight(   30.f );
    
        m_removalRequested = false;
        m_headerColor = { 0.85f, 0.99f, 0.9f };

        m_expandButton.setPressed(   true );
        m_activateButton.setPressed( true );
    }

public: 

    TN::Vec3< float > headerColor() const 
    {
        return m_headerColor;
    }

    virtual void apply(
        Marrus::Expressions & exprProcessor,
        TN::SimpleDataManager< float > * dm,
        const std::map< std::string, long double  > & constants,
        const std::map< std::string, std::string  > & derivedConstants,
        const std::set< std::string >               & variables,
        const std::map< std::string, std::string  > & derivedVariables,
        const ConstIdPtrVariant & comp_band_ids,
        const ConstIdPtrVariant & cell_comp_ids,
        const ConstIdPtrVariant & elem_cell_ids,
        const int64_t N,
        const uint8_t filterLevel,
        FlagPtrVariant bitFlags,
        Marrus::FilterOp::Type op,
        int BIT_MASK ) = 0;

    virtual bool valid() const = 0;

    bool active()
    {
        return m_activateButton.isPressed();
    }

    virtual void updateInformation(  
        Marrus::Expressions & exprProcessor,
        const std::map< std::string, long double  > & constants,
        const std::map< std::string, std::string  > & derivedConstants,
        const std::set< std::string >               & variables,
        const std::map< std::string, std::string  > & derivedVariables,
        const Marrus::Tessellation & tesselation )
    {}

    bool isCollapsed() const 
    {
        return ! m_expandButton.isPressed();
    }

    std::vector< TN::TexturedPressButton * > buttons()
    {
        std::vector< TN::TexturedPressButton * > v;
        v.push_back( & m_activateButton );
        v.push_back( & m_removeButton   );
        v.push_back( & m_expandButton );
        return v;
    } 

    virtual void render( 
        TN::Renderer2D & renderer,
        TN::TextRenderer & textRenderer, 
        const float windowWidth,
        const float windowHeight) = 0;

    virtual void renderHeader( 
        TN::Renderer2D & renderer,
        TN::TextRenderer & textRenderer,        
        const float windowWidth,
        const float windowHeight)
    {
        renderer.clearBox(
            position().x(),
            position().y() + size().y() - 34,
            size().x(),
            34,
            TN::Vec4( 
                m_headerColor.r(), 
                m_headerColor.g(), 
                m_headerColor.b(),  
                0.9f ) );

        glViewport( 0, 0, windowWidth, windowHeight );

        float top  = position().y() + size().y() - 24;
        float left = position().x() + 20;

        textRenderer.renderText(
            windowWidth,
            windowHeight,
            m_label,
            { left + 30, top },
            { 0.0f, 0.0f, 0.0f, 1.f },
            false,
            18 );

        std::vector< TN::TexturedPressButton * > bts = buttons();
        for( auto b : bts ) {
            renderer.renderPressButton( b, { 0.2, 0.2, 0.2 }, windowWidth, windowHeight, true );
        }
    }

    virtual void clearRequests()
    {}

    virtual std::vector< TN::ComboWidget * > visibleCombos() {
        return {};
    }

    virtual bool requestDefine() const
    {
        return false;
    }

    virtual std::string defineType() const 
    {
        return "";
    }

    virtual bool handleInput( 
        const InputState & input )
    {
        auto p = input.mouseState.position;
        MouseButtonState mouseButtonState = input.mouseState.buttonState;
        bool changed = false;

        if( pointInViewPort( p ) )
        {
            if( m_activateButton.pointInViewPort( p ) ) 
            {
                if ( input.mouseState.event == MouseEvent::LeftPressed ) {
                    m_activateButton.setPressed( ! m_activateButton.isPressed() );
                    changed = true;
                }
            }
            if( m_removeButton.pointInViewPort( p )  ) {                
                if( input.mouseState.event == MouseEvent::LeftReleased )
                {
                    if( m_removeButton.isPressed() ) {
                        m_removalRequested = true;
                    }
                    changed = true;
                    m_removeButton.setPressed( false );
                }
                else if ( input.mouseState.event == MouseEvent::LeftPressed ) {
                    m_removeButton.setPressed( true );
                    changed = true;
                }
            }
            if( m_expandButton.pointInViewPort( p ) ) 
            {
                if ( input.mouseState.event == MouseEvent::LeftPressed ) {
                    m_expandButton.setPressed( ! m_expandButton.isPressed() );
                    changed = true;
                }
            }
        }

        return changed;
    }

    virtual void applyLayout(         
        float X0, 
        float YT,
        float W ) 
    {
        m_activateButton.setPosition( X0 + W - 32.f, YT - 32 );
        m_removeButton.setPosition(   X0 + W - 66.f, YT - 32 );
        m_expandButton.setPosition( X0 + 2,        YT - 32 );
    }

    FilterView( const std::string & resourceDir ) : 
        Widget(),
        m_resourceDir( resourceDir )
    {
        init();
    }

    bool requestRemoval() const 
    {
        return m_removalRequested;
    }

    virtual ~FilterView() {}
};

}

#endif