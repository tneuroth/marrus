#ifndef TN_BOOLEAN_EXPRESSION_VIEW_HPP
#define TN_BOOLEAN_EXPRESSION_VIEW_HPP

#include "input/SimpleDataManager.hpp"
#include "expressions/Expressions.hpp"
#include "utils/ParseUtils.hpp"
#include "filter/FilterOps.hpp"
#include "filter/FilterView.hpp"
#include "views/ExpressionEdit.hpp"
#include "BooleanExpressionFilter.hpp"
#include "render/RenderModule2D/Renderer2D.hpp"
#include "render/TextRenderer/TextRenderer.hpp"
#include "Tessellation.hpp"

#include <jsonhpp/json.hpp>

#include <memory>

namespace Marrus {
	
class BooleanExpressionFilterView : public Marrus::FilterView {

	TN::ExpressionEdit m_edit;
	bool m_valid;

	std::map< std::string, TN::Vec3< float > > m_syntaxHighlighting;
	std::vector< std::string > m_expressionTokens;

public:
	
	virtual void apply(
		Marrus::Expressions & exprProcessor,
       	TN::SimpleDataManager< float > * dm,
        const std::map< std::string, long double  > & constants,
        const std::map< std::string, std::string  > & derivedConstants,
        const std::set< std::string >               & variables,
        const std::map< std::string, std::string  > & derivedVariables,
        const ConstIdPtrVariant & comp_band_ids,
        const ConstIdPtrVariant & cell_comp_ids,
        const ConstIdPtrVariant & elem_cell_ids,
        const int64_t N,        
        const uint8_t filterLevel,
        FlagPtrVariant bitFlags,
        Marrus::FilterOp::Type op,
        int BIT_MASK ) override
	{
		std::vector< uint8_t > exprResult( N, 0 );

		std::map< std::string, float * > inputData;
		std::map< std::string, double > normalizationFactors;
		std::map< std::string, TN::Vec2< double > > ranges;

		for( auto & v : variables )
		{
			std::vector< float  > * vptr = dm->get( v );
			if( vptr == nullptr )
			{
				std::cerr << "Error in BooleanExpressionFilterView: tried accessing variable " << v 
				<< ", SimpleDataManager::get return nullptr" << std::endl; 
				exit( 1 );
			}

			if( vptr->size() != N )
			{
				std::cerr << "Error in BooleanExpressionFilterView: tried accessing variable " << v 
				<< ", SimpleDataManager::get return vector<float>* with size " << vptr->size()
				<< ", not equal to size of bitflags " << N << std::endl; 
				exit( 1 );	
			}

			inputData.insert( {
				v,
				vptr->data(),
			} );

			ranges.insert( {
				v,
				dm->getRange( v )
			} );
		}
		
		std::cout << "Processing: " << expression() << std::endl;

		bool success = exprProcessor.attemptProcess( 
			expression(),
			N,
			constants,
			derivedConstants,
			inputData,
			ranges,
			derivedVariables,		
			exprResult.data() );

		if( success == false )
		{
			std::cerr << "Error in BooleanExpressionFilterView: unable to process expression, make sure it is valid " 
			<< std::endl; 
		}
		else
		{
			Marrus::FilterOp::Apply(
				bitFlags,
				exprResult.data(),
				N,
				BIT_MASK,
				op );
		}
	}

	virtual bool valid() const override 
	{
		return m_valid;
	}

	virtual void configure( const nlohmann::json & j ) override
	{

	}

	virtual void updateInformation(  
		Marrus::Expressions & exprProcessor,
        const std::map< std::string, long double  > & constants,
        const std::map< std::string, std::string  > & derivedConstants,
        const std::set< std::string >               & variables,
        const std::map< std::string, std::string  > & derivedVariables,
        const Marrus::Tessellation & tesselation ) override
	{
		std::string exp = expression();

		m_valid = exprProcessor.validate(
			expression(),
			constants,
			derivedConstants,
			variables,
			derivedVariables );

		TN::Vec3< float > constColor = { 0.3, 0.7, 0.5 };

		m_syntaxHighlighting.clear();
		for( auto & c : constants )
		{
			m_syntaxHighlighting.insert( { c.first, constColor } );
		}
		for( auto & c : derivedConstants )
		{
			m_syntaxHighlighting.insert( { c.first, constColor } );
		}
	
		TN::Vec3< float > varColor = { 0.0, 0.2, 0.9 };

		m_syntaxHighlighting.clear();
		for( auto & v : variables )
		{
			m_syntaxHighlighting.insert( { v, varColor } );
		}
		for( auto & v : derivedVariables )
		{
			m_syntaxHighlighting.insert( { v.first, varColor } );
		}

		TN::Vec3< float > kwColor = { 0.5, 0.6, 0.2 };

		for( auto & kw : Marrus::ExprtkKeyWords )
		{
			m_syntaxHighlighting.insert( { kw, kwColor } );
		}

		TN::Vec3< float > opColor = { 0.4, 0.1, 0.2 };

		for( auto & op : Marrus::ExprtkOperators )
		{
			m_syntaxHighlighting.insert( { op, opColor } );
		}

		// extract numbers in expression

		TN::Vec3< float > numColor = { 0.3, 0.2, 0.7 };
		m_expressionTokens = exprProcessor.extractTokens( exp );

		for( auto & token : m_expressionTokens )
		{
			if( TN::validateNumber( token ) )
			{
				m_syntaxHighlighting.insert( { token, numColor } );
			}
		}

		TN::Vec3< float > invColor = { 0.2, 0.2, 0.2 };
		for( auto token : m_expressionTokens )
		{
			if( ! m_syntaxHighlighting.count( token ) )
			{
				m_syntaxHighlighting.insert( { token, invColor } );
			}
		}
	} 

	std::string expression() const 
	{
		return m_edit.text();
	}

	BooleanExpressionFilterView( const std::string & resourceDir ) : FilterView( resourceDir )
	{
		m_edit.label( "Expr." );
		m_label = "Boolean Expression";
		m_valid = false;
	}

    virtual bool handleInput( const InputState & input ) override
    {
    	bool changed = FilterView::handleInput( input );

    	if( ! isCollapsed() )
    	{
        	bool c = m_edit.handleInput( input );
    	}      
        return changed;
    }

    virtual void render( 
        TN::Renderer2D & renderer,
        TN::TextRenderer & textRenderer,
        const float windowWidth,
        const float windowHeight) override 
    {
        renderer.clearBox(
            position().x(),
            position().y(),
            size().x(),
            size().y(),
            TN::Vec4( 0.9f, 0.9f, 0.9f, 1.0f ) );

    	FilterView::renderHeader( renderer, textRenderer, windowWidth, windowHeight );
    	
    	if( ! valid() )
    	{
	        glViewport( 0, 0, windowWidth, windowHeight );

	        float top  = position().y() + size().y() - 24;
	        float left = position().x() + 20;

	        textRenderer.renderText(
	            windowWidth,
	            windowHeight,
	            "invalid expression",
	            { left + 230, top },
	            { 1.0f, 0.0f, 0.0f, 1.f },
	            false,
	            16 );
    	}

    	if( ! isCollapsed() )
    	{
    		renderer.renderExpressionEdit( 
    			m_edit, 
    			windowWidth, 
    			windowHeight, 
    			{ 0.2, 0.2, 0.2 },
    			m_expressionTokens,
    			m_syntaxHighlighting );
    	}
    }

    virtual void applyLayout( float X0, float YT, float W ) override 
    {
    	FilterView::applyLayout( X0, YT, W );

    	const float Y = isCollapsed() ? 34 : 68;
    	
    	m_edit.setPosition( X0 + 100, YT - Y ),
    	m_edit.setSize( W - 100, 34 ); 

    	setSize( W, Y );
    	setPosition( X0, YT - Y );
    }

	virtual ~BooleanExpressionFilterView() = default;
};

}

#endif