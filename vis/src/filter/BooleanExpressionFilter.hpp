#ifndef TN_MARRUS_BOOLEAN_EXPRESSION_FILTER_HPP
#define TN_MARRUS_BOOLEAN_EXPRESSION_FILTER_HPP

#include "filter/Filter.hpp"
#include "filter/FilterOps.hpp"
#include <jsonhpp/json.hpp>

#include <vector>
#include <iostream>
#include <cstdint>
#include <string>

#if (__cplusplus >= 201703L) || (defined(_MSVC_LANG) && (_MSVC_LANG >= 201703L) && (_MSC_VER >= 1913))
	#if __has_include(<any>)
		#include <any>
	 using Any = std::any;
	#else
		#include <boost/any.hpp>
		using Any = boost::any;
	#endif
#else
	 #include <boost/any.hpp>
	 using Any = boost::any;
#endif

namespace Marrus {

class BooleanExpressionFilter : public Marrus::Filter
{
	virtual std::pair< uint8_t, std::string > commit()
	{
		return { Filter::NOT_READY, "" };
	};

	virtual std::pair< uint8_t, std::string >  apply()
	{
		return { Filter::NOT_READY, "" };
	};

public :

	virtual std::pair< uint8_t, std::string > attemptCommit() 
	{
		status = commit();
		return status;
	}

	virtual std::pair< uint8_t, std::string > attemptApply(
		std::vector< uint8_t > & bitflags,
		const uint8_t & level,
		const Marrus::FilterOp::Type operand, 
		const std::map< std::string, Any > features ) 
	{
		status = apply();
		return status;
	}

	virtual void fromJSON( const nlohmann::json & j )
	{

	};

	virtual ~BooleanExpressionFilter() = default;
};

}

#endif