#ifndef TN_MULTI_LEVEL_FILTER_PIPELINE_HPP
#define TN_MULTI_LEVEL_FILTER_PIPELINE_HPP

#include "filter/FilterGroup.hpp"
#include "filter/DataFlowEngine.hpp"

#include <jsonhpp/json.hpp>

#include <vector>
#include <iostream>
#include <cstdint>
#include <string>
#include <memory>

/***************************************************************

 Working Model

 The pipeline operates on multi-level data, starting from the
 highest level, in stages, down to the lowest level.

 At each level you can have a series of internal stages, where
 each stage is encapsulated by a FilterGroup

 A filter group encapsulates a set of filters, and could be 
 either staged, based on expressions implementing boolean combination,
 or based on linked selections in sets of spatial views.

 ***************************************************************/

namespace Marrus {

class FilterPipelineLevel 
{
	std::vector< std::unique_ptr< Marrus::FilterGroup > > m_filterGroups;
	std::vector< int64_t > m_selectedIds;

public: 

	void apply( 
		Marrus::DataFlowEngine * dataFlowEngine,
		const std::vector< int64_t > & ids ) 
	{
		m_selectedIds = ids;
		size_t NF = m_filterGroups.size();
		for( size_t i = 0; i < NF; ++i )
		{
			m_filterGroups[ i ]->apply( m_selectedIds, dataFlowEngine );
		}
	}

	const std::vector< int64_t > & selectedIds() const 
	{
		return m_selectedIds;
	}
};

template< class ID_TYPE > 
class SparseNestedHierarchicalFilterPipeline 
{
	std::vector< std::unique_ptr< Marrus::FilterPipelineLevel > > levels;

public:

	void apply( Marrus::NestedHierachicalDataFlowEngine * dataFlowEngine ) 
	{
		std::vector< ID_TYPE > ids = dataFlowEngine->topLevelIds();

		const int N_LEVELS = levels.size();
		for( int l = N_LEVELS - 1; l >= 0; ++l )
		{
			// apply the filters to this level
			levels[ l ]->apply( ids, dataFlowEngine );
			const std::vector< ID_TYPE > & selection = levels[ l ]->selectedIds();

			if( l > 0 ) 
			{
				// get the ids of the lower level elements encapsulated by the selection
				ids = dataFlowEngine->extractLowerLevelSubset( l, selection );
			}
		}
	}
};

}

#endif