
#ifndef TN_MARRUS_DATA_FLOW_ENGINE_HPP
#define TN_MARRUS_DATA_FLOW_ENGINE_HPP

#include <cstdint>

namespace Marrus {

class AccessEngine 
{

public:

	virtual ~AccessEngine() = default;
};

class ProcessingEngine 
{

public:

	virtual ~ProcessingEngine() = default;
};

class ResourceManagementSystem
{

public:

	virtual ~ResourceManagementSystem() = default;

};

class DataFlowEngine 
{
	std::unique_ptr< Marrus::AccessEngine > m_accessEngine;
	std::unique_ptr< Marrus::ProcessingEngine > m_processingEngine;
	std::unique_ptr< Marrus::ResourceManagementSystem > m_resourceManagementSystem;

public:

	virtual ~DataFlowEngine() = default;
};

class NestedHierachicalDataFlowEngine : public DataFlowEngine
{

public:

	virtual std::vector< int64_t > extractLowerLevelSubset(
		int l,
		std::vector< int64_t > superSetIds ) = 0;
	virtual const std::vector< int64_t > & topLevelIds() = 0;
	virtual ~NestedHierachicalDataFlowEngine() = default;
};

}

#endif

/* E.g. from LSRCVT we get levels, with features at each level

L0 : bands
L1 : components 
L2 : component blocks
L3 : voronoi cells 
L4 : voxels    

*/

/*

	DataFlowEngine 
		AccessEngine
		ProcessingEngine
		ResourceManagementSystem

	(1) File System: e.g., dhffdbs
	(2) File System Access
	(3) Resource Managagement and Processing

*/