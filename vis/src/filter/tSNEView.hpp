#ifndef TN_TSNE_VIEW_HPP
#define TN_TSNE_VIEW_HPP

#include "SpatialEmbeddingView.hpp"

#include <jsonhpp/json.hpp>

namespace Marrus {

struct Feature {

};

class tSNE_View : public Marrus::SpatialEmbeddingView {

	std::string m_backend;

	float       m_perplexity;
	float       m_learning_rate;
	int         m_n_iter;

	std::vector< std::string > m_featureKeys;

public:

	virtual void configure( const nlohmann::json & j )
	{

	}

	virtual ~tSNE_View() = default;
};

}

#endif