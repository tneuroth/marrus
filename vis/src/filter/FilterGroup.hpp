#ifndef TN_MARRUS_FILTER_GROUP_HPP
#define TN_MARRUS_FILTER_GROUP_HPP

#include "filter/Filter.hpp"
#include "filter/DataFlowEngine.hpp"

#include <jsonhpp/json.hpp>

#include <vector>
#include <iostream>
#include <cstdint>
#include <string>
#include <memory>

namespace Marrus {

class FilterGroup
{
	std::vector< std::string > filterSymbols;
	std::vector< std::unique_ptr< Marrus::Filter > > filters;
	std::vector< uint8_t > activeFilters;

public: 

	virtual void apply( 
		std::vector< int64_t > & ids,
		Marrus::DataFlowEngine * database ) = 0;

	virtual void addFilter( const nlohmann::json & json ) = 0;
	virtual ~FilterGroup() = default;
};

// class CombinatoryFilterGroup : public FilterGroup
// {
// 	std::string expression;

// public: 

// 	virtual void apply( 
// 		std::vector< int64_t > & ids,
// 		const SimpleDatabase & database ) override 
// 	{

// 	}

// 	virtual void addFilter( const nlohmann::json & json ) override 
// 	{

// 	}

// 	virtual ~CombinatoryFilterGroup() = default;
// };

// class StagedFilterGroup : public FilterGroup
// {

// public: 

// 	virtual void apply( 
// 		std::vector< int64_t > & ids,
// 		const SimpleDatabase & database ) override 
// 	{
		
// 	}

// 	virtual void addFilter( const nlohmann::json & json ) override 
// 	{

// 	}

// 	virtual ~StagedFilterGroup() = default;
// };

// class PlotFilterGroup : public FilterGroup
// {

// public: 

// 	virtual void apply( 
// 		std::vector< int64_t > & ids,
// 		const SimpleDatabase & database ) override 
// 	{
		
// 	}

// 	virtual void addFilter( const nlohmann::json & json ) override 
// 	{

// 	}

// 	virtual ~PlotFilterGroup() = default;
// };

}

#endif