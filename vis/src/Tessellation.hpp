#ifndef TN_MARRUS_TESSELATION_HPP
#define TN_MARRUS_TESSELATION_HPP

#include "utils/io.hpp"

#include <string>
#include <array>
#include <vector>
#include <cstdint>
#include <set>
#include <map>

namespace Marrus
{

struct SummaryFeature
{
    std::string name;
    std::string latex;
    int dataOffsetIndex;
    std::string type;
    std::vector< int > dims;
    std::vector< float > values;
    std::vector< std::string > dimLabels;
    std::string description;
};

struct DerivedSummaryFeature
{
    std::string name;
    std::string latex;
    std::vector< int > dims;
    std::vector< std::string > dimLabels;
    std::string expression;
    std::string description;
    std::vector< float > values;
};

struct Summary
{
    std::string filePath;
    std::map< std::string, Marrus::SummaryFeature        > features;
    std::map< std::string, Marrus::DerivedSummaryFeature > derivedFeatures;

    // =====================================================================

    std::set< std::string > scalarFeaturesParseInfo()
    {
        std::set< std::string > result;

        for( auto & e : features )
        {
            auto & f = e.second;
            if( f.dims.size() == 1 )
            {
                result.insert( f.name );
            }
        }
        
        return result;
    }

    std::map< std::string, std::string > scalarDerivedFeaturesParseInfo()
    {
        std::map< std::string, std::string > result;

        for( auto & e : derivedFeatures )
        {
            auto & f = e.second;
            if( f.dims.size() == 1 )
            {
                result.insert( { f.name, f.expression } );
            }
        }
        
        return result;
    }

    // =========================================================================
};

struct MultiLevelSummarization
{
    Marrus::Summary globalSummary;
    Marrus::Summary bandSummary;
    Marrus::Summary componentSummary;
    Marrus::Summary cellSummary;
};

struct Tessellation 
{
    void fromFile( const std::string & directory ) 
    {
        std::vector< int64_t > tmp( 7 );

        loadData( 
            directory + "n_lyr_comp_sites_x_y_z.bin", 
            tmp,
            0,
            static_cast< int64_t >( tmp.size() )
        );

        const int64_t NL = tmp[ 0 ];
        m_NC             = tmp[ 1 ];
        m_nSites         = tmp[ 2 ];
        M_DIMS[ 0 ]      = tmp[ 3 ];
        M_DIMS[ 1 ]      = tmp[ 4 ];
        M_DIMS[ 2 ]      = tmp[ 5 ];
        M_NV = M_DIMS[ 0 ] * M_DIMS[ 1 ] * M_DIMS[ 2 ];
        m_hasBackground = static_cast< bool >( tmp[ 6 ] );

        m_sitePoints.resize( m_nSites*3 );
        loadData( directory + "sitePoints.bin", m_sitePoints, 0, m_nSites*3 );

        for( size_t i = 0, end = m_nSites; i < end; ++i ) 
        {
            m_sitePoints[ i*3   ] = m_sitePoints[ i*3   ] * M_DIMS[ 0 ];
            m_sitePoints[ i*3+1 ] = m_sitePoints[ i*3+1 ] * M_DIMS[ 1 ];
            m_sitePoints[ i*3+2 ] = m_sitePoints[ i*3+2 ] * M_DIMS[ 2 ];
        }

        loadData( directory + "site.principle_direction_1.bin", m_siteDirection1,        0, m_nSites*3 );
        loadData( directory + "site.principle_direction_2.bin", m_siteDirection2,        0, m_nSites*3 );
        loadData( directory +                "site.normal.bin", m_siteNormal,            0, m_nSites*3 );

        loadData( directory +    "site.guassian_curvature.bin", m_siteGuassianCurvature, 0, m_nSites   );
        loadData( directory +        "site.mean_curvature.bin", m_siteMeanCurvature,     0, m_nSites   );

        /////////////////////////////////////////////////////////////////////////////////////////////////

        

        /////////////////////////////////////////////////////////////////////////////////////////////////

        // normalizing this variable for experiment

        float max_abs_guassian_curvature = 0.f;
        for( const auto & c : m_siteGuassianCurvature )
        {
            max_abs_guassian_curvature = std::max( std::abs( c ), max_abs_guassian_curvature );
        }
        for( auto & c : m_siteGuassianCurvature )
        {
            c /= max_abs_guassian_curvature;
        }

        float max_abs_mean_curvature = 0.f;
        for( const auto & c : m_siteMeanCurvature )
        {
            max_abs_mean_curvature = std::max( std::abs( c ), max_abs_mean_curvature );
        }
        for( auto & c : m_siteMeanCurvature )
        {
            c /= max_abs_mean_curvature;
        }

        loadData( directory + "siteLayers.bin"       , m_siteLayers       , 0, m_nSites );
        loadData( directory + "siteComponentIds.bin" , m_siteComponentIds , 0, m_nSites );

        loadData( directory + "voxelSiteIds.bin"     , m_voxelSiteIds     , 0, M_NV );
        loadData( directory + "voxelLayerIds.bin"    , m_voxelLayerIds    , 0, M_NV );
        loadData( directory + "voxelComponentIds.bin", m_voxelComponentIds, 0, M_NV );

        m_siteLayerIndices.resize( NL );
        for( int i = 0; i < m_nSites; ++i )
        {
            int id = m_siteLayers[ i ] - 1;
            
            // shouldn't happen
            if( id < 0 || id >= NL ) {
                std::cerr << "Error: Tessellation.hpp, site layer id is invalid" << std::endl;
            }
            else {
                m_siteLayerIndices[ id ].push_back( i ); 
            }
        }

        m_sitePointsGrouped.resize(            NL ); 
        m_siteDirection1Grouped.resize(        NL );
        m_siteDirection2Grouped.resize(        NL );  
        m_siteNormalGrouped.resize(            NL );  
        m_siteGuassianCurvatureGrouped.resize( NL );  
        m_siteMeanCurvatureGrouped.resize(     NL ); 

        for (int i = 0; i < NL; ++i) {

            const size_t N_SITES_GROUP = m_siteLayerIndices[i].size();
            
            m_sitePointsGrouped[            i ].resize( N_SITES_GROUP * 3);  
            m_siteDirection1Grouped[        i ].resize( N_SITES_GROUP * 3); 
            m_siteDirection2Grouped[        i ].resize( N_SITES_GROUP * 3);   
            m_siteNormalGrouped[            i ].resize( N_SITES_GROUP * 3);   
            m_siteGuassianCurvatureGrouped[ i ].resize( N_SITES_GROUP);   
            m_siteMeanCurvatureGrouped[     i ].resize( N_SITES_GROUP);  

            for (int j = 0; j < N_SITES_GROUP; ++j) {

                const int idx = m_siteLayerIndices[ i ][ j ] * 3;

                m_sitePointsGrouped[     i ][ j*3     ] = m_sitePoints[     idx     ];
                m_sitePointsGrouped[     i ][ j*3 + 1 ] = m_sitePoints[     idx + 1 ];
                m_sitePointsGrouped[     i ][ j*3 + 2 ] = m_sitePoints[     idx + 2 ];

                m_siteDirection1Grouped[ i ][ j*3     ] = m_siteDirection1[ idx     ];
                m_siteDirection1Grouped[ i ][ j*3 + 1 ] = m_siteDirection1[ idx + 1 ];
                m_siteDirection1Grouped[ i ][ j*3 + 2 ] = m_siteDirection1[ idx + 2 ];

                m_siteDirection2Grouped[ i ][ j*3     ] = m_siteDirection2[ idx     ];
                m_siteDirection2Grouped[ i ][ j*3 + 1 ] = m_siteDirection2[ idx + 1 ];
                m_siteDirection2Grouped[ i ][ j*3 + 2 ] = m_siteDirection2[ idx + 2 ];

                m_siteNormalGrouped[     i ][ j*3     ] = m_siteNormal[     idx     ];
                m_siteNormalGrouped[     i ][ j*3 + 1 ] = m_siteNormal[     idx + 1 ];
                m_siteNormalGrouped[     i ][ j*3 + 2 ] = m_siteNormal[     idx + 2 ];

                m_siteGuassianCurvatureGrouped[ i ][ j ] = m_siteGuassianCurvature[ m_siteLayerIndices[i][j] ];
                m_siteMeanCurvatureGrouped[     i ][ j ] = m_siteMeanCurvature[     m_siteLayerIndices[i][j] ];
            }
        }

        if( false ) {
            loadData( directory + "m_siteGeodesicDistanceMatrix.bin",     m_siteGeodesicDistanceMatrix, 0, m_nSites*m_nSites  );
            loadData( directory + "m_siteEuclideanDistanceMatrix.bin",   m_siteEuclideanDistanceMatrix, 0, m_nSites*m_nSites  );
        }

        // layers needs to be converted to guarantee contiguous form first
        std::vector< float > ly;
        loadData( directory + "layers.bin", ly, 0, NL*2 );
        m_layers.resize( NL );
        for( size_t i = 0; i < NL; ++i ) {
            m_layers[ i ][ 0 ] = ly[ i*2     ];
            m_layers[ i ][ 1 ] = ly[ i*2 + 1 ];
        }
    }

    int64_t M_NV;
    std::array< int64_t, 3  > M_DIMS;

    std::vector< int16_t >                m_siteLayers;                  
    std::vector< float >                  m_sitePoints;

    std::vector< float > m_siteDirection1; 
    std::vector< float > m_siteDirection2; 
    std::vector< float > m_siteNormal; 
    std::vector< float > m_siteGuassianCurvature; 
    std::vector< float > m_siteMeanCurvature; 

    std::vector< std::vector< float > > m_sitePointsGrouped; 
    std::vector< std::vector< float > > m_siteDirection1Grouped; 
    std::vector< std::vector< float > > m_siteDirection2Grouped; 
    std::vector< std::vector< float > > m_siteNormalGrouped; 
    std::vector< std::vector< float > > m_siteGuassianCurvatureGrouped; 
    std::vector< std::vector< float > > m_siteMeanCurvatureGrouped; 

    std::vector< int32_t >                m_siteComponentIds;

    std::vector< std::vector< std::uint32_t > > m_siteLayerIndices;

    std::vector< float >                  m_siteGeodesicDistanceMatrix;  
    std::vector< float >                  m_siteEuclideanDistanceMatrix; 
    
    std::vector< int32_t >                m_voxelSiteIds;                
    std::vector< int32_t >                m_voxelComponentIds;           
    std::vector< int16_t >                m_voxelLayerIds;               
    
    int64_t                               m_nSites;                      
    int64_t                               m_NC;                          
    int64_t                               m_numLayers;  
    
    bool                                  m_hasBackground;

    std::vector< std::array< float, 2 > > m_layers;  

    void clear()
    {
        m_siteLayers = {}; 
        m_sitePoints = {};                
        m_siteComponentIds = {};
        m_siteGeodesicDistanceMatrix = {};
        m_siteEuclideanDistanceMatrix = {};
        m_voxelSiteIds = {};
        m_voxelComponentIds = {};
        m_voxelLayerIds = {};
        m_layers = {};
    }

    Tessellation() 
    {
        m_nSites=0;                      
        m_NC=0;
        m_hasBackground=false; 
    }

    ~Tessellation() {}

    std::vector< int16_t > & getSiteLayers()    { return m_siteLayers; }
    std::vector< float >   & getSitePositions() { return m_sitePoints; } 

    std::vector< float >   & getSiteDirection1()        { return m_siteDirection1;        } 
    std::vector< float >   & getSiteDirection2()        { return m_siteDirection2;        } 
    std::vector< float >   & getSiteNormal()            { return m_siteNormal;            } 
    std::vector< float >   & getSiteGuassianCurvature() { return m_siteGuassianCurvature; } 
    std::vector< float >   & getSiteMeanCurvature()     { return m_siteMeanCurvature;     } 

    std::vector< std::vector< float > > & getSitePointsGrouped()            { return m_sitePointsGrouped;            } 
    std::vector< std::vector< float > > & getSiteDirection1Grouped()        { return m_siteDirection1Grouped;        } 
    std::vector< std::vector< float > > & getSiteDirection2Grouped()        { return m_siteDirection2Grouped;        } 
    std::vector< std::vector< float > > & getSiteNormalGrouped()            { return m_siteNormalGrouped;            } 
    std::vector< std::vector< float > > & getSiteGuassianCurvatureGrouped() { return m_siteGuassianCurvatureGrouped; } 
    std::vector< std::vector< float > > & getSiteMeanCurvatureGrouped()     { return m_siteMeanCurvatureGrouped;     }

    std::vector< int32_t > & getSiteComponentIds()            { return m_siteComponentIds;            }
    std::vector< float >   & getSiteGeodesicDistanceMatrix()  { return m_siteGeodesicDistanceMatrix;  }
    std::vector< float >   & getSiteEuclideanDistanceMatrix() { return m_siteEuclideanDistanceMatrix; }
    std::vector< int32_t > & getClassification()              { return m_voxelSiteIds;                }
    std::vector< int32_t > & getVoxelComponentIds()           { return m_voxelComponentIds;           }
    std::vector< int16_t > & getVoxelLayerIds()               { return m_voxelLayerIds;               }

    std::vector< std::vector< uint32_t > > & getSiteLayerIndices() { return m_siteLayerIndices; }
    int64_t                  getNSites()                      const { return m_nSites;          }
    int64_t                  getNComponents()                 const { return m_NC;              }
    int64_t                  getNLayers()                     const { return m_layers.size();   }    
    const std::vector< std::array< float, 2 > > & getLayers() const { return m_layers;          }        
    bool                     hasBackground()                  const { return m_hasBackground;   }
};

} // end namespace Marrus

#endif