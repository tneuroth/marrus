import numpy as np
import matplotlib.pyplot as plt
import numpy as np
from sklearn.preprocessing import MinMaxScaler
#from tsnecuda import TSNE
from sklearn.manifold  import TSNE
import sys
projDir = sys.argv[ 1 ]

print( "using " + projDir )

hists = np.fromfile( projDir +"/histograms.0.bin", dtype=np.float32 )
dims_counts = np.loadtxt( projDir +"/dims_counts.0.txt" )
dims = ( int( dims_counts[ 0 ] ), int( dims_counts[ 1 ] ) )
counts = int( dims_counts[ 2 ] )
NB = dims[ 0 ] * dims[ 1 ]

X = MinMaxScaler().fit_transform( hists.reshape( counts, NB ) )
# X_embedded = TSNE( n_components=2, perplexity=30 ).fit_transform( X )

X_embedded = TSNE(n_components=2, perplexity=50, learning_rate=10).fit_transform(X)

min = np.min( X_embedded )
max = np.max( X_embedded )
X_embedded = ( X_embedded - min ) / ( max - min )
X = np.vstack( (  X_embedded[ : , 0 ], X_embedded[ : , 1 ] ) )
X.tofile( projDir +"/projection.0.bin" )

plt.scatter( X_embedded[ : , 0 ], X_embedded[ :, 1 ] )
plt.show();
