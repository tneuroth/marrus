import numpy as np
import matplotlib.pyplot as plt
# import numpy as np
from sklearn.preprocessing import MinMaxScaler
#from tsnecuda import TSNE
import tsnecuda

#from sklearn.manifold  import TSNE
# import sys

projDir = "../projects/TestData/subprojects/subproject1/precomputed_plots/"

hists = np.fromfile( projDir +"/histograms.0.bin", dtype=np.float32 )
dims_counts = np.loadtxt( projDir +"/dims_counts.0.txt" )

print( hists )
print( dims_counts )

dims = ( int( dims_counts[ 0 ] ), int( dims_counts[ 1 ] ) )
counts = int( dims_counts[ 2 ] )
NB = dims[ 0 ] * dims[ 1 ]

X = hists.reshape( counts, NB )

print( X )

X = MinMaxScaler().fit_transform( X )

print( X )

X_embedded = tsnecuda.TSNE( n_components=2, perplexity=50, learning_rate=10 ).fit_transform( X )

print( X_embedded )

min = np.min( X_embedded )
max = np.max( X_embedded )
X_embedded = ( X_embedded - min ) / ( max - min )

X = np.vstack( (  X_embedded[ : , 0 ], X_embedded[ : , 1 ] ) )

# X.tofile( projDir +"/projection.0.bin" )

plt.scatter( X_embedded[ : , 0 ], X_embedded[ :, 1 ] )
plt.show();
