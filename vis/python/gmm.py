import numpy as np
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
from sklearn import mixture
import scipy.io as sio
import gzip
import sys

width  = int( float( sys.argv[ 1 ] ) )
height = int( float( sys.argv[ 2 ] ) )

xmin  = float( sys.argv[ 3 ] )
xmax  = float( sys.argv[ 4 ] )
ymin  = float( sys.argv[ 5 ] )
ymax  = float( sys.argv[ 6 ] )

x = np.fromfile( "gmm.x.dat", dtype='f4' )
y = np.fromfile( "gmm.y.dat", dtype='f4' )

clf = mixture.GaussianMixture( n_components=5, covariance_type='diag', max_iter=200, tol=1e-9 ).fit( np.array( [ x, y ] ).T )

print( clf.get_params() )
print( clf.means_ )
print( clf.covariances_ )
print( clf.weights_ )

X, Y = np.meshgrid( np.linspace( xmin, xmax, width ), np.linspace( ymin, ymax, height ) )
Z = np.exp( clf.score_samples( np.array([X.ravel(), Y.ravel()]).T ) )

#Z = np.flip( Z.reshape(X.shape), axis=0 )

# CS = plt.contour( X, Y, Z, norm=LogNorm(vmin=1.0, vmax=1000.0), levels=np.logspace( 0, 3, 10 ) )
# CB = plt.colorbar( CS, shrink=0.8, extend='both')

# plt.imshow( Z, cmap='GnBu' )
# plt.title('Negative log-likelihood predicted by a GMM')
# plt.axis('tight')
# plt.show()

Z = Z.ravel().astype( 'f4' )
Z.tofile( "gmm.dat" )

# plt.scatter( x, y )
# plt.show()