
import numpy as np
import matplotlib.pyplot as plt
import numpy as np
from sklearn.manifold  import TSNE
#from tsnecuda import TSNE
#import sys

projDir = sys.argv[ 1 ]

print( "using " + projDir )

distances = np.fromfile( projDir + "/distances.2.bin", dtype=np.float32 )
dims_counts = np.loadtxt( projDir + "/dims_counts.2.txt" )
dims = ( int( dims_counts[ 0 ] ), int( dims_counts[ 1 ] ) )

distances = distances.reshape( dims )
X_embedded = TSNE( n_components=2, perplexity=30, metric="precomputed" ).fit_transform( distances )

min = np.min( X_embedded )
max = np.max( X_embedded )
if( max - min ) > 0:
	X_embedded = ( X_embedded - min ) / ( max - min )
X = np.vstack( (  X_embedded[ : , 0 ], X_embedded[ : , 1 ] ) )
X.tofile( projDir + "/projection.2.bin" )

plt.scatter( X_embedded[ : , 0 ], X_embedded[ :, 1 ] )
plt.show();

# X = np.vstack( np.random.rand( dims[ 1 ]  ), np.random.( dims[ 1 ]  ) )
# X.tofile( "./projection.2.bin" )

