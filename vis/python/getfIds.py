
import numpy as np

def flat( x, y, z, X, Y, Z ):
	return x + y*X + z*Y*X

def td( idx, X, Y, Z ):
    tmp = idx
    x = tmp % X
    tmp = ( tmp - x ) // X
    y = tmp % Y
    z  = ( tmp - y ) // Y
    return  [ x, y, z ]

XF = 1728
YF = 640
ZF = 1280
cmpIds = np.fromfile( "/home/v1/VIDI1_MNT/combustion/turb_shear/10atm_connected_components/componentMap.1728.640.1280.6.0000E-04.dat", dtype="int32" )
N = cmpIds.size
fid = 0
mx = np.max( cmpIds )
cids = [ 328, 11, 1892 ]

for cid in range( 3 ):
	masked = np.zeros( N, dtype="f4" )
	masked[ np.where( cmpIds == cids[ cid ] ) ] = 1.0

	# for i in range( N ) :
	# 	if cmpIds[ i ] == cids[ cid ]:
	# 		masked[ i ] = 1.0
	print( "writing masked " + str( cid ) )
	masked.tofile( "masked." + str(cid) + ".dat" )

# for fp in [ 
# 	"c.componentVoxelIDs.n131698.d300.400.426.f1.bin", \
# 	"c.componentVoxelIDs.n364403.d300.400.426.f2.bin", \
# 	"c.componentVoxelIDs.n55216.d300.400.426.f3.bin" ] :
# 	minZ = 99999999999
# 	maxZ = 0 
# 	# masked = np.zeros( N, dtype="f4" )

# 	hist = np.zeros( mx )

# 	d = np.fromfile( fp, dtype="int64"  )
# 	L = d.size
# 	for i in range( L ):
# 		idx = d[ i ]
# 		i3 = td( idx, XF, YF, ZF )
# 		idF = flat( i3[ 0 ], i3[ 1 ], i3[ 2 ], XF, YF, ZF )
# 		maxZ = max( maxZ, i3[ 2 ] )
# 		minZ = min( minZ, i3[ 2 ] )
# 		hist[ cmpIds[ idx ] ] += 1.0

# 	for i in range( mx ):
# 		if hist[ i ] > 0 :
# 			print( str( i ) + "," + str( hist[ i ] ) )

# 	input( "..." )

		# masked[ d[ i ] ] = 1.0			


	
	# print( minZ )
	# print( maxZ )

	# masked.tofile( "masked." + str(fid) + ".dat" ) 
	# fid += 1

