import numpy as np
import matplotlib.pyplot as plt
import numpy as np
#from sklearn.manifold  import TSNE
from tsnecuda import TSNE
import sys

projDir = sys.argv[ 1 ]

print( "using " + projDir )

hists = np.fromfile( projDir + "/histograms.bin", dtype=np.float32 )
dims_counts = np.loadtxt( projDir + "/dims_counts.txt" )
dims = ( int( dims_counts[ 0 ] ), int( dims_counts[ 1 ] ) )
counts = int( dims_counts[ 2 ] )

NB = dims[ 0 ] * dims[ 1 ]
distances = np.zeros( ( counts, counts ) )
for i in range( counts ):
	hist1 = np.reshape( hists[ i*NB : (i+1)*NB ], ( 17, 17 ) )
	for j in range( counts ):
		hist2 = np.reshape( hists[ j*NB : (j+1)*NB ], ( 17, 17 ) )
		distances[ i, j ] = np.sum( np.sqrt( ( hist1 - hist2 )**2 ) )

# X_embedded = TSNE( n_components=2, perplexity=30 ).fit_transform( distances )
X_embedded = TSNE(n_components=2, perplexity=50, learning_rate=10).fit_transform(X)

min = np.min( X_embedded )
max = np.max( X_embedded )
X_embedded = ( X_embedded - min ) / ( max - min )
X = np.vstack( (  X_embedded[ : , 0 ], X_embedded[ : , 1 ] ) )
X.tofile( projDir + "/projection.0.bin" )

plt.scatter( X_embedded[ : , 0 ], X_embedded[ :, 1 ] )
plt.show();

# X = np.vstack( np.random.rand( counts ), np.random.( counts ) )
# X.tofile( "./projection.0.bin" )