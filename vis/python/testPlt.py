import numpy as np
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
from sklearn import mixture
import scipy.io as sio
import gzip

x = np.fromfile( "test.x.bin", dtype='f4' )
y = np.fromfile( "test.y.bin", dtype='f4' )

clf = mixture.GaussianMixture( n_components=5, covariance_type='diag', max_iter=100, tol=1e-9 ).fit( np.array( [ x, y ] ).T )
print( clf.get_params() )

print( clf.means_ )
print( clf.covariances_ )
print( clf.weights_ )

xmin = np.min( x )
ymin = np.min( y )
xmax = np.max( x )
ymax = np.max( y )

X, Y = np.meshgrid( np.linspace( xmin, xmax, 512 ), np.linspace( ymin, ymax, 512 ) )
Z = np.exp( clf.score_samples( np.array([X.ravel(), Y.ravel()]).T ) )
Z = np.flip( Z.reshape(X.shape), axis=0 )

# CS = plt.contour( X, Y, Z, norm=LogNorm(vmin=1.0, vmax=1000.0), levels=np.logspace( 0, 3, 10 ) )
# CB = plt.colorbar( CS, shrink=0.8, extend='both')

plt.imshow( Z, cmap='GnBu' )
plt.title('Negative log-likelihood predicted by a GMM')
plt.axis('tight')
plt.show()

H, yedges, xedges = np.histogram2d( y, x, bins=10 )
H = np.flip( H, axis=0 )
plt.imshow( H, cmap='GnBu' )
plt.show()

print( np.shape( H ) )

# H.tofile( "h.bin" )

# plt.scatter( x, y )
# plt.show()