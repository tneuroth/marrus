
import numpy as np

def flat( x, y, z, X, Y, Z ):
	return x + y*X + z*Y*X

def td( idx, X, Y, Z ):
    tmp = idx
    x = tmp % X
    tmp = ( tmp - x ) // X
    y = tmp % Y
    z  = ( tmp - y ) // Y
    return  [ x, y, z ]

XS = 300
YS = 400
ZS = 426

XF = 1728
YF = 640
ZF = 1280

for fp in [ 
	"componentVoxelIDs.n131698.d300.400.426.f1.bin", \
	"componentVoxelIDs.n364403.d300.400.426.f2.bin", \
	"componentVoxelIDs.n55216.d300.400.426.f3.bin" ] :

	d = np.fromfile( fp, dtype="int64"  )
	conv = np.zeros( d.size, dtype="int64" )

	for i in range( d.size ):
		i3 = td( d[ i ], XS, YS, ZS )
		conv[ i ] = flat( i3[ 0 ], i3[ 1 ], i3[ 2 ], XF, YF, ZF )      

	conv.tofile( "c." + fp )

