#!/bin/bash

#####################################################
# gets the path that this script is in

customrealpath() {
    [[ $1 = /* ]] && echo "$1" || echo "$PWD/${1#./}"
}
SCRIPT=`customrealpath $0`
THIS_PATH=`dirname $SCRIPT`
BASE_PATH=`realpath $THIS_PATH/../../`

#####################################################

export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$BASE_PATH/../lsrcvt/lib

LATEX_DIR=$(dirname $(which pdftocairo))
CAIRO_DIR=$(dirname $(which pdftocairo))
CONVERT_DIR=$(dirname $(which convert))

stat $CONVERT_DIR/convert
stat $CAIRO_DIR/pdftocairo
stat $LATEX_DIR/pdflatex

#valgrind \
#	--leak-check=full \
#	--show-leak-kinds=all \
#	--track-origins=yes \
#	--verbose \
#	--log-file=valgrind-out.txt \

# CONDA_ENV = conda info | grep -i 'base environment'
# source ~/anaconda3/bin/activate
# conda activate cuda

$BASE_PATH/bin/application \
	$BASE_PATH \
	$LATEX_DIR \
	$CAIRO_DIR \
	$CONVERT_DIR
