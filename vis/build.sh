#!/bin/bash

#################################################################################################
# gets the path that this script is in

customrealpath() {
    [[ $1 = /* ]] && echo "$1" || echo "$PWD/${1#./}"
}

SCRIPT=`customrealpath $0`
THIS_PATH=`dirname $SCRIPT`
BASE_PATH=$THIS_PATH

g++ -std=c++17 -O3 -fopenmp -frounding-math -w \
    -o $BASE_PATH/bin/application \
    $BASE_PATH/../common/algorithms/Standard/MyAlgorithms.cpp \
    $BASE_PATH/../common/algorithms/IsoContour/CubeMarcher.cpp \
    $BASE_PATH/../thirdParty/glad/src/glad.c \
    $BASE_PATH/../common/render/Texture/Texture.cpp \
    $BASE_PATH/../common/render/RenderModule3D/RenderModule.cpp \
    $BASE_PATH/../common/render/RenderModule3D/RenderGlyphs.cpp \
    $BASE_PATH/../common/geometry/SurfaceGenerator.cpp \
    $BASE_PATH/../common/views/SpatialView3D.cpp \
    $BASE_PATH/src/main.cpp \
    -I$BASE_PATH/src/ \
    -I$BASE_PATH/../common/ \
    -I$BASE_PATH/../summarize/ \
    -I$BASE_PATH/../processing/include/ \
    -I$BASE_PATH/../tessellation/include/ \
    -I$BASE_PATH/../thirdParty/ \
    -I$BASE_PATH/../thirdParty/glad/include/ \
    -I/usr/include/opencv4/ \
    -I/usr/include/freetype2/ \
    -L/usr/local/lib64/ \
    -L/usr/local/lib/ \
    -L/opt/cuda/lib64/ \
    -L$BASE_PATH/../tessellation/lib/ \
    -L$BASE_PATH/../processing/lib/ \
    -lopencv_core \
    -lopencv_imgproc \
    -lopencv_imgcodecs \
    -lglfw \
    -lGL \
    -lpthread \
    -lX11 \
    -lXrandr \
    -lXi \
    -ldl \
    -lboost_system \
    -lboost_filesystem \
    -lfreetype \
    -llsrcvt \
    -lfftw3 \
    -lmexprs

