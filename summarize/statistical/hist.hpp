#ifndef TN_MARRUS_HIST_HPP
#define TN_MARRUS_HIST_HPP

#include <cmath>
#include <vector>
#include <algorithm>

#include "algorithms/Standard/MyAlgorithms.hpp"
#include "utils/IndexUtils.hpp"

namespace Marrus {

template< typename T1, typename T2 >
inline
void computePaddedHistogram2D(
    const int PADDED_WIDTH,
    const int PADDED_HEIGHT,
    const int PAD_X,
    const int PAD_Y,
    const std::vector< T1 >& x, 
    const std::vector< T1 >& y, 
    const double x0, 
    const double y0,
    const double xW,
    const double yW,
    std::vector< T2 > & hist )
{
    hist.resize( PADDED_WIDTH*PADDED_HEIGHT );
    std::fill( hist.begin(), hist.end(), static_cast< T2 >( 0.0 ) );

    const int HIST_WIDTH  =  PADDED_WIDTH - PAD_X*2;
    const int HIST_HEIGHT = PADDED_HEIGHT - PAD_Y*2;

    const size_t NP = x.size();
    for ( size_t pIdx = 0; pIdx < NP; ++pIdx ) {

        const double px = static_cast< double >( x[ pIdx ] );
        const double py = static_cast< double >( y[ pIdx ] );

        const int bX = static_cast<int>( std::floor( ( ( px - x0 ) / xW ) * HIST_WIDTH  ) );
        const int bY = static_cast<int>( std::floor( ( ( py - y0 ) / yW ) * HIST_HEIGHT ) );

        if( bX >= 0 && bX < HIST_WIDTH 
         && bY >= 0 && bY < HIST_HEIGHT )
        {
        	const int IX = bX;
        	const int IY = bY; 	

	        const size_t IDX = IY * PADDED_WIDTH + IX; 
	        hist[ IDX ] += static_cast< T2 >( 1.0 );
        } 
    }
}

template< typename T1, typename T2 >
inline
void computeHistogram2D(
    const int width,
    const int height,
    const std::vector< T1 >& x, 
    const std::vector< T1 >& y, 
    const double x0, 
    const double y0,
    const double xW,
    const double yW,
    std::vector< T2 > & hist )
{
    hist.resize( width*height );
    std::fill( hist.begin(), hist.end(), static_cast< T2 >( 0.0 ) );

    const size_t NP = x.size();
    for ( size_t pIdx = 0; pIdx < NP; ++pIdx ) {

        const double px = static_cast< double >( x[ pIdx ] );
        const double py = static_cast< double >( y[ pIdx ] );

        const int bX = static_cast<int>( std::floor( ( ( px - x0 ) / xW ) * width  ) );
        const int bY = static_cast<int>( std::floor( ( ( py - y0 ) / yW ) * height ) );

        if( bX >= 0 && bX < width 
         && bY >= 0 && bY < height )
        {
	        const size_t IDX = bY * width + bX; 
	        hist[ IDX ] += static_cast< T2 >( 1.0 );
        } 
    }
}

}
	
#endif