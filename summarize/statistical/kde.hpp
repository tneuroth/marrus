#ifndef MARRUS_STATS_KDE_LIB_HPP
#define MARRUS_STATS_KDE_LIB_HPP

#include "hist.hpp"
#include "utils/IndexUtils.hpp"

#include <iostream>
#include <vector>
#include <algorithm>
#include <cmath>
#include <fftw3.h>

namespace Marrus {

void fftshift(
    std::vector<double>& data, 
    int W, 
    int H )
{
    std::vector<double> temp(data);

    int half_W = W / 2;
    int half_H = H / 2;

    for( int i = 0; i < W; ++i )
    {
        for( int j = 0; j < H; ++j )
        {
            int x = (i + half_W) % W;
            int y = (j + half_H) % H;

            data[i * H + j] = temp[x * H + y];
        }
    }
}

void createGaussianKernel(
    std::vector< double > & kernel, 
    int W, 
    int H, 
    double bandwidth )
{
    const auto & IDX = TN::flatIndex2dCM;

    const int half_W = W / 2;
    const int half_H = H / 2;

    const double norm = 1.0 / ( 2.0 * M_PI * bandwidth * bandwidth );
    const double inv_2sigma2 =  1.0 / ( 2.0 * bandwidth * bandwidth );

    for( int i = 0; i < W; ++i )
    {
        #pragma omp parallel for simd
        for( int j = 0; j < H; ++j )
        {
            const int x = i - half_W;
            const int y = j - half_H;
            kernel[ IDX( i, j, W, H ) ] = norm * exp( -( x * x + y * y ) * inv_2sigma2 );
        }
    }
}

template< typename T1, typename T2 >
inline
void computeKDE(
    const int width,
    const int height,
    const double bandwidth, 
    const std::vector< T1 >& x, 
    const std::vector< T1 >& y, 
    const double x0, 
    const double y0,
    const double xW,
    const double yW,
    std::vector< T2 > & image )
{
    int PX = width  / 2;
    int PY = height / 2;

    const int W = width  + PX*2;
    const int H = height + PY*2;

    const auto & IDX = TN::flatIndex2dCM;

    std::vector< double > hist  ( W * H, 0.0 );
    std::vector< double > kernel( W * H, 0.0 );

    // Create the histogram

    Marrus::computePaddedHistogram2D( W, H, PX, PY, x, y, x0, y0, xW, yW, hist ); 

    createGaussianKernel( kernel, W, H, bandwidth );

    // Allocate memory for FFTW

    fftw_complex* hist_in = 
        (fftw_complex*) fftw_malloc( sizeof( fftw_complex ) * W * H );
    
    fftw_complex* hist_out = 
        (fftw_complex*) fftw_malloc( sizeof( fftw_complex ) * W * H );

    fftw_complex* kernel_in = 
        (fftw_complex*) fftw_malloc( sizeof( fftw_complex ) * W * H );
    
    fftw_complex* kernel_out = 
        (fftw_complex*) fftw_malloc( sizeof( fftw_complex ) * W * H );

    // Copy data to FFTW arrays

    for( int i = 0; i < W; ++i )
    {
        #pragma omp parallel for simd
        for( int j = 0; j < H; ++j )
        {
            hist_in[   IDX( i, j, W, H ) ][ 0 ] = hist[ IDX( i, j, W, H ) ];
            hist_in[   IDX( i, j, W, H ) ][ 1 ] = 0.0;
            kernel_in[ IDX( i, j, W, H ) ][ 0 ] = kernel[ IDX( i, j, W, H ) ];
            kernel_in[ IDX( i, j, W, H ) ][ 1 ] = 0.0;
        }
    }

    // Create FFTW plans

    fftw_plan plan_hist = 
        fftw_plan_dft_2d( W, H, hist_in, hist_out, FFTW_FORWARD, FFTW_ESTIMATE );

    fftw_plan plan_kernel = 
        fftw_plan_dft_2d( W, H, kernel_in, kernel_out, FFTW_FORWARD, FFTW_ESTIMATE );

    fftw_plan plan_kde = 
        fftw_plan_dft_2d( W, H, kernel_out, kernel_out, FFTW_BACKWARD, FFTW_ESTIMATE );

    // Execute FFTW plans

    fftw_execute( plan_hist );
    fftw_execute( plan_kernel );

    // Multiply in frequency domain

    for( int i = 0; i < W; ++i )
    {
        #pragma omp parallel for simd
        for( int j = 0; j < H; ++j )
        {
            std::complex<double> hist_val(
                hist_out[ IDX( i, j, W, H ) ][ 0 ], 
                hist_out[ IDX( i, j, W, H ) ][ 1 ] );

            std::complex<double> kernel_val(
                kernel_out[ IDX( i, j, W, H ) ][ 0 ], 
                kernel_out[ IDX( i, j, W, H ) ][ 1 ] );

            std::complex<double> kde_val = hist_val * kernel_val;

            kernel_out[ IDX( i, j, W, H ) ][ 0 ] = kde_val.real();
            kernel_out[ IDX( i, j, W, H ) ][ 1 ] = kde_val.imag();
        }
    }

    // Execute inverse FFT

    fftw_execute( plan_kde );

    // Copy result back to image array

    image.resize( width*height );

    for( int i = 0; i < width; ++i )
    {
        #pragma omp parallel for simd
        for( int j = 0; j < height; ++j )
        {
            int iSRC = i + ( W  / 2 - 1 ) % W;
            int jSRC = j + ( H  / 2 - 1 ) % H;

            int iDST = i;
            int jDST = j;

            image[ IDX( iDST, jDST, width, height ) ] = kernel_out[ IDX( iSRC, jSRC, W, H ) ][ 0 ] / ( W * H ); 
        }
    }

    // Clean up FFTW resources

    fftw_destroy_plan( plan_hist );
    fftw_destroy_plan( plan_kernel );
    fftw_destroy_plan( plan_kde );
    fftw_free( hist_in );
    fftw_free( hist_out );
    fftw_free( kernel_in );
    fftw_free( kernel_out );
}

}

#endif