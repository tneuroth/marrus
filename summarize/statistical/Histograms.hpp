#ifndef TN_MARRUS_HISTOGRAMS_HPP
#define TN_MARRUS_HISTOGRAMS_HPP

#include "geometry/Vec.hpp"
#include "dhff_dbs.hpp"

#include <cstdint>
#include <algorithm>
#include <cmath>
#include <vector>

namespace Marrus {

template< typename T, typename T2  >
inline
void generateHistograms( 
	const std::vector< CCRecord > & ccRec,
	const std::vector< T >        & sortedData,
	const TN::Vec2< double >      & histExtent,
	const int nBins,
	std::vector< T2 > & histBins ) // assumes bins are allocated and set to 0
{
	const double WIDTH = histExtent.b() - histExtent.a();
	const size_t END = ccRec.size();

	#pragma omp parallel for
	for( size_t cc = 0; cc < END; ++cc ) {

		const size_t C_START = ccRec[ cc ].i0;
		const size_t C_END   = cc == END - 1 ? END : ccRec[ cc + 1 ].i0;

		for( size_t i = C_START; i < C_END; ++i ) {
			int bIdx = std::max( 0, 
				std::min( 
					static_cast<int>( std::floor( ( ( sortedData[ i ] - histExtent.a() ) / WIDTH ) * nBins ) ), 
					nBins - 1 ) );

			histBins[ cc * nBins + bIdx ] += static_cast< T2 >( 1 );
		}
	}
}

}
	
#endif