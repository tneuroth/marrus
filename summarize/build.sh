#!/bin/bash

#####################################################
# gets the path that this script is in

customrealpath() {
    [[ $1 = /* ]] && echo "$1" || echo "$PWD/${1#./}"
}
SCRIPT=`customrealpath $0`
THIS_PATH=`dirname $SCRIPT`

#####################################################

echo Building DHFF Database ...
$THIS_PATH/dhff/build.sh

echo Building Statistical Summarization Module ...
$THIS_PATH/statistical/build.sh

echo Building Geometric Summarization Module ...
$THIS_PATH/geometrical/build.sh