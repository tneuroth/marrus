#!/bin/bash

#################################################################################################
# gets the path that this script is in

customrealpath() {
    [[ $1 = /* ]] && echo "$1" || echo "$PWD/${1#./}"
}
SCRIPT=`customrealpath $0`
THIS_PATH=`dirname $SCRIPT`
BASE_PATH=$THIS_PATH

g++ -std=c++17 -O3 -fopenmp -frounding-math -w -fPIC \
    -I$BASE_PATH/ \
    -I$BASE_PATH/../../common/ \
    -o $BASE_PATH/../lib/libdhff.so --shared \
    $BASE_PATH/dhff_lib.cpp

