
#include "dhff_dbs.hpp"
#include "statistical/Histograms.hpp"
#include <utils/io.hpp>
#include "algorithms/Standard/MyAlgorithms.hpp"

#include <string>
#include <vector>
#include <algorithm>
#include <array>
#include <iostream>
#include <cstdint>
#include <unordered_set>
#include <chrono>
#include <fstream>

using namespace std;
using namespace std::chrono;

using namespace dhffdbs;

int main() {

    const string IN_DIR    = "/home/ts/hd_mnt/combustion/turb_shear/dhffdbs_10atm/";
	const string OUT_DIR   = "/home/ts/hd_mnt/combustion/turb_shear/summary_10atm/";

	std::vector< float >    data;
	std::vector< CCRecord > ccRecords;

	std::vector< float > histData;
	const int32_t nBins = 17;
	TN::Vec2< double > histExtent = { 0.0, 0.00534456 };

    const double dt = 0.000002;
    double current = 0.0002;

	while( current <= 0.0006 + 0.0000001 ) {
		
		string ts = toStringWFMT( current );

		// load OH

	    std::ifstream inFile( IN_DIR + "OH." + ts + ".dat", std::ios::in | std::ios::binary );
	    uint64_t NV;
	    inFile.read( ( char * ) & NV, sizeof( uint64_t ) );
	    data.resize( NV );
	    inFile.read( ( char * ) data.data(), sizeof( float ) * NV );
	    inFile.close();

		// load records

	    inFile.open( IN_DIR + "dhffdbs.cc." + ts + ".dat" );
	    uint64_t NC;
	    inFile.read( ( char * ) & NC, sizeof( uint64_t ) );
	    ccRecords.resize( NC );
	    inFile.read( ( char * ) ccRecords.data(), sizeof( CCRecord ) * NC );
	    inFile.close();

		// compute hists

	    std::cout << "ts=" << ts <<  ", NC = " << NC << endl;

		histData.resize( nBins * NC );
		std::fill( histData.begin(), histData.end(), 0.f );

		dhffdbs::generateHistograms( 
			ccRecords,
			data,
			histExtent,
			nBins,
			histData );

		// write hists and meta data

	    std::ofstream outFile( OUT_DIR + "hist.OH." + ts + ".dat", std::ios::out | std::ios::binary );
	    if( ! outFile.is_open() ) {
	    	cout << "file not open" << endl;
	    	exit( 1 );
	    }

	    outFile.write( (char*) & NC,   sizeof( uint64_t ) );
	    outFile.write( (char*) & nBins, sizeof(  int32_t ) );
	    outFile.write( ( char* ) histData.data(), histData.size() * sizeof( float ) );
	    outFile.close();

	    current += dt;
	}
}