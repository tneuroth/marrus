
# Building (instructions for Ubuntu)

## Core Dependencies

The following core dependencies are needed, and can be installed using the apt package manager:

build-essential 
imagemagick
texlive 
texlive-latex-extra
libboost-filesystem-dev 
libboost-graph-dev 
libopencv-dev 
libglm-dev 
libglfw3-dev 
libx11-dev 
libxrandr-dev 
libxi-dev 
libfreetype-dev  
libfftw3-dev

Command to install all of them at once:

```
sudo apt update && sudo apt install -y \
	build-essential \
	imagemagick \
	texlive \
	texlive-latex-extra \
	libboost-filesystem-dev \
	libboost-graph-dev \
	libopencv-dev \
	libglm-dev \
	libglfw3-dev \
	libx11-dev \
	libxrandr-dev \
	libxi-dev \
	libfreetype-dev \
	libfftw3-dev
```

### Cuda

You will need the cuda toolkit installed.

### Python3 with Numpy

You will need python3 with numpy, matplotlib, and pandas.

## Compile

Running the build_all.sh script will compile everything, including lsrcvt, expressions library, and 
the marrus vis system.

```$./build_all.sh```

Once lsrcvt and expressions are built, you can build Marrus vis separately for faster compilation. 

```
$cd vis
$./build.sh
```

## Test Data and Tessellation Configuration

Once downloaded, extract the test data to ```test_data/test_data_3D```.

You can view the lsrcvt configuration files in ```test_data/test_data_3D/tessellation``` and 
```test_data/test_data_3D/tessellation2``` to see how to configure lsrcvt. And you can view the 
example Marrus Vis project configuration files in ```vis/projects/example``` to see how the vis 
system can be configured.

## Generate Test Tessellation

If you have downloaded the 3D test data, and placed it in test_data/test_data_3D, then running the 
generate_test_tessellations.sh script will compute two example lsrcvt's and output the 
results to ```test_data/test_data_3D/tessellation``` and ```test_data/test_data_3D/tessellation2```.

```./generate_test_tessellations.sh```

The Marrus Vis example project is configured to load the test data and tessellation in the given 
location, so once the test tessellation is generated, you should be ready to test the vis system.

## Run Vis System

A script to run the vis system is included at the top level directory, and in the vis directory.

From the top level directory:

```./run_vis_system.sh```

Or:

```
cd vis
./run.sh
```
To load the configuration, open the project tab, then click the open button and select the 
configuration file (e.g. ```vis/projects/example_project/config.js```), then click apply.

