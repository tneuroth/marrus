#!/bin/bash

if [ ! -d "./renderings/" ] 
then
    mkdir "./renderings/"
fi

# gets the path that this script is in
customrealpath() {
	[[ $1 = /* ]] && echo "$1" || echo "$PWD/${1#./}"
}

SCRIPT=`customrealpath $0`
THIS_PATH=`dirname $SCRIPT`

filePath=$(grep "dataFile" $THIS_PATH/configuration.json | tr -d [:space:] | tr -d ',"' | cut -c 10-)

python \
	$THIS_PATH/../visualization_tools/renderTesselation2d.py  \
	1 \
	$THIS_PATH/output/voxelSiteIds.bin \
	$THIS_PATH/output/distances.bin \
	$THIS_PATH/output/srcMap.bin \
	$THIS_PATH/output/voxelComponentIds.bin \
	$THIS_PATH/output/sitePoints.bin \
	$THIS_PATH/output/voxelStatus.bin \
	$THIS_PATH/renderings \
	$filePath \
	double \
	672 672 \
	0.49 0.54 0.565 0.6 0.64 0.714

