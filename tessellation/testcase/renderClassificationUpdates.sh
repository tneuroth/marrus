#!/bin/bash

if [ ! -d "./renderings/" ] 
then
    mkdir "./renderings/"
fi

# gets the path that this script is in
customrealpath() {
	[[ $1 = /* ]] && echo "$1" || echo "$PWD/${1#./}"
}

SCRIPT=`customrealpath $0`
THIS_PATH=`dirname $SCRIPT`

filePath=$(grep "dataFile" ./configuration.json | tr -d [:space:] | tr -d ',"' | cut -c 10-)

python \
	../../../visualization_tools/renderTesselation2d.py  \
	$1 \
	$THIS_PATH/output/tesselation.dat \
	$THIS_PATH/output/distances.dat \
	$THIS_PATH/output/sourceMap.dat \
	$THIS_PATH/output/components.dat \
	$THIS_PATH/output/sitePositions.dat \
	$THIS_PATH/output/voxelStatuses.dat \
	$THIS_PATH/renderings \
	$filePath \
	double \
	672 672 \
	0.49 0.54 0.565 0.6 0.64 0.714

