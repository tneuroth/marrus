#!/bin/bash

if [ ! -d "./renderings/" ] 
then
    mkdir "./renderings/"
fi

# gets the path that this script is in
customrealpath() {
	[[ $1 = /* ]] && echo "$1" || echo "$PWD/${1#./}"
}

SCRIPT=`customrealpath $0`
THIS_PATH=`dirname $SCRIPT`

filePath=$(grep "dataFile" ./configuration.json | tr -d [[:space:]] | tr -d ',"' | cut -c 10-)

echo $filePath

in1=00000

for i in {00001..99999}; do

	prefix=$THIS_PATH/output/
	postfx=$i.dat
	postfx=$i.dat
	postf1=$in1.dat	
	FILE=$prefix/tesselation.$postfx

	echo $postfx
	echo $postf1

	if test -f "$FILE"; then
		python \
			../../../visualization_tools/renderTesselation2dWithArrows.py  \
			$i \
			$prefix/tesselation.$postf1 \
			$prefix/distances.$postf1 \
			$prefix/sourceMap.$postf1 \
			$prefix/components.dat \
			$prefix/sitePositions.$postfx \
			$prefix/sitePositions.$postf1 \
			$prefix/voxelStatuses.$postf1 \
			$THIS_PATH/renderings \
			$filePath \
			double \
			672 672 \
		0.49 0.54 0.565 0.6 0.64 0.714
	else
		break
	fi

	in1=$i

done
