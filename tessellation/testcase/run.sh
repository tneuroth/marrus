#!/bin/bash

# gets the path that this script is in
customrealpath() {
	[[ $1 = /* ]] && echo "$1" || echo "$PWD/${1#./}"
}

SCRIPT=`customrealpath $0`
THIS_PATH=`dirname $SCRIPT`
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$THIS_PATH/../lib/
$THIS_PATH/../bin/lsrcvt $1 $THIS_PATH/configuration.json

#/opt/cuda/extras/compute-sanitizer/compute-sanitizer $THIS_PATH/../../../bin/lsrcvd $THIS_PATH/configuration.json

./render.sh $1
