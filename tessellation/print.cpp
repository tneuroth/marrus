#include <iostream>
#include <vector>
#include <cmath>
using namespace std;

void printPattern( int n ) {

    int step = n / 2;
    
    // Start with the midpoint of the full range
    while ( step >= 1 ) 
    {
        // Iterate over all midpoints at this step size
        for (int i = step; i < n; i += step * 2) {
            int mid = i;
            std::cout << mid << " ";
        }
        
        // Move to the next smaller step size
        step /= 2;
    }
}

int main() {
    int n;
    cin >> n;
    printPattern(n);
    return 0;
}
