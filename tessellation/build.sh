
#!/bin/bash

#####################################################
# gets the path that this script is in

customrealpath() {
    [[ $1 = /* ]] && echo "$1" || echo "$PWD/${1#./}"
}

SCRIPT=`customrealpath $0`
THIS_PATH=`dirname $SCRIPT`

######################################################

$THIS_PATH/scripts/install/compile_lib.sh 
$THIS_PATH/scripts/install/compile_bin.sh 
 