#!/bin/bash

#####################################################
# gets the path that this script is in

customrealpath() {
    [[ $1 = /* ]] && echo "$1" || echo "$PWD/${1#./}"
}

SCRIPT=`customrealpath $0`
THIS_PATH=`dirname $SCRIPT`
BASE_PATH=$THIS_PATH/../../

#####################################################

g++ -std=c++17 -O3 -fpic -fopenmp \
    -o $BASE_PATH/bin/lsrcvt \
    -I$BASE_PATH/../common/ \
    -I$BASE_PATH/include/ \
    -I$BASE_PATH/../thirdParty/ \
    $BASE_PATH/../common/algorithms/Standard/MyAlgorithms.cpp \
    $BASE_PATH/src/lsrcvt/lsrcvt.cpp \
    -L$BASE_PATH/lib/ -llsrcvt

# g++ -std=c++17 -O3 -fpic -fopenmp \
#     -o $BASE_PATH/bin/lsrcvt_ooc \
#     -I$BASE_PATH/../common/ \
#     -I$BASE_PATH/include/ \
#     -I$BASE_PATH/../thirdParty/ \
#     $BASE_PATH/../common/algorithms/Standard/MyAlgorithms.cpp \
#     $BASE_PATH/src/lsrcvt/lsrcvt_ooc.cpp \
#     -L$BASE_PATH/lib/ -llsrcvt
