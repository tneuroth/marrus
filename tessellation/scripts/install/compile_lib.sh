#!/bin/bash

#####################################################
# gets the path that this script is in

customrealpath() {
    [[ $1 = /* ]] && echo "$1" || echo "$PWD/${1#./}"
}
SCRIPT=`customrealpath $0`
THIS_PATH=`dirname $SCRIPT`
BASE_PATH=$THIS_PATH/../../

#####################################################

echo $BASE_PATH

nvcc -std=c++17 -O3 -w -m 64 -rdc=false -ftz=true -use_fast_math \
    -Xptxas -O4,\
    -Xcompiler -O4 \
     -arch=sm_52 \
    -I$BASE_PATH/../common/ \
    -I$BASE_PATH/../connected_components/ \
    -lboost_graph \
    --ptxas-options=-v --compiler-options '-fPIC' -o $BASE_PATH/lib/liblsrcvt.so --shared \
    $BASE_PATH/../common/algorithms/Standard/MyAlgorithms.cpp \
    $BASE_PATH/src/lsrcvt/lsrcvt_lib.cu \
    $BASE_PATH/src/lsrcvt/wlsrcvt_lib.cu

cp $BASE_PATH/src/lsrcvt/lsrcvt_lib.hpp      $BASE_PATH/include/lsrcvt/liblsrcvt.hpp
cp $BASE_PATH/src/lsrcvt/configuration.hpp   $BASE_PATH/include/lsrcvt/

