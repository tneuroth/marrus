#!/bin/bash

./compile_lib.sh

mpic++ -std=c++17 -O3 -fpic -fopenmp \
    -o ./bin/lsrcvt_mpi \
    -I./common/ \
    -I./third_party/ \
    -Wl,-R -Wl, ./lib/lsrcvt.so  \
    ./common/algorithms/Standard/MyAlgorithms.cpp \
    ./src/lsrcvd/lsrcvt_mpi.cpp
