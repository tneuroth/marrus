#!/bin/bash

# ./compile_lib.sh

g++ -std=c++17 -O3 -fpic -fopenmp \
    -o dhff_dbs \
    -I./src/ \
    -I./common/ \
    ./common/algorithms/Standard/MyAlgorithms.cpp \
    ./src/dhff_dbs/dhff_dbs_test.cpp

g++ -std=c++17 -O3 -fpic -fopenmp \
    -o dhff_dbs_summarize \
    -I./src/ \
    -I./common/ \
    ./common/algorithms/Standard/MyAlgorithms.cpp \
    ./src/dhffdbs/summarize_test.cpp
