import numpy as np
import matplotlib.pyplot as plt
import random
from matplotlib import colors
from scipy import ndimage
import sys
import random 
from random import randrange


def index3d( id, X, Y, Z ) :
    x = id % X;
    tmp = ( id - x ) / X
    y = tmp % Y;
    z  = ( tmp - y ) / Y
    return ( x, y, z )

def extractPath( idx, srcMap, sx, sy, stats ) :
	id3 = index3d( idx, 672, 672, 1  )
	px = [ id3[ 0 ] ]
	py = [ id3[ 1 ] ]
	curr = idx
	while True:
		if stats[ curr ] & 1 : # los
			px.append( sx[ srcMap[ curr ] ] )
			py.append( sy[ srcMap[ curr ] ] )
			return px, py
		sidx = srcMap[ curr ]
		sid3 = index3d( sidx, 672, 672, 1  )
		px.append( sid3[ 0 ] )
		py.append( sid3[ 1 ] )
		curr = sidx
		#print( "appending " )

plt.rcParams["figure.figsize"] = [ 5, 5 ]
plt.rcParams['axes.facecolor']='red'
plt.rcParams['savefig.facecolor']='red'

# minimum args:    <tesellation directory> <render output director> <dims.x> <dims.y> 
# to show contours <filepath> <format> <offset> <contour1, contour2, ..., countourn>  )

if len( sys.argv ) < 6:
	print( "Error: Minimum command line args not provide. " \
		   "Expected at least <tesellation directory> <render output director> <dims.x> <dims.y>")
	exit(1)

timestep   = sys.argv[ 1 ]
tessPath   = sys.argv[ 2 ]
distPath   = sys.argv[ 3 ]
srcMapPath = sys.argv[ 4 ]
cmpPath    = sys.argv[ 5 ]
sitePath   = sys.argv[ 6 ]
sitePath1  = sys.argv[ 7 ]
statsPath  = sys.argv[ 8 ]
outPath    = sys.argv[ 9 ]
dataPath   = sys.argv[ 10 ]
dataFormat = sys.argv[ 11 ]
DX         = int( sys.argv[ 12 ] )
DY         = int( sys.argv[ 13 ] )

contours = []
for i in range( 14, len( sys.argv ) ):
	contours.append( sys.argv[ i ] )

statuses = np.fromfile( statsPath,  dtype='uint8' ) #.reshape( ( DX, DY ) )
tess     = np.fromfile( tessPath,   dtype='int32' )
comp     = np.fromfile( cmpPath,    dtype='int32' ).reshape( ( DX, DY ) )
dist     = np.fromfile( distPath,   dtype='f4' ).reshape( ( DX, DY ) )
sm       = np.fromfile( srcMapPath, dtype='int64' ) #.reshape( ( DX, DY ) )

maxDis = np.max( dist )
minDis = np.min( dist )

#print( minDis )
#print( maxDis )

mxSrc = np.max( sm )
mnSrc = np.min( sm )

# #print( mnSrc )
# #print( mxSrc )

sites  = np.fromfile( sitePath, dtype='f8' )
sites1 = np.fromfile( sitePath1, dtype='f8' )

print( sitePath )
print( sitePath1 )

NS = int( len( sites ) / 3 ) 

# x = [ sites[ i * 3     ] * 672 - 0.5 for i in range( NS ) ]
# y = [ sites[ i * 3 + 1 ] * 672 - 0.5 for i in range( NS ) ]

random.seed(100019)

plt.xlim( ( 0, 672 ) )
plt.ylim( ( 672, 0 ) )

plt.imshow( np.log2( dist, where=dist > 0 ), cmap="bone_r", vmin=-1.0, vmax=8 )

#plt.imshow( np.log( dist, where=dist > 0 ), cmap="Greys_r" )
if len( contours ) > 0 :
	fieldDat = np.fromfile( dataPath, dtype=( 'f8' if ( dataFormat == "double" ) else "f4" ) )
	fieldDat = ( fieldDat[ 28 * ( 672 * 672 ) : 29 * ( 672 * 672 ) ] ).reshape( ( 672, 672 ) )
	plt.contour( fieldDat, contours, linewidths=1.5, cmap="YlOrBr"   )

paths = [ randrange( 672*672-1 ) for k in range( 16 ) ]
# for i in paths :
# 	p_x, p_y = extractPath( i, sm, x, y, statuses )
# 	plt.plot( p_x, p_y, color='black', linewidth=1.0 )

x0 = [ sites[ i * 3     ] * 672 - 0.5 for i in range( NS ) ]
y0 = [ sites[ i * 3 + 1 ] * 672 - 0.5 for i in range( NS ) ]

x1 = [ sites1[ i * 3     ] * 672 - 0.5 for i in range( NS ) ]
y1 = [ sites1[ i * 3 + 1 ] * 672 - 0.5 for i in range( NS ) ]


# plt.scatter( x1, y1, color=( 0.1, 0.1, 0.1 ), s=10 )
# plt.scatter( x1, y1, color=( 0.1, 0.1, 0.1 ), s=10 )

# plt.scatter( x1, y1, color=( 1.0, 1.0, 1.0 ), s=10 )
# plt.scatter( x0, y0, color=( 1.0, 1.0, 1.0 ), s=10 )

# plt.show();


plt.axis('off')
plt.tight_layout()
# plt.savefig( "r" + str( timestep ).zfill( 4 ) + ".png", dpi=300, transparent=True )
# exit()

# plt.clear()

# plt.imshow( sm )
# plt.show();

mn = np.min( tess )
mx = np.max( tess )

perm = np.arange( 0, mx+2, 1 )
random.shuffle( perm )
tess = np.asarray( [ t if t < 0 else perm[ t + 1 ]  for t in tess ] ).reshape( ( DX, DY ) )
# divnorm=colors.Normalize( vmin=mn, vmax=mx )

norm = colors.Normalize(vmin=mn, vmax=mx*1.3)
cmap = plt.cm.tab20b
m = plt.cm.ScalarMappable(norm=norm, cmap=cmap)
tessRGB = m.to_rgba( tess )

statuses2d = statuses.reshape( ( DX, DY ) )

sx = ndimage.sobel( tess, axis=0 ) # mode='constant')
sy = ndimage.sobel( tess, axis=1 ) # mode='constant')
sobel=np.hypot(sx,sy)
sobel = np.where( sobel > 0.00001, 1, 0 )

# CMP = 4
for i in range( DX ) :
	for j in range( DY ) :
		if( sobel[ i, j ] > 0 ) :
			for c in range( 3 ):
				tessRGB[ i, j, c ] = tessRGB[ i, j, c ] - 0.15
		# if comp[ i ][ j ] != CMP :
		# 	tessRGB[ i, j, :3 ] = 1.0,1.0,1.0
		# 	continue  
		# if( statuses2d[ i, j ] & 8 == 0 ):                              # not line of site and not resolved
		# 	if( randrange( 0, 2 ) % 2 == 0 ):
		# 		for c in range( 3 ):
		# 			tessRGB[ i, j, c ] = tessRGB[ i, j, c ] - 0.15
		# if statuses[ i, j ] & 6:
		# 	tessRGB[ i, j, :3 ] = 1.0, 0.8, 1.0 
		# if( statuses0[ i, j ] & 4 and not ( statuses0[ i, j ] & 8 ) ): # resolved not line of site				
		# 	rgba[ i, j, :3 ] = 1.0, 0.9, 0.7 			
		# if( statuses2d[ i, j ] & 16 ) :                                 # inner egdes
		# 	tessRGB[ i, j, :3 ] = 0.5, 1.0, 0.5                           
		# if( statuses2d[ i, j ] & 32 ):                                 # outerEdge
		# 	tessRGB[ i, j, :3 ] = 0.7, 0.7, 0.7
		# if( tess[ i, j ] == -1 ):  
		# 	tessRGB[ i, j, :3 ] = 0.8, 0.3, 0.4
		if( ( statuses2d[ i, j ] & 8 != 0 ) ) : 
			tessRGB[ i, j, :3 ] = tessRGB[ i, j, :3 ] + [ 0.09, 0.09, 0.09 ]

plt.imshow( tessRGB )

# if len( contours ) > 0 :
# 	fieldDat = np.fromfile( dataPath, dtype=( 'f8' if ( dataFormat == "double" ) else "f4" ) )
# 	fieldDat = ( fieldDat[ 28 * ( 672 * 672 ) : 29 * ( 672 * 672 ) ] ).reshape( ( 672, 672 ) )
# 	plt.contour( fieldDat, contours, linewidths=1.5 )


# plt.axis('off')
# plt.tight_layout()

if len( contours ) > 0 :
	fieldDat = np.fromfile( dataPath, dtype=( 'f8' if ( dataFormat == "double" ) else "f4" ) )
	fieldDat = ( fieldDat[ 28 * ( 672 * 672 ) : 29 * ( 672 * 672 ) ] ).reshape( ( 672, 672 ) )
	plt.contour( fieldDat, contours, linewidths=1.5, cmap="YlOrBr" ) #colors='black' )

if( int( timestep ) == 1 ):
	plt.scatter( x1, y1, color=( 1.0, 1.0,  1.0 ), s=6, zorder=4 )
	plt.scatter( x1, y1, color=(  0.2, 0.2, 0.2),  s=3, zorder=4 )
	plt.savefig( "tess" + str( 0 ).zfill( 4 ) + ".png", dpi=300, transparent=True )

for i in range( NS ):
	plt.arrow( x1[ i ], y1[ i ], ( x0[ i ] - x1[ i ] ), ( y0[ i ] - y1[ i ] ), head_length=5, color=(0.2, 0.2, 0.2), length_includes_head=True, head_width=4, zorder=5  )
	#plt.plot( [ x1[ i ], x0[i] ], [ y1[ i ], y0[i] ], color=(0.0, 0.0, 0.0), linewidth=1 )

plt.xlim( ( 0, 672 ) )
plt.ylim( ( 672, 0 ) )

# plt.scatter( x0, y0, color=( 1.0, 0.1, 0.0 ),  s=4, zorder=2 )

plt.scatter( x1, y1, color=( 1.0, 1.0,  1.0 ), s=6, zorder=4 )
plt.scatter( x1, y1, color=(  0.2, 0.2, 0.2),  s=3, zorder=4 )


#testIds = [ randrange( 0, 672 * 672 - 1 ) for i in range( 30 ) ]
testIds = paths

#print( testIds )

# for i in testIds :
# 	p_x, p_y = extractPath( i, sm, x, y, statuses )
# 	plt.plot( p_x, p_y, color='black' )

# plt.scatter( x, y, color=( 1.0, 1.0, 1.0 ), s=60 )
# plt.scatter( x, y, color=( 0.1, 0.1, 0.1 ), s=40 )

plt.tight_layout()
plt.show()
# plt.savefig( "tess" + str( timestep ).zfill( 4 ) + ".png", dpi=300, transparent=True )

