import numpy as np
import matplotlib.pyplot as plt
import random
from matplotlib import colors
from scipy import ndimage
import sys
import random 
from random import randrange


def index3d( id, X, Y, Z ) :
    x = id % X;
    tmp = ( id - x ) / X
    y = tmp % Y;
    z  = ( tmp - y ) / Y
    return ( x, y, z )

def extractPath( idx, srcMap, sx, sy, stats ) :
	id3 = index3d( idx, 672, 672, 1  )
	px = [ id3[ 0 ] ]
	py = [ id3[ 1 ] ]
	curr = idx
	while True:
		if stats[ curr ] & 1 : # los
			px.append( sx[ srcMap[ curr ] ] )
			py.append( sy[ srcMap[ curr ] ] )
			return px, py
		sidx = srcMap[ curr ]
		sid3 = index3d( sidx, 672, 672, 1  )
		px.append( sid3[ 0 ] )
		py.append( sid3[ 1 ] )
		curr = sidx
		#print( "appending " )

plt.rcParams["figure.figsize"] = [ 5, 5 ]

# minimum args:    <tesellation directory> <render output director> <dims.x> <dims.y> 
# to show contours <filepath> <format> <offset> <contour1, contour2, ..., countourn>  )

if len( sys.argv ) < 6:
	print( "Error: Minimum command line args not provide. " \
		   "Expected at least <tesellation directory> <render output director> <dims.x> <dims.y>")
	exit(1)

timestep   = sys.argv[ 1 ]
tessPath   = sys.argv[ 2 ]
distPath   = sys.argv[ 3 ]
srcMapPath = sys.argv[ 4 ]
cmpPath    = sys.argv[ 5 ]
sitePath   = sys.argv[ 6 ]
statsPath  = sys.argv[ 7 ]
outPath    = sys.argv[ 8 ]
dataPath   = sys.argv[ 9 ]
dataFormat = sys.argv[ 10 ]
DX         = int( sys.argv[ 11 ] )
DY         = int( sys.argv[ 12 ] )

contours = []
for i in range( 13, len( sys.argv ) ):
	contours.append( sys.argv[ i ] )

statuses = np.fromfile( statsPath, dtype='uint8' ) #.reshape( ( DX, DY ) )
tess     = np.fromfile( tessPath, dtype='int32' )
comp     = np.fromfile( cmpPath, dtype='int32' ).reshape( ( DX, DY ) )
dist     = np.fromfile( distPath, dtype='f4' ).reshape( ( DX, DY ) )
sm       = np.fromfile( srcMapPath, dtype='int64' ) #.reshape( ( DX, DY ) )


maxDis = np.max( dist )
minDis = np.min( dist )

#print( minDis )
#print( maxDis )

mxSrc = np.max( sm )
mnSrc = np.min( sm )

# #print( mnSrc )
# #print( mxSrc )

sites = np.fromfile( sitePath, dtype='f4' )
NS = int( len( sites ) / 3 ) 

x = [ sites[ i * 3     ] * 672 - 0.5 for i in range( NS ) ]
y = [ sites[ i * 3 + 1 ] * 672 - 0.5 for i in range( NS ) ]

random.seed(10008)

plt.imshow( np.log( dist, where=dist > 0 ) )
if len( contours ) > 0 :
	fieldDat = np.fromfile( dataPath, dtype=( 'f8' if ( dataFormat == "double" ) else "f4" ) )
	fieldDat = ( fieldDat[ 28 * ( 672 * 672 ) : 29 * ( 672 * 672 ) ] ).reshape( ( 672, 672 ) )
	plt.contour( fieldDat, contours, linewidths=2.5, cmap="YlOrBr" )
plt.scatter( x, y, color=( 1.0, 1.0, 1.0 ), s=10 )
plt.scatter( x, y, color=( 0.1, 0.1, 0.1 ), s=10 )


# paths = [ randrange( 672*672-1 ) for k in range( 16 ) ]

# for i in paths :
# 	p_x, p_y = extractPath( i, sm, x, y, statuses )
# 	plt.plot( p_x, p_y, color='black', linewidth=1.0 )

# plt.show();

plt.axis('off')
plt.tight_layout()
plt.savefig( "r" + str( timestep ).zfill( 4 ) + ".png", dpi=200 )
# exit()

# plt.imshow( sm )
# plt.show();

mn = np.min( tess )
mx = np.max( tess )

perm = np.arange( 0, mx+2, 1 )
random.shuffle( perm )
tess = np.asarray( [ t if t < 0 else perm[ t + 1 ]  for t in tess ] ).reshape( ( DX, DY ) )
# divnorm=colors.Normalize( vmin=mn, vmax=mx )

norm = colors.Normalize(vmin=mn, vmax=mx*1.3)
cmap = plt.cm.gist_ncar
m = plt.cm.ScalarMappable(norm=norm, cmap=cmap)
tessRGB = m.to_rgba( tess )

statuses2d = statuses.reshape( ( DX, DY ) )

for i in range( DX ) :
	for j in range( DY ) :

		if tess[ i, j ] == -1 :
			tessRGB[ i, j, :3 ] = 0.7, 0.7, 0.7

		# if( statuses2d[ i, j ] & 8 == 0 ):                              # not line of site and not resolved
		# 	if( randrange( 0, 2 ) % 2 == 0 ):
		# 		for c in range( 3 ):
		# 			tessRGB[ i, j, c ] = tessRGB[ i, j, c ] - 0.15

		# if statuses2d[ i, j ] & 6:
		# 	tessRGB[ i, j, :3 ] = 1.0, 0.8, 1.0 

		# if( statuses2d[ i, j ] & 4 and not ( statuses2d[ i, j ] & 8 ) ): # resolved not line of site				
		# 	rgba[ i, j, :3 ] = 1.0, 0.9, 0.7 			
		# if( statuses2d[ i, j ] & 16 ) :                                 # inner egdes
		# 	tessRGB[ i, j, :3 ] = 0.5, 1.0, 0.5                           
		# if( statuses2d[ i, j ] & 32 ):                                 # outerEdge
		# 	tessRGB[ i, j, :3 ] = 0.7, 0.7, 0.7
		# if( tess[ i, j ] == -1 ):  
		# 	tessRGB[ i, j, :3 ] = 0.8, 0.3, 0.4
		# if( ( statuses2d[ i, j ] & 8 == 0 ) ) : #non los and is node
		# 	tessRGB[ i, j, :3 ] = 1.0, 1.0, 0.0

plt.imshow( tessRGB )

if len( contours ) > 0 :
	fieldDat = np.fromfile( dataPath, dtype=( 'f8' if ( dataFormat == "double" ) else "f4" ) )
	fieldDat = ( fieldDat[ 28 * ( 672 * 672 ) : 29 * ( 672 * 672 ) ] ).reshape( ( 672, 672 ) )
	plt.contour( fieldDat, contours, linewidths=2.5, cmap="YlOrBr" )



#testIds = [ randrange( 0, 672 * 672 - 1 ) for i in range( 30 ) ]
# testIds = paths

#print( testIds )

# for i in testIds :
# 	p_x, p_y = extractPath( i, sm, x, y, statuses )
# 	plt.plot( p_x, p_y, color='black' )

plt.scatter( x, y, color=( 1.0, 1.0, 1.0 ), s=60 )
plt.scatter( x, y, color=( 0.1, 0.1, 0.1 ), s=40 )

plt.show()

# plt.savefig( "tess" + str( timestep ).zfill( 4 ) + ".png", dpi=200 )

# plt.show()

