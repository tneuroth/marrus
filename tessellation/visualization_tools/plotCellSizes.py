import numpy as np
import matplotlib.pyplot as plt
import random
import matplotlib
from matplotlib import colors
from scipy import ndimage
import sys
from scipy.optimize import curve_fit
import matplotlib.ticker as mtick


fpath = "/home/v1/VIDI1_MNT/combustion/turb_shear/10atm_connected_components/level2ComponentCounts.bin"
ccsz = np.fromfile( fpath, dtype='int32' ).reshape( ( 199, 32 ) )
#plt.imshow( ccsz )
#plt.show()

def func(x, a, b, c, d, e, f):
    return a + b*x + c*x*x + d*x*x*x + e*x*x*x*x + f*x*x*x*x*x

x = np.linspace( 0.0002, 0.000598, 199 )
y = [ ccsz[ i ][ 2 ] for i in range( 199 ) ]

y1 = [ ccsz[ i ][ 0  ] for i in range( 199 ) ]
y2 = [ ccsz[ i ][ 1  ] for i in range( 199 ) ]
y3 = [ ccsz[ i ][ 2  ] for i in range( 199 ) ]
y4 = [ ccsz[ i ][ 3  ] for i in range( 199 ) ]
y5 = [ ccsz[ i ][ 4  ] for i in range( 199 ) ]
y6 = [ ccsz[ i ][ 5  ] for i in range( 199 ) ]
y7 = [ ccsz[ i ][ 6  ] for i in range( 199 ) ]
y8 = [ ccsz[ i ][ 7  ] for i in range( 199 ) ]
y9 = [ ccsz[ i ][ 8  ] for i in range( 199 ) ]
y10 = [ ccsz[ i ][ 9  ] for i in range( 199 ) ]

fig = plt.figure()
ax = fig.add_subplot(111)

ax.plot( x, y1,  label="|C|>1"    )
ax.plot( x, y2,  label="|C|>2"    )
ax.plot( x, y3,  label="|C|>4"    )
ax.plot( x, y4,  label="|C|>8"    )
ax.plot( x, y5,  label="|C|>16"   )
ax.plot( x, y6,  label="|C|>32"   )
ax.plot( x, y7,  label="|C|>64"   )
ax.plot( x, y8,  label="|C|>128"  )
ax.plot( x, y9,  label="|C|>256"  )
ax.plot( x, y10, label="|C|>512"  )


ax.xaxis.set_major_formatter(mtick.FormatStrFormatter('%.2e'))

plt.title( "Connected Components of Super Level-Sets of Temp. at 1770" ) 
plt.ylabel( "num components" )
plt.xlabel( r"$t$" ) 

plt.legend()
plt.show()
