import numpy as np
import matplotlib.pyplot as plt
import random
import matplotlib
from matplotlib import colors
from scipy import ndimage
import sys

matplotlib.use('Agg')

plt.rcParams["figure.figsize"] = [ 5, 5 ]
centroidalStep = int( sys.argv[ 1 ] )
nIntermediates = int( sys.argv[ 2 ] )
whichKind = str( sys.argv[ 3 ] )

savePath = "./results/test2d/img/" + whichKind
statusPathPrefix = "./results/test2d/dat/voxelStatuses." + str(centroidalStep).zfill( 5 ) + "." + whichKind + "."; 

lfPath = "/home/ts/Desktop/data/s3d_auto_ignition_2D/data/hcci.1.1650E-03.field.mpi";
fieldDat = np.fromfile( lfPath, dtype='f8' )
fieldDat = ( fieldDat[ 28 * ( 672 * 672 ) : 29 * ( 672 * 672 ) ] ).reshape( ( 672, 672 ) )
fmin = np.min( fieldDat )
fmax = np.max( fieldDat )
ct = [ 0.54, 0.565, 0.6, 0.64 ]
cs = plt.contour( fieldDat, ct, linewidths=2.5 ) 

sitePath = "./results/test2d/dat/sitePositions." + str( centroidalStep ).zfill( 5 ) + ".dat"
sites = np.fromfile( sitePath, dtype='f8' )
nsites = int( sites.size / 3 ) 

x = [ sites[ i * 3     ] * 671 for i in range( nsites ) ]
y = [ sites[ i * 3 + 1 ] * 671 for i in range( nsites ) ]

rgb = np.ones( [ 672, 672, 3 ] )
st  = np.ones( [ 672*672 ],  dtype='uint8' )

activeColor    = np.array( [ 1.0, 0.5, 0.0 ], dtype=np.float32 )
resolvedColor  = np.array( [ 0.0, 0.5, 1.0 ], dtype=np.float32 )
outerEdgeColor = np.array( [ 0.2, 0.2, 0.2 ], dtype=np.float32 )
innerEdgeColor = np.array( [ 0.7, 0.7, 0.7 ], dtype=np.float32 )
unclassColored = np.array( [ 0.9, 0.9, 0.9 ], dtype=np.float32 )

def pixMap( s ):
	return activeColor    if s & 2 and ( not s & 4 ) else \
		   outerEdgeColor if s & 128 else \
		   innerEdgeColor if s & 64  else \
		   resolvedColor  if s & 4   else \
		   unclassColored

for iStep in range( nIntermediates ):

	st  = np.fromfile( statusPathPrefix + str( iStep ).zfill( 5 ) + ".dat", dtype='uint8' )
	rgb = np.array( [ pixMap( s ) for s in st ] ).reshape( ( 672, 672, 3 ) )

	plt.axis('off')
	plt.tight_layout()
	plt.imshow(  rgb )
	plt.contour( fieldDat, ct, linewidths=2.5 )
	plt.scatter( x, y, color='black', s=2 )
	plt.savefig( savePath + '/istep.' + whichKind + "." + str(iStep).zfill( 5 ) + '.png', format='png', transparent=True, bbox_inches='tight', pad_inches=0  ) 

	print( "saved image " + str( iStep ) )