import numpy as np
import matplotlib.pyplot as plt
import random
from matplotlib import colors
from scipy import ndimage
import sys
from matplotlib.artist import Artist
# plt.style.use('plot_style.txt')



plt.rcParams["figure.figsize"] = [ 12.7, 2.5 ]
datPath = "../examples/workstation/autoignition/output/"

# plt.rcParams['axes.facecolor']=(0.1, 0.1, 0.1)
# plt.rcParams['savefig.facecolor']=(0.1, 0.1, 0.1)
plt.rc('axes',edgecolor='gray')
plt.rc('axes',edgecolor='gray')
plt.rcParams['text.color']      = (0.2, 0.2, 0.2)
plt.rcParams['axes.labelcolor'] = (0.2, 0.2, 0.2)
plt.rcParams['xtick.color']     = (0.2, 0.2, 0.2)
plt.rcParams['ytick.color']     = (0.2, 0.2, 0.2)

# plt.rc('text', usetex=True)
plt.rc('axes', linewidth=2)
plt.rc('font', weight='bold')

plt.xlabel( 'Number of Centroidal Updates', fontsize=17)

# ax.spines['bottom'].set_color('white')
# ax.spines['top'].set_color('white') 
# ax.spines['right'].set_color('white')
# ax.spines['left'].set_color('white')
# ax.tick_params(axis='x', colors='white')
# ax.tick_params(axis='y', colors='white')

m1 = np.fromfile( datPath + "meanDelta.double.60.dat", dtype='f8' )
m2 = np.fromfile( datPath +  "maxDelta.double.60.dat", dtype='f8' )

time = np.arange( 1, 61 )

frame1 = plt.text(0.6, 0.7,'', size=18, ha="center", va="center" )
frame2 = plt.text(0.6, 0.7,'', size=18, ha="center", va="center" )

plt.plot( time, m1, linewidth=4, color=(0.5, 0.5, 0.5), label="Mean ds", zorder=-1 )
plt.plot( time, m2, linewidth=4, color=(0.9, 0.9, 0.9), label="Max ds", zorder=-1 )
plt.legend()

plt.ylim((0, 90))

for i in range( 60 ):

	# plt.text( 0.9, 0.9, str(m1[i-1:i+1]), size=15, color='purple')

	if i > 1:
		plt.plot( time[i-2:i], m1[i-2:i], linewidth=4.0, color=(1.0, 0.5, 0.4) )
		plt.plot( time[i-2:i], m2[i-2:i], linewidth=4.0, color=(0.4, 0.5, 1.0) )

	if i > 0:

		Artist.set_visible(frame1, False)
		Artist.set_visible(frame2, False)	
		frame1 = plt.text( 32.0, 60.0, str(round(m2[i-1], 4)), size=18, color=(0.4, 0.5, 1.0) )
		frame2 = plt.text( 32.0, 74.0, str(round(m1[i-1], 4)), size=18, color=(1.0, 0.5, 0.4) )
		Artist.set_visible(frame1, True)
		Artist.set_visible(frame2, True)

		plt.scatter( time[i-1], m1[i-1], color=(1.0, 0.5, 0.4) )
		plt.scatter( time[i-1], m2[i-1], color=(0.4, 0.5, 1.0) )

		# plt.scatter( time[i], m1[i], color=(1.0, 0.5, 0.4) )
		# plt.scatter( time[i], m2[i], color=(0.4, 0.5, 1.0) )

	plt.xlabel( "Number of Centroidal Updates" );

	plt.tight_layout() 
	plt.savefig( "delta" + str(i).zfill( 5 ) + ".png", dpi=300, transparent=True )
