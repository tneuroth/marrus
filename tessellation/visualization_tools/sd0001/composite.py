

from PIL import Image, ImageDraw, ImageFilter

DIR = "../examples/workstation/autoignition/"

for i in range(40):

	bkg = Image.new("RGBA", (3840, 2160), ( 255, 255, 255, 255 ) )

	im1 = Image.open( "./delta" + str(i).zfill(5) + ".png"    )
	im2 = Image.open( DIR + 'r' + str(i).zfill(5) + ".png"    )
	im3 = Image.open( DIR + 'tess' + str(i).zfill(5) + ".png" )	

	bkg.paste( im1, (0, 1450 ), im1 )
	bkg.paste( im3, (2378, 0 ), im3 )
	bkg.paste( im2, ( 960,    0 ), im2 )

	bkg.save( './comp.' + str(i).zfill(5) + ".png" )

