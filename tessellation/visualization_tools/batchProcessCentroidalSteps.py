import numpy as np
import matplotlib.pyplot as plt
import random
from matplotlib import colors
from scipy import ndimage
import sys

plt.rcParams["figure.figsize"] = [ 5, 5 ]
nsteps = int( sys.argv[ 1 ] )

resultPath = "./results/test2d/img/centroidal/"

DIR = "/home/v1/Desktop/data/data/s3d_auto_ignition_2D/data/";
lfPath = DIR + "hcci.1.1650E-03.field.mpi";
offset = 28

fieldDat = np.fromfile( lfPath, dtype='f8' )
fieldDat = ( fieldDat[ 28 * ( 672 * 672 ) : 29 * ( 672 * 672 ) ] ).reshape( ( 672, 672 ) )

fmin = np.min( fieldDat )
fmax = np.max( fieldDat )

cmap = plt.cm.Greys
norm = plt.Normalize( fmin, fmax )

cmpPath  = "./results/test2d/dat/components.dat"
components = np.fromfile( cmpPath, dtype='int32' )


for step in range( 3, nsteps+1 ):

	sitePath0 = "./results/test2d/dat/sitePositions." + str( step - 1  ).zfill( 5 ) + ".dat"
	sitePath1 = "./results/test2d/dat/sitePositions." + str( step      ).zfill( 5 ) + ".dat"

	stPath0  = "./results/test2d/dat/voxelStatuses."  + str( step - 1  ).zfill( 5 ) + ".dat"
	stPath1  = "./results/test2d/dat/voxelStatuses."  + str( step      ).zfill( 5 ) + ".dat"

	tessPath  = "./results/test2d/dat/tesselation."  + str( step ).zfill( 5 ) + ".dat"
	tess = np.fromfile( tessPath, dtype='int32' )

	statuses0 = np.fromfile( stPath0, dtype='uint8' )
	statuses1 = np.fromfile( stPath1, dtype='uint8' )

	sites0 = np.fromfile( sitePath0, dtype='f8' )
	sites1 = np.fromfile( sitePath1, dtype='f8' )

	nsites = int( sites0.size / 3 ) 



	x0 = [ sites0[ i * 3     ] * 671 + 0.5 for i in range( nsites ) ]
	y0 = [ sites0[ i * 3 + 1 ] * 671 + 0.5 for i in range( nsites ) ]

	x1 = [ sites1[ i * 3     ] * 671 + 0.5 for i in range( nsites ) ]
	y1 = [ sites1[ i * 3 + 1 ] * 671 + 0.5 for i in range( nsites ) ]

	rgba = cmap( norm( fieldDat ) )
	statuses0 = statuses0.reshape( ( 672, 672 ) )

	mn = np.min( tess )
	mx = np.max( tess )
	perm = np.arange( 0, mx+2, 1 )
	random.shuffle( perm )
	random.shuffle( perm )	
	tess = np.asarray( [ t if t < 0 else perm[ t + 1 ]  for t in tess ] )
	tess = tess.reshape( ( 672, 672 ) )
	tessRGB = plt.cm.PuBuGn( tess )

	for i in range( 672 ) :
		for j in range( 672 ) :
			# if( statuses0[ i, j ] & 12 == 0 ):                              # not line of site and not resolved
			# 	rgba[ i, j, :3 ] = 0.5,0.5, 0.5  
			# if( statuses0[ i, j ] & 4 and       statuses0[ i, j ] & 8 ):   # resolved line of site 
			# 	rgba[ i, j, :3 ] = 0.6, 0.8, 1.0 
			# if( statuses0[ i, j ] & 4 and not ( statuses0[ i, j ] & 8 ) ): # resolved not line of site				
			# 	rgba[ i, j, :3 ] = 1.0, 0.9, 0.7 			
			# if( statuses0[ i, j ] & 64 ) :                                 # inner egdes
			# 	rgba[ i, j, :3 ] = 0.3, 0.4, 0.5                           
				#tessRGB[ i, j, :3 ] = 0.3, 0.6, 0.8  
			# if( statuses0[ i, j ] & 128 ):                                 # outerEdge
			# 	rgba[ i, j, :3 ] = 1.0, 0.6, 0.2
			# 	tessRGB[ i, j, :3 ] = 1.0, 1.0, 0.2		
			if( tess[ i, j ] == -1 ):  
				tessRGB[ i, j, :3 ] = 0.8, 0.3, 0.4
			if( ( statuses0[ i, j ] & 8 == 0 ) ) : #non los and is node
				tessRGB[ i, j, :3 ] = 1.0, 1.0, 0.0



	plt.axis('off')
	plt.tight_layout()

	divnorm=colors.TwoSlopeNorm( vmin=-1.0, vcenter=0., vmax=mx)
	plt.imshow( tessRGB )#, norm=divnorm )
	#plt.imshow(  rgba )

	ct = [ 0.54, 0.565, 0.6, 0.64 ]
	plt.contour( fieldDat, ct, linewidths=2.5, colors=(0.3, 0.3, 0.3, 1) )

	# plt.xticks([56*i for i in range(0,12)])
	# plt.yticks([56*i for i in range(0,12)])
	# plt.grid(True,linewidth=2)
	# plt.savefig( resultPath + 'step' + str(step).zfill( 5 ) + '.blocks.png', dpi=400 ) 

	for i in range( nsites ):
		plt.arrow( x0[ i ], y0[ i ], ( x1[ i ] - x0[ i ] ), ( y1[ i ] - y0[ i ] ), head_length=5, color='black', length_includes_head=True, head_width=6  )
		#plt.plot(  [ x0[ i ], x1[ i ] ], ( y0[ i ], y1[ i ] ) )

	# #  order not the same, just get 
	# plt.scatter( x0, y0, color='black', s=4 )
	# # plt.scatter( x1, y1, color='orange', s=4 )

	plt.show()

	#plt.savefig( resultPath + 'step' + str(step).zfill( 5 ) + '.png', dpi=400 ) 

