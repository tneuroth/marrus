
#include "utils/IndexUtils.hpp"
#include "utils/TF.hpp"
#include "geometry/Vec.hpp"
#include "graph/const.hpp"
#include "dhffdbs/dhffdbs.hpp"

#include "geometry/SurfaceGenerator.hpp"
#include "geometry/GeometryUtils.hpp"
#include <fbxsdk.h>

#include <cstdint>

#include <vector>
#include <fstream>
#include <iostream>
#include <chrono>
#include <unordered_map>
#include <unordered_set>
#include <map>

using namespace std::chrono;
using namespace std;
using namespace dhffdbs;

template< typename T >
void writeData( 
    const string & filePath,
    const vector< T > & data )
{
    cout << " writing data " << data.size() << endl;

    ofstream outFile( filePath, ios::out | ios::binary );

    if( ! outFile.is_open() )
    {
        cout << "file was no opened" << filePath << endl;
    }

    if( data.size() == 0 )
    {
        cout << "data size is 0 " << endl;
    }
    else
    {
        outFile.write( ( char* ) data.data(), data.size() * sizeof( T )  );
        outFile.close();
    }
}

template< typename T >
void loadData( 
    const string & filePath,
    vector< T > & data, 
    size_t offset, 
    size_t numElements )
{
    ifstream inFile( filePath, ios::in | ios::binary );
    if( ! inFile.is_open() ) {
        cout << "could not be opened for reading " << filePath << endl;
       exit( 1 );
    }
    data.resize( numElements );
    inFile.seekg( offset * sizeof( T ) );
    inFile.read( (char*) data.data(), numElements * sizeof( T ) ); 
    inFile.close();
}

int main(int argc, char** argv)
{
    /////////////////////////////////////////////////////////////////////////
    // graph data

    std::vector< std::vector< CCRecord > > ccRecords;
    std::vector< std::vector< IBRecord > > ibRecords;

    std::vector< std::vector< int64_t > > cmpSizes;
    std::vector< int64_t > numEdges;
    std::vector< std::vector< int32_t > > to;
    std::vector< std::vector< int32_t > > from;
    std::vector< std::vector< int32_t > > intersectionSize;


    // nodes
    ibRecords.resize( timeSteps.size() );
    ccRecords.resize( timeSteps.size() );
    cmpSizes.resize(  timeSteps.size() );

    // edges
    to.resize(               timeSteps.size() - 1 );
    from.resize(             timeSteps.size() - 1 );
    intersectionSize.resize( timeSteps.size() - 1 );

    // load number of edges/correspondences per step

    numEdges.resize( timeSteps.size() - 1 );

    std::ifstream inFile( 
        GR_DIR + "/correspondenceCounts.bin", 
        std::ios::in | std::ios::binary );

    if( ! inFile.is_open() ) {
        std::cout << "file not open " << GR_DIR + "/correspondenceCounts.bin" << std::endl;
        exit( 1 );
    }

    inFile.read( (char*) numEdges.data(), numEdges.size()*sizeof( int64_t ) );
    inFile.close();

    std::cout << "loaded correspondenceCounts" << std::endl;

    for( size_t t = 0; t < timeSteps.size(); ++t ) {

        // load ccRecords 

        inFile.open( 
            DB_DIR + "dhffdbs.cc." + timeSteps[ t ] + ".dat", 
            std::ios::in | std::ios::binary );

        if( ! inFile.is_open() ) {
            std::cout << "file not open " << DB_DIR + "dhffdbs.cc." + timeSteps[ t ] + ".dat" << std::endl;
            exit( 1 );
        }

        uint64_t NC;
        inFile.read( (char*) &NC, sizeof( uint64_t ) ); 
        ccRecords[ t ].resize( NC );
        cmpSizes[  t ].resize( NC );
        inFile.read( (char*) ccRecords[ t ].data(), NC * sizeof( CCRecord ) ); 
        inFile.close();

        inFile.open( 
            DB_DIR + "dhffdbs.ib." + timeSteps[ t ] + ".dat", 
            std::ios::in | std::ios::binary );

        if( ! inFile.is_open() ) {
            std::cout << "file not open " << DB_DIR + "dhffdbs.ib." + timeSteps[ t ] + ".dat" << std::endl;
            exit( 1 );
        }

        uint64_t NIB;
        inFile.read( (char*) &NIB, sizeof( uint64_t ) ); 
        ibRecords[ t ].resize( NIB );
        inFile.read( (char*) ibRecords[ t ].data(), NIB * sizeof( IBRecord ) ); 
        inFile.close();

        //cout << NIB << endl;

        uint64_t NV;

        // need to know how many voxels in order to get size of last component 

        inFile.open( 
            DB_DIR + "sorted_index_space." + timeSteps[ t ] + ".dat", 
            std::ios::in | std::ios::binary );

        if( ! inFile.is_open() ) {
            std::cout << "file not open " << DB_DIR + "sorted_index_space." + timeSteps[ t ] + ".dat" << std::endl;
            exit( 1 );
        }

        inFile.read( (char*) &NV, sizeof( uint64_t ) ); 
        inFile.close();

        // double NTOT = std::stod( timeSteps[ t ] ) < 0.000351000 ? 1728*320*1280 : std::stod( timeSteps[ t ] ) < 0.000539001 ? 1728*480*1280 : 1728*640*1280;

        // for( size_t i = 1; i < NIB; ++i ) {
        //    //std::cout << "(" << timeSteps[t] << "," << ( ibRecords[ t ][ 1 ].i0 - ibRecords[ t ][  ].i0 ) / (double) NTOT << ")" << std::endl;
        // }
        // std::cout << "(" << timeSteps[t] << "," << ( NV - ibRecords[ t ][ NIB-1 ].i0 ) / (double) NTOT << ")" << std::endl;

        // compute cmp sizes 

        for( size_t i = 1; i < NC; ++i ) {
            cmpSizes[ t ][ i - 1 ] = ccRecords[ t ][ i ].i0 - ccRecords[ t ][ i - 1 ].i0;
        }

        cmpSizes[ t ][ NC-1 ] = NV - ccRecords[ t ][ NC-1 ].i0;

        // edges

        if( t < timeSteps.size() - 1 ) {

            from[             t ].resize( numEdges[ t ] );
            to[               t ].resize( numEdges[ t ] );
            intersectionSize[ t ].resize( numEdges[ t ] );  

            inFile.open( 
                GR_DIR + "from." + timeSteps[ t ] + "..bin", 
                std::ios::in | std::ios::binary );

            if( ! inFile.is_open() ) {
                std::cout << "file not open " << GR_DIR + "from." + timeSteps[ t ] + "..bin" << std::endl;
                exit( 1 );
            }

            inFile.read( ( char * )from[ t ].data(), from[ t ].size() * sizeof( int32_t ) );
            inFile.close();

            inFile.open( 
                GR_DIR + "to." + timeSteps[ t ] + "..bin", 
                std::ios::in | std::ios::binary );

            if( ! inFile.is_open() ) {
                std::cout << "file not open " << GR_DIR + "to." + timeSteps[ t ] + "..bin" << std::endl;
                exit( 1 );
            }

            inFile.read( ( char * )to[ t ].data(), to[ t ].size() * sizeof( int32_t ) );
            inFile.close();

            inFile.open( 
                GR_DIR + "counts." + timeSteps[ t ] + "..bin", 
                std::ios::in | std::ios::binary );

            if( ! inFile.is_open() ) {
                std::cout << "file not open " << GR_DIR + "counts." + timeSteps[ t ] + "..bin" << std::endl;
                exit( 1 );
            }

            inFile.read( ( char * )intersectionSize[ t ].data(), intersectionSize[ t ].size() * sizeof( int32_t ) );
            inFile.close();


            // std::cout << "loaded step " << t << std::endl;
        }
    }

    ///////////////////////////////////////////////////////////////////

    // these are the first 3
    // std::vector< int > current = { 89, 319, 543 }; // for t=16
   std::vector< int > current = { 3 };

    string outPath = "/home/ts/hd_mnt/combustion/turb_shear/mesh/";

    int X = 1728;
    int Y = 320;    
    int Z = 1280;
    
    vector< float >   temp;
    vector< float >   OH; 
    vector< int16_t > lyrIds;
    vector< int32_t > cmpIds;
    vector< uint8_t > mask;

    int minXP = 0;
    int minZP = 0; 

    int minYP = 0; 
    int maxYP = Y;

    int maxXP = X;
    int maxZP = Z; 

    std::vector< TN::Vec3< float > > TF;
    cout << "opening TF" << endl;
    TN::CM::loadTF( "./visualization_tools/visualization_system/TF/diverging/coolwarm.txt", TF );

    string subfolder = "10atm";

    for( int64_t t = 77; t < timeSteps.size(); ++t ) { // timeSteps.size(); ++t ) {
        
        if( t >= 170 ) {
            Y = 640; 
            minYP = 0; 
            maxYP = Y;      
            subfolder = "10atm_new_grid_2";      
        }
        else if( t >= 76 ) {
            Y = 480;
            minYP = 0; 
            maxYP = Y;     
            subfolder = "10atm_new_grid";      
        }

        string fname1  = "/home/ts/hd_mnt/combustion/turb_shear/" + subfolder + "/float32_downsampled/turb_shear_no_flame." + timeSteps[ t ] + ".field.mpi.temp.1728."   + to_string( Y ) + ".1280.f32.dat";
        string fname2  = "/home/ts/hd_mnt/combustion/turb_shear/" + subfolder + "/float32_downsampled/turb_shear_no_flame." + timeSteps[ t ] + ".field.mpi.OH.1728."     + to_string( Y ) + ".1280.f32.dat";
        string cmpFile = "/home/ts/hd_mnt/combustion/turb_shear/10atm_connected_components/componentMap.1728."  + to_string( Y ) + ".1280." + timeSteps[ t ] + ".dat";
        string lyrFile = "/home/ts/hd_mnt/combustion/turb_shear/10atm_connected_components/layerMap.1728."      + to_string( Y ) + ".1280." + timeSteps[ t ] + ".dat";

        int XS = 0; // max( minXP - 20 0 );
        int YS = 0; // max( minYP - 20 0 ); 
        int ZS = 0; // max( minZP - 20 0 ); 

        int XE = X; //min( maxXP + 20, X );
        int YE = Y; //min( maxYP + 20, Y ); 
        int ZE = Z; //min( maxZP + 20, Z ); 

        loadData( fname2,  OH,      0,  (size_t) X*Y*ZE );

        cout << "loaded OH" << endl;

        loadData( fname1,  temp,    0,  (size_t) X*Y*ZE );

        cout << "loaded temp" << endl;

        loadData( cmpFile, cmpIds,  0, (int32_t) X*Y*ZE );

        cout << "loaded cmpIds" << endl;

        loadData( lyrFile, lyrIds,  0, (int16_t) X*Y*ZE );

        cout << "loaded layerIds" << endl;

        // load the map of edges from. for each in from where, compute the mesh, and then update current to. 
        // print out the sequences of from, to for graph rendering.

        for( auto cur : current ) {

            mask = vector< uint8_t >( X*Y*Z, 0u );

            int minX = X;
            int minY = Y; 
            int minZ = Z; 

            int maxX = 0;
            int maxY = 0; 
            int maxZ = 0; 

            map< int32_t, int64_t > cmpCounts;

            for( int x = XS; x < XE; ++x )
            for( int y = YS; y < YE; ++y )
            for( int z = ZS; z < ZE; ++z ) {

                bool flag = false;
                for( int xn = max( 0, x-2 ); xn < min( X, x + 2 ); ++xn )
                for( int yn = max( 0, y-2 ); yn < min( Y, y + 2 ); ++yn )
                for( int zn = max( 0, z-2 ); zn < min( Z, z + 2 ); ++zn ) {
                    int64_t flat = xn + yn*X + zn*Y*X;
                    // if( cmpIds[ flat ] == cur ) {
                    if( lyrIds[ flat ] == 3 ) {                        
                        flag = true;
                    }
                }
                if( flag ) {
                    int64_t flat = x + y*X + z*Y*X;
                    mask[ flat ] = 1;
                    minX = min( minX, x );
                    minY = min( minY, y );
                    minZ = min( minZ, z );
                    maxX = max( minX, x );
                    maxY = max( maxY, y );
                    maxZ = max( maxZ, z );                        
                }
            }

            minXP = minX;
            minYP = minY;
            minZP = minZ;

            maxXP = maxX;
            maxYP = maxY;
            maxZP = maxZ;
            
            vector< TN::Vec3< float > > surfaceVerts;
            vector< TN::Vec3< float > > surfaceNorms;    

            TN::Vec3< int > dims = { X, Y, Z };

            cout << "computing surface" << endl;

            computeIsosurface( 
                14.75f,    
                temp, 
                mask,
                dims, 
                TN::Vec3< int > ( { minX, minY, minZ } ),
                TN::Vec3< int > ( { maxX, maxY, maxZ } ),        
                surfaceVerts, 
                surfaceNorms );

            cout << "interpolating OH" << endl;

            double NORM = std::max( std::max( X, Y ), Z );
            std::vector< float > scalars;
            triInt(
                OH,
                X,
                Y,
                Z,
                { 
                    { -( X / NORM )*.5, -( Y / NORM )*.5,-( Z / NORM )*.5 },
                    {  ( X / NORM )*.5,  ( Y / NORM )*.5, ( Z / NORM )*.5 }
                },
                surfaceVerts,
                scalars );

            cout << "creating FBX file" << endl;

            TN::Vec2< float > rng = { 0.00028, 0.0016 }; //TN::Sequential::getRange( scalars );

            // cout << "rng = " << rng.a() << " " << rng.b() << endl;

            FbxManager* lSdkManager = FbxManager::Create();
            FbxScene* pScene = FbxScene::Create(lSdkManager,"myScene");
            FbxNode* lMeshNode = FbxNode::Create(pScene, "meshNode");
            FbxMesh* lMesh = FbxMesh::Create(pScene, "mesh");
            lMeshNode->SetNodeAttribute(lMesh);
            FbxNode *lRootNode = pScene->GetRootNode();
            lRootNode->AddChild(lMeshNode);
            lMesh->InitControlPoints( scalars.size() );
            FbxVector4 * lControlPoints = lMesh->GetControlPoints();

            FbxLayer * lLayer = lMesh->GetLayer(0);
            if(lLayer == NULL) {
                lMesh->CreateLayer();
                lLayer = lMesh->GetLayer(0);
            }

            FbxLayer * colorLayer = lMesh->GetLayer(1);
            if(colorLayer == NULL) {
                lMesh->CreateLayer();
                colorLayer = lMesh->GetLayer(1);
            }

            FbxLayerElementNormal* lLayerElementNormal= FbxLayerElementNormal::Create(lMesh, "");
            FbxLayerElementVertexColor * vertexColorLayer = FbxLayerElementVertexColor::Create(lMesh, "");

            vertexColorLayer->SetMappingMode(FbxGeometryElement::eByControlPoint);
            vertexColorLayer->SetReferenceMode(FbxGeometryElement::eDirect);

            lLayerElementNormal->SetMappingMode(FbxGeometryElement::eByControlPoint);
            lLayerElementNormal->SetReferenceMode(FbxGeometryElement::eDirect);

            // load the TF

            float smrMin = rng.a();
            float smrMax = rng.b();        

            cout << "mesh has " << surfaceVerts.size() << " vertices" << std::endl;

            for( size_t k = 0; k < surfaceVerts.size() / 3; ++k )
            {
                FbxVector4 vertexA( 
                    surfaceVerts[ k * 3 ].x(),
                    surfaceVerts[ k * 3 ].y(),
                    surfaceVerts[ k * 3 ].z() );

                FbxVector4 vertexB( 
                    surfaceVerts[ k * 3 + 1 ].x(),
                    surfaceVerts[ k * 3 + 1 ].y(),
                    surfaceVerts[ k * 3 + 1 ].z() );

                FbxVector4 vertexC( 
                    surfaceVerts[ k * 3 + 2 ].x(),
                    surfaceVerts[ k * 3 + 2 ].y(),
                    surfaceVerts[ k * 3 + 2 ].z() );

                lControlPoints[ k * 3 + 0 ] = vertexA;
                lControlPoints[ k * 3 + 1 ] = vertexB;
                lControlPoints[ k * 3 + 2 ] = vertexC;   

                lLayerElementNormal->GetDirectArray().Add(
                    FbxVector4( 
                        surfaceNorms[ k * 3 ].x(),
                        surfaceNorms[ k * 3 ].y(),
                        surfaceNorms[ k * 3 ].z() )
                );

                lLayerElementNormal->GetDirectArray().Add(
                    FbxVector4( 
                        surfaceNorms[ k * 3 + 1 ].x(),
                        surfaceNorms[ k * 3 + 1 ].y(),
                        surfaceNorms[ k * 3 + 1 ].z() )
                );

                lLayerElementNormal->GetDirectArray().Add(
                    FbxVector4( 
                        surfaceNorms[ k * 3 + 2 ].x(),
                        surfaceNorms[ k * 3 + 2 ].y(),
                        surfaceNorms[ k * 3 + 2 ].z() )
                );

                {
                    float r = std::min( std::max( ( scalars[ k * 3 ] - smrMin ) / ( smrMax - smrMin ), 0.f ), 1.f );
                    int clIdx = std::min( (int) std::floor( r * TF.size() ), (int) TF.size() - 1 );
                    TN::Vec3< float > color = TF[ clIdx ];
                    vertexColorLayer->GetDirectArray().Add(
                        FbxColor( 
                            // color.x() * 255, color.y() * 255, color.z()*255 )
                            1.0, 0.0, 0.0 )                            
                    );
                }

                {
                    float r = std::min( std::max( ( scalars[ k * 3 + 1 ] - smrMin ) / ( smrMax - smrMin ), 0.f ), 1.f );
                    int clIdx = std::min( (int) std::floor( r * TF.size() ), (int) TF.size() - 1 );
                    TN::Vec3< float > color = TF[ clIdx ];

                    if( color.x() < 0.05 && color.y() < 0.05 && color.z() < 0.05 ) {
                        cout << "b";
                    }

                    vertexColorLayer->GetDirectArray().Add(
                        FbxColor( 
                            // color.x() * 255, color.y() * 255, color.z()*255 )
                             1.0, 0.0, 0.0 )
                    );
                }

                {
                    float r = std::min( std::max( ( scalars[ k * 3 + 2 ] - smrMin ) / ( smrMax - smrMin ), 0.f ), 1.f );
                    int clIdx = std::min( (int) std::floor( r * TF.size() ), (int) TF.size() - 1 );
                    TN::Vec3< float > color = TF[ clIdx ];
                    vertexColorLayer->GetDirectArray().Add(
                        FbxColor( 
                            // color.x() * 255, color.y() * 255, color.z()*255 )
                            1.0, 0.0, 0.0 )
                    );

                    lMesh->BeginPolygon();
                    lMesh->AddPolygon( k * 3 );
                    lMesh->AddPolygon( k * 3 + 1 );
                    lMesh->AddPolygon( k * 3 + 2 );
                    lMesh->EndPolygon();                                                                
                }
            }

            lLayer->SetNormals(        lLayerElementNormal );
            colorLayer->SetVertexColors( vertexColorLayer  );

            FbxGeometryConverter lGeometryConverter( lSdkManager );
            bool s = lGeometryConverter.Triangulate( pScene, false );

            FbxExporter* lExporter = FbxExporter::Create( lSdkManager, "");
            bool lExportStatus = lExporter->Initialize( ( 
                outPath + "componentSufrace.colored.id_" 
                + std::to_string( cur ) + "." + timeSteps[ t ]
                + ".fbx" ).c_str() , -1, lSdkManager->GetIOSettings() );

            if(!lExportStatus) {
                printf("Call to FbxExporter::Initialize() failed.\n");
                printf("Error returned: %s\n\n", lExporter->GetStatus().GetErrorString());
                return false;
            }

            lExporter->Export(pScene);
            lExporter->Destroy();

            vertexColorLayer->Destroy();
            lLayerElementNormal->Destroy();

            lMesh->Destroy();
            lMeshNode->Destroy();
            pScene->Destroy();
            lSdkManager->Destroy();

            std::cout << " finished ts " << timeSteps[ t ] << endl;
        }

        ////////////////////////////////////////////////////////////////////////////////////////
        // next meshes to compute
        
        // std::unordered_set< int32_t > uniqueTo;

        // std::cout << "curr:" << endl;
        // for( int cur : current ) {
        //     cout << cur << endl;
        // }

        // cout << "\nfrom: " << endl;
        // for( int f = 0; f < from[ t ].size(); ++f ) {
        //     cout << from[ t ][ f ] << endl;
        // }

        // cout << "\nto: " << endl;
        // for( int f = 0; f < to[ t ].size(); ++f ) {
        //     cout << to[ t ][ f ] << endl;
        // }

        // cout << "\nfrom: " << endl;
        // for( int f = 0; f < to[ t ].size(); ++f ) {
        //     cout << to[ t ][ f ] << endl;
        //     for( int cur : current ) {
        //         if( to[ t ][ f ] == cur ) {
        //             uniqueTo.insert( to[ t ][ f ] );
        //         }
        //     }   
        // }

        // current.clear();
        // for( auto u : uniqueTo ) {
        //     current.push_back( u );
        // }

        // if( current.size() == 0 ) {
        //     std::cout << "graph terminated" << endl;
        //     break;
        // }
    }
}