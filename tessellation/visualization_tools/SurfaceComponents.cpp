
#include "utils/IndexUtils.hpp"
#include "utils/TF.hpp"
#include "geometry/Vec.hpp"
#include "algorithms/IsoContour/CubeMarcher.hpp"
#include "geometry/GeometryUtils.hpp"
// #include <fbxsdk.h>

#include <omp.h>

#include <algorithm>
#include <cstdint>
#include <vector>
#include <fstream>
#include <iostream>
#include <chrono>
#include <unordered_map>
#include <array>

using namespace std::chrono;
using namespace std;

template< typename T >
void writeData( 
    ofstream & outFile,
    const vector< T > & data )
{
    outFile.write( ( char* ) data.data(), data.size() * sizeof( T )  );
}

template< typename T >
void loadData( 
    ifstream & inFile,
    vector< T > & data, 
    size_t offset, 
    size_t numElements )
{
    inFile.seekg( offset * sizeof( T ) );
    inFile.read( (char*) data.data(), numElements * sizeof( T ) ); 
}

int main(int argc, char** argv)
{
    const string vfile = argv[ 1 ];
    const size_t X        = stoul( argv[ 2  ] );
    const size_t Y        = stoul( argv[ 3  ] );
    const size_t Z        = stoul( argv[ 4  ] );
    const size_t V_OFFSET = stoul( argv[ 5  ] );
    const string type     =        argv[ 6  ];
    const double isovalue = stod(  argv[ 7  ] );
    const double vtxAlloc = stod(  argv[ 8  ] );
    const size_t N_STAGES = stoul( argv[ 9  ] );
    const string oFile    =        argv[ 10 ];
    
    const size_t N = X*Y*Z;

    ifstream inFile( vfile, ios::in | ios::binary );    
    if( ! inFile.is_open() ) {
        cout << "could not be opened for reading " << vfile << endl;
        exit( 1 );
    }

    cout << "file exists " << vfile << endl;

    ofstream outFile( oFile, ios::out | ios::binary );    
    if( ! outFile.is_open() ) {
        cout << "could not be opened for writing " << oFile << endl;
        exit( 1 );
    }

    int nThreads = 1;
    #pragma omp parallel 
    {
        nThreads = omp_get_num_threads();
    }

    cout << "computing isosurfaces in " << N_STAGES << " out of core stages" << endl;
    cout << "using " << nThreads << " CPU threads" << endl; 

    size_t BLOCKS_PER_STAGE = nThreads;  

    vector< float > dataChunk;
    const size_t TAR_ZC_SIZE = Z / N_STAGES;
    const size_t MAX_ZC_SIZE = Z - TAR_ZC_SIZE * ( N_STAGES - 1 );
    const size_t ZSLICE_SIZE = X*Y;
    dataChunk.reserve( MAX_ZC_SIZE );

    TN::CubeMarcher marchers[ nThreads ];
    vector< TN::Vec3< float > > mesh;

    for( int stage = 0, end = N_STAGES; stage < end; ++stage ) 
    {
        const size_t Z_START = stage * TAR_ZC_SIZE;
        const size_t Z_END   = stage == N_STAGES - 1 ? Z : ( stage + 1 ) * TAR_ZC_SIZE;        
        const size_t ZC_SIZE = Z_END - Z_START;
        const size_t N_ELEMENTS = ZC_SIZE * ZSLICE_SIZE; 

        cout << "loading " << N_ELEMENTS << " elements at offset " << V_OFFSET + Z_START * ZSLICE_SIZE << endl; 
        dataChunk.resize( N_ELEMENTS );

        loadData( 
            inFile, 
            dataChunk,  
            V_OFFSET + Z_START * ZSLICE_SIZE,
            N_ELEMENTS );

        cout << "done loading " << endl; 

        #pragma omp parallel 
        {
            int thIdx = omp_get_thread_num();
            const size_t MY_Z_START    = thIdx * ( ZC_SIZE / nThreads ); 
            const size_t MY_Z_END      = thIdx == nThreads - 1 ? ZC_SIZE : ( thIdx + 1 ) * ( ZC_SIZE / nThreads );
            marchers[ thIdx ]( dataChunk, MY_Z_START, MY_Z_END, X, Y,  MY_Z_END - MY_Z_START, isovalue );            
        }

        cout << "done marching " << endl; 

        for( int thIdx = 0, end = nThreads; thIdx < end; ++thIdx ) 
        {
            const auto & partialResult = marchers[ thIdx ].getVerts();
            for( auto & v : partialResult ) { mesh.push_back( v ); }
        }
    
        cout << "out of core isosurface extraction stage "  << stage << " complete\n" << endl;
    }

    cout << "done extracting isosurface" << endl;
    
    cout << "the surface has " << mesh.size() << " triangles and uses " << mesh.size() * sizeof( float ) * 3 << " bytes" << endl;

    cout << "extracting connected components " << endl;



    cout << "indexing vertices" << endl;

    

    cout << "writing" << endl;

    inFile.close();
    outFile.close();
}