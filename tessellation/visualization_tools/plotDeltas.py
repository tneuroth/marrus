import numpy as np
import matplotlib.pyplot as plt
import random
from matplotlib import colors
from scipy import ndimage
import sys
# plt.style.use('plot_style.txt')

plt.rcParams["figure.figsize"] = [ 6, 5 ]
datPath = "./results/test3d/dat/"

m1 = np.fromfile( datPath + "d4X1X4/meanDelta.double.00014.50.0dat", dtype='f8' )
m2 = np.fromfile( datPath + "d6X1X5/meanDelta.double.00014.50.0dat", dtype='f8' )
m3 = np.fromfile( datPath + "d8X1X8/meanDelta.double.00014.50.0dat", dtype='f8' )
m4 = np.fromfile( datPath + "d4X1X4/meanDelta.double.00028.50.0dat", dtype='f8' )
m5 = np.fromfile( datPath + "d6X1X5/meanDelta.double.00028.50.0dat", dtype='f8' )
m6 = np.fromfile( datPath + "d8X1X8/meanDelta.double.00028.50.0dat", dtype='f8' )
m7 = np.fromfile( datPath + "d4X1X4/meanDelta.double.00056.50.0dat", dtype='f8' )
m8 = np.fromfile( datPath + "d6X1X5/meanDelta.double.00056.50.0dat", dtype='f8' )
m9 = np.fromfile( datPath + "d8X1X8/meanDelta.double.00056.50.0dat", dtype='f8' )


# for p in [  ( m1, "4x1x4", "4451" ),( m4, "4x1x4", "2775" ), ( m7, "4x1x4", "1554" ), \
# 			( m2, "6x1x5", "4451" ),( m5, "6x1x5", "2775" ), ( m8, "6x1x5", "1554" ), \
# 			( m3, "8x1x8", "4451" ),( m6, "8x1x8", "2775" ), ( m9, "8x1x8", "1554" ) ]:

time = np.arange( 50 )
for p in [  ( m1, "4x1x4", "4451 v/s" ),( m4, "4x1x4", "2775 v/s" ), ( m7, "4x1x4", "1554 v/s" ) ] :
			# ( m2, "6x1x5", "4451" ),( m5, "6x1x5", "2775" ), ( m8, "6x1x5", "1554" ), \
			# ( m3, "8x1x8", "4451" ),( m6, "8x1x8", "2775" ), ( m9, "8x1x8", "1554" ) ]:
	plt.plot( time, p[ 0 ], label=p[2], linewidth=4  )




# plt.plot( [ 0, 50 ], [ 0.5, 0.5 ], label="0.5" )
plt.xlabel( "n iterations" );
plt.ylabel( "mean ds" );
plt.title( "Convergence by Site Density for Ammonia Data using 88,473,600 Voxels Per Node" )
plt.legend()
plt.show()

# plt.plot( [ 88473600, 47185920, 22118400 ], [  3.343,  4.392,   5.864 ], linestyle='-', marker='o', label="1554 v/s" )
# plt.plot( [ 88473600, 47185920, 22118400 ], [  5.986,  9.294 , 12.964 ], linestyle='-', marker='o', label="2775 v/s" )
# plt.plot( [ 88473600, 47185920, 22118400 ], [ 11.774, 14.082,  24.289 ], linestyle='-', marker='o', label="4451 v/s" )

# plt.xlabel( "voxels" );
# plt.ylabel( "Time (s) until mean ds < 0.5" );
# plt.legend()
#plt.show()

# plt.savefig( "convergence.pdf", format="pdf" )
