import numpy as np
import matplotlib.pyplot as plt
import random
from matplotlib import colors
from scipy import ndimage
import sys
import random 
from random import randrange


def index3d( id, X, Y, Z ) :
    x = id % X;
    tmp = ( id - x ) / X
    y = tmp % Y;
    z  = ( tmp - y ) / Y
    return ( x, y, z )

def extractPath( idx, srcMap, sx, sy, stats ) :
	id3 = index3d( idx, 672, 672, 1  )
	px = [ id3[ 0 ] ]
	py = [ id3[ 1 ] ]
	curr = idx
	while True:
		if stats[ curr ] & 1 : # los
			px.append( sx[ srcMap[ curr ] ] )
			py.append( sy[ srcMap[ curr ] ] )
			return px, py
		sidx = srcMap[ curr ]
		sid3 = index3d( sidx, 672, 672, 1  )
		px.append( sid3[ 0 ] )
		py.append( sid3[ 1 ] )
		curr = sidx
		#print( "appending " )

plt.rcParams["figure.figsize"] = [ 5, 5 ]

plt.axis('off')
plt.tight_layout()

# minimum args:    <tesellation directory> <render output director> <dims.x> <dims.y> 
# to show contours <filepath> <format> <offset> <contour1, contour2, ..., countourn>  )

if len( sys.argv ) < 6:
	print( "Error: Minimum command line args not provide. " \
		   "Expected at least <tesellation directory> <render output director> <dims.x> <dims.y>")
	exit(1)

timestep   = sys.argv[       1 ]   ; print( timestep   )
tessPath   = sys.argv[       2 ]   ; print( tessPath   )
distPath   = sys.argv[       3 ]   ; print( distPath   )
srcMapPath = sys.argv[       4 ]   ; print( srcMapPath )
cmpPath    = sys.argv[       5 ]   ; print( cmpPath    )
sitePath   = sys.argv[       6 ]   ; print( sitePath   )
statsPath  = sys.argv[       7 ]   ; print( statsPath  )
outPath    = sys.argv[       8 ]   ; print( outPath    )
dataPath   = sys.argv[       9 ]   ; print( dataPath   )
dataFormat = sys.argv[      10 ]   ; print( dataFormat )
DX         = int( sys.argv[ 11 ] ) ; print( DX         )
DY         = int( sys.argv[ 12 ] ) ; print( DY         )

contours = []
for i in range( 13, len( sys.argv ) ):
	contours.append( sys.argv[ i ] )

statuses = np.fromfile( statsPath, dtype='uint8' ) #.reshape( ( DX, DY ) )
tess     = np.fromfile( tessPath, dtype='int32' )
comp     = np.fromfile( cmpPath, dtype='int32' ).reshape( ( DX, DY ) )
dist     = np.fromfile( distPath, dtype='f4' ).reshape( ( DX, DY ) )
sm       = np.fromfile( srcMapPath, dtype='int64' ) #.reshape( ( DX, DY ) )

blkCmpIds     = np.fromfile( "/home/v1/projects/lsrcvd/examples/workstation/autoignition/output/cmpblockids.dat",   dtype='int64' )
sortedIndices = np.fromfile( "/home/v1/projects/lsrcvd/examples/workstation/autoignition/output/sortedIndices.dat", dtype='int64' )
mass          = np.fromfile( "/home/v1/projects/lsrcvd/examples/workstation/autoignition/output/mass.dat", dtype='f8' )

bcid = np.zeros( DX*DY, dtype="int64" )
massImage = np.zeros( DX*DY )

for i in range( DX*DY ):
	index = sortedIndices[ i ]
	bcid[ index ] = blkCmpIds[ i ]
	massImage[ index ] = mass[ bcid[ index ] ]

maxDis = np.max( dist )
minDis = np.min( dist )

#print( minDis )
#print( maxDis )

mxSrc = np.max( sm )
mnSrc = np.min( sm )

# #print( mnSrc )
# #print( mxSrc )

sites = np.fromfile( sitePath, dtype='f8' )
NS = int( len( sites ) / 3 ) 

x = [ sites[ i * 3     ] * 672 - 0.5 for i in range( NS ) ]
y = [ sites[ i * 3 + 1 ] * 672 - 0.5 for i in range( NS ) ]

random.seed(10008)

###########################################################################################3

mn = np.min( comp )
mx = np.max( comp )

perm = np.arange( 0, mx+2, 1 )
random.shuffle( perm )
random.shuffle( perm )
random.shuffle( perm )
random.shuffle( perm )
random.shuffle( perm )
comp2 = np.asarray( [ perm[ t ]  for t in comp ] ).reshape( ( DX, DY ) )
# divnorm=colors.Normalize( vmin=mn, vmax=mx )

norm = colors.Normalize(vmin=mn, vmax=mx*1.3)
cmap = plt.cm.gist_ncar
m = plt.cm.ScalarMappable(norm=norm, cmap=cmap)
cmpRGB = m.to_rgba( comp2 )

plt.imshow( cmpRGB )
# plt.show()

mn = np.min( massImage )
mx = np.max( massImage )

massImage = massImage.reshape( (DX, DY) )
# plt.imshow( massImage, cmap="pink", vmax=mx*1.2 )
# plt.show()
# exit()

############################################################################################


# plt.imshow( np.log( dist, where=dist > 0 ) )
if len( contours ) > 0 :
	fieldDat = np.fromfile( dataPath, dtype=( 'f8' if ( dataFormat == "double" ) else "f4" ) )
	fieldDat = ( fieldDat[ 28 * ( 672 * 672 ) : 29 * ( 672 * 672 ) ] ).reshape( ( 672, 672 ) )
	#plt.imshow( fieldDat, cmap="YlOrBr" )
	plt.contour( fieldDat, contours, linewidths=2.0, cmap="YlOrBr" )

plt.scatter( x, y, color=( 1.0, 1.0, 1.0 ), s=4 )
plt.scatter( x, y, color=( 0.1, 0.1, 0.1 ), s=2 )

# plt.show()
plt.savefig( "./figure/step_6.png", dpi=300, transparent=True )
exit()

paths = [ randrange( 672*672-1 ) for k in range( 16 ) ]

for i in paths :
	p_x, p_y = extractPath( i, sm, x, y, statuses )
	plt.plot( p_x, p_y, color='black', linewidth=1.0 )

# plt.show();

plt.axis('off')
plt.tight_layout()

# plt.imshow( sm )
# plt.show();

mn = np.min( tess )
mx = np.max( tess )

perm = np.arange( 0, mx+2, 1 )
random.shuffle( perm )
tess = np.asarray( [ t if t < 0 else perm[ t + 1 ]  for t in tess ] ).reshape( ( DX, DY ) )
# divnorm=colors.Normalize( vmin=mn, vmax=mx )

norm = colors.Normalize(vmin=mn, vmax=mx*1.3)
cmap = plt.cm.GnBu
m = plt.cm.ScalarMappable(norm=norm, cmap=cmap)
tessRGB = m.to_rgba( tess )

statuses2d = statuses.reshape( ( DX, DY ) )

# CMP = 4
# for i in range( DX ) :
# 	for j in range( DY ) :
		# if comp[ i ][ j ] != CMP :
		# 	tessRGB[ i, j, :3 ] = 1.0,1.0,1.0
		# 	continue  
		# if( statuses2d[ i, j ] & 8 == 0 ):                              # not line of site and not resolved
		# 	if( randrange( 0, 2 ) % 2 == 0 ):
		# 		for c in range( 3 ):
		# 			tessRGB[ i, j, c ] = tessRGB[ i, j, c ] - 0.15
		# if statuses[ i, j ] & 6:
		# 	tessRGB[ i, j, :3 ] = 1.0, 0.8, 1.0 
		# if( statuses0[ i, j ] & 4 and not ( statuses0[ i, j ] & 8 ) ): # resolved not line of site				
		# 	rgba[ i, j, :3 ] = 1.0, 0.9, 0.7 			
		# if( statuses2d[ i, j ] & 16 ) :                                 # inner egdes
		# 	tessRGB[ i, j, :3 ] = 0.5, 1.0, 0.5                           
		# if( statuses2d[ i, j ] & 32 ):                                 # outerEdge
		# 	tessRGB[ i, j, :3 ] = 0.7, 0.7, 0.7
		# if( tess[ i, j ] == -1 ):  
		# 	tessRGB[ i, j, :3 ] = 0.8, 0.3, 0.4
		# if( ( statuses0[ i, j ] & 8 == 0 ) ) : #non los and is node
		# 	tessRGB[ i, j, :3 ] = 1.0, 1.0, 0.0

plt.imshow( tessRGB )

if len( contours ) > 0 :
	fieldDat = np.fromfile( dataPath, dtype=( 'f8' if ( dataFormat == "double" ) else "f4" ) )
	fieldDat = ( fieldDat[ 28 * ( 672 * 672 ) : 29 * ( 672 * 672 ) ] ).reshape( ( 672, 672 ) )
	plt.contour( fieldDat, contours, linewidths=2.5, cmap="YlOrBr" )



#testIds = [ randrange( 0, 672 * 672 - 1 ) for i in range( 30 ) ]
testIds = paths

#print( testIds )

# for i in testIds :
# 	p_x, p_y = extractPath( i, sm, x, y, statuses )
# 	plt.plot( p_x, p_y, color='black' )

plt.scatter( x, y, color=( 1.0, 1.0, 1.0 ), s=60 )
plt.scatter( x, y, color=( 0.1, 0.1, 0.1 ), s=40 )

# plt.savefig( "tess" + str( timestep ).zfill( 4 ) + ".png", dpi=300, transparent=True )

# plt.show()

