
#include "utils/IndexUtils.hpp"
#include "utils/TF.hpp"
#include "geometry/Vec.hpp"
#include "geometry/SurfaceGenerator.hpp"
#include "geometry/GeometryUtils.hpp"
#include <fbxsdk.h>

#include <cstdint>

#include <vector>
#include <fstream>
#include <iostream>
#include <chrono>
#include <unordered_map>

using namespace std::chrono;
using namespace std;

template< typename T >
void writeData( 
    const string & filePath,
    const vector< T > & data )
{
    cout << " writing data " << data.size() << endl;

    ofstream outFile( filePath, ios::out | ios::binary );

    if( ! outFile.is_open() )
    {
        cout << "file was no opened" << filePath << endl;
    }

    if( data.size() == 0 )
    {
        cout << "data size is 0 " << endl;
    }
    else
    {
        outFile.write( ( char* ) data.data(), data.size() * sizeof( T )  );
        outFile.close();
    }
}

template< typename T >
void loadData( 
    const string & filePath,
    vector< T > & data, 
    size_t offset, 
    size_t numElements )
{
    ifstream inFile( filePath, ios::in | ios::binary );
    if( ! inFile.is_open() ) {
        cout << "could not be opened for reading " << filePath << endl;
       exit( 1 );
    }
    data.resize( numElements );
    inFile.seekg( offset * sizeof( T ) );
    inFile.read( (char*) data.data(), numElements * sizeof( T ) ); 
    inFile.close();
}

int main(int argc, char** argv)
{
    // load temperature field
    // compute isosurface within bounding box, where cmpid in nb( idx )

    string outPath = "./";

    string fname1 = "/home/v1/VIDI1_MNT/combustion/turb_shear/10atm_new_grid_2/float32_downsampled/turb_shear_no_flame.6.0000E-04.field.mpi.temp.1728.640.1280.f32.dat";
    string fname2 = "/home/v1/VIDI1_MNT/combustion/turb_shear/10atm_new_grid_2/float32_downsampled/turb_shear_no_flame.6.0000E-04.field.mpi.OH.1728.640.1280.f32.dat";

    string cmpFile = "/home/v1/VIDI1_MNT/combustion/turb_shear/10atm_connected_components/componentMap.1728.640.1280.6.0000E-04.dat";

    int64_t X = 1728;
    int64_t Y = 640;
    int64_t Z = 299;
    
    const int64_t N = X*Y*Z;

    vector< float > temp;
    vector< float > OH; 
    vector< int32_t > cmpIds;
    vector< uint8_t > mask;
    vector< uint8_t > select;
       
    loadData( fname2,      OH, 0, (size_t) X*Y*Z );
    loadData( fname1,    temp, 0, (size_t) X*Y*Z );
    loadData( cmpFile, cmpIds, 0, (size_t) X*Y*Z );

    std::unordered_map< int32_t, int64_t > cmpSizes;

    for( int64_t x = 0; x < X; ++x )
    for( int64_t y = 0; y < Y; ++y )
    for( int64_t z = 0; z < Z; ++z ) {
        int64_t flat = x + y*X + z*Y*X;
        int32_t cmp = cmpIds[ flat ];
        if( ! cmpSizes.count( cmp ) ) {
            cmpSizes.insert( { cmp, 1L } );
        } else {
            cmpSizes.at( cmp ) += 1L;
        }
    }            
    
    vector< int64_t > vscales( 10 );
    for( int i = 0; i < 10; ++i ) {
        vscales[ i ] = (int64_t) std::pow( 2.0, double( i*3 + 4 ) );
    }

    for( int sidx = 0; sidx < 10; ++sidx ) {

        int64_t volumeThreshold = vscales[ sidx ];
        int64_t volumeThresholdLast = sidx == 0 ? 0 : vscales[ sidx - 1 ];

        mask   = vector< uint8_t >( X*Y*Z, 0u );
        select = vector< uint8_t >( X*Y*Z, 0u );

        #pragma omp parallel for
        for( size_t idx = 1; idx < N; ++idx ) {
            if( cmpSizes.count( cmpIds[ idx ] ) ) {
                if( cmpSizes.at( cmpIds[ idx ] ) < volumeThreshold && cmpSizes.at( cmpIds[ idx ] ) >= volumeThresholdLast ) {
                    select[ idx ]= 1u;
                }
            }
        }

        for( int64_t x = 0; x < 700; ++x )
        for( int64_t y = 0; y < Y; ++y )
        for( int64_t z = 0; z < Z; ++z ) {
            bool flag = false;
            for( int64_t xn = max( 0L, x-2 ); xn < min( X, x + 2L ); ++xn )
            for( int64_t yn = max( 0L, y-2 ); yn < min( Y, y + 2L ); ++yn )
            for( int64_t zn = max( 0L, z-2 ); zn < min( Z, z + 2L ); ++zn ) {
                int64_t flat = xn + yn*X + zn*Y*X;
                if( select[ flat ] ) {
                    flag = true;
                }
            }
            if( flag ) {
                int64_t flat = x + y*X + z*Y*X;
                mask[ flat ] = 1u;
            }
        }     

        vector< TN::Vec3< float > > surfaceVerts;
        vector< TN::Vec3< float > > surfaceNorms;    

        TN::Vec3< int > dims = { X, Y, Z };

        computeIsosurface( 
            14.75f,    
            temp, 
            mask,
            dims, 
            surfaceVerts, 
            surfaceNorms );

        double NORM = std::max( std::max( X, Y ), Z );
        std::vector< float > scalars;
        triInt(
            OH,
            X,
            Y,
            Z,
            { 
                { -( X / NORM )*.5, -( Y / NORM )*.5,-( Z / NORM )*.5 },
                {  ( X / NORM )*.5,  ( Y / NORM )*.5, ( Z / NORM )*.5 }
            },
            surfaceVerts,
            scalars );

        TN::Vec2< float > rng = { 0.00028, 0.0016 }; //TN::Sequential::getRange( scalars );

        // cout << "rng = " << rng.a() << " " << rng.b() << endl;

        FbxManager* lSdkManager = FbxManager::Create();
        FbxScene* pScene = FbxScene::Create(lSdkManager,"myScene");
        FbxNode* lMeshNode = FbxNode::Create(pScene, "meshNode");
        FbxMesh* lMesh = FbxMesh::Create(pScene, "mesh");
        lMeshNode->SetNodeAttribute(lMesh);
        FbxNode *lRootNode = pScene->GetRootNode();
        lRootNode->AddChild(lMeshNode);
        lMesh->InitControlPoints( scalars.size() );
        FbxVector4 * lControlPoints = lMesh->GetControlPoints();

        FbxLayer * lLayer = lMesh->GetLayer(0);
        if(lLayer == NULL) {
            lMesh->CreateLayer();
            lLayer = lMesh->GetLayer(0);
        }

        FbxLayer * colorLayer = lMesh->GetLayer(1);
        if(colorLayer == NULL) {
            lMesh->CreateLayer();
            colorLayer = lMesh->GetLayer(1);
        }

        FbxLayerElementNormal* lLayerElementNormal= FbxLayerElementNormal::Create(lMesh, "");
        FbxLayerElementVertexColor * vertexColorLayer = FbxLayerElementVertexColor::Create(lMesh, "");

        vertexColorLayer->SetMappingMode(FbxGeometryElement::eByControlPoint);
        vertexColorLayer->SetReferenceMode(FbxGeometryElement::eDirect);

        lLayerElementNormal->SetMappingMode(FbxGeometryElement::eByControlPoint);
        lLayerElementNormal->SetReferenceMode(FbxGeometryElement::eDirect);

        // load the TF

        std::vector< TN::Vec3< float > > TF;
        cout << "opening TF" << endl;
        TN::CM::loadTF( "./visualization_tools/visualization_system/TF/sequential/YlOrBr.txt", TF );

        float smrMin = rng.a();
        float smrMax = rng.b();        

        for( size_t k = 0; k < surfaceVerts.size() / 3; ++k )
        {
            FbxVector4 vertexA( 
                surfaceVerts[ k * 3 ].x(),
                surfaceVerts[ k * 3 ].y(),
                surfaceVerts[ k * 3 ].z() );

            FbxVector4 vertexB( 
                surfaceVerts[ k * 3 + 1 ].x(),
                surfaceVerts[ k * 3 + 1 ].y(),
                surfaceVerts[ k * 3 + 1 ].z() );

            FbxVector4 vertexC( 
                surfaceVerts[ k * 3 + 2 ].x(),
                surfaceVerts[ k * 3 + 2 ].y(),
                surfaceVerts[ k * 3 + 2 ].z() );

            lControlPoints[ k * 3 + 0 ] = vertexA;
            lControlPoints[ k * 3 + 1 ] = vertexB;
            lControlPoints[ k * 3 + 2 ] = vertexC;   

            lLayerElementNormal->GetDirectArray().Add(
                FbxVector4( 
                    surfaceNorms[ k * 3 ].x(),
                    surfaceNorms[ k * 3 ].y(),
                    surfaceNorms[ k * 3 ].z() )
            );

            lLayerElementNormal->GetDirectArray().Add(
                FbxVector4( 
                    surfaceNorms[ k * 3 + 1 ].x(),
                    surfaceNorms[ k * 3 + 1 ].y(),
                    surfaceNorms[ k * 3 + 1 ].z() )
            );

            lLayerElementNormal->GetDirectArray().Add(
                FbxVector4( 
                    surfaceNorms[ k * 3 + 2 ].x(),
                    surfaceNorms[ k * 3 + 2 ].y(),
                    surfaceNorms[ k * 3 + 2 ].z() )
            );

            {
                float r = std::min( std::max( ( scalars[ k * 3 ] - smrMin ) / ( smrMax - smrMin ), 0.f ), 1.f );
                int clIdx = std::min( (int) std::floor( r * TF.size() ), (int) TF.size() - 1 );
                TN::Vec3< float > color = TF[ clIdx ];
                vertexColorLayer->GetDirectArray().Add(
                    FbxColor( 
                        color.x(), color.y(), color.z() )
                );
            }

            {
                float r = std::min( std::max( ( scalars[ k * 3 + 1 ] - smrMin ) / ( smrMax - smrMin ), 0.f ), 1.f );
                int clIdx = std::min( (int) std::floor( r * TF.size() ), (int) TF.size() - 1 );
                TN::Vec3< float > color = TF[ clIdx ];
                vertexColorLayer->GetDirectArray().Add(
                    FbxColor( 
                        color.x(), color.y(), color.z() )
                );
            }

            {
                float r = std::min( std::max( ( scalars[ k * 3 + 2 ] - smrMin ) / ( smrMax - smrMin ), 0.f ), 1.f );
                int clIdx = std::min( (int) std::floor( r * TF.size() ), (int) TF.size() - 1 );
                TN::Vec3< float > color = TF[ clIdx ];
                vertexColorLayer->GetDirectArray().Add(
                    FbxColor( 
                        color.x(), color.y(), color.z() )
                );

                lMesh->BeginPolygon();
                lMesh->AddPolygon( k * 3 );
                lMesh->AddPolygon( k * 3 + 1 );
                lMesh->AddPolygon( k * 3 + 2 );
                lMesh->EndPolygon();                                                                
            }
        }

        lLayer->SetNormals(        lLayerElementNormal );
        colorLayer->SetVertexColors( vertexColorLayer  );

        FbxGeometryConverter lGeometryConverter( lSdkManager );
        bool s = lGeometryConverter.Triangulate( pScene, false );

        FbxExporter* lExporter = FbxExporter::Create(lSdkManager, "");
        bool lExportStatus = lExporter->Initialize( ( outPath + "componentSufrace.scaleFiltered." + to_string( volumeThreshold ) + ".fbx" ).c_str() , -1, lSdkManager->GetIOSettings() );

        if(!lExportStatus) {
            printf("Call to FbxExporter::Initialize() failed.\n");
            printf("Error returned: %s\n\n", lExporter->GetStatus().GetErrorString());
            return false;
        }

        lExporter->Export(pScene);
        lExporter->Destroy();
    }
}