#ifndef TN_CS_JSON_PARSE_HPP
#define TN_CS_JSON_PARSE_HPP

#include "input/JsonUtils.hpp"
#include <jsonhpp/json.hpp>

#include <string>
#include <iostream>
#include <vector>
#include <cstdint>
#include <fstream>
#include <sstream>
#include <set>


struct LSRCVD_Configuration {

	/* data files */

    std::array< int64_t, 3 > dims;

    std::string dataFile;
    std::string dataFormat;    
    int64_t dataOffset;

    std::string massFile;
    std::string massFormat;       
    int64_t massOffset;

    std::pair< std::string, double > stoppingCondition;
    int64_t maxIterations;

	/* algorithms parameters */

    std::vector< std::array< float, 2 > > layers;    
    double siteDensity;
    double attractionBias;

    /* output */

	std::set< std::string > VALID_OUTPUTS;

    std::string outputDirectory;
    std::set< std::string > outputs;

    /* validation and logging */

    bool validate;
    bool verbose;

	/* used for testing and producting animations, can be ignored */

	std::set< std::string > intermediateOutputs;
	bool outputCentroidalUpdates;
    bool outputLOSSteps; 
    int  maxLOSSteps;
    bool outputLOESteps; 
    int  maxLOESteps;    
    bool overrideLOE;       

    /* status */

    std::pair< bool, std::string > status;
    bool weighted;

    void fromJSON( nlohmann::json json ) {

    	status.first  = true;
    	status.second = "";

    	int nMissingRequired = 0;

    	/* required *************************************/

		nMissingRequired += ! TN::CF::getParameter( json, "dims",                  dims, status, false, {}  );
		nMissingRequired += ! TN::CF::getParameter( json, "dataFile",          dataFile, status, false, std::string( "" )  );
		nMissingRequired += ! TN::CF::getParameter( json, "dataFormat",      dataFormat, status, false, std::string( "" )  );
		nMissingRequired += ! TN::CF::getParameter( json, "layers",              layers, status, false, {}  );
		nMissingRequired += ! TN::CF::getParameter( json, "siteDensity",            siteDensity, status, false, double( 1.0 ) );
		nMissingRequired += ! TN::CF::getParameter( json, "outputDirectory",    outputDirectory, status, false, std::string( "" ) );
		nMissingRequired += ! TN::CF::getParameter( json, "maxIterations",        maxIterations, status, false, int64_t( 0 ) );		

		if( nMissingRequired )  {
			//redundant since getParameters acomplishes this but a good reminder maybe
    		status.second += "Error: missing " + std::to_string( nMissingRequired ) + "required parameters.\n";
			status.first = false; 
		}

		/* can be left to default */

    	bool found = true;

		found = TN::CF::getParameter( json, "outputs",                    outputs, status, true, { "tesselation, deltas, timers" }, VALID_OUTPUTS );		
		found = TN::CF::getParameter( json, "stoppingConditon", stoppingCondition, status, true, { "none", 0 } );
    	found = TN::CF::getParameter( json, "dataOffset", dataOffset, status, true, int64_t( 0 ) );

		found = TN::CF::getParameter( json, "validate", validate, status, true, false );	
		found = TN::CF::getParameter( json, "verbose",   verbose, status, true, false );	

    	/* required for weighted LSRCVD *****************/

    	// all or nothing
    	
    	bool hadWP = false;
    	weighted = true;

		weighted &= TN::CF::getParameter( json, "massFile", massFile, status, true, std::string( "" ) ); 
		hadWP |= weighted;
		weighted &= TN::CF::getParameter( json, "massFormat", massFormat, status, true, std::string( "" ) ); 
		hadWP |= weighted;
		weighted &= TN::CF::getParameter( json, "attractionBias", attractionBias, status, true, double( 0.0 ) ); 
		hadWP |= weighted;
		weighted &= TN::CF::getParameter( json, "massOffset", massOffset, status, true, int64_t( 0 ) );
		hadWP |= weighted;

    	if( hadWP && ( ! weighted ) ) {
    		status.second += "Warning: found parameters for weighted LSRVD, " 
    					   + std::string( "but missing others. Defaulting to unweighted.\n" );
    	}

    	/* for animations and testing, defaults to all disabled*/

		found = TN::CF::getParameter( json, "intermediateOutputs",           intermediateOutputs, status, true, {}, VALID_OUTPUTS );
		found = TN::CF::getParameter( json, "outputCentroidalUpdates",   outputCentroidalUpdates, status, true, false );	
		found = TN::CF::getParameter( json, "outputLOSSteps",                     outputLOSSteps, status, true, false );
		found = TN::CF::getParameter( json, "outputLOESteps",                     outputLOESteps, status, true, false );
		found = TN::CF::getParameter( json, "maxLOESteps",                           maxLOESteps, status, true, int32_t( -1 ) );
		found = TN::CF::getParameter( json, "maxLOSSteps",                           maxLOSSteps, status, true, int32_t( -1 ) );				
		found = TN::CF::getParameter( json, "overrideLOE",                           overrideLOE, status, true, false );
    }

    bool isValid() {
    	return status.first;
    }

    bool isWeighted() {
    	return weighted;
    }

    std::string getMessage() {
    	return status.second;
    }

    std::pair< bool, std::string > getStatus() {
    	return status;
    }

	LSRCVD_Configuration( const LSRCVD_Configuration & other ) = default;
	LSRCVD_Configuration( LSRCVD_Configuration && other ) = default;
	LSRCVD_Configuration & operator=( const LSRCVD_Configuration & other ) = default;

    LSRCVD_Configuration(): 
    	VALID_OUTPUTS( { 
			"tesselation", 
			"statuses", 
			"voxelLayers",
			"voxelComponents",
			"sitePositions", 
			"siteLayers", 	
			"siteComponents",
			"deltas",
			"timing" } ) 
	{}

    LSRCVD_Configuration( std::string path ) : 
    	VALID_OUTPUTS( { 
			"tesselation", 
			"statuses", 
			"voxelLayers",
			"voxelComponents",
			"sitePositions", 
			"siteLayers", 	
			"siteComponents",
			"deltas",
			"timing" } ) 
    {
		std::ifstream input( path );
		if( !input.is_open() )	{
			std::cerr << "Couldn't open config file: " << path << std::endl;
			exit( 1 );
		}	
		std::stringstream sstr;
		sstr << input.rdbuf();
    	fromJSON( nlohmann::json::parse( sstr.str(), nullptr, true, true ) ); 
    }    
};

#endif