
#ifndef TN_CENTROIDAL_SAMPLER_HPP
#define TN_CENTROIDAL_SAMPLER_HPP

#include <iostream>
#include <vector>
#include <cstdint>
#include <unordered_set>
#include <fstream>
#include <array>
#include <cstddef>

struct CentroidalUpdateStatistics {
    float maxDelta;
    float meanDelta;
    CentroidalUpdateStatistics( float mx, float mean ) : 
        maxDelta( mx ), 
        meanDelta( mean ) {}
};

class CentroidalSampler
{
    void freeBuffers();
    void finalizeCellMapOptimal();

    void floydWarshal();
    bool validateSiteIdContiguity();

    void setLayers(
        const std::vector< std::array< float, 2 > > & l );

protected:

    CentroidalUpdateStatistics updateSites();

    int64_t blockDecomposeComponents( 
        const int64_t blockSize,
        int64_t  * voxelComponentBlockIds_d,
        int32_t  * componentOffsetsInBlockComponentsVector_d );

    void computeComponentBlockOffsets( 
        const int64_t blockSize,
        const int64_t N_CMP_BLOCKS,
        int64_t * componentBlockVoxelOffsets_d,
        int64_t * voxelComponentBlockIds_d,
        int32_t * componentOffsetsInBlockComponentsVector_d,
        int32_t * componentIdsInBlockComponentsVector_d );

    void computeBlockComponentMass(
        const int64_t N_CMP_BLOCKS,
        const float attractionBias,
        int64_t * voxelComponentBlockIds_d,
        int64_t * componentBlockVoxelOffsets_d,
        float   * componentBlockMass_d,
        int64_t * componentBlockVoxelCounts_d );

    void sampleBlockComponents(
        const int64_t N_CMP_BLOCKS,
        const float  density,
        int32_t     * componentOffsetsInBlockComponentsVector_d,
        int32_t     * componentIdsInBlockComponentsVector_d,
        float       * componentBlockMass_d,
        int64_t     * componentBlockVoxelCounts_d,
        int64_t     * componentBlockVoxelOffsets_d );

    void computeInitialSiteDistribution(
        const float density,
        const float uniformityBias,
        const float * massFunct,
        bool saveResults = false,
        const std::string & outPath = "");

    void computeComponents();

    /******************* Host Data *********************/

    // execution settings

    bool m_validate;
    bool m_verbose;
    int64_t m_maxRES;

    // parameters

    bool m_simpleSingleSiteCMPHeuristic;
    int64_t m_nPrevUpdates;
    bool m_overrideLineOfEdge;    
    std::vector< std::array< float, 2 > > m_layers;

    // other

    int64_t M_NV;
    std::array< int64_t, 3  > M_DIMS;
    int64_t m_maxCId;
    int64_t m_NC;
    int64_t m_nSites;
    bool    m_hasBackground;

    // per-voxel

    std::vector< int16_t > m_voxelLayerIds;
    std::vector< int32_t > m_voxelSiteIds;
    std::vector< int8_t  > m_voxelStatus;    
    std::vector< int32_t > m_voxelComponentIds;
    std::vector< int64_t > m_srcMap;
    std::vector< float >   m_distances;

    // per-site

    std::vector<     float > m_sitePoints;
    std::vector<   int16_t > m_siteLayers;
    std::vector<   int32_t > m_siteComponentIds;

    std::vector< std::unordered_set< int32_t > > m_siteAdjacency;
    std::vector< float > m_siteGeodesicDistanceMatrix;
    std::vector< float > m_siteEuclideanDistanceMatrix;

    /******************* Device Data *********************/

    // per-level set

    float * layers_d;

    // per-voxel

    float   * data_d;
    int64_t * voxelIndicesSorted_d;
    int8_t  * minNeighbors_d;
    uint8_t * voxelSiteStatus_d;
    int16_t * voxelLayerIds_d;
    int32_t * voxelSiteIds_d;
    int32_t * voxelComponentIds_d;

    // per-site

    float  * sitePositions_d;
    float  * siteDeltas_d;
    int32_t * siteComponents_d;
    int16_t * siteLayers_d;
    float  * netMass_d;
    float  * centerOfMass_d;

    // per component

    int64_t * componentSiteCounts_d;

    ////////////////////////////////////////////

public:

    void computeStorageLayout( 
        std::vector< int64_t > & layerOffsets,
        std::vector< int64_t > & componentOffsets, 
        std::vector< int64_t > & layout );

    void toFile(   const std::string & directory,  bool distMatricesToo );
    void toFile(   const std::string & directory, const std::string & prefix, bool distMatricesToo );
    void fromFile( const std::string & directory,  bool distMatricesToo );

    void finalize();

    void computeSiteAdjacency();
    void computeSiteGeodesicDistances();

    virtual CentroidalUpdateStatistics geodesicCentroidalUpdate();

    void evolve( const int64_t N_UPDATES );
    void geodesicVoronoiClassification();

    void geodesicVoronoiClassification(
        const int64_t losStopStepMax,      
        const int64_t loeStopStepMax,      
        bool overrideLineOfEdge,     
        bool simpleSingleSiteCMPHeuristic,
        int64_t & losStopStepsTook,  
        int64_t & loeStopStepsTook );

    void geodesicVoronoiClassificationV2(
        const int64_t losStopStepMax,      
        const int64_t loeStopStepMax,      
        bool overrideLineOfEdge,     
        bool simpleSingleSiteCMPHeuristic,
        int64_t & losStopStepsTook,  
        int64_t & loeStopStepsTook );

    void initializeSiteDistribution( 
        float density,        
        bool saveResults = false,
        const std::string & outPath = "" );

    void computeLayerDecomposition(
        float * fieldData,
        const std::vector< std::array< float, 2 > > & layers );

    void setValidate( bool c );
    void setVerbose(  bool c );
    void setMaxRES( int64_t r );

    ///////////////////////////////////////////////////////////////
    // getters

    const std::vector< std::array< float, 2 > > & getLayers() const;

    const std::vector< int32_t >   & getClassification();
    std::vector< int64_t >           getSourceMap();    
    const std::vector< int16_t >   & getVoxelLayerIds();
    const std::vector< int8_t >    & getVoxelStatuses();
    const std::vector< int32_t >   & getVoxelComponentIds();
    const std::vector< int32_t >   & getSiteComponentIds();
    const std::vector< int16_t >   & getSiteLayers();
    std::vector< float   >           getVoxelDistances();    
    const std::vector< float >     & getSitePositions();
    int64_t getNComponents() const;
    const std::vector< float >     & getSiteEuclideanDistanceMatrix() const;
    const std::vector< float >     & getSiteGeodesicDistanceMatrix() const;
    bool hasBackgroundVoxels() const;

    ///////////////////////////////////////////////////////////////////

    virtual ~CentroidalSampler();
    CentroidalSampler( const std::array< int64_t, 3 > & _dimensions );
};

#endif