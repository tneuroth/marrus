#ifndef TN_CU_UTIL_H
#define TN_CU_UTIL_H

#include <cuda.h>
#include <stdio.h>

#include "constants.cuh"

#define gpuErrchk(ans) { gpuAssert((ans), __FILE__, __LINE__); }
inline void gpuAssert(cudaError_t code, const char *file, int line, bool abort=true)
{
   if (code != cudaSuccess) 
   {
      fprintf(stderr,"GPUassert: %s %s %d\n", cudaGetErrorString(code), file, line);
      if (abort) exit(code);
   }
}

inline int gridSize( int64_t blockSize, int64_t N ) 
{
    return static_cast<int>( ( N + blockSize - 1 ) / blockSize );
}

inline __device__ float length( const float3 a ) 
{
    return  sqrt( a.x*a.x + a.y*a.y + a.z*a.z );  
}

inline __device__ float3 operator+( const float3 a, const float3 b) 
{
    return make_float3(a.x + b.x, a.y + b.y, a.z + b.z);
}

inline __device__ float3 operator-( const float3 a, const float3 b) 
{
    return make_float3(a.x - b.x, a.y - b.y, a.z - b.z);
}

inline __device__ float3 operator*( const float a, const float3 b) 
{
    return make_float3( b.x * a, b.y * a, b.z * a );
}

inline __device__ float3 cross( const float3 a, const float3 b)
{
    return make_float3( 
        a.y * b.z - a.z * b.y, 
        a.z * b.x - a.x * b.z, 
        a.x * b.y - a.y * b.x );
}

inline __device__ float3 normalized( const float3 a ) 
{
    float len = sqrt( a.x*a.x + a.y*a.y + a.z*a.z );  
    return make_float3( a.x / len, a.y / len, a.z / len );
}

inline __device__ float dot(float3 a, float3 b)
{
    return a.x * b.x + a.y * b.y + a.z * b.z;
}

inline __device__ float3 closestPointToLine( const float3 p,  const float3 A, const float3 B  ) {
    const float3 ap = p-A;
    const float3 ab = B-A;
    return A + dot( ap, ab ) / dot( ab, ab ) * ab;
}

inline __device__ float distToLine( const float3 p,  const float3 A, const float3 B ) 
{
    return length( cross( B - A, A - p ) ) / length( B - A );
}

inline __device__ float distSquared( float3 v1, float3 v2 )
{ 
    float3 d = v1 - v2;
    return dot(d, d);
}

inline __device__
uint64_t randLCG( uint64_t & rndState )
{
    rndState = 1664525 * rndState + 1013904223;
    return rndState;
}

inline __device__
uint64_t randXOR( uint64_t & rndState )
{
    // Xorshift algorithm from George Marsaglia's paper
    rndState ^= (rndState << 13);
    rndState ^= (rndState >> 17);
    rndState ^= (rndState << 5);
    return rndState;
}

template< typename T >
inline __device__ 
void setBit( 
    int pos,
    const T mask )
{
    mask |= 1UL << (pos);
}

template< typename T >
inline __device__ 
bool checkBit( 
    int pos,
    const T mask )
{
    return ( mask >> pos ) & 1U;
}

inline __device__ 
long2 index2D( 
    const int64_t idx,
    const int64_t xDim,
    const int64_t yDim )
{
    long x = idx % xDim;
    long y = ( idx - x ) / xDim;
    return 
    { x, y };
}

inline __device__ 
int64_t flatIndex2D( 
    const int64_t x,
    const int64_t y,
    const int64_t xDim,
    const int64_t yDim )
{
     return y * xDim + x;
}

inline __device__ 
LI3 index3D( 
    const int64_t idx,
    const int64_t X,
    const int64_t Y,
    const int64_t Z )
{ 
    const long x = idx % X;
    const long tmp = ( idx - x ) / X;
    const long y = tmp % Y;
    const long z  = ( tmp - y ) / Y;

    return 
    { x, y, z };
}

inline __device__ 
LI3 index3D( 
    const int64_t idx,
    const LI3 & DIMS )
{ 
    const long x = idx % DIMS.x;
    const long tmp = ( idx - x ) / DIMS.x;
    const long y = tmp % DIMS.y;
    const long z  = ( tmp - y ) / DIMS.y;
    return 
    { x, y, z };
}

inline __device__ 
int64_t flatIndex3D( 
    const LI3 & i3,
    const LI3 & DIMS )
{
     return i3.x + ( i3.y + i3.z*DIMS.y ) * DIMS.x;
}


inline __device__ 
int64_t flatIndex3D( 
    const int64_t x,
    const int64_t y,
    const int64_t z,
    const int64_t X,
    const int64_t Y,
    const int64_t Z )
{
     return x + ( y + z*Y ) * X;
}

inline __device__ 
float location1D( 
    const int64_t x,
    const int64_t xDim )
{
    return  ( x + 0.5 ) / ( float ) ( xDim );
}

inline __device__ 
int64_t locationInverse1D( 
    const float x,
    const int64_t xDim )
{   
    return static_cast< int64_t >( 
        max( 
            min( 
                (long) floor( x * xDim ), 
                (long)( xDim - 1 ) ), 
            ( long ) 0 ) );
}

inline __device__ 
bool inBounds( 
    const LI3 & idx3,
    const LI3 & DIMS )
{
    return  idx3.x >= 0 && idx3.x < DIMS.x
        &&  idx3.y >= 0 && idx3.y < DIMS.y
        &&  idx3.z >= 0 && idx3.z < DIMS.z;
}


inline __device__ 
float3 location3D( 
    const LI3 & idx3,
    const LI3 & DIMS )
{
    return
    {
        location1D( idx3.x, DIMS.x ),
        location1D( idx3.y, DIMS.y ),
        location1D( idx3.z, DIMS.z ) 
    };             
}

inline __device__ 
float3 location3D( 
    const int64_t x,
    const int64_t y,
    const int64_t z,
    const int64_t xDim,
    const int64_t yDim,
    const int64_t zDim )
{
    return
    {
        location1D( x, xDim ),
        location1D( y, yDim ),
        location1D( z, zDim ) 
    };             
}

inline __host__ __device__ LI3 operator+( const LI3 a, const LI3 b)
{
    return { a.x + b.x, a.y + b.y, a.z + b.z };
}

inline
__device__
float voxelDist( 
    const LI3 v1, 
    const LI3 v2 )
{
    const float dx = ( v1.x - v2.x );
    const float dy = ( v1.y - v2.y );
    const float dz = ( v1.z - v2.z );
    return sqrt( dx*dx + dy*dy + dz*dz );
}

inline __device__ double atomicAddFallback( double * res, double val ) { 
    
    unsigned long long int* address = ( unsigned long long int* ) res; 
    unsigned long long int old = *address;
    unsigned long long int assumed; 
    do { 
        assumed = old; 
        old = atomicCAS( 
            address, assumed, 
            __double_as_longlong( val + __longlong_as_double( assumed ) ) ); 
    } while ( assumed != old );
    return __longlong_as_double(old); 
}


/////////////////////////////////////////////////////////////////////////////

// Thrust

/////////////////////////////////////////////////////////////////////////////

#include <thrust/device_ptr.h>


struct get_changed_site_ids
{
    const thrust::device_ptr< int64_t > idArray;
    const thrust::device_ptr< int32_t > siteMap;

    get_changed_site_ids( 
        const thrust::device_ptr< int64_t > & ids,  
        const thrust::device_ptr< int32_t > & s ) 
            : idArray( ids ), siteMap( s ) {}

    __device__
    bool operator()( const int64_t & i ) {
        return siteMap[ idArray[ i ] ];
    }
};

template< typename T >
struct get_changed_src_ids
{
    const thrust::device_ptr< int64_t > idArray;
    const thrust::device_ptr< int64_t > srcMap;

    get_changed_src_ids( 
        const thrust::device_ptr< T > & ids,  
        const thrust::device_ptr< T > & srcs ) 
            : idArray( ids ), srcMap( srcs ) {}

    __device__
    bool operator()( const T & i ) {
        return srcMap[ idArray[ i ] ];
    }
};

template< class T1, class T2, class T3 >
struct ThrustAccessorNotEqualsIndirect
{
    const thrust::device_ptr< T1 > ids;
    const thrust::device_ptr< T2 > values;
    const T2 cmp;
    const int64_t N;
    const int64_t NV;

    ThrustAccessorNotEqualsIndirect( 
        const thrust::device_ptr< T1 > & _ids,  
        const thrust::device_ptr< T2 > & _vals,                 
        const T2 & c,
        const int64_t N_IDS,
        const int64_t N_VALS ) 
            : ids( _ids ), values( _vals ), cmp( c ), N( N_IDS ), NV( N_VALS ) {}

    __device__
    bool operator()( const T3 & i )
    {
        return values[ ids[ i ] ] != cmp;
    }
};

template< class T1, class T2, class T3 >
struct ThrustAccessorEqualsIndirect
{
    const thrust::device_ptr< T1 > ids;
    const thrust::device_ptr< T2 > values;
    const T2 cmp;

    ThrustAccessorEqualsIndirect( 
        const thrust::device_ptr< T1 > & _ids,  
        const thrust::device_ptr< T2 > & _vals,                 
        const T2 & c ) 
            : ids( _ids ), values( _vals ), cmp( c ) {}

    __device__
    bool operator()( const T3 & i )
    {
        return values[ ids[ i ] ] == cmp;
    }
};

template< class TV, class TI >
struct ThrustAccessorEquals
{
    const thrust::device_ptr< TV > vals;
    const TV cmp;
    ThrustAccessorEquals( const thrust::device_ptr< TV > & v, const TV & c ) : vals( v ), cmp( c ) {}

    __device__
    bool operator()( const TI & i )
    {
        return *( vals + i ) == cmp;
    }
};

struct set_bit_indirect {
    thrust::device_ptr< uint8_t > bitfields;
    const uint8_t flag; 
    const bool newState;
    set_bit_indirect( 
        const thrust::device_ptr< uint8_t > & bm, 
        const uint8_t f, 
        const bool ns ) 
        : bitfields( bm ), flag( f ), newState( ns ) {}
    __device__
    void operator()( int64_t & i ) 
    {
        if( newState == true ) {
            bitfields[ i ] = bitfields[ i ] & ( ~flag );
        } else {
            bitfields[ i ] = bitfields[ i ] | flag;
        }
    }
};

struct check_both_or_not_both_indirect
{
    const thrust::device_ptr< uint8_t > bitfields;
    const uint8_t flag1; 
    const uint8_t flag2;     
    check_both_or_not_both_indirect( const thrust::device_ptr< uint8_t > & bm, uint8_t f1, uint8_t f2 ) 
        : bitfields( bm ), flag1( f1 ), flag2( f2 )  {}
    __device__
    bool operator()( const int64_t & i ) {
        const uint8_t b = bitfields[ i ];
        const bool v1 = ( bitfields[ i ] & flag1 );
        const bool v2 = ( bitfields[ i ] & flag2 );        
        return ( v1 && v2 ) || ( ! ( v1 || v2 ) );
    }
};

struct check_bit_indirect
{
    const thrust::device_ptr< uint8_t > bitfields;
    const uint8_t flag; 
    const bool    cmp;     
    check_bit_indirect( const thrust::device_ptr< uint8_t > & bm, uint8_t f, bool v ) 
        : bitfields( bm ), flag( f ), cmp( v ) {}
    __device__
    bool operator()( const int64_t & i ) {
        const bool isSet = static_cast< bool >( bitfields[ i ] & flag );
        return cmp == true ? isSet : ! isSet;
    }
};

template<typename T >
struct key_indirect {

    const thrust::device_ptr< T > keys;

    key_indirect( 
        const thrust::device_ptr< T > & k ) : keys( k ) {}

    __device__
    float operator()( const int64_t & i ) const {
        return keys[ i ];
    }
};

struct value_if_indirect {

    const thrust::device_ptr< uint8_t > bitfields;
    const thrust::device_ptr< float >   values;

    const uint8_t flag;                         
    const bool cmp;                         
    const float def_val;
    
    value_if_indirect( 
        const thrust::device_ptr< uint8_t > & bm, 
        const thrust::device_ptr< float   > & vals, 
        uint8_t f, 
        bool v, 
        float defVal ) 
        : bitfields( bm ), values( vals ), flag( f ), cmp( v ), def_val( defVal ) {}

    __device__
    float operator()( const int64_t & i ) const {
        bool isSet = ( bitfields[ i ] & flag ) != 0;
        return ( cmp == isSet ) ? values[ i ] : def_val;
    }
};

struct is_even_indirect
{
    const thrust::device_ptr< int8_t > vals;
    is_even_indirect( const thrust::device_ptr< int8_t > & v ) : vals( v ) {}

    __device__
    bool operator()( const int64_t & i )
    {
        return !( ( *( vals + i ) ) & 1 );
    }
};

struct is_even
{
    __host__ __device__
    bool operator()( const int8_t & i )
    {
        return !( i & 1 );
    }
};

struct is_odd
{
    __host__ __device__
    bool operator()( const int8_t & i )
    {
        return i & 1;
    }
};

template<typename T1, typename T2>
struct ThrustTypeCastIndirection
{
    const thrust::device_ptr< T2 > level2;
    ThrustTypeCastIndirection( const thrust::device_ptr< T2 > & l2 ) : level2( l2 ) {}

    __host__ __device__ T2 operator()( const T1 & i ) const
    {
        return level2[ i ];
    }
};

#endif