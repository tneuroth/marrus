#ifndef TN_LSRCVT_DATA_HPP
#define TN_LSRCVT_DATA_HPP

#include <string>
#include <array>
#include <vector>
#include <cstdint>

namespace Marrus { 

class LsrcvtData
{
    int64_t M_NV;
    std::array< int64_t, 3  > M_DIMS;
    std::vector< int16_t >                m_siteLayers;                  
    std::vector< float >                  m_sitePoints;                  
    std::vector< int32_t >                m_siteComponentIds;            
    std::vector< float >                  m_siteGeodesicDistanceMatrix;  
    std::vector< float >                  m_siteEuclideanDistanceMatrix; 
    std::vector< int32_t >                m_voxelSiteIds;                
    std::vector< int32_t >                m_voxelComponentIds;           
    std::vector< int16_t >                m_voxelLayerIds;               
    int64_t                               m_nSites;                      
    int64_t                               m_NC;                          
    int64_t                               m_numLayers;  
    bool                                  m_hasBackground;
    std::vector< std::array< float, 2 > > m_layers;  

public:

    LsrcvtData() 
    {
        m_nSites=0;                      
        m_NC=0;
        m_hasBackground=false; 
    }

	~LsrcvtData() {}

    void fromFile( const std::string & directory ) 
    {
        std::vector< int64_t > tmp( 7 );

        loadData( 
            directory + "n_lyr_comp_sites_x_y_z.bin", 
            tmp,
            0,
            static_cast< int64_t >( tmp.size() )
        );

        const int64_t NL = tmp[ 0 ];
        m_NC             = tmp[ 1 ];
        m_nSites         = tmp[ 2 ];
        M_DIMS[ 0 ]      = tmp[ 3 ];
        M_DIMS[ 1 ]      = tmp[ 4 ];
        M_DIMS[ 2 ]      = tmp[ 5 ];
        M_NV = M_DIMS[ 0 ] * M_DIMS[ 1 ] * M_DIMS[ 2 ];
        m_hasBackground = static_cast< bool >( tmp[ 6 ] );

        std::vector< double > tmpSites( m_nSites*3 );
        loadData( directory + "sitePoints.bin", tmp, 0, m_nSites*3 );
        m_sitePoints.resize( m_nSites*3 );
        for( size_t i = 0, end = m_nSites*3; i < end; ++i ) m_sitePoints[ i ] = ( float ) tmpSites[ i ];

        loadData( directory + "siteLayers.bin"       , m_siteLayers       , 0, m_nSites );
        loadData( directory + "siteComponentIds.bin" , m_siteComponentIds , 0, m_nSites );
        loadData( directory + "voxelSiteIds.bin"     , m_voxelSiteIds     , 0, M_NV );
        loadData( directory + "voxelLayerIds.bin"    , m_voxelLayerIds    , 0, M_NV );
        loadData( directory + "voxelComponentIds.bin", m_voxelComponentIds, 0, M_NV );

        if( false ) {
            loadData( directory + "m_siteGeodesicDistanceMatrix.bin",     m_siteGeodesicDistanceMatrix, 0, m_nSites*m_nSites  );
            loadData( directory + "m_siteEuclideanDistanceMatrix.bin",   m_siteEuclideanDistanceMatrix, 0, m_nSites*m_nSites  );
        }

        // layers needs to be converted to guarantee contiguous form first
        std::vector< float > ly;
        loadData( directory + "layers.bin", ly, 0, NL*2 );
        m_layers.resize( NL );
        for( size_t i = 0; i < NL; ++i ) {
            m_layers[ i ][ 0 ] = ly[ i*2     ];
            m_layers[ i ][ 1 ] = ly[ i*2 + 1 ];
        }
    }

	std::vector< int16_t > & getSiteLayers()                        { return m_siteLayers;                  }
	std::vector< float >   & getSitePositions()                     { return m_sitePoints;                  } 
	std::vector< int32_t > & getSiteComponentIds()                  { return m_siteComponentIds;            }
	std::vector< float >   & getSiteGeodesicDistanceMatrix()        { return m_siteGeodesicDistanceMatrix;  }
	std::vector< float >   & getSiteEuclideanDistanceMatrix()       { return m_siteEuclideanDistanceMatrix; }
	std::vector< int32_t > & getClassification()                    { return m_voxelSiteIds;                }
	std::vector< int32_t > & getVoxelComponentIds()                 { return m_voxelComponentIds;           }
	std::vector< int16_t > & getVoxelLayerIds()                     { return m_voxelLayerIds;               }
	int64_t                  getNSites()                      const { return m_nSites;                      }
	int64_t                  getNComponents()                 const { return m_NC;                          }
	int64_t                  getNLayers()                     const { return m_layers.size();               }    
	const std::vector< std::array< float, 2 > > & getLayers() const { return m_layers;                      }        
	bool                     hasBackground()                  const { return m_hasBackground;               }
};

}

#endif

