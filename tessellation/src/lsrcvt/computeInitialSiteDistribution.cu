#include "lsrcvt_lib.hpp"
#include "wlsrcvt_lib.hpp"
#include "utility.cuh"

#include "algorithms/Standard/MyAlgorithms.hpp"

#include <thrust/reduce.h>
#include <thrust/execution_policy.h>
#include <thrust/device_ptr.h>
#include <thrust/iterator/discard_iterator.h>
#include <thrust/functional.h>
#include <thrust/transform_scan.h>
#include <thrust/device_vector.h>
#include <thrust/device_ptr.h>
#include <thrust/iterator/permutation_iterator.h>
#include <thrust/partition.h>
#include <thrust/iterator/constant_iterator.h>
#include <thrust/unique.h>

#include <cuda.h>

#include <cstddef>
#include <cstdint>
#include <chrono>
#include <set>

using namespace std::chrono;

__global__
void applyMassFunction(
    float * mfield,
    float attractionBias ) 
{
    const int64_t idx = blockIdx.x * blockDim.x + threadIdx.x;
    if( idx >= D_NV ) { return; }
    mfield[ idx ] = pow( mfield[ idx ], attractionBias );
}

__global__
void mapBlocks(
    const int64_t BX,
    const int64_t BY,
    const int64_t BZ,
    const int64_t blockSize,
    const int32_t * cmpIds,
    int64_t * cmpBlkId,
    int64_t * indices )
{
    const int64_t idx = blockIdx.x * blockDim.x + threadIdx.x;
    if( idx >= D_NV ) { return; }


    indices[  idx ] = idx;
    const int64_t cmpId = cmpIds[ idx ];

    const int64_t NBLOCKS = BX * BY * BZ;
    const LI3 idx3d = index3D( idx, D_DIMS );

    const int64_t flatBlkId = flatIndex3D(
        min( static_cast< int64_t >( idx3d.x / blockSize ), BX - 1 ),
        min( static_cast< int64_t >( idx3d.y / blockSize ), BY - 1 ),
        min( static_cast< int64_t >( idx3d.z / blockSize ), BZ - 1 ),
        BX,
        BY,
        BZ );

    cmpBlkId[ idx ] = cmpId * NBLOCKS + flatBlkId;
}

__global__
void flagTransitions(
    const int64_t * componentBlockIds,
    uint8_t        * componentBlockTransitions )
{
    const int64_t idx = blockIdx.x * blockDim.x + threadIdx.x;

    if( idx >= D_NV ) { return; }


    if( idx == 0 )
    {
        componentBlockTransitions[ idx ] = 1;
        return;
    }

    componentBlockTransitions[ idx ] = 0;

    const int64_t cmpBlkId   = componentBlockIds[ idx     ];
    const int64_t cmpBlkId_l = componentBlockIds[ idx - 1 ];

    if( cmpBlkId != cmpBlkId_l )
    {
        componentBlockTransitions[ idx ] = 1;
    }
}


__global__
void getComponentBlockMass(
    const int64_t     NBC,    
    const float       attractionBias,
    const int64_t   * voxelComponentBlockIds,
    const int32_t   * voxelComponentIds,
    const float     * massFunct,
    const int64_t   * sortedVoxelIndices,
    const int64_t   * componentBlockVoxelOffsets,
    float          * componentBlockMass,
    int64_t         * componentBlockVoxelCounts )
{
    const int64_t idx = blockIdx.x * blockDim.x + threadIdx.x;
    if( idx >= NBC ) { return; }

    const int64_t START = componentBlockVoxelOffsets[ idx ];
    const int64_t END   = ( idx == NBC - 1 ) ? D_NV : componentBlockVoxelOffsets[ idx + 1 ];

    for( int64_t i = START; i < END; ++i ) {
        const int64_t vIdx  = sortedVoxelIndices[ i ];
        const int64_t cmpId = voxelComponentIds[ vIdx ];

        if( cmpId <= 0 ) {
            continue;
        }

        const int64_t vCBID = voxelComponentBlockIds[ i ];
        componentBlockMass[ vCBID ] += massFunct[ vIdx ];
        componentBlockVoxelCounts[ vCBID ] += 1;
    }
}

__global__
void getComponentBlockMass(
    const int64_t NBC,    
    const int64_t   * voxelComponentBlockIds,
    const int32_t   * voxelComponentIds,
    const int64_t   * sortedVoxelIndices,
    const int64_t   * componentBlockVoxelOffsets,
    float  * componentBlockMass,
    int64_t * componentBlockVoxelCounts )
{
    const int64_t idx = blockIdx.x * blockDim.x + threadIdx.x;

    if( idx >= NBC )
    {
        return;
    }

    const int64_t START = componentBlockVoxelOffsets[ idx ];
    const int64_t END   = ( idx == NBC - 1 ) ? D_NV : componentBlockVoxelOffsets[ idx + 1 ];

    for( int64_t i = START; i < END; ++i ) {
        const int64_t vIdx  = sortedVoxelIndices[ i ];
        const int64_t cmpId = voxelComponentIds[ vIdx ];
        
        if( cmpId <= 0 ) {
            continue;
        }

        const int64_t vCBID = voxelComponentBlockIds[ i ];
        componentBlockMass[ vCBID ] += 1.0;
        componentBlockVoxelCounts[ vCBID ] += 1;
    }
}


__global__
void getComponentMass(
    const int64_t     NC,
    const int64_t     NBC,    
    const int32_t   * cmpOffsets, 
    const float     * cmpBlkMass,
    const int64_t   * cmpBlkCounts,    
    float           * componentMass,
    int64_t         * componentVoxelCounts )
{
    const int64_t idx = blockIdx.x * blockDim.x + threadIdx.x;

    if( idx >= NC )
    {
        return;
    }

    const int64_t START = cmpOffsets[ idx ];
    const int64_t END   = ( idx == NC - 1 ) ? NBC : cmpOffsets[ idx + 1 ];

    int64_t s = 0;
    for( int64_t i = START; i < END; ++i )
    {
        // if( i < 0 || i >= NBC )
        // {
        //     printf( "E4" );
        // }

        componentMass[ idx ]        += cmpBlkMass[   i ];
        componentVoxelCounts[ idx ] += cmpBlkCounts[ i ];
        ++s;
    }

    // if( s == 0 )
    // {
    //     printf( "ER %d", idx );
    // }
}

__global__
void getComponentSiteCounts(
    const int64_t     NC,
    const float      siteDensity,
    const float    * componentMass,
    const int64_t   * componentVoxelCounts,
    int64_t         * siteCounts )
{
    const int64_t idx = blockIdx.x * blockDim.x + threadIdx.x;
    if( idx >= NC ) { return; }

    const int64_t COMPONENT_VOXEL_COUNT = componentVoxelCounts[ idx ];

    if( componentVoxelCounts[ idx ] == 0 ) {
        siteCounts[ idx ] = 0;
        return;
    }

    // at most one site per voxel,
    // and at least one site per component
    siteCounts[ idx ] = min(
        max( (int64_t) ceil( componentMass[ idx ] * siteDensity ), (int64_t) 1 ),
        COMPONENT_VOXEL_COUNT );
}


__global__
void chooseCmpBlkCounts(
    const int64_t     NC,
    const int64_t     NCB,
    const float     * componentBlockMassFractions,
    const int64_t   * componentSiteCounts,
    const int32_t   * componentOffsetsInComponentBlockVector,
    float           * representation,
    int64_t         * indices,
    int64_t         * componentBlockSiteCounts )
{
    const int64_t idx = blockIdx.x * blockDim.x + threadIdx.x;

    if( idx >= NC ) // the first component is the empty component
    {
        return;
    }

    const int64_t componentBlockStart = componentOffsetsInComponentBlockVector[ idx ];
    const int64_t componentBlockEnd   = ( idx == NC - 1 )
                                        ? NCB
                                        : componentOffsetsInComponentBlockVector[ idx + 1 ];

    // if( componentBlockStart < 0 || componentBlockEnd > NCB )
    // {
    //     printf( "ERCMPB %d, ", idx );
    // }

    int64_t nComponentBlocks = componentBlockEnd - componentBlockStart;
    const int64_t nSites = componentSiteCounts[ idx ];

    if( nSites == 0 )
    {
        // printf( "ZS %d ", idx );
        for( int64_t i = componentBlockStart; i < componentBlockEnd; ++i )
        {
            // printf( "%d ", i );
            componentBlockSiteCounts[ i ] = 0;
            representation[ i ] = 1;
            indices[ i ] = i;
        }
        return;
    }
    else
    {
        int64_t nDistributed = 0;

        // for each block in the component get the number of sites it wholy 'deserves'
        for( int64_t i = componentBlockStart; i < componentBlockEnd; ++i )
        {
            componentBlockSiteCounts[ i ]  = floor( componentBlockMassFractions[ i ] * nSites );
            nDistributed += componentBlockSiteCounts[ i ];
            // lower representation means that the amount the block is more under-represented
            representation[ i ] = 1.0 - ( ( componentBlockMassFractions[ i ] * nSites )
                                          - componentBlockSiteCounts[ i ] );
            indices[ i ] = i;
        }

        // how many we wanted vs how many we got
        const int64_t diff = nSites - nDistributed;

        // if it wasn't exact
        if( diff > 0 )
        {
            // sort the blocks in the component by their representation, so that ones needing more
            // representation are first
            thrust::sort_by_key(
                thrust::seq,
                thrust::device_pointer_cast( representation ) + componentBlockStart,
                thrust::device_pointer_cast( representation ) + componentBlockEnd,
                thrust::device_pointer_cast( indices        ) + componentBlockStart );

            // distribute the left over sites to the most under-represented blocks
            for( int64_t k = componentBlockStart; k < componentBlockStart + diff; ++k )
            {
                // if( k < componentBlockStart || k >= componentBlockEnd )
                // {
                //     printf( "kce diff=%d, ", diff );
                // }

                // if( indices[ k ] < componentBlockStart || indices[ k ] >= componentBlockEnd )
                // {
                //     printf( "ikce diff=%d, ", diff );
                // }

                ++componentBlockSiteCounts[ indices[ k ] ];
                ++nDistributed;
            }
        }
    }
}

__global__
void convertMassToFraction(
    const int64_t NBC,
    const bool hasBackground,
    const int64_t * sortedVoxelIndices,
    const int32_t * voxelComponentIds,
    const int64_t * componentBlockOffset,
    const float   * componentMass,
    float         * componentBlockMass )
{
    const int64_t idx = blockIdx.x * blockDim.x + threadIdx.x;
    if( idx >= NBC )
    {
        return;
    }

    const int64_t vIdx0   = componentBlockOffset[ idx ];

    // if there is no background, then the component ids start at 1 in the array
    // so convert to 0 based indexing
    const int32_t cmpIdx  = ( ! hasBackground ) ? voxelComponentIds[ sortedVoxelIndices[ vIdx0 ] ] - 1 
                            : voxelComponentIds[ sortedVoxelIndices[ vIdx0 ] ];

    // if( vIdx0 < 0 || vIdx0 >= D_NV )
    // {
    //     printf( "vidx oor %d, ", idx );
    // }

    // background
    if( hasBackground && cmpIdx == 0 )
    {
        componentBlockMass[ idx ] = 0;
        return;
    }

    const float cmpMass = componentMass[ cmpIdx ];

    // if( cmpMass <= 0.0 )
    // {
    //     // something wrong
    //     printf( "err%d ", cmpIdx );
    //     return;
    // }

    // if( ! isfinite( cmpMass ) ||  ! isfinite( componentBlockMass[ idx ]  ) )
    // {
    //     printf( "inf %d ", cmpIdx );
    // }

    // if( ! isfinite( componentBlockMass[ idx ] / cmpMass ) )
    // {
    //     printf( "inf ci %d\n", cmpIdx  );
    //     printf( "inf bci %d\n", idx  );
    //     printf( "inf mbc %f\n", componentBlockMass[ idx ]  );
    //     printf( "inf mc %f\n", cmpMass  );       
    //     float r = componentBlockMass[ idx ] / cmpMass;
    //     printf( "inf mbc/mc %f\n", r  );             

    // }  

    componentBlockMass[ idx ] /= cmpMass;
}

__global__
void sampleComponentsBlocks(
    const int64_t   NBC,
    const int64_t * componentBlockVoxelOffsets,
    const int64_t * componentBlockSiteCounts,
    uint8_t       * voxelSiteInitial )
{
    const int64_t idx = blockIdx.x * blockDim.x + threadIdx.x;
    if( idx >= NBC )
    {
        return;
    }

    const int64_t START = componentBlockVoxelOffsets[ idx ];
    const int64_t END   = ( idx == NBC - 1 ) ? D_NV : componentBlockVoxelOffsets[ idx + 1 ];
    const int64_t NVC   = END - START;

    const int64_t NS = componentBlockSiteCounts[ idx ];
    int64_t nSites = 0;

    uint64_t rndSeed = static_cast<uint64_t>( idx + 314697 );
    while( nSites < NS )
    {
        int64_t rIdx = START + static_cast< int64_t >( randXOR( rndSeed ) % NVC );
        if( voxelSiteInitial[ rIdx ] == 0 )
        {
            voxelSiteInitial[ rIdx ] = 1;
            ++nSites;
        }
    }
}

__global__
void initializeComponentBlockSites(
    const int64_t NBC,
    const int64_t   * sortedIndices,
    const int64_t   * componentBlockVoxelOffsets,
    const int64_t   * componentBlockSiteOffsets,
    const int32_t   * voxelComponents,
    const int16_t   * voxelLayerIds,
    const uint8_t   * voxelSiteInitial,
    int32_t         * siteComponents,
    int16_t         * siteLayers,
    float           * sites )
{
    const int64_t idx = blockIdx.x * blockDim.x + threadIdx.x;

    if( idx >= NBC )
    {
        return;
    }

    const int64_t START = componentBlockVoxelOffsets[ idx ];
    const int64_t END   = ( idx == NBC - 1 ) ? D_NV : componentBlockVoxelOffsets[ idx + 1 ];
    const int64_t siteOffset = componentBlockSiteOffsets[ idx ];

    int64_t count = 0;
    for( int64_t i = START; i < END; ++i )
    {
        if( voxelSiteInitial[ i ] > 0 )
        {
            int64_t vIdx = sortedIndices[ i ];

            LI3 idx3d =
                index3D(
                    vIdx,
                    D_DIMS.x,
                    D_DIMS.y,
                    D_DIMS.z );

            float3 location = location3D(
                                  idx3d.x,
                                  idx3d.y,
                                  idx3d.z,
                                  D_DIMS.x,
                                  D_DIMS.y,
                                  D_DIMS.z );

            sites[ 3 * ( siteOffset + count )     ] = location.x;
            sites[ 3 * ( siteOffset + count ) + 1 ] = location.y;
            sites[ 3 * ( siteOffset + count ) + 2 ] = location.z;

            siteComponents[ siteOffset + count ] = voxelComponents[ vIdx ];
            siteLayers[     siteOffset + count ] = voxelLayerIds[   vIdx ];

            ++count;
        }
    }
}

__global__
void getComponentIdsInComponentBlockIdVector(
    const int64_t   N_COMPONENT_BLOCKS,
    const int64_t * sortedVoxelIndices,
    const int64_t * componentBlockVoxelOffsets,
    const int32_t * voxelComponentIds,
    int32_t       * componentIds )
{
    const int64_t idx = blockIdx.x * blockDim.x + threadIdx.x;
    if( idx >= N_COMPONENT_BLOCKS )
    {
        return;
    }

    componentIds[ idx ] =
        voxelComponentIds[ sortedVoxelIndices[ componentBlockVoxelOffsets[ idx ] ] ];
}

template<typename T1, typename T2>
struct ThrustTypeCast
{
    __host__ __device__ T2 operator()( const T1 & v ) const
    {
        return static_cast< T2 >( v );
    }
};

/////////////////////////////////////////////////////////

int64_t CentroidalSampler::blockDecomposeComponents( 
    const int64_t blockSize,
    int64_t * voxelComponentBlockIds_d,
    int32_t * componentOffsetsInBlockComponentsVector_d )
{

    /*************************************************************************************/
    // create a course cartesian grid //////////////////////////

    std::vector< int64_t > blockDims = {
        std::max( (int ) std::floor( M_DIMS[ 0 ] / ( float ) blockSize ), 1 ),
        std::max( (int ) std::floor( M_DIMS[ 1 ] / ( float ) blockSize ), 1 ),
        std::max( (int ) std::floor( M_DIMS[ 2 ] / ( float ) blockSize ), 1 )
    };

    int64_t nBlocks = blockDims[ 0 ] * blockDims[ 1 ] * blockDims[ 2 ];

    /************************************************************************************/

    gpuErrchk( cudaPeekAtLastError() );
    gpuErrchk( cudaDeviceSynchronize() );

    /*************************************************************************************/

    // cc returns component id 0 for backgroundm, so if there is backgrond,
    // then 0, 1, 2, 3, ... numer of components  ( including 0 ); 
    // Otherwise 1, 2, 3, 4, 5 ... number of components ;

    m_maxCId = thrust::reduce(
        thrust::device_pointer_cast( voxelComponentIds_d ),
        thrust::device_pointer_cast( voxelComponentIds_d ) + M_NV,
        0,
        thrust::maximum< int32_t >() );                


    m_hasBackground = !( m_maxCId == m_NC );

    /*************************************************************************************/
    // compute an id in the space of components X blocks, for each voxel and initialize the
    // indicies vector

    if( m_verbose ) {
        std::cout << "blockSize = " << blockSize << std::endl;
    }

    mapBlocks<<< gridSize( VX_THREADS_PER_BLOCK, M_NV ), VX_THREADS_PER_BLOCK >>>(
        blockDims[ 0 ],
        blockDims[ 1 ],
        blockDims[ 2 ],
        blockSize,
        voxelComponentIds_d,
        voxelComponentBlockIds_d,
        voxelIndicesSorted_d );

    gpuErrchk( cudaPeekAtLastError() );
    gpuErrchk( cudaDeviceSynchronize() );

    /***********************************************************************/
    // sort the component/block ids and get the permutation

    thrust::sort_by_key(
        thrust::device_pointer_cast( voxelComponentBlockIds_d ),
        thrust::device_pointer_cast( voxelComponentBlockIds_d ) + M_NV,
        thrust::device_pointer_cast( voxelIndicesSorted_d     ) );

    gpuErrchk( cudaPeekAtLastError() );
    gpuErrchk( cudaDeviceSynchronize() );

    /************************************************************************/
    // flag where the ids change to get the number of block components ///////

    flagTransitions<<< gridSize( VX_THREADS_PER_BLOCK, M_NV ), VX_THREADS_PER_BLOCK >>>(
        voxelComponentBlockIds_d,
        voxelSiteStatus_d );

    gpuErrchk( cudaPeekAtLastError() );
    gpuErrchk( cudaDeviceSynchronize() );

    const int64_t N_CMP_BLOCKS = thrust::count(
        thrust::device_pointer_cast( voxelSiteStatus_d ),
        thrust::device_pointer_cast( voxelSiteStatus_d ) + M_NV,
        1 ); // count number of 1s

    gpuErrchk( cudaPeekAtLastError() );
    gpuErrchk( cudaDeviceSynchronize() );

    /**************************************************************************/
    // reduce the voxels component block ids to be incremental 0,1,2,3 ... /////

    // 1000010010 ->
    // 1111122233 ->
    // 0000011122

    thrust::transform_inclusive_scan(
        thrust::device_pointer_cast( voxelSiteStatus_d ), 
        thrust::device_pointer_cast( voxelSiteStatus_d ) + M_NV,
        thrust::device_pointer_cast( voxelComponentBlockIds_d ), 
        ThrustTypeCast<uint8_t,int64_t>(),
        thrust::plus<int64_t>() );

    // subtract 1 for 0-based indexing

    thrust::transform(
        thrust::device_pointer_cast( voxelComponentBlockIds_d ),
        thrust::device_pointer_cast( voxelComponentBlockIds_d ) + M_NV,
        thrust::make_constant_iterator( 1 ),
        thrust::device_pointer_cast( voxelComponentBlockIds_d ),
        thrust::minus< int64_t >() );

    gpuErrchk( cudaPeekAtLastError() );
    gpuErrchk( cudaDeviceSynchronize() );

    return N_CMP_BLOCKS;
}

void CentroidalSampler::computeComponentBlockOffsets( 
    const int64_t blockSize,
    const int64_t N_CMP_BLOCKS,
    int64_t * componentBlockVoxelOffsets_d,
    int64_t * voxelComponentBlockIds_d,
    int32_t * componentOffsetsInBlockComponentsVector_d,
    int32_t * componentIdsInBlockComponentsVector_d )
{
    /************************************************************************/
    // compute the offsets ///////////////////////////////////////////////////

    thrust::unique_by_key_copy(
        thrust::device_pointer_cast( voxelComponentBlockIds_d ),
        thrust::device_pointer_cast( voxelComponentBlockIds_d ) + M_NV,
        thrust::make_counting_iterator(0),
        thrust::make_discard_iterator(),
        thrust::device_pointer_cast( componentBlockVoxelOffsets_d ) );

    gpuErrchk( cudaPeekAtLastError() );
    gpuErrchk( cudaDeviceSynchronize() );

    //************************************************************************************
    // for each component, get the index in block component vector where the component begins

    getComponentIdsInComponentBlockIdVector<<< gridSize( CM_THREADS_PER_BLOCK, N_CMP_BLOCKS ), CM_THREADS_PER_BLOCK >>>(
        N_CMP_BLOCKS,
        voxelIndicesSorted_d,
        componentBlockVoxelOffsets_d,
        voxelComponentIds_d,
        componentIdsInBlockComponentsVector_d );

    gpuErrchk( cudaPeekAtLastError() );
    gpuErrchk( cudaDeviceSynchronize() );

    //************************************************************************************
    // get component offsets in component block vector //////////////////////////////////

    thrust::unique_by_key_copy(
        thrust::device_pointer_cast( componentIdsInBlockComponentsVector_d ),
        thrust::device_pointer_cast( componentIdsInBlockComponentsVector_d ) + N_CMP_BLOCKS,
        thrust::make_counting_iterator(0),
        thrust::make_discard_iterator(),
        thrust::device_pointer_cast( componentOffsetsInBlockComponentsVector_d ) );

    gpuErrchk( cudaPeekAtLastError() );
    gpuErrchk( cudaDeviceSynchronize() );
}

void CentroidalSampler::sampleBlockComponents(
    const int64_t N_CMP_BLOCKS,
    const float   density,
    int32_t     * componentOffsetsInBlockComponentsVector_d,
    int32_t     * componentIdsInBlockComponentsVector_d,
    float       * componentBlockMass_d,
    int64_t     * componentBlockVoxelCounts_d,
    int64_t     * componentBlockVoxelOffsets_d ) 
{   
    // temporary

    int64_t * componentVoxelCounts_d;
    cudaMalloc( (void **) & componentVoxelCounts_d, m_NC * sizeof( int64_t ) );
    cudaMemset( componentVoxelCounts_d, 0, m_NC * sizeof( int64_t ) );

    float * componentMass_d;
    cudaMalloc( (void **) & componentMass_d,                     m_NC * sizeof( float ) );
    cudaMemset( componentMass_d, 0, m_NC * sizeof( float ) );

    float    * componentBlockRepresentationd_d = 0;
    int64_t   * componentBlockIndices_d         = 0;
    int64_t   * componentBlockSiteCounts_d      = 0; 
    int64_t   * componentBlockSiteOffsets_d     = 0;

    /////////////////////////////////////////////////////////////////////////////////////

    getComponentMass<<< gridSize( CM_THREADS_PER_BLOCK, m_NC ), CM_THREADS_PER_BLOCK >>>(
        m_NC,
        N_CMP_BLOCKS,         
        componentOffsetsInBlockComponentsVector_d, 
        componentBlockMass_d,
        componentBlockVoxelCounts_d,    
        componentMass_d,
        componentVoxelCounts_d );

    gpuErrchk( cudaPeekAtLastError() );
    gpuErrchk( cudaDeviceSynchronize() );

    /*****************************************************************************/
    // compute site counts for the components

    getComponentSiteCounts<<< gridSize( CM_THREADS_PER_BLOCK, m_NC ), CM_THREADS_PER_BLOCK >>>(
        m_NC,
        density,
        componentMass_d,
        componentVoxelCounts_d,
        componentSiteCounts_d );

    gpuErrchk( cudaPeekAtLastError() );
    gpuErrchk( cudaDeviceSynchronize() );

    /****************************************************************************/
    // get the total numnber of sites over all components

    m_nSites = thrust::reduce(
        thrust::device_pointer_cast( componentSiteCounts_d ),
        thrust::device_pointer_cast( componentSiteCounts_d ) + m_NC );

    gpuErrchk( cudaPeekAtLastError() );
    gpuErrchk( cudaDeviceSynchronize() );

    /****************************************************************************/
    // get fraction of component mass in each component block

    convertMassToFraction<<< gridSize( CM_THREADS_PER_BLOCK, N_CMP_BLOCKS ), CM_THREADS_PER_BLOCK >>>(
        N_CMP_BLOCKS,
        m_hasBackground, // if 0 (background) is missing, subtract 1 on cmpidx
        voxelIndicesSorted_d,
        voxelComponentIds_d,
        componentBlockVoxelOffsets_d,
        componentMass_d,
        componentBlockMass_d );

    gpuErrchk( cudaPeekAtLastError() );
    gpuErrchk( cudaDeviceSynchronize() );

    // /************************************************************************************/
    // // distribute each component's sites fairly among its blocks

    cudaMalloc( (void **) & componentBlockSiteCounts_d, N_CMP_BLOCKS  * sizeof( int64_t ) );
    cudaMalloc( (void **) & componentBlockRepresentationd_d, N_CMP_BLOCKS * sizeof(    float ) );
    cudaMalloc( (void **) & componentBlockIndices_d,         N_CMP_BLOCKS * sizeof(   int64_t ) );

    cudaMemset( componentBlockSiteCounts_d, 0, N_CMP_BLOCKS * sizeof( int64_t ) );

    // for on device sorting by key of under-represented blocks
    auto indices = thrust::make_counting_iterator( 0 );
    thrust::copy(
        indices,
        indices + N_CMP_BLOCKS,
        thrust::device_pointer_cast( componentBlockIndices_d ) );

    chooseCmpBlkCounts<<< gridSize( CM_THREADS_PER_BLOCK, m_NC ), CM_THREADS_PER_BLOCK >>>(
        m_NC,
        N_CMP_BLOCKS,
        componentBlockMass_d,
        componentSiteCounts_d,
        componentOffsetsInBlockComponentsVector_d,
        componentBlockRepresentationd_d,
        componentBlockIndices_d,
        componentBlockSiteCounts_d );

    gpuErrchk( cudaPeekAtLastError() );
    gpuErrchk( cudaDeviceSynchronize() );
 
    // /**************************************************************************/
    // // randomly generate c(b) sites for each block b

    cudaMemset( voxelSiteStatus_d, 0, M_NV * sizeof( uint8_t ) );
    sampleComponentsBlocks<<< gridSize( CM_THREADS_PER_BLOCK, N_CMP_BLOCKS ), CM_THREADS_PER_BLOCK >>>(
        N_CMP_BLOCKS,
        componentBlockVoxelOffsets_d,
        componentBlockSiteCounts_d,
        voxelSiteStatus_d );

    gpuErrchk( cudaPeekAtLastError() );
    gpuErrchk( cudaDeviceSynchronize() );

    /*****************************************************************************/
    // need to know where in the site array to begin putting the sites for a given
    // component block, e.g. for each component block, which offset into the sites to begin at

    cudaMalloc( (void **) & componentBlockSiteOffsets_d, N_CMP_BLOCKS * sizeof( int64_t ) );

    thrust::exclusive_scan(
        thrust::device_pointer_cast( componentBlockSiteCounts_d ),
        thrust::device_pointer_cast( componentBlockSiteCounts_d ) + N_CMP_BLOCKS,
        thrust::device_pointer_cast( componentBlockSiteOffsets_d ) );

    gpuErrchk( cudaPeekAtLastError() );
    gpuErrchk( cudaDeviceSynchronize() );

    /********************************************************************************/
    // // now set the site positions, map their component ids ////////////////////////
    // // when evolving, need to know component of voxel, component of site

    // persistent 

    cudaMalloc( (void **) & siteDeltas_d,     m_nSites * sizeof( float )  );
    cudaMalloc( (void **) & sitePositions_d,  m_nSites * sizeof( float ) * 3 );
    cudaMalloc( (void **) & siteComponents_d, m_nSites * sizeof( int32_t ) );
    cudaMalloc( (void **) & siteLayers_d,     m_nSites * sizeof( int16_t ) );

    initializeComponentBlockSites<<< gridSize( CM_THREADS_PER_BLOCK, N_CMP_BLOCKS ), CM_THREADS_PER_BLOCK >>>(
        N_CMP_BLOCKS,
        voxelIndicesSorted_d,
        componentBlockVoxelOffsets_d,
        componentBlockSiteOffsets_d,
        voxelComponentIds_d,
        voxelLayerIds_d,
        voxelSiteStatus_d,
        siteComponents_d,
        siteLayers_d,
        sitePositions_d );

    gpuErrchk(   cudaPeekAtLastError() );
    gpuErrchk( cudaDeviceSynchronize() );

    /*************************************************/

    cudaFree( componentBlockRepresentationd_d );
    cudaFree( componentBlockIndices_d );
    cudaFree( componentBlockSiteCounts_d );  
    cudaFree( componentBlockSiteOffsets_d );      
    cudaFree( componentVoxelCounts_d );
    cudaFree( componentMass_d );
}

void CentroidalSampler::computeInitialSiteDistribution(
    const float density,
    const float attractionBias,
    const float * mass_d,
    bool saveResults,
    const std::string & outPath )
{
    const int64_t blockSize = 50;    

    // temporary --- freed after used in this function /////////////////////////

    int64_t  * voxelComponentBlockIds_d;
    cudaMalloc( (void **) & voxelComponentBlockIds_d, M_NV * sizeof( int64_t ) );

    int32_t  * componentOffsetsInBlockComponentsVector_d;
    cudaMalloc( (void **) & componentOffsetsInBlockComponentsVector_d, m_NC * sizeof( int32_t ) );

    // // persistent ////////////////////////////////////////////////////////////////

    cudaMalloc( (void **) & componentSiteCounts_d, m_NC * sizeof( int64_t ) );
    cudaMemset( componentSiteCounts_d, 0, m_NC * sizeof( int64_t ) );

    ////////////////////////////////////////////////////////////////////////////

    const int64_t N_CMP_BLOCKS = blockDecomposeComponents( 
        blockSize, 
        voxelComponentBlockIds_d,
        componentOffsetsInBlockComponentsVector_d );

    /****************************************************************************/

    // temporary --- freed after used in this function

    float    * componentBlockMass_d;
    cudaMalloc( (void **) & componentBlockMass_d,       N_CMP_BLOCKS  * sizeof( float ) );
    cudaMemset( componentBlockMass_d, 0, N_CMP_BLOCKS * sizeof( float ) );

    int64_t * componentBlockVoxelCounts_d;
    cudaMalloc( (void **) & componentBlockVoxelCounts_d, N_CMP_BLOCKS * sizeof( int64_t ) );    
    cudaMemset( componentBlockVoxelCounts_d, 0, N_CMP_BLOCKS * sizeof( int64_t ) );

    int64_t  * componentBlockVoxelOffsets_d;
    cudaMalloc( (void **) & componentBlockVoxelOffsets_d, N_CMP_BLOCKS * sizeof( int64_t ) );

    int32_t   * componentIdsInBlockComponentsVector_d;
    cudaMalloc( (void **) & componentIdsInBlockComponentsVector_d, N_CMP_BLOCKS * sizeof( int32_t ) );

    ////////////////////////////////////////////////////////////////////////////////////

    computeComponentBlockOffsets(
        blockSize, 
        N_CMP_BLOCKS,
        componentBlockVoxelOffsets_d,
        voxelComponentBlockIds_d,
        componentOffsetsInBlockComponentsVector_d,
        componentIdsInBlockComponentsVector_d );

    /////////////////////////////////////////////////////////////////////////////////////

    if( mass_d != NULL ) {
        getComponentBlockMass<<< gridSize( 256, N_CMP_BLOCKS ), 256 >>>(
            N_CMP_BLOCKS,
            attractionBias,
            voxelComponentBlockIds_d,
            voxelComponentIds_d,
            mass_d,
            voxelIndicesSorted_d,
            componentBlockVoxelOffsets_d,
            componentBlockMass_d,
            componentBlockVoxelCounts_d );
    } else {
        getComponentBlockMass<<< gridSize( 256, N_CMP_BLOCKS ), 256 >>>(
            N_CMP_BLOCKS,
            voxelComponentBlockIds_d,
            voxelComponentIds_d,
            voxelIndicesSorted_d,
            componentBlockVoxelOffsets_d,
            componentBlockMass_d,
            componentBlockVoxelCounts_d );
    }

    gpuErrchk( cudaPeekAtLastError() );
    gpuErrchk( cudaDeviceSynchronize() );

        ///////////////////////////////////////////////////////////////////

    if( saveResults ) {

        std::vector< float > mass( N_CMP_BLOCKS );

        cudaMemcpy(
            mass.data(),
            componentBlockMass_d,
            mass.size() * sizeof( float ),
            cudaMemcpyDeviceToHost );

        std::ofstream outFile( outPath + "/mass.dat", std::ios::out | std::ios::binary );
        outFile.write( ( char* ) mass.data(), mass.size() * sizeof( float )  );
        outFile.close();
    }
    
    // //////////////////////////////////////////////////////////////////////////////////

    sampleBlockComponents(
        N_CMP_BLOCKS,
        density,
        componentOffsetsInBlockComponentsVector_d,
        componentIdsInBlockComponentsVector_d,
        componentBlockMass_d,
        componentBlockVoxelCounts_d,
        componentBlockVoxelOffsets_d );

    if( saveResults ) {

        std::vector< int64_t > dat( M_NV );

        ///////////////////////////////////////////////////////////////////

        cudaMemcpy(
            dat.data(),
            voxelComponentBlockIds_d,
            dat.size() * sizeof( int64_t ),
            cudaMemcpyDeviceToHost );

        std::ofstream outFile( outPath + "/cmpblockids.dat", std::ios::out | std::ios::binary );
        if( ! outFile.is_open() ) {
            std::cerr << "file could not be opened for writing" << outPath << std::endl;
            exit( 1 );
        }

        outFile.write( ( char* ) dat.data(), dat.size() * sizeof( int64_t )  );
        outFile.close();

        ///////////////////////////////////////////////////////////////////

        cudaMemcpy(
            dat.data(),
            voxelIndicesSorted_d,
            dat.size() * sizeof( int64_t ),
            cudaMemcpyDeviceToHost );

        outFile.open( outPath + "/sortedIndices.dat", std::ios::out | std::ios::binary );
        outFile.write( ( char* ) dat.data(), dat.size() * sizeof( int64_t )  );
        outFile.close();


        ///////////////////////////////////////////////////////////////////

        std::vector< float > siteSampleImage( M_NV );

        cudaMemcpy(
            siteSampleImage.data(),
            voxelSiteStatus_d,
            siteSampleImage.size() * sizeof( uint8_t ),
            cudaMemcpyDeviceToHost );

        outFile.open( outPath + "/initSampleImage.dat", std::ios::out | std::ios::binary );
        outFile.write( ( char* ) siteSampleImage.data(), siteSampleImage.size() * sizeof( uint8_t )  );
        outFile.close();
    }

    /////////////////////////////////////////////////////////////////////////////////

    cudaFree( componentBlockMass_d );
    cudaFree( componentBlockVoxelCounts_d );    
    cudaFree( componentBlockVoxelOffsets_d );
    cudaFree( voxelComponentBlockIds_d );
    cudaFree( componentOffsetsInBlockComponentsVector_d );
    cudaFree( componentIdsInBlockComponentsVector_d );

    // persistent --- freed on descruction of class instance

    cudaMalloc( (void **) & netMass_d,          m_nSites * sizeof( float ) );
    cudaMalloc( (void **) & centerOfMass_d,     m_nSites * 3 * sizeof( float ) );
    cudaMalloc( (void **) & voxelSiteIds_d,           M_NV * sizeof( int32_t ) );

    if( m_verbose ) {
        std::cout << "Computed " << m_nSites << " initial site positions based on density parameter " << density << " with NC = " << m_NC << std::endl;
    }
}

// unweighted version

void CentroidalSampler::initializeSiteDistribution(
    float density,
    bool saveResults,
    const std::string & outPath ) {
    computeInitialSiteDistribution( density, NULL, NULL, saveResults, outPath );
}

// weighted version

void WeightedCentroidalSampler::initializeSiteDistribution(
    float * mass,
    float density,
    float attractionBias,
    bool saveResults,
    const std::string & outPath ) 
{
    LI3 tmpD = { M_DIMS[ 0 ], M_DIMS[ 1 ], M_DIMS[ 2 ] };
    cudaMemcpyToSymbol( D_DIMS, &tmpD, sizeof( tmpD.x ) * 3 );
    cudaMemcpyToSymbol( D_NV,   &M_NV, sizeof( int64_t ) );

    applyMassFunction<<< gridSize( CM_THREADS_PER_BLOCK, M_NV ), CM_THREADS_PER_BLOCK >>>(
        mass_d, 
        attractionBias );
    computeInitialSiteDistribution( density, attractionBias, mass_d, saveResults, outPath );
}