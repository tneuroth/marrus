
#include "utils/IndexUtils.hpp"
#include "utils/TF.hpp"
#include "geometry/Vec.hpp"
#include "lsrcvt_lib.hpp"

#include <jsonhpp/json.hpp>

#include <mpi.h>
#include <cstdint>

#include <vector>
#include <fstream>
#include <iostream>
#include <chrono>
#include <unordered_map>


using namespace std::chrono;
using namespace std;

template< typename T >
void writeData( 
    const string & filePath,
    const vector< T > & data )
{
    cout << "Writing " << data.size()*sizeof(T) << " bytes to " << filePath << endl;
    ofstream outFile( filePath, ios::out | ios::binary );

    if( ! outFile.is_open() )
    {
        cout << "file was no opened" << filePath << endl;
    }
    else
    {
        outFile.write( ( char* ) data.data(), data.size() * sizeof( T )  );
        outFile.close();
    }
}

template< typename TMP, typename RT >
void parallelLoad(
    const string & filePath,
    const int64_t FieldOffset,
    const TN::Vec3< int64_t > & xyzStart,
    const TN::Vec3< int64_t > & myDims,   
    const TN::Vec3< int64_t > & fullDims,        
    vector< TMP > & tmp,
    vector< RT > & result )
{
    const int64_t OFFSET = fullDims.x() * fullDims.y() * fullDims.z() * FieldOffset +
        TN::flatIndex3dCM( 
            0, 
            0, 
            xyzStart.z(), 
            fullDims.x(), 
            fullDims.y(), 
            fullDims.z() );

    const int64_t NLOAD  = fullDims.x() * fullDims.y() * myDims.z();

    ifstream inFile( filePath, ios::in | ios::binary );

    if( ! inFile.is_open() )
    {
        cerr << "File didn't open " << filePath << endl;
    }

    // cout << "seeking to " << OFFSET * sizeof( TMP ) << endl;

    inFile.seekg( OFFSET * sizeof( TMP ), ios::beg );    
    tmp.resize( NLOAD );

    // cout << "reading " << NLOAD * sizeof( TMP ) << " bytes " << endl;

    inFile.read( (char*) tmp.data(), NLOAD * sizeof( TMP ) );
    inFile.close();

    const int64_t MY_N = myDims.x() * myDims.y() * myDims.z();
    result.resize( MY_N );

    // cout << "my chunk is " << MY_N << endl;

    for( int64_t x = 0; x < myDims.x(); ++x )
    for( int64_t y = 0; y < myDims.y(); ++y )        
    for( int64_t z = 0; z < myDims.z(); ++z )
    {
        int64_t flatIdx  = TN::flatIndex3dCM( 
            xyzStart.x() + x, 
            xyzStart.y() + y, 
            z, 
            fullDims.x(), 
            fullDims.y(), 
            myDims.z() );

        int64_t flatIdx2 = TN::flatIndex3dCM( 
            x, 
            y, 
            z, 
            myDims.x(), 
            myDims.y(), 
            myDims.z() );        

        result[ flatIdx2 ] = tmp[ flatIdx ];
    }
}


int main(int argc, char** argv)
{
    /**************************************************

                         Parameters

    ***************************************************/

    MPI_Init(NULL, NULL);

    int nranks;
    MPI_Comm_size(MPI_COMM_WORLD, &nranks);

    int rank;
    MPI_Comm_rank( MPI_COMM_WORLD, &rank );

    const int64_t X = 3456;
    const int64_t Y = 1280;
    const int64_t Z = 2560;

    int64_t NBX = 8;
    int64_t NBY = 3;
    int64_t NBZ = 8;
    
    const int64_t BLKW_X = X / NBX;
    const int64_t BLKW_Y = Y / NBY;
    const int64_t BLKW_Z = Z / NBZ;

    const int64_t N_BLOCKS = BLKW_X * BLKW_Y * BLKW_Z;

    const TN::Vec3< int64_t > MY_BLOCK_OFFSETS = 
        TN::index3dCM( int64_t(rank), NBX, NBY, NBZ );

    const TN::Vec3< int64_t > MY_VOX_START =
    {
        MY_BLOCK_OFFSETS.x() * BLKW_X,
        MY_BLOCK_OFFSETS.y() * BLKW_Y,
        MY_BLOCK_OFFSETS.z() * BLKW_Z
    };

    //const string DIR = "/run/media/v1/VIDI_Data_2/ts/testData/";

    //const string lfPath = DIR + "turb_shear_no_flame.5.4000E-04.field.mpi.temp.1728.640.1280.f32.smoothgs3.dat";
    //const string mfPath = DIR + "turb_shear_no_flame.5.4000E-04.field.mpi.temp.1728.640.1280.f32.smoothgs3.dat";

    const string DIR = "/gpfs/alpine/scratch/tneuroth/cmb103/turb_shear_downsample/turb_shear/1atm_new_grid_2/float32/";

    const string lfPath = DIR + "turb_shear_no_flame.5.4875E-04.field.mpi.temp.f32.dat";
    const string mfPath = DIR + "turb_shear_no_flame.5.4875E-04.field.mpi.temp.f32.dat";

    const int64_t lfOffset = 0;
    const int64_t mfOffset = 0;

    const int MY_NV = BLKW_X * BLKW_Y * BLKW_Z;

    vector< float > myLField( MY_NV );
    // vector< float > myMField( MY_NV );
    vector< float > tmp;

    parallelLoad(
        lfPath,
        lfOffset,
        MY_VOX_START,
        { BLKW_X, BLKW_Y, BLKW_Z },
        { X, Y, Z },
        tmp,
        myLField );

    auto rng = TN::Sequential::getRange( myLField );
    cout << "lfield range is " << rng.a() << " " << rng.b() << endl;

    float siteDensity = 0.0003;
    float attraction  = 0.0;

    vector< TN::Vec2< float > > layers = {
        {  6.0,      9.25 },
        { 9.25,      13.4 },
        { 13.4,    14.584 },
        { 14.584,   14.71 },  
        { 14.71,     18.0 }                    
    };

    const int NL = layers.size();

    const string outPath  = "./results/test3d/dat/";

    /**************************************************

                     LRV Decomposition

    ***************************************************/

    cout << "instantiaed sampler" << endl;

    CentroidalSampler sampler( { BLKW_X, BLKW_Y, BLKW_Z } );

    cout << "setting fields" << endl;

    sampler.setFields(
        myLField.data(),
        myLField.data(),
        { BLKW_X, BLKW_Y, BLKW_Z } );

    sampler.setLayers( layers );
    sampler.computeComponents();

    auto time0 = high_resolution_clock::now();

    sampler.computeInitialSiteDistribution( siteDensity, attraction );

    vector< float > maxDeltas;
    vector< float > meanDeltas;    

    float threshold = 0.25;
    const int maxCentroidalUpdates = 50;

    for( int i = 1; i <= maxCentroidalUpdates; ++i ) {

        const string iterStr      = to_string( i ); 
        const string iterStrZfill = string( 5 - iterStr.size(), '0' ) + iterStr;

        int64_t losStepsTook = 0;
        int64_t loeStepsTook = 0;   

        // Normal Full Stage ////////////////////////////////////////////////////////////////////////////////

        sampler.geodesicVoronoiClassification( 
            -1 ,             // -1 -> no limit to steps in LOS classification process
            -1,              // -1 -> no limit to steps in LOE classification process
            false,           // parameter to override LOE stage
            true,            // simple heuristic for components with only one site (pre-classify and move only once) 
            losStepsTook,    // recieve back the number of steps taken
            loeStepsTook     // recieve back the number of steps taken
        );

        CentroidalUpdateStatistics stats = sampler.geodesicCentroidalUpdate();

        maxDeltas.push_back(  stats.maxDelta  );
        meanDeltas.push_back( stats.meanDelta );       

        cout << "rank " << rank << "max delta : "  << maxDeltas.back()  << endl;
        cout << "rank " << rank << "mean delta : " << meanDeltas.back() << endl;
        auto tk = high_resolution_clock::now();
        cout << "rank " << rank << " took thus far: " << duration_cast<milliseconds>( tk - time0 ).count()<< " ms" << endl;
        if( meanDeltas.back() < threshold ) {
            break;
        }
    }

    auto time1 = high_resolution_clock::now();
    double t = duration_cast<milliseconds>( time1 - time0 ).count();
    cout << "rank " << rank << " took total " << t << " ms" << endl;
    writeData( outPath +  "maxDelta.double." + to_string( maxDeltas.size() ) + "." + to_string( rank ) +  ".dat", maxDeltas  );
    writeData( outPath + "meanDelta.double." + to_string( maxDeltas.size() ) + "." + to_string( rank ) +  ".dat", meanDeltas );  
    sampler.finalize();  
    
    MPI_Finalize();
}
