#include "lsrcvt_lib.hpp"
#include "utility.cuh"
#include "constants.cuh"

#include "algorithms/Standard/MyAlgorithms.hpp"
#include <ConnectedComponents27.hpp>
#include "algorithms/ConnectedComponentsCPU.hpp"
#include "algorithms/CCTest.hpp"
#include "utils/IndexUtils.hpp"

#include <cuda.h>

#include <chrono>
#include <cstdio>
#include <vector>
#include <iostream>

using namespace std::chrono;

__global__
void mapLayers(
    int16_t     * layerMap,
    const float * lfield,
    const float * layers,
    const int NL )
{
    const int64_t idx = blockIdx.x * blockDim.x + threadIdx.x;
    if( idx >= D_NV ) { return; }
    float lf = lfield[ idx ];
    
    layerMap[ idx ] = 0;

    for( int l = 0; l < NL; ++l )
    {
        float la = layers[ 2*l     ];
        float lb = layers[ 2*l + 1 ];

        float width   = lb - la;

        if( lf >= la && lf < lb  )
        {
            layerMap[ idx ] = l + 1;
            break;
        }
    }
}

void CentroidalSampler::computeComponents()
{
    if( m_verbose ) 
    {
        std::cout << "computing connected components" << std::endl;
    }

    /*********************************************************************/
    // map layers 

    auto time = high_resolution_clock::now();

    mapLayers<<< gridSize( VX_THREADS_PER_BLOCK, M_NV ), VX_THREADS_PER_BLOCK >>>(
        voxelLayerIds_d,
        data_d,
        layers_d,
        m_layers.size() );

    gpuErrchk( cudaPeekAtLastError() );
    gpuErrchk( cudaDeviceSynchronize() );

    m_voxelLayerIds.resize( M_NV );
    
    cudaMemcpy(
        m_voxelLayerIds.data(),
        voxelLayerIds_d,
        M_NV * sizeof( int16_t ),
        cudaMemcpyDeviceToHost );

    gpuErrchk( cudaPeekAtLastError() );
    gpuErrchk( cudaDeviceSynchronize() );


    if( m_validate ) {
        auto lyrRange = TN::Sequential::getRange( m_voxelLayerIds );
        std::cout << "lyrRange = " << lyrRange.a() << " " << lyrRange.b() << std::endl;
    }

    /*********************************************************************/
    // get the connected components 

    int NCC;
    m_voxelComponentIds.resize( M_NV );

    // TN::connectedComponentsFull(
    //     m_voxelLayerIds.data(),
    //     M_DIMS,
    //     m_voxelComponentIds.data(),
    //     NCC );

    TN::connectedComponents(
        m_voxelLayerIds,
        M_DIMS,
        M_NV / 8,
        m_voxelComponentIds,
        NCC );

    m_NC = NCC;

    cudaMalloc( (void **) & voxelComponentIds_d, M_NV * sizeof( int32_t ) );
    cudaMemcpy(
        voxelComponentIds_d,
        m_voxelComponentIds.data(),
        M_NV * sizeof( int32_t ),
        cudaMemcpyHostToDevice );

    /************************************************************************/
    // verification 

    if( m_validate ) {
        if( ! TN::verifyComponents( m_voxelComponentIds, m_voxelLayerIds, NCC, M_DIMS ) ) {
            std::cout << "Error: connected components failed" << std::endl;
            exit( 0 );
        }
        else {
            std::cout << "Connected components verified" << std::endl;
        }
    }
}