
#include "lsrcvt_lib.hpp"

#include <thrust/reduce.h>
#include <thrust/execution_policy.h>
#include <thrust/device_ptr.h>
#include <thrust/iterator/discard_iterator.h>
#include <thrust/functional.h>
#include <thrust/transform_scan.h>
#include <thrust/device_vector.h>
#include <thrust/device_ptr.h>
#include <thrust/iterator/permutation_iterator.h>
#include <thrust/partition.h>

#include <chrono>
#include <cstdio>
#include <set>
#include <fstream>
#include <vector>
#include <iostream>

using namespace std::chrono;

void CentroidalSampler::computeStorageLayout( 
    std::vector< int64_t > & layerOffsets,
    std::vector< int64_t > & componentOffsets, 
    std::vector< int64_t > & layout )
{
    std::vector< int64_t > ids;
    layout.clear();

    const int32_t N_LAYERS = m_layers.size();
    size_t count = 0;

    for( size_t i = 0; i < M_NV; ++i )
    {
        if( m_voxelLayerIds[ i ] > 0 )
        {
            layout.push_back( i );
            ids.push_back( N_LAYERS * m_voxelComponentIds[ i ]  + m_voxelLayerIds[ i ] );
            ++count;
        }
    }

    std::vector< int32_t > subSelectedComponentIds( count );
    std::vector< int16_t > subSelectedLayerIds(     count );

    thrust::sort_by_key( ids.begin(), ids.end(), layout.begin() );

    #pragma omp parallel for
    for( size_t i = 0; i < count; ++i )
    {
        subSelectedLayerIds[     i ] = m_voxelLayerIds[      layout[ i ] ];
        subSelectedComponentIds[ i ] = m_voxelComponentIds[  layout[ i ] ];
    }

    layerOffsets.resize(        N_LAYERS );
    componentOffsets.resize(        m_NC );

    thrust::unique_by_key_copy(
        subSelectedLayerIds.begin(),
        subSelectedLayerIds.end(),
        thrust::make_counting_iterator(0),
        thrust::make_discard_iterator(),
        layerOffsets.begin() );

    thrust::unique_by_key_copy(
        subSelectedComponentIds.begin(),
        subSelectedComponentIds.end(),
        thrust::make_counting_iterator(0),
        thrust::make_discard_iterator(),
        componentOffsets.begin() );
}