
#include "lsrcvt_lib.hpp"

#include "utils/IndexUtils.hpp"
#include "algorithms/Standard/MyAlgorithms.hpp"

#include "constants.cuh"
#include "utility.cuh"

////////////////////////////////////////////////

#include "computeComponents.cu"
#include "computeInitialSiteDistribution.cu"
#include "computeSiteAdjacency.cu"
#include "computeStorageLayout.cu"
#include "geodesicVoronoiClassificationV2.cu"
#include "geodesicCentroidalUpdate.cu"
#include "floydWarshal.cu"
#include "computeSiteGeodesicDistances.cu"

////////////////////////////////////////////////

#include <thrust/reduce.h>
#include <thrust/execution_policy.h>
#include <thrust/device_ptr.h>
#include <thrust/iterator/discard_iterator.h>
#include <thrust/functional.h>
#include <thrust/transform_scan.h>
#include <thrust/device_vector.h>
#include <thrust/device_ptr.h>
#include <thrust/iterator/permutation_iterator.h>
#include <thrust/partition.h>

#include <cuda.h>
#include <curand.h>
#include <curand_kernel.h>

#include <cstdint>
#include <cfloat>
#include <chrono>
#include <cstdio>
#include <set>
#include <fstream>

using namespace std::chrono;

namespace CentroidSamplerWriter {
template< typename T >
inline
void writeData( 
    const std::string & filePath,
    const std::vector< T > & data )
{
    // std::cout << "Writing data to " <<  filePath << std::endl;
    std::ofstream outFile( filePath, std::ios::out | std::ios::binary );
    if( ! outFile.is_open() ) {
        std::cerr << "file could not be opened for writing" << filePath << std::endl;
        exit( 1 );
    }
    outFile.write( reinterpret_cast< const char* >( data.data() ), data.size() * sizeof( T )  );
    outFile.close();
}

template< typename T >
inline
void readData( 
    const std::string      & filePath,
    std::vector< T > & data,
    const int64_t N )
{
    std::cout << "reading data from " <<  filePath << std::endl;

    data.resize( N );
    std::ifstream inFile( filePath, std::ios::in| std::ios::binary );
    if( ! inFile.is_open() ) {
        std::cerr << "file could not be opened for reading" << filePath << std::endl;
        exit( 1 );
    }
    
    inFile.read( reinterpret_cast< char* >( data.data() ), N * sizeof( T ) );
    inFile.close();
}

}

void CentroidalSampler::freeBuffers()
{
    cudaFree( data_d                 );
    cudaFree( layers_d               );
    cudaFree( voxelLayerIds_d        );
    cudaFree( sitePositions_d        );
    cudaFree( siteDeltas_d           );    
    cudaFree( siteComponents_d       );
    cudaFree( voxelSiteStatus_d      );
    cudaFree( minNeighbors_d         );
    cudaFree( siteLayers_d           );
    cudaFree( voxelIndicesSorted_d   );
    cudaFree( voxelSiteIds_d         );
    cudaFree( netMass_d              );
    cudaFree( centerOfMass_d         );
    cudaFree( componentSiteCounts_d  );
}

void CentroidalSampler::setLayers(
    const std::vector< std::array< float, 2 > > & l )
{
    m_layers = l;
    size_t NL = l.size();

    // in case C++ implementation doesn't create contiguous memory for vector of array
    std::vector< float > lcpy( NL*2 );
    for( size_t i = 0; i < NL; ++i ) {
        lcpy[ i * 2 + 0 ] = l[ i ][ 0 ];
        lcpy[ i * 2 + 1 ] = l[ i ][ 1 ];        
    }

    cudaMalloc( (void **) &  layers_d, NL * 2 * sizeof( float ) );
    cudaMalloc( (void **) &voxelLayerIds_d, M_NV * sizeof( int16_t ) );

    cudaMemset( voxelLayerIds_d, 2, M_NV * sizeof( int16_t ) );
    cudaMemcpy( layers_d, (float*) lcpy.data(), NL * 2 * sizeof( float ), cudaMemcpyHostToDevice );
}

void CentroidalSampler::computeLayerDecomposition(
    float * fieldData,
    const std::vector< std::array< float, 2 > > & layers )
{
    cudaMemcpy( data_d, fieldData, M_NV * sizeof( float ), cudaMemcpyHostToDevice );   
    setLayers( layers );
    computeComponents();
}

bool CentroidalSampler::validateSiteIdContiguity()
{
    bool valid = true;
    std::set< int32_t > siteIds;
    for( size_t i = 0; i < M_NV; ++i ) {
        if( m_voxelSiteIds[ i ] < -1 ||  m_voxelSiteIds[ i ] > m_nSites ) {
            valid = false;
            break;
        }
        siteIds.insert( m_voxelSiteIds[ i ] );
    }
    return valid && siteIds.size() == M_NV;
}

void CentroidalSampler::setValidate( bool c ) { m_validate = c; }
void CentroidalSampler::setVerbose(  bool c ) { m_verbose  = c; }
void CentroidalSampler::setMaxRES(  int64_t r ) { m_maxRES  = r; }

const std::vector< std::array< float, 2 > > & CentroidalSampler:: getLayers() const
{
    return m_layers;
}

bool CentroidalSampler::hasBackgroundVoxels() const {
    return m_hasBackground;
}


int64_t CentroidalSampler::getNComponents() const
{
    return m_NC;
}

const std::vector< float > & CentroidalSampler::getSiteEuclideanDistanceMatrix() const
{
    return m_siteEuclideanDistanceMatrix;
}

const std::vector< float > & CentroidalSampler::getSiteGeodesicDistanceMatrix() const
{
    return m_siteGeodesicDistanceMatrix;
}

const std::vector< int8_t > & CentroidalSampler::getVoxelStatuses()
{
    return m_voxelStatus;
}

const std::vector< float > & CentroidalSampler::getSitePositions()
{
    return m_sitePoints;
}

const std::vector< int32_t > & CentroidalSampler::getClassification()
{
    return m_voxelSiteIds;
}

const std::vector< int16_t > & CentroidalSampler::getVoxelLayerIds()
{
    return m_voxelLayerIds;
}

const std::vector< int32_t > & CentroidalSampler::getVoxelComponentIds()
{
    return m_voxelComponentIds;
}

const std::vector< int32_t > & CentroidalSampler::getSiteComponentIds()
{
    return m_siteComponentIds;
}

const std::vector< int16_t > & CentroidalSampler::getSiteLayers() {
    return m_siteLayers;
}

std::vector< int64_t > CentroidalSampler::getSourceMap()
{
    return m_srcMap;
}

std::vector< float > CentroidalSampler::getVoxelDistances()
{
    return m_distances;
}


void CentroidalSampler::finalize()
{
    m_sitePoints.resize( m_nSites * 3 );
    cudaMemcpy(
        m_sitePoints.data(),
        sitePositions_d,
        m_sitePoints.size() * sizeof( float ),
        cudaMemcpyDeviceToHost );

    m_siteLayers.resize( m_nSites );
    cudaMemcpy(
        m_siteLayers.data(),
        siteLayers_d,
        m_nSites * sizeof( int16_t ),
        cudaMemcpyDeviceToHost );

    m_siteComponentIds.resize( m_nSites );
    cudaMemcpy(
        m_siteComponentIds.data(),
        siteComponents_d,
        m_nSites * sizeof( int32_t ),
        cudaMemcpyDeviceToHost );

    ////////////////////////////////////////////////////////////

    m_voxelSiteIds.resize( M_NV );
    cudaMemcpy(
        m_voxelSiteIds.data(),
        voxelSiteIds_d,
        M_NV * sizeof( int32_t ),
        cudaMemcpyDeviceToHost );


    m_voxelLayerIds.resize( M_NV );
    cudaMemcpy(
        m_voxelLayerIds.data(),
        voxelLayerIds_d,
        M_NV * sizeof( int16_t ),
        cudaMemcpyDeviceToHost );

    m_voxelComponentIds.resize( M_NV );
    cudaMemcpy(
        m_voxelComponentIds.data(),
        voxelComponentIds_d,
        M_NV * sizeof( int32_t ),
        cudaMemcpyDeviceToHost );

    m_voxelStatus.resize( M_NV );
    cudaMemcpy(
        m_voxelStatus.data(),
        voxelSiteStatus_d,
        M_NV * sizeof( uint8_t ),
        cudaMemcpyDeviceToHost );

    m_distances.resize( M_NV );
    cudaMemcpy(
        m_distances.data(),
        data_d,
        M_NV * sizeof( float ),
        cudaMemcpyDeviceToHost );

    m_srcMap.resize( M_NV );
    cudaMemcpy(
        m_srcMap.data(),
        voxelIndicesSorted_d,
        M_NV * sizeof( int64_t ),
        cudaMemcpyDeviceToHost );
}

void CentroidalSampler::fromFile( const std::string & directory, bool distMatricesToo ) {

    std::vector< int64_t > tmp( 7 );
    CentroidSamplerWriter::readData( 
        directory + "n_lyr_comp_sites_x_y_z.bin", 
        tmp,
        static_cast< int64_t >( tmp.size() )
     );

    const int64_t NL = tmp[ 0 ];
    m_NC             = tmp[ 1 ];
    m_nSites         = tmp[ 2 ];
    M_DIMS[ 0 ]      = tmp[ 3 ];
    M_DIMS[ 1 ]      = tmp[ 4 ];
    M_DIMS[ 2 ]      = tmp[ 5 ];
    M_NV = M_DIMS[ 0 ] * M_DIMS[ 1 ] * M_DIMS[ 2 ];
    m_hasBackground = static_cast< bool >( tmp[ 6 ] );

    CentroidSamplerWriter::readData( directory + "sitePoints.bin"       , m_sitePoints       , m_nSites*3 );
    CentroidSamplerWriter::readData( directory + "siteLayers.bin"       , m_siteLayers       , m_nSites );
    CentroidSamplerWriter::readData( directory + "siteComponentIds.bin" , m_siteComponentIds , m_nSites );
    CentroidSamplerWriter::readData( directory + "voxelSiteIds.bin"     , m_voxelSiteIds     , M_NV );
    CentroidSamplerWriter::readData( directory + "voxelLayerIds.bin"    , m_voxelLayerIds    , M_NV );
    CentroidSamplerWriter::readData( directory + "voxelComponentIds.bin", m_voxelComponentIds, M_NV );
    CentroidSamplerWriter::readData( directory + "voxelStatus.bin"      , m_voxelStatus      , M_NV );
    CentroidSamplerWriter::readData( directory + "distances.bin"        , m_distances        , M_NV );
    CentroidSamplerWriter::readData( directory + "srcMap.bin"           , m_srcMap           , M_NV );

    if( distMatricesToo ) {
        CentroidSamplerWriter::readData( directory + "m_siteGeodesicDistanceMatrix.bin",     m_siteGeodesicDistanceMatrix,  m_nSites*m_nSites  );
        CentroidSamplerWriter::readData( directory + "m_siteEuclideanDistanceMatrix.bin",   m_siteEuclideanDistanceMatrix,  m_nSites*m_nSites  );
    }

    // layers needs to be converted to guarantee contiguous form first
    std::vector< float > ly;
    CentroidSamplerWriter::readData( directory + "layers.bin", ly, NL*2 );
    m_layers.resize( NL );
    for( size_t i = 0; i < NL; ++i ) {
        m_layers[ i ][ 0 ] = ly[ i*2     ];
        m_layers[ i ][ 1 ] = ly[ i*2 + 1 ];
    }
}

void CentroidalSampler::toFile( const std::string & directory, const std::string & prefix, bool distMatricesToo ) {

    CentroidSamplerWriter::writeData( 
        directory + prefix + "n_lyr_comp_sites_x_y_z.bin", 
        std::vector< int64_t >( {  
            static_cast< int64_t >( m_layers.size() ),
            m_NC,
            m_nSites, 
            M_DIMS[ 0 ],   
            M_DIMS[ 1 ],
            M_DIMS[ 2 ],
            static_cast< int64_t >( m_hasBackground ) } ) );

    CentroidSamplerWriter::writeData( directory + prefix + "sitePoints.bin"       , m_sitePoints        );    
    CentroidSamplerWriter::writeData( directory + prefix + "siteLayers.bin"       , m_siteLayers        );
    CentroidSamplerWriter::writeData( directory + prefix + "siteComponentIds.bin" , m_siteComponentIds  );
    CentroidSamplerWriter::writeData( directory + prefix + "voxelSiteIds.bin"     , m_voxelSiteIds      );
    CentroidSamplerWriter::writeData( directory + prefix + "voxelLayerIds.bin"    , m_voxelLayerIds     );
    CentroidSamplerWriter::writeData( directory + prefix + "voxelComponentIds.bin", m_voxelComponentIds );
    CentroidSamplerWriter::writeData( directory + prefix + "voxelStatus.bin"      , m_voxelStatus       );
    CentroidSamplerWriter::writeData( directory + prefix + "distances.bin"        , m_distances         );
    CentroidSamplerWriter::writeData( directory + prefix + "srcMap.bin"           , m_srcMap            );

    if( distMatricesToo ) {
        CentroidSamplerWriter::writeData( directory + prefix + "m_siteGeodesicDistanceMatrix.bin",    m_siteGeodesicDistanceMatrix  );
        CentroidSamplerWriter::writeData( directory + prefix + "m_siteEuclideanDistanceMatrix.bin",   m_siteEuclideanDistanceMatrix );
    }

    // layers needs to be converted to guarantee contiguous form first

    std::vector< float > ly( m_layers.size() * 2 );
    for( size_t i = 0; i < m_layers.size(); ++i ) {
        ly[ i*2     ] = m_layers[ i ][ 0 ];
        ly[ i*2 + 1 ] = m_layers[ i ][ 1 ];        
    }

    CentroidSamplerWriter::writeData( directory + prefix + "layers.bin", ly ); 
}

void CentroidalSampler::toFile( const std::string & directory, bool distMatricesToo ) {
    toFile( directory, "", distMatricesToo );
}   

void CentroidalSampler::evolve( const int64_t N_UPDATES ) {
    for( int64_t i = 0; i < N_UPDATES; ++i ) {
        geodesicVoronoiClassification();
        CentroidalUpdateStatistics stats = geodesicCentroidalUpdate();
        if( m_verbose ) {
            std::cout << "meanDelta=" << stats.meanDelta << ", maxDelta=" << stats.maxDelta << std::endl;
        }
    }
    geodesicVoronoiClassification();
    finalize();
}

void init( const std::array< int64_t, 3 > & _d )
{
    
}

CentroidalSampler::CentroidalSampler( 
    const std::array< int64_t, 3 > & _d ) : 
        M_NV( _d[ 0 ] * _d[ 1 ] * _d[ 2 ] ),
        M_DIMS( _d ), m_nPrevUpdates( 0 )
{
    m_overrideLineOfEdge           = false;
    m_simpleSingleSiteCMPHeuristic = false;

    layers_d            = nullptr;
    voxelLayerIds_d     = nullptr;
    sitePositions_d     = nullptr;
    siteDeltas_d        = nullptr;    
    siteComponents_d    = nullptr;
    siteLayers_d        = nullptr;
    voxelSiteIds_d      = nullptr;
    netMass_d           = nullptr;
    centerOfMass_d      = nullptr;

    LI3 tmpD = { M_DIMS[ 0 ], M_DIMS[ 1 ], M_DIMS[ 2 ] };
    cudaMemcpyToSymbol( D_DIMS, &tmpD, sizeof( tmpD.x ) * 3 );

    gpuErrchk( cudaPeekAtLastError() );
    gpuErrchk( cudaDeviceSynchronize() );

    cudaMemcpyToSymbol( D_NV, &M_NV, sizeof( int64_t ) );

    gpuErrchk( cudaPeekAtLastError() );
    gpuErrchk( cudaDeviceSynchronize() );

    cudaMalloc( (void **) &data_d, M_NV * sizeof(   float ) );
    cudaMalloc( (void **) &voxelSiteStatus_d, M_NV * sizeof( uint8_t ) );
    cudaMalloc( (void **) &minNeighbors_d, M_NV * sizeof( int8_t ) );
    cudaMalloc( (void **) &voxelIndicesSorted_d, M_NV * sizeof( int64_t ) );

    gpuErrchk( cudaPeekAtLastError() );
    gpuErrchk( cudaDeviceSynchronize() );
}

CentroidalSampler::~CentroidalSampler()
{
    freeBuffers();
}
