
#include "lsrcvt_lib.hpp"
#include "utility.cuh"
#include "constants.cuh"

#include <thrust/reduce.h>
#include <thrust/execution_policy.h>
#include <thrust/device_ptr.h>
#include <thrust/iterator/discard_iterator.h>
#include <thrust/functional.h>
#include <thrust/transform_scan.h>
#include <thrust/device_vector.h>
#include <thrust/device_ptr.h>
#include <thrust/iterator/permutation_iterator.h>
#include <thrust/partition.h>

#include <chrono>
#include <cstdint>
#include <cfloat>
#include <bitset>

using namespace std::chrono;

__global__
void classifyEdgeSources(
    const int64_t   level,
    const int64_t   NV,
    const int32_t * voxelComponentIds,
    const int32_t * voxelSiteIds,
    uint8_t       * voxelStatus ) 
{
    const int64_t idx = blockIdx.x * blockDim.x + threadIdx.x;
    if( idx >= D_NV ) { return; }

    const int32_t siteId = voxelSiteIds[ idx ];
    if( siteId == BACKGROUND_VX_ID ) { return; }

    // condition, is resolved, and has neighbor in same component 
    // with a different site id // that is unclassified

    if( isResolved( voxelStatus[ idx ] ) )
    {
        const int32_t my_cmp_id = voxelComponentIds[ idx ];
        const LI3 DIMS = D_DIMS;
        const LI3 my_idx3 = index3D( idx, D_DIMS );
        const int32_t my_site_id = voxelSiteIds[ idx ];

        // a voxel can lie on both an inner and outer edge at the same time
        bool isInnerEdgeSRC = false;
        bool isOuterEdgeSRC = false;

        for( int i = 1; i < 27; ++i )
        {
            // compute indices and check bounds
            const LI3 nb_idx3 = my_idx3 + ntable[ i ];
            if( ! inBounds( nb_idx3, DIMS ) ) { continue; }
            const int64_t nb_idx = flatIndex3D( nb_idx3, DIMS );

            // must be in same component
            if( voxelComponentIds[ nb_idx ] != voxelComponentIds[ idx ] ) { continue; }

            const int32_t nb_site_id = voxelSiteIds[ nb_idx ];
            const bool NB_RESOLVED = nb_site_id != VX_SOURCE_NONE;

            // is active or resolved implies it was in line of site at previous level

            // unseen
            if( ! NB_RESOLVED ) { 
                isOuterEdgeSRC = true;
                if( isInnerEdgeSRC ) { break; }
            }
            
            //  bordering a different site's region 
            if( NB_RESOLVED && ( my_site_id != nb_site_id ) ) { 
                isInnerEdgeSRC = true;
                if( isOuterEdgeSRC ) { break; }
            }
        }

        if( level == 0 && ( isOuterEdgeSRC || ( RESOSOLVE_INNER_EDGES && isInnerEdgeSRC ) ) ) { 
            setEdgeSRCRoot( voxelStatus[ idx ], true ); 
        } 
        else if( isOuterEdgeSRC || ( RESOSOLVE_INNER_EDGES && isInnerEdgeSRC ) ) {          
            setEdgeSRCLeaf( voxelStatus[ idx ], true ); 
        }

        if( isOuterEdgeSRC ) {
            setOuterEdge( voxelStatus[ idx ], true ); 
        }
        if( isInnerEdgeSRC ) {
            setInnerEdge( voxelStatus[ idx ], true ); 
        }
    }
}

__global__
void applyLOEUpdate(
    const int64_t     NE,
    const int64_t   * ids,
    const int8_t    * minNeighbor,

    const int64_t   * sourceMapCopy,
    const int32_t   * siteMapCopy,    
    const uint8_t   * statusCopy,    
    const float     * distCopy,      
    int64_t         * sourceMap,
    int32_t         * siteMap,    
    uint8_t         * statuses,  
    float           * distances  )   
{
    const int64_t edx = blockIdx.x * blockDim.x + threadIdx.x;
    if( edx >= NE ) { return; }

    const int64_t idx = ids[ edx ];
    const uint8_t VX_STATUS = statusCopy[ edx ];

    if( isResolved( VX_STATUS ) ) { return; } //&& ! isActive( VX_STATUS ) ) { return; }

    if( minNeighbor[ idx ] != VX_MIN_NEIGHBOR_NONE ) {
        
        distances[ idx ] = distCopy[      edx ];
        sourceMap[ idx ] = sourceMapCopy[ edx ];
        siteMap[   idx ] = siteMapCopy[   edx ];      
        statuses[  idx ] = statusCopy[    edx ];
    }
}

__global__
void classifyLOEVoxels(
    const int64_t     NE,
    const int64_t     NV,
    const int64_t     NS,
    const int64_t   * ids,
    const int64_t   * sourceMap,
    const int32_t   * siteIds,
    const int32_t   * voxelComponentIds,
    const uint8_t   * voxelStatus,    
    const float    * sitePos,
    const float     * distances,
    float           * distCopy,   
    int8_t          * minNeighbor,
    int32_t         * siteIdsCopy,
    int64_t         * srcIdsCopy,
    uint8_t         * statusCopy )
{
    const int64_t edx = blockIdx.x * blockDim.x + threadIdx.x;
    if( edx >= NE ) { return; }

    const int64_t idx = ids[ edx ];
    minNeighbor[ idx ] = VX_MIN_NEIGHBOR_NONE;

    const uint8_t VX_STATUS = voxelStatus[ idx ];
    if ( isResolved( VX_STATUS ) ) { return; } //&& ! isActive( VX_STATUS ) ) { return; }

    const int64_t my_src_idx = sourceMap[ idx ];
    const int32_t my_cmp_idx = voxelComponentIds[ idx ];

    const LI3 DIMS = D_DIMS;
    const LI3 idx3d = index3D( idx, DIMS );
    const float3 voxelPos = location3D( idx3d, DIMS );

    for( int i = 1; i < 27; ++i )
    {
        const LI3 nb_idx3d = idx3d + ntable[ i ];
        if( ! inBounds( nb_idx3d, DIMS ) ) { continue; }

        const int64_t nb_idx = flatIndex3D( nb_idx3d, DIMS );
        const int64_t nb_src_id  = sourceMap[ nb_idx ];

        // if the neighbor has no source yet
        // or already mapped to the same source as me
        // or in a different component
        // then no need to check this neighbor
        if(  ( nb_src_id == VX_SOURCE_NONE )
          || ( nb_src_id == my_src_idx     )
          || ( voxelComponentIds[ nb_idx ] != my_cmp_idx ) ) { 
            continue; 
        }

        const uint8_t nb_status  = voxelStatus[ nb_idx ];

        // if the neighbor is a source node then the distance should be distance to it and distance from it to its site
        // since its adjacent, it's already known to be line of site
        if( isEdgeSRCNode( nb_status ) ) {
            const float fullDist = distances[ nb_idx ] + voxelDist( idx3d, nb_idx3d );
            if( ( fullDist + DIST_EPSILON ) < distances[ idx ] ) {
                    minNeighbor[ idx ] = i;
                    distCopy[    edx ] = fullDist;
                    siteIdsCopy[ edx ] = siteIds[ nb_idx ];
                    srcIdsCopy[  edx ] = nb_idx;
                    statusCopy[  edx ] = voxelStatus[ idx ];
                    setActive( statusCopy[ edx ], true ); 
                return;
            } 
            continue;
        }
        // otherwise, if it's a voxel mapped to either a node or a site
        // need to get distance to its source + the source's distance
        else 
        {   
            // if the source is not an edge node, then this is a LOS voxel mapped to a site 
            // otherwise could be a root node, or it could be a leaf node
            const bool SRC_IS_SITE = srcIsSite( nb_status );

            const float3 sourcePosition = SRC_IS_SITE ?  
                  float3( { sitePos[ nb_src_id * 3 + 0 ], sitePos[ nb_src_id * 3 + 1 ], sitePos[ nb_src_id * 3 + 2 ] } )
                : location3D( index3D( nb_src_id, DIMS ), DIMS );

            const float lx = ( sourcePosition.x - voxelPos.x ) * D_DIMS.x;
            const float ly = ( sourcePosition.y - voxelPos.y ) * D_DIMS.y;
            const float lz = ( sourcePosition.z - voxelPos.z ) * D_DIMS.z;

            const float distToSource = lx*lx + ly*ly + lz*lz;
            const float fullDist = SRC_IS_SITE ? 
                distToSource 
              : distToSource + distances[ nb_idx ];

            if( ( fullDist + DIST_EPSILON ) < distances[ idx ] )
            {
                bool lineOfSite = true;

                const int N_CHECK = round( max( max( abs(lx), abs(ly) ), abs(lz) ) );
                const float dx = lx / N_CHECK;
                const float dy = ly / N_CHECK;
                const float dz = lz / N_CHECK;

                for( int j = 1; j < N_CHECK; ++j )
                {
                    const int64_t ix = idx3d.x + static_cast< int64_t >( round( j * dx ) );
                    const int64_t iy = idx3d.y + static_cast< int64_t >( round( j * dy ) );
                    const int64_t iz = idx3d.z + static_cast< int64_t >( round( j * dz ) );

                    LI3 curr_idx3 = { ix, iy, iz };
                    
                    if( ! inBounds( curr_idx3, DIMS ) ) { 
                        lineOfSite = false;
                        break;
                    }

                    const int64_t curr_idx = flatIndex3D( curr_idx3, DIMS );
                    const int32_t curr_cmp_idx = voxelComponentIds[ curr_idx ];

                    // it it crossed a component boundary
                    // or if we are not doing inner edge resolution and it wants to cross over
                    // a different sources's region
                    // then we stop, note that we have no good reason not to resolve inner edges so far
                    if( ( my_cmp_idx != curr_cmp_idx ) 
                     || ( ( ! RESOSOLVE_INNER_EDGES )
                       && ( isResolved( voxelStatus[ curr_idx ] ) 
                           && ( nb_src_id != sourceMap[ curr_idx ] ) ) ) ) {
                        lineOfSite = false;
                        break;
                    }
                }
                // finally, if we found a new source to remap the voxel to, then track which neighbor held
                // the idx of the new source we will map to, so we can retrieve it in the next update stage 
                if( lineOfSite ) {
                    minNeighbor[ idx ] = i;
                    distCopy[    edx ] = fullDist;
                    siteIdsCopy[ edx ] = siteIds[ nb_idx ];
                    srcIdsCopy[  edx ] = sourceMap[  nb_idx ];
                    statusCopy[  edx ] = voxelStatus[ idx ];
                    setActive(  statusCopy[ edx ], true );      
                    if( SRC_IS_SITE ) {
                        setSrcIsSite( statusCopy[ edx ], true  );
                    }
                }
            }
        }
    }
}

__global__
void finalizeSiteStatus(
    const int64_t     NE,
    const int64_t   * ids,
    const int64_t   * sourceMap,
    uint8_t         * voxelStatus,
    const int32_t   * voxelSiteIds )
{
    const int64_t edx = blockIdx.x * blockDim.x + threadIdx.x;
    if( edx >= NE ) { return; }
    const int64_t idx = ids[ edx ];
    if( voxelSiteIds[ idx ] == BACKGROUND_VX_ID ) { return; }

    if( isActiveOrResolved( voxelStatus[ idx ] ) ) {
        // setResolved( voxelStatus[ idx ], true );

        // voxels go through stages through the different levels of LOE expansion

        /************************************************************************        
             ( neither active nor resolved, then if found a neighbor during stage to map to )
          -> ( active, then at the end of the stage or durring the stage ) 
          -> ( active and resolved, then at the end of the stage or during the stage) 
          -> ( resolved ) the final stage in which the voxel can no longer make any further updates

         ************************************************************************/

        // active and resolved (note previous level of enclosing if statements)
        if( isResolved( voxelStatus[ idx ] ) ) {
            setActive( voxelStatus[ idx ], false );
        } 
        // active and not resolved
        else {
            setResolved( voxelStatus[ idx ], true );
            setActive(   voxelStatus[ idx ], true ); // redundent but good for clarity
        }
    }
}

void CentroidalSampler::classifyLineOfEdge(
    int64_t * unresolvedIds, 
    int64_t * sourceMap_d,
    const int64_t N,
    const int64_t MAX_ITER,
    int64_t & stepsTook )
{
    int nIter = 0;
    int N_START = 10;
    const int IterMax = MAX_ITER == -1 ? 1000 : MAX_ITER; // just in case

    int64_t nChanged = 0;
    int64_t nChangedLast = 0;
    
    //////////////////////////////////////////////////////////////////////////////

    int64_t * srcIdCopy = 0;
    cudaMalloc( (char**) & srcIdCopy, N * sizeof( int64_t ) );

    gpuErrchk( cudaPeekAtLastError() );
    gpuErrchk( cudaDeviceSynchronize() );

    int32_t * siteIdCopy = 0;
    cudaMalloc( (char**) & siteIdCopy, N * sizeof( int32_t ) );

    gpuErrchk( cudaPeekAtLastError() );
    gpuErrchk( cudaDeviceSynchronize() );

    float * distCopy = 0;
    cudaMalloc( (char**) & distCopy, N * sizeof( float ) );

    gpuErrchk( cudaPeekAtLastError() );
    gpuErrchk( cudaDeviceSynchronize() );

    uint8_t * statusCopy = 0;
    cudaMalloc( (char**) & statusCopy, N * sizeof( uint8_t ) );

    gpuErrchk( cudaPeekAtLastError() );
    gpuErrchk( cudaDeviceSynchronize() );

    /////////////////////////////////////////////////////////////////////////////

    while( 1 )
    {
        gpuErrchk( cudaPeekAtLastError() );
        gpuErrchk( cudaDeviceSynchronize() );

        classifyLOEVoxels<<< gridSize( CM_THREADS_PER_BLOCK, N ), CM_THREADS_PER_BLOCK >>>(
            N,
            M_NV,
            m_nSites,
            unresolvedIds,
            sourceMap_d,  
            voxelSiteIds_d,      
            voxelComponentIds_d,
            voxelSiteStatus_d,            
            sitePositions_d,
            data_d,
            distCopy,
            minNeighbors_d,
            siteIdCopy,
            srcIdCopy,
            statusCopy );

        gpuErrchk( cudaPeekAtLastError() );
        gpuErrchk( cudaDeviceSynchronize() );

        // check how many states changed 
        nChanged = thrust::count_if(
            thrust::make_counting_iterator<int64_t>( 0 ),
            thrust::make_counting_iterator<int64_t>( N ),
            ThrustAccessorNotEqualsIndirect<int64_t, int8_t, int64_t>( 
                thrust::device_pointer_cast<int64_t>( unresolvedIds ), 
                thrust::device_pointer_cast<int8_t>(  minNeighbors_d ), 
                VX_MIN_NEIGHBOR_NONE, N, M_NV ) );

        gpuErrchk(   cudaPeekAtLastError() );
        gpuErrchk( cudaDeviceSynchronize() );

        applyLOEUpdate<<< gridSize( CM_THREADS_PER_BLOCK, N ), CM_THREADS_PER_BLOCK >>>(
            N,
            unresolvedIds,
            minNeighbors_d,   

            srcIdCopy,
            siteIdCopy,
            statusCopy,
            distCopy,

            sourceMap_d,     
            voxelSiteIds_d,
            voxelSiteStatus_d,
            data_d );

        gpuErrchk(   cudaPeekAtLastError() );
        gpuErrchk( cudaDeviceSynchronize() );

        //////////////////////////////////////////////////////////////////////////////////
        // stopping condition
        if( nChanged == 0 ) { break; }
    }

    stepsTook = nIter;
    finalizeSiteStatus<<< gridSize( CM_THREADS_PER_BLOCK, N ), CM_THREADS_PER_BLOCK >>>(
        N,
        unresolvedIds,
        sourceMap_d,
        voxelSiteStatus_d,
        voxelSiteIds_d );

    gpuErrchk(   cudaPeekAtLastError() );
    gpuErrchk( cudaDeviceSynchronize() );

    cudaFree( srcIdCopy  ); 
    cudaFree( siteIdCopy );    
    cudaFree( distCopy   );         
    cudaFree( statusCopy );   
}

/************************************************************
    
          Line of Site (Voronoi site) Phase Kernels

************************************************************/

__global__
void classifyLOSVoxels(
    const int64_t     NV,
    const int64_t     NSITES,    
    const bool        hasBackground,
    const int32_t   * voxelComponentIds,
    const float    * sitePositions,
    const int32_t   * voxelSiteIds,
    const uint8_t   * voxelStatus,
    float           * currentDistance, // set by me and not read by other threads    
    int8_t          * minNeighbor )    // set by me and not read by other threads    
{
    const int64_t my_idx = blockIdx.x * blockDim.x + threadIdx.x;
    if( my_idx >= NV ) { return; }
 
    if( isResolved( voxelStatus[ my_idx ] ) ) { 
        minNeighbor[ my_idx ] = VX_MIN_NEIGHBOR_NONE;
        return; 
    }

    const LI3 DIMS       = D_DIMS;
    const LI3 my_idx3    = index3D( my_idx, DIMS );
    const float3 voxelPos = location3D( my_idx3, DIMS );
    minNeighbor[ my_idx ]  = VX_MIN_NEIGHBOR_NONE;

    const int32_t my_cmp_id = voxelComponentIds[ my_idx ];

    for( int i = 1; i < 27; ++i )
    {
        const LI3 nb_idx3 = my_idx3 + ntable[ i ];
        if( ! inBounds( nb_idx3, DIMS ) ) { continue; }
        const int64_t nb_idx = flatIndex3D( nb_idx3, DIMS );
        if( voxelComponentIds[ nb_idx ] != my_cmp_id ) { continue; }
        const int32_t nb_site_id = voxelSiteIds[ nb_idx ];

        // if the neighbor doesn't have a current closest site,
        // or if the neighbor has the same site we have (if we have one)
        // then no need to check this neighbor

        if( ( ! isActiveOrResolved( voxelStatus[ nb_idx ] ) ) ||
            (   isActiveOrResolved( voxelStatus[ nb_idx ] ) && nb_site_id == voxelSiteIds[ my_idx ] ) ) {
            continue;
        }

        const float lx = ( sitePositions[ nb_site_id*3     ] - voxelPos.x ) * D_DIMS.x;
        const float ly = ( sitePositions[ nb_site_id*3 + 1 ] - voxelPos.y ) * D_DIMS.y;
        const float lz = ( sitePositions[ nb_site_id*3 + 2 ] - voxelPos.z ) * D_DIMS.z;

        const float dist = static_cast< float >( lx*lx + ly*ly + lz*lz );
        if( ( dist + DIST_EPSILON ) < currentDistance[ my_idx ] )
        {
            bool lineOfSite = true;

            const float DC = max( max( abs(lx), abs(ly) ), abs(lz) );
            const int N_CHECK = round( DC );

            const float dx = lx / N_CHECK;
            const float dy = ly / N_CHECK;
            const float dz = lz / N_CHECK;

            for( int j = 1; j < N_CHECK; ++j )
            {
                const LI3 curr_idx3 = {
                    my_idx3.x + static_cast< int64_t >( round( j * dx ) ), 
                    my_idx3.y + static_cast< int64_t >( round( j * dy ) ), 
                    my_idx3.z + static_cast< int64_t >( round( j * dz ) ) };
            
                if( ! inBounds( curr_idx3, DIMS ) ) { 
                    lineOfSite = false;
                    break;
                }

                const int64_t curr_idx = flatIndex3D( curr_idx3, DIMS );
                const int32_t curr_cmp = voxelComponentIds[ curr_idx ];

                // intersects boundary between components
                if( ( my_cmp_id != curr_cmp )
                 /* or it crosses a region captured by another site () */
                 /* if we will resolve the inner edges, then this is fine */
                  || ( ( ! RESOSOLVE_INNER_EDGES )
                    && ( isResolved( voxelStatus[ curr_idx ] ) 
                        && ( voxelSiteIds[ curr_idx ] != nb_site_id ) ) ) )
                {
                    lineOfSite = false;
                    break;
                }

                // get upper, lower, forward, and backwards neighbor
                // if all of them are in line of sight, then this one must be too

                // need to expand to work in 3d, and then test whether it helps in 
                // the optimization stage

                // if( curr_idx3.x > 0 && curr_idx3.x < D_DIMS.x - 1 
                //  && curr_idx3.y > 0 && curr_idx3.y < D_DIMS.y - 1 ) 
                // {
                //     const int64_t upFlat    = flatIndex3D( curr_idx3 + LI3( {  0,  1, 0 } ), DIMS );
                //     const int64_t downFlat  = flatIndex3D( curr_idx3 + LI3( {  0, -1, 0 } ), DIMS );
                //     const int64_t leftFlat  = flatIndex3D( curr_idx3 + LI3( { -1,  0, 0 } ), DIMS );
                //     const int64_t rightFlat = flatIndex3D( curr_idx3 + LI3( {  1,  0, 0 } ), DIMS );

                //     if( isActiveOrResolved( voxelStatus[ upFlat    ] ) && voxelSiteIds[ upFlat    ] == nb_site_id
                //     &&  isActiveOrResolved( voxelStatus[ downFlat  ] ) && voxelSiteIds[ downFlat  ] == nb_site_id
                //     &&  isActiveOrResolved( voxelStatus[ leftFlat  ] ) && voxelSiteIds[ leftFlat  ] == nb_site_id
                //     &&  isActiveOrResolved( voxelStatus[ rightFlat ] ) && voxelSiteIds[ rightFlat ] == nb_site_id )
                //     {
                //         lineOfSite = true;
                //         break;
                //     }
                // }      
            }
            if( lineOfSite )
            {
                currentDistance[ my_idx ] = dist;
                minNeighbor[ my_idx ] = i;
            }
        }
    }
}

__global__
void initializeVoxelStates(
    const int64_t NV,
    const int32_t   * voxelComponentIds,
    const bool hasBackground,
    int32_t * voxelSiteIds,
    uint8_t * voxelStatus,
    int8_t * minNeighbor,    
    float * currentDistance,
    int64_t * sourceMap )
{
    const int64_t idx = blockIdx.x * blockDim.x + threadIdx.x;
    if( idx >= NV ) { return; }

    clearFlags( voxelStatus[ idx ] );

    if( hasBackground && voxelComponentIds[ idx ] == 0 ) {
        setResolved( voxelStatus[ idx ], true );
        voxelSiteIds[ idx ] = BACKGROUND_VX_ID;
    }
    else {
        voxelSiteIds[ idx ]    = VX_SOURCE_NONE;
        currentDistance[ idx ] = 9999999999.0;
        minNeighbor[ idx ]     = VX_MIN_NEIGHBOR_NONE;
    }

    sourceMap[ idx ] = VX_SOURCE_NONE;
}

__global__
void applyLOSUpdate(
    const int64_t  NV,
    const int64_t  NS,
    const int ITER,
    const int8_t    * minNeighbor,    
    const float     * currentDistance,
    const int32_t   * voxelSiteIds,    //  read
    int64_t         * voxelSiteIdsTMP, // write 
    uint8_t         * voxelStatus )    // 
{
    const int64_t idx = blockIdx.x * blockDim.x + threadIdx.x;
    if( idx >= NV ) { return; }
    if( isResolved( voxelStatus[ idx ] ) ) { return; }

    if( minNeighbor[ idx ] != VX_MIN_NEIGHBOR_NONE )
    {
        const LI3 DIMS = D_DIMS;
        const LI3 idx3d = index3D( idx, DIMS );
        const LI3 n_idx3 = idx3d + ntable[ minNeighbor[ idx ] ];
        const int64_t nb_idx = flatIndex3D( n_idx3, DIMS );
        voxelSiteIdsTMP[ idx ] = voxelSiteIds[ nb_idx ];

        // in this stage, the source is always a site
        if( ! isActive(  voxelStatus[ idx ] ) ) { 
            setActive(    voxelStatus[ idx ], true );
            setSrcIsSite( voxelStatus[ idx ], true ); 
        }
    }

    if( isActive( voxelStatus[ idx ] ) && ( currentDistance[ idx ] ) <= ITER * ITER + DIST_EPSILON ) {     
        setResolved( voxelStatus[ idx ], true );
    }
}

__global__
void updateSiteIds(
    const int64_t  NV,
    const int8_t    * minNeighbor,    
     int32_t        * voxelSiteIds,    // write
    const int64_t   * voxelSiteIdsTMP ) // read
{
    const int64_t idx = blockIdx.x * blockDim.x + threadIdx.x;
    if( idx >= NV ) { return; }
    if( minNeighbor[ idx ] != VX_MIN_NEIGHBOR_NONE ) {
        voxelSiteIds[ idx ] = static_cast<int32_t>( voxelSiteIdsTMP[ idx ] );
    }
}

__global__
void applyLOSFinalUpdate(
    const int64_t  NV,
    const int32_t * siteIds,
    uint8_t       * voxelStatus )
{
    const int64_t idx = blockIdx.x * blockDim.x + threadIdx.x;
    if( idx >= NV ) { return; }
    if( siteIds[ idx ] == BACKGROUND_VX_ID ) { return; }

    if( isActive( voxelStatus[ idx ] ) || isResolved( voxelStatus[ idx ]  ) ) {
        // // set the resolved flag
        setResolved(   voxelStatus[ idx ], true );
        // set the is line of site (Voronoi Site) flag
        setLineOfSite( voxelStatus[ idx ], true );
    }
}

__global__
void classifySingleSiteComponents(
    const int64_t  NV,
    const int32_t * voxelComponents,
    const int32_t * componentSites,
    const bool hasBackground,
    int32_t * voxelSiteMap,
    uint8_t * voxelStatus )
{
    const int64_t idx = blockIdx.x * blockDim.x + threadIdx.x;
    if( idx >= NV ) { return; }

    const int32_t vx_cmp = hasBackground ? voxelComponents[ idx ] : voxelComponents[ idx ] - 1;
    const int32_t cmp_site = componentSites[ vx_cmp ];

    if( cmp_site >= 0 ) {
        voxelSiteMap[ idx ] = cmp_site;
        setResolved( voxelStatus[ idx ], true );
    }
}

__global__
void classifySeedVoxels(
    const int64_t     NS,
    const float    * sitePositions,
    const int32_t   * siteComponents,
    const int64_t   * componentSiteCounts,        
    const int32_t   * voxelComponents,
    const bool        hasBackground, 
    const bool        simpleSingleSiteCMPHeuristic,
    float           * currentDistance,
    int32_t         * componentSitesSingle,
    int32_t         * voxelSiteIds,
    uint8_t         * voxelStatus,
    int64_t         * srcMap )
{
    const int64_t idx = blockIdx.x * blockDim.x + threadIdx.x;
    if( idx >= NS ) { return; }

    const LI3 DIMS = D_DIMS;

    const int64_t xi = locationInverse1D( sitePositions[ idx * 3 + 0 ], DIMS.x );
    const int64_t yi = locationInverse1D( sitePositions[ idx * 3 + 1 ], DIMS.y );
    const int64_t zi = locationInverse1D( sitePositions[ idx * 3 + 2 ], DIMS.z );
    const int64_t vIdx = flatIndex3D( { xi, yi, zi }, DIMS );

    // note that because of the way the sites were initially distributed,  
    // there should never be two sites within the same voxel after inititialize distribution
    // so this cannot result in a race condition below in normal cirucmstances

    voxelSiteIds[ vIdx ] = idx;
    setResolved( voxelStatus[  vIdx ], true );

    currentDistance[ vIdx ] = 0;
    setSrcIsSite( voxelStatus[  vIdx ], true );
    
    srcMap[ vIdx ] = idx;

    if( simpleSingleSiteCMPHeuristic )
    {
        const int32_t siteCMP = hasBackground ? siteComponents[ idx ] : siteComponents[ idx ] - 1;
        if( componentSiteCounts[  siteCMP ] == 1 ) {
            componentSitesSingle[ siteCMP ] = idx;
        } 
    }
}

void CentroidalSampler::classifyLineOfSite( 
    int64_t * srcMap_d,  
    const int64_t MAX_ITER, 
    int64_t & stepsTook )
{
    int nIter = 0;
    int64_t nChanged = -1;
    int64_t nChangedLast = -1;
    while( true )
    {
        classifyLOSVoxels<<< gridSize( VX_THREADS_PER_BLOCK, M_NV ), VX_THREADS_PER_BLOCK >>>(
            M_NV,
            m_nSites,
            m_hasBackground,
            voxelComponentIds_d,
            sitePositions_d,
            voxelSiteIds_d,
            voxelSiteStatus_d,
            data_d,            
            minNeighbors_d );
        
        gpuErrchk( cudaPeekAtLastError() );
        gpuErrchk( cudaDeviceSynchronize() );

        /**************************************************************************************/

        // update the states, site mapping, and distances
        applyLOSUpdate<<< gridSize( VX_THREADS_PER_BLOCK, M_NV ), VX_THREADS_PER_BLOCK >>>(
            M_NV,
            m_nSites,
            nIter,
            minNeighbors_d,
            data_d,
            voxelSiteIds_d,       // read from
            srcMap_d,             // write to
            voxelSiteStatus_d );

        gpuErrchk(   cudaPeekAtLastError() );
        gpuErrchk( cudaDeviceSynchronize() );

       // check how many states changed 
        nChanged = thrust::count_if(
            thrust::device_pointer_cast<int8_t>( minNeighbors_d ),
            thrust::device_pointer_cast<int8_t>( minNeighbors_d ) + M_NV,
            ( thrust::placeholders::_1 != VX_MIN_NEIGHBOR_NONE ) );

        updateSiteIds<<< gridSize( VX_THREADS_PER_BLOCK, M_NV ), VX_THREADS_PER_BLOCK >>>(
            M_NV,
            minNeighbors_d,
            voxelSiteIds_d,
            srcMap_d );

        gpuErrchk( cudaPeekAtLastError() );
        gpuErrchk( cudaDeviceSynchronize() );

        // stopping condition
        if( nChanged == 0 ) { break; }
        
        if( nChangedLast == nChanged ) { break; }
        nChangedLast = nChanged;

        ++nIter;
        if( MAX_ITER > 0 && nIter >= MAX_ITER ) { break; }   
    }

    stepsTook = nIter;

    applyLOSFinalUpdate<<< gridSize( VX_THREADS_PER_BLOCK, M_NV ), VX_THREADS_PER_BLOCK >>>(
        M_NV,
        voxelSiteIds_d,
        voxelSiteStatus_d );

    gpuErrchk( cudaPeekAtLastError() );
    gpuErrchk( cudaDeviceSynchronize() );
}

void CentroidalSampler::geodesicVoronoiClassification() {
    int64_t losStepsTook, loeStepsTook;
    geodesicVoronoiClassification( 
           -1,        // -1 -> no limit to steps in LOS classification process
           -1,        // -1 -> no limit to steps in LOE classification process
        false,        // parameter to override LOE stage
        false,        // simple heuristic for components with only one site (pre-classify and move only once) 
        losStepsTook, // recieve back the number of steps taken
        loeStepsTook  // recieve back the number of steps taken
    );
}

void CentroidalSampler::geodesicVoronoiClassification( 
    const int64_t losStepMax, // -1 implies no limit, used to be able to step through the process and see intermediate results
    const int64_t loeStepMax, // -1 implies no limit, used to be able to step through the process and see intermediate results
    bool overrideLineOfEdge,  // used to be able to step through the process and see intermediate results
    bool simpleSingleSiteCMPHeuristic,
    int64_t & losStepsTook,   // used to be able to step through the process and see intermediate results
    int64_t & loeStepsTook )  // used to be able to step through the process and see intermediate results
{
    std::cout << "\n" << "Computing geodesic Voronoi " << std::endl;

    // reused under new name
    int64_t * sourceMap_d = voxelIndicesSorted_d;

    m_overrideLineOfEdge = overrideLineOfEdge;
    m_simpleSingleSiteCMPHeuristic = simpleSingleSiteCMPHeuristic;

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////

    initializeVoxelStates<<< gridSize( VX_THREADS_PER_BLOCK, M_NV ), VX_THREADS_PER_BLOCK >>>(
        M_NV,
        voxelComponentIds_d,
        m_hasBackground,            
        voxelSiteIds_d,
        voxelSiteStatus_d,
        minNeighbors_d,
        data_d,
        sourceMap_d );

    gpuErrchk( cudaPeekAtLastError() );
    gpuErrchk( cudaDeviceSynchronize() );

    int32_t * cmpSiteIds_d = 0;
    cudaMalloc( (char**) & cmpSiteIds_d, m_NC * sizeof( int32_t ) );
    thrust::fill( 
        thrust::device_pointer_cast<int32_t>( cmpSiteIds_d ),
        thrust::device_pointer_cast<int32_t>( cmpSiteIds_d ) + m_NC,
        -1 );

    classifySeedVoxels<<< gridSize( CM_THREADS_PER_BLOCK, m_nSites ), CM_THREADS_PER_BLOCK >>>(
        m_nSites,
        sitePositions_d,
        siteComponents_d,
        componentSiteCounts_d,
        voxelComponentIds_d,
        m_hasBackground,
        simpleSingleSiteCMPHeuristic,
        data_d,
        cmpSiteIds_d,
        voxelSiteIds_d,
        voxelSiteStatus_d,
        sourceMap_d );

    gpuErrchk( cudaPeekAtLastError() );
    gpuErrchk( cudaDeviceSynchronize() );

    if( simpleSingleSiteCMPHeuristic ){
        classifySingleSiteComponents<<< gridSize( CM_THREADS_PER_BLOCK, M_NV ), CM_THREADS_PER_BLOCK >>>(
            M_NV,
            voxelComponentIds_d,
            cmpSiteIds_d,
            m_hasBackground,
            voxelSiteIds_d,
            voxelSiteStatus_d );
    }

    gpuErrchk( cudaPeekAtLastError() );
    gpuErrchk( cudaDeviceSynchronize() );

    cudaFree( cmpSiteIds_d );

    ////////////////////////////////////////////////////////////////////////////////////

    classifyLineOfSite( sourceMap_d, losStepMax, losStepsTook );

    int64_t nUnresolved = thrust::count_if(
        thrust::device_pointer_cast<uint8_t>( voxelSiteStatus_d ),
        thrust::device_pointer_cast<uint8_t>( voxelSiteStatus_d ) + M_NV,
        ( thrust::placeholders::_1 & VX_RESOLVED_FLAG ) == 0 );

    int64_t nUnresolvedLast = 0;

    ////////////////////////////////////////////////////////////////////////////////////

    std::cout << "LOS phase done: " << nUnresolved << " of " << M_NV << " left \n";

    if( overrideLineOfEdge ) {

        std::cout << "overriding line of edge stage" << std::endl;

        // if only outer edges mattered, then could do this over the unresolved ids instead of all of
        classifyEdgeSources<<< gridSize( CM_THREADS_PER_BLOCK, M_NV ), CM_THREADS_PER_BLOCK >>>(
            0,
            M_NV,
            voxelComponentIds_d,
            voxelSiteIds_d,
            voxelSiteStatus_d );

        gpuErrchk( cudaPeekAtLastError() );
        gpuErrchk( cudaDeviceSynchronize() );

        return;
    }
    else{
        std::cout << "Beginning LOE phase\n";
    }

    // recursive edge wise classification
    if( nUnresolved != 0  )
    {
        int64_t * uUnresolvedIds = 0;
        cudaMalloc( (char**) & uUnresolvedIds, nUnresolved * sizeof( int64_t ) );

        int niter = 0;     
        while( nUnresolved != 0 )
        {
            // classify the edge sources (voxels which neighbor an unresolved voxel in same component) (outer edge)
            // or resolved | active voxel with a different site id (inner edge)
        // if only outer edges mattered, then could do this over the unresolved ids instead of all of        /
            classifyEdgeSources<<< gridSize( CM_THREADS_PER_BLOCK, M_NV ), CM_THREADS_PER_BLOCK >>>(
                niter,
                M_NV,
                voxelComponentIds_d,
                voxelSiteIds_d,
                voxelSiteStatus_d );

            gpuErrchk( cudaPeekAtLastError() );
            gpuErrchk( cudaDeviceSynchronize() );

            // if the site status has the resolved flag set to 0, then it's unresolved
            // copy each voxel index where the corresponding voxel is unresolved
            thrust::copy_if(
                thrust::make_counting_iterator<int64_t>( 0 ),
                thrust::make_counting_iterator<int64_t>( M_NV ),
                thrust::device_pointer_cast( voxelSiteStatus_d ),
                thrust::device_pointer_cast( uUnresolvedIds ),
                ( thrust::placeholders::_1 & VX_RESOLVED_FLAG ) == 0 ); 

            gpuErrchk( cudaPeekAtLastError() );
            gpuErrchk( cudaDeviceSynchronize() );

            ////////////////////////////////////////////////////////////////////////////////////

            classifyLineOfEdge( 
                uUnresolvedIds, 
                sourceMap_d, 
                nUnresolved, 
                loeStepMax, 
                loeStepsTook );

            // now want to count how many are not resolved
            nUnresolved = thrust::count_if(
                thrust::device_ptr< int64_t >( uUnresolvedIds ), 
                thrust::device_ptr< int64_t >( uUnresolvedIds ) + nUnresolved, 
                check_bit_indirect( 
                    thrust::device_pointer_cast( voxelSiteStatus_d ), 
                    VX_RESOLVED_FLAG,
                    false ) );

            if( nUnresolvedLast == nUnresolved ) { break; }
            nUnresolvedLast = nUnresolved;
            ++niter;
        }

        cudaFree( uUnresolvedIds );
    }

    // classifyEdgeSources<<< gridSize( CM_THREADS_PER_BLOCK, M_NV ), CM_THREADS_PER_BLOCK >>>(
    //     1,
    //     M_NV,
    //     voxelComponentIds_d,
    //     voxelSiteIds_d,
    //     voxelSiteStatus_d );
}