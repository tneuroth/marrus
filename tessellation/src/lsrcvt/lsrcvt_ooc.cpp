
#include "lsrcvt_lib.hpp"
#include "configuration.hpp"
#include "utils/IndexUtils.hpp"
#include "algorithms/ConnectedComponentsCPU.hpp"

#include <cstdint>
#include <vector>
#include <fstream>
#include <iostream>
#include <chrono>
#include <string>

using namespace std::chrono;
using namespace std;

template< typename T >
void writeData( 
    const string & filePath,
    const vector< T > & data )
{
    cout << "Writing data to " <<  filePath << endl;
    ofstream outFile( filePath, ios::out | ios::binary );
    if( ! outFile.is_open() )
    {
        cerr << "file could not be opened for writing" << filePath << endl;
        exit( 1 );
    }
    outFile.write( ( char* ) data.data(), data.size() * sizeof( T )  );
    outFile.close();
}

template< typename T >
void loadData( 
    const string & filePath,
    vector< T > & data, 
    int64_t offset, 
    int64_t numElements )
{
    ifstream inFile( filePath, ios::in | ios::binary );
    if( ! inFile.is_open() ) {
        cout << "could not be opened for reading " << filePath << endl;
        exit( 1 );
    }
    data.resize( numElements );
    inFile.seekg( offset * sizeof( T ) );
    inFile.read( (char*) data.data(), numElements * sizeof( T ) ); 
    inFile.close();
}

int main(int argc, char** argv)
{
    /**************************************************

                         Parameters

    ***************************************************/

    if( argc != 7 ) {
        cerr << "program expects argument <configuration file path> <bx> <by> <bz> <doMerge=0|1>"  << endl;
        cerr << "Where b is the domain decomposition block size for ooc processing."               << endl;
        cerr << "And doMerge is 1 to merge blocks or 0 to not merge blocks"                        << endl;
        exit( 1 );
    }

    LSRCVD_Configuration configuration( argv[ 1 ] );

    if( ! configuration.isValid() ) {
        cerr << "Configuration " << argv[ 1 ] << " not valid\n";
        cerr << configuration.getMessage();
    }

    const int64_t BX = stoul( argv[ 2 ] );
    const int64_t BY = stoul( argv[ 3 ] );
    const int64_t BZ = stoul( argv[ 4 ] );

    // required parameters

    const auto dims              = configuration.dims;
    const auto dataFile          = configuration.dataFile;
    const auto dataFormat        = configuration.dataFormat;
    const auto dataOffset        = configuration.dataOffset;    
    const auto layers            = configuration.layers;
    const auto siteDensity       = configuration.siteDensity;
    const auto outPath           = configuration.outputDirectory;
    const auto stoppingCondition = configuration.stoppingCondition;
    const auto maxIterations     = configuration.maxIterations;
    const auto validate          = configuration.validate;
    const auto verbose           = configuration.verbose;

    // additional require parameters for the weighted case

    const auto massFile    = configuration.massFile;
    const auto massFormat  = configuration.massFormat;
    const auto massOffset  = configuration.massOffset;

    const auto attractionBias = configuration.attractionBias;

    // optional parameters for testing and animation

    const auto outputCentroidalUpdates  = configuration.outputCentroidalUpdates;
    const auto maxLOESteps              = configuration.maxLOESteps;
    const auto maxLOSSteps              = configuration.maxLOSSteps;    
    const auto outputLOSSteps           = configuration.outputLOSSteps;
    const auto outputLOESteps           = configuration.outputLOESteps;
    const auto overrideLOE              = configuration.overrideLOE;

    /**************************************************

                        Load Data

    ***************************************************/

    const int64_t N = dims[ 0 ] * dims[ 1 ] * dims[ 2 ];
    vector< float > data( N );

    if( dataFormat == "double" )
    {
        vector< double > tmp( N );
        loadData( dataFile, tmp, N*dataOffset, N  );      
        data = std::vector< float >( tmp.begin(), tmp.end() );  
    } 
    else 
    {
        loadData( dataFile, data, N*dataOffset, N );
    }

    /*********************************************************************/
    // global data

    int NCC;

    std::vector< int16_t > voxelLayerIds(     N );
    std::vector< int32_t > voxelComponentIds( N );
    std::vector< int32_t > voxelSiteIds(      N );

    std::vector< int32_t > siteComponentIds; 
    std::vector< int16_t > siteLayerIds;
    std::vector< float   > sitePositions;

    size_t n_sites_global = 0;

    /*********************************************************************/
    // get the global connected components 

    TN::mapLayers( 
        N, 
        data.data(), 
        layers, 
        voxelLayerIds.data() );

    TN::connectedComponents(
        voxelLayerIds,
        dims,
        N / 8,
        voxelComponentIds,
        NCC );

    bool valid = TN::verifyComponents(
        voxelComponentIds,
        voxelLayerIds,
        NCC,
        { dims[ 0 ], dims[ 1 ], dims[ 2 ] },
        true );

    /*********************************************************************/

    /*************************************************

                   Domain Decomposition

    ***************************************************/

    const int64_t NBX = dims[ 0 ] / BX;
    const int64_t NBY = dims[ 1 ] / BY;
    const int64_t NBZ = dims[ 2 ] / BZ;

    for( int64_t bx = 0; bx < NBX; ++bx )
    for( int64_t by = 0; by < NBY; ++by )
    for( int64_t bz = 0; bz < NBZ; ++bz )
    {
        const std::array< int64_t, 3 > I_0 = { bx*BX, by*BY, bz*BZ };

        const std::array< int64_t, 3 > bDims = {  
            max( (bx+1)*BZ, dims[ 0 ] ),
            max( (bx+1)*BZ, dims[ 1 ] ),
            max( (bx+1)*BZ, dims[ 2 ] )
        };

        const int64_t BN = bDims[ 0 ] * bDims[ 1 ] * bDims[ 2 ];

        std::vector< float > blockData( BN );

        /// copy data to the block

        for( int64_t ix = 0; ix < bDims[ 0 ]; ++ix )
        for( int64_t iy = 0; iy < bDims[ 1 ]; ++iy )
        for( int64_t iz = 0; iz < bDims[ 2 ]; ++iz )
        {
            const int64_t L_IDX = TN::flatIndex3dCM( ix, iy, iz, bDims[ 0 ], bDims[ 1 ], bDims[ 2 ] ); 

            const int64_t G_IDX = TN::flatIndex3dCM( 
                I_0[ 0 ] + ix, I_0[ 1 ] + iy, I_0[ 2 ] + iz, 
                dims[ 0 ], dims[ 0 ], dims[ 0 ] );


            blockData[ L_IDX ] = data[ G_IDX ];
        }

        /**************************************************

                         LRV Decomposition

        ***************************************************/

        CentroidalSampler sampler( bDims );
        
        sampler.setValidate( validate );
        sampler.setVerbose(   verbose );

        sampler.computeLayerDecomposition( blockData.data(), layers );   
        sampler.initializeSiteDistribution( siteDensity, false, outPath );

        vector< float > maxDeltas;
        vector< float > meanDeltas;    

        int64_t losStepsTook = 0;
        int64_t loeStepsTook = 0;   

        for( int i = 0; i < maxIterations;++i ) {

            const string iterStr      = to_string( i ); 
            const string iterStrZfill = string( 5 - iterStr.size(), '0' ) + iterStr;

            // Normal Full Stage ////////////////////////////////////////////////////////////////////////////////

            sampler.geodesicVoronoiClassification( 
                -1 ,             // -1 -> no limit to steps in LOS classification process
                -1,              // -1 -> no limit to steps in LOE classification process
                overrideLOE,     // parameter to override LOE stage
                false,            // simple heuristic for components with only one site (pre-classify and move only once) 
                losStepsTook,    // recieve back the number of steps taken
                loeStepsTook     // recieve back the number of steps taken
            );

            CentroidalUpdateStatistics stats = sampler.geodesicCentroidalUpdate();

            maxDeltas.push_back(   stats.maxDelta );
            meanDeltas.push_back( stats.meanDelta );

            cout << "ITER=" << i << ", MAX_DELTA=" << stats.maxDelta << ", MEAN_DELTA=" << stats.meanDelta  <<  endl; 

            if( stoppingCondition.first == "meanDelta" ) {
                if( stats.meanDelta  < stoppingCondition.second ) {
                    std::cout << "Stopping condition meanDelta = " 
                              << stats.meanDelta << " < " 
                              << stoppingCondition.second 
                              << " met." << std::endl;
                    break;
                }
            } 
            else if( stoppingCondition.first == "maxDelta" ) {
                if( stats.maxDelta < stoppingCondition.second ) {
                    std::cout << "Stopping condition maxDelta = " 
                              << stats.maxDelta << " < " 
                              << stoppingCondition.second 
                              << " met." << std::endl;     
                    break;
                }
            }
        }

        sampler.geodesicVoronoiClassification( 
            -1 ,             // -1 -> no limit to steps in LOS classification process
            -1,              // -1 -> no limit to steps in LOE classification process
            overrideLOE,     // parameter to override LOE stage
            false,            // simple heuristic for components with only one site (pre-classify and move only once) 
            losStepsTook,    // recieve back the number of steps taken
            loeStepsTook     // recieve back the number of steps taken
        );

        sampler.finalize();


        /**********************************************************************

            Translate the result from the local context to the global context

        ***********************************************************************/

        const auto & localSitesPositions  = sampler.getSitePositions();
        const auto & localSitesComponents = sampler.getSiteComponentIds();
        const auto & localSitesLayers     = sampler.getSiteLayers();
        const auto & localVoxelSites      = sampler.getClassification();

        const size_t N_SITES_LOCAL = localVoxelSites.size();

        // shift the voxel site ids and copy the block to the global data

        for( int64_t ix = 0; ix < bDims[ 0 ]; ++ix )
        for( int64_t iy = 0; iy < bDims[ 1 ]; ++iy )
        for( int64_t iz = 0; iz < bDims[ 2 ]; ++iz )
        {
            const int64_t L_IDX = TN::flatIndex3dCM( ix, iy, iz, bDims[ 0 ], bDims[ 1 ], bDims[ 2 ] ); 

            const int64_t G_IDX = TN::flatIndex3dCM( 
                I_0[ 0 ] + ix, I_0[ 1 ] + iy, I_0[ 2 ] + iz, 
                dims[ 0 ], dims[ 0 ], dims[ 0 ] );


            voxelSiteIds[ G_IDX ] = localVoxelSites[ L_IDX ] + n_sites_global;
        }

        // shift site positions and concatenate them to the full data
        for( size_t i = 0; i < N_SITES_LOCAL; ++i )
        {
            siteLayerIds.push_back( localSitesLayers[ i ] );

            sitePositions.push_back(
                localSitesPositions[ i * 3 + 0 ]
            );
            sitePositions.push_back(
                localSitesPositions[ i * 3 + 1 ]
            );
            sitePositions.push_back(
                localSitesPositions[ i * 3 + 2 ]
            );
        }


        n_sites_global += localSitesLayers.size();
    }
}
