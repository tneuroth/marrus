
#include "lsrcvt_lib.hpp"

#include "utils/IndexUtils.hpp"
#include "algorithms/Standard/MyAlgorithms.hpp"
#include "geometry/Vec.hpp"

#include "utility.cuh"
#include "massFunction.cuh"

#include <thrust/reduce.h>
#include <thrust/execution_policy.h>
#include <thrust/device_ptr.h>
#include <thrust/iterator/discard_iterator.h>
#include <thrust/functional.h>
#include <thrust/transform_scan.h>
#include <thrust/device_vector.h>
#include <thrust/device_ptr.h>
#include <thrust/iterator/permutation_iterator.h>
#include <thrust/partition.h>

#include <cuda.h>
#include <curand.h>
#include <curand_kernel.h>

#include <cstdint>
#include <cfloat>
#include <chrono>
#include <cstdio>
#include <set>
#include <fstream>

using namespace std::chrono;

inline __host__ __device__ long3 operator+( const long3 a, const long3 b)
{
    return make_long3( a.x + b.x, a.y + b.y, a.z + b.z );
}

__device__
float voxelDist( 
    const long3 v1, 
    const long3 v2 )
{
    const float dx = ( v1.x - v2.x );
    const float dy = ( v1.y - v2.y );
    const float dz = ( v1.z - v2.z );
    // return sqrt( dx*dx + dy*dy + dz*dz );    
    return dx*dx + dy*dy + dz*dz;
}

void normalizeCenterOfMass2(
    const double * netMass,
    const double * centerOfMass,
    const int32_t   * siteComponents,
    const int32_t   * vxCmp,
    const int32_t   * fieldDims,
    double       * sites,
    double       * siteDeltas,
    const int N_DIMS,
    const int N_CELLS )
{
    const int64_t idx = blockIdx.x * blockDim.x + threadIdx.x;
    const double MASS_EPSILON = 0.0000000000001;

    if( idx < N_CELLS )
    {
        if( netMass[ idx ] > MASS_EPSILON )
        {
            double newPos[ 3 ];

            for( int i = 0; i < N_DIMS; ++i )
            {
                newPos[ i ] = centerOfMass[ idx * N_DIMS + i ] / netMass[ idx ];
            }

            const double x = newPos[ 0 ];
            const double y = newPos[ 1 ];
            const double z = newPos[ 2 ];

            const int64_t xi = locationInverse1D( x, fieldDims[ 0 ] );
            const int64_t yi = locationInverse1D( y, fieldDims[ 1 ] );
            const int64_t zi = locationInverse1D( z, fieldDims[ 2 ] );

            const int64_t vIdx = flatIndex3D(
                xi,
                yi,
                zi,
                fieldDims[ 0 ],
                fieldDims[ 1 ],
                fieldDims[ 2 ] );

            if( siteComponents[ idx ] == vxCmp[ vIdx ] )
            {
                const double dx  = ( newPos[ 0 ] - sites[ idx * N_DIMS + 0 ] ) * fieldDims[ 0 ];
                const double dy  = ( newPos[ 1 ] - sites[ idx * N_DIMS + 1 ] ) * fieldDims[ 1 ];
                const double dz  = ( newPos[ 2 ] - sites[ idx * N_DIMS + 2 ] ) * fieldDims[ 2 ];

                siteDeltas[ idx ]   = sqrt( dx*dx + dy*dy + dz*dz );

                for( int i = 0; i < N_DIMS; ++i )
                {
                    sites[ idx * N_DIMS + i ] = newPos[ i ];
                }
            }
        }
    }
}

///////////////////////////////////////////////////////////////////////////////

__global__
void computeNearestVoxelToCentroid(
    const int64_t NR, // number of site regions to computer over
    const int64_t * offsets,
    const int32_t * fieldDims,
    const int64_t * regionVoxelIds,    
    const int32_t * regionVoxelIdsSiteIds,    
    double  * sitePositions,
    double  * siteDeltas )
{
    const int64_t idx = blockIdx.x * blockDim.x + threadIdx.x;

    if( idx >= NR )
    {
        return;
    }

    const int64_t first = offsets[ idx ];
    const int64_t last = idx < ( NR - 1 ) ? offsets[ idx + 1 ] : NR;    
    const int64_t siteId = regionVoxelIdsSiteIds[ regionVoxelIds[ first ] ];

    printf( "%d ", siteId );

    const double3 siteCentroid = make_double3(
        sitePositions[ siteId * 3 + 0 ],
        sitePositions[ siteId * 3 + 1 ],
        sitePositions[ siteId * 3 + 2 ] );

    int64_t minIndex   = first;
    double minDistance = 999999999.0;
    double3 minVoxelPosition;

    for( int64_t i = first; i < last; ++i )
    {
        // get the voxel position

        const int64_t voxelId = regionVoxelIds[ i ];

        const long3 idx3d =
            index3D(
                voxelId,
                fieldDims[ 0 ],
                fieldDims[ 1 ],
                fieldDims[ 2 ] );

        const double3 voxelPos =
            location3D(
                idx3d.x,
                idx3d.y,
                idx3d.z,
                fieldDims[ 0 ],
                fieldDims[ 1 ],
                fieldDims[ 2 ] );

        // get distance to the centroid

        const double dx  = ( siteCentroid.x - voxelPos.x ) * fieldDims[ 0 ];
        const double dy  = ( siteCentroid.y - voxelPos.y ) * fieldDims[ 1 ];
        const double dz  = ( siteCentroid.z - voxelPos.z ) * fieldDims[ 2 ];
        const double d = sqrt( dx*dx + dy*dy + dz*dz );

        // update min distance

        if( d < minDistance )
        {
            minDistance = d;
            minIndex = i;
            minVoxelPosition = voxelPos;
        }
    }

    // update centroid position to a relaxed centroidal position within the boundary

    sitePositions[ siteId * 3 + 0 ] = minVoxelPosition.x;
    sitePositions[ siteId * 3 + 1 ] = minVoxelPosition.y;
    sitePositions[ siteId * 3 + 2 ] = minVoxelPosition.z;

    printf( "md=%f ", minDistance ); 
    siteDeltas[ idx ] = minDistance;
}

/////////////////////////////////////////////////////////////////////////////////////////////////

__constant__ long3 ntable[ 27 ] =
{
    { 0,0,0}, { 0,0,-1}, { 0,-1,-1}, { 0,-1,0}, { 0,-1,1}, { 0,0,1}, { 0,1,1}, { 0,1,0}, { 0,1,-1},
    {-1,0,0}, {-1,0,-1}, {-1,-1,-1}, {-1,-1,0}, {-1,-1,1}, {-1,0,1}, {-1,1,1}, {-1,1,0}, {-1,1,-1},
    { 1,0,0}, { 1,0,-1}, { 1,-1,-1}, { 1,-1,0}, { 1,-1,1}, { 1,0,1}, { 1,1,1}, { 1,1,0}, { 1,1,-1}
};

__constant__ long3 ntable2[ 7 ] =
{
    { 0,0,0 }, { 0,0,-1}, { 0,-1,0}, {-1,0,0 }, { 0,0,1}, { 0,1,0},  { 1,0,0 }
};

__global__
void initializeSources(
    const int64_t NV,
    int32_t * cellMap,
    int8_t * voxelStatus )
{
    const int64_t idx = blockIdx.x * blockDim.x + threadIdx.x;
    if( idx >= NV )
    {
        return;
    }
    if( ( voxelStatus[ idx ] & VX_RESOLVED ) || ( voxelStatus[ idx ] & VX_ACTIVE ) )
    {
        voxelStatus[ idx ] = VX_RESOLVED | VX_IS_SRC;
    }
}

__global__
void initializeRegions(
    const int64_t     NS,
    const double    * sitePositions,
    const int32_t   * siteComponents,
    const int32_t   * voxelComponents,
    const int32_t   * dims,
    float       * currentDistance,
    int32_t         * voxelCellMap,
    int8_t          * voxelStatus )
{
    const int64_t idx = blockIdx.x * blockDim.x + threadIdx.x;
    if( idx >= NS )
    {
        return;
    }

    const double x = sitePositions[ idx * 3 + 0 ];
    const double y = sitePositions[ idx * 3 + 1 ];
    const double z = sitePositions[ idx * 3 + 2 ];

    const int64_t xi = locationInverse1D( x, dims[ 0 ] );
    const int64_t yi = locationInverse1D( y, dims[ 1 ] );
    const int64_t zi = locationInverse1D( z, dims[ 2 ] );

    const int64_t vIdx = flatIndex3D(
                             xi,
                             yi,
                             zi,
                             dims[ 0 ],
                             dims[ 1 ],
                             dims[ 2 ] );


    voxelCellMap[ vIdx ]    = idx;
    voxelStatus[  vIdx ]    = VX_RESOLVED;
    currentDistance[ vIdx ] = 0;
}

__global__
void initializeUnresolved(
    const int64_t NE,
    int64_t * unresolvedIds,
    int32_t * voxelCellMap,
    int64_t * sourceMap )
{
    const int64_t edx = blockIdx.x * blockDim.x + threadIdx.x;
    if( edx >= NE )
    {
        return;
    }

    int64_t idx = unresolvedIds[ edx ];
    voxelCellMap[ idx ] = edx;
    sourceMap[ edx ] = -1;
}

__global__
void initCellMap(
    const int64_t NV,
    const int32_t   * voxelComponentIds,
    const bool hasBackground,
    int32_t * voxelCellMap,
    int8_t * voxelStatus,
    int8_t * minNeighbor,    
    float * currentDistance )
{
    const int64_t idx = blockIdx.x * blockDim.x + threadIdx.x;
    if( idx >= NV )
    {
        return;
    }

    if( hasBackground && voxelComponentIds[ idx ] == 0 )
    {
        voxelStatus[ idx ]  = VX_RESOLVED;
        voxelCellMap[ idx ] = BACKGROUND_VX;
    }
    else
    {
        voxelStatus[ idx ]     = VX_UNCLASSIFIED;
        voxelCellMap[ idx ]    = -1;
        currentDistance[ idx ] = 999999999.0;
        minNeighbor[ idx ]     = -1;
    }
}

__global__
void updateRegion2(
    const int64_t  NV,
    const int ITER,
    const int32_t   * fieldDims,
    int8_t          * minNeighbor,    
    const float     * currentDistance,
    int32_t         * voxelCellMap,
    int8_t          * voxelStatus )
{
    const int64_t idx = blockIdx.x * blockDim.x + threadIdx.x;

    if( idx >= NV )
    {
        return;
    }

    if( voxelStatus[ idx ] == VX_RESOLVED )
    {
        return;
    }

    if( minNeighbor[ idx ] >= 0 )
    {
     
        // if( minNeighbor[ idx ] < 0 || minNeighbor[ idx ] >= 27 )
        // {
        //     printf( "E:%d", minNeighbor[ idx ]  );
        // }     

        voxelStatus[ idx ] = VX_ACTIVE;

        const long3 idx3d =
            index3D(
                idx,
                fieldDims[ 0 ],
                fieldDims[ 1 ],
                fieldDims[ 2 ] );

        const long3 nIdx = idx3d + ntable[ minNeighbor[ idx ] ];

        // if( nIdx.x >= fieldDims[ 0 ] || nIdx.x < 0 || 
        //     nIdx.y >= fieldDims[ 1 ] || nIdx.y < 0 || 
        //     nIdx.z >= fieldDims[ 2 ] || nIdx.z < 0 )
        // {
        //     printf( "E: nid " );
        // }

        const int64_t flatNIdx = flatIndex3D(
                               nIdx.x,
                               nIdx.y,
                               nIdx.z,
                               fieldDims[ 0 ],
                               fieldDims[ 1 ],
                               fieldDims[ 2 ] );

        // if( flatNIdx < 0 || flatNIdx >= NV )
        // {
        //     printf( "E: fNidx " );
        // }

        voxelCellMap[ idx ] = voxelCellMap[ flatNIdx ];
    }
    if( voxelStatus[ idx ] == VX_ACTIVE && ( currentDistance[ idx ] ) <= ITER * ITER + FLT_EPSILON )
    {         
        voxelStatus[ idx ] = VX_RESOLVED;
        minNeighbor[ idx ] = -1;       
    }
}

__global__
void updateRegion2(
    const int64_t     NV,    
    const int64_t       NUR,
    const int64_t   *   URI,
    const int ITER,
    const int32_t   * fieldDims,
    int8_t         * minNeighbor,    
    const float * currentDistance,
    int32_t         * voxelCellMap,
    int8_t          * voxelStatus )
{
    const int64_t nridx = blockIdx.x * blockDim.x + threadIdx.x;
    if( nridx >= NUR )
    {
        return;
    }

    int64_t idx = URI[ nridx ];

    if( minNeighbor[ idx ] >= 0 )
    {
        voxelStatus[ idx ] = VX_ACTIVE;

        const long3 idx3d =
            index3D(
                idx,
                fieldDims[ 0 ],
                fieldDims[ 1 ],
                fieldDims[ 2 ] );

        // if( minNeighbor[ idx ] < 0 || minNeighbor[ idx ] >= 27 )
        // {
        //     printf( "E: ntable " );
        // }

        const long3 nIdx = idx3d + ntable[ minNeighbor[ idx ] ];

        // if( nIdx.x >= fieldDims[ 0 ] || nIdx.x < 0 || 
        //     nIdx.y >= fieldDims[ 1 ] || nIdx.y < 0 || 
        //     nIdx.z >= fieldDims[ 2 ] || nIdx.z < 0 )
        // {
        //     printf( "E: nid " );
        // }

        const int64_t flatNIdx = flatIndex3D(
                               nIdx.x,
                               nIdx.y,
                               nIdx.z,
                               fieldDims[ 0 ],
                               fieldDims[ 1 ],
                               fieldDims[ 2 ] );

        // if( flatNIdx < 0 || flatNIdx >= NV )
        // {
        //     printf( "E: fNidx " );
        // }

        voxelCellMap[ idx ] = voxelCellMap[ flatNIdx ];
    }

    if( voxelStatus[ idx ] == VX_ACTIVE && ( currentDistance[ idx ] ) <= ITER * ITER + FLT_EPSILON )
    {         
        voxelStatus[ idx ] = VX_RESOLVED;
        minNeighbor[ idx ] = -1;       
    }
}


__global__
void activateFromSource(
    const int64_t     NV,
    const int32_t   * fieldDims,
    float       * currentDistance,
    int32_t         * voxelCellMap,
    int8_t         * voxelStatus,
    int8_t          * minNeighbor )
{
    const int64_t idx = blockIdx.x * blockDim.x + threadIdx.x;

    if( idx >= NV )
    {
        return;
    }

    if( voxelStatus[ idx ] == VX_RESOLVED )
    {
        return;
    }

    if( minNeighbor[ idx ] >= 0 )
    {
        voxelStatus[ idx ] = VX_ACTIVE;
    }
}

__global__
void updateRegionFromSource(
    const int64_t     NE,
    const int64_t   * ids,
    const int32_t   * fieldDims,
    const int32_t   * cellMap,
    float       * currentDistance,
    int64_t         * sourceMap,
    int8_t         * voxelStatus,
    int8_t          * minNeighbor )
{
    const int64_t edx = blockIdx.x * blockDim.x + threadIdx.x;

    if( edx >= NE )
    {
        return;
    }

    const int64_t idx = ids[ edx ];

    if( voxelStatus[ idx ] == VX_RESOLVED )
    {
        return;
    }

    if( minNeighbor[ idx ] >= 0 )
    {
        voxelStatus[ idx ] = VX_ACTIVE;

        long3 idx3d =
            index3D(
                idx,
                fieldDims[ 0 ],
                fieldDims[ 1 ],
                fieldDims[ 2 ] );

        long3 nIdx = idx3d + ntable[ minNeighbor[ idx ] ];
        int64_t flatNIdx = flatIndex3D(
                               nIdx.x,
                               nIdx.y,
                               nIdx.z,
                               fieldDims[ 0 ],
                               fieldDims[ 1 ],
                               fieldDims[ 2 ] );

        if( voxelStatus[ flatNIdx ] & VX_IS_SRC )
        {
            // direct map the neighbor as the source if it is a source
            sourceMap[ edx ] = flatNIdx;
        }
        else
        {
            // get the neighbors source
            int64_t nEdx = cellMap[ flatNIdx ];
            sourceMap[ edx ] = sourceMap[ nEdx ];
        }
    }
}

__global__
void finalizeRegionFromSource(
    const int64_t     NE,
    const int64_t     NS,
    const int64_t   * ids,
    int64_t         * sourceMap,
    int8_t         * voxelStatus,
    int32_t         * cellMap )
{
    const int64_t edx = blockIdx.x * blockDim.x + threadIdx.x;

    if( edx >= NE )
    {
        return;
    }

    const int64_t idx = ids[ edx ];

    if( voxelStatus[ idx ] != VX_UNCLASSIFIED )
    {
        const int64_t sourceIdx = sourceMap[ edx ];
        const int32_t sourceSiteId = cellMap[ sourceIdx ];

        if( sourceSiteId < 0 || sourceSiteId >= NS )
        {
            printf( "ERRS%d ", sourceSiteId );
        }

        cellMap[ idx ] = sourceSiteId;

        voxelStatus[ idx ] = VX_RESOLVED | VX_ACTIVE;
    }
}

__global__
void growRegionsLineOfSite(
    const int64_t       NUR,
    const int64_t       NSITES,    
    const int64_t   *   URI,
    const bool hasBackground,
    const int32_t   * fieldDims,
    const int32_t   * voxelComponentIds,
    const double    * sitePositions,
    float           * currentDistance,
    int32_t         * voxelCellMap,
    int8_t          * voxelStatus,
    int8_t          * minNeighbor )
{
    const int64_t nridx = blockIdx.x * blockDim.x + threadIdx.x;
    if( nridx >= NUR )
    {
        return;
    }

    int64_t idx = URI[ nridx ];

    const int32_t cmp_Idx = voxelComponentIds[ idx ];

    const long3 idx3d =
        index3D(
            idx,
            fieldDims[ 0 ],
            fieldDims[ 1 ],
            fieldDims[ 2 ] );

    const double3 voxelPos =
        location3D(
            idx3d.x,
            idx3d.y,
            idx3d.z,
            fieldDims[ 0 ],
            fieldDims[ 1 ],
            fieldDims[ 2 ] );

    minNeighbor[ idx ] = -1;
    
    for( int i = 1; i < 27; ++i )
    {
        const long3 nIdx = idx3d + ntable[ i ];

        if( ( nIdx.x < 0 || nIdx.x >= fieldDims[ 0 ] )
                || ( nIdx.y < 0 || nIdx.y >= fieldDims[ 1 ] )
                || ( nIdx.z < 0 || nIdx.z >= fieldDims[ 2 ] ) )
        {
            continue;
        }

        const int64_t flatNIdx = flatIndex3D(
                                     nIdx.x,
                                     nIdx.y,
                                     nIdx.z,
                                     fieldDims[ 0 ],
                                     fieldDims[ 1 ],
                                     fieldDims[ 2 ] );

        // must be in the same component
        if( voxelComponentIds[ flatNIdx ] != cmp_Idx )
        {
            continue;
        }

        const int32_t siteId = voxelCellMap[ flatNIdx ];

        // if the neighbor doesn't have a current closest site,
        // or if the neighbor has the same site we have (if we have one)
        // then no need to check this neighbor

        if( ( voxelStatus[ flatNIdx ] == VX_UNCLASSIFIED ) ||
          ( ( voxelStatus[ idx ] != VX_UNCLASSIFIED ) && siteId == voxelCellMap[ idx ] ) )
        {
            continue;
        }

        const double lx = ( sitePositions[ siteId*3     ] - voxelPos.x ) * fieldDims[ 0 ];
        const double ly = ( sitePositions[ siteId*3 + 1 ] - voxelPos.y ) * fieldDims[ 1 ];
        const double lz = ( sitePositions[ siteId*3 + 2 ] - voxelPos.z ) * fieldDims[ 2 ];

        const float dist = static_cast< float >( lx*lx + ly*ly + lz*lz );
        if( ( dist + FLT_EPSILON ) < currentDistance[ idx ] )
        {
            bool lineOfSite = true;

            const double DC = max( max( abs(lx), abs(ly) ), abs(lz) );
            const int N_CHECK = round( DC );

            const float dx = lx / N_CHECK;
            const float dy = ly / N_CHECK;
            const float dz = lz / N_CHECK;

            for( int j = 1; j < N_CHECK; ++j )
            {
                const int64_t ix = idx3d.x + static_cast< int64_t >( round( j * dx ) );
                const int64_t iy = idx3d.y + static_cast< int64_t >( round( j * dy ) );
                const int64_t iz = idx3d.z + static_cast< int64_t >( round( j * dz ) );

                const int64_t jFIDX = flatIndex3D(
                    ix,
                    iy,
                    iz,
                    static_cast< int64_t >( fieldDims[ 0 ] ),
                    static_cast< int64_t >( fieldDims[ 1 ] ),
                    static_cast< int64_t >( fieldDims[ 2 ] ) );

                const int32_t jIdx = voxelComponentIds[ jFIDX ];

                if( cmp_Idx != jIdx )
                {
                    lineOfSite = false;
                    break;
                }

                if( voxelStatus[ jFIDX ] == VX_RESOLVED && voxelCellMap[ jFIDX ] != siteId )
                {
                    lineOfSite = false;
                    break;
                }
            }
            if( true ) //lineOfSite )
            {
                currentDistance[ idx ] = dist;
                minNeighbor[ idx ] = i;
            }
        }
    }
}

__global__
void growRegionsLineOfSite(
    const int64_t     NV,
    const int64_t     NSITES,    
    const bool hasBackground,
    const int32_t   * fieldDims,
    const int32_t   * voxelComponentIds,
    const double    * sitePositions,
    float           * currentDistance,
    int32_t         * voxelCellMap,
    int8_t          * voxelStatus,
    int8_t          * minNeighbor )
{
    const int64_t idx = blockIdx.x * blockDim.x + threadIdx.x;
    if( idx >= NV )
    {
        return;
    }

    if( voxelStatus[ idx ] == VX_RESOLVED )
    {
        return;
    }

    const int32_t cmp_Idx = voxelComponentIds[ idx ];

    // empty layer
    if( hasBackground && cmp_Idx == 0 )
    {
        currentDistance[ idx ] = -1.f;
        voxelStatus[ idx ]     = VX_RESOLVED;
        return;
    }

    const long3 idx3d =
        index3D(
            idx,
            fieldDims[ 0 ],
            fieldDims[ 1 ],
            fieldDims[ 2 ] );

    const double3 voxelPos =
        location3D(
            idx3d.x,
            idx3d.y,
            idx3d.z,
            fieldDims[ 0 ],
            fieldDims[ 1 ],
            fieldDims[ 2 ] );

    minNeighbor[ idx ] = -1;
    
    //////////////////////////////////////////// error below ?

    for( int i = 1; i < 27; ++i )
    {
        const long3 nIdx = idx3d + ntable[ i ];

        if( ( nIdx.x < 0 || nIdx.x >= fieldDims[ 0 ] )
                || ( nIdx.y < 0 || nIdx.y >= fieldDims[ 1 ] )
                || ( nIdx.z < 0 || nIdx.z >= fieldDims[ 2 ] ) )
        {
            continue;
        }

        const int64_t flatNIdx = flatIndex3D(
                                     nIdx.x,
                                     nIdx.y,
                                     nIdx.z,
                                     fieldDims[ 0 ],
                                     fieldDims[ 1 ],
                                     fieldDims[ 2 ] );

        // if( flatNIdx < 0 || flatNIdx >= NV )
        // {
        //     printf("F1");
        // }

        // must be in the same component

        if( voxelComponentIds[ flatNIdx ] != cmp_Idx )
        {
            continue;
        }

        const int32_t siteId = voxelCellMap[ flatNIdx ];

        // if the neighbor doesn't have a current closest site,
        // or if the neighbor has the same site we have (if we have one)
        // then no need to check this neighbor

        if( ( voxelStatus[ flatNIdx ] == VX_UNCLASSIFIED ) ||
          ( ( voxelStatus[ idx ]      != VX_UNCLASSIFIED ) && siteId == voxelCellMap[ idx ] ) )
        {
            continue;
        }

        // if( siteId < 0  || siteId >= NSITES )
        // {
        //     printf("F2");
        // }

        const double lx = ( sitePositions[ siteId*3     ] - voxelPos.x ) * fieldDims[ 0 ];
        const double ly = ( sitePositions[ siteId*3 + 1 ] - voxelPos.y ) * fieldDims[ 1 ];
        const double lz = ( sitePositions[ siteId*3 + 2 ] - voxelPos.z ) * fieldDims[ 2 ];

        const float dist = static_cast< float >( lx*lx + ly*ly + lz*lz );
        if( ( dist + FLT_EPSILON ) < currentDistance[ idx ] )
        {
            bool lineOfSite = true;

            const double DC = max( max( abs(lx), abs(ly) ), abs(lz) );
            const int N_CHECK = round( DC );

            const float dx = lx / N_CHECK;
            const float dy = ly / N_CHECK;
            const float dz = lz / N_CHECK;

            for( int j = 1; j < N_CHECK; ++j )
            {
                const int64_t ix = idx3d.x + static_cast< int64_t >( round( j * dx ) );
                const int64_t iy = idx3d.y + static_cast< int64_t >( round( j * dy ) );
                const int64_t iz = idx3d.z + static_cast< int64_t >( round( j * dz ) );

                const int64_t jFIDX = flatIndex3D(
                    ix,
                    iy,
                    iz,
                    static_cast< int64_t >( fieldDims[ 0 ] ),
                    static_cast< int64_t >( fieldDims[ 1 ] ),
                    static_cast< int64_t >( fieldDims[ 2 ] ) );

                // if( jFIDX < 0 || jFIDX >= NV )
                // {
                //     printf("lz=%f\n", lz );     
                //     printf("dz=%f\n", dz );   
                //     printf("DC=%f\n", DC );   
                //     printf("NC=%d\n", N_CHECK );    
                    

                //     printf("ix=%d\n", ix );     
                //     printf("iy=%d\n", iy );   
                //     printf("iz=%d\n", iz );   

                //     printf("sx=%f\n", sitePositions[ siteId*3     ] );     
                //     printf("sy=%f\n", sitePositions[ siteId*3 + 1 ] );   
                //     printf("sz=%f\n", sitePositions[ siteId*3 + 2 ] );   
                // }

                const int32_t jIdx = voxelComponentIds[ jFIDX ];

                if( cmp_Idx != jIdx )
                {
                    lineOfSite = false;
                    break;
                }

                if( voxelStatus[ jFIDX ] == VX_RESOLVED && voxelCellMap[ jFIDX ] != siteId )
                {
                    lineOfSite = false;
                    break;
                }
            }
            if( true ) //lineOfSite )
            {
                currentDistance[ idx ] = dist;
                minNeighbor[ idx ] = i;
            }
        }
    }
}

__global__
void growRegionsLineOfSource(
    const int64_t     NE,
    const int64_t     NV,
    const int64_t   * ids,
    const int64_t   * sourceMap,
    const int32_t   * fieldDims,
    const int32_t   * voxelComponentIds,
    const double    * sitePositions,
    float           * currentDistance,
    const int32_t   * voxelCellMap,
    const int8_t    * voxelStatus,
    int8_t          * minNeighbor )
{
    const int64_t edx = blockIdx.x * blockDim.x + threadIdx.x;

    if( edx >= NE )
    {
        return;
    }

    const int64_t idx = ids[ edx ];

    if( voxelStatus[ idx ] == VX_RESOLVED )
    {
        return;
    }

    const int32_t cmp_Idx = voxelComponentIds[ idx ];

    const long3 idx3d =
        index3D(
            idx,
            fieldDims[ 0 ],
            fieldDims[ 1 ],
            fieldDims[ 2 ] );

    const double3 voxelPos =
        location3D(
            idx3d.x,
            idx3d.y,
            idx3d.z,
            fieldDims[ 0 ],
            fieldDims[ 1 ],
            fieldDims[ 2 ] );

    minNeighbor[ idx ] = -1;
    for( int i = 1; i < 27; ++i )
    {
        const long3 nIdx = idx3d + ntable[ i ];

        if( ( nIdx.x < 0 || nIdx.x >= fieldDims[ 0 ] )
                || ( nIdx.y < 0 || nIdx.y >= fieldDims[ 1 ] )
                || ( nIdx.z < 0 || nIdx.z >= fieldDims[ 2 ] ) )
        {
            continue;
        }

        const int64_t flatNIdx = flatIndex3D(
                                     nIdx.x,
                                     nIdx.y,
                                     nIdx.z,
                                     fieldDims[ 0 ],
                                     fieldDims[ 1 ],
                                     fieldDims[ 2 ] );


        if( flatNIdx < 0 || flatNIdx >= NV )
        {
            printf( "EX");
        }

        // must be in the same component
        if( voxelComponentIds[ flatNIdx ] != cmp_Idx )
        {
            continue;
        }

        // if the neighbor is a source then the distance should be distance to it and distance from it to its site
        // since it's adjacent, it's already known to be line of site

        if( voxelStatus[ flatNIdx ] & VX_IS_SRC )
        {
            const float dist = currentDistance[ flatNIdx ] + voxelDist( idx3d, nIdx );
            if( ( dist + FLT_EPSILON ) < currentDistance[ idx ] )
            {
                currentDistance[ idx ] = dist;
                minNeighbor[ idx ] = i;
            }
        }
        // otherwise, if it's a voxel mapped to a source
        // need to get distance to its source + the sources distance
        else if( voxelStatus[ flatNIdx ] & VX_RESOLVED || voxelStatus[ flatNIdx ] & VX_ACTIVE )
        {

            // HERE

            // get the index into the sourceMap array where the neighbors source voxel index is stored 
            // this is stored in voxelCellMap
            const int64_t neighborEDX = voxelCellMap[  flatNIdx ];


            
            // the voxel associated with the neighbors source
            const int64_t sourceVoxelId = sourceMap[ neighborEDX ];

            const long3 sourceIdx3 =
                index3D(
                    sourceVoxelId,
                    fieldDims[ 0 ],
                    fieldDims[ 1 ],
                    fieldDims[ 2 ] );

            const double3 sourcePosition = location3D(
                sourceIdx3.x,
                sourceIdx3.y,
                sourceIdx3.z,
                fieldDims[ 0 ],
                fieldDims[ 1 ],
                fieldDims[ 2 ] );

            const double lx = ( sourcePosition.x - voxelPos.x ) * fieldDims[ 0 ];
            const double ly = ( sourcePosition.y - voxelPos.y ) * fieldDims[ 1 ];
            const double lz = ( sourcePosition.z - voxelPos.z ) * fieldDims[ 2 ];

            // const float distToSource = sqrt( lx*lx + ly*ly + lz*lz );
            const double distToSource = lx*lx + ly*ly + lz*lz;
            const float fullDist = distToSource + currentDistance[ sourceVoxelId ];

            if( ( fullDist + FLT_EPSILON ) < currentDistance[ idx ] )
            {
                bool lineOfSite = true;

                const double DC = max( max( abs(lx), abs(ly) ), abs(lz) );
                const int N_CHECK = round( DC );

                const float dx = lx / N_CHECK;
                const float dy = ly / N_CHECK;
                const float dz = lz / N_CHECK;

                for( int j = 1; j < N_CHECK; ++j )
                {
                    const int64_t ix = idx3d.x + static_cast< int64_t >( round( j * dx ) );
                    const int64_t iy = idx3d.y + static_cast< int64_t >( round( j * dy ) );
                    const int64_t iz = idx3d.z + static_cast< int64_t >( round( j * dz ) );

                    const int64_t jFIDX = flatIndex3D(
                        ix,
                        iy,
                        iz,
                        static_cast< int64_t >( fieldDims[ 0 ] ),
                        static_cast< int64_t >( fieldDims[ 1 ] ),
                        static_cast< int64_t >( fieldDims[ 2 ] ) );

                    const int32_t jIdx = voxelComponentIds[ jFIDX ];

                    if( cmp_Idx != jIdx )
                    {
                        lineOfSite = false;
                        break;
                    }
                }
                if( lineOfSite )
                {
                    currentDistance[ idx ] = fullDist;
                    minNeighbor[ idx ] = i;
                }
            }
        }
    }
}


// __global__
// void mapLayers(
//     const int64_t NV,
//     int16_t * layerMap,
//     const float * lfield,
//     const float * layers,
//     const int NL )
// {
//     const int64_t idx = blockIdx.x * blockDim.x + threadIdx.x;
//     if( idx >= NV )
//     {
//         return;
//     }

//     float lf = lfield[ idx ];
//     layerMap[ idx ] = 0;

//     for( int l = 0; l < NL; ++l )
//     {
//         float la = layers[ 2*l     ];
//         float lb = layers[ 2*l + 1 ];

//         float width   = lb - la;

//         if( lf >= la && lf < lb  )
//         {
//             layerMap[ idx ] = l + 1;
//             break;
//         }
//     }
// }

__global__
void applyMassFunction(
    const int64_t NV,
    float * mfield,
    float attractionBias )
{
    const int64_t idx = blockIdx.x * blockDim.x + threadIdx.x;
    if( idx >= NV )
    {
        return;
    }

    mfield[ idx ] = massFunction( mfield[ idx ], attractionBias );
}

/*****************************************************************/
/*****************************************************************/
/*****************************************************************/
/*****************************************************************/

              // NEW EXPERIMENTAL FINALIZATION STAGE


__global__
void initCellMapFinal(
    const int64_t NV,
    int32_t * voxelCellMap,
    int8_t * voxelStatus,
    int8_t * minNeighbor,    
    float * currentDistance )
{
    const int64_t idx = blockIdx.x * blockDim.x + threadIdx.x;
    if( idx >= NV )
    {
        return;
    }

    voxelStatus[ idx ] = VX_ACTIVE;
    minNeighbor[ idx ] = -1;
}

__global__
void growRegionsLineOfSiteFinal(
    const int64_t     NV,
    const int64_t     NSITES,    
    const bool hasBackground,
    const int32_t   * fieldDims,
    const int32_t   * voxelComponentIds,
    const double    * sitePositions,
    float           * currentDistance,
    int32_t         * voxelCellMap,
    int8_t          * voxelStatus,
    int8_t          * minNeighbor )
{
   const int64_t idx = blockIdx.x * blockDim.x + threadIdx.x;

    if( idx >= NV )
    {
        return;
    }

    if( voxelStatus[ idx ] == VX_RESOLVED )
    {
        printf("R");
        return;
    }

    const int32_t cmp_Idx = voxelComponentIds[ idx ];

    const long3 idx3d =
        index3D(
            idx,
            fieldDims[ 0 ],
            fieldDims[ 1 ],
            fieldDims[ 2 ] );

    const double3 voxelPos =
        location3D(
            idx3d.x,
            idx3d.y,
            idx3d.z,
            fieldDims[ 0 ],
            fieldDims[ 1 ],
            fieldDims[ 2 ] );

    minNeighbor[ idx ] = -1;
    for( int i = 1; i < 27; ++i )
    {
        const long3 nIdx = idx3d + ntable[ i ];

        if( ( nIdx.x < 0 || nIdx.x >= fieldDims[ 0 ] )
                || ( nIdx.y < 0 || nIdx.y >= fieldDims[ 1 ] )
                || ( nIdx.z < 0 || nIdx.z >= fieldDims[ 2 ] ) )
        {
            continue;
        }

        const int64_t flatNIdx = flatIndex3D(
                                     nIdx.x,
                                     nIdx.y,
                                     nIdx.z,
                                     fieldDims[ 0 ],
                                     fieldDims[ 1 ],
                                     fieldDims[ 2 ] );


        if( flatNIdx < 0 || flatNIdx >= NV )
        {
            printf( "EX");
        }

        // must be in the same component
        if( voxelComponentIds[ flatNIdx ] != cmp_Idx )
        {
            continue;
        }

        // if the neighbor is a source then the distance should be distance to it and distance from it to its site
        // since it's adjacent, it's already known to be line of site

        // if( voxelStatus[ flatNIdx ] & VX_IS_SRC )
        // {
            const float dist = currentDistance[ flatNIdx ] + voxelDist( idx3d, nIdx );
            if( ( dist + FLT_EPSILON ) < currentDistance[ idx ] )
            {
                currentDistance[ idx ] = dist;
                minNeighbor[ idx ] = i;
            }
        // }
    }
}

__global__
void updateRegion2Final(
    const int64_t     NV,
    const int ITER,
    const int32_t   * fieldDims,
    int8_t         * minNeighbor,    
    const float * currentDistance,
    int32_t         * voxelCellMap,
    int8_t          * voxelStatus )
{
    const int64_t idx = blockIdx.x * blockDim.x + threadIdx.x;

    if( idx >= NV )
    {
        return;
    }

    if( voxelStatus[ idx ] == VX_RESOLVED )
    {
        return;
    }

    if( minNeighbor[ idx ] >= 0 )
    {
        // printf( "x" );
     
        // if( minNeighbor[ idx ] < 0 || minNeighbor[ idx ] >= 27 )
        // {
        //     printf( "E:%d", minNeighbor[ idx ]  );
        // }     

        voxelStatus[ idx ] = VX_ACTIVE;

        const long3 idx3d =
            index3D(
                idx,
                fieldDims[ 0 ],
                fieldDims[ 1 ],
                fieldDims[ 2 ] );

        const long3 nIdx = idx3d + ntable[ minNeighbor[ idx ] ];

        // if( nIdx.x >= fieldDims[ 0 ] || nIdx.x < 0 || 
        //     nIdx.y >= fieldDims[ 1 ] || nIdx.y < 0 || 
        //     nIdx.z >= fieldDims[ 2 ] || nIdx.z < 0 )
        // {
        //     printf( "E: nid " );
        // }

        const int64_t flatNIdx = flatIndex3D(
                               nIdx.x,
                               nIdx.y,
                               nIdx.z,
                               fieldDims[ 0 ],
                               fieldDims[ 1 ],
                               fieldDims[ 2 ] );

        // if( flatNIdx < 0 || flatNIdx >= NV )
        // {
        //     printf( "E: fNidx " );
        // }

        // if( voxelCellMap[ idx ] != voxelCellMap[ flatNIdx ] )
        // {
        //     printf("u");
        // }

        voxelCellMap[ idx ] = voxelCellMap[ flatNIdx ];

    }
}

void CentroidalSampler::freeBuffers()
{
    cudaFree( layers_d             );
    cudaFree( voxelLayerIds_d      );
    cudaFree( sitePositions_d      );
    cudaFree( siteDeltas_d         );    
    cudaFree( siteComponents_d     );
    cudaFree( voxelSiteStatus_d    );
    cudaFree( minNeighbors_d       );
    cudaFree( siteLayers_d         );
    cudaFree( data_d             );
    // cudaFree( mfield_d             );
    cudaFree( fieldDims_d          );
    cudaFree( voxelSiteIds_d       );
    cudaFree( netMass_d            );
    cudaFree( centerOfMass_d       );
}

void CentroidalSampler::setLayers(
    const std::vector< TN::Vec2< float > > & l )
{
    m_layers = l;
    size_t NL = l.size();

    std::cout << "layers are: " << std::endl;
    for( size_t i = 0; i < NL; ++i )
    {
        std::cout << l[ i ].a() << ", " << l[ i ].b() << std::endl;
    }

    cudaMalloc( (void **) &  layers_d, NL * 2 * sizeof( float ) );
    cudaMemcpy( layers_d, (float*) m_layers.data(), NL * 2 * sizeof( float ), cudaMemcpyHostToDevice );

    cudaMalloc(
        (void **) &voxelLayerIds_d,
        m_nVoxels * sizeof( int16_t ) );

    cudaMemset( voxelLayerIds_d, 0,
                m_nVoxels * sizeof( int16_t ) );

    gpuErrchk( cudaPeekAtLastError() );
    gpuErrchk( cudaDeviceSynchronize() );
}

void CentroidalSampler::setFields(
    float * layerField,
    float * massField,
    const std::vector< int > & dimensions )
{
    m_dimensions = dimensions;

    std::cout << "set dims " << dimensions[ 0 ] << " " << dimensions[ 1 ] << " " << dimensions[ 2 ] << std::endl;

    int64_t NE = 1;
    for( auto dim : m_dimensions )
    {
        NE *= dim;
    }

    auto dataRange = TN::Sequential::getRange( layerField, NE );
    std::cout << "data range is " << dataRange.a() << " " << dataRange.b() << std::endl;

    //cudaMalloc( (void **) &mfield_d, NE * sizeof(   float ) );
    cudaMalloc( (void **) &data_d, NE * sizeof(   float ) );
    cudaMalloc( (void **) &voxelSiteStatus_d, NE * sizeof( int8_t ) );
    cudaMalloc( (void **) &minNeighbors_d, NE * sizeof( int8_t ) );
    cudaMalloc( (void **) &fieldDims_d, m_dimensions.size() * sizeof( int32_t ) );

    m_nVoxels = NE;

    std::cout << "m_nVoxels=" << NE << std::endl;

    gpuErrchk( cudaPeekAtLastError() );
    gpuErrchk( cudaDeviceSynchronize() );

    cudaMemcpy( data_d, layerField, NE * sizeof( float ), cudaMemcpyHostToDevice );
    //cudaMemcpy( mfield_d,  massField, NE * sizeof( float ), cudaMemcpyHostToDevice );
    cudaMemcpy( fieldDims_d, m_dimensions.data(), m_dimensions.size() * sizeof( int32_t ), cudaMemcpyHostToDevice );
    cudaMemset( voxelSiteStatus_d, 0, NE * sizeof( int8_t ) );
    cudaMemset( minNeighbors_d, 0, NE * sizeof( int8_t ) );

    gpuErrchk( cudaPeekAtLastError() );
    gpuErrchk( cudaDeviceSynchronize() );

    xCoords_d = 0;
    yCoords_d = 0;
    zCoords_d = 0;

    m_useCoords = false;
}

int64_t CentroidalSampler::mapLineOfSource( 
    int64_t * elementIDs, 
    int64_t * elementSources,
    int64_t NE )
{
    int64_t lastSum = 0;
    int nIter = 0;
    int N_START = 10;
    while( 1 )
    {
        growRegionsLineOfSource<<< gridSize( CM_THREADS_PER_BLOCK, m_nVoxels ), CM_THREADS_PER_BLOCK >>>(
            NE,
            m_nVoxels,
            elementIDs,
            elementSources,            
            fieldDims_d,
            voxelComponentIds_d,
            sitePositions_d,
            data_d,
            voxelSiteIds_d,
            voxelSiteStatus_d,
            minNeighbors_d );

        gpuErrchk( cudaPeekAtLastError() );
        gpuErrchk( cudaDeviceSynchronize() );

        updateRegionFromSource<<< gridSize( CM_THREADS_PER_BLOCK, m_nVoxels ), CM_THREADS_PER_BLOCK >>>(
            NE,
            elementIDs,
            fieldDims_d,
            voxelSiteIds_d,
            data_d,
            elementSources,
            voxelSiteStatus_d,
            minNeighbors_d );

        gpuErrchk( cudaPeekAtLastError() );
        gpuErrchk( cudaDeviceSynchronize() );

        if( nIter > N_START )
        {
            thrust::device_ptr<int8_t> cptr = thrust::device_pointer_cast( voxelSiteStatus_d );
            int64_t res = thrust::count( cptr, cptr + m_nVoxels, 0 );

            if( res == lastSum )
            {
                std::cout << "converged " << std::endl;
                lastSum = res;
                break;
            }
            lastSum = res;
        }
        ++nIter;
    }
    return lastSum;
}

int64_t CentroidalSampler::mapLineOfSite()
{
    int nIter = 0;
    int64_t nUnresolved = -1;

    while( 1 )
    {
        int64_t nUnresolved = thrust::count( 
            thrust::device_ptr<int8_t>( voxelSiteStatus_d ),
            thrust::device_ptr<int8_t>( voxelSiteStatus_d ) + m_nVoxels, 
            0 );

        std::cout << "n_unresolved=" << nUnresolved << std::endl;

        gpuErrchk( cudaPeekAtLastError() );
        gpuErrchk( cudaDeviceSynchronize() );

        if( nUnresolved == 0 )
        {
            break;
        }

        growRegionsLineOfSite<<< gridSize( VX_THREADS_PER_BLOCK, m_nVoxels ), VX_THREADS_PER_BLOCK >>>(
            m_nVoxels,
            m_nSites,
            m_hasBackground,
            fieldDims_d,
            voxelComponentIds_d,
            sitePositions_d,
            data_d,
            voxelSiteIds_d,
            voxelSiteStatus_d,
            minNeighbors_d );
        
        gpuErrchk( cudaPeekAtLastError() );
        gpuErrchk( cudaDeviceSynchronize() );

        /************************************************************************************/

        updateRegion2<<< gridSize( VX_THREADS_PER_BLOCK, m_nVoxels ), VX_THREADS_PER_BLOCK >>>(
            m_nVoxels,
            nIter,
            fieldDims_d,
            minNeighbors_d,
            data_d,
            voxelSiteIds_d,
            voxelSiteStatus_d );

        gpuErrchk( cudaPeekAtLastError() );
        gpuErrchk( cudaDeviceSynchronize() );

        ++nIter;
    }

    return nUnresolved;
}

bool CentroidalSampler::validateSiteIdContiguity()
{
    bool valid = true;
    std::set< int32_t > siteIds;
    for( size_t i = 0; i < m_nVoxels; ++i )
    {
        if( m_voxelSiteIds[ i ] < -1 ||  m_voxelSiteIds[ i ] > m_nSites )
        {
            std::cout << "Error: encountered out of range site id: " << m_voxelSiteIds[ i ] << std::endl;
            valid = false;
            break;
            siteIds.insert( m_voxelSiteIds[ i ] );
        }
    }
    return valid && siteIds.size() == m_nVoxels;
}

void CentroidalSampler::finalizeCellMapOptimal()
{
    initCellMapFinal<<< gridSize( VX_THREADS_PER_BLOCK, m_nVoxels ), VX_THREADS_PER_BLOCK >>>(
        m_nVoxels,    
        voxelSiteIds_d,
        voxelSiteStatus_d,
        minNeighbors_d,
        data_d );

    gpuErrchk( cudaPeekAtLastError() );
    gpuErrchk( cudaDeviceSynchronize() );

    int nIter = 0;
    int NITER = 100000;
    while( 1 )
    {
        growRegionsLineOfSiteFinal<<< gridSize( VX_THREADS_PER_BLOCK, m_nVoxels ), VX_THREADS_PER_BLOCK >>>(
            m_nVoxels,
            m_nSites,
            m_hasBackground,
            fieldDims_d,
            voxelComponentIds_d,
            sitePositions_d,
            data_d,
            voxelSiteIds_d,
            voxelSiteStatus_d,
            minNeighbors_d );
        
        std::cout << "grow: done " << std::endl;

        gpuErrchk( cudaPeekAtLastError() );
        gpuErrchk( cudaDeviceSynchronize() );

        std::cout <<  "Grew error check " << nIter << std::endl;

        updateRegion2Final<<< gridSize( VX_THREADS_PER_BLOCK, m_nVoxels ), VX_THREADS_PER_BLOCK >>>(
            m_nVoxels,
            nIter,
            fieldDims_d,
            minNeighbors_d,
            data_d,
            voxelSiteIds_d,
            voxelSiteStatus_d );

        std::cout << "updated region 2 ..." << std::endl;

        gpuErrchk( cudaPeekAtLastError() );
        gpuErrchk( cudaDeviceSynchronize() );

        ++nIter;

        if( nIter > NITER )
        {
            break;
        }
    }
}

void CentroidalSampler::mapCells2( bool fillStage )
{
    // note that need to correct initial distance to be not discrete,
    // e.g. the distance from the sites postion to it's covered voxel
    // is only surely 0 on the first evolution

    initCellMap<<< gridSize( VX_THREADS_PER_BLOCK, m_nVoxels ), VX_THREADS_PER_BLOCK >>>(
        m_nVoxels,
        voxelComponentIds_d,
        m_hasBackground,            
        voxelSiteIds_d,
        voxelSiteStatus_d,
        minNeighbors_d,
        data_d );

    gpuErrchk( cudaPeekAtLastError() );
    gpuErrchk( cudaDeviceSynchronize() );

    // std::cout << "init cell map " << m_nSites << std::endl;

    // std::cout << "m_nSites=" << m_nSites << std::endl;

    initializeRegions<<< gridSize( CM_THREADS_PER_BLOCK, m_nSites ), CM_THREADS_PER_BLOCK >>>(
        m_nSites,
        sitePositions_d,
        siteComponents_d,
        voxelComponentIds_d,
        fieldDims_d,
        data_d,
        voxelSiteIds_d,
        voxelSiteStatus_d );

    gpuErrchk( cudaPeekAtLastError() );
    gpuErrchk( cudaDeviceSynchronize() );

    auto time = high_resolution_clock::now();

    int64_t nUnresolved = mapLineOfSite();

    std::cout << "unresolved=" << nUnresolved << std::endl; 

    //////////////////////////////////////////////////////////////
    // verification

    // std::vector< int32_t > siteIdsHost( m_nVoxels );
    // cudaMemcpy(
    //     siteIdsHost.data(),
    //     voxelSiteIds_d,
    //     m_nVoxels * sizeof( int32_t ),
    //     cudaMemcpyDeviceToHost );

    // int64_t voxelCount = 0;
    // bool invalidMapping = false;

    // for( int64_t i = 0; i < m_nVoxels; ++i )
    // {
    //     int32_t siteId = siteIdsHost[ i ];

    //     if( siteId < 0 || siteId >= m_nSites  )
    //     {
    //         invalidMapping = true;
    //         std::cout << "out of bounds site id " << siteId << "\n";
    //     }
    // }

    // if( invalidMapping )
    // {
    //     exit( 1 );
    // }
    // else
    // {
    //     std::cout << "ALL VOXELS ARE MAPPED TO A SITE" << std::endl;
    // }
}

float CentroidalSampler::evolve3()
{
    std::cout << "\n--------------------------------------------\n";
    std::cout << "\nevolving\n";
    std::cout << "\n--------------------------------------------\n\n";

    mapCells2( false );

    gpuErrchk( cudaPeekAtLastError() );
    gpuErrchk( cudaDeviceSynchronize() );

    std::cout << "mapped cells " << std::endl;

    //////////////////////////////////////////////////////////////

    cudaMemset( centerOfMass_d, 0, m_nSites * 3 * sizeof( double ) );
    cudaMemset( netMass_d,      0, m_nSites * sizeof( double ) );
    cudaMemset( siteDeltas_d,   0, m_nSites * sizeof( double ) );

    gpuErrchk( cudaPeekAtLastError() );
    gpuErrchk( cudaDeviceSynchronize() );

    computeMass2<<< gridSize( VX_THREADS_PER_BLOCK, m_nVoxels ), VX_THREADS_PER_BLOCK >>>(
        m_nVoxels,
        mfield_d,
        fieldDims_d,
        m_dimensions.size(),
        voxelSiteIds_d,
        netMass_d,
        centerOfMass_d,
        1.f );

    gpuErrchk( cudaPeekAtLastError() );
    gpuErrchk( cudaDeviceSynchronize() );

    std::cout << "mass computed " << std::endl;

    ///////////////////////////////////////////////////////////////////////////////////////////////

    int8_t * siteFlags_d = 0;
    cudaMalloc( (void **) & siteFlags_d, m_nSites * sizeof( int8_t ) );

    // computes the centroid and marks sites with cointroids outside of their area

    normalizeCenterOfMass3<<< gridSize( CM_THREADS_PER_BLOCK, m_nSites ), CM_THREADS_PER_BLOCK >>>(
        netMass_d,
        centerOfMass_d,
        voxelSiteIds_d,
        fieldDims_d,
        sitePositions_d,
        siteDeltas_d,
        siteFlags_d,
        m_dimensions.size(),
        m_nSites );

    gpuErrchk( cudaPeekAtLastError() );
    gpuErrchk( cudaDeviceSynchronize() );

    std::cout << "centroid computed " << std::endl;

    // count how many regions there are that need to be modified

    int64_t NR = thrust::count_if( 
        thrust::device_pointer_cast( siteFlags_d ), 
        thrust::device_pointer_cast( siteFlags_d ) + m_nSites, 
        is_even() ); // because 0 means needs modify, and 1 means resolved

    gpuErrchk( cudaPeekAtLastError() );
    gpuErrchk( cudaDeviceSynchronize() );

    std::cout << "counted number of regions to modify " << NR << std::endl;

    if( NR == 0 )
    {
        std::cout << "no regions to modify" << std::endl;
    }
    else
    {
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // this block now seems to be logically correct, but assumes all voxels have been assigned a site, 
        // yet it seems that a few (2) of them in the test case have -1 assigned as their site id indicating a 
        // problem upstream

        // num voxels in regions where the centroid is outside of the sites area

        int64_t NVC = thrust::count_if( 
            thrust::device_pointer_cast( voxelSiteIds_d ), 
            thrust::device_pointer_cast( voxelSiteIds_d ) + m_nVoxels, 
            ThrustAccessorEqualsIndirect< int8_t, int8_t, int32_t >( 
                thrust::device_pointer_cast( siteFlags_d ),      
                0 ) );

        gpuErrchk( cudaPeekAtLastError() );
        gpuErrchk( cudaDeviceSynchronize() );

        int64_t * nvcIds_d = 0;
        cudaMalloc( (void **) & nvcIds_d, NVC * sizeof( int64_t ) );

        int32_t * nvcSiteIds_d = 0;
        cudaMalloc( (void **) & nvcSiteIds_d, NVC * sizeof( int32_t ) );

        std::cout << "counted regions to modify voxels " << NVC << std::endl;

        exit( 1 );

        // copy ids of all voxels which belong to sites which have centroids outside of their boundaries 

        // thrust::copy_if(
        //     thrust::make_counting_iterator<int64_t>( 0         ),
        //     thrust::make_counting_iterator<int64_t>( m_nVoxels ),
        //     thrust::device_pointer_cast( nvcIds_d ),
        //     ThrustAccessorEqualsIndirect< int32_t, int8_t, int64_t >( 
        //         thrust::device_pointer_cast( voxelSiteIds_d ), 
        //         thrust::device_pointer_cast( siteFlags_d ),             
        //         0 ) );

        gpuErrchk( cudaPeekAtLastError() );
        gpuErrchk( cudaDeviceSynchronize() );

        std::cout << "copied regions to modify voxel ids " << std::endl;

        ////////////////////////////////////////////////////////////

        thrust::transform( 
            thrust::device_pointer_cast( nvcIds_d ),
            thrust::device_pointer_cast( nvcIds_d ) + NVC,
            thrust::device_pointer_cast( nvcSiteIds_d ),
            ThrustTypeCastIndirection< int64_t, int32_t >( 
                thrust::device_pointer_cast( voxelSiteIds_d ) ) );

        gpuErrchk( cudaPeekAtLastError() );
        gpuErrchk( cudaDeviceSynchronize() );


        ////////////////////////////////////////////////////////////////////////////////////////////////////////////

        std::cout << "copied site ids of the regions to modify voxel ids " << std::endl;

        // organize them such that they make consecutive blocks where each 
        // block represents the voxels pertaining to a specific site

        thrust::sort_by_key(
            thrust::device_pointer_cast( nvcSiteIds_d ),
            thrust::device_pointer_cast( nvcSiteIds_d ) + NVC,
            thrust::device_pointer_cast( nvcIds_d ) );

        gpuErrchk( cudaPeekAtLastError() );
        gpuErrchk( cudaDeviceSynchronize() );

        // get the lengths of each subsequence ( as first step to computing offsets )

        int64_t * nvcOffsets = 0;
        cudaMalloc( (void **) & nvcOffsets, NR * sizeof( int64_t ) );

        gpuErrchk( cudaPeekAtLastError() );
        gpuErrchk( cudaDeviceSynchronize() );

        thrust::reduce_by_key(
            thrust::device_pointer_cast( nvcSiteIds_d ),
            thrust::device_pointer_cast( nvcSiteIds_d ) + NVC,
            thrust::make_constant_iterator( 1 ),
            thrust::make_discard_iterator(),
            thrust::device_pointer_cast( nvcOffsets ) );

        gpuErrchk( cudaPeekAtLastError() );
        gpuErrchk( cudaDeviceSynchronize() );

        std::cout << "\n\n" << std::endl;


        std::cout << "sorted regions to modify voxel ids " << std::endl;

        exit( 1 );

        std::cout << "computed lengths of subsequences " << std::endl;

        // convert the lengths to offsets (in place)

        thrust::exclusive_scan(
            thrust::device_pointer_cast( nvcOffsets ),
            thrust::device_pointer_cast( nvcOffsets ) + NR,
            thrust::device_pointer_cast( nvcOffsets ) );

        gpuErrchk( cudaPeekAtLastError() );
        gpuErrchk( cudaDeviceSynchronize() );

        std::cout << "computed offsets " << std::endl;

        // overall 

        // [ a, b, b, b, b, c, c, d, e, e, e ] ->
        // [ 1, 4, 2, 1, 3 ] ->
        // [ 0, 1, 5, 7, 8 ]

        // now invoke kernel to find the voxel within the boundaries of the cell which is nearest to the centroid

        computeNearestVoxelToCentroid<<< gridSize( CM_THREADS_PER_BLOCK, NR ), CM_THREADS_PER_BLOCK >>>(
            NR,
            nvcOffsets,
            fieldDims_d,
            nvcIds_d,
            voxelSiteIds_d,
            sitePositions_d,
            siteDeltas_d );

        std::cout << "relaxed the centroids " << std::endl;

        cudaFree( nvcIds_d    );
        cudaFree( nvcOffsets  );   
        cudaFree( siteFlags_d );
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////

   float maxDelta = *( thrust::max_element( 
       thrust::device_pointer_cast( siteDeltas_d ),
       thrust::device_pointer_cast( siteDeltas_d ) + m_nSites           
   ) );

   std::cout << "checked deltas " << maxDelta << std::endl;

    std::cout << "\n--------------------------------------------\n";

   return maxDelta;
}


float CentroidalSampler::evolve2()
{
    mapCells2( false );

    gpuErrchk( cudaPeekAtLastError() );
    gpuErrchk( cudaDeviceSynchronize() );

    std::cout << "mapped cells " << std::endl;

    //////////////////////////////////////////////////////////////

    cudaMemset( centerOfMass_d, 0, m_nSites * 3 * sizeof( double ) );
    cudaMemset( netMass_d,      0, m_nSites * sizeof( double ) );
    cudaMemset( siteDeltas_d,   0, m_nSites * sizeof( double ) );

    gpuErrchk( cudaPeekAtLastError() );
    gpuErrchk( cudaDeviceSynchronize() );

    computeMass2<<< gridSize( VX_THREADS_PER_BLOCK, m_nVoxels ), VX_THREADS_PER_BLOCK >>>(
        m_nVoxels,
        mfield_d,
        fieldDims_d,
        m_dimensions.size(),
        voxelSiteIds_d,
        netMass_d,
        centerOfMass_d,
        1.f );

    gpuErrchk( cudaPeekAtLastError() );
    gpuErrchk( cudaDeviceSynchronize() );

    std::cout << "mass computed " << std::endl;

    normalizeCenterOfMass2<<< gridSize( CM_THREADS_PER_BLOCK, m_nSites ), CM_THREADS_PER_BLOCK >>>(
        netMass_d,
        centerOfMass_d,
        siteComponents_d,
        voxelComponentIds_d,
        fieldDims_d,
        sitePositions_d,
        siteDeltas_d,
        m_dimensions.size(),
        m_nSites );

    gpuErrchk( cudaPeekAtLastError() );
    gpuErrchk( cudaDeviceSynchronize() );

    std::cout << "sites updated" << std::endl;

   float maxDelta = *( thrust::max_element( 
       thrust::device_pointer_cast( siteDeltas_d ),
       thrust::device_pointer_cast( siteDeltas_d ) + m_nSites           
   ) );

   std::cout << "checked deltas" << std::endl;

   return maxDelta;
}

void CentroidalSampler::finalize()
{
    // mapCells2( false );

    // std::cout << "mapped cells true" << std::endl;

    // finalizeCellMapOptimal(); 

    m_voxelSiteIds.resize( m_nVoxels );
    cudaMemcpy(
        m_voxelSiteIds.data(),
        voxelSiteIds_d,
        m_nVoxels * sizeof( int32_t ),
        cudaMemcpyDeviceToHost );

    m_sitePoints.resize( m_nSites * 3 );

    std::cout << "sites=" << m_nSites << ", ndims=" << m_dimensions.size() << std::endl;

    cudaMemcpy(
        m_sitePoints.data(),
        sitePositions_d,
        m_sitePoints.size() * sizeof( double ),
        cudaMemcpyDeviceToHost );

    // for( size_t i = 0; i < m_nSites; ++i ) 
    // {
    //     std::cout << "(" << m_sitePoints[ i * 2 ] << ", " << m_sitePoints[ i * 2 + 1 ] << ") ";
    // }

    ////////////////////////////////////////////////////////////

    m_voxelLayerIds.resize( m_nVoxels );
    cudaMemcpy(
        m_voxelLayerIds.data(),
        voxelLayerIds_d,
        m_nVoxels * sizeof( int16_t ),
        cudaMemcpyDeviceToHost );

    m_voxelComponentIds.resize( m_nVoxels );
    cudaMemcpy(
        m_voxelComponentIds.data(),
        voxelComponentIds_d,
        m_nVoxels * sizeof( int32_t ),
        cudaMemcpyDeviceToHost );

    //////////////////////////////////////////////////////////////

    m_siteComponentIds.resize( m_nSites );
    cudaMemcpy(
        m_siteComponentIds.data(),
        siteComponents_d,
        m_nSites * sizeof( int32_t ),
        cudaMemcpyDeviceToHost );

    m_siteLayers.resize( m_nSites );
    cudaMemcpy(
        m_siteLayers.data(),
        siteLayers_d,
        m_nSites * sizeof( int16_t ),
        cudaMemcpyDeviceToHost );


    bool valid = validateSiteIdContiguity();
}

void CentroidalSampler::evolveAndFinalize()
{
    // evolve();
    // finalize();
}

CentroidalSampler::CentroidalSampler()
{
    m_nVoxels = 0;

    layers_d            = nullptr;
    voxelLayerIds_d     = nullptr;
    sitePositions_d     = nullptr;
    siteDeltas_d        = nullptr;    
    siteComponents_d    = nullptr;
    voxelSiteStatus_d   = nullptr;
    minNeighbors_d      = nullptr;
    siteLayers_d        = nullptr;
    data_d            = nullptr;
    mfield_d            = nullptr;
    fieldDims_d         = nullptr;
    voxelSiteIds_d      = nullptr;
    netMass_d           = nullptr;
    centerOfMass_d      = nullptr;
}

CentroidalSampler::~CentroidalSampler()
{
    freeBuffers();
}


