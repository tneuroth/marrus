
#include "wlsrcvt_lib.hpp"
#include "constants.cuh"

WeightedCentroidalSampler::WeightedCentroidalSampler( 
    const std::array< int64_t, 3 > & _dimensions ) :
        CentroidalSampler( _dimensions ) {
    cudaMalloc( (void **) &mass_d, M_NV * sizeof( float ) );
}

WeightedCentroidalSampler::~WeightedCentroidalSampler() {
    cudaFree( mass_d );
}