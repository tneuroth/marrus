
#include "lsrcvt_lib.hpp"
#include "wlsrcvt_lib.hpp"
#include "utility.cuh"
#include "constants.cuh"

#include <cuda.h>

#include <thrust/reduce.h>
#include <thrust/execution_policy.h>
#include <thrust/device_ptr.h>
#include <thrust/iterator/discard_iterator.h>
#include <thrust/functional.h>
#include <thrust/transform_scan.h>
#include <thrust/device_vector.h>
#include <thrust/device_ptr.h>
#include <thrust/iterator/permutation_iterator.h>
#include <thrust/partition.h>

#include <cfloat>
#include <vector>
#include <iostream>
#include <chrono>
#include <cstdint>
#include <bitset>

inline __device__ int64_t getEdgeSRCRoot( 
    const int64_t   nodeId,
    const uint8_t * states,
    const int64_t * map ) 
{
    int64_t current = nodeId;
    // condition could be ( true ), but could be dangerous when used incorrectly
    while( ! isLineOfSite( states[ current ] ) ) {
        current = map[ current ];
    }
    return current;
}

__global__
void updateSitePositions(
    const int64_t   nPreviousUpdates, 
    const float * netMass,
    const float * centerOfMass,
    const int32_t * voxelSiteIds,
    const int32_t * voxelComponentIds,    
    const int32_t * siteComponentIds,    
    const int64_t * componentSiteCounts,
    float        * sites,
    float        * siteDeltas,
    const int N_CELLS,
    const bool simpleSingleSiteCMPHeuristic,
    bool hasBackground )
{
    const int64_t idx = blockIdx.x * blockDim.x + threadIdx.x;
    if( idx >= N_CELLS ) { return; }

    const float MASS_EPSILON = 0.0000000000001;

    // prevents divide by extremely small numbers or zero
    // which might be possible in small connected components
    // or when doing weighted centroids with inappropriate weighting scheme

    const float NET_MASS = netMass[ idx ];
    if( NET_MASS < MASS_EPSILON )
    {
        siteDeltas[ idx ] = 0;
        return;
    }

    // if( simpleSingleSiteCMPHeuristic && nPreviousUpdates > 0 ) {
    //     const int64_t cmpIDX = hasBackground ? siteComponentIds[ idx ] : siteComponentIds[ idx ] - 1;
    //     if( componentSiteCounts[ cmpIDX ] <= 1 ) {
    //         siteDeltas[ idx ] = 0;            
    //         return;
    //     }
    // }

    float newPos[ 3 ];
    for( int i = 0; i < 3; ++i ) {
        newPos[ i ] = centerOfMass[ idx * 3 + i ] / NET_MASS;
    }

    const int64_t xi = locationInverse1D( newPos[ 0 ], D_DIMS.x );
    const int64_t yi = locationInverse1D( newPos[ 1 ], D_DIMS.y );
    const int64_t zi = locationInverse1D( newPos[ 2 ], D_DIMS.z );

    const int64_t vIdx = flatIndex3D( { xi, yi, zi }, D_DIMS );

    // TODO 
     const float ux  = ( newPos[ 0 ] - sites[ idx * 3 + 0 ] );
     const float uy  = ( newPos[ 1 ] - sites[ idx * 3 + 1 ] );
     const float uz  = ( newPos[ 2 ] - sites[ idx * 3 + 2 ] );

     const float uxSc  = ux * D_DIMS.x;
     const float uySc  = uy * D_DIMS.y;
     const float uzSc  = uz * D_DIMS.z;

     const float length = sqrt( uxSc*uxSc + uySc*uySc + uzSc*uzSc );

    // get unit direction vector, such that each increment will be of length of a voxel width

    const float NORM = max( max( abs(uxSc), abs(uySc) ), abs(uzSc) );

    const float dx = ux / NORM;
    const float dy = uy / NORM;
    const float dz = uz / NORM;

    const int64_t N_CHECK = ceil( NORM );

    int64_t stoppingIndex = -1;

    for( int64_t i = 1; i < N_CHECK; ++i )
    {
        // need to make this more precise and mathematically verified
        const LI3 curr_idx3 = {
            locationInverse1D( sites[ idx * 3 + 0 ] + i * dx, D_DIMS.x ), 
            locationInverse1D( sites[ idx * 3 + 1 ] + i * dy, D_DIMS.y ), 
            locationInverse1D( sites[ idx * 3 + 2 ] + i * dz, D_DIMS.z ) };

        // shouldn't happen but want to be extra cautious
        if( ! inBounds( curr_idx3, D_DIMS ) ) {
            stoppingIndex = i-1;
            break; 
        }

        const int64_t curr_idx = flatIndex3D( curr_idx3, D_DIMS );
        const int32_t curr_cmp = voxelComponentIds[ curr_idx ];

        // crossed the boundary
        if( curr_cmp != siteComponentIds[ idx ] ) {
            stoppingIndex = i-1;
            break;
        }
    }

    // didn't cross any component boundary
    if( stoppingIndex == -1 ) {

        // compute how far it moved
        siteDeltas[ idx ] = length;

        // update site position to the center of mass
        const float x = newPos[ 0 ];
        const float y = newPos[ 1 ];
        const float z = newPos[ 2 ];

        const int64_t xi = locationInverse1D( x, D_DIMS.x );
        const int64_t yi = locationInverse1D( y, D_DIMS.y );
        const int64_t zi = locationInverse1D( z, D_DIMS.z );

        const int64_t vIdx = flatIndex3D( { xi, yi, zi }, D_DIMS );

        if( siteComponentIds[ idx ] != voxelComponentIds[ vIdx ] ) { 
            siteDeltas[ idx ] = 0;                
        }
        else {
            sites[ idx * 3 + 0 ] = x; 
            sites[ idx * 3 + 1 ] = y; 
            sites[ idx * 3 + 2 ] = z;           
        }
    }
    else {

        // give a small amount of padding
        int64_t finalStop = max( int( 0 ), int( stoppingIndex - 1 ) );

        const float x = sites[ idx * 3 + 0 ] + stoppingIndex * dx;
        const float y = sites[ idx * 3 + 1 ] + stoppingIndex * dy;
        const float z = sites[ idx * 3 + 2 ] + stoppingIndex * dz;

        const int64_t xi = locationInverse1D( x, D_DIMS.x );
        const int64_t yi = locationInverse1D( y, D_DIMS.y );
        const int64_t zi = locationInverse1D( z, D_DIMS.z );

        const int64_t vIdx = flatIndex3D(
            xi,
            yi,
            zi,
            D_DIMS.x,
            D_DIMS.y,
            D_DIMS.z );

        if( siteComponentIds[ idx ] != voxelComponentIds[ vIdx ] )
        { 
            siteDeltas[ idx ] = 0;            
        }
        else
        {
            sites[ idx * 3 + 0 ] = x;
            sites[ idx * 3 + 1 ] = y;
            sites[ idx * 3 + 2 ] = z;           

            const float dxf = dx * stoppingIndex * D_DIMS.x;
            const float dyf = dy * stoppingIndex * D_DIMS.y;
            const float dzf = dz * stoppingIndex * D_DIMS.z;
            siteDeltas[ idx ] = sqrt( dxf*dxf + dyf*dyf + dzf*dzf );
        }
    }
}

__global__
void sumMomentsGeodesic(
    const int64_t     nPreviousUpdates,
    const float     * massFunct,
    const int64_t   * srcMap,
    const int32_t   * voxelSiteIds,
    const int32_t   * siteComponents,
    const float     * sitePositions,
    const uint8_t   * voxelSiteStatus,
    const int64_t   * componentSiteCounts,
    float           * netMass,
    float           * centerOfMass,
    const float       attractionBias,
    const bool        simpleSingleSiteCMPHeuristic,
    const bool        hasBackground,
    const bool        overrideLineOfEdge )
{
    const int64_t idx = blockIdx.x * blockDim.x + threadIdx.x;
    if( idx >= D_NV ) { return; }

    // background voxels
    if( voxelSiteIds[ idx ] < 0 ) { return; }

    const int32_t site_idx = voxelSiteIds[ idx ];

    // in the case where we simplify the process for single site components
    // we only compute centroids and update for the first time
    // note to be cautious, because then the mass will be 0, so be sure to synchronize changes
    // with any future computation using the mass, including the update

    // if( simpleSingleSiteCMPHeuristic && nPreviousUpdates > 0 ) {
    //     const int64_t cmpIDX = hasBackground ? siteComponents[ site_idx ] : siteComponents[ site_idx ] - 1;
    //     if( componentSiteCounts[ cmpIDX ] == 1 ) {
    //         return;
    //     }
    // }

    // propagrate its weight to its root src nodes,
    int64_t sourceIdx = idx;
    if( ! isLineOfSite( voxelSiteStatus[ idx ] ) ) { 
        int64_t sx = getEdgeSRCRoot( idx, voxelSiteStatus, srcMap );
        sourceIdx = sx;
    }

    const LI3 sourceIdx3d = index3D( sourceIdx, D_DIMS );
    const float3 p = location3D( sourceIdx3d, D_DIMS );

    const float mass = massFunct[ sourceIdx ];
    const float mx = mass * p.x;
    const float my = mass * p.y;
    const float mz = mass * p.z;

    // fallback for old GPU without atomic support for double precision floating point 
    // #if !defined( __CUDA_ARCH__ ) || __CUDA_ARCH__ >= 600
        atomicAdd( & netMass[ site_idx ], mass );
        atomicAdd( & centerOfMass[ site_idx * 3     ], mx );
        atomicAdd( & centerOfMass[ site_idx * 3 + 1 ], my );
        atomicAdd( & centerOfMass[ site_idx * 3 + 2 ], mz );
    // #else
    //     atomicAddFallback( & netMass[ site_idx ], mass );
    //     atomicAddFallback( & centerOfMass[ site_idx * 3     ], mx );
    //     atomicAddFallback( & centerOfMass[ site_idx * 3 + 1 ], my );
    //     atomicAddFallback( & centerOfMass[ site_idx * 3 + 2 ], mz );
    // #endif
}

__global__
void sumMomentsGeodesic(
    const int64_t     nPreviousUpdates,    
    const int64_t   * srcMap,
    const float     * distances,
    const int32_t   * voxelSiteIds,
    const int32_t   * siteComponents,    
    const float     * sitePositions,
    const uint8_t   * voxelSiteStatus, 
    const int64_t   * componentSiteCounts,   
    float           * netMass,
    float           * centerOfMass,
    const bool        simpleSingleSiteCMPHeuristic,
    const bool        hasBackground,
    const bool        overrideLineOfEdge )
{
    const int64_t idx = blockIdx.x * blockDim.x + threadIdx.x;
    if( idx >= D_NV ) { return; }

    // background voxels
    if( voxelSiteIds[ idx ] < 0 ) { return; }

    const int32_t site_idx = voxelSiteIds[ idx ];

    // in the case where we simplify the process for single site components
    // we only compute centroids and update for the first time
    // note to be cautious, because then the mass will be 0, so be sure to synchronize changes
    // with any future computation using the mass, including the update

    // if( simpleSingleSiteCMPHeuristic && nPreviousUpdates > 0 ) {
    //     const int64_t cmpIDX = hasBackground ? siteComponents[ site_idx ] : siteComponents[ site_idx ] - 1;  
    //     if( componentSiteCounts[ cmpIDX ] == 1 ) {
    //         return;
    //     }
    // }

    float mass = 1.0;

    const float3 p_v = location3D( index3D( idx, D_DIMS ), D_DIMS );

    // site positions are normalized by dims!
    const float3 p_site = { 
        sitePositions[ site_idx * 3 + 0 ],
        sitePositions[ site_idx * 3 + 1 ],
        sitePositions[ site_idx * 3 + 2 ] };  

    const float beta = 1.75;
    float3 p_c;

    // propagrate its weight to its root src nodes,
    int64_t sourceIdx = idx;
    if( ! isLineOfSite( voxelSiteStatus[ idx ] ) ) { 

        // return;

        sourceIdx = getEdgeSRCRoot( idx, voxelSiteStatus, srcMap );
        mass = ( distances[ idx ] / distances[ sourceIdx ] );

        if( mass <= 0 ) {
            printf( "lo " );
        }
        else if ( mass <= 1.0 ) {
            printf( "l1 " );
        }
        else if ( isinf( mass ) ) {
            printf( "inf " );
        }
        else if ( isnan( mass ) ) {
            printf( "nan ");
        }
        else if ( mass > 672 ) {
            //printf( "m=%f ", mass );
            //printf( "ds=%f ", distances[ sourceIdx ] );   
            //printf( "dv=%f ", distances[ idx ] );                        
        }

        // location3D is normalized by dims!
        const float3 p_phi = location3D( index3D( sourceIdx,     D_DIMS ), D_DIMS );

        // because the site positions and everything are normalized, need to convert to voxel distances as appropriate

        const float3 p_src = location3D( index3D( srcMap[ idx ], D_DIMS ), D_DIMS );

        const float3 u_phi = normalized( p_phi - p_site );        
        const float3 u_src = normalized( p_src - p_v    );

        float3 p_int;

        // p_phi is the intersection point we are looking for
        if( sourceIdx == srcMap[ idx ] ) {
            p_int = p_phi;
        }
        else {
            p_int = closestPointToLine( p_site, p_src, p_v );
        }

        // // const float3 p_tar = p_int + beta * normalized( 0.5 * ( u_phi + u_src ) );
        // p_c = p_int + beta * normalized( 0.5 * ( u_phi + u_src ) );

        p_c = p_int;

        const float lx = ( p_c.x - p_site.x ) * ( D_DIMS.x - 1 );
        const float ly = ( p_c.y - p_site.y ) * ( D_DIMS.y - 1 );
        const float lz = ( p_c.z - p_site.z ) * ( D_DIMS.z - 1 );
        const float dc = sqrt( lx*lx + ly*ly + lz*lz );

        //mass = ( distances[ idx ] / dc );
    } 
    else {
        const LI3 sourceIdx3d = index3D( sourceIdx, D_DIMS );
        p_c = location3D( sourceIdx3d, D_DIMS );
    }

    const float mx = mass * p_c.x;
    const float my = mass * p_c.y;
    const float mz = mass * p_c.z;

    // fallback for old GPU without atomic support for double precision floating point 
    // #if !defined( __CUDA_ARCH__ ) || __CUDA_ARCH__ >= 600
        atomicAdd( & netMass[ site_idx ], mass );
        atomicAdd( & centerOfMass[ site_idx * 3     ], mx );
        atomicAdd( & centerOfMass[ site_idx * 3 + 1 ], my );
        atomicAdd( & centerOfMass[ site_idx * 3 + 2 ], mz );
    // #else
    //     atomicAddFallback( & netMass[ site_idx ], mass );
    //     atomicAddFallback( & centerOfMass[ site_idx * 3     ], mx );
    //     atomicAddFallback( & centerOfMass[ site_idx * 3 + 1 ], my );
    //     atomicAddFallback( & centerOfMass[ site_idx * 3 + 2 ], mz );
    // #endif
}

CentroidalUpdateStatistics CentroidalSampler::updateSites()
{
    updateSitePositions<<< gridSize( CM_THREADS_PER_BLOCK, m_nSites ), CM_THREADS_PER_BLOCK >>>(
        m_nPrevUpdates,
        netMass_d,
        centerOfMass_d,
        voxelSiteIds_d,
        voxelComponentIds_d,
        siteComponents_d,
        componentSiteCounts_d,
        sitePositions_d,
        siteDeltas_d,
        m_nSites,
        m_simpleSingleSiteCMPHeuristic,
        m_hasBackground );

    gpuErrchk( cudaPeekAtLastError() );
    gpuErrchk( cudaDeviceSynchronize() );

   float maxDelta = *( thrust::max_element( 
       thrust::device_pointer_cast( siteDeltas_d ),
       thrust::device_pointer_cast( siteDeltas_d ) + m_nSites           
   ) );

   float meanDelta = thrust::reduce( 
       thrust::device_pointer_cast( siteDeltas_d ),
       thrust::device_pointer_cast( siteDeltas_d ) + m_nSites           
   ) / m_nSites;

    ++m_nPrevUpdates;

   return { maxDelta, meanDelta };
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// unweighted version

CentroidalUpdateStatistics CentroidalSampler::geodesicCentroidalUpdate()
{   
    if( m_verbose ) {
        std::cout << "centroidal update " << m_nPrevUpdates << std::endl;
    }

    // reused
    int64_t * srcMap_d = voxelIndicesSorted_d;

    cudaMemset( centerOfMass_d, 0, m_nSites * 3 * sizeof( float ) );
    cudaMemset( netMass_d,      0, m_nSites * sizeof( float ) );
    cudaMemset( siteDeltas_d,   0, m_nSites * sizeof( float ) );

    sumMomentsGeodesic<<< gridSize( VX_THREADS_PER_BLOCK, M_NV ), VX_THREADS_PER_BLOCK >>>(
        m_nPrevUpdates,
        srcMap_d,
        data_d,
        voxelSiteIds_d,
        siteComponents_d,
        sitePositions_d,
        voxelSiteStatus_d,
        componentSiteCounts_d,
        netMass_d,
        centerOfMass_d,
        m_simpleSingleSiteCMPHeuristic,
        m_hasBackground,
        m_overrideLineOfEdge );

    gpuErrchk( cudaPeekAtLastError() );
    gpuErrchk( cudaDeviceSynchronize() );

    return updateSites();
}

///////////////////////////////////////////////////////////////////////

// Weighted Version

CentroidalUpdateStatistics WeightedCentroidalSampler::geodesicCentroidalUpdate() 
{   
    // reused
    int64_t * srcMap_d = voxelIndicesSorted_d;

    cudaMemset( centerOfMass_d, 0, m_nSites * 3 * sizeof( float ) );
    cudaMemset( netMass_d,      0, m_nSites * sizeof( float ) );
    cudaMemset( siteDeltas_d,   0, m_nSites * sizeof( float ) );

    if( m_verbose ) {
        std::cout << "w nprev " << m_nPrevUpdates << std::endl;
    }

    sumMomentsGeodesic<<< gridSize( VX_THREADS_PER_BLOCK, M_NV ), VX_THREADS_PER_BLOCK >>>(
        m_nPrevUpdates,
        mass_d,
        srcMap_d,
        voxelSiteIds_d,
        siteComponents_d,
        sitePositions_d,
        voxelSiteStatus_d,
        componentSiteCounts_d,
        netMass_d,
        centerOfMass_d,
        1.f,
        m_simpleSingleSiteCMPHeuristic,
        m_hasBackground,
        m_overrideLineOfEdge );

    gpuErrchk( cudaPeekAtLastError() );
    gpuErrchk( cudaDeviceSynchronize() );

    return updateSites();
}
