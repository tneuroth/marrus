
#include "lsrcvt_lib.hpp"

#include <boost/graph/graph_traits.hpp>
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/floyd_warshall_shortest.hpp>
#include <boost/graph/johnson_all_pairs_shortest.hpp>
#include <boost/graph/exterior_property.hpp>

#include <vector>
#include <unordered_map>
#include <unordered_set>
#include <cstdint>

typedef boost::adjacency_list<
boost::listS,
      boost::vecS,
      boost::undirectedS,
      boost::no_property,
      boost::property < boost::edge_weight_t, float > > graph_t;

typedef boost::exterior_vertex_property< graph_t, float > DistanceProperty;
typedef DistanceProperty::matrix_type DistanceMatrix;
typedef DistanceProperty::matrix_map_type DistanceMatrixMap;

typedef boost::property_map< graph_t, boost::edge_weight_t>::type WeightMap;
typedef boost::graph_traits < graph_t >::vertex_descriptor vertex_descriptor;
typedef boost::graph_traits < graph_t >::edge_descriptor   edge_descriptor;

void CentroidalSampler::floydWarshal()
{

    if( m_verbose ) {
        std::cout << "computing graph" << std::endl;
    }

    std::vector< std::pair< int32_t, int32_t > > edge_array;
    std::vector< float > weights;
    std::unordered_map< int32_t, std::unordered_set< int32_t > > mapped;

    for( int32_t i = 0; i < m_nSites; ++i )
    {
        for( const auto & j : m_siteAdjacency[ i ] )
        {
            if( j < m_nSites )
            {
                if( mapped.count( i ) )
                {
                    if( mapped.at( i ).count( j ) )
                    {
                        continue;
                    }
                }
                else
                {
                    mapped.insert( { i, std::unordered_set<int32_t>() } );
                }

                if( j < 0 || j >= m_nSites )
                {
                    std::cout << "out of bounds" << std::endl;
                    exit( 0 );
                }

                mapped.at( i ).insert( j );
                edge_array.push_back( { i, j } );
                weights.push_back( m_siteEuclideanDistanceMatrix[ i * m_nSites + j ] );
            }
        }
    }


    if( m_verbose ) {
        std::cout << "created edge list " << edge_array.size() << std::endl;
    }

    graph_t g(
        edge_array.begin(),
        edge_array.end(),
        weights.begin(),
        m_nSites );
    
    if( m_verbose ) {
        std::cout << "created graph " << boost::num_vertices( g ) << " " << boost::num_edges( g ) << std::endl;
    }

    DistanceMatrix distances( boost::num_vertices( g ) );
    DistanceMatrixMap dm( distances, g );

    if( m_verbose ) {
        std::cout << "running johnson_all_pairs_shortest_paths" << std::endl;
    }

    // boost::floyd_warshall_all_pairs_shortest_paths( g, dm );
    boost::johnson_all_pairs_shortest_paths( g, dm );

    if( m_verbose ) {
        std::cout << "topological distance" << std::endl;
    }

    m_siteGeodesicDistanceMatrix.resize( m_nSites * m_nSites );

    for( int32_t i = 0; i < m_nSites; ++i )
    {
        for( int32_t j = 0; j < m_nSites; ++j )
        {
            m_siteGeodesicDistanceMatrix[ i * m_nSites + j ] = dm[ i ][ j ];
        }
    }

    if( m_verbose ) {
        std::cout << "done" << std::endl;
    }
}
