
#ifndef TN_WEIGHTED_CENTROIDAL_SAMPLER_HPP
#define TN_WEIGHTED_CENTROIDAL_SAMPLER_HPP

#include "lsrcvt_lib.hpp"

class WeightedCentroidalSampler : public CentroidalSampler {

    float * mass_d;

    // this makes the base class version which has a different 
    // signature since its missing the mass parameter 
    // inaccessable to user code for the derived class
    using CentroidalSampler::initializeSiteDistribution;

public:

    WeightedCentroidalSampler( const std::array< int64_t, 3 > & dims );

    void initializeSiteDistribution(
        float * mass,
        float density,
        float uniformityBias,
        bool saveResults = false,
        const std::string & outPath = "" );
    
    CentroidalUpdateStatistics geodesicCentroidalUpdate() override;

    virtual ~WeightedCentroidalSampler();
};

#endif