#ifndef TN_CU_CONSTANTS_CUH
#define TN_CU_CONSTANTS_CUH

#include <cuda.h>
#include <cstdint>

#define CM_THREADS_PER_BLOCK 64
#define SC_THREADS_PER_BLOCK 64
#define VX_THREADS_PER_BLOCK 64
#define RESOSOLVE_INNER_EDGES 1

// in unit voxels 
#define DIST_EPSILON 0.1
#define FTOI_SCALE 10000000


typedef longlong3 LI3;

__constant__ LI3 D_DIMS;
__constant__ int64_t D_NV;

__constant__ LI3 ntable[ 27 ] =
{
    { 0,0,0}, { 0,0,-1}, { 0,-1,-1}, { 0,-1,0}, { 0,-1,1}, { 0,0,1}, { 0,1,1}, { 0,1,0}, { 0,1,-1},
    {-1,0,0}, {-1,0,-1}, {-1,-1,-1}, {-1,-1,0}, {-1,-1,1}, {-1,0,1}, {-1,1,1}, {-1,1,0}, {-1,1,-1},
    { 1,0,0}, { 1,0,-1}, { 1,-1,-1}, { 1,-1,0}, { 1,-1,1}, { 1,0,1}, { 1,1,1}, { 1,1,0}, { 1,1,-1}
};

#define BACKGROUND_VX_ID     -13
#define VX_SOURCE_NONE       -1
#define VX_MIN_NEIGHBOR_NONE -1

const uint8_t UI8_0   = static_cast< uint8_t >(  0 );
const uint8_t UI8_1   = static_cast< uint8_t >(  1 );
const uint8_t UI8_2   = static_cast< uint8_t >(  2 );
const uint8_t UI8_4   = static_cast< uint8_t >(  4 );
const uint8_t UI8_6   = static_cast< uint8_t >(  6 );
const uint8_t UI8_8   = static_cast< uint8_t >(  8 );
const uint8_t UI8_16  = static_cast< uint8_t >( 16 );
const uint8_t UI8_32  = static_cast< uint8_t >( 32 );
const uint8_t UI8_48  = static_cast< uint8_t >( 48 );
const uint8_t UI8_64  = static_cast< uint8_t >( 64 );
const uint8_t UI8_128 = static_cast< uint8_t >( 128 );

const uint8_t VX_ACTIVE_FLAG   = UI8_2;
const uint8_t VX_RESOLVED_FLAG = UI8_4;
const uint8_t VX_EDGE_FLAG     = UI8_128;

inline __host__ __device__ bool srcIsSite(          const uint8_t b ) { return ( b &  UI8_1    ) != UI8_0; }
inline __host__ __device__ bool isActive(           const uint8_t b ) { return ( b &  UI8_2    ) != UI8_0; }
inline __host__ __device__ bool isResolved(         const uint8_t b ) { return ( b &  UI8_4    ) != UI8_0; }
inline __host__ __device__ bool isActiveOrResolved( const uint8_t b ) { return ( b &  UI8_6    ) != UI8_0; }
inline __host__ __device__ bool isLineOfSite(       const uint8_t b ) { return ( b &  UI8_8    ) != UI8_0; }

inline __host__ __device__ bool inSrcSpace( const uint8_t b ) { return ( b &  UI8_16   ) != UI8_0; }
// inline __host__ __device__ bool isEdgeSRCLeaf(      const uint8_t b ) { return ( b &  UI8_32   ) != UI8_0; }
// inline __host__ __device__ bool isEdgeSRCNode(      const uint8_t b ) { return ( b &  UI8_48   ) != UI8_0; }

inline __host__ __device__ bool wasUpdated(        const uint8_t b ) { return (  b &  UI8_64   )  != UI8_0; }
inline __host__ __device__ bool onOuterEdge(        const uint8_t b ) { return ( b &  UI8_128  )  != UI8_0; }

inline __host__ __device__ bool clearFlags(      uint8_t & b ) { b = UI8_0; }
inline __host__ __device__ void setSrcIsSite(    uint8_t & b, const bool v ) { b = ( b & ~ UI8_1   ) | ( static_cast< uint8_t >( v ) * UI8_1   ); }
inline __host__ __device__ void setActive(       uint8_t & b, const bool v ) { b = ( b & ~ UI8_2   ) | ( static_cast< uint8_t >( v ) * UI8_2   ); }
inline __host__ __device__ void setResolved(     uint8_t & b, const bool v ) { b = ( b & ~ UI8_4   ) | ( static_cast< uint8_t >( v ) * UI8_4   ); }
inline __host__ __device__ void setLineOfSite(   uint8_t & b, const bool v ) { b = ( b & ~ UI8_8   ) | ( static_cast< uint8_t >( v ) * UI8_8   ); }

inline __host__ __device__ void setInSrcSpace(  uint8_t & b, const bool v ) { b = ( b & ~ UI8_16  ) | ( static_cast< uint8_t >( v ) * UI8_16  ); }

// inline __host__ __device__ void setSrcSpace(  uint8_t & b, const bool v ) { b = ( b & ~ UI8_32  ) | ( static_cast< uint8_t >( v ) * UI8_32  ); }

inline __host__ __device__ void setWasUpdated(   uint8_t & b, const bool v ) { b = ( b & ~ UI8_64  ) | ( static_cast< uint8_t >( v ) * UI8_64  ); }

// inline __host__ __device__ void setInnerEdge(    uint8_t & b, const bool v ) { b = ( b & ~ UI8_64  ) | ( static_cast< uint8_t >( v ) * UI8_64  ); }
inline __host__ __device__ void setOnOuterEdge(    uint8_t & b, const bool v ) { b = ( b & ~ UI8_128 ) | ( static_cast< uint8_t >( v ) * UI8_128 ); }

#endif