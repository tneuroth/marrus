
#include "lsrcvt_lib.hpp"
#include "configuration.hpp"

#include <cstdint>
#include <vector>
#include <fstream>
#include <iostream>
#include <chrono>

using namespace std::chrono;
using namespace std;

template< typename T >
void writeData( 
    const string & filePath,
    const vector< T > & data )
{
    cout << "Writing data to " <<  filePath << endl;
    ofstream outFile( filePath, ios::out | ios::binary );
    if( ! outFile.is_open() ) {
        cerr << "file could not be opened for writing" << filePath << endl;
        exit( 1 );
    }
    outFile.write( ( char* ) data.data(), data.size() * sizeof( T )  );
    outFile.close();
}

template< typename T >
void loadData( 
    const string & filePath,
    vector< T > & data, 
    size_t offset, 
    size_t numElements )
{
    ifstream inFile( filePath, ios::in | ios::binary );
    if( ! inFile.is_open() ) {
        cout << "could not be opened for reading " << filePath << endl;
        exit( 1 );
    }
    data.resize( numElements );
    inFile.seekg( offset * sizeof( T ) );
    inFile.read( (char*) data.data(), numElements * sizeof( T ) ); 
    inFile.close();
}

int main(int argc, char** argv)
{
    /**************************************************

                         Parameters

    ***************************************************/

    if( argc != 2 ) {
        cerr << "program expects argument <configuration file path>" << endl;
        cerr << "arg count was " << argc - 1 << endl;
        exit( 1 );
    }

    LSRCVD_Configuration configuration( argv[ 1 ] );

    if( ! configuration.isValid() ) {
        cerr << "Configuration " << argv[ 1 ] << " not valid\n";
        cerr << configuration.getMessage();
    }

    // required parameters

    const auto dims              = configuration.dims;
    const auto dataFile          = configuration.dataFile;
    const auto dataFormat        = configuration.dataFormat;
    const auto dataOffset        = configuration.dataOffset;    
    const auto layers            = configuration.layers;
    const auto siteDensity       = configuration.siteDensity;
    const auto outPath           = configuration.outputDirectory;
    const auto stoppingCondition = configuration.stoppingCondition;
    const auto maxIterations     = configuration.maxIterations;
    const auto validate          = configuration.validate;
    const auto verbose           = configuration.verbose;

    // additional require parameters for the weighted case

    const auto massFile    = configuration.massFile;
    const auto massFormat  = configuration.massFormat;
    const auto massOffset  = configuration.massOffset;

    const auto attractionBias = configuration.attractionBias;

    // optional parameters for testing and animation

    const auto outputCentroidalUpdates  = configuration.outputCentroidalUpdates;
    const auto maxLOESteps              = configuration.maxLOESteps;
    const auto maxLOSSteps              = configuration.maxLOSSteps;    
    const auto outputLOSSteps           = configuration.outputLOSSteps;
    const auto outputLOESteps           = configuration.outputLOESteps;
    const auto overrideLOE              = configuration.overrideLOE;

    /**************************************************

                        Load Data

    ***************************************************/

    const int64_t N = dims[ 0 ] * dims[ 1 ] * dims[ 2 ];
    vector< float > data( N );
    if( dataFormat == "double" ) {
        vector< double > tmp( N );
        loadData( dataFile, tmp, N*dataOffset, N  );      
        data = std::vector< float >( tmp.begin(), tmp.end() );  
    } else {
        loadData( dataFile, data, N*dataOffset, N );
    }

    /**************************************************

                     LRV Decomposition

    ***************************************************/

    CentroidalSampler sampler( dims );
    
    sampler.setValidate( validate );
    sampler.setVerbose(   verbose );

    sampler.computeLayerDecomposition( data.data(), layers ); 
    sampler.initializeSiteDistribution( siteDensity, false, outPath );

    vector< float > maxDeltas;
    vector< float > meanDeltas;    

    int64_t losStepsTook = 0;
    int64_t loeStepsTook = 0;   

    for( int i = 0; i < maxIterations;++i ) {

        const string iterStr      = to_string( i ); 
        const string iterStrZfill = string( 5 - iterStr.size(), '0' ) + iterStr;

        // optional partial computations for generating animations ////////////////

        if( outputLOSSteps ) {
            // for( int j = 0; j < maxLOSSteps; ++j ) {
            //     const string jStr      = to_string( j ); 
            //     const string jStrZfill = string( 5 - jStr.size(), '0' ) + jStr;
            //     sampler.geodesicVoronoiClassification( 
            //         j,    // -1 -> no limit to steps in LOS classification process
            //         0,    // LOE steps
            //         true, // parameter to override LOE stage
            //         true,
            //         losStepsTook,    // recieve back the number of steps taken
            //         loeStepsTook     // recieve back the number of steps taken
            //     );

            //     sampler.finalize();
            //     writeData( outPath + "voxelStatuses." + iterStrZfill + ".los." + jStrZfill + ".dat", sampler.getVoxelStatuses() ); 
            //     if( losStepsTook < j ) { break; }
            // }
        }

        if( outputLOESteps && ! overrideLOE ) {
            // for( int j = 0; j < maxLOESteps; ++j ) {
            //     const string jStr      = to_string( j ); 
            //     const string jStrZfill = string( 5 - jStr.size(), '0' ) + jStr;
            //     sampler.geodesicVoronoiClassification( 
            //         -1 ,             // -1 -> no limit to steps in LOS classification process
            //         j,   // -1 -> no limit to steps in LOE classification process
            //         false,           // parameter to override LOE stage
            //         true,
            //         losStepsTook,    // recieve back the number of steps taken
            //         loeStepsTook     // recieve back the number of steps taken
            //     );
            //     sampler.finalize();                
            //     writeData( outPath + "voxelStatuses." + iterStrZfill + ".loe." + jStrZfill + ".dat", sampler.getVoxelStatuses() ); 
            //     if( loeStepsTook < j ) { break; }
            // }
        }

        // Normal Full Stage ////////////////////////////////////////////////////////////////////////////////

        auto t0 = std::chrono::high_resolution_clock::now();

        sampler.geodesicVoronoiClassification( 
            -1 ,             // -1 -> no limit to steps in LOS classification process
            -1,              // -1 -> no limit to steps in LOE classification process
            overrideLOE,     // parameter to override LOE stage
            false,            // simple heuristic for components with only one site (pre-classify and move only once) 
            losStepsTook,    // recieve back the number of steps taken
            loeStepsTook     // recieve back the number of steps taken
        );

        std::cout << "Classification took: " 
                  << std::chrono::duration_cast<std::chrono::milliseconds>( std::chrono::high_resolution_clock::now() - t0 ).count() / 1000.0
                  << " seconds" << std::endl;

        if( outputCentroidalUpdates ) {
            // sampler.finalize();
            // writeData( outPath + "sitePositions." + iterStrZfill + ".dat", sampler.getSitePositions()  );
            // writeData( outPath +   "tesselation." + iterStrZfill + ".dat", sampler.getClassification() );   
            // writeData( outPath + "voxelStatuses." + iterStrZfill + ".dat", sampler.getVoxelStatuses()  );
            // writeData( outPath + "distances."     + iterStrZfill + ".dat", sampler.getVoxelDistances()  );
            // writeData( outPath + "sourceMap."     + iterStrZfill + ".dat", sampler.getSourceMap()  );            
        }

        t0 = std::chrono::high_resolution_clock::now();

        CentroidalUpdateStatistics stats = sampler.geodesicCentroidalUpdate();

        std::cout << "Centroidal Update took: " 
                  << std::chrono::duration_cast<std::chrono::milliseconds>( std::chrono::high_resolution_clock::now() - t0 ).count()  / 1000.0
                  << " seconds" << std::endl;

        maxDeltas.push_back(   stats.maxDelta );
        meanDeltas.push_back( stats.meanDelta );       

        cout << "ITER=" << i << ", MAX_DELTA=" << stats.maxDelta << ", MEAN_DELTA=" << stats.meanDelta  <<  endl; 

        if( stoppingCondition.first == "meanDelta" ) {
            if( stats.meanDelta  < stoppingCondition.second ) {
                std::cout << "Stopping condition meanDelta = " 
                          << stats.meanDelta << " < " 
                          << stoppingCondition.second 
                          << " met." << std::endl;
                break;
            }
        } 
        else if( stoppingCondition.first == "maxDelta" ) {
            if( stats.maxDelta < stoppingCondition.second ) {
                std::cout << "Stopping condition maxDelta = " 
                          << stats.maxDelta << " < " 
                          << stoppingCondition.second 
                          << " met." << std::endl;     
                break;
            }
        }
    }

    sampler.geodesicVoronoiClassification( 
        -1 ,             // -1 -> no limit to steps in LOS classification process
        -1,              // -1 -> no limit to steps in LOE classification process
        overrideLOE,     // parameter to override LOE stage
        false,            // simple heuristic for components with only one site (pre-classify and move only once) 
        losStepsTook,    // recieve back the number of steps taken
        loeStepsTook     // recieve back the number of steps taken
    );

    // compute the distances would be here I think ...

    sampler.finalize();
    sampler.toFile( outPath, false );

    // writeData( outPath + "sitePositions.dat", sampler.getSitePositions()  );
    // writeData( outPath +   "tesselation.dat", sampler.getClassification() );
    //writeData( outPath + "voxelStatuses.dat", sampler.getVoxelStatuses()  );
    //writeData( outPath + "distances.dat", sampler.getVoxelDistances()  );
    //writeData( outPath + "sourceMap.dat", sampler.getSourceMap()  );
    // writeData( outPath +  "maxDelta.float." + to_string( maxDeltas.size() ) + ".dat", maxDeltas  );
    // writeData( outPath + "meanDelta.float." + to_string( maxDeltas.size() ) + ".dat", meanDeltas );   
}
