
#include "lsrcvt_lib.hpp"

#include <geometry/Vec.hpp>

#include <vector>
#include <iostream>
#include <cstdint>

// assumes adjacency computed already
void CentroidalSampler::computeSiteGeodesicDistances()
{
    computeSiteAdjacency();

    if( m_verbose ) {
        std::cout << "allocating distance matrix for " << m_nSites << " sites " << std::endl;
    }

    m_siteEuclideanDistanceMatrix
        = std::vector< float >( m_nSites * m_nSites, 0.f );

    if( m_verbose ) {
        std::cout << "computing site distances " << std::endl;
    }

    // euclidean distance
    for( int32_t i = 0; i < m_nSites; ++i )
    {
        TN::Vec3< float > sitePos1 =
        {
            m_sitePoints[ i * 3 + 0 ],
            m_sitePoints[ i * 3 + 1 ],
            m_sitePoints[ i * 3 + 2 ]
        };

        for( int32_t j = 0; j < m_nSites; ++j )
        {
            if( i == j )
            {
                m_siteEuclideanDistanceMatrix[ i * m_nSites + j ] = 0;
                continue;
            }

            TN::Vec3< float > sitePos2 =
            {
                m_sitePoints[ j * 3 + 0 ],
                m_sitePoints[ j * 3 + 1 ],
                m_sitePoints[ j * 3 + 2 ]
            };

            m_siteEuclideanDistanceMatrix[ i * m_nSites + j ]
                = sitePos1.distance3( sitePos2 );
        }
    }

    if( m_verbose ) {
        std::cout << "set euclidean distance" << std::endl;
    }

    floydWarshal();
}