
#include "lsrcvt_lib.hpp"
#include "constants.cuh"

#include "geometry/Vec.hpp"

#include <cstdint>

void CentroidalSampler::computeSiteAdjacency()
{
    if( m_verbose ) {
        std::cout << "computeSiteAdjacency " << std::endl;
    }

    // for each voxel in classification, mark its site as a neighbor of the voxel neighbors site.
    m_siteAdjacency.resize( m_nSites );

    const TN::Vec3< int64_t > neighborhood[ 27 ] =
    {
        { 0,0,0}, { 0,0,-1}, { 0,-1,-1}, { 0,-1,0}, { 0,-1,1}, { 0,0,1}, { 0,1,1}, { 0,1,0}, { 0,1,-1},
        {-1,0,0}, {-1,0,-1}, {-1,-1,-1}, {-1,-1,0}, {-1,-1,1}, {-1,0,1}, {-1,1,1}, {-1,1,0}, {-1,1,-1},
        { 1,0,0}, { 1,0,-1}, { 1,-1,-1}, { 1,-1,0}, { 1,-1,1}, { 1,0,1}, { 1,1,1}, { 1,1,0}, { 1,1,-1}
    };

    for( int64_t i = 0; i < M_NV; ++i )
    {
        int32_t vxSite = m_voxelSiteIds[ i ];
        int32_t vxCmp  = m_voxelComponentIds[ i ];

        if( vxSite >= m_nSites )
        {
            std::cout << " site id is out of range " << std::endl;
            exit(1);
        }

        if( ( vxCmp < 0 && vxSite >= 0 ) || vxCmp >= m_NC + ( m_NC == m_maxCId  ) )
        {
            std::cout << " cmp id is out of range " << vxCmp << " vs " << m_NC << " and " << vxSite << " vs " << m_nSites << std::endl;
            exit(1);
        }

        if( vxSite >= 0 ) {
            TN::Vec3< int64_t > xyzIdx = TN::index3dCM( i, M_DIMS );

            for( int64_t j = 1; j < 27; ++j )
            {
                TN::Vec3< int64_t > nIdx = xyzIdx + neighborhood[ j ];

                if( ( nIdx.x() < 0 || nIdx.x() >= M_DIMS[ 0 ] )
                        || ( nIdx.y() < 0 || nIdx.y() >= M_DIMS[ 1 ] )
                        || ( nIdx.z() < 0 || nIdx.z() >= M_DIMS[ 2 ] ) )
                {

                    continue;
                }

                int64_t vxN = TN::flatIndex3dCM(
                                  nIdx.x(),
                                  nIdx.y(),
                                  nIdx.z(),
                                  M_DIMS[ 0 ],
                                  M_DIMS[ 1 ],
                                  M_DIMS[ 2 ] );

                if( vxSite >= m_nSites )
                {
                    std::cout << "error vxSite " << vxSite << " vs " << m_nSites << std::endl;
                    exit( 0 );
                }

                if( vxN < 0 || vxN >= M_NV )
                {
                    std::cout << "error vxN " << vxN << " vs " << M_NV << std::endl;
                    exit( 0 );
                }

                int32_t site2 = m_voxelSiteIds[ vxN ];
                int32_t cmp2  = m_voxelComponentIds[ vxN ];

                if( site2 >= 0 && vxCmp == cmp2 )
                {
                    if( site2 >= m_nSites )
                    {
                        std::cout << "error site2 " << site2 << " vs " << m_nSites << std::endl;
                        exit( 0 );
                    }

                    m_siteAdjacency[ vxSite ].insert( site2 );
                }
            }
        }
    }
}