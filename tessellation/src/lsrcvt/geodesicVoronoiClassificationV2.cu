
#include "lsrcvt_lib.hpp"
#include "utility.cuh"
#include "constants.cuh"

#include <thrust/reduce.h>
#include <thrust/execution_policy.h>
#include <thrust/device_ptr.h>
#include <thrust/iterator/discard_iterator.h>
#include <thrust/functional.h>
#include <thrust/transform_scan.h>
#include <thrust/device_vector.h>
#include <thrust/device_ptr.h>
#include <thrust/iterator/permutation_iterator.h>
#include <thrust/partition.h>

#include <chrono>
#include <cstdint>
#include <cfloat>
#include <bitset>

using namespace std::chrono;

__global__
void resolve(
    const int64_t     NE,
    const int64_t   * ids,
    const int       * minFronts, 
    uint8_t         * status,      
    const int32_t   * componentIds,   
    const float     * distances  )   
{
    const int64_t edx = blockIdx.x * blockDim.x + threadIdx.x;
    if( edx >= NE ) { return; }

    const int64_t idx = ids[ edx ];
    const uint8_t VX_STATUS = status[    idx ];
    const int32_t cmp_id = componentIds[ idx ];

    if( isActive( status[ idx ] ) )
    {
        int32_t d = static_cast<int32_t>(ceil( distances[ idx ] * FTOI_SCALE ) );

        if( d < minFronts[ cmp_id ] )
        {
            setResolved( status[ idx ], true );
        }
    }
}


__global__
void updateFront(
    const int64_t     NE,
    const int64_t   * ids,
    int             * minFronts, 
    const uint8_t   * status,      
    const int32_t   * componentIds,   
    const float     * distances  )   
{
    const int64_t edx = blockIdx.x * blockDim.x + threadIdx.x;
    if( edx >= NE ) { return; }

    const int64_t idx = ids[ edx ];

    const uint8_t VX_STATUS = status[    idx ];
    const int32_t cmp_id = componentIds[ idx ];

    if( onOuterEdge( VX_STATUS ) )
    {
        int32_t d = static_cast<int32_t>(floor( distances[ idx ] * FTOI_SCALE ));
        atomicMin( & minFronts[ cmp_id ], d );
    }
}

__global__
void updateSrcSpace(
    const int64_t NE,
    const int64_t * ids,
    bool boundaries,
    const int64_t   level,
    const int32_t * voxelComponentIds,
    const int32_t * voxelSiteIds,
    uint8_t       * voxelStatus ) 
{
    const int64_t edx = blockIdx.x * blockDim.x + threadIdx.x;
    if( edx >= NE ) { return; }

    const int64_t idx = ids[ edx ];

    setOnOuterEdge( voxelStatus[ idx ], false );

    const int32_t siteId = voxelSiteIds[ idx ];
    if( siteId == BACKGROUND_VX_ID ) { return; }

    if( isActive( voxelStatus[ idx ] ) )
    {
        const int32_t my_cmp_id = voxelComponentIds[ idx ];
        const LI3 my_idx3 = index3D( idx, D_DIMS );
        const int32_t my_site_id = voxelSiteIds[ idx ];

        // a voxel can lie on both an inner and outer edge at the same time

        bool onInnerEdge = false;
        bool onOuterEdge = false;
        bool onBoarder   = false;

        ///////////////////////////////////////////////////////////////////////////

        for( int i = 1; i < 27; ++i )
        {
            // compute indices and check bounds
            const LI3 nb_idx3 = my_idx3 + ntable[ i ];
            if( ! inBounds( nb_idx3, D_DIMS ) ) { continue; }
            
            const int64_t nb_idx = flatIndex3D( nb_idx3, D_DIMS );
            const int32_t nb_site_id = voxelSiteIds[ nb_idx ];

            const bool NB_ACTIVE = isActive( voxelStatus[ nb_idx ] );

            //component boundaries
            if( voxelComponentIds[ nb_idx ] != voxelComponentIds[ idx ] ) { 

                if( boundaries ) 
                {
                    onBoarder = true;
                } 
                else 
                {
                    continue;
                }
            }

            // unseen
            if( ! NB_ACTIVE ) { 
                onOuterEdge = true;
                if( onInnerEdge ) { break; }
            }
            
            //  bordering a different site's region 
            if( NB_ACTIVE && ( my_site_id != nb_site_id ) ) { 
                onInnerEdge = true;
                if( onOuterEdge ) { break; }
            }
        }

        if( level == 0 ) {
            setLineOfSite( voxelStatus[ idx ], true );
        }

        if( onOuterEdge ) 
        { 
            setInSrcSpace(  voxelStatus[ idx ], true ); 
            setOnOuterEdge( voxelStatus[ idx ], true ); 
        } 
        else
        {
            setInSrcSpace(  voxelStatus[ idx ], wasUpdated( voxelStatus[ idx ] ) );
            setOnOuterEdge( voxelStatus[ idx ], false );
        }
    }
}

__global__
void applyUpdates(
    bool resolvingInner,
    const int64_t     NE,
    const int         level,
    const int64_t   * ids,
    const int8_t    * minNeighbor,
    const int64_t   * sourceMapCopy,
    const int32_t   * siteMapCopy,    
    const uint8_t   * statusCopy,    
    const float     * distCopy,     
    int64_t         * sourceMap,
    int32_t         * siteMap,    
    uint8_t         * statuses,  
    float           * distances  )   
{
    const int64_t edx = blockIdx.x * blockDim.x + threadIdx.x;
    if( edx >= NE ) { return; }

    const int64_t idx = ids[ edx ];
    const uint8_t VX_STATUS = statusCopy[ edx ];

    if( isResolved( VX_STATUS ) ) { return; }

    if( minNeighbor[ idx ] != VX_MIN_NEIGHBOR_NONE ) 
    {
        distances[ idx ] = distCopy[      edx ];
        sourceMap[ idx ] = sourceMapCopy[ edx ];
        siteMap[   idx ] = siteMapCopy[   edx ];
        statuses[  idx ] = statusCopy[    edx ];

        setWasUpdated( statuses[  idx ], true );

        if( level > 0 ) {
            setLineOfSite( statuses[  idx ], false );
        }
    }
}

__global__
void applyCopy(
    const int64_t     NE,
    const int64_t   * ids,
    const int64_t   * sourceMap,
    const int32_t   * siteIds,
    const uint8_t   * voxelStatus,    
    const float     * sitePos,
    const float     * distances,
    float           * distCopy,   
    int8_t          * minNeighbor,
    int32_t         * siteIdsCopy,
    int64_t         * srcIdsCopy,
    uint8_t         * statusCopy )
{
    const int64_t edx = blockIdx.x * blockDim.x + threadIdx.x;
    if( edx >= NE ) { return; }

    const int64_t idx  = ids[ edx ];
    minNeighbor[ idx ] = VX_MIN_NEIGHBOR_NONE;
    distCopy[    edx ] = distances[   idx ];
    siteIdsCopy[ edx ] = siteIds[     idx ];
    srcIdsCopy[  edx ] = sourceMap[   idx ];
    statusCopy[  edx ] = voxelStatus[ idx ];
}

__global__
void classifyLineOfSite(
    const int64_t     NE,
    const int64_t     NS,
    const int64_t   * ids,
    const int64_t   * sourceMap,
    const int32_t   * siteIds,
    const int32_t   * voxelComponentIds,
    const uint8_t   * voxelStatus,    
    const float     * sitePos,
    const float     * distances,
    float           * distCopy,   
    int8_t          * minNeighbor,
    int32_t         * siteIdsCopy,
    int64_t         * srcIdsCopy,
    uint8_t         * statusCopy )
{
    const int64_t edx = blockIdx.x * blockDim.x + threadIdx.x;
    if( edx >= NE ) { return; }

    const int64_t idx  = ids[ edx ];

    const int32_t my_site_id = siteIds[ idx ];
    if( my_site_id == BACKGROUND_VX_ID ) { return; }

    const uint8_t VX_STATUS  = voxelStatus[ idx ];

    if( isResolved( VX_STATUS ) ) { return; }

    const int64_t my_src_idx = sourceMap[         idx ];
    const int32_t my_cmp_idx = voxelComponentIds[ idx ];

    const LI3 idx3d = index3D( idx, D_DIMS );
    const float3 voxelPos = location3D( idx3d, D_DIMS );

    for( int i = 1; i < 27; ++i )
    {
        const LI3 nb_idx3d = idx3d + ntable[ i ];
        if( ! inBounds( nb_idx3d, D_DIMS ) ) { continue; }

        const int64_t nb_idx     = flatIndex3D( nb_idx3d, D_DIMS );
        const int64_t nb_src_id  = sourceMap[   nb_idx ];
        const uint8_t nb_status  = voxelStatus[ nb_idx ];

        const bool N_SRC_IS_SITE = srcIsSite( nb_status );

        if(  
             ( nb_src_id == VX_SOURCE_NONE ) ||
             ( voxelComponentIds[ nb_idx ] != my_cmp_idx ) ) { 
            continue; 
        }

        if( inSrcSpace( nb_status ) )  {

            const float fullDist =
              + distances[ nb_idx ] 
              + voxelDist( idx3d, nb_idx3d );

            if( fullDist + DIST_EPSILON < distCopy[ edx ] ) {
                minNeighbor[ idx ] = i;
                distCopy[    edx ] = fullDist;
                srcIdsCopy[  edx ] = nb_idx;
                siteIdsCopy[ edx ] = siteIds[ nb_idx ];        

                setActive(      statusCopy[ edx ], true  );
                setSrcIsSite(   statusCopy[ edx ], false );
            }
        }
        else { 

            const float3 sourcePosition = N_SRC_IS_SITE ?  
                  float3( { sitePos[ nb_src_id * 3 + 0 ], sitePos[ nb_src_id * 3 + 1 ], sitePos[ nb_src_id * 3 + 2 ] } )
                : location3D( index3D( nb_src_id, D_DIMS ), D_DIMS );

            const float lx = ( sourcePosition.x - voxelPos.x ) * ( D_DIMS.x - 1 );
            const float ly = ( sourcePosition.y - voxelPos.y ) * ( D_DIMS.y - 1 );
            const float lz = ( sourcePosition.z - voxelPos.z ) * ( D_DIMS.z - 1 );

            const float distToSource = sqrt( lx*lx + ly*ly + lz*lz );
            
            const float fullDist = N_SRC_IS_SITE ? 
                distToSource 
              : distToSource + distances[ nb_idx ];

            if( ( fullDist + DIST_EPSILON ) < distCopy[ edx ] ) {

                bool lineOfSite = true;

                const int N_CHECK = floor( max( max( abs(lx), abs(ly) ), abs(lz) ) );

                const float dx = lx / N_CHECK;
                const float dy = ly / N_CHECK;
                const float dz = lz / N_CHECK;

                for( int j = 1; j < N_CHECK; ++j )
                {
                    const LI3 curr_idx3 = { 
                        __float2int_rn( idx3d.x + j * dx ),
                        __float2int_rn( idx3d.y + j * dy ),
                        __float2int_rn( idx3d.z + j * dz )
                    };
                    
                    if( ! inBounds( curr_idx3, D_DIMS ) ) { 
                        lineOfSite = false;
                        break;
                    }

                    if( my_cmp_idx != voxelComponentIds[ flatIndex3D( curr_idx3, D_DIMS ) ] )
                    {
                        lineOfSite = false;
                        break;
                    }
                }

                if( lineOfSite ) 
                {
                    minNeighbor[ idx ] = i;
                    distCopy[    edx ] = fullDist;
                    siteIdsCopy[ edx ] = siteIds[    nb_idx ];
                    srcIdsCopy[  edx ] = sourceMap[  nb_idx ];
                    setActive(    statusCopy[ edx ], true );      
                    setSrcIsSite( statusCopy[ edx ], N_SRC_IS_SITE );
                }
            }
        }
    }
}

__global__
void clearWasUpdated(
    const int64_t     NE,
    const int64_t   * ids,
    uint8_t         * voxelStatus )
{
    const int64_t edx = blockIdx.x * blockDim.x + threadIdx.x;
    if( edx >= NE ) { return; }
    const int64_t idx  = ids[ edx ];
    setWasUpdated( voxelStatus[ idx ], false );
}

__global__
void initializeVoxelStates(
    const int32_t   * voxelComponentIds,
    const bool hasBackground,
    int32_t * voxelSiteIds,
    uint8_t * voxelStatus,
    int8_t * minNeighbor,    
    float * currentDistance,
    int64_t * sourceMap )
{
    const int64_t idx = blockIdx.x * blockDim.x + threadIdx.x;
    if( idx >= D_NV ) { return; }
    clearFlags( voxelStatus[ idx ] );
    if( hasBackground && voxelComponentIds[ idx ] == 0 ) {
        setResolved( voxelStatus[ idx ], true );
        voxelSiteIds[ idx ] = BACKGROUND_VX_ID;
    }
    else {
        voxelSiteIds[ idx ]    = VX_SOURCE_NONE;
        currentDistance[ idx ] = 9999999999.0;
        minNeighbor[ idx ]     = VX_MIN_NEIGHBOR_NONE;
    }
    sourceMap[ idx ] = VX_SOURCE_NONE;
}

__global__
void classifySeedVoxels(
    const int64_t     NS,
    const float    * sitePositions,
    const int32_t   * siteComponents,
    const int64_t   * componentSiteCounts,        
    const int32_t   * voxelComponents,
    const bool        hasBackground, 
    const bool        simpleSingleSiteCMPHeuristic,
    float           * currentDistance,
    int32_t         * componentSitesSingle,
    int32_t         * voxelSiteIds,
    uint8_t         * voxelStatus,
    int64_t         * srcMap )
{
    const int64_t idx = blockIdx.x * blockDim.x + threadIdx.x;
    if( idx >= NS ) { return; }

    const int64_t xi = locationInverse1D( sitePositions[ idx * 3 + 0 ], D_DIMS.x );
    const int64_t yi = locationInverse1D( sitePositions[ idx * 3 + 1 ], D_DIMS.y );
    const int64_t zi = locationInverse1D( sitePositions[ idx * 3 + 2 ], D_DIMS.z );
    const int64_t vIdx = flatIndex3D( { xi, yi, zi }, D_DIMS );

    // note that because of the way the sites were initially distributed,  
    // there should never be two sites within the same voxel after inititialize distribution
    // so this cannot result in a race condition below in normal cirucmstances

    voxelSiteIds[ vIdx ] = idx;
    setResolved( voxelStatus[  vIdx ], true );
    currentDistance[ vIdx ] = 0;
    setSrcIsSite(  voxelStatus[  vIdx ], true );
    setLineOfSite( voxelStatus[  vIdx ], true );
    srcMap[ vIdx ] = idx;

    if( simpleSingleSiteCMPHeuristic )
    {
        const int32_t siteCMP = hasBackground ? siteComponents[ idx ] : siteComponents[ idx ] - 1;
        if( componentSiteCounts[  siteCMP ] == 1 ) {
            componentSitesSingle[ siteCMP ] = idx;
        } 
    }
}

void CentroidalSampler::geodesicVoronoiClassification() {
    int64_t losStepsTook, loeStepsTook;
    geodesicVoronoiClassification( 
           -1,        // -1 -> no limit to steps in LOS classification process
           -1,        // -1 -> no limit to steps in LOE classification process
        false,        // parameter to override LOE stage
        false,        // simple heuristic for components with only one site (pre-classify and move only once) 
        losStepsTook, // recieve back the number of steps taken
        loeStepsTook  // recieve back the number of steps taken
    );
}

void CentroidalSampler::geodesicVoronoiClassification( 
    const int64_t losStepMax, // -1 implies no limit, used to be able to step through the process and see intermediate results
    const int64_t loeStepMax, // -1 implies no limit, used to be able to step through the process and see intermediate results
    bool overrideLineOfEdge,  // used to be able to step through the process and see intermediate results
    bool simpleSingleSiteCMPHeuristic,
    int64_t & losStepsTook,   // used to be able to step through the process and see intermediate results
    int64_t & loeStepsTook )  // used to be able to step through the process and see intermediate results
{
    int64_t * sourceMap_d = voxelIndicesSorted_d;
    m_overrideLineOfEdge  = overrideLineOfEdge;
    m_simpleSingleSiteCMPHeuristic = simpleSingleSiteCMPHeuristic;

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////

    initializeVoxelStates<<< gridSize( VX_THREADS_PER_BLOCK, M_NV ), VX_THREADS_PER_BLOCK >>>(
        voxelComponentIds_d,
        m_hasBackground,            
        voxelSiteIds_d,
        voxelSiteStatus_d,
        minNeighbors_d,
        data_d,
        sourceMap_d );

    // gpuErrchk( cudaPeekAtLastError() );
    gpuErrchk( cudaDeviceSynchronize() );

    ////////////////////////////////////////////////////////////////////

    int32_t * cmpSiteIds_d = 0;
    cudaMalloc( (char**) & cmpSiteIds_d, m_NC * sizeof( int32_t ) );
    thrust::fill( 
        thrust::device_pointer_cast<int32_t>( cmpSiteIds_d ),
        thrust::device_pointer_cast<int32_t>( cmpSiteIds_d ) + m_NC,
        -1 );

    // gpuErrchk( cudaPeekAtLastError() );
    gpuErrchk( cudaDeviceSynchronize() );

    int32_t * min_fronts_d = 0;
    cudaMalloc( (char**) & min_fronts_d, m_NC * sizeof( int32_t ) );
    thrust::fill( 
        thrust::device_pointer_cast<int32_t>( min_fronts_d ),
        thrust::device_pointer_cast<int32_t>( min_fronts_d ) + m_NC,
        0 );

    // gpuErrchk( cudaPeekAtLastError() );
    gpuErrchk( cudaDeviceSynchronize() );

    classifySeedVoxels<<< gridSize( SC_THREADS_PER_BLOCK, m_nSites ), SC_THREADS_PER_BLOCK >>>(
        m_nSites,
        sitePositions_d,
        siteComponents_d,
        componentSiteCounts_d,
        voxelComponentIds_d,
        m_hasBackground,
        simpleSingleSiteCMPHeuristic,
        data_d,
        cmpSiteIds_d,
        voxelSiteIds_d,
        voxelSiteStatus_d,
        sourceMap_d );

    // gpuErrchk( cudaPeekAtLastError() );
    gpuErrchk( cudaDeviceSynchronize() );

    cudaFree( cmpSiteIds_d );

    ///////////////////////////////////////////////////////////////////

    int64_t NR_L = thrust::count_if(
        thrust::device_pointer_cast<uint8_t>( voxelSiteStatus_d ),
        thrust::device_pointer_cast<uint8_t>( voxelSiteStatus_d ) + M_NV,
        ( thrust::placeholders::_1 & VX_RESOLVED_FLAG ) == 0  );

    // gpuErrchk( cudaPeekAtLastError() );
    gpuErrchk( cudaDeviceSynchronize() );

    int64_t * unresolvedIds = 0;
    cudaMalloc( (char**) & unresolvedIds, NR_L * sizeof( int64_t ) );

    // gpuErrchk( cudaPeekAtLastError() );
    gpuErrchk( cudaDeviceSynchronize() );

    thrust::copy_if(
        thrust::make_counting_iterator<int64_t>( 0 ),
        thrust::make_counting_iterator<int64_t>( M_NV ),
        thrust::device_pointer_cast( voxelSiteStatus_d ),
        thrust::device_pointer_cast( unresolvedIds ),
        ( thrust::placeholders::_1 & VX_RESOLVED_FLAG ) == 0 ); 

    // gpuErrchk( cudaPeekAtLastError() );
    gpuErrchk( cudaDeviceSynchronize() );

    ////////////////////////////////////////////////////////////////////////////////////

    int64_t * srcIdCopy = 0;
    cudaMalloc( (char**) & srcIdCopy, NR_L * sizeof( int64_t ) );

    int32_t * siteIdCopy = 0;
    cudaMalloc( (char**) & siteIdCopy, NR_L * sizeof( int32_t ) );

    float * distCopy = 0;
    cudaMalloc( (char**) & distCopy, NR_L * sizeof( float ) );

    uint8_t * statusCopy = 0;
    cudaMalloc( (char**) & statusCopy, NR_L * sizeof( uint8_t ) );

    int iter = 0;
    float min_front = 0.f;

    while( true ) {

        int nChangedLast = -1;

        clearWasUpdated<<< gridSize( SC_THREADS_PER_BLOCK, NR_L ), SC_THREADS_PER_BLOCK >>>(
            NR_L,
            unresolvedIds,
            voxelSiteStatus_d );

        gpuErrchk( cudaDeviceSynchronize() );

        int innerIter = 0;
        while( true ) 
        {
            applyCopy<<< gridSize( SC_THREADS_PER_BLOCK, NR_L ), SC_THREADS_PER_BLOCK >>>(
                NR_L,
                unresolvedIds,
                sourceMap_d,  
                voxelSiteIds_d,      
                voxelSiteStatus_d,            
                sitePositions_d,
                data_d,
                distCopy,
                minNeighbors_d,
                siteIdCopy,
                srcIdCopy,
                statusCopy );

            gpuErrchk( cudaDeviceSynchronize() );

            classifyLineOfSite<<< gridSize( SC_THREADS_PER_BLOCK, NR_L ), SC_THREADS_PER_BLOCK >>>(
                NR_L,
                m_nSites,
                unresolvedIds,
                sourceMap_d,  
                voxelSiteIds_d,      
                voxelComponentIds_d,
                voxelSiteStatus_d,            
                sitePositions_d,
                data_d,
                distCopy,
                minNeighbors_d,
                siteIdCopy,
                srcIdCopy,
                statusCopy );

            gpuErrchk( cudaDeviceSynchronize() );

            applyUpdates<<< gridSize( SC_THREADS_PER_BLOCK, NR_L ), SC_THREADS_PER_BLOCK >>>(
                true,
                NR_L,
                iter,
                unresolvedIds,
                minNeighbors_d,

                srcIdCopy,
                siteIdCopy,
                statusCopy,
                distCopy,

                sourceMap_d,     
                voxelSiteIds_d,
                voxelSiteStatus_d,
                data_d );

            gpuErrchk( cudaDeviceSynchronize() );

            // Can try relaxing the condition and break earlier

            int64_t nChanged = thrust::count_if(
                thrust::make_counting_iterator<int64_t>( 0 ),
                thrust::make_counting_iterator<int64_t>( NR_L ),
                ThrustAccessorNotEqualsIndirect<int64_t, int8_t, int64_t>( 
                    thrust::device_pointer_cast<int64_t>( unresolvedIds ), 
                    thrust::device_pointer_cast<int8_t>(  minNeighbors_d ), 
                    VX_MIN_NEIGHBOR_NONE, NR_L, M_NV ) );

            gpuErrchk( cudaDeviceSynchronize() );

            if( nChanged == 0 ) { break; }
            nChangedLast = nChanged;

            if( innerIter > 100000 )
            {
                std::cout << "Exceded limit" << std::endl;
                exit(1);
            }

            ++innerIter;
        }

        resolve<<< gridSize( SC_THREADS_PER_BLOCK, NR_L ), SC_THREADS_PER_BLOCK >>>(
            NR_L,
            unresolvedIds,
            min_fronts_d,
            voxelSiteStatus_d,
            voxelComponentIds_d,
            data_d );

        gpuErrchk( cudaDeviceSynchronize() );

        thrust::fill( 
            thrust::device_pointer_cast<int32_t>( min_fronts_d ),
            thrust::device_pointer_cast<int32_t>( min_fronts_d ) + m_NC,
            std::numeric_limits<int32_t>::max() );

        gpuErrchk( cudaDeviceSynchronize() );

        updateFront<<< gridSize( SC_THREADS_PER_BLOCK, NR_L ), SC_THREADS_PER_BLOCK >>>(
            NR_L,
            unresolvedIds,
            min_fronts_d,
            voxelSiteStatus_d,
            voxelComponentIds_d,
            data_d );

        gpuErrchk( cudaDeviceSynchronize() );

        updateSrcSpace<<< gridSize( SC_THREADS_PER_BLOCK, NR_L ), SC_THREADS_PER_BLOCK >>>(
            NR_L,
            unresolvedIds,
            false,
            iter,
            voxelComponentIds_d,
            voxelSiteIds_d,
            voxelSiteStatus_d );

         // gpuErrchk( cudaPeekAtLastError() );
         gpuErrchk( cudaDeviceSynchronize() )

         ++iter;

        if( iter > 1000 || innerIter == 0 ) 
        {
           break;
        }

        auto partition_point = thrust::partition(
            thrust::device_ptr<int64_t>(unresolvedIds),
            thrust::device_ptr<int64_t>(unresolvedIds) + NR_L,
            check_bit_indirect(
                thrust::device_pointer_cast( voxelSiteStatus_d ), 
                VX_RESOLVED_FLAG, 
                false ) );

        NR_L = partition_point - thrust::device_ptr<int64_t>( unresolvedIds );

        if( NR_L == 0 )
        {
            break;
        }

        gpuErrchk( cudaDeviceSynchronize() );
    }

    cudaFree( min_fronts_d   );
    cudaFree( unresolvedIds  );
    cudaFree( srcIdCopy      );
    cudaFree( siteIdCopy     );
    cudaFree( distCopy       );
    cudaFree( statusCopy     );            
}
