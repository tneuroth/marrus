# Dynamic Hierachical Flow Feature Database (DHFFDBS)

## Files and Format

	dhffdbs.ib.<ts>.dat         : | uint64_t count | ibRecord_0, ibRecord_1, ..., ibRecord_{count-1} |
	dhffdbs.cc.<ts>.dat         : | uint64_t count | ccRecord_0, ccRecord_1, ..., ccRecord_{count-1} |
	sorted_index_space.<ts>.dat : | uint64_t count | i_0, i_1, ..., i_{ni-1}                       | 
 	
 	for each processed field
 		dhffdbs.cc.<ts>.dat     : | uint64_t count | v(i_0), v(i_1), ..., v(i_{count-1}) |

