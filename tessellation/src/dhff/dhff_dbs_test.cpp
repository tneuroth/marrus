
#include "dhff_dbs.hpp"
#include <utils/io.hpp>
#include "algorithms/Standard/MyAlgorithms.hpp"

#include <string>
#include <vector>
#include <algorithm>
#include <array>
#include <iostream>
#include <cstdint>
#include <unordered_set>
#include <chrono>

using namespace std;
using namespace std::chrono;

int main() {

    array< string,  3 > dires = {  
        "/home/ts/hd_mnt/combustion/turb_shear/10atm/float32_downsampled/",
        "/home/ts/hd_mnt/combustion/turb_shear/10atm_new_grid/float32_downsampled/",
        "/home/ts/hd_mnt/combustion/turb_shear/10atm_new_grid_2/float32_downsampled/"                
    };

    const string IN_DIR    = "/home/ts/hd_mnt/combustion/turb_shear/10atm_connected_components/";
	const string OUT_DIR   = "/home/ts/hd_mnt/combustion/turb_shear/dhffdbs_10atm/";

	std::vector< string > fields = { "temp", "OH" };
	std::vector< std::vector< float > > sortedFieldData( fields.size() );	
	std::vector< float > dataTemp;

	array< int64_t, 3 > yDims = { 320, 480, 640 };
    array< int64_t, 2 > xzDims = { 1728, 1280 };
    const double dt = 0.000002;

    array< pair< double, double >, 3 > firstAndLas = {
        pair< double, double >( { 0.0002,    0.00035  } ),
        pair< double, double >( { 0.000352,  0.000538 } ),
        pair< double, double >( { 0.00054,   0.0006   } )           
    };

    double loadTime = 0;
    double loadTimeField = 0;

    double writeTime = 0;
    double writeTimeField = 0;
    double writeTimeRecord = 0;

    double sortTime      = 0;
	double sortTimeField = 0;

    double recordGenTime = 0;

	size_t totalSize  = 0;
	size_t reduceSize = 0; 

	TN::Vec2< double > OHMinMax = { 999999999.0, -9999999999.0 };

    for( int stg = 0; stg < 3; ++stg ) {

    	double last    = firstAndLas[ stg ].second;
        double current = firstAndLas[ stg ].first;

        while( current <= last + 0.0000001 ) {

       		string ts = toStringWFMT( current );

            array< int64_t, 3 > dims = { xzDims[ 0 ], yDims[ stg ], xzDims[ 1 ] };
            const int64_t N = dims[ 0 ] * dims[ 1 ] * dims[ 2 ];

            totalSize += N;

            const string POSTFX = to_string( dims[ 0 ] ) + "." + to_string( dims[ 1 ] ) + "." + to_string( dims[ 2 ] ) + "." + ts + ".dat";

            const string C_PATH = IN_DIR + "componentMap." + POSTFX;
            const string L_PATH = IN_DIR + "componentMap." + POSTFX;

            vector< int16_t > isobands;
            vector< int32_t > components;

            // Loading 

		    cout << "loading decomposition" << endl;

            auto loadStart = high_resolution_clock::now();

            loadData( IN_DIR +     "layerMap." + to_string( dims[ 0 ] ) + "." + to_string( dims[ 1 ] ) + "." + to_string( dims[ 2 ] ) + "." + ts + ".dat",     isobands, 0, N );
            loadData( IN_DIR + "componentMap." + to_string( dims[ 0 ] ) + "." + to_string( dims[ 1 ] ) + "." + to_string( dims[ 2 ] ) + "." + ts + ".dat", components, 0, N );
            
            loadTime += duration_cast<seconds>( high_resolution_clock::now() - loadStart ).count();

            // Sorting index space /////////////////////////////////////////////////////////////////////

		    cout << "sorting" << endl;

            auto sortStart = high_resolution_clock::now();

			vector< uint64_t > indexSpace = dhffdbs::sortedIndexSpace( isobands, components, unordered_set<int16_t>( { 2, 3 } ) );
			const size_t NIDX = indexSpace.size();

			reduceSize += NIDX;

            sortTime += duration_cast<seconds>( high_resolution_clock::now() - sortStart ).count();

            // Generating DHFFDBS Records /////////////////////////////////////////////////////////////////////////////////////////

            dhffdbs::DHFFDBS_STEP records;
            vector< TN::Vec2< double > > isobandEdges = {
		        TN::Vec2< double >( { 7.0,   10.0  } ),
		        TN::Vec2< double >( { 10.0,  14.75 } ),
		        TN::Vec2< double >( { 14.75,  18.0 } )  
		    };

		    cout << "generating records" << endl;

		    auto recordGenStart = high_resolution_clock::now();

            dhffdbs::generateRecords( 
				isobandEdges, 	
				isobands,
				components,
				indexSpace,
				records );

			recordGenTime += duration_cast<seconds>( high_resolution_clock::now() - recordGenStart ).count();

			// Loading and sorting each field based on sorted index space /////////////////////////////////////////////////////////

		    cout << "processing field data" << endl;

			for( size_t fId = 0; fId < fields.size(); ++fId ) {

		   	 	cout << "loading " << fields[ fId ] << endl;

				auto loadStartField = high_resolution_clock::now();

            	loadData( dires[ stg ]
            		+ "turb_shear_no_flame." + ts + ".field.mpi." 
            		+ fields[ fId ] + "." 
            		+ to_string( dims[ 0 ] ) + "." + to_string( dims[ 1 ] ) + "." + to_string( dims[ 2 ] ) 
            		+ ".f32.dat",     
            		dataTemp, 0, N );

            	loadTimeField += duration_cast<seconds>( high_resolution_clock::now() - loadStartField ).count();

            	auto sortStartField = high_resolution_clock::now();
            	sortedFieldData[ fId ].resize( indexSpace.size() );

		    	cout << "sorting " << fields[ fId ] << endl;

            	#pragma omp parallel for
            	for( size_t idx = 0; idx < NIDX; ++idx ) {
            		sortedFieldData[ fId ][ idx ] = dataTemp[ indexSpace[ idx ] ]; 
            	}

            	sortTimeField += duration_cast<seconds>( high_resolution_clock::now() - sortStartField ).count();

            	if( fields[ fId ] == "OH" ) {
            		cout << "getting OH range " << fields[ fId ] << endl;
            		auto rng = TN::Sequential::getRange( sortedFieldData[ fId ] );
            		OHMinMax.a( min( rng.a(), OHMinMax.a() ) );
            		OHMinMax.b( max( rng.b(), OHMinMax.b() ) );            		
            	}
			}

            // Writing //////////////////////////////////////////////////////////////////////////////////////////////////////////////

		    cout << "writing " << endl;

            auto writeStart = high_resolution_clock::now();

			writeDataWithSizeHeader( OUT_DIR + "sorted_index_space." + ts + ".dat", indexSpace.data(), indexSpace.size() );

			writeTime += duration_cast<seconds>( high_resolution_clock::now() - writeStart ).count();
			
            auto writeStartField = high_resolution_clock::now();

			for( size_t fId = 0; fId < fields.size(); ++fId ) { 
				writeDataWithSizeHeader( OUT_DIR + fields[ fId ] + "." + ts + ".dat", sortedFieldData[ fId ].data(), NIDX );
			}

            writeTimeField += duration_cast<seconds>( high_resolution_clock::now() - writeStartField ).count();

            auto writeRecordStart = high_resolution_clock::now();

			writeDataWithSizeHeader( OUT_DIR + "dhffdbs.ib." + ts + ".dat", records.ibRecords.data(), records.ibRecords.size() );
			writeDataWithSizeHeader( OUT_DIR + "dhffdbs.cc." + ts + ".dat", records.ccRecords.data(), records.ccRecords.size() );

			writeTimeRecord += duration_cast<seconds>( high_resolution_clock::now() - writeRecordStart ).count();

            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

			cout << ts << " done, " << " size=" << indexSpace.size() << "/" << N << "=" << (double)  indexSpace.size() / N << endl;
            current += dt;
        }
    }

	cout << "\n" << "load "       <<      loadTime << " seconds" << endl;
	cout << "\n" << "load field " << loadTimeField << " seconds" << endl;

	cout << "\n" << "sort "       << sortTime << " seconds" << endl;
	cout << "\n" << "sort fields" << sortTimeField << " seconds" << endl;

	cout << "\n" << "write "        << writeTime << " seconds" << endl;
	cout << "\n" << "write fields"  << writeTimeField << " seconds" << endl;
	cout << "\n" << "write records" << writeTimeRecord << " seconds" << endl;

	cout << "net reduction " << reduceSize << "/" << totalSize << "=" << (double) reduceSize / totalSize << endl << endl;

	cout << "OHMinMax=" << OHMinMax.a() << " " << OHMinMax.b() << endl;
}