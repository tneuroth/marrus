#ifndef DHFF_DBS_HPP 
#define DHFF_DBS_HPP

#include "geometry/Vec.hpp"

#include <cstdint>
#include <vector>
#include <fstream>
#include <algorithm>
#include <unordered_set>
#include <iostream>

namespace TN
{

class DHFFDBS_Module 
{

};

}

namespace dhffdbs { 

using namespace std;

struct 
IBRecord {
	double   a,b;
	uint64_t c0;
	uint64_t i0;	
	IBRecord( 
		double   _a, 
		double   _b,
		uint64_t _c0,  
		uint64_t _i0 ) : 
			a(_a), 
			b(_b), 
			c0(_c0),
			i0(_i0)
	{}
	IBRecord() {}
};

struct CCRecord {
	uint64_t i0;	
	CCRecord(
		uint64_t _i0 ) : 
			i0(_i0)
	{}
	CCRecord() {}
};

struct VRRecord {	
	uint64_t i0;
};

struct DHFFDBS_STEP {
	std::vector< IBRecord >  ibRecords; 
	std::vector< CCRecord >  ccRecords; 	
	std::vector< VRRecord >  vrRecords; 		
};

/***********************************************************************

	Generate

************************************************************************/

void generateRecords( 
	const vector< TN::Vec2< double > > & isobandEdges, 	
	const vector< int16_t >  & isobandIds,
	const vector< int32_t >  & componentIds,
	const vector< uint64_t > & sortedIndexSpace,
	DHFFDBS_STEP & records )
{
	const size_t N = sortedIndexSpace.size();
	
	if( N > 0 ) {
		
		int16_t bId = isobandIds[   sortedIndexSpace[ 0 ] ]; 
		int32_t cId = componentIds[ sortedIndexSpace[ 0 ] ];

		uint64_t b_i0 = 0;
		uint64_t b_c0 = 0;
		uint64_t c_i0 = 0;	
		uint64_t cIdx = 0;

		for( size_t i = 0; i < N; ++i ) {

			const size_t dIDX = sortedIndexSpace[ i ];

			if( cId != componentIds[ dIDX ] ) {
				records.ccRecords.push_back( { c_i0 } );
				cId = componentIds[ dIDX ];
				c_i0 = i;
				++cIdx;
			}

			if( bId != isobandIds[ dIDX ] ) {
				
				records.ibRecords.push_back( {				
					isobandEdges[ bId - 1 ].a(), // 0 = null band 
					isobandEdges[ bId - 1 ].b(),
					b_c0,
					b_i0 } );
				bId = isobandIds[ dIDX ];
				b_c0 = cIdx;
				b_i0 = i;
			}

			// last record
			if( i == N-1 ) {
				const double a = isobandEdges[ bId - 1 ].a(); // 0 = null band which is discarded
				const double b = isobandEdges[ bId - 1 ].a();
				records.ccRecords.push_back( { c_i0 } );
				records.ibRecords.push_back( { a, b, b_c0, b_i0 } );
			}
		}
	}
}

/***********************************************************************

	Input Functions 

************************************************************************/

// template< class T >
// void loadIsobandDataBlocks( 
// 	const uint64_t                         B_IDX, 
// 	const vector< uint64_t >             & blockIds, 	
// 	const uint64_t                         ELEM_PER_FIELD,
// 	ifstream                             & inputFile, 
// 	const vector< Isoband >              & bands,
// 	const vector< ConnectedComponent >            & components,	
// 	const uint64_t                         N_BLOCKS,
// 	const vector< ConnectedComponentBlock >       & componentBlocks,
// 	const vector< Region >               & regions,
// 	vector< T >                          & result ) 
// {
// 	const uint64_t CS = bands[ B_IDX ].cs;
// 	const uint64_t CE = bands[ B_IDX ].ce;

// 	uint64_t nReadTotal = 0;
// 	for( uint64_t c = CS; c < CE; ++c ) {
// 		for( uint64_t i = 0, end = blockIds.size(); i < end; ++i ) {
// 			const uint64_t BLOCK_ID = blockIds[ i ];
// 			const ConnectedComponentBlock BLOCK_OFFSET = componentBlocks[ c*N_BLOCKS + BLOCK_ID ];
// 			if( BLOCK_OFFSET.rs == BLOCK_OFFSET_NONE ) { continue; }
// 			const uint64_t START  = regions[ BLOCK_OFFSET.rs ].is;
// 			const uint64_t END    = regions[ BLOCK_OFFSET.re ].ie;	
// 			nReadTotal += END - START;
// 		}
// 	}

// 	result.resize( nReadTotal );
// 	uint64_t resultOffset = 0;
	
// 	for( uint64_t c = CS; c < CE; ++c ) {
// 		for( uint64_t i = 0, end = blockIds.size(); i < end; ++i ) {
// 			const uint64_t BLOCK_ID = blockIds[ i ];
// 			const ConnectedComponentBlock BLOCK_OFFSET = componentBlocks[ c*N_BLOCKS + BLOCK_ID ];
// 			if( BLOCK_OFFSET.rs == BLOCK_OFFSET_NONE ) { continue; }
// 			const uint64_t START  = regions[ BLOCK_OFFSET.rs + BLOCK_OFFSET.rs ].is;
// 			const uint64_t END    = regions[ BLOCK_OFFSET.re ].ie;	
// 			const uint64_t N_READ = END - START;
// 			inputFile.seekg( START*sizeof(T) );
// 			inputFile.read( static_cast<char*>( result.data() + resultOffset ), N_READ*sizeof(T) );
// 			resultOffset += N_READ;
// 		}
// 	}
// }

// template< class T >
// void loadIsobandData( 
// 	const uint64_t              B_IDX, 
// 	ifstream                  & inputFile, 
// 	const vector< Isoband >   & bands,
// 	vector< T >               & result ) 
// {
// 	const uint64_t START  = bands[ B_IDX ].is;
// 	const uint64_t END    = bands[ B_IDX ].ie;		
// 	const uint64_t N_READ = END - START;	
// 	result.resize( N_READ );
// 	inputFile.seekg( START*sizeof(T) );
// 	inputFile.read( static_cast<char*>( result.data() ), N_READ*sizeof(T) );
// }

// template< class T >
// void loadIsobandDataUnsorted( 
// 	const uint64_t              B_IDX, 
// 	const uint64_t              FIELD_IDX,
// 	ifstream                  & inputFile, 
// 	const vector< uint64_t >  & permutation,
// 	const vector< Isoband >   & bands,
// 	const vector< ConnectedComponent > & components,	
// 	const vector< Region >    & regions,
// 	vector< T >               & result ) 
// {
// 	const uint64_t START  = regions[ components[ bands[ B_IDX ].cs ].rs ].is;
// 	const uint64_t END    = regions[ components[ bands[ B_IDX ].ce ].re ].ie;	
// 	const uint64_t N_READ = END - START;
// 	result.resize( N_READ );
// 	for( uint64_t i = START; i < END; ++i ) {
// 		inputFile.seekg( permutation[ i ]*sizeof(T) );
// 		inputFile.read( static_cast<char*>( result.data() + i ), sizeof(T) );
// 	}
// }

// template< class T >
// void loadConnectedComponentData( 
// 	const uint64_t C_IDX, 
// 	ifstream                  & inputFile, 
// 	const vector< ConnectedComponent > & components,	
// 	const vector< Region >    & regions,
// 	vector< T >               & result )
// {
// 	const uint64_t START  = components[ C_IDX ].is;
// 	const uint64_t END    = components[ C_IDX ].ie;
// 	const uint64_t N_READ = END - START;			
// 	result.resize( N_READ );
// 	inputFile.seekg( START*sizeof(T) );
// 	inputFile.read( static_cast<char*>( result.data() ), N_READ*sizeof(T) );
// }

// template< class T >
// void loadRegionData( 
//  	const uint64_t R_IDX, 
// 	ifstream                  & inputFile, 
// 	const vector< Region >    & regions,
// 	vector< T >               & result )
// {
// 	const uint64_t START  = regions[ R_IDX ].is;
// 	const uint64_t END    = regions[ R_IDX ].ie;
// 	const uint64_t N_READ = END - START;		
// 	result.resize( N_READ );
// 	inputFile.seekg( START*sizeof(T) );
// 	inputFile.read( static_cast<char*>( result.data() ), N_READ*sizeof(T) );
// }

/***************************************************************************
 
	Summary Records	

***************************************************************************/

// template< class T >
// void loadIsobandSummaryRecord( 
// 	const uint64_t              B_IDX, 
// 	ifstream                  & inputFile, 
// 	const vector< Isoband >   & bands,
// 	vector< T >               & result ) 
// {
// 	const uint64_t OFFSET = bands[ B_IDX ].rs;
// 	const uint64_t END    = bands[ B_IDX ].re;	
// 	const uint64_t N_READ = END - START;
// 	result.resize( N_READ );
// 	inputFile.seekg( START*sizeof(T) );
// 	inputFile.read( static_cast<char*>( result.data() ), N_READ*sizeof(T) );
// }

// template< class T >
// void loadConnectedComponentSummaryRecord( 
// 	const uint64_t              C_IDX, 
// 	ifstream                  & inputFile, 
// 	const vector< Isoband >   & bands,
// 	const vector< ConnectedComponent > & components,	
// 	const vector< Region >    & regions,
// 	vector< T >               & result ) 
// {
// 	const uint64_t START  = components[ C_IDX ].rs;
// 	const uint64_t END    = components[ C_IDX ].re;
// 	const uint64_t N_READ = END - START;	
// 	result.resize( N_READ );
// 	inputFile.seekg( START*sizeof(T) );
// 	inputFile.read( static_cast<char*>( result.data() ), N_READ*sizeof(T) );
// }

// template< class T >
// void loadRegionSummaryData( 
// 	const uint64_t              R_IDX, 
// 	ifstream                  & inputFile, 
// 	const vector< Isoband >   & bands,
// 	const vector< ConnectedComponent > & components,	
// 	const vector< Region >    & regions,
// 	vector< T >               & result ) 
// {
// 	const uint64_t START  = R_IDX;
// 	const uint64_t END    = R_IDX+1;
// 	const uint64_t N_READ = END - START;	
// 	result.resize( N_READ );
// 	inputFile.seekg( START*sizeof(T) );
// 	inputFile.read( static_cast<char*>( result.data() ), N_READ*sizeof(T) );
// }


/***********************************************************************

	Preprocessing and Output Functions 

************************************************************************/

vector< uint64_t > sortedIndexSpace(
	const vector< int16_t > & bandIds,
	const vector< int32_t > & compIds,
	const unordered_set<  int16_t > & subselection  )
{
	vector< uint64_t > indexSpace;
	indexSpace.reserve( bandIds.size() );

	for( uint64_t i = 0, end = bandIds.size(); i < end; ++i ) {
		if( subselection.count( bandIds[ i ] ) ) {
			indexSpace.push_back( i );
		}
	}

	sort( 
		indexSpace.begin(), 
		indexSpace.end(),  
		[ & ] ( const uint64_t & a, const uint64_t & b ) { 
			return bandIds[ a ] != bandIds[ b ] ? bandIds[ a ] < bandIds[ b ] 
			 	 : compIds[ a ] != compIds[ b ] ? compIds[ a ] < compIds[ b ] 
			 	 : a < b;
		}
	);

	return move( indexSpace );
}

vector< uint64_t > sortedIndexSpace(
	const vector< int16_t > & bandIds,
	const vector< int32_t > & compIds )
{
	vector< uint64_t > indexSpace;
	indexSpace.reserve( bandIds.size() );

	for( uint64_t i = 0, end = bandIds.size(); i < end; ++i ) {
		if( bandIds[ i ] > 0 ) {
			indexSpace.push_back( i );
		}
	}

	sort( 
		indexSpace.begin(), 
		indexSpace.end(),  
		[ & ] ( const uint64_t & a, const uint64_t & b ) { 
			return bandIds[ a ] != bandIds[ b ] ? bandIds[ a ] < bandIds[ b ] 
			 	 : compIds[ a ] != compIds[ b ] ? compIds[ a ] < compIds[ b ] 
			 	 : a < b;
		} 
	);

	return move( indexSpace );
}

// vector< uint64_t > sortedIndexSpace(  
// 	const vector< int16_t > & bandIds,
// 	const vector< int32_t > & compIds,	
// 	const vector< int32_t > & vregIds )
// {
// 	vector< uint64_t > indexSpace;
// 	indexSpace.reserve( bandIds.size() );

// 	for( uint64_t i = 0, end = bandIds.size(); i < end; ++i ) {
// 		if( bandIds[ i ] > 0 ) {
// 			indexSpace.push_back( i );
// 		}
// 	}

// 	sort( 
// 		indexSpace.begin(), 
// 		indexSpace.end(),  
// 		[ bandIds, compIds, vregIds ] ( uint64_t a, uint64_t b ) { 
// 			return bandIds[ a ] != bandIds[ b ] ? bandIds[ a ] < bandIds[ b ] 
// 			 	 : compIds[ a ] != compIds[ b ] ? compIds[ a ] < compIds[ b ] 
// 			 	 : vregIds[ a ] != vregIds[ b ] ? vregIds[ a ] < vregIds[ b ] 
// 			 	 : a < b;
// 		}
// 	);

// 	return move( indexSpace );
// }

} // end anonymous namespace

#endif
