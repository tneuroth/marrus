
import numpy as np
import subprocess
import os

variables = [ "H2O", "H2O2", "OH", "temp", "pressure", "velx", "vely", "velz" ]
fdct3d_path = "/home/ubuntu/dev/curveletTF-main/CurveLab-2.1.3/fdct3d/src/custom"

# Since we are relying on padding, which increases dimain by a factor of 2 in each dimension, 
# to avoid border artifacts, we will only generate multiscale data for 256^3 and 128^3
# data blocks, for now. 
#
# Investigating ways to scale up to larger data may include computing the data in blocks and 
# then merging them, or running on a machine with more memory.

f = open( 'options_template', 'r')
ops_template = f.read()
f.close()

cwd = os.getcwd()

for variable in variables :	

	print( "\nProcessing " + variable )

	for d in [ 128 ]:

		D = ( d, d, d )

		print( "\nData size: " + str( D ) + "\n" )

		data = np.fromfile( str( 512 ) + "/data/" + variable + ".bin", dtype='f4' ).reshape( ( 512, 512, 512 ) )
		data = data[ -(d+1):, -(d+1):, -(d+1): ]

		# pad the data ###############################################

		if d < 512 :

			reflected = np.pad( data,
				( ( D[0]-1, 0 ), 
				  ( D[1]-1, 0 ), 
				  ( D[2]-1, 0 ) ), 
				mode='reflect' )
			
			# width of padded data
			NP = 2*D[0]

		else : 
			reflected = data
			NP = D[0]
			
		allScales = reflected[ d:2*d, d:2*d, d:2*d ]

		# write the original data
		allScales.tofile(
			str( d ) + "/data/" 
			+ variable 
			+ ".bin" );

		# write the padded data to file so fdct3d can access it ######
		reflected.tofile( "in.bin" )

		# number of scales
		nscales = int( np.floor( np.log2( NP // 2 ) ) )

		# logic is that bin nscales-1 always represents the same frequency with the same grid spacing 
		# no matter what nscales is (which depends on the grid size). So we are decomposing the same
		# way for 512, 256, 128 grid sizes

		# keep ns-1, ns-2, ns-3 (  0.076 fs to 0.67  fs )
		# then ns-4, ns-5       (  0.67  fs to 2.426 fs )
		# then ns-6  down to 0  (  2.426 fs to dc       )

		scale_list = [
			[ nscales - k for k in range( 1, 4         ) ],
			[ nscales - k for k in range( 4, 6         ) ],
			[ nscales - k for k in range( 6, nscales+1 ) ]
		]

		# for each way we will filer the data based on scales we want to keep
		for scales in scale_list :

			print( "Keeping scales: " + str( scales ) )

			# set the dimensions parameters
			ops = ops_template.replace( "$1$", str( NP ) )

			# set the input file parameter
			ops = ops.replace( "$2$", cwd + "/in.bin" )

			# set the output directory parameter
			ops = ops.replace( "$3$", cwd )

			# set the scales parameter
			ops = ops.replace( "$4$", ",".join( [ str( scale ) for scale in scales ] ) )

			# write the options file so fdct3d can read it
			f = open( 'options_cstm', 'w')
			f.write( ops )
			f.close()

			# run the fdct3s program
			command = [ fdct3d_path ]
			print( subprocess.check_output( command ) )

			# load the output of fdct3d and remove the padding
			result = np.fromfile( "out.bin", dtype='f4' ).reshape( ( NP, NP, NP ) )

			if D[0] < 512 :
				result = result[ D[0]:2*D[0], D[1]:2*D[1], D[2]:2*D[2] ]

			# write the result to file
			result.tofile(
				str( D[0] ) + "/data/" 
				+ variable  + "_"
				+ ".".join( sorted( [ str( scale ) for scale in scales ] ) )
				+ ".bin" );

		# remove the temporary data
		subprocess.check_output( [ "rm", "in.bin"       ] )
		subprocess.check_output( [ "rm", "out.bin"      ] )
		subprocess.check_output( [ "rm", "options_cstm" ] )


