=======================================================================================================

Cuvelet-Based Scale Decomposition

========================================================================================================

Data: ammonia 
Time Step: 6.0000E-04
Dimensions : 256^3
Type : 32-bit float (dtype='f4')
file format: <variable>_<scale bin>.bin
padding method: "reflect"

========================================================================================================

Length Scales

In flame scale units (fs) with 1fs=188e-6:

scale 0 [ 19.409 , 9.705  ] (fs)
scale 1 [ 9.705 , 4.852  ]  (fs)
scale 2 [ 4.852 , 2.426  ]  (fs)
scale 3 [ 2.426 , 1.213  ]  (fs)
scale 4 [ 1.213 , 0.607  ]  (fs)
scale 5 [ 0.607 , 0.303  ]  (fs)
scale 6 [ 0.303 , 0.152  ]  (fs)
scale 7 [ 0.152 , 0.076  ]  (fs)

In meters:

scale 0 [ 0.003649 , 0.001824  ] (m)
scale 1 [ 0.001824 , 0.000912  ] (m)
scale 2 [ 0.000912 , 0.000456  ] (m)
scale 3 [ 0.000456 , 0.000228  ] (m)
scale 4 [ 0.000228 , 0.000114  ] (m)
scale 5 [ 0.000114 , 0.000057  ] (m)
scale 6 [ 0.000057 , 0.000029  ] (m)
scale 7 [ 0.000029 , 0.000014  ] (m)

========================================================================================================

Padding Code:

# extract 258^3 block (2 extra because of the nature of 'reflect' padding not repeating borders)

d=256
data = np.fromfile( "512/data/" + variable + ".bin", dtype='f4' ).reshape( ( 512, 512, 512 ) )
data = data[ 0:d+2, 0:d+2, 0:d+2 ]

# pad by d-2, to not duplicate the borders in tiling 
# ( [ 0, 1, 2, 3 ] -> [ 2, 1, 0, 1, 2, 3 ] )

reflected = np.pad( data,
	( ( d - 2, 0 ), 
	  ( d - 2, 0 ), 
	  ( d - 2, 0 ) ), 
	mode='reflect' )

... 

# extract the non-padded part of the result
result = result[ d:2*d, d:2*d, d:2*d ]

=========================================================================================================
