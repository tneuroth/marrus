#!/bin/bash

echo -e "\nComputing Tessellation for N=128\n"
cd 128/tessellation/
./run.sh

echo -e "\nComputing Tessellation for N=256\n"
cd ../../256/tessellation/
./run.sh

echo -e "\nComputing Tessellation for N=256, scale by scale\n"
cd ../../256_scale_by_scale/tessellation/
./run.sh

echo -e "\nComputing Tessellation for N=512, scale by scale\n"
cd ../../512/tessellation/
./run.sh
