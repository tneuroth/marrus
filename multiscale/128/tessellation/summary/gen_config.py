
import sys
import json
import copy

n = len(sys.argv)

if n != 2:
	print( "requires command line argument: <tessellation directory>" )
	exit()

tessellation_directory = sys.argv[ 1 ]

#################################################################

# specify the variables we are working with and how to load them

data_path = "../data/"

variables = {}

##############################################

variables[ "H2O" ] = {
	"file"   : data_path + "/H2O.bin",
	"offset" : 0,
	"type"   : "float"
}

variables[ "H2O_0.1" ] = {
	"file"   : data_path + "/H2O_0.1.bin",
	"offset" : 0,
	"type"   : "float"
}

variables[ "H2O_2.3" ] = {
	"file"   : data_path + "/H2O_2.3.bin",
	"offset" : 0,
	"type"   : "float"
}

variables[ "H2O_4.5.6" ] = {
	"file"   : data_path + "/H2O_4.5.6.bin",
	"offset" : 0,
	"type"   : "float"
}

###############################################


variables[ "H2O2" ] = {
	"file"   : data_path + "/H2O2.bin",
	"offset" : 0,
	"type"   : "float"
}

variables[ "H2O2_0.1" ] = {
	"file"   : data_path + "/H2O2_0.1.bin",
	"offset" : 0,
	"type"   : "float"
}

variables[ "H2O2_2.3" ] = {
	"file"   : data_path + "/H2O2_2.3.bin",
	"offset" : 0,
	"type"   : "float"
}

variables[ "H2O2_4.5.6" ] = {
	"file"   : data_path + "/H2O2_4.5.6.bin",
	"offset" : 0,
	"type"   : "float"
}

###############################################

variables[ "OH" ] = {
	"file"   : data_path + "/OH.bin",
	"offset" : 0,
	"type"   : "float"
}

variables[ "OH_0.1" ] = {
	"file"   : data_path + "/OH_0.1.bin",
	"offset" : 0,
	"type"   : "float"
}

variables[ "OH_2.3" ] = {
	"file"   : data_path + "/OH_2.3.bin",
	"offset" : 0,
	"type"   : "float"
}

variables[ "OH_4.5.6" ] = {
	"file"   : data_path + "/OH_4.5.6.bin",
	"offset" : 0,
	"type"   : "float"
}

###############################################

variables[ "pressure" ] = {
	"file"   : data_path + "/pressure.bin",
	"offset" : 0,
	"type"   : "float"
}

variables[ "pressure_0.1" ] = {
	"file"   : data_path + "/pressure_0.1.bin",
	"offset" : 0,
	"type"   : "float"
}

variables[ "pressure_2.3" ] = {
	"file"   : data_path + "/pressure_2.3.bin",
	"offset" : 0,
	"type"   : "float"
}

variables[ "pressure_4.5.6" ] = {
	"file"   : data_path + "/pressure_4.5.6.bin",
	"offset" : 0,
	"type"   : "float"
}

###############################################

variables[ "temp" ] = {
	"file"   : data_path + "/temp.bin",
	"offset" : 0,
	"type"   : "float"
}

variables[ "temp_0.1" ] = {
	"file"   : data_path + "/temp_0.1.bin",
	"offset" : 0,
	"type"   : "float"
}

variables[ "temp_2.3" ] = {
	"file"   : data_path + "/temp_2.3.bin",
	"offset" : 0,
	"type"   : "float"
}

variables[ "temp_4.5.6" ] = {
	"file"   : data_path + "/temp_4.5.6.bin",
	"offset" : 0,
	"type"   : "float"
}

###############################################


variables[ "velx" ] = {
	"file"   : data_path + "/velx.bin",
	"offset" : 0,
	"type"   : "float"
}

variables[ "velx_0.1" ] = {
	"file"   : data_path + "/velx_0.1.bin",
	"offset" : 0,
	"type"   : "float"
}

variables[ "velx_2.3" ] = {
	"file"   : data_path + "/velx_2.3.bin",
	"offset" : 0,
	"type"   : "float"
}

variables[ "velx_4.5.6" ] = {
	"file"   : data_path + "/velx_4.5.6.bin",
	"offset" : 0,
	"type"   : "float"
}

###############################################

variables[ "vely" ] = {
	"file"   : data_path + "/vely.bin",
	"offset" : 0,
	"type"   : "float"
}

variables[ "vely_0.1" ] = {
	"file"   : data_path + "/vely_0.1.bin",
	"offset" : 0,
	"type"   : "float"
}

variables[ "vely_2.3" ] = {
	"file"   : data_path + "/vely_2.3.bin",
	"offset" : 0,
	"type"   : "float"
}

variables[ "vely_4.5.6" ] = {
	"file"   : data_path + "/vely_4.5.6.bin",
	"offset" : 0,
	"type"   : "float"
}

###############################################

variables[ "velz" ] = {
	"file"   : data_path + "/velz.bin",
	"offset" : 0,
	"type"   : "float"
}

variables[ "velz_0.1" ] = {
	"file"   : data_path + "/velz_0.1.bin",
	"offset" : 0,
	"type"   : "float"
}

variables[ "velz_2.3" ] = {
	"file"   : data_path + "/velz_2.3.bin",
	"offset" : 0,
	"type"   : "float"
}

variables[ "velz_4.5.6" ] = {
	"file"   : data_path + "/velz_4.5.6.bin",
	"offset" : 0,
	"type"   : "float"
}

###############################################

# Specify our summarization

configuration = {
	"bands"      : {},
	"components" : {},
	"cells"      : {}
}


# group_summary = {
# 	"histograms"               : [],
# 	"moments"                  : [],
# 	"co-moments"               : [],
# 	"abs_sums"                 : [],
# 	"sums"                     : [],
# 	"squared_sums"             : [],
# 	"spectral_energy"          : []
# }


group_summary = {
	"histograms"               : [],
	"moments"                  : [],
	"co-moments"               : [],
	"spectral_energy"          : []
}

######################################################################

# statistical moments and sums

group_summary[ "moments" ].append( "OH"       )
group_summary[ "moments" ].append( "H2O"      )
group_summary[ "moments" ].append( "H2O2"     )
group_summary[ "moments" ].append( "pressure" )
group_summary[ "moments" ].append( "temp"     )
group_summary[ "moments" ].append( "velx"     )
group_summary[ "moments" ].append( "vely"     )
group_summary[ "moments" ].append( "velz"     )

group_summary[ "moments" ].append( "velx_0.1" )
group_summary[ "moments" ].append( "vely_0.1" )
group_summary[ "moments" ].append( "velz_0.1" )

group_summary[ "moments" ].append( "velx_2.3" )
group_summary[ "moments" ].append( "vely_2.3" )
group_summary[ "moments" ].append( "velz_2.3" )

group_summary[ "moments" ].append( "velx_4.5.6" )
group_summary[ "moments" ].append( "vely_4.5.6" )
group_summary[ "moments" ].append( "velz_4.5.6" )

group_summary[ "spectral_energy" ].append( {
	"variable"   : "H2O",
	"scale_decomposition" : [ "0.1", "2.3", "4.5.6" ]
} )

group_summary[ "spectral_energy" ].append( {
	"variable"  : "H2O2",
	"scale_decomposition" : [ "0.1", "2.3", "4.5.6" ]
} )

group_summary[ "spectral_energy" ].append( {
	"variable"  : "OH",
	"scale_decomposition" : [ "0.1", "2.3", "4.5.6" ]
} )

group_summary[ "spectral_energy" ].append( {
	"variable"  : "pressure",
	"scale_decomposition" : [ "0.1", "2.3", "4.5.6" ]
} )

group_summary[ "spectral_energy" ].append( {
	"variable"  : "temp",
	"scale_decomposition" : [ "0.1", "2.3", "4.5.6" ]
} )

group_summary[ "spectral_energy" ].append( {
	"variable"  : "velx",
	"scale_decomposition" : [ "0.1", "2.3", "4.5.6" ]
} )

group_summary[ "spectral_energy" ].append( {
	"variable"  : "vely",
	"scale_decomposition" : [ "0.1", "2.3", "4.5.6" ]
} )

group_summary[ "spectral_energy" ].append( {
	"variable"  : "velz",
	"scale_decomposition" : [ "0.1", "2.3", "4.5.6" ]
} )

#######################################################################
# histograms 

group_summary[ "histograms" ].append( {
	"variables" : [ "temp", "OH" ],
	"dims"      : [ 17, 17 ],
	"edges"     : "bands"
} )

group_summary[ "histograms" ].append( {
	"variables" : [ "temp" ],
	"dims"      : [ 3 ],
	"edges"     : "bands"
} )

group_summary[ "histograms" ].append( {
	"variables" : [ "OH" ],
	"dims"      : [ 3 ],
	"edges"     : "bands"
} )

#######################################################################

# co-moments

group_summary[ "co-moments" ].append( [ "temp", "OH" ] )

#######################################################################

configuration[ "bands"      ] = copy.deepcopy( group_summary )
configuration[ "components" ] = copy.deepcopy( group_summary )
configuration[ "cells"      ] = copy.deepcopy( group_summary )

#######################################################################

# compute summary

with open( tessellation_directory + "/summary/configuration.json", 'w') as f:
    json.dump( configuration, f, ensure_ascii=True, indent=4, sort_keys=False )

with open( tessellation_directory + "/summary/variables.json", 'w') as f:
    json.dump( variables, f, ensure_ascii=True, indent=4, sort_keys=False )

