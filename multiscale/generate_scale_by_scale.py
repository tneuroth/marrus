
import numpy as np
import subprocess
import os

variables = [ "H2O", "H2O2", "OH", "temp", "pressure", "velx", "vely", "velz" ]
fdct3d_path = "/home/ubuntu/dev/curveletTF-main/CurveLab-2.1.3/fdct3d/src/custom"

f = open( 'options_template', 'r')
ops_template = f.read()
f.close()

cwd = os.getcwd()

dst = np.fromfile( "./512/data/flame_distance.bin", dtype='f4' ).reshape( ( 512, 512, 512 ) )
dst = dst[ 256:512, 256:512, 256:512 ]
dst.tofile( "256_scale_by_scale/data/flame_distance.bin" )

for variable in variables :

	print( "\nProcessing " + variable )

	for d in [ 256 ]:

		D = ( d, d, d )

		# width of padded data
		NP = 2*d

		print( "\nData size: " + str( D ) + "\n" )

		# assumes you have a folder <2d>/data/ with the data in it named variable.bin
		data = np.fromfile( str( 2*d ) + "/data/" + variable + ".bin", dtype='f4' ).reshape( ( NP, NP, NP ) )

		# extracting 2 extra grid points because we don't repeat the borders with reflective padding
		# so to get 2^n in the padded data, we need 2^(n-1)-2 grid size before padding
		data = data[ -(d+1):, -(d+1):, -(d+1): ]

		reflected = np.pad( data,
			( ( d - 1, 0 ), 
			  ( d - 1, 0 ), 
			  ( d - 1, 0 ) ), 
			mode='re1lect' )

		allScales = reflected[ d:2*d, d:2*d, d:2*d ]

		# write the original data
		allScales.tofile(
			str( d ) + "_scale_by_scale/data/" 
			+ variable 
			+ ".bin" );

		# write the padded data to file so fdct3d can access it ######
		reflected.tofile( "in.bin" )

		# number of scales
		nscales = int( np.floor( np.log2( NP // 2 ) ) )

		# each scale bin separately
		scale_list = [ [ nscales - n ] for n in range( 1, nscales+1 ) ]

		# for each way we will filter the data based on scales we want to keep
		for scales in scale_list :

			print( "Keeping scales: " + str( scales ) )

			# set the dimensions parameters
			ops = ops_template.replace( "$1$", str( NP ) )

			# set the input file parameter
			ops = ops.replace( "$2$", cwd + "/in.bin" )

			# set the output directory parameter
			ops = ops.replace( "$3$", cwd )

			# set the scales parameter
			ops = ops.replace( "$4$", ",".join( [ str( scale ) for scale in scales ] ) )

			# write the options file so fdct3d can read it
			f = open( 'options_cstm', 'w')
			f.write( ops )
			f.close()

			# run the fdct3d program
			command = [ fdct3d_path ]
			print( subprocess.check_output( command ) )

			# load the output of fdct3d and remove the padding
			result = np.fromfile( "out.bin", dtype='f4' ).reshape( ( NP, NP, NP ) ) 
			result = result[ d:2*d, d:2*d, d:2*d ]

			# write the result to file
			result.tofile(
				str( d ) + "_scale_by_scale/data/"
				+ variable  + "_"
				+ ".".join( sorted( [ str( scale ) for scale in scales ] ) )
				+ ".bin" );

		# remove the temporary data
		subprocess.check_output( [ "rm", "in.bin"       ] )
		subprocess.check_output( [ "rm", "out.bin"      ] )
		subprocess.check_output( [ "rm", "options_cstm" ] )


