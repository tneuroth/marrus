#!/bin/bash

# gets the path that this script is in
customrealpath() {
        [[ $1 = /* ]] && echo "$1" || echo "$PWD/${1#./}"
}

SCRIPT=`customrealpath $0`
THIS_PATH=`dirname $SCRIPT`

LSRCVT_PATH=$THIS_PATH/../../../tessellation/
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$LSRCVT_PATH/lib/

# $LSRCVT_PATH/bin/lsrcvt $THIS_PATH/configuration.json
OUT_PATH=$(dirname $THIS_PATH/$(cat configuration.json | grep "outputDirectory" | cut -d':' -f2 | cut -d'"' -f2))

if [ $THIS_PATH != $OUT_PATH ]; then
    cp $THIS_PATH/configuration.json $OUT_PATH   
fi

echo
echo "Wrote tessellation to: " $OUT_PATH
echo

echo "Computing curvature and normals at each site point"
echo

DATA_FILE=$THIS_PATH/$(grep -oP '"dataFile"\s*:\s*\K[^,]+' configuration.json | tr -d ' "')
DATA_OFFSET=$(grep -oP '"dataOffset"\s*:\s*\K[^,]+' configuration.json )
DATA_TYPE=$(grep -oP '"dataFormat"\s*:\s*\K[^,]+' configuration.json | tr -d ' "')
DIMS=$(grep -oP '"dims"\s*:\s*\K[^]]+' configuration.json | tr -d '[,' )
read -ra DIMS <<< $DIMS

python3 ../../../python/estimate_principle_directions.py $THIS_PATH $DATA_FILE $DATA_OFFSET $DATA_TYPE ${DIMS[0]} ${DIMS[1]} ${DIMS[2]}

echo "Computing statistical summary data"
echo

python3 ./summary/gen_config.py $THIS_PATH
python3 ../../../python/summarize/run.py  $THIS_PATH
