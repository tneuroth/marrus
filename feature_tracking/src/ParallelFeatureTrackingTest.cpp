
#include "feature_tracking/CCParallel.hpp"
#include "feature_tracking/ParallelRegionBasedFeatureTrackingCartesianGrid.hpp"
#include "feature_tracking/ParallelRegionBasedFeatureTrackingWeightedRectilinearGrid.hpp"

#include "geometry/Vec.hpp"

#include <thread>
#include <chrono>
#include <fstream>
#include <mpi.h>
#include <cstdint>
#include <array>

int main( int argc, char ** argv ) 
{
    MPI_Init( NULL, NULL );

    int nRanks;
    int rank;
    const MPI_Comm COMM = MPI_COMM_WORLD;

    MPI_Comm_size( COMM, &nRanks );
    MPI_Comm_rank( COMM, &rank   );

    const TN::Vec3< int > dims = { 300, 400, 200 };
    const TN::BlockDecomposition3D blkd( dims, { 1, 1, nRanks } );

    const std::string filePath = "/home/ubuntu/dev/Data/c_0.0.0.300.400.200/appended/turb_shear_no_flame.5.6000E-04.temp.OH.0.0.0.300.400.200.f4.bin";
    const size_t offset = 0;

    const std::string outputDirector  = "./";
    const std::string fileId = "example";
    const int64_t timestep = 0;
    const bool logTimings = true;

    //====================================================================================

    // Compute and write connected components

    const std::vector< TN::Vec2< double > > bands = { { 14.0, 30.0 } };

    TN::ConnectedComponentsParallel< int16_t, int32_t > ccParallel;
    ccParallel.init( nRanks, rank, COMM, logTimings );

    ccParallel.computeConnectedScalarBandsFromFile<float>( filePath, offset, bands, blkd );

    const bool writeClassIds = true;
    ccParallel.writeGlobal( outputDirector, fileId, timestep, blkd, writeClassIds );

    //====================================================================================

    // Get the connected components output to be input to the feature tracking

    const int32_t * ccid_ptr     = ccParallel.ccid_ptr();
    const int32_t num_components = ccParallel.numComponents();

    //====================================================================================

    // Compute and write correspondences between i and j (where i = j, for test purposes) 

    TN::ParallelRegionBasedFeatureTrackingCartesianGrid<int32_t> featureTracker;
    featureTracker.init( nRanks, rank, COMM, logTimings );

    auto blockSize = blkd.localBlockSize( rank );
    const size_t N = ( size_t) blockSize.x() * blockSize.y() * blockSize.z();

    featureTracker.onStep( N, ccid_ptr, ccid_ptr ); 
    featureTracker.writeGlobal( outputDirector, fileId, timestep ); 

    /*====================================================================================

                                        Test the results
    Since i=j

    (1) The sum of the counts should be the number of total feature voxels.
    (2) The number of correspondences should equal the number of components.
    (3) The correspondences should be one to one, exclusively mapping From[ i ] to To[ i ].
    
    ======================================================================================*/

    // count the number of voxels that are part of a features on this rank

    int64_t n_fg_local = 0;
    for( size_t i = 0; i < N; ++i )
    {
        if( ccid_ptr[ i ] != 0 ) {
            ++n_fg_local;
        }
    }

    // sum the number of voxels part of features from all ranks

    int64_t n_fg;
    int error = MPI_Allreduce( 
        & n_fg_local,
        & n_fg, 
        1,
        MPI_INT64_T, 
        MPI_SUM, 
        COMM );

    // count the number of correspondences, and the cumultive associated counts

    auto & correspondenceMap = featureTracker.correspondenceMap();

    size_t cnt_sum = 0;
    size_t csp_sum = 0;

    bool one_to_one = true;
    for( auto & fromMap : correspondenceMap )
    for( auto & toMap   : fromMap.second )
    {
        ++csp_sum;
        cnt_sum += toMap.second;
        if( fromMap.first != toMap.first ) {
            one_to_one = false;
        }
    }

    int my_result_correct = ( cnt_sum == n_fg ) 
                         && ( csp_sum == num_components )
                         && one_to_one;

    // Make sure all ranks have correct results

    int sumCorrect = 0;
    error = MPI_Allreduce( 
        & my_result_correct, 
        & sumCorrect, 
        1,
        MPI_INT,
        MPI_SUM,
        COMM );

    if( rank == 0 )
    {
        std::cout << "Expected " << num_components << " correspondences and " 
                  << n_fg << " cumulative counts." << std::endl; 

        if( sumCorrect == nRanks )
        {
            std::cout << "Test passed." << std::endl;
        }
        else
        {
            std::cout << "Test failed." << std::endl;
        }
    }

    if( sumCorrect != nRanks )
    {
        std::this_thread::sleep_for ( std::chrono::milliseconds( rank * 50 ) );
        std::cout << "Found " << csp_sum << " and " << cnt_sum << " respectively on rank " << rank << "." << std::endl;
        std::cout << "Correspondences " << ( one_to_one ? "are" : "aren't" )
                  << " one to one on rank " << rank << "." << std::endl; 
    }

    /*=============================================================================

                            Weighed Rectilinear Version

    ==============================================================================*/

    TN::ParallelRegionBasedFeatureTrackingWeightedRectilinearGrid< int32_t, double > featureTrackerWL;

    // TODO: need to find a good dataset and pair of time steps

    MPI_Finalize();

    return 0;
}