#ifndef TN_PARALLEL_FEATURE_TRACKING_CARTESIAN_GRID_HPP
#define TN_PARALLEL_FEATURE_TRACKING_CARTESIAN_GRID_HPP

#include "ParallelFeatureTrackingBase.hpp"
#include "CCParallel.hpp"

#include <mpi.h>
#include <unordered_map>
#include <cstdint>
#include <vector>
#include <string>

/*=====================================================================================

        The connected component ids are expected to be from a single feature
        class, meaning that if a component is adjacent to another, it is 
        considered connected.

        In a multi-class implementation, adjacent components could be considered
        disconnected if they have different class ids

======================================================================================*/

namespace TN {

template< typename CC_ID_TYPE >
class ParallelRegionBasedFeatureTrackingCartesianGrid : public ParallelFeatureTrackingBase< CC_ID_TYPE >
{

public : 

    void computeLocal(
            const size_t N,
            const CC_ID_TYPE * cc_i,
            const CC_ID_TYPE * cc_j )
    {
        this->m_correspondenceMap.clear();

        for( size_t idx = 0; idx < N; ++idx ) {

            // component id for t_i
            const CC_ID_TYPE C_I = cc_i[ idx ];

            // component id for t_j
            const CC_ID_TYPE C_J = cc_j[ idx ];

            // background     -> non-background
            // non-background -> background 

            if( C_I == 0 || C_J == 0 ) { continue; }

            // Insert correspondence map if it doesn't exist
            if( ! this->m_correspondenceMap.count( C_I ) ) {
                this->m_correspondenceMap.insert( { C_I, {} } ); 
            }

            auto & mp = this->m_correspondenceMap.at( C_I );

            if( ! mp.count( C_J ) ) { 
                mp.insert( { C_J, 1 } ); 
            } else {
                mp.at( C_J ) += 1;
            }
        }

        this->m_from.clear();
        this->m_to.clear();
        this->m_weights.clear();

        // Extract the correspondences into contiguous array form 

        for( auto & source : this->m_correspondenceMap ) {
            for( auto & dst_count : source.second ) {
                this->m_from.push_back( source.first );
                this->m_to.push_back( dst_count.first );
                this->m_weights.push_back( dst_count.second );
            }
        }
    }

    void onStep(
            const size_t N,
            const CC_ID_TYPE * cc_i,
            const CC_ID_TYPE * cc_j )
    {
        using namespace std::chrono;

        if( this->m_logTimings == true )
        {
            MPI_Barrier( this->m_comm );
        }

        auto start = high_resolution_clock::now();
        computeLocal( N, cc_i, cc_j );
        auto stopLocal = high_resolution_clock::now();

        if( this->m_logTimings == true ) {
            MPI_Barrier( this->m_comm );
        }

        auto startReduce = high_resolution_clock::now();
        this->reduce();
        auto stopTotal = high_resolution_clock::now();

        if( this->m_logTimings == true )
        {
            MPI_Barrier( this->m_comm );
            if( this->m_rank == 0 )
            {
                auto durationLocal = duration_cast<microseconds>(     stopLocal - start       );
                auto durationReduction = duration_cast<microseconds>( stopTotal - startReduce );
                auto durationTotal = duration_cast<microseconds>(     stopTotal - start       );

                this->m_logFile << "ParallelFeatureTracking local Computation took " <<     durationLocal.count() / 1000000.0 << " s." << std::endl;
                this->m_logFile << "ParallelFeatureTracking reduction took "         << durationReduction.count() / 1000000.0 << " s." << std::endl;
                this->m_logFile << "ParallelFeatureTracking total computation took " <<     durationTotal.count() / 1000000.0 << " s." << std::endl;
            }
        }
    }

    void cacheStep( 
        const size_t N, 
        const CC_ID_TYPE * cc_j )
    {
        this->m_cached_ccids.resize( N );
        for( size_t idx = 0; idx < N; ++idx ) {
            this->m_cached_ccids[ idx ] = cc_j[ idx ];
        }
        this->m_previousStepCached = true;
    }

    void onStep(
            const size_t N,
            const CC_ID_TYPE * cc_j )
    {
        if( this->m_previousStepCached ) {
            onStep( N, this->m_cached_ccids.data(), cc_j );
        }
        
        cacheStep( N, cc_j );
    }
};

}

#endif