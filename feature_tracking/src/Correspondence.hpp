#ifndef TN_FEATURE_SPATIAL_FEATURE_CORRESPONDENCE_HPP
#define TN_FEATURE_SPATIAL_FEATURE_CORRESPONDENCE_HPP

#include <cstdint>
#include <vector>
#include <unordered_map>
#include <fstream>

namespace FT {

template< typename T >
void writeData( 
    const string & filePath,
    const T * data,
    int64_t N )
{
    // cout << "Writing data to " <<  filePath << endl;
    ofstream outFile( filePath, ios::out | ios::binary );
    if( ! outFile.is_open() ) {
        cerr << "file could not be opened for writing" << filePath << endl;
        exit( 1 );
    }
    outFile.write( ( char* ) data, N * sizeof( T )  );
    outFile.close();
}

template< typename T >
void loadData( 
    const string & filePath,
    vector< T > & data, 
    size_t offset, 
    size_t numElements )
{
    ifstream inFile( filePath, ios::in | ios::binary );
    if( ! inFile.is_open() ) {
        cout << "could not be opened for reading " << filePath << endl;
       exit( 1 );
    }
    data.resize( numElements );
    inFile.seekg( offset * sizeof( T ) );
    inFile.read( (char*) data.data(), numElements * sizeof( T ) ); 
    inFile.close();
}

static inline 
void writeCorrespondences(
	const std::string & directory,
	const std::string & ts_string,
	const std::unordered_map< int32_t, std::unordered_map< int32_t, int64_t > > & correspondenceMap )
{
	// store correspondence data in arrays for saving to disk
	vector< int32_t > from;
	vector< int32_t > to;
	vector< int64_t > counts;

	// extract the correspondences
	for( auto & source : correspondenceMap ) {
	    for( auto & dst_count : source.second ) {
	        from.push_back( source.first );
	        to.push_back( dst_count.first );
	        counts.push_back( dst_count.second );
	    }
	}

	// write to disk, non interleaved

	int64_t nccr = from.size();
	nCorsFile.write( (char*) & nccr, sizeof( int64_t ) );

	FT::writeData( 
	    directory + "/from." + ts_string + "." + ".bin",
	    from.data(),
	    from.size() );

	FT::writeData( 
	    directory + "/to." + ts_string + "." + ".bin",
	    to.data(),
	    to.size() );

	FT::writeData( 
	    directory + "/counts." + ts_string + "." + ".bin",
	    counts.data(),
	    counts.size() );
}

static inline 
void loadCorrespondences(
	const std::string & directory,
	const std::string & ts_string,
	int64_t N,
	std::unordered_map< int32_t, std::unordered_map< int32_t, int64_t > > & correspondenceMap )
{
	vector< int32_t > from;
	vector< int32_t > to;
	vector< int64_t > counts;

	FT::loadData( directory + "/from." + ts_string + "." + ".bin", from,   0, N );
	FT::loadData( directory + "/from." + ts_string + "." + ".bin", to  ,   0, N );
	FT::loadData( directory + "/from." + ts_string + "." + ".bin", counts, 0, N );

	for( size_t i = 0; i < N; ++i )
	{
		if( ! correspondenceMap.count( from[ i ] ) ) {
		    correspondenceMap.insert( { from[ i ], {} } ); 
		}
		auto & mp = correspondenceMap.at( from[ i ] );
		mp.insert( { to[ i ], count[ i ] } ); 
	}
}


static inline 
void computeCorrespondences(
		const size_t N,
		const int32_t * cc_i,
		const int32_t * cc_j,
		std::unordered_map< int32_t, std::unordered_map< int32_t, int64_t > > & correspondenceMap )
{
	correspondenceMap.clear();

	for( size_t idx = 0; idx < N; ++idx ) {

		// component id for t_i
		const int32_t C_I = cc_i[ idx ];

		// component id for t_j
		const int32_t C_J = cc_j[ idx ];

		// background     -> non-background
		// non-background -> background 
		if( cmp1 == 0 || cmp2 == 0 ) { continue; }

		// Insert correspondence map if it doesn't exist
		if( ! correspondenceMap.count( C_I ) ) {
		    correspondenceMap.insert( { C_I, {} } ); 
		}

		auto & mp = correspondenceMap.at( C_I );

		if( ! mp.count( C_J ) ) { 
		    mp.insert( { C_J, 1 } ); 
		} else {
		    mp.at( C_J ) += 1;
		}
	}
}

} // end namespace FT

#endif