
#include "utils/SimpleBlockDecompositionColumnOrder.hpp"
#include "feature_tracking/CCParallel.hpp"
#include "geometry/Vec.hpp"

#include <fstream>
#include <mpi.h>
#include <cstdint>
#include <array>

int main( int argc, char ** argv ) 
{
    MPI_Init( NULL, NULL );

    int nRanks;
    int rank;    
    const MPI_Comm COMM = MPI_COMM_WORLD;

    MPI_Comm_size( COMM, &nRanks );
    MPI_Comm_rank( COMM, &rank   );

    const TN::Vec3< int > dims = { 300, 400, 200 };
    const std::string filePath = "/home/ubuntu/dev/Data/c_0.0.0.300.400.200/appended/turb_shear_no_flame.5.6000E-04.temp.OH.0.0.0.300.400.200.f4.bin";
    const size_t offset = 0;

    const std::vector< TN::Vec2< double > > bands = { { 14.0, 30.0 } };
    const TN::BlockDecomposition3D blkd( dims, { 1, 1, nRanks } );

    TN::ConnectedComponentsParallel< int16_t, int32_t > ccParallel;
    bool logTimings = true;

    ccParallel.init( nRanks, rank, COMM, logTimings );

    ccParallel.computeConnectedScalarBandsFromFile<float>( 
        filePath, 
        offset,
        bands, 
        blkd );

    std::string directory  = "./";
    std::string identifier = "example";
    int64_t timestep = 0;
    bool writeFeatureIds = true;

    ccParallel.writeGlobal( directory, identifier, timestep, blkd, writeFeatureIds );

    MPI_Finalize();

    return 0;
}