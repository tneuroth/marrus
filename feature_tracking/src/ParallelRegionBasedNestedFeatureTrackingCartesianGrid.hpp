#ifndef TN_PARALLEL_REGION_BASED_NESTED_FEATURE_TRACKING_CARTESIAN_GRID_HPP
#define TN_PARALLEL_REGION_BASED_NESTED_FEATURE_TRACKING_CARTESIAN_GRID_HPP

#include "feature_tracking/ParallelFeatureTrackingBase.hpp"
#include "feature_tracking/CCParallel.hpp"
#include "feature_tracking/NestingGraph.hpp"

#include <mpi.h>
#include <unordered_map>
#include <cstdint>
#include <vector>
#include <string>

/*============================================================================================

 TODO : Implement it!

    Tracks nested features.

    It is assumed that the components belong to nested classes, with the nesting graph 
    representing the nesting of components within components

    It's also assumed that the class ids go from 1 to num_classes, with 0 representing 
    background.

    It's also assumed that the classes are coherent, meaning that class_k at time
    step i has the same meaning as class_k for time step j. 

    Also, it is assumed that a component is nested uniquely into a single component 
    encapsulating it. By default, that might not be the case even when expected because
    the discretization of the grid might close off thinly separated regions. For safety
    the constraint will be checked, and if the constraint is not met, then a warning will
    be logged, equivalencies will be determined between what will be assumed to be erroniously 
    segmented components, the correspondences will be stored based on the smallest id of 
    the euqivalent segements and the equivalencies will be written when write is called. 

    A correspondence is only established between components within the same class
    however, the weights are computed based on the intersection of the enire interior,
    S_c, where S_c represents the union over i=0 to i=c of S_{c-i}, where c is the 
    class id, assuming S_{c-1} is nested in S_c for all c.

==============================================================================================*/

namespace TN {

template< typename CC_ID_TYPE, typename CLASS_ID_TYPE >
class ParallelRegionBasedNestedFeatureTrackingCartesianGrid : public ParallelFeatureTrackingBase< CC_ID_TYPE >
{
    std::vector< CLASS_ID_TYPE > m_cached_classes;
    TN::NestingGraph< CC_ID_TYPE, CLASS_ID_TYPE > m_cached_nestingMap;

public : 

    void computeLocal(
            const size_t N,
            const CC_ID_TYPE * cc_i,
            const CC_ID_TYPE * cc_j,
            const CLASS_ID_TYPE * classes_i,
            const CLASS_ID_TYPE * classes_j,
            const TN::NestingGraph< CC_ID_TYPE, CLASS_ID_TYPE > & nestingMap_i,
            const TN::NestingGraph< CC_ID_TYPE, CLASS_ID_TYPE > & nestingMap_j )
    {
        this->m_correspondenceMap.clear();

        // assume the same class 

        const std::vector< CLASS_ID_TYPE > classIdsI = nestingMap_i.classIds();
        const std::vector< CLASS_ID_TYPE > classIdsJ = nestingMap_j.classIds();

        for( size_t idx = 0; idx < N; ++idx ) {

            const CC_ID_TYPE C_I = cc_i[ idx ];
            const CC_ID_TYPE C_J = cc_j[ idx ];

            const CLASS_ID_TYPE class_i = classes_i[ idx ];
            const CLASS_ID_TYPE class_j = classes_j[ idx ];

            // background     -> non-background
            // non-background -> background 

            if( C_I == 0 || C_J == 0 ) { continue; }

            for( int l_i = classes_i; l_i > 0; --l_i ) 
            {
                // ...
                
            }
        }

        this->m_from.clear();
        this->m_to.clear();
        this->m_weights.clear();

        // Extract the correspondences into contiguous array form 

        for( auto & source : this->m_correspondenceMap ) {
            for( auto & dst_count : source.second ) {
                this->m_from.push_back( source.first );
                this->m_to.push_back( dst_count.first );
                this->m_weights.push_back( dst_count.second );
            }
        }
    }

    void onStep(
            const size_t N,
            const CC_ID_TYPE * cc_i,
            const CC_ID_TYPE * cc_j,
            const CLASS_ID_TYPE * classes_i,
            const CLASS_ID_TYPE * classes_j,
            const TN::NestingGraph< CC_ID_TYPE, CLASS_ID_TYPE > & nestingMap_i,
            const TN::NestingGraph< CC_ID_TYPE, CLASS_ID_TYPE > & nestingMap_j )
    {
        using namespace std::chrono;

        if( this->m_logTimings == true )
        {
            MPI_Barrier( this->m_comm );
        }

        auto start = high_resolution_clock::now();
        computeLocal( N, cc_i, cc_j, nestingMap_i, nestingMap_j );
        auto stopLocal = high_resolution_clock::now();

        if( this->m_logTimings == true ) {
            MPI_Barrier( this->m_comm );
        }

        auto startReduce = high_resolution_clock::now();
        this->reduce();
        auto stopTotal = high_resolution_clock::now();

        if( this->m_logTimings == true )
        {
            MPI_Barrier( this->m_comm );
            if( this->m_rank == 0 )
            {
                auto durationLocal = duration_cast<microseconds>(     stopLocal - start       );
                auto durationReduction = duration_cast<microseconds>( stopTotal - startReduce );
                auto durationTotal = duration_cast<microseconds>(     stopTotal - start       );

                this->m_logFile << "ParallelFeatureTracking local Computation took " <<     durationLocal.count() / 1000000.0 << " s." << std::endl;
                this->m_logFile << "ParallelFeatureTracking reduction took "         << durationReduction.count() / 1000000.0 << " s." << std::endl;
                this->m_logFile << "ParallelFeatureTracking total computation took " <<     durationTotal.count() / 1000000.0 << " s." << std::endl;
            }
        }
    }

    void cacheStep( 
        const size_t N, 
        const CC_ID_TYPE * cc_j,
        const CLASS_ID_TYPE * classes_j,
        const TN::NestingGraph< CC_ID_TYPE, CLASS_ID_TYPE > & nestingMap )
    {
        this->m_cached_ccids.resize( N );
        for( size_t idx = 0; idx < N; ++idx ) {
            this->m_cached_ccids[   idx ] =      cc_j[ idx ];
            this->m_cached_classes[ idx ] = classes_j[ idx ];
        }
        this->m_cached_nestingMap = nestingMap;
        this->m_previousStepCached = true;
    }

    void onStep(
            const size_t N,
            const CC_ID_TYPE * cc_j,
            const CLASS_ID_TYPE * classes_j,
            const TN::NestingGraph< CC_ID_TYPE, CLASS_ID_TYPE > & nestingMap_j )
    {
        if( this->m_previousStepCached ) {
            onStep( 
                N, 
                this->m_cached_ccids.data(), 
                cc_j, 
                this->m_cached_classes.data(), 
                classes_j,
                this->m_cached_nestingMap,
                nestingMap_j );
        }

        cacheStep( N, cc_j, classes_j, nestingMap_j );
    }
};

}

#endif