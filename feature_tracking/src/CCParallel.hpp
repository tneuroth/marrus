#ifndef TN_CC_PARALLEL_HPP
#define TN_CC_PARALLEL_HPP

#include "algorithms/ConnectedComponentsCPU.hpp"
#include "algorithms/CCTest.hpp"
#include "geometry/Vec.hpp"
#include "utils/IndexUtils.hpp"
#include "utils/SimpleBlockDecompositionColumnOrder.hpp"

#include <fstream>
#include <mpi.h>
#include <cstdint>
#include <array>
#include <unordered_set>
#include <limits>
#include <vector>

#include <chrono>
#include <thread>

namespace TN {

template < typename CLASS_ID_TYPE, typename CC_ID_TYPE >
class ConnectedComponentsParallel 
{
    MPI_Comm m_comm;
    int m_rank;
    int m_nRanks;

    // for each voxel
    std::vector< CC_ID_TYPE > m_ccVoxelIdsLocal;
    std::vector<  CLASS_ID_TYPE > m_voxelClassIds;
    
    CLASS_ID_TYPE * m_voxelClassIdsPtr;

    bool m_classIdsStored;

    // a table that translates local cc ids to global ccids
    std::vector< CC_ID_TYPE > m_ccs2Global;

    CC_ID_TYPE m_nc_local;
    CC_ID_TYPE m_nc_global;

    MPI_Datatype CC_ID_TYPE_MPI;
    MPI_Datatype CLASS_ID_TYPE_MPI;

    std::ofstream m_logFile;
    bool m_logTimings;

    template < typename T >
    void mapBands(
        const int64_t N,
        CLASS_ID_TYPE * result,
        const T * data,
        const std::vector< TN::Vec2< double > > & layers )
    {
        for( int64_t idx = 0; idx < N; ++ idx ) {
            float s = data[ idx ];
            result[ idx ] = 0;
            for( size_t l = 0, end = layers.size(); l < end; ++l ) {
                const double la = layers[ l ].a();
                const double lb = layers[ l ].b();
                if( s >= la && s < lb  ) {
                    result[ idx ] = l + 1;
                    break;
                }
            }
        }
    }

    CC_ID_TYPE uf_find( CC_ID_TYPE idx, CC_ID_TYPE * eqc )
    {
        int root = idx;
        while ( eqc[ root ] != root ) 
        { 
            root = eqc[ root ]; 
        }
        
        // path compression
        while ( eqc[ idx ] != idx ) 
        {
          const CC_ID_TYPE newIdx = eqc[ idx ];
          eqc[ idx ] = root;
          idx = newIdx;
        }

        return root;
    }

    void uf_union( const CC_ID_TYPE idx1, const CC_ID_TYPE idx2, CC_ID_TYPE * eqc )
    {
        const CC_ID_TYPE tmp1 = uf_find( idx1, eqc );
        const CC_ID_TYPE tmp2 = uf_find( idx2, eqc );
        eqc[ tmp1 ] = tmp2;
    }

public: 

    ConnectedComponentsParallel() : m_voxelClassIdsPtr( 0 ), m_classIdsStored( false )
    {
        static_assert( 
            std::is_integral< CLASS_ID_TYPE >::value && sizeof( CLASS_ID_TYPE ) >= 1 && sizeof( CLASS_ID_TYPE ) <= 8,
            "Error in CCParallel: First template parameter, class id type, must be integral, and between 1 and 8 bytes." );

        static_assert( 
            ( std::is_integral< CC_ID_TYPE >::value ) && sizeof( CC_ID_TYPE ) >= 4 && sizeof( CC_ID_TYPE ) <= 8,
            "Error in CCParallel: Second template parameter, component id type, must be integral and have size of 4 bytes or 8 bytes." );

        CC_ID_TYPE_MPI = sizeof( CC_ID_TYPE ) == 4 ? MPI_INT32_T : MPI_INT64_T;

        CLASS_ID_TYPE_MPI  = sizeof( CLASS_ID_TYPE ) == 1 ? MPI_INT8_T 
                       : sizeof( CLASS_ID_TYPE ) == 2 ? MPI_INT16_T
                       : sizeof( CLASS_ID_TYPE ) == 4 ? MPI_INT32_T
                       : MPI_INT64_T;
    
        m_logFile.open( "CCParallel.log.txt" );
    }

    ~ConnectedComponentsParallel()
    {
        m_logFile.close();
    }

    void init( int nRanks, int rank, MPI_Comm comm, bool logTimings )
    {
        m_logTimings = logTimings;
        m_rank       = rank;
        m_nRanks     = nRanks;
        m_comm       = comm;
    }

    void computeLocalComponents( 
        CLASS_ID_TYPE * classIdsLocal, 
        const TN::BlockDecomposition3D & bc )
    {
        TN::Vec3< int > d = bc.localBlockSize( m_rank );
        const int MAX_CMPS = ( d.x() * d.y() * d.z() ) / 10L;

        m_ccVoxelIdsLocal.resize( d.x() * d.y() * d.z() );
        TN::connectedComponents( 
            classIdsLocal,
            std::array< int64_t, 3 >( { d.x(), d.y(), d.z() } ),    
            MAX_CMPS,
            m_ccVoxelIdsLocal.data(),
            m_nc_local );

        bool valid = TN::verifyComponents(
            m_ccVoxelIdsLocal.data(),
            classIdsLocal,
            m_nc_local,
            std::array< int64_t, 3 >( { d.x(), d.y(), d.z() } ),
            false );
    }

    void reduce( 
        CC_ID_TYPE * ccIds, 
        const TN::BlockDecomposition3D & bc  )
    {
        TN::Vec3< int > bIdx       = bc.bIdx3D( m_rank );
        TN::Vec3< int > blockSize  = bc.localBlockSize( m_rank );
        TN::Vec3< int > bDims      = bc.bDims;
        MPI_Status status;

        /*==========================================================

               Make initial global ids consecutive and unique

        ============================================================*/

        // initialization of local to global cc id table

        std::vector< CC_ID_TYPE > tableLengths( m_nRanks ); 
        m_ccs2Global.resize( m_nc_local );

        for( CC_ID_TYPE i = 0; i < m_nc_local; ++i ) { m_ccs2Global[ i ] = i; }

        int error = MPI_Allgather(
            & m_nc_local, 
            1, 
            CC_ID_TYPE_MPI,
            tableLengths.data(), 
            1, 
            CC_ID_TYPE_MPI,
            m_comm );

        CC_ID_TYPE myIncrement = 0;
        CC_ID_TYPE maxGlobalId = 0;

        for( int i = 0; i < m_rank; ++i  )
        {
            myIncrement += tableLengths[ i ];
        }

        for( int i = 0; i < m_nRanks; ++i )
        {
            maxGlobalId += tableLengths[ i ];
        }

        for( int i = 1; i < m_ccs2Global.size(); ++i )
        {
            m_ccs2Global[ i ] += myIncrement;
        }

        /*=========================================================
    
                            Send adjacent slabs

        ===========================================================*/

        // first extract and send the slabs ///////////////////

        const int64_t X = blockSize.x();
        const int64_t Y = blockSize.y();
        const int64_t Z = blockSize.z();

        if( bIdx.z() < bDims.z() - 1 ) {
            
            std::vector< CC_ID_TYPE > zSlab_send( X*Y );
            
            for( int64_t x = 0; x < X; ++x )
            for( int64_t y = 0; y < Y; ++y )            
            {
                const CC_ID_TYPE IDX = TN::flatIndex3dCM( x, y, Z-1, X, Y, Z );
                zSlab_send[ x + y*X ] = m_ccs2Global[ ccIds[ IDX ] ];
            }
            
            const int recieverRank = TN::flatIndex3dCM( 
                 bIdx.x(),
                 bIdx.y(),
                 bIdx.z() + 1, 
                 bDims.x(), 
                 bDims.y(),
                 bDims.z() );

            int s = MPI_Send(
                zSlab_send.data(),
                zSlab_send.size(), 
                CC_ID_TYPE_MPI, 
                recieverRank, 
                0, 
                m_comm );
        }

        if( bIdx.y() < bDims.y() - 1 ) {

            std::vector< CC_ID_TYPE > ySlab_send( X*Z );
            
            for( int64_t x = 0; x < X; ++x )
            for( int64_t z = 0; z < Z; ++z )            
            {
                const int64_t IDX = TN::flatIndex3dCM( x, Y-1, z, X, Y, Z );
                ySlab_send[ x + z*X ] = m_ccs2Global[ ccIds[ IDX ] ];
            }

            const int recieverRank = TN::flatIndex3dCM( 
                 bIdx.x(),  
                 bIdx.y() + 1,  
                 bIdx.z(), 
                 bDims.x(), 
                 bDims.y(),
                 bDims.z() );

            int s = MPI_Send(
                ySlab_send.data(),
                ySlab_send.size(), 
                CC_ID_TYPE_MPI, 
                recieverRank, 
                0, 
                m_comm );
        }

        if( bIdx.x() < bDims.x() - 1 ) {
            
            std::vector< CC_ID_TYPE > xSlab_send( Y*Z );
            
            for( int64_t y = 0; y < Y; ++y )
            for( int64_t z = 0; z < Z; ++z )            
            {
                const int64_t IDX = TN::flatIndex3dCM( X-1, y, z, X, Y, Z );
                xSlab_send[ y + z*Y ] = m_ccs2Global[ ccIds[ IDX ] ];       
            }

            const int recieverRank = TN::flatIndex3dCM( 
                 bIdx.x() + 1,  
                 bIdx.y(),  
                 bIdx.z(), 
                 bDims.x(), 
                 bDims.y(),
                 bDims.z() );

            int s = MPI_Send(
                xSlab_send.data(),
                xSlab_send.size(), 
                CC_ID_TYPE_MPI, 
                recieverRank, 
                0, 
                m_comm );
        }

        /*============================================================
         
             Recieve adjacent slabs and compute equivalence classes

        =============================================================*/

        std::vector< CC_ID_TYPE > equiv( maxGlobalId+1, 1 );
        for( CC_ID_TYPE i = 0; i < equiv.size(); ++i )
        {
            equiv[ i ] = i;
        }

        if( bIdx.z() > 0 ) {

            std::vector< CC_ID_TYPE > zSlab_recv( X*Y );
            const int senderRank = TN::flatIndex3dCM(
                 bIdx.x(),  
                 bIdx.y(),  
                 bIdx.z() - 1, 
                 bDims.x(), 
                 bDims.y(),
                 bDims.z() );

            // recieve the neighbors adjacent slab of cc ids

            int s = MPI_Recv(
                zSlab_recv.data(), 
                zSlab_recv.size(),
                CC_ID_TYPE_MPI, 
                senderRank, 
                0, 
                m_comm, 
                & status );

            for( int64_t x = 0; x < X; ++x )
            for( int64_t y = 0; y < Y; ++y )            
            {
                const int64_t IDX = TN::flatIndex3dCM( x, y, 0, X, Y, Z );
                const CC_ID_TYPE cc  = m_ccs2Global[ ccIds[ IDX ] ]; 
                const CC_ID_TYPE ccr = zSlab_recv[ x + y*X ];

                if( cc != 0 && ccr != 0 )
                {
                    uf_union( cc, ccr, equiv.data() );
                }
            }
        }
        if( bIdx.y() > 0 ) {

            std::vector< CC_ID_TYPE > ySlab_recv( X*Z );

            const int senderRank = TN::flatIndex3dCM(
                 bIdx.x(),  
                 bIdx.y() - 1,  
                 bIdx.z(), 
                 bDims.x(), 
                 bDims.y(),
                 bDims.z() );

            // recieve the neighbors adjacent slab of cc ids

            int s = MPI_Recv(
                ySlab_recv.data(), 
                ySlab_recv.size(),
                CC_ID_TYPE_MPI, 
                senderRank, 
                0, 
                m_comm, 
                & status );

            for( int64_t x = 0; x < X; ++x )
            for( int64_t z = 0; z < Z; ++z )            
            {
                const int64_t IDX = TN::flatIndex3dCM( x, 0, z, X, Y, Z );
                const CC_ID_TYPE cc  = m_ccs2Global[ ccIds[ IDX ] ]; 
                const CC_ID_TYPE ccr = ySlab_recv[ x + z*X ];

                if( cc != 0 && ccr != 0 )
                {
                    uf_union( cc, ccr, equiv.data() );
                }
            }           
        }

        if( bIdx.x() > 0 ) {

            std::vector< CC_ID_TYPE > xSlab_recv( Y*Z );

            const int senderRank = TN::flatIndex3dCM(
                 bIdx.x() - 1,  
                 bIdx.y(),
                 bIdx.z(), 
                 bDims.x(), 
                 bDims.y(),
                 bDims.z() );

            // recieve the neighbors adjacent slab of cc ids

            int s = MPI_Recv(
                xSlab_recv.data(), 
                xSlab_recv.size(),
                CC_ID_TYPE_MPI, 
                senderRank, 
                0, 
                m_comm, 
                & status );

            for( int64_t y = 0; y < Y; ++y )
            for( int64_t z = 0; z < Z; ++z )            
            {
                const int64_t IDX = TN::flatIndex3dCM( 0, y, z, X, Y, Z );
                const CC_ID_TYPE cc  = m_ccs2Global[ ccIds[ IDX ] ]; 
                const CC_ID_TYPE ccr = xSlab_recv[ y + z*Y ];

                if( cc != 0 && ccr != 0 )
                {
                    uf_union( cc, ccr, equiv.data() );
                }
            }       
        }

        /*================================================================
         
             Merge the equivalence classes and send them to each rank

        ==================================================================*/

        int numEQ = equiv.size();

        std::vector< int > eqLengths( m_nRanks ); 
        error = MPI_Gather(
            &numEQ, 
            1, 
            MPI_INT,
            eqLengths.data(), 
            1, 
            MPI_INT,
            0,
            m_comm );

        std::vector< CC_ID_TYPE > allEquivs;
        std::vector< int > offsets( m_nRanks, 0 ); 

        if( m_rank == 0 ) {
            size_t totalLength = eqLengths[ 0 ];
            for( int i = 1; i < m_nRanks; ++i )
            {
                offsets[ i ] = offsets[ i - 1 ] + eqLengths[ i ]; 
                totalLength += eqLengths[ i ];
            }
            allEquivs.resize( totalLength );
        }

        error = MPI_Gatherv(
            equiv.data(), 
            numEQ, 
            CC_ID_TYPE_MPI,
            allEquivs.data(), 
            eqLengths.data(), 
            offsets.data(),
            CC_ID_TYPE_MPI, 
            0, 
            m_comm );

        // now merge the equivalence classes

        if( m_rank == 0 )
        {
            for( int i = 1; i < m_nRanks; ++i )
            {
                const int OFF = offsets[ i ];
                const int N   = eqLengths[ i ];
                for( int j = 1; j < N; ++j )
                {
                    const CC_ID_TYPE b = uf_find( j, & allEquivs[ OFF ] );
                    uf_union( j, b, equiv.data() );
                }
            }
        }

        // now send the global equivalence classes to all nodes

        std::vector< CC_ID_TYPE > equivGlobal( equiv.size() );
        if( m_rank == 0 )
        {
            equivGlobal = equiv;
        }

        error = MPI_Bcast(
            equivGlobal.data(),
            equivGlobal.size(),
            CC_ID_TYPE_MPI,
            0,
            m_comm );

        /*=========================================================
         
              Relabel the Global Ids and make them consecutive

        ==========================================================*/

        // not get all tables for all ids, which is only needed in order to 
        // make the ids consecutive / go from 0 to NC

        int totalTableLength = 0;
        std::vector< int > tableOffsets( m_nRanks, 0 );

        for( int i = 0; i < tableLengths.size(); ++i )
        {
            tableOffsets[ i ] = totalTableLength; 
            totalTableLength += tableLengths[ i ]; 
        }

        std::vector< CC_ID_TYPE > allTables( totalTableLength );

        error = MPI_Allgatherv(
            m_ccs2Global.data(), 
            m_ccs2Global.size(), 
            CC_ID_TYPE_MPI,
            allTables.data(), 
            tableLengths.data(), 
            tableOffsets.data(),
            CC_ID_TYPE_MPI, 
            m_comm );

        // now update the ids and make them consecutive

        std::fill( equiv.begin(), equiv.end(), 0 );
        CC_ID_TYPE nextId = 1;

        for( int r = 0; r < m_nRanks; ++r )
        {
            const CC_ID_TYPE * tableR = allTables.data() + tableOffsets[ r ];
            const int N = tableLengths[ r ];

            for( int i = 1; i < N; ++i )
            {
                const CC_ID_TYPE root = uf_find( tableR[ i ], equivGlobal.data() );

                if( equiv[ root ] == 0 )
                {
                    equiv[ root ] = nextId;
                    ++nextId;
                }
                if( r == m_rank ) 
                {
                    m_ccs2Global[ i ] = equiv[ root ]; 
                }
            }
        }

        m_nc_global = nextId - 1;

        // convert local ids to global ids

        for( size_t i = 0; i < m_ccVoxelIdsLocal.size(); ++i )
        {
            m_ccVoxelIdsLocal[ i ] = m_ccs2Global[ m_ccVoxelIdsLocal[ i ] ];
        }
    }

    void compute( 
        CLASS_ID_TYPE * classIdsLocal, 
        const TN::BlockDecomposition3D & bc )
    {
        m_voxelClassIdsPtr = classIdsLocal;

        using namespace std::chrono;

        if( m_logTimings == true )
        {
            MPI_Barrier( m_comm );
        }

        auto start = high_resolution_clock::now();
        computeLocalComponents( classIdsLocal, bc );
        auto stopLocal = high_resolution_clock::now();

        if( m_logTimings == true )
        {
            MPI_Barrier( m_comm );
        }

        auto startReduce = high_resolution_clock::now();
        reduce( m_ccVoxelIdsLocal.data(), bc );
        auto stopTotal = high_resolution_clock::now();

        if( m_logTimings == true )
        {
            MPI_Barrier( m_comm );
            if( m_rank == 0 )
            {
                auto durationLocal     = duration_cast< microseconds>( stopLocal - start       );
                auto durationReduction = duration_cast< microseconds>( stopTotal - startReduce );
                auto durationTotal     = duration_cast< microseconds>( stopTotal - start       );

                m_logFile << "CCParallel local Computation took " <<     durationLocal.count() / 1000000.0 << " s." << std::endl;
                m_logFile << "CCParallel reduction took "         << durationReduction.count() / 1000000.0 << " s." << std::endl;
                m_logFile << "CCParallel total computation took " <<     durationTotal.count() / 1000000.0 << " s." << std::endl;
            }
        }
    }

    template< typename DT >
    void computeConnectedScalarBands( 
        const size_t N,
        DT * s,
        const std::vector< TN::Vec2< double > > & bands,
        const TN::BlockDecomposition3D & blockDecomposition )
    {
        using namespace std::chrono;
        
        if( m_logTimings )
        {
            MPI_Barrier( m_comm );
        }

        auto start = high_resolution_clock::now();

        m_classIdsStored = true;
        m_voxelClassIds.resize( N );
        mapBands( 
            N,
            m_voxelClassIds.data(), 
            s, 
            bands );

        if( m_logTimings == true )
        {
            MPI_Barrier( m_comm );
            if( m_rank == 0 )
            {
                auto stop = high_resolution_clock::now();
                auto duration = duration_cast<microseconds>(stop - start);
                m_logFile << "CCParallel labelling scalar field bands took " << duration .count() / 1000000.0 << " s." << std::endl;
            }
        }

        compute( 
            m_voxelClassIds.data(),
            blockDecomposition );
    }

    template< typename DT >
    void computeConnectedScalarBandsFromFile( 
        const std::string & filePath,
        const size_t fieldOffset,
        const std::vector< TN::Vec2< double > > & bands,
        const TN::BlockDecomposition3D & blockDecomposition )
    {   
        using namespace std::chrono;

        if( m_logTimings )
        {
            MPI_Barrier( m_comm );
        }

        auto start = high_resolution_clock::now();

        const auto blockSize    = blockDecomposition.localBlockSize(   m_rank ); 
        const auto blockOffsets = blockDecomposition.localDataOffsets( m_rank ); 
        const auto blockDims    = blockDecomposition.bDims;
        const auto dataDims     = blockDecomposition.dataDims;

        if( blockDims.x()*blockDims.y()*blockDims.z() != m_nRanks ) {
            if( m_rank == 0 ) {
                std::cerr << "Error in CCParallel: must same number of blocks many ranks." << std::endl;
            }
            MPI_Finalize();
            exit( 0 );
        }

        const size_t N = dataDims.x() * dataDims.y() * dataDims.z();
        const size_t N_LOAD = blockSize.x() * blockSize.y() * blockSize.z();

        MPI_File fileHandle;
        MPI_Info info = MPI_INFO_NULL;
        MPI_Status status;
        MPI_Datatype fileType;

        int error = MPI_File_open(
            m_comm, 
            filePath.c_str(), 
            MPI_MODE_RDONLY, 
            info, 
            &fileHandle );

        std::vector< DT > s( N_LOAD );

        int sizes[    3 ] = {     dataDims.x(),     dataDims.y(),     dataDims.z() };
        int subsizes[ 3 ] = {    blockSize.x(),    blockSize.y(),    blockSize.z() };
        int starts  [ 3 ] = { blockOffsets.x(), blockOffsets.y(), blockOffsets.z() };

        error = MPI_Type_create_subarray(
            3, 
            sizes, 
            subsizes, 
            starts,
            MPI_ORDER_FORTRAN,
            CC_ID_TYPE_MPI, 
            &fileType );

        error = MPI_Type_commit( & fileType );

        error = MPI_File_set_view( 
            fileHandle, 
            0,
            CC_ID_TYPE_MPI, 
            fileType, 
            "native", 
            MPI_INFO_NULL );

        error = MPI_File_read_all(
            fileHandle, 
            s.data(), 
            s.size(), 
            CC_ID_TYPE_MPI, 
            & status ); 

        error = MPI_Type_free(  &fileType );
        error = MPI_File_close( &fileHandle );

        if( m_logTimings == true )
        {
            MPI_Barrier( m_comm );
            if( m_rank == 0 )
            {
                auto stop = high_resolution_clock::now();
                auto duration = duration_cast<microseconds>(stop - start);
                m_logFile << "CCParallel reading scalar field took " << duration .count() / 1000000.0 << " s" << std::endl;
            }
        }

        // compute bands and connected components of them
        computeConnectedScalarBands<DT>( s.size(), s.data(), bands, blockDecomposition );
    }

    void writeGlobal( 
        const std::string & directory, 
        const std::string & identifier,  
        int64_t timestep,
        const TN::BlockDecomposition3D & b,
        bool writeClassIds )
    {   
        if( m_logTimings )
        {
            MPI_Barrier( m_comm );
        }

        std::string tstr = std::to_string( timestep );
        std::string prefix = directory + "/cc_ids." + identifier + ".";
        std::string path = prefix + tstr + ".bin";

        using namespace std::chrono;
        auto start = high_resolution_clock::now();

        MPI_File fileHandle;
        MPI_Info info = MPI_INFO_NULL;
        MPI_Status status;
        MPI_Datatype fileType;

        int error = MPI_File_open(
            MPI_COMM_WORLD, 
            path.c_str(), MPI_MODE_CREATE | MPI_MODE_WRONLY, 
            info, 
            &fileHandle );

        const auto blockSize    = b.localBlockSize(   m_rank ); 
        const auto blockOffsets = b.localDataOffsets( m_rank ); 
        const auto blockDims    = b.bDims;
        const auto dataDims     = b.dataDims;

        int sizes[    3 ] = {     dataDims.x(),     dataDims.y(),     dataDims.z() };
        int subsizes[ 3 ] = {    blockSize.x(),    blockSize.y(),    blockSize.z() };
        int starts  [ 3 ] = { blockOffsets.x(), blockOffsets.y(), blockOffsets.z() };
    
        error = MPI_Type_create_subarray(
            3,
            sizes,
            subsizes,
            starts,
            MPI_ORDER_FORTRAN,
            CC_ID_TYPE_MPI,
            & fileType );

        error = MPI_Type_commit( & fileType );

        error = MPI_File_set_view( 
            fileHandle, 
            0,
            CC_ID_TYPE_MPI, 
            fileType, 
            "native", 
            MPI_INFO_NULL );

        error = MPI_File_write_all(
            fileHandle, 
            m_ccVoxelIdsLocal.data(), 
            m_ccVoxelIdsLocal.size(), 
            CC_ID_TYPE_MPI, 
            & status );

        error = MPI_Type_free( & fileType   );
        MPI_File_close(        & fileHandle );

        if( writeClassIds ) {

            prefix = directory + "/class_ids." + identifier + ".";
            path = prefix + tstr + ".bin";

            int error = MPI_File_open(
                MPI_COMM_WORLD, 
                path.c_str(), MPI_MODE_CREATE | MPI_MODE_WRONLY, 
                info, 
                &fileHandle );

            error = MPI_Type_create_subarray(
                3, 
                sizes, 
                subsizes, 
                starts,
                MPI_ORDER_FORTRAN, 
                CLASS_ID_TYPE_MPI,                         
                &fileType );

            error = MPI_Type_commit( & fileType );

            error = MPI_File_set_view( 
                fileHandle, 
                0,
                CLASS_ID_TYPE_MPI, 
                fileType, 
                "native", 
                MPI_INFO_NULL );

            error = MPI_File_write_all(
                fileHandle, 
                m_voxelClassIdsPtr, 
                m_voxelClassIds.size(), 
                CLASS_ID_TYPE_MPI, 
                & status ); 

            error = MPI_Type_free( &fileType );
            MPI_File_close( &fileHandle );
        }

        if( m_logTimings )
        {
            MPI_Barrier( m_comm );
            if( m_rank == 0 )
            {
                auto stop = high_resolution_clock::now();
                auto duration = duration_cast<microseconds>(stop - start);

                m_logFile << "CCParallel writing CCIDs " 
                          << ( writeClassIds ? " and class ids " : "" ) 
                          << "to file took " 
                          << duration .count() / 1000000.0 << " s." << std::endl;
            }
        }
    }

    CC_ID_TYPE * ccid_ptr()
    {
        return m_ccVoxelIdsLocal.data();
    }

    CC_ID_TYPE numComponents()
    {
        return m_nc_global;
    }
};

}

#endif