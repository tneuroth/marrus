
#include "algorithms/CCTest.hpp"
#include "algorithms/ConnectedComponentsCPU.hpp"

#include <iostream>
#include <fstream>
#include <vector>
#include <array>
#include <cstdint>

using namespace std;

int main()
{
    array< int64_t, 3 > dims = { 300, 400, 200 };
    vector< int32_t > ccIds(      dims[ 0 ] * dims[ 1 ] * dims[ 2 ] );
    vector< int16_t > featureIds( dims[ 0 ] * dims[ 1 ] * dims[ 2 ] );

    ifstream inFile( "./cc_ids.example.0.bin", ios::in );
    inFile.read( ( char * ) ccIds.data(), ccIds.size() * sizeof( int32_t ) );
    inFile.close();

    inFile.open( "./feature_ids.example.0.bin", ios::in );
    inFile.read( ( char * ) featureIds.data(), featureIds.size() * sizeof( int16_t ) );
    inFile.close();

    const int MAX_CMPS = featureIds.size() / 10L;

    std::vector< int32_t > ccSerial( featureIds.size() );
    int nc;
    TN::connectedComponents( 
        featureIds,
        dims,   
        MAX_CMPS,
        ccSerial,
        nc );

    std::cout << "serial has " << nc << " unique components" << std::endl;

    bool valid = TN::verifyComponents(  
        ccIds,
        featureIds,
        dims,
        true );

    return 0;
}