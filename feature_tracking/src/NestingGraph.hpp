#ifndef TN_CONNECTED_COMPONENT_NESTING_GRAPH_HPP
#define TN_CONNECTED_COMPONENT_NESTING_GRAPH_HPP

namespace TN {

template< typename CC_ID_TYPE, typename CLASS_ID_TYPE >
class NestingGraph 
{
	std::vector< CLASS_ID_TYPE > m_classIds;

public:

	std::vector< CLASS_ID_TYPE > classIds() const
	{
		return m_classIds;
	}

	int64_t getEncapsulatingComponent( 
		const CC_ID_TYPE cc_id )
	{
		return 1;
	}
};

}

#endif