#ifndef TN_PARALLEL_FEATURE_TRACKING_WEIGHTED_RECTILINEAR_GRID_HPP
#define TN_PARALLEL_FEATURE_TRACKING_WEIGHTED_RECTILINEAR_GRID_HPP

#include "feature_tracking/ParallelFeatureTrackingBase.hpp"
#include "feature_tracking/CCParallel.hpp"
#include "utils/IndexUtils.hpp"
#include "utils/SimpleBlockDecompositionColumnOrder.hpp"

#include <mpi.h>
#include <unordered_map>
#include <cstdint>
#include <vector>
#include <string>
#include <array>

/*=====================================================================================

        The connected component ids are expected to be from a single feature
        class, meaning that if a component is adjacent to another, it is 
        considered connected.

        In a multi-class implementation, adjacent components could be considered
        disconnected if they have different class ids

======================================================================================*/

namespace TN {

template< typename CC_ID_TYPE, typename COORD_TYPE >
class ParallelRegionBasedFeatureTrackingWeightedRectilinearGrid : public ParallelFeatureTrackingBase< CC_ID_TYPE >
{
    std::vector< COORD_TYPE > m_cached_xCoords;
    std::vector< COORD_TYPE > m_cached_yCoords;
    std::vector< COORD_TYPE > m_cached_zCoords;

    std::array< COORD_TYPE, 2 > m_cached_xGhostCoords;
    std::array< COORD_TYPE, 2 > m_cached_yGhostCoords;
    std::array< COORD_TYPE, 2 > m_cached_zGhostCoords;
    TN::BlockDecomposition3D    m_cached_blockDecomposition;

    double ds( 
        const int64_t idx,
        const COORD_TYPE * coords,
        const std::array< COORD_TYPE, 2 > & ghostCoords, 
        const int64_t N )
    {
        const double a = idx > 0     ? coords[ idx - 1 ] : ghostCoords[ 0 ];
        const double b =               coords[ idx     ];
        const double c = idx < N - 1 ? coords[ idx + 1 ] : ghostCoords[ 1 ];
        return ( ( b - a ) / 2.0 ) + ( ( c - b ) / 2.0 );
    }

    double gridPointWeight( 
        const std::array< int64_t, 3 > & idx3,
        const COORD_TYPE * xCoords,
        const COORD_TYPE * yCoords,
        const COORD_TYPE * zCoords,
        const std::array< COORD_TYPE, 2 > & xGhostCoords,
        const std::array< COORD_TYPE, 2 > & yGhostCoords,
        const std::array< COORD_TYPE, 2 > & zGhostCoords,
         const std::array< int64_t, 3 > & dims )
    {
        return ds( idx3[ 0 ], xCoords, xGhostCoords, dims[ 0 ] )
             * ds( idx3[ 1 ], yCoords, yGhostCoords, dims[ 1 ] )
             * ds( idx3[ 2 ], zCoords, zGhostCoords, dims[ 2 ] );
    }

public : 

    /*===========================================================

                     If the grids are different

    ============================================================*/

    void computeLocal(
            const size_t N,
            const CC_ID_TYPE * cc_i,
            const CC_ID_TYPE * cc_j,
            const COORD_TYPE * xCoords_i,
            const COORD_TYPE * yCoords_i,
            const COORD_TYPE * zCoords_i,
            const COORD_TYPE * xCoords_j,
            const COORD_TYPE * yCoords_j,
            const COORD_TYPE * zCoords_j,
            const std::array< COORD_TYPE, 2 > & xGhostCoords_i,
            const std::array< COORD_TYPE, 2 > & yGhostCoords_i,
            const std::array< COORD_TYPE, 2 > & zGhostCoords_i,
            const std::array< COORD_TYPE, 2 > & xGhostCoords_j,
            const std::array< COORD_TYPE, 2 > & yGhostCoords_j,
            const std::array< COORD_TYPE, 2 > & zGhostCoords_j,
            const TN::BlockDecomposition3D & bc_i,
            const TN::BlockDecomposition3D & bc_j )
    {
        this->m_correspondenceMap.clear();

        // TODO

        this->m_from.clear();
        this->m_to.clear();
        this->m_weights.clear();

        // Extract the correspondences into contiguous array form 

        for( auto & source : this->m_correspondenceMap ) {
            for( auto & dst_count : source.second ) {
                this->m_from.push_back( source.first );
                this->m_to.push_back( dst_count.first );
                this->m_weights.push_back( dst_count.second );
            }
        }
    }


    /*===========================================================

                     If the grids are the same

    ============================================================*/

    void computeLocal(
            const CC_ID_TYPE * cc_i,
            const CC_ID_TYPE * cc_j,
            const COORD_TYPE * xCoords,
            const COORD_TYPE * yCoords,
            const COORD_TYPE * zCoords,
            const std::array< COORD_TYPE, 2 > & xGhostCoords,
            const std::array< COORD_TYPE, 2 > & yGhostCoords,
            const std::array< COORD_TYPE, 2 > & zGhostCoords,
            const TN::BlockDecomposition3D & bc )
    {
        this->m_correspondenceMap.clear();

        const int64_t N = bc.gridSize1D( this->m_rank );
        const auto bSZ = bc.localBlockSize( this->m_rank );
        const std::array< int64_t, 3 > localDims = { bSZ.x(), bSZ.y(), bSZ.z() };

        for( size_t idx = 0; idx < N; ++idx ) {

            // component id for t_i
            const CC_ID_TYPE C_I = cc_i[ idx ];

            // component id for t_j
            const CC_ID_TYPE C_J = cc_j[ idx ];

            // background     -> non-background
            // non-background -> background 
            // ...
            if( C_I == 0 || C_J == 0 ) { continue; }

            const auto idx3D    = TN::index3dCM( idx, localDims );
            const double weight = gridPointWeight( 
                idx3D, 
                xCoords, 
                yCoords, 
                zCoords,
                xGhostCoords,
                yGhostCoords,
                zGhostCoords,
                localDims );

            // Insert correspondence map if it doesn't exist
            if( ! this->m_correspondenceMap.count( C_I ) ) {
                this->m_correspondenceMap.insert( { C_I, {} } ); 
            }

            auto & mp = this->m_correspondenceMap.at( C_I );

            if( ! mp.count( C_J ) ) { 
                mp.insert( { C_J, weight } ); 
            } else {
                mp.at( C_J ) += weight;
            }
        }

        this->m_from.clear();
        this->m_to.clear();
        this->m_weights.clear();

        // Extract the correspondences into contiguous array form 

        for( auto & source : this->m_correspondenceMap ) {
            for( auto & dst_count : source.second ) {
                this->m_from.push_back( source.first );
                this->m_to.push_back( dst_count.first );
                this->m_weights.push_back( dst_count.second );
            }
        }
    }

    void onStep(
            const CC_ID_TYPE * cc_i,
            const CC_ID_TYPE * cc_j,
            const COORD_TYPE * xCoords_i,
            const COORD_TYPE * yCoords_i,
            const COORD_TYPE * zCoords_i,
            const COORD_TYPE * xCoords_j,
            const COORD_TYPE * yCoords_j,
            const COORD_TYPE * zCoords_j,
            const std::array< COORD_TYPE, 2 > & xGhostCoords_i,
            const std::array< COORD_TYPE, 2 > & yGhostCoords_i,
            const std::array< COORD_TYPE, 2 > & zGhostCoords_i,
            const std::array< COORD_TYPE, 2 > & xGhostCoords_j,
            const std::array< COORD_TYPE, 2 > & yGhostCoords_j,
            const std::array< COORD_TYPE, 2 > & zGhostCoords_j,
            const TN::BlockDecomposition3D & bc_i,
            const TN::BlockDecomposition3D & bc_j )
    {
        using namespace std::chrono;

        if( this->m_logTimings == true )
        {
            MPI_Barrier( this->m_comm );
        }

        auto start = high_resolution_clock::now();

        // If the grid and block decomposition is the same for both time steps, use the simple method

        bool gridAndBlockDecompositionIdentical = ( bc_i == bc_j );
        if( bc_i == bc_j ) 
        {
            const auto bSZ = bc_i.localBlockSize( this->m_rank );
            const std::array< int64_t, 2 > D = { bSZ.x(), bSZ.y(), bSZ.z() };

            gridAndBlockDecompositionIdentical = 
                TN::arraysEqual( xCoords_i, xCoords_j, D[ 0 ] ) && ( xGhostCoords_i == xGhostCoords_j )
             && TN::arraysEqual( yCoords_i, yCoords_j, D[ 1 ] ) && ( yGhostCoords_i == yGhostCoords_j )
             && TN::arraysEqual( zCoords_i, zCoords_j, D[ 2 ] ) && ( zGhostCoords_i == zGhostCoords_j );
        }


        if( gridAndBlockDecompositionIdentical ) 
        {
            computeLocal(
                cc_i,
                cc_j,
                xCoords_i,
                yCoords_i,
                zCoords_i,
                xGhostCoords_i,
                yGhostCoords_i,
                zGhostCoords_i,
                bc_i );
        }

        // Otherwise we need to do something more complicated

        else
        {
            computeLocal( 
                cc_i, 
                cc_j,
                xCoords_i,
                yCoords_i,
                zCoords_i,
                xCoords_j,
                yCoords_j,
                zCoords_j,
                xGhostCoords_i,
                yGhostCoords_i,
                zGhostCoords_i,
                xGhostCoords_j,
                yGhostCoords_j,
                zGhostCoords_j,
                bc_i,
                bc_j );
        }

        auto stopLocal = high_resolution_clock::now();

        if( this->m_logTimings == true ) {
            MPI_Barrier( this->m_comm );
        }

        auto startReduce = high_resolution_clock::now();
        
        this->reduce();
        
        auto stopTotal = high_resolution_clock::now();

        if( this->m_logTimings == true )
        {
            MPI_Barrier( this->m_comm );
            if( this->m_rank == 0 )
            {
                auto durationLocal = duration_cast<microseconds>(stopLocal - start);
                auto durationReduction = duration_cast<microseconds>(stopTotal - startReduce);
                auto durationTotal = duration_cast<microseconds>(stopTotal - start);

                this->m_logFile << "ParallelFeatureTracking local Computation took " <<     durationLocal.count() / 1000000.0 << " s." << std::endl;
                this->m_logFile << "ParallelFeatureTracking reduction took "         << durationReduction.count() / 1000000.0 << " s." << std::endl;
                this->m_logFile << "ParallelFeatureTracking total computation took " <<     durationTotal.count() / 1000000.0 << " s." << std::endl;
            }
        }
    }

    void cacheStep( 
        const CC_ID_TYPE * cc_j,
        const COORD_TYPE * xCoords,
        const COORD_TYPE * yCoords,
        const COORD_TYPE * zCoords,
        const std::array< COORD_TYPE, 2 > & xGhostCoords,
        const std::array< COORD_TYPE, 2 > & yGhostCoords,
        const std::array< COORD_TYPE, 2 > & zGhostCoords,
        const TN::BlockDecomposition3D & bc )
    {
        const int64_t N = bc.gridSize1D( this->m_rank );
        const auto bSZ = bc.localBlockSize( this->m_rank );
        const std::array< int64_t, 3 > localDims = { bSZ.x(), bSZ.y(), bSZ.z() };

        this->m_cached_ccids.resize( N );
        
        for( size_t idx = 0; idx < N; ++idx ) {
            this->m_cached_ccids[ idx ] = cc_j[ idx ];
        }

        this->m_cached_xCoords.resize( localDims[ 0 ] );
        this->m_cached_yCoords.resize( localDims[ 1 ] );        
        this->m_cached_zCoords.resize( localDims[ 2 ] );

        for( int64_t x = 0, end = this->m_cached_xCoords.size(); x < end; ++x )
        {
            this->m_cached_xCoords[ x ] = xCoords[ x ];
        }

        for( int64_t y = 0, end = this->m_cached_yCoords.size(); y < end; ++y )
        {
            this->m_cached_yCoords[ y ] = yCoords[ y ];
        }

        for( int64_t z = 0, end = this->m_cached_zCoords.size(); z < end; ++z )
        {
            this->m_cached_zCoords[ z ] = xCoords[ z ];
        }

        this->m_cached_xGhostCoords = xGhostCoords;
        this->m_cached_yGhostCoords = yGhostCoords;
        this->m_cached_zGhostCoords = zGhostCoords;

        this->m_cached_blockDecomposition = bc;
        
        this->m_previousStepCached = true;
    }

    void exchangeGhostCoordinates(
        std::array< COORD_TYPE, 2 > & xCoordsG,
        std::array< COORD_TYPE, 2 > & yCoordsG,        
        std::array< COORD_TYPE, 2 > & zCoordsG,
        const COORD_TYPE * xCoords,
        const COORD_TYPE * yCoords,
        const COORD_TYPE * zCoords,
        const TN::BlockDecomposition3D & bc )
    {

        /*==============================================================

                         Get ghost coord 0 (left/back/down)

        ===============================================================*/

        MPI_Status status;

        const auto bIdx  = bc.bIdx3D( this->m_rank );
        const auto bDims = bc.localBlockSize( this->m_rank );

        if( bIdx.x() > 0 ) {
            
            const int recieverRank = TN::flatIndex3dCM( 
                 bIdx.x() - 1,
                 bIdx.y(),
                 bIdx.z(), 
                 bDims.x(), 
                 bDims.y(),
                 bDims.z() );

            // send my first coordinate to the block to the left

            int s = MPI_Send(
                xCoords.data(),
                1, 
                this->CC_ID_TYPE_MPI,
                recieverRank, 
                0,
                this->m_comm );
        }
        else {
            xCoordsG[ 0 ] = xCoords[ 0 ];
        }

        if( bIdx.x() > 0 ) {

            const int recieverRank = TN::flatIndex3dCM( 
                 bIdx.x(),
                 bIdx.y() - 1,
                 bIdx.z(), 
                 bDims.x(), 
                 bDims.y(),
                 bDims.z() );

            // send my first coordinate to the block to the left

            int s = MPI_Send(
                yCoords.data(),
                1, 
                this->CC_ID_TYPE_MPI,
                recieverRank, 
                0,
                this->m_comm );

        }
        else {
            yCoordsG[ 0 ] = yCoords[ 0 ];
        }

        if( bIdx.z() > 0 ) {

            const int recieverRank = TN::flatIndex3dCM( 
                 bIdx.x(),
                 bIdx.y(),
                 bIdx.z() - 1, 
                 bDims.x(), 
                 bDims.y(),
                 bDims.z() );

            // send my first coordinate to the block to the left

            int s = MPI_Send(
                zCoords.data(),
                1, 
                this->CC_ID_TYPE_MPI,
                recieverRank, 
                0,
                this->m_comm );
        }
        else {
            zCoordsG[ 0 ] = zCoords[ 0 ];            
        }


        /*==============================================================

                         Get ghost coord 1 (right/forward/up)

        ===============================================================*/

        if( bIdx.x() < bDims.x() ) {

            const int senderRank = TN::flatIndex3dCM( 
                 bIdx.x() + 1,
                 bIdx.y(),
                 bIdx.z(), 
                 bDims.x(), 
                 bDims.y(),
                 bDims.z() );

            // recieve the first coordinate of the block to the right
            int s = MPI_Recv(
                xCoordsG.data() + 1,
                1, 
                this->CC_ID_TYPE_MPI,
                senderRank, 
                0,
                this->m_comm,
                & status );

        }
        else {
            xCoordsG[ 1 ] = xCoords[ bDims.x() - 1 ];      
        }

        if( bIdx.y() < bDims.y() ) {

            const int senderRank = TN::flatIndex3dCM( 
                 bIdx.x(),
                 bIdx.y() + 1,
                 bIdx.z(), 
                 bDims.x(), 
                 bDims.y(),
                 bDims.z() );

            // recieve the first coordinate of the block to the right
            int s = MPI_Recv(
                yCoordsG.data() + 1,
                1, 
                this->CC_ID_TYPE_MPI,
                senderRank, 
                0,
                this->m_comm,
                & status );
        }
        else {
            yCoordsG[ 1 ] = yCoords[ bDims.y() - 1 ];      
        }

        if( bIdx.z() < bDims.z() ) {

            const int senderRank = TN::flatIndex3dCM( 
                 bIdx.x(),
                 bIdx.y(),
                 bIdx.z() + 1, 
                 bDims.x(), 
                 bDims.y(),
                 bDims.z() );

            // recieve the first coordinate of the block to the right
            int s = MPI_Recv(
                zCoordsG.data() + 1,
                1, 
                this->CC_ID_TYPE_MPI,
                senderRank, 
                0,
                this->m_comm,
                & status );
        }
        else {
            zCoordsG[ 1 ] = zCoords[ bDims.z() - 1 ];      
        }
    }

    void onStep(
            const CC_ID_TYPE * cc_ids,            
            const COORD_TYPE * xCoords,
            const COORD_TYPE * yCoords,
            const COORD_TYPE * zCoords,
            const std::array< COORD_TYPE, 2 > & xGhostCoords,
            const std::array< COORD_TYPE, 2 > & yGhostCoords,
            const std::array< COORD_TYPE, 2 > & zGhostCoords,
            const TN::BlockDecomposition3D    & blockDecomposition )
    {
        if( this->m_previousStepCached ) {

            onStep( 
                
                this->m_cached_ccids.data(), 
                cc_ids, 

                this->m_cached_xCoords.data(),
                this->m_cached_yCoords.data(),
                this->m_cached_zCoords.data(),
                xCoords,
                yCoords,
                zCoords,
                
                this->m_cached_xGhostCoords,
                this->m_cached_yGhostCoords,
                this->m_cached_zGhostCoords,
                xGhostCoords,
                yGhostCoords,
                zGhostCoords,

                this->m_cached_blockDecomposition,
                blockDecomposition );
        }

        cacheStep(
            cc_ids, 
            xCoords,
            yCoords,
            zCoords,
            xGhostCoords,
            yGhostCoords,
            zGhostCoords,
            blockDecomposition );
    }

    void onStep(
            const CC_ID_TYPE * cc,            
            const COORD_TYPE * xCoords,
            const COORD_TYPE * yCoords,
            const COORD_TYPE * zCoords,
            const TN::BlockDecomposition3D & blockDecomposition )
    {

        // No ghost coordinates, have to communicate them between ranks

        std::array< COORD_TYPE, 2 > xCoordsG;
        std::array< COORD_TYPE, 2 > yCoordsG;        
        std::array< COORD_TYPE, 2 > zCoordsG;

        exchangeGhostCoordinates( 
            xCoordsG,
            yCoordsG,
            zCoordsG,
            xCoords,
            yCoords,
            zCoords,
            blockDecomposition );

        onStep( 
            cc,            
            xCoords,
            yCoords,
            zCoords,
            xCoordsG,
            yCoordsG,
            zCoordsG,
            blockDecomposition );
    }
};

}

#endif