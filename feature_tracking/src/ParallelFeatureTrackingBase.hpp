#ifndef TN_PARALLEL_FEATURE_TRACKING_BASE_HPP
#define TN_PARALLEL_FEATURE_TRACKING_BASE_HPP

#include "CCParallel.hpp"

#include <mpi.h>
#include <unordered_map>
#include <cstdint>
#include <vector>
#include <string>

/*=====================================================================================

        The connected component ids are expected to be from a single feature
        class, meaning that if a component is adjacent to another, it is 
        considered connected.

        In a multi-class implementation, adjacent components could be considered
        disconnected if they have different class ids

======================================================================================*/

namespace TN {

template< typename CC_ID_TYPE >
class ParallelFeatureTrackingBase
{

protected: 

    MPI_Comm m_comm;
    int m_rank;
    int m_nRanks;

    std::vector< CC_ID_TYPE > m_cached_ccids;
    bool m_previousStepCached;

    std::unordered_map< CC_ID_TYPE, std::unordered_map< CC_ID_TYPE, double > > m_correspondenceMap;
    
    std::vector< CC_ID_TYPE > m_from;
    std::vector< CC_ID_TYPE > m_to;
    std::vector< double >     m_weights;

    MPI_Datatype CC_ID_TYPE_MPI;

    std::ofstream m_logFile;
    bool m_logTimings;

public : 

    ParallelFeatureTrackingBase() : m_previousStepCached( false )
    {
        static_assert( 
            ( std::is_integral< CC_ID_TYPE >::value ) && sizeof( CC_ID_TYPE ) >= 4 && sizeof( CC_ID_TYPE ) <= 8,
            "Error in ParallelFeatureTracking: template parameter, component id type, must be integral and have size of 4 bytes or 8 bytes." );

        CC_ID_TYPE_MPI = sizeof( CC_ID_TYPE ) == 4 ? MPI_INT32_T : MPI_INT64_T;

        m_logFile.open( "ParallelFeatureTracking.log.txt" );
    }

    virtual ~ParallelFeatureTrackingBase() 
    {
        m_logFile.close();
    }

    virtual void init( int nRanks, int rank, MPI_Comm comm, bool logTimings )
    {
        m_rank               = rank;
        m_nRanks             = nRanks;
        m_comm               = comm;
        m_logTimings         = logTimings;  
        m_previousStepCached = false;
    }

    virtual void reduce()
    {
        MPI_Status status;

        int m = 0;
        int offset = 1;

        // gather the correspondences from each rank on rank 0

        int totalLength = 0;

        std::vector< int > lengths( m_nRanks );
        std::vector< int > offsets( m_nRanks, 0 );

        // gather lengths of each rank's tables

        int myLength = (int) m_from.size();
        int error = MPI_Gather(
            & myLength, 
            1, 
            MPI_INT,
            lengths.data(), 
            1,
            MPI_INT,
            0,
            m_comm );

        // compute offsets

        if( m_rank == 0 )
        {
            for( int i = 0; i < m_nRanks; ++i )
            {
                offsets[ i ] = totalLength; 
                totalLength += lengths[ i ]; 
            }
        }

        // gather all tables onto rank 0

        std::vector< CC_ID_TYPE > all_from( totalLength );
        error = MPI_Gatherv(
            m_from.data(), 
            m_from.size(), 
            CC_ID_TYPE_MPI,
            all_from.data(), 
            lengths.data(), 
            offsets.data(),
            CC_ID_TYPE_MPI,
            0,
            m_comm );

        std::vector< CC_ID_TYPE > all_to( totalLength );
        error = MPI_Gatherv(
            m_to.data(), 
            m_to.size(), 
            CC_ID_TYPE_MPI,
            all_to.data(), 
            lengths.data(), 
            offsets.data(),
            CC_ID_TYPE_MPI,
            0, 
            m_comm );

        std::vector< double > all_counts( totalLength );
        error = MPI_Gatherv(
            m_weights.data(), 
            m_weights.size(), 
            MPI_DOUBLE,
            all_counts.data(), 
            lengths.data(), 
            offsets.data(),
            MPI_DOUBLE,
            0, 
            m_comm );

        // merge them 

        if( m_rank == 0 )
        {
            for( int i = 1; i < m_nRanks; ++i )
            {
                const size_t OFFSET = offsets[ i ];
                const size_t N = lengths[ i ];

                for( size_t i = 0; i < N; ++i )
                {
                    if( ! m_correspondenceMap.count(  all_from[ OFFSET + i ] ) ) {
                        m_correspondenceMap.insert( { all_from[ OFFSET + i ], {} } ); 
                    }
                    auto & mp = m_correspondenceMap.at( all_from[ OFFSET + i ] );

                    if( mp.count( all_to[ OFFSET + i ] ) )
                    {
                        mp.at( all_to[ OFFSET + i ] ) += all_counts[ OFFSET + i ];
                    }
                    else
                    {
                        mp.insert( { all_to[ OFFSET + i ], all_counts[ OFFSET + i ] } ); 
                    }
                }
            }

            m_from.clear();
            m_to.clear();
            m_weights.clear();

            // // Extract the correspondences into contiguous array form 

            for( auto & source : m_correspondenceMap ) {
                for( auto & dst_count : source.second ) {
                    m_from.push_back( source.first );
                    m_to.push_back( dst_count.first );
                    m_weights.push_back( dst_count.second );
                }
            }
        }

        // send the merged results to each rank

        int64_t nSend = m_from.size();

        error = MPI_Bcast(
            & nSend,
            1,
            MPI_INT64_T,
            0,
            m_comm );

        if( m_rank != 0 )
        {
            m_from.resize(    nSend );
            m_to.resize(      nSend );
            m_weights.resize( nSend );
        }

        error = MPI_Bcast(
            m_from.data(),
            nSend,
            CC_ID_TYPE_MPI,
            0,
            m_comm );

        error = MPI_Bcast(
            m_to.data(),
            nSend,
            CC_ID_TYPE_MPI,
            0,
            m_comm );

        error = MPI_Bcast(
            m_weights.data(),
            nSend,
            MPI_DOUBLE,
            0,
            m_comm );

        if( m_rank != 0 )
        {
            m_correspondenceMap.clear();
            for( size_t i = 0; i < nSend; ++i )
            {
                if( ! m_correspondenceMap.count(  m_from[ i ] ) ) {
                    m_correspondenceMap.insert( { m_from[ i ], {} } ); 
                }
                auto & mp = m_correspondenceMap.at( m_from[ i ] );

                if( mp.count( m_to[ i ] ) )
                {
                    mp.at( m_to[ i ] ) += m_weights[ i ];
                }
                else
                {
                    mp.insert( { m_to[ i ], m_weights[ i ] } ); 
                }
            }
        }
    }

    virtual void writeGlobal( 
        const std::string & directory, 
        const std::string & identifier,  
        int64_t timestep )
    {
        using namespace std::chrono;

        auto start = high_resolution_clock::now();

        MPI_File fileHandle;
        MPI_Info info = MPI_INFO_NULL;
        MPI_Status status;

        std::string tstr   = std::to_string( timestep );
        std::string prefix = directory + "/tracking." + identifier + ".";
        std::string path   = prefix + tstr + ".from.bin";

        int error = MPI_File_open(
            MPI_COMM_WORLD, 
            path.c_str(), MPI_MODE_CREATE | MPI_MODE_WRONLY, 
            info, 
            &fileHandle );

        // each rank have a copy of the same data, and will write one portion of it collectively

        size_t WRSIZE = std::ceil( (double) m_from.size() / m_nRanks );
        size_t OFFSET = WRSIZE * m_rank;
        if( m_rank == m_nRanks - 1 ) {
            WRSIZE = m_from.size() - OFFSET;
        }

        error = MPI_File_write_all(
            fileHandle, 
            m_from.data() + OFFSET, 
            WRSIZE, 
            CC_ID_TYPE_MPI, 
            & status ); 

        MPI_File_close( &fileHandle );

        path = prefix + tstr + ".to.bin";
        error = MPI_File_open(
            MPI_COMM_WORLD, 
            path.c_str(), MPI_MODE_CREATE | MPI_MODE_WRONLY, 
            info, 
            &fileHandle );

        error = MPI_File_write_all(
            fileHandle, 
            m_to.data() + OFFSET, 
            WRSIZE, 
            CC_ID_TYPE_MPI, 
            & status ); 

        MPI_File_close( &fileHandle );

        path = prefix + tstr + ".counts.bin";
        error = MPI_File_open(
            MPI_COMM_WORLD, 
            path.c_str(), MPI_MODE_CREATE | MPI_MODE_WRONLY, 
            info, 
            &fileHandle );

        error = MPI_File_write_all(
            fileHandle, 
            m_weights.data() + OFFSET, 
            WRSIZE, 
            MPI_DOUBLE, 
            & status ); 

        MPI_File_close( &fileHandle );

        auto stop = high_resolution_clock::now();
        if( m_logTimings == true )
        {
            MPI_Barrier( m_comm );
            if( m_rank == 0 )
            {
                auto duration = duration_cast<microseconds>(stop - start);
                m_logFile << "ParallelFeatureTracking correspondence writing took " 
                          << duration.count() / 1000000.0 << " s." << std::endl;
            }
        }
    }

    virtual std::unordered_map< CC_ID_TYPE, std::unordered_map< CC_ID_TYPE, double > > & correspondenceMap()
    {
        return m_correspondenceMap;
    }
};

}

#endif