mpic++ -O3 -fopenmp -I../common/ -I../ CCParallelExample.cpp -o ccparallel
g++ -O3 -I../common/ -I../ CCParallelTest.cpp -o cctest
mpic++ -O3 -fopenmp -I../common/  -I../ ParallelFeatureTrackingTest.cpp -o ftrack
