import numpy as np
import matplotlib.pyplot as plt

dims = ( 100, 300, 200 )

data  = np.fromfile( "../test_data/test_data_3D/ts.5.6.data.bin", dtype='f4' )
Temp  = data[ 0 : 100*300*200 ].reshape( dims, order='F' )

plane = np.zeros( ( 8, 200, 200 ), dtype='f4' )
for i in range( 8 ): 
	for x in range( 200 ) :
		for y in range( 200 ):
			plane[ i, x, y ] = Temp[ 0, x+100, y ]

# plt.imshow( plane )
# plt.show()

plane.tofile( "../test_data/test_data_3D/temp_slice.200.200.8.bin" )