import numpy as np

D = ( 512, 512, 512 )

input_dir = "/home/ubuntu/dev/marrus/multiscale_data/512/data/padded/"

##################################################

# reflected_lo   = np.fromfile( input_dir + "reflected.512-64.bin"  , dtype='f4' ).reshape( D )
x   = np.fromfile( input_dir + "reflected.64-2.bin"    , dtype='f4' ).reshape( D )
# symmetric_lo   = np.fromfile( input_dir + "symmetric.512-64.bin"  , dtype='f4' ).reshape( D )
# symmetric_hi   = np.fromfile( input_dir + "symmetric.64-2.bin"    , dtype='f4' ).reshape( D )
# reflected_lo_0 = np.fromfile( input_dir + "reflected_0.512-64.bin", dtype='f4' ).reshape( D )
# reflected_hi_0 = np.fromfile( input_dir + "reflected_0.64-2.bin"  , dtype='f4' ).reshape( D )
# symmetric_lo_0 = np.fromfile( input_dir + "symmetric_0.512-64.bin", dtype='f4' ).reshape( D )
# symmetric_hi_0 = np.fromfile( input_dir + "symmetric_0.64-2.bin"  , dtype='f4' ).reshape( D )

# ##############################################################

# reflected_lo = reflected_lo[ -256:, -256:, -256: ]
# reflected_hi = reflected_hi[ -256:, -256:, -256: ]

# symmetric_lo = symmetric_lo[ -256:, -256:, -256: ]
# symmetric_hi = symmetric_hi[ -256:, -256:, -256: ]

# reflected_lo_0 = reflected_lo_0[ -256:, -256:, -256: ]
# reflected_hi_0 = reflected_hi_0[ -256:, -256:, -256: ]

# symmetric_lo_0 = symmetric_lo_0[ -256:, -256:, -256: ]
# symmetric_hi_0 = symmetric_hi_0[ -256:, -256:, -256: ]

# ########################################################

# reflected_lo  .tofile( input_dir + "extracted_reflected.512-64.bin"   )
# reflected_hi  .tofile( input_dir + "extracted_reflected.64-2.bin"     )
# symmetric_lo  .tofile( input_dir + "extracted_symmetric.512-64.bin"   )
# symmetric_hi  .tofile( input_dir + "extracted_symmetric.64-2.bin"     )
# reflected_lo_0.tofile( input_dir + "extracted_reflected_0.512-64.bin" )
# reflected_hi_0.tofile( input_dir + "extracted_reflected_0.64-2.bin"   )
# symmetric_lo_0.tofile( input_dir + "extracted_symmetric_0.512-64.bin" )
# symmetric_hi_0.tofile( input_dir + "extracted_symmetric_0.64-2.bin"   )

# x = np.fromfile( input_dir + "zeropadded.64-2.bin.bin"  , dtype='f4' ).reshape( D )
x = x[ -256:, -256:, -256: ]
x.tofile( input_dir + "ex.zeropadded.64-2.bin" )