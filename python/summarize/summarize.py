import numpy as np
import sys
import json
import pandas as pd
import os
import copy

def skewness( x, mean_x, std_x ):
	return np.mean( ( ( x - mean_x ) / std_x ) ** 3 )

def kurtosis( x, mean_x, std_x ):
	return np.mean( ( ( x - mean_x ) / std_x ) ** 4 )

def covariance( x, y, mean_x, mean_y ):
	return np.mean( ( x - mean_x ) * ( y - mean_y ) )

def co_skewness( x, y, mean_x, mean_y, std_x, std_y ):
	return np.mean( ( ( x - mean_x ) / std_x ) ** 2 * ( ( y - mean_y ) / std_y ) )

def co_kurtosis( x, y, mean_x, mean_y, std_x, std_y ):
	return np.mean( ( ( x - mean_x ) / std_x ) ** 3 * ( ( y - mean_y ) / std_y ) )

def load_variable( variable, count ) :

	# only handles float or double for now
	type = "f4" if variable[ "type" ] == "float" else "f8"

	try:

		# open the binary file for reading
		with open( variable["file"], "rb" ) as f:

			offset = int(variable["offset"])
			typesz = 4 if type == "f4" else 8

			# seek to byte offset where this variable begins
			f.seek(offset * count * typesz, os.SEEK_SET)

			# load the grid point values for just this one variable
			values = np.fromfile(f, dtype=type, count=count)
		
		return values

	except FileNotFoundError:
		print(f"Error: The file {variable['file']} was not found.")
		return None
	except IOError as e:
		print(f"Error: Unable to open the file {variable['file']}. {e}")
		return None

def summarize( variables, summary, tessellation_directory ) :
	
	output_dir = tessellation_directory + "/summary/data/"
	base_dir   = tessellation_directory + "/summary/"

	metadata = copy.deepcopy( summary )

	####################################################################################################

	# Load tessellation data

	meta = np.fromfile( f"{tessellation_directory}/n_lyr_comp_sites_x_y_z.bin", dtype='int64' )

	N_LAYERS     = meta[ 0 ]
	N_COMPONENTS = meta[ 1 ]
	N_SITES      = meta[ 2 ]
	DIMS         = [ meta[ 3 ], meta[ 4 ], meta[ 5 ] ]
	N_VOXELS     = DIMS[ 0 ] * DIMS[ 1 ] * DIMS[ 2 ]
	COVERED      = False if meta[ 6 ] != 0 else True

	voxelSiteIds     = np.fromfile( f"{tessellation_directory}/voxelSiteIds.bin",     dtype='int32' )
	sites            = np.fromfile( f"{tessellation_directory}/sitePoints.bin",       dtype='f4'    )
	siteLayerIds     = np.fromfile( f"{tessellation_directory}/siteLayers.bin",       dtype='int16' )
	siteComponentIds = np.fromfile( f"{tessellation_directory}/siteComponentIds.bin", dtype='int32' )
	layers           = np.fromfile( f"{tessellation_directory}/layers.bin",           dtype='f4'    )

	if not COVERED :
		siteLayerIds     = siteLayerIds     - 1
		siteComponentIds = siteComponentIds - 1

	# Add some info to the metadata

	if "bands" in metadata :
		
		metadata[ "bands" ][ "band_edges" ] = [ 
			[ float( layers[ i ] ), float(layers[ i*2 + 1 ] ) ] 
			for i in range( N_LAYERS ) ]

		metadata[ "bands" ][ "count" ] = int( N_LAYERS )

	if "components" in metadata :
		metadata[ "components" ][ "count" ] = int( N_COMPONENTS )

	if "cells" in metadata :
		metadata[ "cells" ][ "count" ] = int( N_SITES )

	################################################################################################

	# convert to sparse representation and get the voxel component and layer ids ######################

	indices = np.where( voxelSiteIds >= 0 )[ 0 ]
	
	cell_ids = voxelSiteIds[ indices ]
	comp_ids = np.array( [ siteComponentIds[ site ] for site in cell_ids ] )
	band_ids = np.array( [     siteLayerIds[ site ] for site in cell_ids ] )

	group_sizes = {
		"cells"      : np.max( cell_ids ) + 1,
		"components" : np.max( comp_ids ) + 1,
		"bands"      : np.max( band_ids ) + 1
	}

	####################################################################################################

	df = pd.DataFrame({
		'cells'      : cell_ids,
		'components' : comp_ids,
		'bands'      : band_ids,
		'indices'    : np.arange( 0, cell_ids.size )
	})

	for group_key in summary : 

		if not group_key in { "cells", "components", "bands" } :
			print( "Invalid grouping key for summarization: " + group_key )
			exit( 1 )

		if not summary[ group_key ] :
			continue

		grouped     = df.groupby( group_key )
		group_size = group_sizes[ group_key ]

		if "moments" in summary[ group_key ] :

			print( "\ncomputing moments for the " + group_key + ", " + str( group_size ) + " regions\n" )

			moments = summary[ group_key ][ "moments" ]

			for v_idx in range( len( moments ) ) :

				v = moments[ v_idx ]

				print( "v = " + v )

				values = load_variable( variables[ v ], N_VOXELS )[ indices ]
				
				mean = np.zeros( group_size, dtype="f4" )
				sdev = np.zeros( group_size, dtype="f4" )
				skew = np.zeros( group_size, dtype="f4" )
				kurt = np.zeros( group_size, dtype="f4" )

				for group, group_data in grouped :

					grouped_values = values[ group_data.indices ]

					if grouped_values.size <= 0 :
						continue

					if group >= group_size : 
						print( group )
						print( "out of bounds" )
						continue

					mean[ group ] = np.mean( grouped_values ) 
					sdev[ group ] = np.std(  grouped_values ) 

					if grouped_values.size > 2 :
						skew[ group ] = skewness( grouped_values, mean[  group ], sdev[ group ] ) 
						skew[ group ] = kurtosis( grouped_values, mean[  group ], sdev[ group ] ) 

				var = np.square( sdev )

				# write to disk ...

				s = metadata[ group_key ][ "moments" ]
				s[ v_idx ] = { "variable" : v }

				s[ v_idx ][ "files" ] = {}

				s[ v_idx ][ "type" ] = "float"
				s[ v_idx ][ "files" ][ "mean"     ] = group_key + ".mean."     +  v + ".bin"
				s[ v_idx ][ "files" ][ "variance" ] = group_key + ".variance." +  v + ".bin"
				s[ v_idx ][ "files" ][ "skewness" ] = group_key + ".skewness." +  v + ".bin"
				s[ v_idx ][ "files" ][ "kurtosis" ] = group_key + ".kurtosis." +  v + ".bin"

				mean.tofile( output_dir + "/" + s[ v_idx ][ "files" ]["mean"    ] )
				var .tofile( output_dir + "/" + s[ v_idx ][ "files" ]["variance"] )
				skew.tofile( output_dir + "/" + s[ v_idx ][ "files" ]["skewness"] )
				kurt.tofile( output_dir + "/" + s[ v_idx ][ "files" ]["kurtosis"] )

		if "co-moments" in summary[ group_key ] :

			print( "\ncomputing co-moments for the " + group_key + ", " + str( group_size ) + " regions\n" )

			co_moments = summary[ group_key ][ "co-moments" ]

			for cm_idx in range( len( co_moments ) ) :

				cm = co_moments[ cm_idx ]

				print( "(x,y) = " + str( cm ) ) 

				x_values = load_variable( variables[ cm[ 0 ] ], N_VOXELS )[ indices ]
				y_values = load_variable( variables[ cm[ 1 ] ], N_VOXELS )[ indices ]

				covar  = np.zeros( N_SITES, dtype="f4" )
				coskew = np.zeros( N_SITES, dtype="f4" )
				cokurt = np.zeros( N_SITES, dtype="f4" )

				for group, group_data in grouped :
				
					grouped_x_values = x_values[ group_data.indices ]
					grouped_y_values = y_values[ group_data.indices ]

					if grouped_x_values.size <= 0 :
						continue

					mean_x = np.mean( grouped_x_values )
					mean_y = np.mean( grouped_y_values )
					
					sdev_x = np.std( grouped_values ) 
					sdev_y = np.std( grouped_values ) 

					covar[ group ] = covariance( 
						grouped_x_values, 
						grouped_y_values, 
						mean_x,
						mean_y )

					if grouped_values.size > 1 :

						coskew[ group ] = co_skewness( 
							grouped_x_values, 
							grouped_y_values, 
							mean_x,
							mean_y,
							sdev_x,
							sdev_y )

						cokurt[ group ] = co_kurtosis( 
							grouped_x_values, 
							grouped_y_values, 
							mean_x,
							mean_y,
							sdev_x,
							sdev_y )

				s = metadata[ group_key ][ "co-moments" ]
				s[ cm_idx ] = { "variables" : cm }

				string_id = ".".join( cm )

				s[ cm_idx ][ "type" ] = "float"
				s[ cm_idx ][ "files" ] = {}
				s[ cm_idx ][ "files" ][ "covariance" ] = group_key + ".covariance." +  string_id + ".bin"
				s[ cm_idx ][ "files" ][ "coskewness" ] = group_key + ".coskewness." +  string_id + ".bin"
				s[ cm_idx ][ "files" ][ "cokurtosis" ] = group_key + ".cokurtosis." +  string_id + ".bin"

				covar .tofile( output_dir + "/" + s[ cm_idx ][ "files" ][ "covariance" ] )
				coskew.tofile( output_dir + "/" + s[ cm_idx ][ "files" ][ "coskewness" ] )
				cokurt.tofile( output_dir + "/" + s[ cm_idx ][ "files" ][ "cokurtosis" ] )

		if "histograms" in summary[ group_key ] :

			print( "\ncomputing histograms for the " + group_key + ", " + str( group_size ) + " regions\n" )

			for hist_idx in range( len( summary[ group_key ][ "histograms" ] ) ) :

				hist = summary[ group_key ][ "histograms" ][ hist_idx ]
				ndims = len( hist[ "dims" ] )

				print( "variables=" + str( hist[ "variables" ] ) ) 

				if ndims < 1 or ndims > 3 :
					raise ValueError( "Number of histogram dimensions must be 1, 2, or 3" )

				# Load values for each variable in the histogram
				values = [ load_variable( variables[ v ], N_VOXELS )[ indices ] for v in hist[ "variables" ] ]
				result = np.zeros( ( group_size, np.prod( hist[ "dims" ] ) ), dtype = "f4" )

				# Initialize edges variable for uniform bin spacing
				ranges = None
				band_ranges = None

				# If using 'bands', calculate per-band edges
				if hist[ "edges" ] == "bands" :

					band_grouped = df.groupby( "bands" )

					band_ranges = [ [
						( np.min( values[ idx ][ gd[ "indices" ] ] ), 
						  np.max( values[ idx ][ gd[ "indices" ] ] ) )
						for idx in range( ndims ) ]
							for _, gd in band_grouped ]

				else :

					ranges = [ ( np.min( values[ idx ] ), np.max( values[ idx ] ) ) for idx in range( ndims ) ]

				# Histogram calculation for each group
				for group, group_data in grouped :

					group_indices = group_data[ "indices" ]
					
					if len( indices ) == 0 :
						continue

					bin_ranges = None

					# Determine which edges to use: band-specific or global
					if hist[ "edges" ] == "bands" :
						band_idx = group_data[ "bands" ].iloc[ 0 ]
						bin_ranges = band_ranges[ band_idx ]  # Use band-specific edges
					else :
						bin_ranges = ranges  # Use global edges

					# Prepare variables for histogram binning
					var_data = [ values[ idx ][ group_indices ] for idx in range( len( hist[ "variables" ] ) ) ]

					# Use numpy's histogramdd to handle multidimensional histograms
					hist_result, _ = np.histogramdd( var_data, bins=hist[ "dims" ], range=bin_ranges )
					binned_count   = np.sum( hist_result )

					if binned_count > 0 : 
						hist_result = hist_result / binned_count

					result[ group ] = hist_result.ravel( order='F' )

				string_id = ".".join( hist[ "variables" ] )

				metadata[ group_key ][ "histograms" ][ hist_idx ][ "type" ] = "float"
				metadata[ group_key ][ "histograms" ][ hist_idx ][ "file" ] = group_key + ".hist." + string_id + ".bin"

				# Save the result
				result.tofile( output_dir + "/" + metadata[ group_key ][ "histograms" ][ hist_idx ][ "file" ] )

		if "sums" in summary[ group_key ] :

			print( "\ncomputing sums for the " + group_key + ", " + str( group_size ) + " regions\n" )

			sums_config = summary[ group_key ][ "sums" ]

			for v_idx in range( len( sums_config ) ) :

				v = sums_config[ v_idx ]

				print( "v=" + v )

				values = load_variable( variables[ v ], N_VOXELS )[ indices ]
				sums   = np.zeros( group_size, dtype="f4" )

				for group, group_data in grouped :

					grouped_values = values[ group_data.indices ]
					sums[ group ] = np.sum( grouped_values ) 

				# write to disk ...

				s = metadata[ group_key ][ "sums" ]
				s[ v_idx ] = { "variable" : v }
				s[ v_idx ][ "file" ] = group_key + ".sums."     +  v + ".bin"
				s[ v_idx ][ "type" ] = "float"

				sums.tofile( output_dir + "/" + s[ v_idx ][ "file" ] )

		if "abs_sums" in summary[ group_key ] :

			print( "\ncomputing abs_sums for the " + group_key + ", " + str( group_size ) + " regions\n" )

			sums_config = summary[ group_key ][ "abs_sums" ]

			for v_idx in range( len( sums_config ) ) :

				v = sums_config[ v_idx ]

				print( "v=" + v )

				values = load_variable( variables[ v ], N_VOXELS )[ indices ]
				sums   = np.zeros( group_size, dtype="f4" )

				for group, group_data in grouped :

					grouped_values = values[ group_data.indices ]
					sums[ group ] = np.sum( np.abs( grouped_values ) ) 

				# write to disk ...

				s = metadata[ group_key ][ "abs_sums" ]
				s[ v_idx ] = { "variable" : v }
				s[ v_idx ][ "file" ] = group_key + ".abs_sums."     +  v + ".bin"
				s[ v_idx ][ "type" ] = "float"

				sums.tofile( output_dir + "/" + s[ v_idx ][ "file" ] )

		if "squared_sums" in summary[ group_key ] :

			print( "\ncomputing squared sums for the " + group_key + ", " + str( group_size ) + " regions\n" )

			squared_sums_config = summary[ group_key ][ "squared_sums" ]

			for v_idx in range( len( squared_sums_config ) ) :

				v = squared_sums_config[ v_idx ]

				print( "v=" + v )

				values = load_variable( variables[ v ], N_VOXELS )[ indices ]
				squared_sums = np.zeros( group_size, dtype="f4" )

				for group, group_data in grouped :

					grouped_values = values[ group_data.indices ]
					squared_sums[ group ] = np.sum( np.square( grouped_values ) ) 

				# write to disk ...

				s = metadata[ group_key ][ "squared_sums" ]
				s[ v_idx ] = { "variable" : v }
				s[ v_idx ][ "file" ] = group_key + ".squared_sums."     +  v + ".bin"
				s[ v_idx ][ "type" ] = "float"
				
				squared_sums.tofile( output_dir + "/" + s[ v_idx ][ "file" ] )

		if "spectral_energy" in summary[ group_key ] :

			print( "\ncomputing mean spectral energy normalized by total energy for " + group_key + ", " + str( group_size ) + " regions\n" )

			cfgs = summary[  group_key ][ "spectral_energy" ]
			s    = metadata[ group_key ][ "spectral_energy" ]

			for e_idx in range( len( cfgs ) ) :

				cf = cfgs[ e_idx ]
				v  = cf[ "variable" ]

				# check if the n (the length of the CTF window was specified for bandwidth normalization)

				bandwidth_numerator = 1.0
				if "n" in cf :
					bandwidth_numerator = float( cf[ "n" ] )
					print( "found n=" + str(bandwidth_numerator) + " in spectral energy summarization configuration" )
				else :
					print( "n not found in spectral energy summarization configuration" )

				print( "v=" + v )

				s[ e_idx ][ "files" ] = []
				s[ e_idx ][ "type"  ] = "float"

				# compute the total energy from the entire grid of raw data

				values_all_scales = load_variable( variables[ v ], N_VOXELS ).astype( 'f8' )
				total_energy = np.sum( np.square( values_all_scales ) )

				for scale_band in cf[ "scale_decomposition" ] :

					scale_values = [ float( sb ) for sb in scale_band.split( "." ) ]

					j_min = np.min( scale_values )
					j_max = np.max( scale_values ) + 1

					largest_scale  = bandwidth_numerator / 2**j_min
					smallest_scale = bandwidth_numerator / 2**j_max

					scale_band_width = largest_scale - smallest_scale

					print( "scale band: "       + str( scale_band       ) )
					print( "largest_scale: "    + str( largest_scale    ) )
					print( "smallest_scale: "   + str( smallest_scale   ) )
					print( "scale band_width: " + str( scale_band_width ) )

					var_name = v + "_" + scale_band
					scale_data = load_variable( variables[ var_name ], N_VOXELS )[ indices ].astype( 'f8' )		
					mean_group_energy = np.zeros( group_size, dtype="f8" )

					output_file_name = group_key + ".spectral_energy." + var_name + ".bin"
					s[ e_idx ][ "files" ].append( output_file_name )

					for group, group_data in grouped :
				
						grouped_values = scale_data[ group_data.indices ]
						mean_group_energy[ group ] = np.sum( np.abs( grouped_values ) ) / scale_band_width				

						# should never be an empty region, just being paranoid
						region_size = group_data.indices.size
						
						if region_size > 0 :

							# normalize by the group size
							mean_group_energy[ group ] = mean_group_energy[ group ] / region_size

					# back to float from double
					mean_group_energy = mean_group_energy.astype( 'f4' )

					# write to disk ...
					mean_group_energy.tofile( output_dir + "/" + output_file_name )

	with open( base_dir + "/metadata.json", 'w') as f:
		json.dump( metadata, f, ensure_ascii=True, indent=4, sort_keys=False )