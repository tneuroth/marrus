
import sys
from summarize import summarize
import json
import copy

n = len(sys.argv)

if n != 2:
	print( "requires command line argument: <tessellation directory>" )
	exit()

tessellation_directory = sys.argv[ 1 ]

variables = None

with open( tessellation_directory + "/summary/variables.json" ) as f:
    variables = json.load(f)

configuration = None

with open( tessellation_directory + "/summary/configuration.json" ) as f:
    configuration = json.load(f)

#################################################################

summarize( variables, configuration, tessellation_directory )

