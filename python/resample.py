
from scipy.interpolate import RegularGridInterpolator
import numpy as np
    
N = 1024
f = "turb_shear_no_flame.6.0000E-04_temp_1024_block_float32.raw"

dx = 6.946454413892908e-06
dy = 7.400000000035850e-06
dz = 7.033997655334115e-06

dl = 5.875e-06 * (3/4) # new grid spacing, results in bins that are multiples of 3 fs
# dl = 5.875e-06         # new grid spacing, results in bins that are multiples of 2 fs

x_coords = np.linspace( 0, N*dx, N, endpoint=True )
y_coords = np.linspace( 0, N*dy, N, endpoint=True )
z_coords = np.linspace( 0, N*dz, N, endpoint=True )

dt = "f4"
values = np.fromfile( f, dtype=dt ).reshape( ( N, N, N ) )

interpolator = RegularGridInterpolator( ( x_coords, y_coords, z_coords ), values )

# to center the new grid points
x_start = int( ( int( N*dx / dl ) - N ) / 2 ) 
y_start = int( ( int( N*dy / dl ) - N ) / 2 ) 
z_start = int( ( int( N*dz / dl ) - N ) / 2 ) 

x_new = np.linspace( start=x_coords[ x_start ], stop=N*dl, num=N, endpoint=True )
y_new = np.linspace( start=y_coords[ y_start ], stop=N*dl, num=N, endpoint=True )
z_new = np.linspace( start=z_coords[ z_start ], stop=N*dl, num=N, endpoint=True )

xg, yg, zg = np.meshgrid( x_new, y_new, z_new )
new_grid_points = np.array( [ xg.ravel(), yg.ravel(), zg.ravel() ] ).T

new_values = interpolator( new_grid_points ).reshape( xg.shape ).astype( dt )
new_values.tofile( f.replace( ".raw", ".resampled.raw" ) )
