import numpy as np
import matplotlib.pyplot as plt
import bisect
import textwrap

len_x = 2.4e-2
len_y = 1.6e-2
len_z = 1.8e-2

grid_x = 3456
grid_y = 1280
grid_z = 2560

def indexAtY( y, coords ) :
	return bisect.bisect( coords, y )
	
def getCoords( file ) :
    
    strbits = np.fromfile( file, dtype=np.int8 )
    a_string = ''.join( [ chr( item ) for item in strbits ] )
    segments = a_string.split()
    return np.array( [ float( c ) for c in segments ] )

x_coords = getCoords( "../../../x.dat" )
y_coords = getCoords( "../../../y.dat" )
z_coords = getCoords( "../../../z.dat" )

print( x_coords.size )
print( y_coords.size )
print( z_coords.size )

mid_y = int( y_coords.size / 2 )

dx = abs( x_coords[ 1 ]     - x_coords[ 2 ]         )
dy = abs( y_coords[ mid_y ] - y_coords[ mid_y - 1 ] )
dz = abs( z_coords[ 1 ]     - z_coords[ 2 ]         )

print( "dx: ", dx )
print( "dy: ", dy )
print( "dz: ", dz )

print( "lx/gx: ", len_x / grid_x )
print( "ly/gy: ", len_y / grid_y )
print( "lz/gz: ", len_z / grid_z )

av = ( dx + dy + dz ) / 3
print( "average: " , av )

# print( str( abs( x_coords[ 0 ] - x_coords[ x_coords.size - 1 ] ) ) )
# print( str( abs( y_coords[ 0 ] - y_coords[ y_coords.size - 1 ] ) ) )
# print( str( abs( z_coords[ 0 ] - z_coords[ z_coords.size - 1 ] ) ) )

plt.plot( y_coords )
plt.xlabel( "xrid Index" )
plt.ylabel( "x coordinate" )
plt.show()

flameScale = 188e-6
n = 512
while n >= 2 :
	scale = n*dx
	scale_f = scale / flameScale
	print( scale_f )
	n = n / 2
