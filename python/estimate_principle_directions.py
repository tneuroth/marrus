import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import sys
import os

def compute_hessian(scalar_field, x, y, z, dx=1.0, dy=1.0, dz=1.0):
    
    H = np.zeros((3, 3))
    
    max_x, max_y, max_z = scalar_field.shape

    def get_value(i, j, k):
        # Check bounds and return scalar field value or the central value if out of bounds
        if i < 0 or i >= max_x or j < 0 or j >= max_y or k < 0 or k >= max_z:
            return scalar_field[ x, y, z ]  # Use the central value for out-of-bounds
        return scalar_field[ i, j, k ]

    # Compute second derivatives (diagonal elements)
    H[0, 0] = (get_value(x + 1, y, z) - 2 * scalar_field[x, y, z] + get_value(x - 1, y, z)) / (dx ** 2)
    H[1, 1] = (get_value(x, y + 1, z) - 2 * scalar_field[x, y, z] + get_value(x, y - 1, z)) / (dy ** 2)
    H[2, 2] = (get_value(x, y, z + 1) - 2 * scalar_field[x, y, z] + get_value(x, y, z - 1)) / (dz ** 2)

    # Compute mixed partial derivatives (off-diagonal elements)
    H[0, 1] = H[1, 0] = (
          get_value(x + 1, y + 1, z) - get_value(x + 1, y - 1, z)
        - get_value(x - 1, y + 1, z) + get_value(x - 1, y - 1, z)
    ) / (4 * dx * dy)

    H[0, 2] = H[2, 0] = (
          get_value(x + 1, y, z + 1) - get_value(x + 1, y, z - 1)
        - get_value(x - 1, y, z + 1) + get_value(x - 1, y, z - 1)
    ) / (4 * dx * dz)

    H[1, 2] = H[2, 1] = (
          get_value(x, y + 1, z + 1) - get_value(x, y + 1, z - 1)
        - get_value(x, y - 1, z + 1) + get_value(x, y - 1, z - 1)
    ) / (4 * dy * dz)

    return H

def principal_directions_of_curvature( scalar_field, x, y, z ):
    H = compute_hessian( scalar_field, x, y, z )
    eigenvalues, eigenvectors = np.linalg.eigh( H )
    return eigenvalues, eigenvectors

n = len(sys.argv)

tessellation_path = sys.argv[1]
data_file         = sys.argv[2]
data_offset       = int( sys.argv[3] )
C_data_type       = sys.argv[4]
data_type = "f4" if C_data_type == "float" else "f8"

X = int( sys.argv[5] )
Y = int( sys.argv[6] )
Z = int( sys.argv[7] )

dims = ( Z, Y, X )
f = open( data_file, "rb" )
f.seek( data_offset * np.dtype(data_type).itemsize, os.SEEK_SET )
values = np.fromfile( f, dtype=data_type, count=X*Y*Z ).reshape( dims )
f.close()

sites       = np.fromfile( tessellation_path + "/sitePoints.bin", dtype='f4' )
siteLayers  = np.fromfile( tessellation_path + "/siteLayers.bin", dtype='int16' )

sites = sites.reshape( ( int( sites.shape[ 0 ] / 3 ), 3 )  )

u  = np.zeros( sites.shape ).astype( 'f4' )
v  = np.zeros( sites.shape ).astype( 'f4' )
n  = np.zeros( sites.shape ).astype( 'f4' )
c  = np.zeros( ( sites.shape[ 0 ] ) ).astype( 'f4' )
mc = np.zeros( ( sites.shape[ 0 ] ) ).astype( 'f4' )

gradients = np.gradient( values )
grad_x, grad_y, grad_z = gradients

for i in range( sites.shape[ 0 ] ) :

    site = sites[ i ]

    x = min( int( dims[ 0 ] * site[ 2 ] ), dims[ 0 ] - 1 )
    y = min( int( dims[ 1 ] * site[ 1 ] ), dims[ 1 ] - 1 )
    z = min( int( dims[ 2 ] * site[ 0 ] ), dims[ 2 ] - 1 )

    vl = ( values[ x, y, z ], siteLayers[ i ], ( x, y, z ) )

    k, d = principal_directions_of_curvature( 
        values, x, y, z )    

    c[  i ] =   k[ 0 ] * k[ 2 ]
    mc[ i ] = ( k[ 0 ] + k[ 2 ] ) / 2.0

    normal_vector  = np.array( [ grad_z[ x, y, z ], grad_y[x, y, z], grad_x[x, y, z] ] )
    normal_vector /= np.linalg.norm( normal_vector )

    u[ i ] = [ d[ 0 ][ 2 ], d[ 0 ][ 1 ], d[ 0 ][ 0 ] ]
    v[ i ] = [ d[ 2 ][ 2 ], d[ 2 ][ 1 ], d[ 2 ][ 0 ] ]
    n[ i ] = normal_vector; # d[ 1 ]

c.tofile(  tessellation_path + "/site.guassian_curvature.bin"    )
mc.tofile( tessellation_path + "/site.mean_curvature.bin"        )
u.tofile(  tessellation_path + "/site.principle_direction_1.bin" )
v.tofile(  tessellation_path + "/site.principle_direction_2.bin" )
n.tofile(  tessellation_path + "/site.normal.bin" )
