import numpy as np

D = ( 512, 512, 512 )

input_directory = "/home/ubuntu/dev/marrus/multiscale/512/data/"
output_directory = "/home/ubuntu/dev/marrus/multiscale/"

files = [ "H2O", "H2O2", "OH", "temp", "pressure", "velx", "vely", "velz" ]
files = [ "flame_distance" ]

for f in files :
	data = np.fromfile( input_directory + f + ".bin", dtype='f4' ).reshape( D )
	d = D[0]
	for i in range(3):
		outfile = output_directory + str( d ) + "/data/" + f + ".bin"
		result  = data[ -d:, -d:, -d: ]
		result.tofile( outfile )
		d = d//2
