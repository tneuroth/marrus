import numpy as np
import matplotlib.pyplot as plt
import random
from matplotlib import colors
from scipy import ndimage
import sys
import random 
from random import randrange

tessellation_path = "../test_data/test_data_3D/slice_tessellation/"
dims =(200,200)

def index3d( id, X, Y, Z ) :
    x = id % X;
    tmp = ( id - x ) / X
    y = tmp % Y;
    z  = ( tmp - y ) / Y
    return ( x, y, z )

plt.rcParams["figure.figsize"] = [ 5, 5 ]

plt.axis('off')
plt.tight_layout()

contours = [ 0, 9, 18, 27 ]

voxel_sites = np.fromfile( tessellation_path + "voxelSiteIds.bin",      dtype='int32' )
voxel_comps = np.fromfile( tessellation_path + "voxelComponentIds.bin", dtype='int32' )
statuses = np.fromfile( tessellation_path    + "voxelStatus.bin",       dtype='uint8' )

sites = np.fromfile( tessellation_path + "sitePoints.bin", dtype='f4' )
NS = int( len( sites ) / 3 ) 

print( "NS=" + str(NS))

x = [ sites[ i * 3     ] * dims[0] - 0.5 for i in range( NS ) ]
y = [ sites[ i * 3 + 1 ] * dims[1] - 0.5 for i in range( NS ) ]

###########################################################################################3

#plt.imshow( voxel_comps )
# plt.show()

############################################################################################

Temp     = np.fromfile(   "../test_data/test_data_3D/temp_slice.200.200.8.bin", dtype="f4" ).reshape( (8, 200,200) ) 
fieldDat = np.fromfile( "../test_data/test_data_3D/distance_slice.200.200.bin", dtype="f4" ).reshape( dims ) 

######################################################################

mn = np.min( voxel_sites )
mx = np.max( voxel_sites )

perm = np.arange( 0, mx+2, 1 )
random.shuffle( perm )

voxel_sites = np.asarray( [ x if x < 0 else perm[ x + 1 ] for x in voxel_sites ] ).reshape( dims )

divnorm = colors.Normalize( vmin=mn, vmax=mx     )
norm    = colors.Normalize( vmin=mn, vmax=mx*1.3 )
cmap    = plt.cm.tab20b
m       = plt.cm.ScalarMappable( norm=norm, cmap=cmap )
tessRGB = m.to_rgba( voxel_sites )

statuses2d = statuses.reshape( ( 200, 200 ) )

for i in range( 200 ) :
	for j in range( 200 ) :
		if voxel_sites[ i, j ] < 0:
			tessRGB[ i, j, :3 ] = 1.0, 1.0, 1.0

while True :

	perm2 = np.arange( 0, mx+2, 1 )
	random.shuffle( perm2 )

	recolor = {}

	for i in range( 200 ) :
		for j in range( 200 ) :

			if voxel_sites[ i, j ] < 0:
				continue

			conflict = False

			color1 = tessRGB[ i, j ]
			site1  = voxel_sites[ i, j ]

			for dx in [ -1, 1 ] :
				for dy in [ -1, 1 ] :
					if i + dx >= 0 and i + dx < 200 and j + dy >= 0 and j + dy < 200 :

						color2 = tessRGB[     i + dx, j + dy ]
						site2  = voxel_sites[ i + dx, j + dy ]

						if site1 != site2 and np.array_equal( color1, color2 ) :
							conflict = True
							break
				
				if conflict == True :
					break

			if conflict == True :
				recolor[ site1 ] = True

	if not recolor :
		break

	for i in range( 200 ) :
		for j in range( 200 ) :
			site_id = voxel_sites[ i , j ]
			if site_id in recolor :
				tessRGB[ i, j, 0:3 ] = cmap( perm2[ site_id ] / mx )[ 0:3 ]

# show the Voronoi regions
plt.imshow( tessRGB )

#############################################################################

# show the bands
plt.contour( fieldDat, contours, linewidths=4.0, colors="black" )

# show the bands
# plt.contour( fieldDat, contours, linewidths=2.0, cmap=plt.cm.summer )

# show the Voronoi sites
plt.scatter( x, y, color=( 0.2, 0.2, 0.2 ), s=99 )
plt.scatter( x, y, color=( 1.0, 1.0, 1.0 ), s=70 )

plt.show()
