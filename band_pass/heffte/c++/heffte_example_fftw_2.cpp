#include "heffte.h"

/*!
 * \brief HeFFTe example 1, simple DFT using two MPI ranks and FFTW backend.
 *
 * Performing DFT on three dimensional data in a box of 4 by 4 by 4 split
 * across the third dimension between two MPI ranks.
 */

inline double k_shifted( int i, int N, double L )
{
    return ( i < N / 2 ) ? ( i     ) * ( 2.0 * M_PI ) / L 
                         : ( i - N ) * ( 2.0 * M_PI ) / L;
}

void compute_dft(MPI_Comm comm){

    int rank; // this process rank within the comm
    MPI_Comm_rank(comm, &rank);

    int num_ranks; // total number of ranks in the comm
    MPI_Comm_size(comm, &num_ranks);

    std::string filepath;
    int XD, YD, ZD;
    std::vector< float > bin_edges;

    std::ifstream file("options.txt");

    // File path
    
    std::getline(file, filepath);
    
    std::string dim_line;
    std::getline(file, dim_line);
    std::istringstream(dim_line) >> XD >> YD >> ZD;
    
    std::string bin_line;
    std::getline(file, bin_line);
    std::istringstream bin_stream(bin_line);

    float value;
    while ( bin_stream >> value ) 
    {
        bin_edges.push_back(value);
    }

    std::string output_directory;
    std::getline( file, output_directory );

    // Read my 

    int64_t my_start_z    = rank * ( ZD / num_ranks );
    int64_t my_end_z      = rank == num_ranks - 1 ? ZD : my_start_z + ZD / num_ranks;
    int64_t my_z_size     = my_end_z - my_start_z;
    int64_t my_N_elements = my_z_size * XD * YD;

    std::vector< float > v( my_N_elements );

    std::cout << my_start_z << std::endl;

    MPI_File inFile;
    MPI_File_open(MPI_COMM_WORLD, filepath.c_str(), MPI_MODE_RDONLY, MPI_INFO_NULL, &inFile );
    MPI_Offset read_offset = my_start_z * XD * YD * sizeof( float );
    MPI_File_read_at( inFile, read_offset, v.data(), my_N_elements, MPI_FLOAT, MPI_STATUS_IGNORE );
    MPI_File_close( &inFile );

    // define box
    heffte::box3d<> const my_box = { { 0, 0, my_start_z }, { XD-1, YD-1, my_end_z-1 } };

    // define the heffte class and the input and output geometry
    heffte::fft3d<heffte::backend::fftw> fft( my_box, my_box, comm );

    // vectors with the correct sizes to store the input and output data
    // taking the size of the input and output boxes
    std::vector<std::complex<float>> spatial_domain(   fft.size_inbox()  );
    std::vector<std::complex<float>> frequency_domain( fft.size_outbox() );

    // copy field data to fft input
    for( size_t i = 0; i < my_N_elements; ++i )
    {
        spatial_domain[ i ] = { v[ i ], 0.f };
    }

    // perform a forward DFT
    fft.forward( spatial_domain.data(), frequency_domain.data() );

    int nEdges = bin_edges.size();
    for( int e0 = 0; e0 < nEdges - 1; ++e0 )
    {
        // make a copy of original frequency domain data
        std::vector<std::complex<float>> frequency_domain_filtered = frequency_domain;

        double lambda_0 = bin_edges[ e0     ];
        double lambda_1 = bin_edges[ e0 + 1 ];

        double k0 = 2.0 * M_PI / lambda_0;
        double k1 = 2.0 * M_PI / lambda_1;

        double theta = 500.0;

        for( int64_t x = 0; x < XD; ++x )
        for( int64_t y = 0; y < YD; ++y )
        for( int64_t z = my_start_z; z < my_end_z; ++z )
        {
            double kx = k_shifted( x, XD, XD );
            double ky = k_shifted( y, YD, YD );
            double kz = k_shifted( z, ZD, ZD );

            double k = std::sqrt( kx*kx + ky*ky + kz*kz );

            double attenuation = k < k0 ? std::exp( -theta/k0 * ( k - k0 )*( k - k0 ) ) :
                                 k > k1 ? std::exp( -theta/k1 * ( k - k1 )*( k - k1 ) ) : 1;

            int64_t idx = x + y*XD + ( z - my_start_z )*XD*YD;

            frequency_domain_filtered[ idx ] *= attenuation;
        }   

        // do the inverse

        fft.backward( frequency_domain_filtered.data(), spatial_domain.data() );
    
        // copy the result to v 
        for( size_t i = 0; i < spatial_domain.size(); ++i )
        {
            v[ i ] = spatial_domain[ i ].real();
        }

        // Write results to disk

        std::string outFilePath = "result." 
            + std::to_string( int( bin_edges[ e0 ] ) ) + "-" 
            + std::to_string( int( bin_edges[ e0 + 1 ] ) ) + ".bin";

        MPI_File outFile;
        MPI_File_open( MPI_COMM_WORLD, outFilePath.c_str(), MPI_MODE_CREATE | MPI_MODE_WRONLY, MPI_INFO_NULL, &outFile );
        MPI_Offset write_offset = MPI_Offset( my_start_z * XD * YD ) * sizeof( float );
        MPI_File_write_at( outFile, write_offset, v.data(), v.size(), MPI_FLOAT, MPI_STATUS_IGNORE );
        MPI_File_close( &outFile );
    }
}

int main(int argc, char** argv){

    MPI_Init(&argc, &argv);

    compute_dft(MPI_COMM_WORLD);

    MPI_Finalize();

    return 0;
}
