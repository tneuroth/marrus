#include "heffte.h"

// There are multiple ways to select the GPU tag

// 1. Using the default_backend trait with the tag::gpu for the location
using backend_tag = heffte::backend::default_backend<heffte::tag::gpu>::type;

// 2. Using the preprocessor macros
#ifdef Heffte_ENABLE_CUDA
using preprocess_tag = heffte::backend::cufft;
#endif
#ifdef Heffte_ENABLE_ROCM
using preprocess_tag = heffte::backend::rocfft;
#endif
#ifdef Heffte_ENABLE_ONEAPI
using preprocess_tag = heffte::backend::onemkl;
#endif

// 3. Using project specific type-traits and the heffte::backend::is_enabled template.
//    While most cumbersome, the heffte::backend::is_enabled can be used to specialize
//    other methods and classes for purposes outside of a simple backend selection.

// if rocfft is enabled, the fallback struct will pick the rocfft as the backend
// however, if not enabled, the specialization will take precedence and onemkl will be chosen
template<typename, typename = void> struct pick_fallback { using type = heffte::backend::rocfft; };
template<typename pick>
struct pick_fallback<pick, typename std::enable_if<not heffte::backend::is_enabled<pick>::value, void>::type>{
    using type = heffte::backend::onemkl;
};

// if the pick is an enabled backend, it will be selected
// however, if not enabled, then the fallback will be used
template<typename pick, typename = void> struct pick_main { using type = pick; };
template<typename pick>
struct pick_main<pick, typename std::enable_if<not heffte::backend::is_enabled<pick>::value, void>::type>{
    using type = typename pick_fallback<heffte::backend::rocfft>::type;
};

// alias that uses a default value to initiate the chain of specializations that will yield the correct backend
template<typename pick = heffte::backend::cufft>
using pick_one = pick_main<pick>;

// the trait_tag will pick one available GPU backend
using trait_tag = typename pick_one<>::type;

inline double k_shifted( int i, int N, double L )
{
    return ( i < N / 2 ) ? ( i     ) * ( 2.0 * M_PI ) / L 
                         : ( i - N ) * ( 2.0 * M_PI ) / L;
}

/*!
 * \brief HeFFTe example 5, using the cuFFT backend.
 *
 * This example is near identical to the first (fftw) example,
 * the main difference is the use of the cufft backend.
 * The interface and types for the cufft backend work the same,
 * with the exception that the array must sit on the GPU device
 * and if the optional vector interface is used then the vector
 * containers are of type heffte::cuda::vector.
 */
void compute_dft(MPI_Comm comm){

    static_assert(std::is_same<backend_tag, preprocess_tag>::value, "Mismatch between Heffte backends chosen by the preprocessor and the default_backend struct.");
    static_assert(std::is_same<backend_tag, trait_tag>::value, "Mismatch between Heffte backends chosen by the trait type and the default_backend struct.");

    int rank; // this process rank within the comm
    MPI_Comm_rank(comm, &rank);

    int num_ranks; // total number of ranks in the comm
    MPI_Comm_size(comm, &num_ranks);

    std::string filepath;
    int XD, YD, ZD;
    std::vector< float > bin_edges;

    std::ifstream file("options.txt");

    // File path
    
    std::getline(file, filepath);
    
    std::string dim_line;
    std::getline(file, dim_line);
    std::istringstream(dim_line) >> XD >> YD >> ZD;
    
    std::string bin_line;
    std::getline(file, bin_line);
    std::istringstream bin_stream(bin_line);

    float value;
    while ( bin_stream >> value ) 
    {
        bin_edges.push_back(value);
    }

    heffte::gpu::device_set(0);

    // if (heffte::gpu::device_count() > 1){
    //     // on a multi-gpu system, distribute the devices across the mpi ranks
    //     heffte::gpu::device_set(heffte::mpi::comm_rank(comm) % heffte::gpu::device_count());
    // }

    // Read my 

    int64_t my_start_z    = rank * ( ZD / num_ranks );
    int64_t my_end_z      = rank == num_ranks - 1 ? ZD : my_start_z + ZD / num_ranks;
    int64_t my_z_size     = my_end_z - my_start_z;
    int64_t my_N_elements = my_z_size * XD * YD;

    std::vector< float > v( my_N_elements );

    std::cout << my_start_z << std::endl;

    MPI_File inFile;
    MPI_File_open(MPI_COMM_WORLD, filepath.c_str(), MPI_MODE_RDONLY, MPI_INFO_NULL, &inFile );
    MPI_Offset read_offset = my_start_z * XD * YD * sizeof( float );
    MPI_File_read_at( inFile, read_offset, v.data(), my_N_elements, MPI_FLOAT, MPI_STATUS_IGNORE );
    MPI_File_close( &inFile );

    // define box
    heffte::box3d<> const my_box = { { 0, 0, my_start_z }, { XD-1, YD-1, my_end_z-1 } };

    // define the heffte class and the input and output geometry
    heffte::fft3d<backend_tag> fft(my_box, my_box, comm);

    // vectors with the correct sizes to store the input and output data
    // taking the size of the input and output boxes
    std::vector<std::complex<float>> spatial_domain(   fft.size_inbox()  );

    // copy field data to fft input
    for( size_t i = 0; i < my_N_elements; ++i )
    {
        spatial_domain[ i ] = { v[ i ], 0.f };
    }

    // copy input data to the GPU
    heffte::gpu::vector<std::complex<float>> gpu_input = heffte::gpu::transfer().load( spatial_domain );

    // allocate memory on the device for the output
    heffte::gpu::vector<std::complex<float>> gpu_output(fft.size_outbox());

    // allocate scratch space, this is using the public type alias buffer_container
    // and for the cufft backend this is heffte::gpu::vector
    // for the CPU backends (fftw and mkl) the buffer_container is std::vector
    heffte::fft3d<backend_tag>::buffer_container<std::complex<float>> workspace(fft.size_workspace());
    static_assert(std::is_same<decltype(gpu_output), decltype(workspace)>::value,
                  "the containers for the output and workspace have different types");


    // perform forward fft using arrays and the user-created workspace
    fft.forward( gpu_input.data(), gpu_output.data(), workspace.data(), heffte::scale::full );

    // move the result back to the CPU
    std::vector<std::complex<float>> frequency_domain = heffte::gpu::transfer::unload( gpu_output );

    int nEdges = bin_edges.size();
    for( int e0 = 0; e0 < nEdges - 1; ++e0 )
    {
        // make a copy of original frequency domain data
        std::vector<std::complex<float>> frequency_domain_filtered = frequency_domain;

        double lambda_0 = bin_edges[ e0     ];
        double lambda_1 = bin_edges[ e0 + 1 ];

        double k0 = 2.0 * M_PI / lambda_0;
        double k1 = 2.0 * M_PI / lambda_1;

        double theta = 500.0;

        for( int64_t x = 0; x < XD; ++x )
        for( int64_t y = 0; y < YD; ++y )
        for( int64_t z = my_start_z; z < my_end_z; ++z )
        {
            double kx = k_shifted( x, XD, XD );
            double ky = k_shifted( y, YD, YD );
            double kz = k_shifted( z, ZD, ZD );

            double k = std::sqrt( kx*kx + ky*ky + kz*kz );

            double attenuation = k < k0 ? std::exp( -theta/k0 * ( k - k0 )*( k - k0 ) ) :
                                 k > k1 ? std::exp( -theta/k1 * ( k - k1 )*( k - k1 ) ) : 1;

            int64_t idx = x + y*XD + ( z - my_start_z )*XD*YD;

            frequency_domain_filtered[ idx ] *= attenuation;
        }   

        // copy filtered frequency data to gpu and do inverse

        heffte::gpu::vector<std::complex<float>> gpu_input = heffte::gpu::transfer().load( frequency_domain_filtered );

        fft.backward( gpu_input.data(), gpu_output.data() );
    
        // move the result back to the CPU for comparison purposes
        std::vector<std::complex<float>> filtered = heffte::gpu::transfer::unload( gpu_output );

        // copy the result to real valued vector

        for( size_t i = 0; i < filtered.size(); ++i )
        {
            v[ i ] = filtered[ i ].real();
        }

        // Write results to disk

        std::string outFilePath = "result." 
            + std::to_string( bin_edges[ e0 ] ) + "-" 
            + std::to_string( bin_edges[ e0 + 1 ] ) + ".bin";

        MPI_File outFile;
        MPI_File_open( MPI_COMM_WORLD, outFilePath.c_str(), MPI_MODE_CREATE | MPI_MODE_WRONLY, MPI_INFO_NULL, &outFile );
        MPI_Offset write_offset = MPI_Offset( my_start_z * XD * YD ) * sizeof( float );
        MPI_File_write_at( outFile, write_offset, v.data(), v.size(), MPI_FLOAT, MPI_STATUS_IGNORE );
        MPI_File_close( &outFile );
    }
}

int main(int argc, char** argv){

    MPI_Init(&argc, &argv);

    compute_dft(MPI_COMM_WORLD);

    MPI_Finalize();

    return 0;
}
