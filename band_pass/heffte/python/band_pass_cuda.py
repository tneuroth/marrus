
import heffte
import numpy as np
from numba import cuda as gpu
from mpi4py import MPI
import math

# backend, type of input data, scale #################################################

backend   = heffte.backend.cufft

# 32 bit float input -> 64 bit complex output (float32,float32)
# 64 bit float input -> 128 bit complex output (float64,float64)

data_type = np.float32
out_type  = np.complex64 if data_type == np.float32 else np.complex128

# don't scale the ouput

scale     = heffte.scale.none

# GPU Kernels to apply the filter #####################################################

@gpu.jit( device=True, inline=True )
def k_shifted( i, N, L ):
    return i * ( 2.0*np.pi ) / L if ( i < N / 2 ) else ( i - N ) * ( 2.0*np.pi ) / L 

@gpu.jit
def applyFilter( 
    freq_domain, 
    GX, GY, GZ, 
    LN, 
    z0, 
    k0, k1,
    theta_0, theta_1 ) :
    # get the flat index into the array
    idx = gpu.blockIdx.x * gpu.blockDim.x + gpu.threadIdx.x
    
    if idx >= LN:
        return
    
    # 3D indexing similar to C++ version
    x = idx % GX
    y = (idx // GX) % GY
    z = (idx // (GX * GY)) + z0
    
    kx = k_shifted( x, GX, GX )
    ky = k_shifted( y, GY, GY )
    kz = k_shifted( z, GZ, GZ )
    
    k = math.sqrt( kx*kx + ky*ky + kz*kz )
    
    attenuation = (math.exp( -theta_0/k0 * ( k - k0 )*( k - k0 ) ) if k < k0 else 
                   math.exp( -theta_1/k1 * ( k - k1 )*( k - k1 ) ) if k > k1 else 1)
    
    freq_domain[ idx ] *= attenuation

################################################################################################

def read_parameters(filename):

    with open(filename, 'r') as f:
    
        input_file  = f.readline().strip()
        output_file = f.readline().strip()
        dims        = list( map(   int, f.readline().split() ) )
        lambdas     = list( map( float, f.readline().split() ) )
        thetas      = list( map( float, f.readline().split() ) )
        padding     = f.readline().split()
        windowing   = f.readline().split()
    
    return input_file, output_file, dims, lambdas, thetas, padding, windowing

in_file, out_file, ( XD, YD, ZD ), lambdas, thetas, padding, windowing = read_parameters( 'options.txt' )

################################################################################################

comm      = MPI.COMM_WORLD
rank      = comm.Get_rank()
num_ranks = comm.Get_size()

################################################################################################

# decompose the domain for parallism over z (each rank gets a contiguous block of xy planes)

my_start_z    = rank * ( ZD // num_ranks );
my_end_z      = ZD if rank == num_ranks - 1 else my_start_z + ZD // num_ranks;
my_z_size     = my_end_z - my_start_z
my_N_elements = my_z_size * XD * YD

# set the block according to the decomposition 

box = heffte.box3d([  0, 0, my_start_z ], [ XD-1, YD-1, my_end_z-1 ] )

# create the fft object

fft = heffte.fft3d( backend, box, box, comm )

# create array to hold the input data for reading

in_array = np.empty( [ my_N_elements ], data_type )

# read data into input array

fh = MPI.File.Open( comm, in_file, MPI.MODE_RDONLY )
fh.Read_at( my_start_z * XD * YD * np.dtype( data_type ).itemsize, in_array )
fh.Close()

# select the gpu

gpu.select_device( rank % len( gpu.gpus ) ) 

# copy input to device and allocate array on GPU for output

spat_domain = gpu.to_device( in_array )
freq_domain = gpu.device_array( [ my_N_elements ], dtype=out_type )

# do the forward fft pass

fft.forward( spat_domain, freq_domain, scale )
freq_domain_host = freq_domain.copy_to_host()

# run the GPU kernel that will apply the band pass filter

blockSize = 256
grid_size = ( ( my_N_elements + blockSize - 1 ) // blockSize )

for e0 in range( len( lambdas ) - 1 ) :

    lambda_0 = lambdas[ e0     ]
    lambda_1 = lambdas[ e0 + 1 ]

    k0 = 2.0 * np.pi / lambda_0
    k1 = 2.0 * np.pi / lambda_1

    theta_0 = thetas[ e0*2     ]
    theta_1 = thetas[ e0*2 + 1 ]

    if e0 > 0 :
        freq_domain = gpu.to_device( freq_domain_host )

    applyFilter[ grid_size, blockSize ](
        freq_domain, 
        XD, YD, ZD, 
        my_N_elements, my_start_z, 
        k0, k1,
        theta_0, theta_1 )

    fft.backward( freq_domain, spat_domain, scale )

    # copy the result back to the host / CPU memory

    result = spat_domain.copy_to_host()

    # write the result to file

    outFilePath = "result." + str( lambda_0 ) + "." + str( lambda_1 ) + ".bin";
    fh = MPI.File.Open( comm, outFilePath, MPI.MODE_WRONLY | MPI.MODE_CREATE )
    fh.Write_at_all( my_start_z * XD * YD * np.dtype( data_type ).itemsize, result )
    fh.Close()


