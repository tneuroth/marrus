
import heffte
import numpy as np
from mpi4py import MPI
import math
from scipy.signal.windows import tukey

# tukey windows

use_tukey_y = True
tukey_alpha = 0.1

# backend, type of input data, scale #################################################

backend   = heffte.backend.fftw

# 32 bit float input -> 64 bit complex output (float32,float32)
# 64 bit float input -> 128 bit complex output (float64,float64)

data_type = np.float32
out_type  = np.complex64 if data_type == np.float32 else np.complex128

# don't scale the ouput

scale = heffte.scale.none

# Filtering #####################################################

# vectorized version, per xy plane instead of over whole 3D block to save memory

def k_shifted(i, N, L):
    factor = 2.0 * np.pi / L
    return np.where( i < N/2, i * factor, (i - N) * factor )

def applyFilter( 
    freq_domain, 
    GX, GY, GZ, 
    z0,zend,
    k0, k1,
    theta_0, theta_1 ) :

    # Pre-compute k values for x and y dimensions

    kx = k_shifted( np.arange(GX), GX, GX )[ None, : ] 
    ky = k_shifted( np.arange(GY), GY, GY )[ :, None ] 
    
    # Precompute the sum of squares for x and y

    kxy_sq = kx*kx + ky*ky 
    
    # Precompute factors for attenuation

    theta_0_k0 = -theta_0 / k0
    theta_1_k1 = -theta_1 / k1

   # Process one z plane at a time

    for z in range( z0, zend ):

        kz = k_shifted(z, GZ, GZ)
        
        # Calculate k magnitude for entire xy plane

        k = np.sqrt( kxy_sq + kz*kz )
        
        # Calculate attenuation factors
        attenuation = np.ones_like( k )

        mask_low  = k < k0
        mask_high = k > k1
        
        attenuation[ mask_low  ] = np.exp( theta_0_k0 * ( k[ mask_low  ] - k0 )**2)
        attenuation[ mask_high ] = np.exp( theta_1_k1 * ( k[ mask_high ] - k1 )**2)
        
        # Apply to current z plane
        plane_start =  ( z - z0 ) * ( GX * GY )
        plane_end   = plane_start + ( GX * GY )

        freq_domain[ plane_start : plane_end ] *= attenuation.ravel()

    return freq_domain

#######################################################################################

def read_parameters(filename):

    with open(filename, 'r') as f:
    
        input_file  = f.readline().strip()
        output_file = f.readline().strip()
        dims        = list( map(   int, f.readline().split() ) )
        lambdas     = list( map( float, f.readline().split() ) )
        thetas      = list( map( float, f.readline().split() ) )
        padding     = f.readline().split()
        windowing   = f.readline().split()
    
    return input_file, output_file, dims, lambdas, thetas, padding, windowing

in_file, out_file_prefix, ( XD, YD, ZD ), lambdas, thetas, padding, windowing = read_parameters( 'options.txt' )

################################################################################################

comm      = MPI.COMM_WORLD
rank      = comm.Get_rank()
num_ranks = comm.Get_size()

# decompose the domain for parallism over z (each rank gets a contiguous block of xy planes)

my_start_z    = rank * ( ZD // num_ranks );
my_end_z      = ZD if rank == num_ranks - 1 else my_start_z + ZD // num_ranks;
my_z_size     = my_end_z - my_start_z;
my_N_elements = my_z_size * XD * YD;

# set the block according to the decomposition 

box = heffte.box3d([  0, 0, my_start_z ], [ XD-1, YD-1, my_end_z-1 ] )

# create the fft object

fft = heffte.fft3d( backend, box, box, comm )

# create array to hold the input data for reading

in_array = np.empty( [ my_N_elements ], dtype=data_type )

# read data into input array

fh = MPI.File.Open( comm, in_file, MPI.MODE_RDONLY )
fh.Read_at( my_start_z * XD * YD * np.dtype( data_type ).itemsize, in_array )
fh.Close()

# apply the window

if use_tukey_y :
    in_array = in_array.reshape( ( my_z_size, YD, XD ) )
    window   = tukey( np.shape( in_array )[ 1 ], tukey_alpha ).reshape( 1, np.shape( in_array )[ 1 ], 1 ).astype( data_type )
    in_array = ( in_array * window ).ravel()

# copy input to device and allocate array on GPU for output

spat_domain = in_array
freq_domain = np.empty( [ my_N_elements ], dtype=np.complex64 )

# do the forward fft pass

fft.forward( spat_domain, freq_domain, scale )
freq_domain_copy = freq_domain.copy()

for e0 in range( len( lambdas ) - 1 ) :

    lambda_0 = lambdas[ e0     ]
    lambda_1 = lambdas[ e0 + 1 ]

    k0 = 2.0 * np.pi / lambda_0
    k1 = 2.0 * np.pi / lambda_1

    theta_0 = thetas[ e0*2     ]
    theta_1 = thetas[ e0*2 + 1 ]

    if e0 > 0 :
        freq_domain = freq_domain_copy.copy()

    applyFilter(
        freq_domain, 
        XD, YD, ZD, 
        my_start_z, my_end_z, 
        k0, k1,
        theta_0, theta_1 )

    fft.backward( freq_domain, spat_domain, scale )

    # write the result to file

    outFilePath = out_file_prefix + "." + str( lambda_0 ) + "." + str( lambda_1 ) + ".bin";
    fh = MPI.File.Open( comm, outFilePath, MPI.MODE_WRONLY | MPI.MODE_CREATE )
    offset = my_start_z * XD * YD * np.dtype( data_type ).itemsize
    fh.Write_at_all( offset, spat_domain )
    fh.Close()


