import numpy as np
import math
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
from numpy.fft import fft2, fftshift, ifftshift, ifft2

# Set global font size for the plot
plt.rcParams.update({
    'font.size': 14,        # Increase overall font size
    'axes.titlesize': 16,   # Title font size
    'axes.labelsize': 14,   # Axis label font size
    'xtick.labelsize': 12,  # X-axis tick label size
    'ytick.labelsize': 12,  # Y-axis tick label size
    'legend.fontsize': 12,  # Legend font size
    'figure.titlesize': 18  # Figure title font size
})

data = np.fromfile( 'temp.bin', dtype='f4' )
data = data[ 0 : 256*256 ].reshape( ( 256, 256 ) )

print(data.shape)

# Domain lengths (e.g. in meters)

Lx = 256
Ly = 256

# Number of grid points in x and y directions

N = 256
M = 256

# Length scale bins for filtering ( lambda_0 and lambda_1 )

scale_bins = [ 
    ( 256, 128 ), 
    ( 128, 64  ), 
    ( 64,  32  ), 
    ( 32,  16  ), 
    ( 16,  8   ), 
    ( 8,   4   ), 
    ( 4,   2   ) ]

lambdas = scale_bins[ 2 ]

# Convert length scales to wavenumbers (k = 2 * math.pi / lambda )

k0 = 2.0 * math.pi / lambdas[ 0 ]   # Lower bound wavenumber
k1 = 2.0 * math.pi / lambdas[ 1 ]   # Upper bound wavenumber

# Define sharpness parameter

theta = 500

# Perform forward FFT (convert field to frequency domain)

frequency_domain_data = fft2(data)

# frequency_domain_data = fftshift(frequency_domain_data)

freq_data = np.abs(frequency_domain_data)

for i in range(0,N):
    for j in range(0,M):

        # Compute wavenumbers in the x and y directions
        # Center the wavenumber grid (handle FFT wrapping)

        if i < N / 2:
            kx = i * ( 2.0 * math.pi / Lx )
        else:
            kx = (i - N) * ( 2.0 * math.pi / Lx )

        if j < M / 2:
            ky = j * ( 2.0 * math.pi / Ly )
        else:
            ky = (j - M) * ( 2.0 * math.pi / Ly )

        # Compute the radial wavenumber

        k = math.sqrt(kx * kx + ky * ky)

       # Compute the filter value using the piecewise Gaussian filter

        if k < k0:
            filter_value = math.exp( -theta/k0 * ( k - k0 )**2 )  # left side fall-off
        elif k > k1:
            filter_value = math.exp( -theta/k1 * ( k - k1 )**2 )  # right side fall-off
        else:
            filter_value = 1.0  # Pass-band region

        # Apply the filter to the frequency domain data

        frequency_domain_data[i][j] *= filter_value

# Perform inverse FFT (convert back to spatial domain)

freq_data2 = np.abs(frequency_domain_data)

# ifft_shifted = ifftshift(frequency_domain_data)
filtered_data = ifft2( frequency_domain_data ).real

w = np.linspace( 2.0 * math.pi / 256, 2.0 * math.pi / 2, 2048 )
y = np.zeros( ( w.size ) )

fig = plt.figure(figsize=(10, 14))
gs = gridspec.GridSpec(4, 2, height_ratios=[1, 1, 0.5, 0.5])

fig.suptitle(
    rf"Filtering Result For $\lambda_{{low}}={lambdas[0]}, \lambda_{{high}}={lambdas[1]}$",
    fontsize=18
)
ax5 = fig.add_subplot(gs[2, :])

for lambdas in scale_bins : 

    k0i = 2.0 * math.pi / lambdas[ 0 ] # Lower bound wavenumber
    k1i = 2.0 * math.pi / lambdas[ 1 ] # Upper bound wavenumber

    for i in range( w.size ) :

        k = w[ i ]

        if   k < k0i :
            y[ i ] = math.exp( -theta/k0i * ( k - k0i )**2 )  # Low-pass fall-off
        elif k > k1i: 
            y[ i ] = math.exp( -theta/k1i * ( k - k1i )**2 )  # Low-pass fall-off
        else:
            y[ i ] = 1.0

    fcolor = 'g' if k0 == k0i and k1 == k1i else 'gray'

    ax5.vlines(x=k0i, ymin=0, ymax=1, color='b')
    ax5.vlines(x=k1i, ymin=0, ymax=1, color='b')
    ax5.plot(w, y, color=fcolor)
    ax5.fill_between(w, 0, y, color=fcolor, alpha=0.75)

ax5.set_ylim(0, 1.1)
ax5.set_xscale('log')
ax5.set_xlabel(r'$\log k$')
ax5.set_ylabel("attenuation")
ax5.set_title("Piecewise Gaussian Bandpass Filters")

ax5_linear = fig.add_subplot(gs[3, :])

for lambdas in scale_bins : 

    k0i = 2.0 * math.pi / lambdas[ 0 ] # Lower bound wavenumber
    k1i = 2.0 * math.pi / lambdas[ 1 ] # Upper bound wavenumber

    for i in range( w.size ) :

        k = w[ i ]

        if   k < k0i :
            y[ i ] = math.exp( -theta/k0i * ( k - k0i )**2 )  # Low-pass fall-off
        elif k > k1i: 
            y[ i ] = math.exp( -theta/k1i * ( k - k1i )**2 )  # Low-pass fall-off
        else:
            y[ i ] = 1.0

    fcolor = 'g' if k0 == k0i and k1 == k1i else 'gray'

    ax5_linear.vlines(x=k0i, ymin=0, ymax=1, color='b')
    ax5_linear.vlines(x=k1i, ymin=0, ymax=1, color='b')
    ax5_linear.plot(w, y, color=fcolor)
    ax5_linear.fill_between(w, 0, y, color=fcolor, alpha=0.75)

ax5_linear.set_ylim(0, 1.1)
ax5_linear.set_xlabel(r'$k$')
ax5_linear.set_ylabel("attenuation")
ax5_linear.set_title("Piecewise Gaussian Bandpass Filters")

# Top-left plot
ax1 = fig.add_subplot(gs[0, 0])
ax1.set_title('Freq Before')
ax1.imshow(np.log(1 + fftshift(freq_data)), cmap='gray', origin='lower')
ax1.set_xlabel('k0')
ax1.set_ylabel('k1')
plt.colorbar(ax1.imshow(np.log(1 + fftshift(freq_data)), cmap='gray', origin='lower'), ax=ax1)

# Top-right plot
ax2 = fig.add_subplot(gs[0, 1])
ax2.set_title('Freq After')
ax2.imshow(np.log(1 + fftshift(freq_data2)), cmap='gray', origin='lower')
ax2.set_xlabel('k0')
ax2.set_ylabel('k1')
plt.colorbar(ax2.imshow(np.log(1 + fftshift(freq_data2)), cmap='gray', origin='lower'), ax=ax2)

# Bottom-left plot
ax3 = fig.add_subplot(gs[1, 0])
ax3.set_title('Original Data')
ax3.imshow(data, cmap='gray', origin='lower')
ax3.set_xlabel('X')
ax3.set_ylabel('Y')
plt.colorbar(ax3.imshow(data, cmap='gray', origin='lower'), ax=ax3)

# Bottom-right plot
ax4 = fig.add_subplot(gs[1, 1])
ax4.set_title('Filtered Data')
ax4.imshow(filtered_data, cmap='gray', origin='lower')
ax4.set_xlabel('X')
ax4.set_ylabel('Y')
plt.colorbar(ax4.imshow(filtered_data, cmap='gray', origin='lower'), ax=ax4)

plt.tight_layout()
plt.show()